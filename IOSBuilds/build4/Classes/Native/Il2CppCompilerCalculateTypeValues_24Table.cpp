﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Collections.Generic.List`1<GoogleARCoreInternal.CrossPlatform.CloudServiceManager/CloudAnchorRequest>
struct List_1_t4061118968;
// System.Predicate`1<GoogleARCoreInternal.CrossPlatform.CloudServiceManager/CloudAnchorRequest>
struct Predicate_1_t3414338350;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.IntPtr,GoogleARCore.Trackable>
struct Dictionary_2_t436085955;
// GoogleARCoreInternal.NativeSession
struct NativeSession_t1550589577;
// System.Collections.Generic.List`1<GoogleARCore.Trackable>
struct List_1_t3686294978;
// System.Collections.Generic.HashSet`1<GoogleARCore.Trackable>
struct HashSet_1_t779169710;
// System.Collections.Generic.List`1<GoogleARCoreInternal.ExperimentBase>
struct List_1_t1982983791;
// System.Func`2<System.Reflection.Assembly,System.Collections.Generic.IEnumerable`1<System.Type>>
struct Func_2_t779105388;
// System.Func`2<System.Type,System.Boolean>
struct Func_2_t561252955;
// GoogleARCoreInternal.ILifecycleManager
struct ILifecycleManager_t3414218955;
// System.Collections.Generic.List`1<GoogleARCore.TrackableHit>
struct List_1_t3385152071;
// GoogleARCoreInternal.ARPrestoCallbackManager/CheckApkAvailabilityResultCallback
struct CheckApkAvailabilityResultCallback_t3176003102;
// GoogleARCoreInternal.ARPrestoCallbackManager/RequestApkInstallationResultCallback
struct RequestApkInstallationResultCallback_t1625331025;
// GoogleARCoreInternal.ARPrestoCallbackManager/CameraPermissionRequestProvider
struct CameraPermissionRequestProvider_t2929024609;
// GoogleARCoreInternal.ARPrestoCallbackManager/EarlyUpdateCallback
struct EarlyUpdateCallback_t1406590691;
// GoogleARCoreInternal.ARPrestoCallbackManager/OnBeforeSetConfigurationCallback
struct OnBeforeSetConfigurationCallback_t2829502455;
// GoogleARCoreInternal.ARPrestoCallbackManager/OnBeforeResumeSessionCallback
struct OnBeforeResumeSessionCallback_t1391226892;
// System.Collections.Generic.List`1<System.Action`1<GoogleARCore.ApkAvailabilityStatus>>
struct List_1_t252299607;
// System.Collections.Generic.List`1<System.Action`1<GoogleARCore.ApkInstallationStatus>>
struct List_1_t3947428544;
// System.Action
struct Action_t1264377477;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Action`1<TKAngleSwipeRecognizer>
struct Action_1_t3839277408;
// System.Collections.Generic.Queue`1<System.Action>
struct Queue_1_t1110636971;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// GoogleARCoreInternal.ARPrestoCallbackManager/CameraPermissionsResultCallback
struct CameraPermissionsResultCallback_t3686029889;
// System.Action`1<GoogleARCore.CrossPlatform.CloudAnchorResult>
struct Action_1_t3619531002;
// System.Collections.Generic.List`1<TKTouch>
struct List_1_t34712449;
// GoogleARCore.ARCoreSession
struct ARCoreSession_t3898212540;
// GoogleARCore.ARCoreSessionConfig
struct ARCoreSessionConfig_t3885767874;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// GoogleARCore.CrossPlatform.XPAnchor
struct XPAnchor_t812171902;
// GoogleARCore.Trackable
struct Trackable_t2214220236;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// GoogleARCore.AugmentedImageDatabase
struct AugmentedImageDatabase_t1074037598;
// System.Action`1<TKAnyTouchRecognizer>
struct Action_1_t3824429044;
// System.Collections.Generic.List`1<GoogleARCore.AugmentedImageDatabaseEntry>
struct List_1_t647451162;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Collections.Generic.List`1<TKAngleSwipeRecognizer/AngleListener>
struct List_1_t1248166469;
// System.Collections.Generic.Dictionary`2<System.IntPtr,GoogleARCore.CrossPlatform.XPAnchor>
struct Dictionary_2_t3329004917;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.XR.ARBackgroundRenderer
struct ARBackgroundRenderer_t852496440;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.Text
struct Text_t1901882714;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef CLOUDSERVICEMANAGER_T1936018857_H
#define CLOUDSERVICEMANAGER_T1936018857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.CrossPlatform.CloudServiceManager
struct  CloudServiceManager_t1936018857  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<GoogleARCoreInternal.CrossPlatform.CloudServiceManager/CloudAnchorRequest> GoogleARCoreInternal.CrossPlatform.CloudServiceManager::m_CloudAnchorRequests
	List_1_t4061118968 * ___m_CloudAnchorRequests_1;

public:
	inline static int32_t get_offset_of_m_CloudAnchorRequests_1() { return static_cast<int32_t>(offsetof(CloudServiceManager_t1936018857, ___m_CloudAnchorRequests_1)); }
	inline List_1_t4061118968 * get_m_CloudAnchorRequests_1() const { return ___m_CloudAnchorRequests_1; }
	inline List_1_t4061118968 ** get_address_of_m_CloudAnchorRequests_1() { return &___m_CloudAnchorRequests_1; }
	inline void set_m_CloudAnchorRequests_1(List_1_t4061118968 * value)
	{
		___m_CloudAnchorRequests_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_CloudAnchorRequests_1), value);
	}
};

struct CloudServiceManager_t1936018857_StaticFields
{
public:
	// GoogleARCoreInternal.CrossPlatform.CloudServiceManager GoogleARCoreInternal.CrossPlatform.CloudServiceManager::s_Instance
	CloudServiceManager_t1936018857 * ___s_Instance_0;
	// System.Predicate`1<GoogleARCoreInternal.CrossPlatform.CloudServiceManager/CloudAnchorRequest> GoogleARCoreInternal.CrossPlatform.CloudServiceManager::<>f__am$cache0
	Predicate_1_t3414338350 * ___U3CU3Ef__amU24cache0_2;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(CloudServiceManager_t1936018857_StaticFields, ___s_Instance_0)); }
	inline CloudServiceManager_t1936018857 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline CloudServiceManager_t1936018857 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(CloudServiceManager_t1936018857 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(CloudServiceManager_t1936018857_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Predicate_1_t3414338350 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Predicate_1_t3414338350 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Predicate_1_t3414338350 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDSERVICEMANAGER_T1936018857_H
#ifndef SHELLHELPER_T1127567194_H
#define SHELLHELPER_T1127567194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ShellHelper
struct  ShellHelper_t1127567194  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHELLHELPER_T1127567194_H
#ifndef MARSHALINGHELPER_T999858804_H
#define MARSHALINGHELPER_T999858804_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.MarshalingHelper
struct  MarshalingHelper_t999858804  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARSHALINGHELPER_T999858804_H
#ifndef INTPTREQUALITYCOMPARER_T902448827_H
#define INTPTREQUALITYCOMPARER_T902448827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.IntPtrEqualityComparer
struct  IntPtrEqualityComparer_t902448827  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTREQUALITYCOMPARER_T902448827_H
#ifndef CONVERSIONHELPER_T900371905_H
#define CONVERSIONHELPER_T900371905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ConversionHelper
struct  ConversionHelper_t900371905  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERSIONHELPER_T900371905_H
#ifndef ARDEBUG_T3607742339_H
#define ARDEBUG_T3607742339_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ARDebug
struct  ARDebug_t3607742339  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARDEBUG_T3607742339_H
#ifndef ARCOREPROJECTSETTINGS_T3272920691_H
#define ARCOREPROJECTSETTINGS_T3272920691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ARCoreProjectSettings
struct  ARCoreProjectSettings_t3272920691  : public RuntimeObject
{
public:
	// System.String GoogleARCoreInternal.ARCoreProjectSettings::Version
	String_t* ___Version_0;
	// System.Boolean GoogleARCoreInternal.ARCoreProjectSettings::IsARCoreRequired
	bool ___IsARCoreRequired_1;
	// System.Boolean GoogleARCoreInternal.ARCoreProjectSettings::IsInstantPreviewEnabled
	bool ___IsInstantPreviewEnabled_2;
	// System.String GoogleARCoreInternal.ARCoreProjectSettings::CloudServicesApiKey
	String_t* ___CloudServicesApiKey_3;
	// System.String GoogleARCoreInternal.ARCoreProjectSettings::IosCloudServicesApiKey
	String_t* ___IosCloudServicesApiKey_4;

public:
	inline static int32_t get_offset_of_Version_0() { return static_cast<int32_t>(offsetof(ARCoreProjectSettings_t3272920691, ___Version_0)); }
	inline String_t* get_Version_0() const { return ___Version_0; }
	inline String_t** get_address_of_Version_0() { return &___Version_0; }
	inline void set_Version_0(String_t* value)
	{
		___Version_0 = value;
		Il2CppCodeGenWriteBarrier((&___Version_0), value);
	}

	inline static int32_t get_offset_of_IsARCoreRequired_1() { return static_cast<int32_t>(offsetof(ARCoreProjectSettings_t3272920691, ___IsARCoreRequired_1)); }
	inline bool get_IsARCoreRequired_1() const { return ___IsARCoreRequired_1; }
	inline bool* get_address_of_IsARCoreRequired_1() { return &___IsARCoreRequired_1; }
	inline void set_IsARCoreRequired_1(bool value)
	{
		___IsARCoreRequired_1 = value;
	}

	inline static int32_t get_offset_of_IsInstantPreviewEnabled_2() { return static_cast<int32_t>(offsetof(ARCoreProjectSettings_t3272920691, ___IsInstantPreviewEnabled_2)); }
	inline bool get_IsInstantPreviewEnabled_2() const { return ___IsInstantPreviewEnabled_2; }
	inline bool* get_address_of_IsInstantPreviewEnabled_2() { return &___IsInstantPreviewEnabled_2; }
	inline void set_IsInstantPreviewEnabled_2(bool value)
	{
		___IsInstantPreviewEnabled_2 = value;
	}

	inline static int32_t get_offset_of_CloudServicesApiKey_3() { return static_cast<int32_t>(offsetof(ARCoreProjectSettings_t3272920691, ___CloudServicesApiKey_3)); }
	inline String_t* get_CloudServicesApiKey_3() const { return ___CloudServicesApiKey_3; }
	inline String_t** get_address_of_CloudServicesApiKey_3() { return &___CloudServicesApiKey_3; }
	inline void set_CloudServicesApiKey_3(String_t* value)
	{
		___CloudServicesApiKey_3 = value;
		Il2CppCodeGenWriteBarrier((&___CloudServicesApiKey_3), value);
	}

	inline static int32_t get_offset_of_IosCloudServicesApiKey_4() { return static_cast<int32_t>(offsetof(ARCoreProjectSettings_t3272920691, ___IosCloudServicesApiKey_4)); }
	inline String_t* get_IosCloudServicesApiKey_4() const { return ___IosCloudServicesApiKey_4; }
	inline String_t** get_address_of_IosCloudServicesApiKey_4() { return &___IosCloudServicesApiKey_4; }
	inline void set_IosCloudServicesApiKey_4(String_t* value)
	{
		___IosCloudServicesApiKey_4 = value;
		Il2CppCodeGenWriteBarrier((&___IosCloudServicesApiKey_4), value);
	}
};

struct ARCoreProjectSettings_t3272920691_StaticFields
{
public:
	// GoogleARCoreInternal.ARCoreProjectSettings GoogleARCoreInternal.ARCoreProjectSettings::<Instance>k__BackingField
	ARCoreProjectSettings_t3272920691 * ___U3CInstanceU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ARCoreProjectSettings_t3272920691_StaticFields, ___U3CInstanceU3Ek__BackingField_7)); }
	inline ARCoreProjectSettings_t3272920691 * get_U3CInstanceU3Ek__BackingField_7() const { return ___U3CInstanceU3Ek__BackingField_7; }
	inline ARCoreProjectSettings_t3272920691 ** get_address_of_U3CInstanceU3Ek__BackingField_7() { return &___U3CInstanceU3Ek__BackingField_7; }
	inline void set_U3CInstanceU3Ek__BackingField_7(ARCoreProjectSettings_t3272920691 * value)
	{
		___U3CInstanceU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCOREPROJECTSETTINGS_T3272920691_H
#ifndef SESSIONSTATUSEXTENSIONS_T2174783218_H
#define SESSIONSTATUSEXTENSIONS_T2174783218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.SessionStatusExtensions
struct  SessionStatusExtensions_t2174783218  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONSTATUSEXTENSIONS_T2174783218_H
#ifndef SESSION_T56890619_H
#define SESSION_T56890619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Session
struct  Session_t56890619  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSION_T56890619_H
#ifndef TRACKABLEMANAGER_T1722633017_H
#define TRACKABLEMANAGER_T1722633017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.TrackableManager
struct  TrackableManager_t1722633017  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.IntPtr,GoogleARCore.Trackable> GoogleARCoreInternal.TrackableManager::m_TrackableDict
	Dictionary_2_t436085955 * ___m_TrackableDict_0;
	// GoogleARCoreInternal.NativeSession GoogleARCoreInternal.TrackableManager::m_NativeSession
	NativeSession_t1550589577 * ___m_NativeSession_1;
	// System.Int32 GoogleARCoreInternal.TrackableManager::m_LastUpdateFrame
	int32_t ___m_LastUpdateFrame_2;
	// System.Collections.Generic.List`1<GoogleARCore.Trackable> GoogleARCoreInternal.TrackableManager::m_NewTrackables
	List_1_t3686294978 * ___m_NewTrackables_3;
	// System.Collections.Generic.List`1<GoogleARCore.Trackable> GoogleARCoreInternal.TrackableManager::m_AllTrackables
	List_1_t3686294978 * ___m_AllTrackables_4;
	// System.Collections.Generic.List`1<GoogleARCore.Trackable> GoogleARCoreInternal.TrackableManager::m_UpdatedTrackables
	List_1_t3686294978 * ___m_UpdatedTrackables_5;
	// System.Collections.Generic.HashSet`1<GoogleARCore.Trackable> GoogleARCoreInternal.TrackableManager::m_OldTrackables
	HashSet_1_t779169710 * ___m_OldTrackables_6;

public:
	inline static int32_t get_offset_of_m_TrackableDict_0() { return static_cast<int32_t>(offsetof(TrackableManager_t1722633017, ___m_TrackableDict_0)); }
	inline Dictionary_2_t436085955 * get_m_TrackableDict_0() const { return ___m_TrackableDict_0; }
	inline Dictionary_2_t436085955 ** get_address_of_m_TrackableDict_0() { return &___m_TrackableDict_0; }
	inline void set_m_TrackableDict_0(Dictionary_2_t436085955 * value)
	{
		___m_TrackableDict_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackableDict_0), value);
	}

	inline static int32_t get_offset_of_m_NativeSession_1() { return static_cast<int32_t>(offsetof(TrackableManager_t1722633017, ___m_NativeSession_1)); }
	inline NativeSession_t1550589577 * get_m_NativeSession_1() const { return ___m_NativeSession_1; }
	inline NativeSession_t1550589577 ** get_address_of_m_NativeSession_1() { return &___m_NativeSession_1; }
	inline void set_m_NativeSession_1(NativeSession_t1550589577 * value)
	{
		___m_NativeSession_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_NativeSession_1), value);
	}

	inline static int32_t get_offset_of_m_LastUpdateFrame_2() { return static_cast<int32_t>(offsetof(TrackableManager_t1722633017, ___m_LastUpdateFrame_2)); }
	inline int32_t get_m_LastUpdateFrame_2() const { return ___m_LastUpdateFrame_2; }
	inline int32_t* get_address_of_m_LastUpdateFrame_2() { return &___m_LastUpdateFrame_2; }
	inline void set_m_LastUpdateFrame_2(int32_t value)
	{
		___m_LastUpdateFrame_2 = value;
	}

	inline static int32_t get_offset_of_m_NewTrackables_3() { return static_cast<int32_t>(offsetof(TrackableManager_t1722633017, ___m_NewTrackables_3)); }
	inline List_1_t3686294978 * get_m_NewTrackables_3() const { return ___m_NewTrackables_3; }
	inline List_1_t3686294978 ** get_address_of_m_NewTrackables_3() { return &___m_NewTrackables_3; }
	inline void set_m_NewTrackables_3(List_1_t3686294978 * value)
	{
		___m_NewTrackables_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_NewTrackables_3), value);
	}

	inline static int32_t get_offset_of_m_AllTrackables_4() { return static_cast<int32_t>(offsetof(TrackableManager_t1722633017, ___m_AllTrackables_4)); }
	inline List_1_t3686294978 * get_m_AllTrackables_4() const { return ___m_AllTrackables_4; }
	inline List_1_t3686294978 ** get_address_of_m_AllTrackables_4() { return &___m_AllTrackables_4; }
	inline void set_m_AllTrackables_4(List_1_t3686294978 * value)
	{
		___m_AllTrackables_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_AllTrackables_4), value);
	}

	inline static int32_t get_offset_of_m_UpdatedTrackables_5() { return static_cast<int32_t>(offsetof(TrackableManager_t1722633017, ___m_UpdatedTrackables_5)); }
	inline List_1_t3686294978 * get_m_UpdatedTrackables_5() const { return ___m_UpdatedTrackables_5; }
	inline List_1_t3686294978 ** get_address_of_m_UpdatedTrackables_5() { return &___m_UpdatedTrackables_5; }
	inline void set_m_UpdatedTrackables_5(List_1_t3686294978 * value)
	{
		___m_UpdatedTrackables_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_UpdatedTrackables_5), value);
	}

	inline static int32_t get_offset_of_m_OldTrackables_6() { return static_cast<int32_t>(offsetof(TrackableManager_t1722633017, ___m_OldTrackables_6)); }
	inline HashSet_1_t779169710 * get_m_OldTrackables_6() const { return ___m_OldTrackables_6; }
	inline HashSet_1_t779169710 ** get_address_of_m_OldTrackables_6() { return &___m_OldTrackables_6; }
	inline void set_m_OldTrackables_6(HashSet_1_t779169710 * value)
	{
		___m_OldTrackables_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_OldTrackables_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEMANAGER_T1722633017_H
#ifndef EXPERIMENTBASE_T510909049_H
#define EXPERIMENTBASE_T510909049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ExperimentBase
struct  ExperimentBase_t510909049  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPERIMENTBASE_T510909049_H
#ifndef EXPERIMENTMANAGER_T29149108_H
#define EXPERIMENTMANAGER_T29149108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ExperimentManager
struct  ExperimentManager_t29149108  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<GoogleARCoreInternal.ExperimentBase> GoogleARCoreInternal.ExperimentManager::m_Experiments
	List_1_t1982983791 * ___m_Experiments_1;
	// System.Boolean GoogleARCoreInternal.ExperimentManager::<IsSessionExperimental>k__BackingField
	bool ___U3CIsSessionExperimentalU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_m_Experiments_1() { return static_cast<int32_t>(offsetof(ExperimentManager_t29149108, ___m_Experiments_1)); }
	inline List_1_t1982983791 * get_m_Experiments_1() const { return ___m_Experiments_1; }
	inline List_1_t1982983791 ** get_address_of_m_Experiments_1() { return &___m_Experiments_1; }
	inline void set_m_Experiments_1(List_1_t1982983791 * value)
	{
		___m_Experiments_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Experiments_1), value);
	}

	inline static int32_t get_offset_of_U3CIsSessionExperimentalU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ExperimentManager_t29149108, ___U3CIsSessionExperimentalU3Ek__BackingField_2)); }
	inline bool get_U3CIsSessionExperimentalU3Ek__BackingField_2() const { return ___U3CIsSessionExperimentalU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsSessionExperimentalU3Ek__BackingField_2() { return &___U3CIsSessionExperimentalU3Ek__BackingField_2; }
	inline void set_U3CIsSessionExperimentalU3Ek__BackingField_2(bool value)
	{
		___U3CIsSessionExperimentalU3Ek__BackingField_2 = value;
	}
};

struct ExperimentManager_t29149108_StaticFields
{
public:
	// GoogleARCoreInternal.ExperimentManager GoogleARCoreInternal.ExperimentManager::s_Instance
	ExperimentManager_t29149108 * ___s_Instance_0;
	// System.Func`2<System.Reflection.Assembly,System.Collections.Generic.IEnumerable`1<System.Type>> GoogleARCoreInternal.ExperimentManager::<>f__am$cache0
	Func_2_t779105388 * ___U3CU3Ef__amU24cache0_3;
	// System.Func`2<System.Type,System.Boolean> GoogleARCoreInternal.ExperimentManager::<>f__am$cache1
	Func_2_t561252955 * ___U3CU3Ef__amU24cache1_4;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(ExperimentManager_t29149108_StaticFields, ___s_Instance_0)); }
	inline ExperimentManager_t29149108 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline ExperimentManager_t29149108 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(ExperimentManager_t29149108 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(ExperimentManager_t29149108_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline Func_2_t779105388 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline Func_2_t779105388 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(Func_2_t779105388 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_4() { return static_cast<int32_t>(offsetof(ExperimentManager_t29149108_StaticFields, ___U3CU3Ef__amU24cache1_4)); }
	inline Func_2_t561252955 * get_U3CU3Ef__amU24cache1_4() const { return ___U3CU3Ef__amU24cache1_4; }
	inline Func_2_t561252955 ** get_address_of_U3CU3Ef__amU24cache1_4() { return &___U3CU3Ef__amU24cache1_4; }
	inline void set_U3CU3Ef__amU24cache1_4(Func_2_t561252955 * value)
	{
		___U3CU3Ef__amU24cache1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPERIMENTMANAGER_T29149108_H
#ifndef LIFECYCLEMANAGER_T2413967345_H
#define LIFECYCLEMANAGER_T2413967345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.LifecycleManager
struct  LifecycleManager_t2413967345  : public RuntimeObject
{
public:

public:
};

struct LifecycleManager_t2413967345_StaticFields
{
public:
	// GoogleARCoreInternal.ILifecycleManager GoogleARCoreInternal.LifecycleManager::s_Instance
	RuntimeObject* ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(LifecycleManager_t2413967345_StaticFields, ___s_Instance_0)); }
	inline RuntimeObject* get_s_Instance_0() const { return ___s_Instance_0; }
	inline RuntimeObject** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(RuntimeObject* value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIFECYCLEMANAGER_T2413967345_H
#ifndef FRAME_T2961216902_H
#define FRAME_T2961216902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Frame
struct  Frame_t2961216902  : public RuntimeObject
{
public:

public:
};

struct Frame_t2961216902_StaticFields
{
public:
	// System.Collections.Generic.List`1<GoogleARCore.TrackableHit> GoogleARCore.Frame::s_TmpTrackableHitList
	List_1_t3385152071 * ___s_TmpTrackableHitList_0;

public:
	inline static int32_t get_offset_of_s_TmpTrackableHitList_0() { return static_cast<int32_t>(offsetof(Frame_t2961216902_StaticFields, ___s_TmpTrackableHitList_0)); }
	inline List_1_t3385152071 * get_s_TmpTrackableHitList_0() const { return ___s_TmpTrackableHitList_0; }
	inline List_1_t3385152071 ** get_address_of_s_TmpTrackableHitList_0() { return &___s_TmpTrackableHitList_0; }
	inline void set_s_TmpTrackableHitList_0(List_1_t3385152071 * value)
	{
		___s_TmpTrackableHitList_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_TmpTrackableHitList_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAME_T2961216902_H
#ifndef CAMERAMETADATA_T1438220491_H
#define CAMERAMETADATA_T1438220491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Frame/CameraMetadata
struct  CameraMetadata_t1438220491  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAMETADATA_T1438220491_H
#ifndef POINTCLOUD_T1493100780_H
#define POINTCLOUD_T1493100780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Frame/PointCloud
struct  PointCloud_t1493100780  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTCLOUD_T1493100780_H
#ifndef CAMERAIMAGE_T3078682607_H
#define CAMERAIMAGE_T3078682607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Frame/CameraImage
struct  CameraImage_t3078682607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAIMAGE_T3078682607_H
#ifndef ARPRESTOCALLBACKMANAGER_T2442480432_H
#define ARPRESTOCALLBACKMANAGER_T2442480432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ARPrestoCallbackManager
struct  ARPrestoCallbackManager_t2442480432  : public RuntimeObject
{
public:
	// GoogleARCoreInternal.ARPrestoCallbackManager/CheckApkAvailabilityResultCallback GoogleARCoreInternal.ARPrestoCallbackManager::m_CheckApkAvailabilityResultCallback
	CheckApkAvailabilityResultCallback_t3176003102 * ___m_CheckApkAvailabilityResultCallback_1;
	// GoogleARCoreInternal.ARPrestoCallbackManager/RequestApkInstallationResultCallback GoogleARCoreInternal.ARPrestoCallbackManager::m_RequestApkInstallationResultCallback
	RequestApkInstallationResultCallback_t1625331025 * ___m_RequestApkInstallationResultCallback_2;
	// GoogleARCoreInternal.ARPrestoCallbackManager/CameraPermissionRequestProvider GoogleARCoreInternal.ARPrestoCallbackManager::m_RequestCameraPermissionCallback
	CameraPermissionRequestProvider_t2929024609 * ___m_RequestCameraPermissionCallback_3;
	// GoogleARCoreInternal.ARPrestoCallbackManager/EarlyUpdateCallback GoogleARCoreInternal.ARPrestoCallbackManager::m_EarlyUpdateCallback
	EarlyUpdateCallback_t1406590691 * ___m_EarlyUpdateCallback_4;
	// GoogleARCoreInternal.ARPrestoCallbackManager/OnBeforeSetConfigurationCallback GoogleARCoreInternal.ARPrestoCallbackManager::m_OnBeforeSetConfigurationCallback
	OnBeforeSetConfigurationCallback_t2829502455 * ___m_OnBeforeSetConfigurationCallback_5;
	// GoogleARCoreInternal.ARPrestoCallbackManager/OnBeforeResumeSessionCallback GoogleARCoreInternal.ARPrestoCallbackManager::m_OnBeforeResumeSessionCallback
	OnBeforeResumeSessionCallback_t1391226892 * ___m_OnBeforeResumeSessionCallback_6;
	// System.Collections.Generic.List`1<System.Action`1<GoogleARCore.ApkAvailabilityStatus>> GoogleARCoreInternal.ARPrestoCallbackManager::m_PendingAvailabilityCheckCallbacks
	List_1_t252299607 * ___m_PendingAvailabilityCheckCallbacks_7;
	// System.Collections.Generic.List`1<System.Action`1<GoogleARCore.ApkInstallationStatus>> GoogleARCoreInternal.ARPrestoCallbackManager::m_PendingInstallationRequestCallbacks
	List_1_t3947428544 * ___m_PendingInstallationRequestCallbacks_8;
	// System.Action GoogleARCoreInternal.ARPrestoCallbackManager::EarlyUpdate
	Action_t1264377477 * ___EarlyUpdate_9;
	// System.Action GoogleARCoreInternal.ARPrestoCallbackManager::BeforeResumeSession
	Action_t1264377477 * ___BeforeResumeSession_10;

public:
	inline static int32_t get_offset_of_m_CheckApkAvailabilityResultCallback_1() { return static_cast<int32_t>(offsetof(ARPrestoCallbackManager_t2442480432, ___m_CheckApkAvailabilityResultCallback_1)); }
	inline CheckApkAvailabilityResultCallback_t3176003102 * get_m_CheckApkAvailabilityResultCallback_1() const { return ___m_CheckApkAvailabilityResultCallback_1; }
	inline CheckApkAvailabilityResultCallback_t3176003102 ** get_address_of_m_CheckApkAvailabilityResultCallback_1() { return &___m_CheckApkAvailabilityResultCallback_1; }
	inline void set_m_CheckApkAvailabilityResultCallback_1(CheckApkAvailabilityResultCallback_t3176003102 * value)
	{
		___m_CheckApkAvailabilityResultCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_CheckApkAvailabilityResultCallback_1), value);
	}

	inline static int32_t get_offset_of_m_RequestApkInstallationResultCallback_2() { return static_cast<int32_t>(offsetof(ARPrestoCallbackManager_t2442480432, ___m_RequestApkInstallationResultCallback_2)); }
	inline RequestApkInstallationResultCallback_t1625331025 * get_m_RequestApkInstallationResultCallback_2() const { return ___m_RequestApkInstallationResultCallback_2; }
	inline RequestApkInstallationResultCallback_t1625331025 ** get_address_of_m_RequestApkInstallationResultCallback_2() { return &___m_RequestApkInstallationResultCallback_2; }
	inline void set_m_RequestApkInstallationResultCallback_2(RequestApkInstallationResultCallback_t1625331025 * value)
	{
		___m_RequestApkInstallationResultCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_RequestApkInstallationResultCallback_2), value);
	}

	inline static int32_t get_offset_of_m_RequestCameraPermissionCallback_3() { return static_cast<int32_t>(offsetof(ARPrestoCallbackManager_t2442480432, ___m_RequestCameraPermissionCallback_3)); }
	inline CameraPermissionRequestProvider_t2929024609 * get_m_RequestCameraPermissionCallback_3() const { return ___m_RequestCameraPermissionCallback_3; }
	inline CameraPermissionRequestProvider_t2929024609 ** get_address_of_m_RequestCameraPermissionCallback_3() { return &___m_RequestCameraPermissionCallback_3; }
	inline void set_m_RequestCameraPermissionCallback_3(CameraPermissionRequestProvider_t2929024609 * value)
	{
		___m_RequestCameraPermissionCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_RequestCameraPermissionCallback_3), value);
	}

	inline static int32_t get_offset_of_m_EarlyUpdateCallback_4() { return static_cast<int32_t>(offsetof(ARPrestoCallbackManager_t2442480432, ___m_EarlyUpdateCallback_4)); }
	inline EarlyUpdateCallback_t1406590691 * get_m_EarlyUpdateCallback_4() const { return ___m_EarlyUpdateCallback_4; }
	inline EarlyUpdateCallback_t1406590691 ** get_address_of_m_EarlyUpdateCallback_4() { return &___m_EarlyUpdateCallback_4; }
	inline void set_m_EarlyUpdateCallback_4(EarlyUpdateCallback_t1406590691 * value)
	{
		___m_EarlyUpdateCallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EarlyUpdateCallback_4), value);
	}

	inline static int32_t get_offset_of_m_OnBeforeSetConfigurationCallback_5() { return static_cast<int32_t>(offsetof(ARPrestoCallbackManager_t2442480432, ___m_OnBeforeSetConfigurationCallback_5)); }
	inline OnBeforeSetConfigurationCallback_t2829502455 * get_m_OnBeforeSetConfigurationCallback_5() const { return ___m_OnBeforeSetConfigurationCallback_5; }
	inline OnBeforeSetConfigurationCallback_t2829502455 ** get_address_of_m_OnBeforeSetConfigurationCallback_5() { return &___m_OnBeforeSetConfigurationCallback_5; }
	inline void set_m_OnBeforeSetConfigurationCallback_5(OnBeforeSetConfigurationCallback_t2829502455 * value)
	{
		___m_OnBeforeSetConfigurationCallback_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnBeforeSetConfigurationCallback_5), value);
	}

	inline static int32_t get_offset_of_m_OnBeforeResumeSessionCallback_6() { return static_cast<int32_t>(offsetof(ARPrestoCallbackManager_t2442480432, ___m_OnBeforeResumeSessionCallback_6)); }
	inline OnBeforeResumeSessionCallback_t1391226892 * get_m_OnBeforeResumeSessionCallback_6() const { return ___m_OnBeforeResumeSessionCallback_6; }
	inline OnBeforeResumeSessionCallback_t1391226892 ** get_address_of_m_OnBeforeResumeSessionCallback_6() { return &___m_OnBeforeResumeSessionCallback_6; }
	inline void set_m_OnBeforeResumeSessionCallback_6(OnBeforeResumeSessionCallback_t1391226892 * value)
	{
		___m_OnBeforeResumeSessionCallback_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnBeforeResumeSessionCallback_6), value);
	}

	inline static int32_t get_offset_of_m_PendingAvailabilityCheckCallbacks_7() { return static_cast<int32_t>(offsetof(ARPrestoCallbackManager_t2442480432, ___m_PendingAvailabilityCheckCallbacks_7)); }
	inline List_1_t252299607 * get_m_PendingAvailabilityCheckCallbacks_7() const { return ___m_PendingAvailabilityCheckCallbacks_7; }
	inline List_1_t252299607 ** get_address_of_m_PendingAvailabilityCheckCallbacks_7() { return &___m_PendingAvailabilityCheckCallbacks_7; }
	inline void set_m_PendingAvailabilityCheckCallbacks_7(List_1_t252299607 * value)
	{
		___m_PendingAvailabilityCheckCallbacks_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_PendingAvailabilityCheckCallbacks_7), value);
	}

	inline static int32_t get_offset_of_m_PendingInstallationRequestCallbacks_8() { return static_cast<int32_t>(offsetof(ARPrestoCallbackManager_t2442480432, ___m_PendingInstallationRequestCallbacks_8)); }
	inline List_1_t3947428544 * get_m_PendingInstallationRequestCallbacks_8() const { return ___m_PendingInstallationRequestCallbacks_8; }
	inline List_1_t3947428544 ** get_address_of_m_PendingInstallationRequestCallbacks_8() { return &___m_PendingInstallationRequestCallbacks_8; }
	inline void set_m_PendingInstallationRequestCallbacks_8(List_1_t3947428544 * value)
	{
		___m_PendingInstallationRequestCallbacks_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_PendingInstallationRequestCallbacks_8), value);
	}

	inline static int32_t get_offset_of_EarlyUpdate_9() { return static_cast<int32_t>(offsetof(ARPrestoCallbackManager_t2442480432, ___EarlyUpdate_9)); }
	inline Action_t1264377477 * get_EarlyUpdate_9() const { return ___EarlyUpdate_9; }
	inline Action_t1264377477 ** get_address_of_EarlyUpdate_9() { return &___EarlyUpdate_9; }
	inline void set_EarlyUpdate_9(Action_t1264377477 * value)
	{
		___EarlyUpdate_9 = value;
		Il2CppCodeGenWriteBarrier((&___EarlyUpdate_9), value);
	}

	inline static int32_t get_offset_of_BeforeResumeSession_10() { return static_cast<int32_t>(offsetof(ARPrestoCallbackManager_t2442480432, ___BeforeResumeSession_10)); }
	inline Action_t1264377477 * get_BeforeResumeSession_10() const { return ___BeforeResumeSession_10; }
	inline Action_t1264377477 ** get_address_of_BeforeResumeSession_10() { return &___BeforeResumeSession_10; }
	inline void set_BeforeResumeSession_10(Action_t1264377477 * value)
	{
		___BeforeResumeSession_10 = value;
		Il2CppCodeGenWriteBarrier((&___BeforeResumeSession_10), value);
	}
};

struct ARPrestoCallbackManager_t2442480432_StaticFields
{
public:
	// GoogleARCoreInternal.ARPrestoCallbackManager GoogleARCoreInternal.ARPrestoCallbackManager::s_Instance
	ARPrestoCallbackManager_t2442480432 * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(ARPrestoCallbackManager_t2442480432_StaticFields, ___s_Instance_0)); }
	inline ARPrestoCallbackManager_t2442480432 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline ARPrestoCallbackManager_t2442480432 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(ARPrestoCallbackManager_t2442480432 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARPRESTOCALLBACKMANAGER_T2442480432_H
#ifndef XPSESSION_T3719927413_H
#define XPSESSION_T3719927413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.CrossPlatform.XPSession
struct  XPSession_t3719927413  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPSESSION_T3719927413_H
#ifndef U3CRUNCOMMANDU3EC__ANONSTOREY0_T2963713103_H
#define U3CRUNCOMMANDU3EC__ANONSTOREY0_T2963713103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ShellHelper/<RunCommand>c__AnonStorey0
struct  U3CRunCommandU3Ec__AnonStorey0_t2963713103  : public RuntimeObject
{
public:
	// System.Text.StringBuilder GoogleARCoreInternal.ShellHelper/<RunCommand>c__AnonStorey0::outputBuilder
	StringBuilder_t * ___outputBuilder_0;
	// System.Text.StringBuilder GoogleARCoreInternal.ShellHelper/<RunCommand>c__AnonStorey0::errorBuilder
	StringBuilder_t * ___errorBuilder_1;

public:
	inline static int32_t get_offset_of_outputBuilder_0() { return static_cast<int32_t>(offsetof(U3CRunCommandU3Ec__AnonStorey0_t2963713103, ___outputBuilder_0)); }
	inline StringBuilder_t * get_outputBuilder_0() const { return ___outputBuilder_0; }
	inline StringBuilder_t ** get_address_of_outputBuilder_0() { return &___outputBuilder_0; }
	inline void set_outputBuilder_0(StringBuilder_t * value)
	{
		___outputBuilder_0 = value;
		Il2CppCodeGenWriteBarrier((&___outputBuilder_0), value);
	}

	inline static int32_t get_offset_of_errorBuilder_1() { return static_cast<int32_t>(offsetof(U3CRunCommandU3Ec__AnonStorey0_t2963713103, ___errorBuilder_1)); }
	inline StringBuilder_t * get_errorBuilder_1() const { return ___errorBuilder_1; }
	inline StringBuilder_t ** get_address_of_errorBuilder_1() { return &___errorBuilder_1; }
	inline void set_errorBuilder_1(StringBuilder_t * value)
	{
		___errorBuilder_1 = value;
		Il2CppCodeGenWriteBarrier((&___errorBuilder_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNCOMMANDU3EC__ANONSTOREY0_T2963713103_H
#ifndef U3CREMOVEANGLERECOGNIZEDEVENTSU3EC__ANONSTOREY0_T2854203665_H
#define U3CREMOVEANGLERECOGNIZEDEVENTSU3EC__ANONSTOREY0_T2854203665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TKAngleSwipeRecognizer/<removeAngleRecognizedEvents>c__AnonStorey0
struct  U3CremoveAngleRecognizedEventsU3Ec__AnonStorey0_t2854203665  : public RuntimeObject
{
public:
	// System.Action`1<TKAngleSwipeRecognizer> TKAngleSwipeRecognizer/<removeAngleRecognizedEvents>c__AnonStorey0::action
	Action_1_t3839277408 * ___action_0;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CremoveAngleRecognizedEventsU3Ec__AnonStorey0_t2854203665, ___action_0)); }
	inline Action_1_t3839277408 * get_action_0() const { return ___action_0; }
	inline Action_1_t3839277408 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_1_t3839277408 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVEANGLERECOGNIZEDEVENTSU3EC__ANONSTOREY0_T2854203665_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ASYNCTASK_T419677264_H
#define ASYNCTASK_T419677264_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.AsyncTask
struct  AsyncTask_t419677264  : public RuntimeObject
{
public:

public:
};

struct AsyncTask_t419677264_StaticFields
{
public:
	// System.Collections.Generic.Queue`1<System.Action> GoogleARCore.AsyncTask::s_UpdateActionQueue
	Queue_1_t1110636971 * ___s_UpdateActionQueue_0;
	// System.Object GoogleARCore.AsyncTask::s_LockObject
	RuntimeObject * ___s_LockObject_1;

public:
	inline static int32_t get_offset_of_s_UpdateActionQueue_0() { return static_cast<int32_t>(offsetof(AsyncTask_t419677264_StaticFields, ___s_UpdateActionQueue_0)); }
	inline Queue_1_t1110636971 * get_s_UpdateActionQueue_0() const { return ___s_UpdateActionQueue_0; }
	inline Queue_1_t1110636971 ** get_address_of_s_UpdateActionQueue_0() { return &___s_UpdateActionQueue_0; }
	inline void set_s_UpdateActionQueue_0(Queue_1_t1110636971 * value)
	{
		___s_UpdateActionQueue_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_UpdateActionQueue_0), value);
	}

	inline static int32_t get_offset_of_s_LockObject_1() { return static_cast<int32_t>(offsetof(AsyncTask_t419677264_StaticFields, ___s_LockObject_1)); }
	inline RuntimeObject * get_s_LockObject_1() const { return ___s_LockObject_1; }
	inline RuntimeObject ** get_address_of_s_LockObject_1() { return &___s_LockObject_1; }
	inline void set_s_LockObject_1(RuntimeObject * value)
	{
		___s_LockObject_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_LockObject_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCTASK_T419677264_H
#ifndef POSEAPI_T3025584507_H
#define POSEAPI_T3025584507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.PoseApi
struct  PoseApi_t3025584507  : public RuntimeObject
{
public:
	// GoogleARCoreInternal.NativeSession GoogleARCoreInternal.PoseApi::m_NativeSession
	NativeSession_t1550589577 * ___m_NativeSession_0;

public:
	inline static int32_t get_offset_of_m_NativeSession_0() { return static_cast<int32_t>(offsetof(PoseApi_t3025584507, ___m_NativeSession_0)); }
	inline NativeSession_t1550589577 * get_m_NativeSession_0() const { return ___m_NativeSession_0; }
	inline NativeSession_t1550589577 ** get_address_of_m_NativeSession_0() { return &___m_NativeSession_0; }
	inline void set_m_NativeSession_0(NativeSession_t1550589577 * value)
	{
		___m_NativeSession_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_NativeSession_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSEAPI_T3025584507_H
#ifndef SESSIONAPI_T3641277092_H
#define SESSIONAPI_T3641277092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.SessionApi
struct  SessionApi_t3641277092  : public RuntimeObject
{
public:
	// GoogleARCoreInternal.NativeSession GoogleARCoreInternal.SessionApi::m_NativeSession
	NativeSession_t1550589577 * ___m_NativeSession_0;

public:
	inline static int32_t get_offset_of_m_NativeSession_0() { return static_cast<int32_t>(offsetof(SessionApi_t3641277092, ___m_NativeSession_0)); }
	inline NativeSession_t1550589577 * get_m_NativeSession_0() const { return ___m_NativeSession_0; }
	inline NativeSession_t1550589577 ** get_address_of_m_NativeSession_0() { return &___m_NativeSession_0; }
	inline void set_m_NativeSession_0(NativeSession_t1550589577 * value)
	{
		___m_NativeSession_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_NativeSession_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONAPI_T3641277092_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef TRACKABLELISTAPI_T1931239134_H
#define TRACKABLELISTAPI_T1931239134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.TrackableListApi
struct  TrackableListApi_t1931239134  : public RuntimeObject
{
public:
	// GoogleARCoreInternal.NativeSession GoogleARCoreInternal.TrackableListApi::m_NativeSession
	NativeSession_t1550589577 * ___m_NativeSession_0;

public:
	inline static int32_t get_offset_of_m_NativeSession_0() { return static_cast<int32_t>(offsetof(TrackableListApi_t1931239134, ___m_NativeSession_0)); }
	inline NativeSession_t1550589577 * get_m_NativeSession_0() const { return ___m_NativeSession_0; }
	inline NativeSession_t1550589577 ** get_address_of_m_NativeSession_0() { return &___m_NativeSession_0; }
	inline void set_m_NativeSession_0(NativeSession_t1550589577 * value)
	{
		___m_NativeSession_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_NativeSession_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLELISTAPI_T1931239134_H
#ifndef IOSCAMERAPERMISSION_T2172579563_H
#define IOSCAMERAPERMISSION_T2172579563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iOSCameraPermission
struct  iOSCameraPermission_t2172579563  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSCAMERAPERMISSION_T2172579563_H
#ifndef TRACKABLEAPI_T1100424269_H
#define TRACKABLEAPI_T1100424269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.TrackableApi
struct  TrackableApi_t1100424269  : public RuntimeObject
{
public:
	// GoogleARCoreInternal.NativeSession GoogleARCoreInternal.TrackableApi::m_NativeSession
	NativeSession_t1550589577 * ___m_NativeSession_0;

public:
	inline static int32_t get_offset_of_m_NativeSession_0() { return static_cast<int32_t>(offsetof(TrackableApi_t1100424269, ___m_NativeSession_0)); }
	inline NativeSession_t1550589577 * get_m_NativeSession_0() const { return ___m_NativeSession_0; }
	inline NativeSession_t1550589577 ** get_address_of_m_NativeSession_0() { return &___m_NativeSession_0; }
	inline void set_m_NativeSession_0(NativeSession_t1550589577 * value)
	{
		___m_NativeSession_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_NativeSession_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEAPI_T1100424269_H
#ifndef SESSIONCONFIGAPI_T3119440216_H
#define SESSIONCONFIGAPI_T3119440216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.SessionConfigApi
struct  SessionConfigApi_t3119440216  : public RuntimeObject
{
public:
	// GoogleARCoreInternal.NativeSession GoogleARCoreInternal.SessionConfigApi::m_NativeSession
	NativeSession_t1550589577 * ___m_NativeSession_0;

public:
	inline static int32_t get_offset_of_m_NativeSession_0() { return static_cast<int32_t>(offsetof(SessionConfigApi_t3119440216, ___m_NativeSession_0)); }
	inline NativeSession_t1550589577 * get_m_NativeSession_0() const { return ___m_NativeSession_0; }
	inline NativeSession_t1550589577 ** get_address_of_m_NativeSession_0() { return &___m_NativeSession_0; }
	inline void set_m_NativeSession_0(NativeSession_t1550589577 * value)
	{
		___m_NativeSession_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_NativeSession_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONCONFIGAPI_T3119440216_H
#ifndef TKRECT_T2880547869_H
#define TKRECT_T2880547869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TKRect
struct  TKRect_t2880547869 
{
public:
	// System.Single TKRect::x
	float ___x_0;
	// System.Single TKRect::y
	float ___y_1;
	// System.Single TKRect::width
	float ___width_2;
	// System.Single TKRect::height
	float ___height_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(TKRect_t2880547869, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(TKRect_t2880547869, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_width_2() { return static_cast<int32_t>(offsetof(TKRect_t2880547869, ___width_2)); }
	inline float get_width_2() const { return ___width_2; }
	inline float* get_address_of_width_2() { return &___width_2; }
	inline void set_width_2(float value)
	{
		___width_2 = value;
	}

	inline static int32_t get_offset_of_height_3() { return static_cast<int32_t>(offsetof(TKRect_t2880547869, ___height_3)); }
	inline float get_height_3() const { return ___height_3; }
	inline float* get_address_of_height_3() { return &___height_3; }
	inline void set_height_3(float value)
	{
		___height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TKRECT_T2880547869_H
#ifndef EXTERNAPI_T1655458871_H
#define EXTERNAPI_T1655458871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ARPrestoCallbackManager/ExternApi
struct  ExternApi_t1655458871 
{
public:
	union
	{
		struct
		{
		};
		uint8_t ExternApi_t1655458871__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNAPI_T1655458871_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef EXTERNAPI_T1122552781_H
#define EXTERNAPI_T1122552781_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.CrossPlatform.CloudServiceManager/ExternApi
struct  ExternApi_t1122552781 
{
public:
	union
	{
		struct
		{
		};
		uint8_t ExternApi_t1122552781__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNAPI_T1122552781_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef POINTCLOUDPOINT_T4057930723_H
#define POINTCLOUDPOINT_T4057930723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.PointCloudPoint
struct  PointCloudPoint_t4057930723 
{
public:
	// System.Single GoogleARCore.PointCloudPoint::X
	float ___X_0;
	// System.Single GoogleARCore.PointCloudPoint::Y
	float ___Y_1;
	// System.Single GoogleARCore.PointCloudPoint::Z
	float ___Z_2;
	// System.Single GoogleARCore.PointCloudPoint::Confidence
	float ___Confidence_3;

public:
	inline static int32_t get_offset_of_X_0() { return static_cast<int32_t>(offsetof(PointCloudPoint_t4057930723, ___X_0)); }
	inline float get_X_0() const { return ___X_0; }
	inline float* get_address_of_X_0() { return &___X_0; }
	inline void set_X_0(float value)
	{
		___X_0 = value;
	}

	inline static int32_t get_offset_of_Y_1() { return static_cast<int32_t>(offsetof(PointCloudPoint_t4057930723, ___Y_1)); }
	inline float get_Y_1() const { return ___Y_1; }
	inline float* get_address_of_Y_1() { return &___Y_1; }
	inline void set_Y_1(float value)
	{
		___Y_1 = value;
	}

	inline static int32_t get_offset_of_Z_2() { return static_cast<int32_t>(offsetof(PointCloudPoint_t4057930723, ___Z_2)); }
	inline float get_Z_2() const { return ___Z_2; }
	inline float* get_address_of_Z_2() { return &___Z_2; }
	inline void set_Z_2(float value)
	{
		___Z_2 = value;
	}

	inline static int32_t get_offset_of_Confidence_3() { return static_cast<int32_t>(offsetof(PointCloudPoint_t4057930723, ___Confidence_3)); }
	inline float get_Confidence_3() const { return ___Confidence_3; }
	inline float* get_address_of_Confidence_3() { return &___Confidence_3; }
	inline void set_Confidence_3(float value)
	{
		___Confidence_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTCLOUDPOINT_T4057930723_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef VECTOR2INT_T3469998543_H
#define VECTOR2INT_T3469998543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2Int
struct  Vector2Int_t3469998543 
{
public:
	// System.Int32 UnityEngine.Vector2Int::m_X
	int32_t ___m_X_0;
	// System.Int32 UnityEngine.Vector2Int::m_Y
	int32_t ___m_Y_1;

public:
	inline static int32_t get_offset_of_m_X_0() { return static_cast<int32_t>(offsetof(Vector2Int_t3469998543, ___m_X_0)); }
	inline int32_t get_m_X_0() const { return ___m_X_0; }
	inline int32_t* get_address_of_m_X_0() { return &___m_X_0; }
	inline void set_m_X_0(int32_t value)
	{
		___m_X_0 = value;
	}

	inline static int32_t get_offset_of_m_Y_1() { return static_cast<int32_t>(offsetof(Vector2Int_t3469998543, ___m_Y_1)); }
	inline int32_t get_m_Y_1() const { return ___m_Y_1; }
	inline int32_t* get_address_of_m_Y_1() { return &___m_Y_1; }
	inline void set_m_Y_1(int32_t value)
	{
		___m_Y_1 = value;
	}
};

struct Vector2Int_t3469998543_StaticFields
{
public:
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Zero
	Vector2Int_t3469998543  ___s_Zero_2;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_One
	Vector2Int_t3469998543  ___s_One_3;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Up
	Vector2Int_t3469998543  ___s_Up_4;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Down
	Vector2Int_t3469998543  ___s_Down_5;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Left
	Vector2Int_t3469998543  ___s_Left_6;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Right
	Vector2Int_t3469998543  ___s_Right_7;

public:
	inline static int32_t get_offset_of_s_Zero_2() { return static_cast<int32_t>(offsetof(Vector2Int_t3469998543_StaticFields, ___s_Zero_2)); }
	inline Vector2Int_t3469998543  get_s_Zero_2() const { return ___s_Zero_2; }
	inline Vector2Int_t3469998543 * get_address_of_s_Zero_2() { return &___s_Zero_2; }
	inline void set_s_Zero_2(Vector2Int_t3469998543  value)
	{
		___s_Zero_2 = value;
	}

	inline static int32_t get_offset_of_s_One_3() { return static_cast<int32_t>(offsetof(Vector2Int_t3469998543_StaticFields, ___s_One_3)); }
	inline Vector2Int_t3469998543  get_s_One_3() const { return ___s_One_3; }
	inline Vector2Int_t3469998543 * get_address_of_s_One_3() { return &___s_One_3; }
	inline void set_s_One_3(Vector2Int_t3469998543  value)
	{
		___s_One_3 = value;
	}

	inline static int32_t get_offset_of_s_Up_4() { return static_cast<int32_t>(offsetof(Vector2Int_t3469998543_StaticFields, ___s_Up_4)); }
	inline Vector2Int_t3469998543  get_s_Up_4() const { return ___s_Up_4; }
	inline Vector2Int_t3469998543 * get_address_of_s_Up_4() { return &___s_Up_4; }
	inline void set_s_Up_4(Vector2Int_t3469998543  value)
	{
		___s_Up_4 = value;
	}

	inline static int32_t get_offset_of_s_Down_5() { return static_cast<int32_t>(offsetof(Vector2Int_t3469998543_StaticFields, ___s_Down_5)); }
	inline Vector2Int_t3469998543  get_s_Down_5() const { return ___s_Down_5; }
	inline Vector2Int_t3469998543 * get_address_of_s_Down_5() { return &___s_Down_5; }
	inline void set_s_Down_5(Vector2Int_t3469998543  value)
	{
		___s_Down_5 = value;
	}

	inline static int32_t get_offset_of_s_Left_6() { return static_cast<int32_t>(offsetof(Vector2Int_t3469998543_StaticFields, ___s_Left_6)); }
	inline Vector2Int_t3469998543  get_s_Left_6() const { return ___s_Left_6; }
	inline Vector2Int_t3469998543 * get_address_of_s_Left_6() { return &___s_Left_6; }
	inline void set_s_Left_6(Vector2Int_t3469998543  value)
	{
		___s_Left_6 = value;
	}

	inline static int32_t get_offset_of_s_Right_7() { return static_cast<int32_t>(offsetof(Vector2Int_t3469998543_StaticFields, ___s_Right_7)); }
	inline Vector2Int_t3469998543  get_s_Right_7() const { return ___s_Right_7; }
	inline Vector2Int_t3469998543 * get_address_of_s_Right_7() { return &___s_Right_7; }
	inline void set_s_Right_7(Vector2Int_t3469998543  value)
	{
		___s_Right_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2INT_T3469998543_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef DLLIMPORTNOOP_T2428006201_H
#define DLLIMPORTNOOP_T2428006201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.DllImportNoop
struct  DllImportNoop_t2428006201  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DLLIMPORTNOOP_T2428006201_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef EXTERNAPI_T3785444511_H
#define EXTERNAPI_T3785444511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ARCoreIOSLifecycleManager/ExternApi
struct  ExternApi_t3785444511 
{
public:
	union
	{
		struct
		{
		};
		uint8_t ExternApi_t3785444511__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNAPI_T3785444511_H
#ifndef EXTERNAPI_T4200038123_H
#define EXTERNAPI_T4200038123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.PointCloudApi/ExternApi
struct  ExternApi_t4200038123 
{
public:
	union
	{
		struct
		{
		};
		uint8_t ExternApi_t4200038123__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNAPI_T4200038123_H
#ifndef AUGMENTEDIMAGEDATABASEENTRY_T3470343716_H
#define AUGMENTEDIMAGEDATABASEENTRY_T3470343716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.AugmentedImageDatabaseEntry
struct  AugmentedImageDatabaseEntry_t3470343716 
{
public:
	// System.String GoogleARCore.AugmentedImageDatabaseEntry::Name
	String_t* ___Name_0;
	// System.Single GoogleARCore.AugmentedImageDatabaseEntry::Width
	float ___Width_1;
	// System.String GoogleARCore.AugmentedImageDatabaseEntry::Quality
	String_t* ___Quality_2;
	// System.String GoogleARCore.AugmentedImageDatabaseEntry::TextureGUID
	String_t* ___TextureGUID_3;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(AugmentedImageDatabaseEntry_t3470343716, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Width_1() { return static_cast<int32_t>(offsetof(AugmentedImageDatabaseEntry_t3470343716, ___Width_1)); }
	inline float get_Width_1() const { return ___Width_1; }
	inline float* get_address_of_Width_1() { return &___Width_1; }
	inline void set_Width_1(float value)
	{
		___Width_1 = value;
	}

	inline static int32_t get_offset_of_Quality_2() { return static_cast<int32_t>(offsetof(AugmentedImageDatabaseEntry_t3470343716, ___Quality_2)); }
	inline String_t* get_Quality_2() const { return ___Quality_2; }
	inline String_t** get_address_of_Quality_2() { return &___Quality_2; }
	inline void set_Quality_2(String_t* value)
	{
		___Quality_2 = value;
		Il2CppCodeGenWriteBarrier((&___Quality_2), value);
	}

	inline static int32_t get_offset_of_TextureGUID_3() { return static_cast<int32_t>(offsetof(AugmentedImageDatabaseEntry_t3470343716, ___TextureGUID_3)); }
	inline String_t* get_TextureGUID_3() const { return ___TextureGUID_3; }
	inline String_t** get_address_of_TextureGUID_3() { return &___TextureGUID_3; }
	inline void set_TextureGUID_3(String_t* value)
	{
		___TextureGUID_3 = value;
		Il2CppCodeGenWriteBarrier((&___TextureGUID_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GoogleARCore.AugmentedImageDatabaseEntry
struct AugmentedImageDatabaseEntry_t3470343716_marshaled_pinvoke
{
	char* ___Name_0;
	float ___Width_1;
	char* ___Quality_2;
	char* ___TextureGUID_3;
};
// Native definition for COM marshalling of GoogleARCore.AugmentedImageDatabaseEntry
struct AugmentedImageDatabaseEntry_t3470343716_marshaled_com
{
	Il2CppChar* ___Name_0;
	float ___Width_1;
	Il2CppChar* ___Quality_2;
	Il2CppChar* ___TextureGUID_3;
};
#endif // AUGMENTEDIMAGEDATABASEENTRY_T3470343716_H
#ifndef EXTERNAPI_T3853040500_H
#define EXTERNAPI_T3853040500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.PoseApi/ExternApi
struct  ExternApi_t3853040500 
{
public:
	union
	{
		struct
		{
		};
		uint8_t ExternApi_t3853040500__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNAPI_T3853040500_H
#ifndef EXTERNAPI_T713910015_H
#define EXTERNAPI_T713910015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ARCoreAndroidLifecycleManager/ExternApi
struct  ExternApi_t713910015 
{
public:
	union
	{
		struct
		{
		};
		uint8_t ExternApi_t713910015__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNAPI_T713910015_H
#ifndef EXTERNAPI_T4031421663_H
#define EXTERNAPI_T4031421663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.TrackableListApi/ExternApi
struct  ExternApi_t4031421663 
{
public:
	union
	{
		struct
		{
		};
		uint8_t ExternApi_t4031421663__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNAPI_T4031421663_H
#ifndef EXTERNAPI_T574091193_H
#define EXTERNAPI_T574091193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.TrackableApi/ExternApi
struct  ExternApi_t574091193 
{
public:
	union
	{
		struct
		{
		};
		uint8_t ExternApi_t574091193__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNAPI_T574091193_H
#ifndef EXTERNAPI_T1767614075_H
#define EXTERNAPI_T1767614075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.SessionApi/ExternApi
struct  ExternApi_t1767614075 
{
public:
	union
	{
		struct
		{
		};
		uint8_t ExternApi_t1767614075__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNAPI_T1767614075_H
#ifndef CAMERAMETADATARATIONAL_T1052259197_H
#define CAMERAMETADATARATIONAL_T1052259197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.CameraMetadataRational
struct  CameraMetadataRational_t1052259197 
{
public:
	// System.Int32 GoogleARCore.CameraMetadataRational::Numerator
	int32_t ___Numerator_0;
	// System.Int32 GoogleARCore.CameraMetadataRational::Denominator
	int32_t ___Denominator_1;

public:
	inline static int32_t get_offset_of_Numerator_0() { return static_cast<int32_t>(offsetof(CameraMetadataRational_t1052259197, ___Numerator_0)); }
	inline int32_t get_Numerator_0() const { return ___Numerator_0; }
	inline int32_t* get_address_of_Numerator_0() { return &___Numerator_0; }
	inline void set_Numerator_0(int32_t value)
	{
		___Numerator_0 = value;
	}

	inline static int32_t get_offset_of_Denominator_1() { return static_cast<int32_t>(offsetof(CameraMetadataRational_t1052259197, ___Denominator_1)); }
	inline int32_t get_Denominator_1() const { return ___Denominator_1; }
	inline int32_t* get_address_of_Denominator_1() { return &___Denominator_1; }
	inline void set_Denominator_1(int32_t value)
	{
		___Denominator_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAMETADATARATIONAL_T1052259197_H
#ifndef EXTERNAPI_T1764421814_H
#define EXTERNAPI_T1764421814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.SessionConfigApi/ExternApi
struct  ExternApi_t1764421814 
{
public:
	union
	{
		struct
		{
		};
		uint8_t ExternApi_t1764421814__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNAPI_T1764421814_H
#ifndef TRACKABLEHITFLAGS_T1358952518_H
#define TRACKABLEHITFLAGS_T1358952518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.TrackableHitFlags
struct  TrackableHitFlags_t1358952518 
{
public:
	// System.Int32 GoogleARCore.TrackableHitFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TrackableHitFlags_t1358952518, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEHITFLAGS_T1358952518_H
#ifndef TRACKABLEQUERYFILTER_T2799753840_H
#define TRACKABLEQUERYFILTER_T2799753840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.TrackableQueryFilter
struct  TrackableQueryFilter_t2799753840 
{
public:
	// System.Int32 GoogleARCore.TrackableQueryFilter::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TrackableQueryFilter_t2799753840, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEQUERYFILTER_T2799753840_H
#ifndef TRACKINGSTATE_T1099935182_H
#define TRACKINGSTATE_T1099935182_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.TrackingState
struct  TrackingState_t1099935182 
{
public:
	// System.Int32 GoogleARCore.TrackingState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TrackingState_t1099935182, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKINGSTATE_T1099935182_H
#ifndef TRACKEDPOINTORIENTATIONMODE_T3853242691_H
#define TRACKEDPOINTORIENTATIONMODE_T3853242691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.TrackedPointOrientationMode
struct  TrackedPointOrientationMode_t3853242691 
{
public:
	// System.Int32 GoogleARCore.TrackedPointOrientationMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TrackedPointOrientationMode_t3853242691, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKEDPOINTORIENTATIONMODE_T3853242691_H
#ifndef CLOUDSERVICERESPONSE_T2141656258_H
#define CLOUDSERVICERESPONSE_T2141656258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.CrossPlatform.CloudServiceResponse
struct  CloudServiceResponse_t2141656258 
{
public:
	// System.Int32 GoogleARCore.CrossPlatform.CloudServiceResponse::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CloudServiceResponse_t2141656258, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDSERVICERESPONSE_T2141656258_H
#ifndef APKINSTALLATIONSTATUS_T2302886207_H
#define APKINSTALLATIONSTATUS_T2302886207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.ApkInstallationStatus
struct  ApkInstallationStatus_t2302886207 
{
public:
	// System.Int32 GoogleARCore.ApkInstallationStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ApkInstallationStatus_t2302886207, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APKINSTALLATIONSTATUS_T2302886207_H
#ifndef NDKCAMERAMETADATATYPE_T306478796_H
#define NDKCAMERAMETADATATYPE_T306478796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.NdkCameraMetadataType
struct  NdkCameraMetadataType_t306478796 
{
public:
	// System.Int32 GoogleARCoreInternal.NdkCameraMetadataType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NdkCameraMetadataType_t306478796, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NDKCAMERAMETADATATYPE_T306478796_H
#ifndef CAMERAMETADATATAG_T2983628881_H
#define CAMERAMETADATATAG_T2983628881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.CameraMetadataTag
struct  CameraMetadataTag_t2983628881 
{
public:
	// System.Int32 GoogleARCore.CameraMetadataTag::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraMetadataTag_t2983628881, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAMETADATATAG_T2983628881_H
#ifndef ANGLELISTENER_T4071059023_H
#define ANGLELISTENER_T4071059023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TKAngleSwipeRecognizer/AngleListener
struct  AngleListener_t4071059023 
{
public:
	// System.Single TKAngleSwipeRecognizer/AngleListener::Varience
	float ___Varience_0;
	// UnityEngine.Vector2 TKAngleSwipeRecognizer/AngleListener::Direction
	Vector2_t2156229523  ___Direction_1;
	// System.Action`1<TKAngleSwipeRecognizer> TKAngleSwipeRecognizer/AngleListener::Action
	Action_1_t3839277408 * ___Action_2;

public:
	inline static int32_t get_offset_of_Varience_0() { return static_cast<int32_t>(offsetof(AngleListener_t4071059023, ___Varience_0)); }
	inline float get_Varience_0() const { return ___Varience_0; }
	inline float* get_address_of_Varience_0() { return &___Varience_0; }
	inline void set_Varience_0(float value)
	{
		___Varience_0 = value;
	}

	inline static int32_t get_offset_of_Direction_1() { return static_cast<int32_t>(offsetof(AngleListener_t4071059023, ___Direction_1)); }
	inline Vector2_t2156229523  get_Direction_1() const { return ___Direction_1; }
	inline Vector2_t2156229523 * get_address_of_Direction_1() { return &___Direction_1; }
	inline void set_Direction_1(Vector2_t2156229523  value)
	{
		___Direction_1 = value;
	}

	inline static int32_t get_offset_of_Action_2() { return static_cast<int32_t>(offsetof(AngleListener_t4071059023, ___Action_2)); }
	inline Action_1_t3839277408 * get_Action_2() const { return ___Action_2; }
	inline Action_1_t3839277408 ** get_address_of_Action_2() { return &___Action_2; }
	inline void set_Action_2(Action_1_t3839277408 * value)
	{
		___Action_2 = value;
		Il2CppCodeGenWriteBarrier((&___Action_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TKAngleSwipeRecognizer/AngleListener
struct AngleListener_t4071059023_marshaled_pinvoke
{
	float ___Varience_0;
	Vector2_t2156229523  ___Direction_1;
	Il2CppMethodPointer ___Action_2;
};
// Native definition for COM marshalling of TKAngleSwipeRecognizer/AngleListener
struct AngleListener_t4071059023_marshaled_com
{
	float ___Varience_0;
	Vector2_t2156229523  ___Direction_1;
	Il2CppMethodPointer ___Action_2;
};
#endif // ANGLELISTENER_T4071059023_H
#ifndef CAMERAINTRINSICS_T3368130170_H
#define CAMERAINTRINSICS_T3368130170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.CameraIntrinsics
struct  CameraIntrinsics_t3368130170 
{
public:
	// UnityEngine.Vector2 GoogleARCore.CameraIntrinsics::FocalLength
	Vector2_t2156229523  ___FocalLength_0;
	// UnityEngine.Vector2 GoogleARCore.CameraIntrinsics::PrincipalPoint
	Vector2_t2156229523  ___PrincipalPoint_1;
	// UnityEngine.Vector2Int GoogleARCore.CameraIntrinsics::ImageDimensions
	Vector2Int_t3469998543  ___ImageDimensions_2;

public:
	inline static int32_t get_offset_of_FocalLength_0() { return static_cast<int32_t>(offsetof(CameraIntrinsics_t3368130170, ___FocalLength_0)); }
	inline Vector2_t2156229523  get_FocalLength_0() const { return ___FocalLength_0; }
	inline Vector2_t2156229523 * get_address_of_FocalLength_0() { return &___FocalLength_0; }
	inline void set_FocalLength_0(Vector2_t2156229523  value)
	{
		___FocalLength_0 = value;
	}

	inline static int32_t get_offset_of_PrincipalPoint_1() { return static_cast<int32_t>(offsetof(CameraIntrinsics_t3368130170, ___PrincipalPoint_1)); }
	inline Vector2_t2156229523  get_PrincipalPoint_1() const { return ___PrincipalPoint_1; }
	inline Vector2_t2156229523 * get_address_of_PrincipalPoint_1() { return &___PrincipalPoint_1; }
	inline void set_PrincipalPoint_1(Vector2_t2156229523  value)
	{
		___PrincipalPoint_1 = value;
	}

	inline static int32_t get_offset_of_ImageDimensions_2() { return static_cast<int32_t>(offsetof(CameraIntrinsics_t3368130170, ___ImageDimensions_2)); }
	inline Vector2Int_t3469998543  get_ImageDimensions_2() const { return ___ImageDimensions_2; }
	inline Vector2Int_t3469998543 * get_address_of_ImageDimensions_2() { return &___ImageDimensions_2; }
	inline void set_ImageDimensions_2(Vector2Int_t3469998543  value)
	{
		___ImageDimensions_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAINTRINSICS_T3368130170_H
#ifndef XPTRACKINGSTATE_T2235208166_H
#define XPTRACKINGSTATE_T2235208166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.CrossPlatform.XPTrackingState
struct  XPTrackingState_t2235208166 
{
public:
	// System.Int32 GoogleARCore.CrossPlatform.XPTrackingState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XPTrackingState_t2235208166, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPTRACKINGSTATE_T2235208166_H
#ifndef CAMERAIMAGEBYTES_T853625235_H
#define CAMERAIMAGEBYTES_T853625235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.CameraImageBytes
struct  CameraImageBytes_t853625235 
{
public:
	// System.IntPtr GoogleARCore.CameraImageBytes::m_ImageHandle
	intptr_t ___m_ImageHandle_0;
	// System.Boolean GoogleARCore.CameraImageBytes::<IsAvailable>k__BackingField
	bool ___U3CIsAvailableU3Ek__BackingField_1;
	// System.Int32 GoogleARCore.CameraImageBytes::<Width>k__BackingField
	int32_t ___U3CWidthU3Ek__BackingField_2;
	// System.Int32 GoogleARCore.CameraImageBytes::<Height>k__BackingField
	int32_t ___U3CHeightU3Ek__BackingField_3;
	// System.IntPtr GoogleARCore.CameraImageBytes::<Y>k__BackingField
	intptr_t ___U3CYU3Ek__BackingField_4;
	// System.IntPtr GoogleARCore.CameraImageBytes::<U>k__BackingField
	intptr_t ___U3CUU3Ek__BackingField_5;
	// System.IntPtr GoogleARCore.CameraImageBytes::<V>k__BackingField
	intptr_t ___U3CVU3Ek__BackingField_6;
	// System.Int32 GoogleARCore.CameraImageBytes::<YRowStride>k__BackingField
	int32_t ___U3CYRowStrideU3Ek__BackingField_7;
	// System.Int32 GoogleARCore.CameraImageBytes::<UVPixelStride>k__BackingField
	int32_t ___U3CUVPixelStrideU3Ek__BackingField_8;
	// System.Int32 GoogleARCore.CameraImageBytes::<UVRowStride>k__BackingField
	int32_t ___U3CUVRowStrideU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_m_ImageHandle_0() { return static_cast<int32_t>(offsetof(CameraImageBytes_t853625235, ___m_ImageHandle_0)); }
	inline intptr_t get_m_ImageHandle_0() const { return ___m_ImageHandle_0; }
	inline intptr_t* get_address_of_m_ImageHandle_0() { return &___m_ImageHandle_0; }
	inline void set_m_ImageHandle_0(intptr_t value)
	{
		___m_ImageHandle_0 = value;
	}

	inline static int32_t get_offset_of_U3CIsAvailableU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CameraImageBytes_t853625235, ___U3CIsAvailableU3Ek__BackingField_1)); }
	inline bool get_U3CIsAvailableU3Ek__BackingField_1() const { return ___U3CIsAvailableU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsAvailableU3Ek__BackingField_1() { return &___U3CIsAvailableU3Ek__BackingField_1; }
	inline void set_U3CIsAvailableU3Ek__BackingField_1(bool value)
	{
		___U3CIsAvailableU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CWidthU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CameraImageBytes_t853625235, ___U3CWidthU3Ek__BackingField_2)); }
	inline int32_t get_U3CWidthU3Ek__BackingField_2() const { return ___U3CWidthU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CWidthU3Ek__BackingField_2() { return &___U3CWidthU3Ek__BackingField_2; }
	inline void set_U3CWidthU3Ek__BackingField_2(int32_t value)
	{
		___U3CWidthU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CHeightU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CameraImageBytes_t853625235, ___U3CHeightU3Ek__BackingField_3)); }
	inline int32_t get_U3CHeightU3Ek__BackingField_3() const { return ___U3CHeightU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CHeightU3Ek__BackingField_3() { return &___U3CHeightU3Ek__BackingField_3; }
	inline void set_U3CHeightU3Ek__BackingField_3(int32_t value)
	{
		___U3CHeightU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CYU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CameraImageBytes_t853625235, ___U3CYU3Ek__BackingField_4)); }
	inline intptr_t get_U3CYU3Ek__BackingField_4() const { return ___U3CYU3Ek__BackingField_4; }
	inline intptr_t* get_address_of_U3CYU3Ek__BackingField_4() { return &___U3CYU3Ek__BackingField_4; }
	inline void set_U3CYU3Ek__BackingField_4(intptr_t value)
	{
		___U3CYU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CUU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CameraImageBytes_t853625235, ___U3CUU3Ek__BackingField_5)); }
	inline intptr_t get_U3CUU3Ek__BackingField_5() const { return ___U3CUU3Ek__BackingField_5; }
	inline intptr_t* get_address_of_U3CUU3Ek__BackingField_5() { return &___U3CUU3Ek__BackingField_5; }
	inline void set_U3CUU3Ek__BackingField_5(intptr_t value)
	{
		___U3CUU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CVU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CameraImageBytes_t853625235, ___U3CVU3Ek__BackingField_6)); }
	inline intptr_t get_U3CVU3Ek__BackingField_6() const { return ___U3CVU3Ek__BackingField_6; }
	inline intptr_t* get_address_of_U3CVU3Ek__BackingField_6() { return &___U3CVU3Ek__BackingField_6; }
	inline void set_U3CVU3Ek__BackingField_6(intptr_t value)
	{
		___U3CVU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CYRowStrideU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CameraImageBytes_t853625235, ___U3CYRowStrideU3Ek__BackingField_7)); }
	inline int32_t get_U3CYRowStrideU3Ek__BackingField_7() const { return ___U3CYRowStrideU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CYRowStrideU3Ek__BackingField_7() { return &___U3CYRowStrideU3Ek__BackingField_7; }
	inline void set_U3CYRowStrideU3Ek__BackingField_7(int32_t value)
	{
		___U3CYRowStrideU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CUVPixelStrideU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CameraImageBytes_t853625235, ___U3CUVPixelStrideU3Ek__BackingField_8)); }
	inline int32_t get_U3CUVPixelStrideU3Ek__BackingField_8() const { return ___U3CUVPixelStrideU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CUVPixelStrideU3Ek__BackingField_8() { return &___U3CUVPixelStrideU3Ek__BackingField_8; }
	inline void set_U3CUVPixelStrideU3Ek__BackingField_8(int32_t value)
	{
		___U3CUVPixelStrideU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CUVRowStrideU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(CameraImageBytes_t853625235, ___U3CUVRowStrideU3Ek__BackingField_9)); }
	inline int32_t get_U3CUVRowStrideU3Ek__BackingField_9() const { return ___U3CUVRowStrideU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CUVRowStrideU3Ek__BackingField_9() { return &___U3CUVRowStrideU3Ek__BackingField_9; }
	inline void set_U3CUVRowStrideU3Ek__BackingField_9(int32_t value)
	{
		___U3CUVRowStrideU3Ek__BackingField_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GoogleARCore.CameraImageBytes
struct CameraImageBytes_t853625235_marshaled_pinvoke
{
	intptr_t ___m_ImageHandle_0;
	int32_t ___U3CIsAvailableU3Ek__BackingField_1;
	int32_t ___U3CWidthU3Ek__BackingField_2;
	int32_t ___U3CHeightU3Ek__BackingField_3;
	intptr_t ___U3CYU3Ek__BackingField_4;
	intptr_t ___U3CUU3Ek__BackingField_5;
	intptr_t ___U3CVU3Ek__BackingField_6;
	int32_t ___U3CYRowStrideU3Ek__BackingField_7;
	int32_t ___U3CUVPixelStrideU3Ek__BackingField_8;
	int32_t ___U3CUVRowStrideU3Ek__BackingField_9;
};
// Native definition for COM marshalling of GoogleARCore.CameraImageBytes
struct CameraImageBytes_t853625235_marshaled_com
{
	intptr_t ___m_ImageHandle_0;
	int32_t ___U3CIsAvailableU3Ek__BackingField_1;
	int32_t ___U3CWidthU3Ek__BackingField_2;
	int32_t ___U3CHeightU3Ek__BackingField_3;
	intptr_t ___U3CYU3Ek__BackingField_4;
	intptr_t ___U3CUU3Ek__BackingField_5;
	intptr_t ___U3CVU3Ek__BackingField_6;
	int32_t ___U3CYRowStrideU3Ek__BackingField_7;
	int32_t ___U3CUVPixelStrideU3Ek__BackingField_8;
	int32_t ___U3CUVRowStrideU3Ek__BackingField_9;
};
#endif // CAMERAIMAGEBYTES_T853625235_H
#ifndef APKAVAILABILITYSTATUS_T2902724566_H
#define APKAVAILABILITYSTATUS_T2902724566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.ApkAvailabilityStatus
struct  ApkAvailabilityStatus_t2902724566 
{
public:
	// System.Int32 GoogleARCore.ApkAvailabilityStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ApkAvailabilityStatus_t2902724566, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APKAVAILABILITYSTATUS_T2902724566_H
#ifndef XPSESSIONSTATUS_T1871925907_H
#define XPSESSIONSTATUS_T1871925907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.CrossPlatform.XPSessionStatus
struct  XPSessionStatus_t1871925907 
{
public:
	// System.Int32 GoogleARCore.CrossPlatform.XPSessionStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XPSessionStatus_t1871925907, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPSESSIONSTATUS_T1871925907_H
#ifndef TKGESTURERECOGNIZERSTATE_T2713539247_H
#define TKGESTURERECOGNIZERSTATE_T2713539247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TKGestureRecognizerState
struct  TKGestureRecognizerState_t2713539247 
{
public:
	// System.Int32 TKGestureRecognizerState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TKGestureRecognizerState_t2713539247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TKGESTURERECOGNIZERSTATE_T2713539247_H
#ifndef SESSIONSTATUS_T551112162_H
#define SESSIONSTATUS_T551112162_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.SessionStatus
struct  SessionStatus_t551112162 
{
public:
	// System.Int32 GoogleARCore.SessionStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SessionStatus_t551112162, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONSTATUS_T551112162_H
#ifndef LIGHTESTIMATESTATE_T4275751054_H
#define LIGHTESTIMATESTATE_T4275751054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.LightEstimateState
struct  LightEstimateState_t4275751054 
{
public:
	// System.Int32 GoogleARCore.LightEstimateState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LightEstimateState_t4275751054, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTESTIMATESTATE_T4275751054_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef NULLABLE_1_T308142655_H
#define NULLABLE_1_T308142655_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<TKRect>
struct  Nullable_1_t308142655 
{
public:
	// T System.Nullable`1::value
	TKRect_t2880547869  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t308142655, ___value_0)); }
	inline TKRect_t2880547869  get_value_0() const { return ___value_0; }
	inline TKRect_t2880547869 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(TKRect_t2880547869  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t308142655, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T308142655_H
#ifndef TOUCHPHASE_T72348083_H
#define TOUCHPHASE_T72348083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t72348083 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t72348083, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T72348083_H
#ifndef POSE_T545244865_H
#define POSE_T545244865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Pose
struct  Pose_t545244865 
{
public:
	// UnityEngine.Vector3 UnityEngine.Pose::position
	Vector3_t3722313464  ___position_0;
	// UnityEngine.Quaternion UnityEngine.Pose::rotation
	Quaternion_t2301928331  ___rotation_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(Pose_t545244865, ___position_0)); }
	inline Vector3_t3722313464  get_position_0() const { return ___position_0; }
	inline Vector3_t3722313464 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t3722313464  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_rotation_1() { return static_cast<int32_t>(offsetof(Pose_t545244865, ___rotation_1)); }
	inline Quaternion_t2301928331  get_rotation_1() const { return ___rotation_1; }
	inline Quaternion_t2301928331 * get_address_of_rotation_1() { return &___rotation_1; }
	inline void set_rotation_1(Quaternion_t2301928331  value)
	{
		___rotation_1 = value;
	}
};

struct Pose_t545244865_StaticFields
{
public:
	// UnityEngine.Pose UnityEngine.Pose::k_Identity
	Pose_t545244865  ___k_Identity_2;

public:
	inline static int32_t get_offset_of_k_Identity_2() { return static_cast<int32_t>(offsetof(Pose_t545244865_StaticFields, ___k_Identity_2)); }
	inline Pose_t545244865  get_k_Identity_2() const { return ___k_Identity_2; }
	inline Pose_t545244865 * get_address_of_k_Identity_2() { return &___k_Identity_2; }
	inline void set_k_Identity_2(Pose_t545244865  value)
	{
		___k_Identity_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSE_T545244865_H
#ifndef TRACKABLE_T2214220236_H
#define TRACKABLE_T2214220236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Trackable
struct  Trackable_t2214220236  : public RuntimeObject
{
public:
	// System.IntPtr GoogleARCore.Trackable::m_TrackableNativeHandle
	intptr_t ___m_TrackableNativeHandle_0;
	// GoogleARCoreInternal.NativeSession GoogleARCore.Trackable::m_NativeSession
	NativeSession_t1550589577 * ___m_NativeSession_1;
	// System.Boolean GoogleARCore.Trackable::m_IsSessionDestroyed
	bool ___m_IsSessionDestroyed_2;

public:
	inline static int32_t get_offset_of_m_TrackableNativeHandle_0() { return static_cast<int32_t>(offsetof(Trackable_t2214220236, ___m_TrackableNativeHandle_0)); }
	inline intptr_t get_m_TrackableNativeHandle_0() const { return ___m_TrackableNativeHandle_0; }
	inline intptr_t* get_address_of_m_TrackableNativeHandle_0() { return &___m_TrackableNativeHandle_0; }
	inline void set_m_TrackableNativeHandle_0(intptr_t value)
	{
		___m_TrackableNativeHandle_0 = value;
	}

	inline static int32_t get_offset_of_m_NativeSession_1() { return static_cast<int32_t>(offsetof(Trackable_t2214220236, ___m_NativeSession_1)); }
	inline NativeSession_t1550589577 * get_m_NativeSession_1() const { return ___m_NativeSession_1; }
	inline NativeSession_t1550589577 ** get_address_of_m_NativeSession_1() { return &___m_NativeSession_1; }
	inline void set_m_NativeSession_1(NativeSession_t1550589577 * value)
	{
		___m_NativeSession_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_NativeSession_1), value);
	}

	inline static int32_t get_offset_of_m_IsSessionDestroyed_2() { return static_cast<int32_t>(offsetof(Trackable_t2214220236, ___m_IsSessionDestroyed_2)); }
	inline bool get_m_IsSessionDestroyed_2() const { return ___m_IsSessionDestroyed_2; }
	inline bool* get_address_of_m_IsSessionDestroyed_2() { return &___m_IsSessionDestroyed_2; }
	inline void set_m_IsSessionDestroyed_2(bool value)
	{
		___m_IsSessionDestroyed_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLE_T2214220236_H
#ifndef APIARSTATUS_T1919843852_H
#define APIARSTATUS_T1919843852_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ApiArStatus
struct  ApiArStatus_t1919843852 
{
public:
	// System.Int32 GoogleARCoreInternal.ApiArStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ApiArStatus_t1919843852, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIARSTATUS_T1919843852_H
#ifndef APIAPKINSTALLATIONSTATUS_T33526722_H
#define APIAPKINSTALLATIONSTATUS_T33526722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ApiApkInstallationStatus
struct  ApiApkInstallationStatus_t33526722 
{
public:
	// System.Int32 GoogleARCoreInternal.ApiApkInstallationStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ApiApkInstallationStatus_t33526722, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIAPKINSTALLATIONSTATUS_T33526722_H
#ifndef APIAVAILABILITY_T1149954610_H
#define APIAVAILABILITY_T1149954610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ApiAvailability
struct  ApiAvailability_t1149954610 
{
public:
	// System.Int32 GoogleARCoreInternal.ApiAvailability::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ApiAvailability_t1149954610, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIAVAILABILITY_T1149954610_H
#ifndef U3C_REQUESTCAMERAPERMISSIONU3EC__ANONSTOREY0_T3100848547_H
#define U3C_REQUESTCAMERAPERMISSIONU3EC__ANONSTOREY0_T3100848547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ARPrestoCallbackManager/<_RequestCameraPermission>c__AnonStorey0
struct  U3C_RequestCameraPermissionU3Ec__AnonStorey0_t3100848547  : public RuntimeObject
{
public:
	// GoogleARCoreInternal.ARPrestoCallbackManager/CameraPermissionsResultCallback GoogleARCoreInternal.ARPrestoCallbackManager/<_RequestCameraPermission>c__AnonStorey0::onComplete
	CameraPermissionsResultCallback_t3686029889 * ___onComplete_0;
	// System.IntPtr GoogleARCoreInternal.ARPrestoCallbackManager/<_RequestCameraPermission>c__AnonStorey0::context
	intptr_t ___context_1;

public:
	inline static int32_t get_offset_of_onComplete_0() { return static_cast<int32_t>(offsetof(U3C_RequestCameraPermissionU3Ec__AnonStorey0_t3100848547, ___onComplete_0)); }
	inline CameraPermissionsResultCallback_t3686029889 * get_onComplete_0() const { return ___onComplete_0; }
	inline CameraPermissionsResultCallback_t3686029889 ** get_address_of_onComplete_0() { return &___onComplete_0; }
	inline void set_onComplete_0(CameraPermissionsResultCallback_t3686029889 * value)
	{
		___onComplete_0 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_0), value);
	}

	inline static int32_t get_offset_of_context_1() { return static_cast<int32_t>(offsetof(U3C_RequestCameraPermissionU3Ec__AnonStorey0_t3100848547, ___context_1)); }
	inline intptr_t get_context_1() const { return ___context_1; }
	inline intptr_t* get_address_of_context_1() { return &___context_1; }
	inline void set_context_1(intptr_t value)
	{
		___context_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_REQUESTCAMERAPERMISSIONU3EC__ANONSTOREY0_T3100848547_H
#ifndef FEATUREPOINTORIENTATIONMODE_T4095457472_H
#define FEATUREPOINTORIENTATIONMODE_T4095457472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.FeaturePointOrientationMode
struct  FeaturePointOrientationMode_t4095457472 
{
public:
	// System.Int32 GoogleARCore.FeaturePointOrientationMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FeaturePointOrientationMode_t4095457472, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEATUREPOINTORIENTATIONMODE_T4095457472_H
#ifndef CLOUDANCHORREQUEST_T2589044226_H
#define CLOUDANCHORREQUEST_T2589044226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.CrossPlatform.CloudServiceManager/CloudAnchorRequest
struct  CloudAnchorRequest_t2589044226  : public RuntimeObject
{
public:
	// System.Boolean GoogleARCoreInternal.CrossPlatform.CloudServiceManager/CloudAnchorRequest::IsComplete
	bool ___IsComplete_0;
	// GoogleARCoreInternal.NativeSession GoogleARCoreInternal.CrossPlatform.CloudServiceManager/CloudAnchorRequest::NativeSession
	NativeSession_t1550589577 * ___NativeSession_1;
	// System.IntPtr GoogleARCoreInternal.CrossPlatform.CloudServiceManager/CloudAnchorRequest::AnchorHandle
	intptr_t ___AnchorHandle_2;
	// System.Action`1<GoogleARCore.CrossPlatform.CloudAnchorResult> GoogleARCoreInternal.CrossPlatform.CloudServiceManager/CloudAnchorRequest::OnTaskComplete
	Action_1_t3619531002 * ___OnTaskComplete_3;

public:
	inline static int32_t get_offset_of_IsComplete_0() { return static_cast<int32_t>(offsetof(CloudAnchorRequest_t2589044226, ___IsComplete_0)); }
	inline bool get_IsComplete_0() const { return ___IsComplete_0; }
	inline bool* get_address_of_IsComplete_0() { return &___IsComplete_0; }
	inline void set_IsComplete_0(bool value)
	{
		___IsComplete_0 = value;
	}

	inline static int32_t get_offset_of_NativeSession_1() { return static_cast<int32_t>(offsetof(CloudAnchorRequest_t2589044226, ___NativeSession_1)); }
	inline NativeSession_t1550589577 * get_NativeSession_1() const { return ___NativeSession_1; }
	inline NativeSession_t1550589577 ** get_address_of_NativeSession_1() { return &___NativeSession_1; }
	inline void set_NativeSession_1(NativeSession_t1550589577 * value)
	{
		___NativeSession_1 = value;
		Il2CppCodeGenWriteBarrier((&___NativeSession_1), value);
	}

	inline static int32_t get_offset_of_AnchorHandle_2() { return static_cast<int32_t>(offsetof(CloudAnchorRequest_t2589044226, ___AnchorHandle_2)); }
	inline intptr_t get_AnchorHandle_2() const { return ___AnchorHandle_2; }
	inline intptr_t* get_address_of_AnchorHandle_2() { return &___AnchorHandle_2; }
	inline void set_AnchorHandle_2(intptr_t value)
	{
		___AnchorHandle_2 = value;
	}

	inline static int32_t get_offset_of_OnTaskComplete_3() { return static_cast<int32_t>(offsetof(CloudAnchorRequest_t2589044226, ___OnTaskComplete_3)); }
	inline Action_1_t3619531002 * get_OnTaskComplete_3() const { return ___OnTaskComplete_3; }
	inline Action_1_t3619531002 ** get_address_of_OnTaskComplete_3() { return &___OnTaskComplete_3; }
	inline void set_OnTaskComplete_3(Action_1_t3619531002 * value)
	{
		___OnTaskComplete_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnTaskComplete_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDANCHORREQUEST_T2589044226_H
#ifndef DISPLAYUVCOORDS_T972795656_H
#define DISPLAYUVCOORDS_T972795656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.DisplayUvCoords
struct  DisplayUvCoords_t972795656 
{
public:
	// UnityEngine.Vector2 GoogleARCore.DisplayUvCoords::TopLeft
	Vector2_t2156229523  ___TopLeft_0;
	// UnityEngine.Vector2 GoogleARCore.DisplayUvCoords::TopRight
	Vector2_t2156229523  ___TopRight_1;
	// UnityEngine.Vector2 GoogleARCore.DisplayUvCoords::BottomLeft
	Vector2_t2156229523  ___BottomLeft_2;
	// UnityEngine.Vector2 GoogleARCore.DisplayUvCoords::BottomRight
	Vector2_t2156229523  ___BottomRight_3;

public:
	inline static int32_t get_offset_of_TopLeft_0() { return static_cast<int32_t>(offsetof(DisplayUvCoords_t972795656, ___TopLeft_0)); }
	inline Vector2_t2156229523  get_TopLeft_0() const { return ___TopLeft_0; }
	inline Vector2_t2156229523 * get_address_of_TopLeft_0() { return &___TopLeft_0; }
	inline void set_TopLeft_0(Vector2_t2156229523  value)
	{
		___TopLeft_0 = value;
	}

	inline static int32_t get_offset_of_TopRight_1() { return static_cast<int32_t>(offsetof(DisplayUvCoords_t972795656, ___TopRight_1)); }
	inline Vector2_t2156229523  get_TopRight_1() const { return ___TopRight_1; }
	inline Vector2_t2156229523 * get_address_of_TopRight_1() { return &___TopRight_1; }
	inline void set_TopRight_1(Vector2_t2156229523  value)
	{
		___TopRight_1 = value;
	}

	inline static int32_t get_offset_of_BottomLeft_2() { return static_cast<int32_t>(offsetof(DisplayUvCoords_t972795656, ___BottomLeft_2)); }
	inline Vector2_t2156229523  get_BottomLeft_2() const { return ___BottomLeft_2; }
	inline Vector2_t2156229523 * get_address_of_BottomLeft_2() { return &___BottomLeft_2; }
	inline void set_BottomLeft_2(Vector2_t2156229523  value)
	{
		___BottomLeft_2 = value;
	}

	inline static int32_t get_offset_of_BottomRight_3() { return static_cast<int32_t>(offsetof(DisplayUvCoords_t972795656, ___BottomRight_3)); }
	inline Vector2_t2156229523  get_BottomRight_3() const { return ___BottomRight_3; }
	inline Vector2_t2156229523 * get_address_of_BottomRight_3() { return &___BottomRight_3; }
	inline void set_BottomRight_3(Vector2_t2156229523  value)
	{
		___BottomRight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAYUVCOORDS_T972795656_H
#ifndef DETECTEDPLANEFINDINGMODE_T2176733542_H
#define DETECTEDPLANEFINDINGMODE_T2176733542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.DetectedPlaneFindingMode
struct  DetectedPlaneFindingMode_t2176733542 
{
public:
	// System.Int32 GoogleARCore.DetectedPlaneFindingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DetectedPlaneFindingMode_t2176733542, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DETECTEDPLANEFINDINGMODE_T2176733542_H
#ifndef AUGMENTEDIMAGE_T279011686_H
#define AUGMENTEDIMAGE_T279011686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.AugmentedImage
struct  AugmentedImage_t279011686  : public Trackable_t2214220236
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUGMENTEDIMAGE_T279011686_H
#ifndef TKABSTRACTGESTURERECOGNIZER_T1903772339_H
#define TKABSTRACTGESTURERECOGNIZER_T1903772339_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TKAbstractGestureRecognizer
struct  TKAbstractGestureRecognizer_t1903772339  : public RuntimeObject
{
public:
	// System.Boolean TKAbstractGestureRecognizer::enabled
	bool ___enabled_0;
	// System.Nullable`1<TKRect> TKAbstractGestureRecognizer::boundaryFrame
	Nullable_1_t308142655  ___boundaryFrame_1;
	// System.UInt32 TKAbstractGestureRecognizer::zIndex
	uint32_t ___zIndex_2;
	// TKGestureRecognizerState TKAbstractGestureRecognizer::_state
	int32_t ____state_3;
	// System.Boolean TKAbstractGestureRecognizer::alwaysSendTouchesMoved
	bool ___alwaysSendTouchesMoved_4;
	// System.Collections.Generic.List`1<TKTouch> TKAbstractGestureRecognizer::_trackingTouches
	List_1_t34712449 * ____trackingTouches_5;
	// System.Collections.Generic.List`1<TKTouch> TKAbstractGestureRecognizer::_subsetOfTouchesBeingTrackedApplicableToCurrentRecognizer
	List_1_t34712449 * ____subsetOfTouchesBeingTrackedApplicableToCurrentRecognizer_6;
	// System.Boolean TKAbstractGestureRecognizer::_sentTouchesBegan
	bool ____sentTouchesBegan_7;
	// System.Boolean TKAbstractGestureRecognizer::_sentTouchesMoved
	bool ____sentTouchesMoved_8;
	// System.Boolean TKAbstractGestureRecognizer::_sentTouchesEnded
	bool ____sentTouchesEnded_9;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(TKAbstractGestureRecognizer_t1903772339, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_boundaryFrame_1() { return static_cast<int32_t>(offsetof(TKAbstractGestureRecognizer_t1903772339, ___boundaryFrame_1)); }
	inline Nullable_1_t308142655  get_boundaryFrame_1() const { return ___boundaryFrame_1; }
	inline Nullable_1_t308142655 * get_address_of_boundaryFrame_1() { return &___boundaryFrame_1; }
	inline void set_boundaryFrame_1(Nullable_1_t308142655  value)
	{
		___boundaryFrame_1 = value;
	}

	inline static int32_t get_offset_of_zIndex_2() { return static_cast<int32_t>(offsetof(TKAbstractGestureRecognizer_t1903772339, ___zIndex_2)); }
	inline uint32_t get_zIndex_2() const { return ___zIndex_2; }
	inline uint32_t* get_address_of_zIndex_2() { return &___zIndex_2; }
	inline void set_zIndex_2(uint32_t value)
	{
		___zIndex_2 = value;
	}

	inline static int32_t get_offset_of__state_3() { return static_cast<int32_t>(offsetof(TKAbstractGestureRecognizer_t1903772339, ____state_3)); }
	inline int32_t get__state_3() const { return ____state_3; }
	inline int32_t* get_address_of__state_3() { return &____state_3; }
	inline void set__state_3(int32_t value)
	{
		____state_3 = value;
	}

	inline static int32_t get_offset_of_alwaysSendTouchesMoved_4() { return static_cast<int32_t>(offsetof(TKAbstractGestureRecognizer_t1903772339, ___alwaysSendTouchesMoved_4)); }
	inline bool get_alwaysSendTouchesMoved_4() const { return ___alwaysSendTouchesMoved_4; }
	inline bool* get_address_of_alwaysSendTouchesMoved_4() { return &___alwaysSendTouchesMoved_4; }
	inline void set_alwaysSendTouchesMoved_4(bool value)
	{
		___alwaysSendTouchesMoved_4 = value;
	}

	inline static int32_t get_offset_of__trackingTouches_5() { return static_cast<int32_t>(offsetof(TKAbstractGestureRecognizer_t1903772339, ____trackingTouches_5)); }
	inline List_1_t34712449 * get__trackingTouches_5() const { return ____trackingTouches_5; }
	inline List_1_t34712449 ** get_address_of__trackingTouches_5() { return &____trackingTouches_5; }
	inline void set__trackingTouches_5(List_1_t34712449 * value)
	{
		____trackingTouches_5 = value;
		Il2CppCodeGenWriteBarrier((&____trackingTouches_5), value);
	}

	inline static int32_t get_offset_of__subsetOfTouchesBeingTrackedApplicableToCurrentRecognizer_6() { return static_cast<int32_t>(offsetof(TKAbstractGestureRecognizer_t1903772339, ____subsetOfTouchesBeingTrackedApplicableToCurrentRecognizer_6)); }
	inline List_1_t34712449 * get__subsetOfTouchesBeingTrackedApplicableToCurrentRecognizer_6() const { return ____subsetOfTouchesBeingTrackedApplicableToCurrentRecognizer_6; }
	inline List_1_t34712449 ** get_address_of__subsetOfTouchesBeingTrackedApplicableToCurrentRecognizer_6() { return &____subsetOfTouchesBeingTrackedApplicableToCurrentRecognizer_6; }
	inline void set__subsetOfTouchesBeingTrackedApplicableToCurrentRecognizer_6(List_1_t34712449 * value)
	{
		____subsetOfTouchesBeingTrackedApplicableToCurrentRecognizer_6 = value;
		Il2CppCodeGenWriteBarrier((&____subsetOfTouchesBeingTrackedApplicableToCurrentRecognizer_6), value);
	}

	inline static int32_t get_offset_of__sentTouchesBegan_7() { return static_cast<int32_t>(offsetof(TKAbstractGestureRecognizer_t1903772339, ____sentTouchesBegan_7)); }
	inline bool get__sentTouchesBegan_7() const { return ____sentTouchesBegan_7; }
	inline bool* get_address_of__sentTouchesBegan_7() { return &____sentTouchesBegan_7; }
	inline void set__sentTouchesBegan_7(bool value)
	{
		____sentTouchesBegan_7 = value;
	}

	inline static int32_t get_offset_of__sentTouchesMoved_8() { return static_cast<int32_t>(offsetof(TKAbstractGestureRecognizer_t1903772339, ____sentTouchesMoved_8)); }
	inline bool get__sentTouchesMoved_8() const { return ____sentTouchesMoved_8; }
	inline bool* get_address_of__sentTouchesMoved_8() { return &____sentTouchesMoved_8; }
	inline void set__sentTouchesMoved_8(bool value)
	{
		____sentTouchesMoved_8 = value;
	}

	inline static int32_t get_offset_of__sentTouchesEnded_9() { return static_cast<int32_t>(offsetof(TKAbstractGestureRecognizer_t1903772339, ____sentTouchesEnded_9)); }
	inline bool get__sentTouchesEnded_9() const { return ____sentTouchesEnded_9; }
	inline bool* get_address_of__sentTouchesEnded_9() { return &____sentTouchesEnded_9; }
	inline void set__sentTouchesEnded_9(bool value)
	{
		____sentTouchesEnded_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TKABSTRACTGESTURERECOGNIZER_T1903772339_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef ARCOREIOSLIFECYCLEMANAGER_T2269316356_H
#define ARCOREIOSLIFECYCLEMANAGER_T2269316356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ARCoreIOSLifecycleManager
struct  ARCoreIOSLifecycleManager_t2269316356  : public RuntimeObject
{
public:
	// System.IntPtr GoogleARCoreInternal.ARCoreIOSLifecycleManager::m_SessionHandle
	intptr_t ___m_SessionHandle_2;
	// System.String GoogleARCoreInternal.ARCoreIOSLifecycleManager::m_CloudServicesApiKey
	String_t* ___m_CloudServicesApiKey_3;
	// System.IntPtr GoogleARCoreInternal.ARCoreIOSLifecycleManager::m_RealArKitSessionHandle
	intptr_t ___m_RealArKitSessionHandle_4;
	// System.IntPtr GoogleARCoreInternal.ARCoreIOSLifecycleManager::m_FrameHandle
	intptr_t ___m_FrameHandle_5;
	// System.Boolean GoogleARCoreInternal.ARCoreIOSLifecycleManager::m_SessionEnabled
	bool ___m_SessionEnabled_6;
	// System.Action GoogleARCoreInternal.ARCoreIOSLifecycleManager::EarlyUpdate
	Action_t1264377477 * ___EarlyUpdate_7;
	// GoogleARCore.SessionStatus GoogleARCoreInternal.ARCoreIOSLifecycleManager::<SessionStatus>k__BackingField
	int32_t ___U3CSessionStatusU3Ek__BackingField_8;
	// GoogleARCore.ARCoreSession GoogleARCoreInternal.ARCoreIOSLifecycleManager::<SessionComponent>k__BackingField
	ARCoreSession_t3898212540 * ___U3CSessionComponentU3Ek__BackingField_9;
	// GoogleARCoreInternal.NativeSession GoogleARCoreInternal.ARCoreIOSLifecycleManager::<NativeSession>k__BackingField
	NativeSession_t1550589577 * ___U3CNativeSessionU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_m_SessionHandle_2() { return static_cast<int32_t>(offsetof(ARCoreIOSLifecycleManager_t2269316356, ___m_SessionHandle_2)); }
	inline intptr_t get_m_SessionHandle_2() const { return ___m_SessionHandle_2; }
	inline intptr_t* get_address_of_m_SessionHandle_2() { return &___m_SessionHandle_2; }
	inline void set_m_SessionHandle_2(intptr_t value)
	{
		___m_SessionHandle_2 = value;
	}

	inline static int32_t get_offset_of_m_CloudServicesApiKey_3() { return static_cast<int32_t>(offsetof(ARCoreIOSLifecycleManager_t2269316356, ___m_CloudServicesApiKey_3)); }
	inline String_t* get_m_CloudServicesApiKey_3() const { return ___m_CloudServicesApiKey_3; }
	inline String_t** get_address_of_m_CloudServicesApiKey_3() { return &___m_CloudServicesApiKey_3; }
	inline void set_m_CloudServicesApiKey_3(String_t* value)
	{
		___m_CloudServicesApiKey_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CloudServicesApiKey_3), value);
	}

	inline static int32_t get_offset_of_m_RealArKitSessionHandle_4() { return static_cast<int32_t>(offsetof(ARCoreIOSLifecycleManager_t2269316356, ___m_RealArKitSessionHandle_4)); }
	inline intptr_t get_m_RealArKitSessionHandle_4() const { return ___m_RealArKitSessionHandle_4; }
	inline intptr_t* get_address_of_m_RealArKitSessionHandle_4() { return &___m_RealArKitSessionHandle_4; }
	inline void set_m_RealArKitSessionHandle_4(intptr_t value)
	{
		___m_RealArKitSessionHandle_4 = value;
	}

	inline static int32_t get_offset_of_m_FrameHandle_5() { return static_cast<int32_t>(offsetof(ARCoreIOSLifecycleManager_t2269316356, ___m_FrameHandle_5)); }
	inline intptr_t get_m_FrameHandle_5() const { return ___m_FrameHandle_5; }
	inline intptr_t* get_address_of_m_FrameHandle_5() { return &___m_FrameHandle_5; }
	inline void set_m_FrameHandle_5(intptr_t value)
	{
		___m_FrameHandle_5 = value;
	}

	inline static int32_t get_offset_of_m_SessionEnabled_6() { return static_cast<int32_t>(offsetof(ARCoreIOSLifecycleManager_t2269316356, ___m_SessionEnabled_6)); }
	inline bool get_m_SessionEnabled_6() const { return ___m_SessionEnabled_6; }
	inline bool* get_address_of_m_SessionEnabled_6() { return &___m_SessionEnabled_6; }
	inline void set_m_SessionEnabled_6(bool value)
	{
		___m_SessionEnabled_6 = value;
	}

	inline static int32_t get_offset_of_EarlyUpdate_7() { return static_cast<int32_t>(offsetof(ARCoreIOSLifecycleManager_t2269316356, ___EarlyUpdate_7)); }
	inline Action_t1264377477 * get_EarlyUpdate_7() const { return ___EarlyUpdate_7; }
	inline Action_t1264377477 ** get_address_of_EarlyUpdate_7() { return &___EarlyUpdate_7; }
	inline void set_EarlyUpdate_7(Action_t1264377477 * value)
	{
		___EarlyUpdate_7 = value;
		Il2CppCodeGenWriteBarrier((&___EarlyUpdate_7), value);
	}

	inline static int32_t get_offset_of_U3CSessionStatusU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(ARCoreIOSLifecycleManager_t2269316356, ___U3CSessionStatusU3Ek__BackingField_8)); }
	inline int32_t get_U3CSessionStatusU3Ek__BackingField_8() const { return ___U3CSessionStatusU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CSessionStatusU3Ek__BackingField_8() { return &___U3CSessionStatusU3Ek__BackingField_8; }
	inline void set_U3CSessionStatusU3Ek__BackingField_8(int32_t value)
	{
		___U3CSessionStatusU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CSessionComponentU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(ARCoreIOSLifecycleManager_t2269316356, ___U3CSessionComponentU3Ek__BackingField_9)); }
	inline ARCoreSession_t3898212540 * get_U3CSessionComponentU3Ek__BackingField_9() const { return ___U3CSessionComponentU3Ek__BackingField_9; }
	inline ARCoreSession_t3898212540 ** get_address_of_U3CSessionComponentU3Ek__BackingField_9() { return &___U3CSessionComponentU3Ek__BackingField_9; }
	inline void set_U3CSessionComponentU3Ek__BackingField_9(ARCoreSession_t3898212540 * value)
	{
		___U3CSessionComponentU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSessionComponentU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CNativeSessionU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(ARCoreIOSLifecycleManager_t2269316356, ___U3CNativeSessionU3Ek__BackingField_10)); }
	inline NativeSession_t1550589577 * get_U3CNativeSessionU3Ek__BackingField_10() const { return ___U3CNativeSessionU3Ek__BackingField_10; }
	inline NativeSession_t1550589577 ** get_address_of_U3CNativeSessionU3Ek__BackingField_10() { return &___U3CNativeSessionU3Ek__BackingField_10; }
	inline void set_U3CNativeSessionU3Ek__BackingField_10(NativeSession_t1550589577 * value)
	{
		___U3CNativeSessionU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNativeSessionU3Ek__BackingField_10), value);
	}
};

struct ARCoreIOSLifecycleManager_t2269316356_StaticFields
{
public:
	// GoogleARCoreInternal.ARCoreIOSLifecycleManager GoogleARCoreInternal.ARCoreIOSLifecycleManager::s_Instance
	ARCoreIOSLifecycleManager_t2269316356 * ___s_Instance_1;

public:
	inline static int32_t get_offset_of_s_Instance_1() { return static_cast<int32_t>(offsetof(ARCoreIOSLifecycleManager_t2269316356_StaticFields, ___s_Instance_1)); }
	inline ARCoreIOSLifecycleManager_t2269316356 * get_s_Instance_1() const { return ___s_Instance_1; }
	inline ARCoreIOSLifecycleManager_t2269316356 ** get_address_of_s_Instance_1() { return &___s_Instance_1; }
	inline void set_s_Instance_1(ARCoreIOSLifecycleManager_t2269316356 * value)
	{
		___s_Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCOREIOSLIFECYCLEMANAGER_T2269316356_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef ARCOREANDROIDLIFECYCLEMANAGER_T1403231889_H
#define ARCOREANDROIDLIFECYCLEMANAGER_T1403231889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ARCoreAndroidLifecycleManager
struct  ARCoreAndroidLifecycleManager_t1403231889  : public RuntimeObject
{
public:
	// GoogleARCore.ARCoreSessionConfig GoogleARCoreInternal.ARCoreAndroidLifecycleManager::m_CachedConfig
	ARCoreSessionConfig_t3885767874 * ___m_CachedConfig_1;
	// System.Action GoogleARCoreInternal.ARCoreAndroidLifecycleManager::EarlyUpdate
	Action_t1264377477 * ___EarlyUpdate_2;
	// GoogleARCore.SessionStatus GoogleARCoreInternal.ARCoreAndroidLifecycleManager::<SessionStatus>k__BackingField
	int32_t ___U3CSessionStatusU3Ek__BackingField_3;
	// GoogleARCore.ARCoreSession GoogleARCoreInternal.ARCoreAndroidLifecycleManager::<SessionComponent>k__BackingField
	ARCoreSession_t3898212540 * ___U3CSessionComponentU3Ek__BackingField_4;
	// GoogleARCoreInternal.NativeSession GoogleARCoreInternal.ARCoreAndroidLifecycleManager::<NativeSession>k__BackingField
	NativeSession_t1550589577 * ___U3CNativeSessionU3Ek__BackingField_5;
	// UnityEngine.Texture2D GoogleARCoreInternal.ARCoreAndroidLifecycleManager::<BackgroundTexture>k__BackingField
	Texture2D_t3840446185 * ___U3CBackgroundTextureU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_m_CachedConfig_1() { return static_cast<int32_t>(offsetof(ARCoreAndroidLifecycleManager_t1403231889, ___m_CachedConfig_1)); }
	inline ARCoreSessionConfig_t3885767874 * get_m_CachedConfig_1() const { return ___m_CachedConfig_1; }
	inline ARCoreSessionConfig_t3885767874 ** get_address_of_m_CachedConfig_1() { return &___m_CachedConfig_1; }
	inline void set_m_CachedConfig_1(ARCoreSessionConfig_t3885767874 * value)
	{
		___m_CachedConfig_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachedConfig_1), value);
	}

	inline static int32_t get_offset_of_EarlyUpdate_2() { return static_cast<int32_t>(offsetof(ARCoreAndroidLifecycleManager_t1403231889, ___EarlyUpdate_2)); }
	inline Action_t1264377477 * get_EarlyUpdate_2() const { return ___EarlyUpdate_2; }
	inline Action_t1264377477 ** get_address_of_EarlyUpdate_2() { return &___EarlyUpdate_2; }
	inline void set_EarlyUpdate_2(Action_t1264377477 * value)
	{
		___EarlyUpdate_2 = value;
		Il2CppCodeGenWriteBarrier((&___EarlyUpdate_2), value);
	}

	inline static int32_t get_offset_of_U3CSessionStatusU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ARCoreAndroidLifecycleManager_t1403231889, ___U3CSessionStatusU3Ek__BackingField_3)); }
	inline int32_t get_U3CSessionStatusU3Ek__BackingField_3() const { return ___U3CSessionStatusU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CSessionStatusU3Ek__BackingField_3() { return &___U3CSessionStatusU3Ek__BackingField_3; }
	inline void set_U3CSessionStatusU3Ek__BackingField_3(int32_t value)
	{
		___U3CSessionStatusU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CSessionComponentU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ARCoreAndroidLifecycleManager_t1403231889, ___U3CSessionComponentU3Ek__BackingField_4)); }
	inline ARCoreSession_t3898212540 * get_U3CSessionComponentU3Ek__BackingField_4() const { return ___U3CSessionComponentU3Ek__BackingField_4; }
	inline ARCoreSession_t3898212540 ** get_address_of_U3CSessionComponentU3Ek__BackingField_4() { return &___U3CSessionComponentU3Ek__BackingField_4; }
	inline void set_U3CSessionComponentU3Ek__BackingField_4(ARCoreSession_t3898212540 * value)
	{
		___U3CSessionComponentU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSessionComponentU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CNativeSessionU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ARCoreAndroidLifecycleManager_t1403231889, ___U3CNativeSessionU3Ek__BackingField_5)); }
	inline NativeSession_t1550589577 * get_U3CNativeSessionU3Ek__BackingField_5() const { return ___U3CNativeSessionU3Ek__BackingField_5; }
	inline NativeSession_t1550589577 ** get_address_of_U3CNativeSessionU3Ek__BackingField_5() { return &___U3CNativeSessionU3Ek__BackingField_5; }
	inline void set_U3CNativeSessionU3Ek__BackingField_5(NativeSession_t1550589577 * value)
	{
		___U3CNativeSessionU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNativeSessionU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CBackgroundTextureU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ARCoreAndroidLifecycleManager_t1403231889, ___U3CBackgroundTextureU3Ek__BackingField_6)); }
	inline Texture2D_t3840446185 * get_U3CBackgroundTextureU3Ek__BackingField_6() const { return ___U3CBackgroundTextureU3Ek__BackingField_6; }
	inline Texture2D_t3840446185 ** get_address_of_U3CBackgroundTextureU3Ek__BackingField_6() { return &___U3CBackgroundTextureU3Ek__BackingField_6; }
	inline void set_U3CBackgroundTextureU3Ek__BackingField_6(Texture2D_t3840446185 * value)
	{
		___U3CBackgroundTextureU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBackgroundTextureU3Ek__BackingField_6), value);
	}
};

struct ARCoreAndroidLifecycleManager_t1403231889_StaticFields
{
public:
	// GoogleARCoreInternal.ARCoreAndroidLifecycleManager GoogleARCoreInternal.ARCoreAndroidLifecycleManager::s_Instance
	ARCoreAndroidLifecycleManager_t1403231889 * ___s_Instance_0;
	// System.Action GoogleARCoreInternal.ARCoreAndroidLifecycleManager::<>f__mg$cache0
	Action_t1264377477 * ___U3CU3Ef__mgU24cache0_7;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(ARCoreAndroidLifecycleManager_t1403231889_StaticFields, ___s_Instance_0)); }
	inline ARCoreAndroidLifecycleManager_t1403231889 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline ARCoreAndroidLifecycleManager_t1403231889 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(ARCoreAndroidLifecycleManager_t1403231889 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_7() { return static_cast<int32_t>(offsetof(ARCoreAndroidLifecycleManager_t1403231889_StaticFields, ___U3CU3Ef__mgU24cache0_7)); }
	inline Action_t1264377477 * get_U3CU3Ef__mgU24cache0_7() const { return ___U3CU3Ef__mgU24cache0_7; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__mgU24cache0_7() { return &___U3CU3Ef__mgU24cache0_7; }
	inline void set_U3CU3Ef__mgU24cache0_7(Action_t1264377477 * value)
	{
		___U3CU3Ef__mgU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCOREANDROIDLIFECYCLEMANAGER_T1403231889_H
#ifndef CAMERAMETADATAVALUE_T1936134915_H
#define CAMERAMETADATAVALUE_T1936134915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.CameraMetadataValue
struct  CameraMetadataValue_t1936134915 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// GoogleARCoreInternal.NdkCameraMetadataType GoogleARCore.CameraMetadataValue::m_Type
			int32_t ___m_Type_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___m_Type_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ByteValue_1_OffsetPadding[4];
			// System.SByte GoogleARCore.CameraMetadataValue::m_ByteValue
			int8_t ___m_ByteValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ByteValue_1_OffsetPadding_forAlignmentOnly[4];
			int8_t ___m_ByteValue_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_IntValue_2_OffsetPadding[4];
			// System.Int32 GoogleARCore.CameraMetadataValue::m_IntValue
			int32_t ___m_IntValue_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_IntValue_2_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_IntValue_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_LongValue_3_OffsetPadding[4];
			// System.Int64 GoogleARCore.CameraMetadataValue::m_LongValue
			int64_t ___m_LongValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_LongValue_3_OffsetPadding_forAlignmentOnly[4];
			int64_t ___m_LongValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_FloatValue_4_OffsetPadding[4];
			// System.Single GoogleARCore.CameraMetadataValue::m_FloatValue
			float ___m_FloatValue_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_FloatValue_4_OffsetPadding_forAlignmentOnly[4];
			float ___m_FloatValue_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_DoubleValue_5_OffsetPadding[4];
			// System.Double GoogleARCore.CameraMetadataValue::m_DoubleValue
			double ___m_DoubleValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_DoubleValue_5_OffsetPadding_forAlignmentOnly[4];
			double ___m_DoubleValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_RationalValue_6_OffsetPadding[4];
			// GoogleARCore.CameraMetadataRational GoogleARCore.CameraMetadataValue::m_RationalValue
			CameraMetadataRational_t1052259197  ___m_RationalValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_RationalValue_6_OffsetPadding_forAlignmentOnly[4];
			CameraMetadataRational_t1052259197  ___m_RationalValue_6_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_m_Type_0() { return static_cast<int32_t>(offsetof(CameraMetadataValue_t1936134915, ___m_Type_0)); }
	inline int32_t get_m_Type_0() const { return ___m_Type_0; }
	inline int32_t* get_address_of_m_Type_0() { return &___m_Type_0; }
	inline void set_m_Type_0(int32_t value)
	{
		___m_Type_0 = value;
	}

	inline static int32_t get_offset_of_m_ByteValue_1() { return static_cast<int32_t>(offsetof(CameraMetadataValue_t1936134915, ___m_ByteValue_1)); }
	inline int8_t get_m_ByteValue_1() const { return ___m_ByteValue_1; }
	inline int8_t* get_address_of_m_ByteValue_1() { return &___m_ByteValue_1; }
	inline void set_m_ByteValue_1(int8_t value)
	{
		___m_ByteValue_1 = value;
	}

	inline static int32_t get_offset_of_m_IntValue_2() { return static_cast<int32_t>(offsetof(CameraMetadataValue_t1936134915, ___m_IntValue_2)); }
	inline int32_t get_m_IntValue_2() const { return ___m_IntValue_2; }
	inline int32_t* get_address_of_m_IntValue_2() { return &___m_IntValue_2; }
	inline void set_m_IntValue_2(int32_t value)
	{
		___m_IntValue_2 = value;
	}

	inline static int32_t get_offset_of_m_LongValue_3() { return static_cast<int32_t>(offsetof(CameraMetadataValue_t1936134915, ___m_LongValue_3)); }
	inline int64_t get_m_LongValue_3() const { return ___m_LongValue_3; }
	inline int64_t* get_address_of_m_LongValue_3() { return &___m_LongValue_3; }
	inline void set_m_LongValue_3(int64_t value)
	{
		___m_LongValue_3 = value;
	}

	inline static int32_t get_offset_of_m_FloatValue_4() { return static_cast<int32_t>(offsetof(CameraMetadataValue_t1936134915, ___m_FloatValue_4)); }
	inline float get_m_FloatValue_4() const { return ___m_FloatValue_4; }
	inline float* get_address_of_m_FloatValue_4() { return &___m_FloatValue_4; }
	inline void set_m_FloatValue_4(float value)
	{
		___m_FloatValue_4 = value;
	}

	inline static int32_t get_offset_of_m_DoubleValue_5() { return static_cast<int32_t>(offsetof(CameraMetadataValue_t1936134915, ___m_DoubleValue_5)); }
	inline double get_m_DoubleValue_5() const { return ___m_DoubleValue_5; }
	inline double* get_address_of_m_DoubleValue_5() { return &___m_DoubleValue_5; }
	inline void set_m_DoubleValue_5(double value)
	{
		___m_DoubleValue_5 = value;
	}

	inline static int32_t get_offset_of_m_RationalValue_6() { return static_cast<int32_t>(offsetof(CameraMetadataValue_t1936134915, ___m_RationalValue_6)); }
	inline CameraMetadataRational_t1052259197  get_m_RationalValue_6() const { return ___m_RationalValue_6; }
	inline CameraMetadataRational_t1052259197 * get_address_of_m_RationalValue_6() { return &___m_RationalValue_6; }
	inline void set_m_RationalValue_6(CameraMetadataRational_t1052259197  value)
	{
		___m_RationalValue_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAMETADATAVALUE_T1936134915_H
#ifndef LIGHTESTIMATE_T1115373647_H
#define LIGHTESTIMATE_T1115373647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.LightEstimate
struct  LightEstimate_t1115373647 
{
public:
	union
	{
		struct
		{
			// GoogleARCore.LightEstimateState GoogleARCore.LightEstimate::<State>k__BackingField
			int32_t ___U3CStateU3Ek__BackingField_0;
			// System.Single GoogleARCore.LightEstimate::<PixelIntensity>k__BackingField
			float ___U3CPixelIntensityU3Ek__BackingField_1;
			// UnityEngine.Color GoogleARCore.LightEstimate::<ColorCorrection>k__BackingField
			Color_t2555686324  ___U3CColorCorrectionU3Ek__BackingField_2;
		};
		uint8_t LightEstimate_t1115373647__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LightEstimate_t1115373647, ___U3CStateU3Ek__BackingField_0)); }
	inline int32_t get_U3CStateU3Ek__BackingField_0() const { return ___U3CStateU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CStateU3Ek__BackingField_0() { return &___U3CStateU3Ek__BackingField_0; }
	inline void set_U3CStateU3Ek__BackingField_0(int32_t value)
	{
		___U3CStateU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CPixelIntensityU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(LightEstimate_t1115373647, ___U3CPixelIntensityU3Ek__BackingField_1)); }
	inline float get_U3CPixelIntensityU3Ek__BackingField_1() const { return ___U3CPixelIntensityU3Ek__BackingField_1; }
	inline float* get_address_of_U3CPixelIntensityU3Ek__BackingField_1() { return &___U3CPixelIntensityU3Ek__BackingField_1; }
	inline void set_U3CPixelIntensityU3Ek__BackingField_1(float value)
	{
		___U3CPixelIntensityU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CColorCorrectionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(LightEstimate_t1115373647, ___U3CColorCorrectionU3Ek__BackingField_2)); }
	inline Color_t2555686324  get_U3CColorCorrectionU3Ek__BackingField_2() const { return ___U3CColorCorrectionU3Ek__BackingField_2; }
	inline Color_t2555686324 * get_address_of_U3CColorCorrectionU3Ek__BackingField_2() { return &___U3CColorCorrectionU3Ek__BackingField_2; }
	inline void set_U3CColorCorrectionU3Ek__BackingField_2(Color_t2555686324  value)
	{
		___U3CColorCorrectionU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTESTIMATE_T1115373647_H
#ifndef CLOUDANCHORRESULT_T3447063407_H
#define CLOUDANCHORRESULT_T3447063407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.CrossPlatform.CloudAnchorResult
struct  CloudAnchorResult_t3447063407 
{
public:
	// GoogleARCore.CrossPlatform.CloudServiceResponse GoogleARCore.CrossPlatform.CloudAnchorResult::Response
	int32_t ___Response_0;
	// GoogleARCore.CrossPlatform.XPAnchor GoogleARCore.CrossPlatform.CloudAnchorResult::Anchor
	XPAnchor_t812171902 * ___Anchor_1;

public:
	inline static int32_t get_offset_of_Response_0() { return static_cast<int32_t>(offsetof(CloudAnchorResult_t3447063407, ___Response_0)); }
	inline int32_t get_Response_0() const { return ___Response_0; }
	inline int32_t* get_address_of_Response_0() { return &___Response_0; }
	inline void set_Response_0(int32_t value)
	{
		___Response_0 = value;
	}

	inline static int32_t get_offset_of_Anchor_1() { return static_cast<int32_t>(offsetof(CloudAnchorResult_t3447063407, ___Anchor_1)); }
	inline XPAnchor_t812171902 * get_Anchor_1() const { return ___Anchor_1; }
	inline XPAnchor_t812171902 ** get_address_of_Anchor_1() { return &___Anchor_1; }
	inline void set_Anchor_1(XPAnchor_t812171902 * value)
	{
		___Anchor_1 = value;
		Il2CppCodeGenWriteBarrier((&___Anchor_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GoogleARCore.CrossPlatform.CloudAnchorResult
struct CloudAnchorResult_t3447063407_marshaled_pinvoke
{
	int32_t ___Response_0;
	XPAnchor_t812171902 * ___Anchor_1;
};
// Native definition for COM marshalling of GoogleARCore.CrossPlatform.CloudAnchorResult
struct CloudAnchorResult_t3447063407_marshaled_com
{
	int32_t ___Response_0;
	XPAnchor_t812171902 * ___Anchor_1;
};
#endif // CLOUDANCHORRESULT_T3447063407_H
#ifndef FEATUREPOINT_T2424401674_H
#define FEATUREPOINT_T2424401674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.FeaturePoint
struct  FeaturePoint_t2424401674  : public Trackable_t2214220236
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEATUREPOINT_T2424401674_H
#ifndef TKTOUCH_T2857605003_H
#define TKTOUCH_T2857605003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TKTouch
struct  TKTouch_t2857605003  : public RuntimeObject
{
public:
	// System.Int32 TKTouch::fingerId
	int32_t ___fingerId_0;
	// UnityEngine.Vector2 TKTouch::position
	Vector2_t2156229523  ___position_1;
	// UnityEngine.Vector2 TKTouch::startPosition
	Vector2_t2156229523  ___startPosition_2;
	// UnityEngine.Vector2 TKTouch::deltaPosition
	Vector2_t2156229523  ___deltaPosition_3;
	// System.Single TKTouch::deltaTime
	float ___deltaTime_4;
	// System.Int32 TKTouch::tapCount
	int32_t ___tapCount_5;
	// UnityEngine.TouchPhase TKTouch::phase
	int32_t ___phase_6;

public:
	inline static int32_t get_offset_of_fingerId_0() { return static_cast<int32_t>(offsetof(TKTouch_t2857605003, ___fingerId_0)); }
	inline int32_t get_fingerId_0() const { return ___fingerId_0; }
	inline int32_t* get_address_of_fingerId_0() { return &___fingerId_0; }
	inline void set_fingerId_0(int32_t value)
	{
		___fingerId_0 = value;
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(TKTouch_t2857605003, ___position_1)); }
	inline Vector2_t2156229523  get_position_1() const { return ___position_1; }
	inline Vector2_t2156229523 * get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(Vector2_t2156229523  value)
	{
		___position_1 = value;
	}

	inline static int32_t get_offset_of_startPosition_2() { return static_cast<int32_t>(offsetof(TKTouch_t2857605003, ___startPosition_2)); }
	inline Vector2_t2156229523  get_startPosition_2() const { return ___startPosition_2; }
	inline Vector2_t2156229523 * get_address_of_startPosition_2() { return &___startPosition_2; }
	inline void set_startPosition_2(Vector2_t2156229523  value)
	{
		___startPosition_2 = value;
	}

	inline static int32_t get_offset_of_deltaPosition_3() { return static_cast<int32_t>(offsetof(TKTouch_t2857605003, ___deltaPosition_3)); }
	inline Vector2_t2156229523  get_deltaPosition_3() const { return ___deltaPosition_3; }
	inline Vector2_t2156229523 * get_address_of_deltaPosition_3() { return &___deltaPosition_3; }
	inline void set_deltaPosition_3(Vector2_t2156229523  value)
	{
		___deltaPosition_3 = value;
	}

	inline static int32_t get_offset_of_deltaTime_4() { return static_cast<int32_t>(offsetof(TKTouch_t2857605003, ___deltaTime_4)); }
	inline float get_deltaTime_4() const { return ___deltaTime_4; }
	inline float* get_address_of_deltaTime_4() { return &___deltaTime_4; }
	inline void set_deltaTime_4(float value)
	{
		___deltaTime_4 = value;
	}

	inline static int32_t get_offset_of_tapCount_5() { return static_cast<int32_t>(offsetof(TKTouch_t2857605003, ___tapCount_5)); }
	inline int32_t get_tapCount_5() const { return ___tapCount_5; }
	inline int32_t* get_address_of_tapCount_5() { return &___tapCount_5; }
	inline void set_tapCount_5(int32_t value)
	{
		___tapCount_5 = value;
	}

	inline static int32_t get_offset_of_phase_6() { return static_cast<int32_t>(offsetof(TKTouch_t2857605003, ___phase_6)); }
	inline int32_t get_phase_6() const { return ___phase_6; }
	inline int32_t* get_address_of_phase_6() { return &___phase_6; }
	inline void set_phase_6(int32_t value)
	{
		___phase_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TKTOUCH_T2857605003_H
#ifndef TRACKABLEHIT_T1913077329_H
#define TRACKABLEHIT_T1913077329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.TrackableHit
struct  TrackableHit_t1913077329 
{
public:
	union
	{
		struct
		{
			// UnityEngine.Pose GoogleARCore.TrackableHit::<Pose>k__BackingField
			Pose_t545244865  ___U3CPoseU3Ek__BackingField_0;
			// System.Single GoogleARCore.TrackableHit::<Distance>k__BackingField
			float ___U3CDistanceU3Ek__BackingField_1;
			// GoogleARCore.TrackableHitFlags GoogleARCore.TrackableHit::<Flags>k__BackingField
			int32_t ___U3CFlagsU3Ek__BackingField_2;
			// GoogleARCore.Trackable GoogleARCore.TrackableHit::<Trackable>k__BackingField
			Trackable_t2214220236 * ___U3CTrackableU3Ek__BackingField_3;
		};
		uint8_t TrackableHit_t1913077329__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CPoseU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TrackableHit_t1913077329, ___U3CPoseU3Ek__BackingField_0)); }
	inline Pose_t545244865  get_U3CPoseU3Ek__BackingField_0() const { return ___U3CPoseU3Ek__BackingField_0; }
	inline Pose_t545244865 * get_address_of_U3CPoseU3Ek__BackingField_0() { return &___U3CPoseU3Ek__BackingField_0; }
	inline void set_U3CPoseU3Ek__BackingField_0(Pose_t545244865  value)
	{
		___U3CPoseU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CDistanceU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TrackableHit_t1913077329, ___U3CDistanceU3Ek__BackingField_1)); }
	inline float get_U3CDistanceU3Ek__BackingField_1() const { return ___U3CDistanceU3Ek__BackingField_1; }
	inline float* get_address_of_U3CDistanceU3Ek__BackingField_1() { return &___U3CDistanceU3Ek__BackingField_1; }
	inline void set_U3CDistanceU3Ek__BackingField_1(float value)
	{
		___U3CDistanceU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CFlagsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TrackableHit_t1913077329, ___U3CFlagsU3Ek__BackingField_2)); }
	inline int32_t get_U3CFlagsU3Ek__BackingField_2() const { return ___U3CFlagsU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CFlagsU3Ek__BackingField_2() { return &___U3CFlagsU3Ek__BackingField_2; }
	inline void set_U3CFlagsU3Ek__BackingField_2(int32_t value)
	{
		___U3CFlagsU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CTrackableU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TrackableHit_t1913077329, ___U3CTrackableU3Ek__BackingField_3)); }
	inline Trackable_t2214220236 * get_U3CTrackableU3Ek__BackingField_3() const { return ___U3CTrackableU3Ek__BackingField_3; }
	inline Trackable_t2214220236 ** get_address_of_U3CTrackableU3Ek__BackingField_3() { return &___U3CTrackableU3Ek__BackingField_3; }
	inline void set_U3CTrackableU3Ek__BackingField_3(Trackable_t2214220236 * value)
	{
		___U3CTrackableU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrackableU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GoogleARCore.TrackableHit
struct TrackableHit_t1913077329_marshaled_pinvoke
{
	union
	{
		struct
		{
			Pose_t545244865  ___U3CPoseU3Ek__BackingField_0;
			float ___U3CDistanceU3Ek__BackingField_1;
			int32_t ___U3CFlagsU3Ek__BackingField_2;
			Trackable_t2214220236 * ___U3CTrackableU3Ek__BackingField_3;
		};
		uint8_t TrackableHit_t1913077329__padding[1];
	};
};
// Native definition for COM marshalling of GoogleARCore.TrackableHit
struct TrackableHit_t1913077329_marshaled_com
{
	union
	{
		struct
		{
			Pose_t545244865  ___U3CPoseU3Ek__BackingField_0;
			float ___U3CDistanceU3Ek__BackingField_1;
			int32_t ___U3CFlagsU3Ek__BackingField_2;
			Trackable_t2214220236 * ___U3CTrackableU3Ek__BackingField_3;
		};
		uint8_t TrackableHit_t1913077329__padding[1];
	};
};
#endif // TRACKABLEHIT_T1913077329_H
#ifndef DETECTEDPLANE_T4062129378_H
#define DETECTEDPLANE_T4062129378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.DetectedPlane
struct  DetectedPlane_t4062129378  : public Trackable_t2214220236
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DETECTEDPLANE_T4062129378_H
#ifndef CAMERAPERMISSIONREQUESTPROVIDER_T2929024609_H
#define CAMERAPERMISSIONREQUESTPROVIDER_T2929024609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ARPrestoCallbackManager/CameraPermissionRequestProvider
struct  CameraPermissionRequestProvider_t2929024609  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAPERMISSIONREQUESTPROVIDER_T2929024609_H
#ifndef CAMERAPERMISSIONSRESULTCALLBACK_T3686029889_H
#define CAMERAPERMISSIONSRESULTCALLBACK_T3686029889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ARPrestoCallbackManager/CameraPermissionsResultCallback
struct  CameraPermissionsResultCallback_t3686029889  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAPERMISSIONSRESULTCALLBACK_T3686029889_H
#ifndef CHECKAPKAVAILABILITYRESULTCALLBACK_T3176003102_H
#define CHECKAPKAVAILABILITYRESULTCALLBACK_T3176003102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ARPrestoCallbackManager/CheckApkAvailabilityResultCallback
struct  CheckApkAvailabilityResultCallback_t3176003102  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKAPKAVAILABILITYRESULTCALLBACK_T3176003102_H
#ifndef REQUESTAPKINSTALLATIONRESULTCALLBACK_T1625331025_H
#define REQUESTAPKINSTALLATIONRESULTCALLBACK_T1625331025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ARPrestoCallbackManager/RequestApkInstallationResultCallback
struct  RequestApkInstallationResultCallback_t1625331025  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTAPKINSTALLATIONRESULTCALLBACK_T1625331025_H
#ifndef SESSIONCREATIONRESULTCALLBACK_T1143368000_H
#define SESSIONCREATIONRESULTCALLBACK_T1143368000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ARPrestoCallbackManager/SessionCreationResultCallback
struct  SessionCreationResultCallback_t1143368000  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONCREATIONRESULTCALLBACK_T1143368000_H
#ifndef ONBEFORESETCONFIGURATIONCALLBACK_T2381356085_H
#define ONBEFORESETCONFIGURATIONCALLBACK_T2381356085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ExperimentManager/OnBeforeSetConfigurationCallback
struct  OnBeforeSetConfigurationCallback_t2381356085  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONBEFORESETCONFIGURATIONCALLBACK_T2381356085_H
#ifndef TRACKEDPOINT_T724314308_H
#define TRACKEDPOINT_T724314308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.TrackedPoint
struct  TrackedPoint_t724314308  : public FeaturePoint_t2424401674
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKEDPOINT_T724314308_H
#ifndef TRACKEDPLANE_T1078381965_H
#define TRACKEDPLANE_T1078381965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.TrackedPlane
struct  TrackedPlane_t1078381965  : public DetectedPlane_t4062129378
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKEDPLANE_T1078381965_H
#ifndef ONBEFORESETCONFIGURATIONCALLBACK_T2829502455_H
#define ONBEFORESETCONFIGURATIONCALLBACK_T2829502455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ARPrestoCallbackManager/OnBeforeSetConfigurationCallback
struct  OnBeforeSetConfigurationCallback_t2829502455  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONBEFORESETCONFIGURATIONCALLBACK_T2829502455_H
#ifndef EARLYUPDATECALLBACK_T1406590691_H
#define EARLYUPDATECALLBACK_T1406590691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ARPrestoCallbackManager/EarlyUpdateCallback
struct  EarlyUpdateCallback_t1406590691  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EARLYUPDATECALLBACK_T1406590691_H
#ifndef ARCORESESSIONCONFIG_T3885767874_H
#define ARCORESESSIONCONFIG_T3885767874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.ARCoreSessionConfig
struct  ARCoreSessionConfig_t3885767874  : public ScriptableObject_t2528358522
{
public:
	// System.Boolean GoogleARCore.ARCoreSessionConfig::MatchCameraFramerate
	bool ___MatchCameraFramerate_2;
	// GoogleARCore.DetectedPlaneFindingMode GoogleARCore.ARCoreSessionConfig::PlaneFindingMode
	int32_t ___PlaneFindingMode_3;
	// System.Boolean GoogleARCore.ARCoreSessionConfig::EnableLightEstimation
	bool ___EnableLightEstimation_4;
	// System.Boolean GoogleARCore.ARCoreSessionConfig::EnableCloudAnchor
	bool ___EnableCloudAnchor_5;
	// GoogleARCore.AugmentedImageDatabase GoogleARCore.ARCoreSessionConfig::AugmentedImageDatabase
	AugmentedImageDatabase_t1074037598 * ___AugmentedImageDatabase_6;

public:
	inline static int32_t get_offset_of_MatchCameraFramerate_2() { return static_cast<int32_t>(offsetof(ARCoreSessionConfig_t3885767874, ___MatchCameraFramerate_2)); }
	inline bool get_MatchCameraFramerate_2() const { return ___MatchCameraFramerate_2; }
	inline bool* get_address_of_MatchCameraFramerate_2() { return &___MatchCameraFramerate_2; }
	inline void set_MatchCameraFramerate_2(bool value)
	{
		___MatchCameraFramerate_2 = value;
	}

	inline static int32_t get_offset_of_PlaneFindingMode_3() { return static_cast<int32_t>(offsetof(ARCoreSessionConfig_t3885767874, ___PlaneFindingMode_3)); }
	inline int32_t get_PlaneFindingMode_3() const { return ___PlaneFindingMode_3; }
	inline int32_t* get_address_of_PlaneFindingMode_3() { return &___PlaneFindingMode_3; }
	inline void set_PlaneFindingMode_3(int32_t value)
	{
		___PlaneFindingMode_3 = value;
	}

	inline static int32_t get_offset_of_EnableLightEstimation_4() { return static_cast<int32_t>(offsetof(ARCoreSessionConfig_t3885767874, ___EnableLightEstimation_4)); }
	inline bool get_EnableLightEstimation_4() const { return ___EnableLightEstimation_4; }
	inline bool* get_address_of_EnableLightEstimation_4() { return &___EnableLightEstimation_4; }
	inline void set_EnableLightEstimation_4(bool value)
	{
		___EnableLightEstimation_4 = value;
	}

	inline static int32_t get_offset_of_EnableCloudAnchor_5() { return static_cast<int32_t>(offsetof(ARCoreSessionConfig_t3885767874, ___EnableCloudAnchor_5)); }
	inline bool get_EnableCloudAnchor_5() const { return ___EnableCloudAnchor_5; }
	inline bool* get_address_of_EnableCloudAnchor_5() { return &___EnableCloudAnchor_5; }
	inline void set_EnableCloudAnchor_5(bool value)
	{
		___EnableCloudAnchor_5 = value;
	}

	inline static int32_t get_offset_of_AugmentedImageDatabase_6() { return static_cast<int32_t>(offsetof(ARCoreSessionConfig_t3885767874, ___AugmentedImageDatabase_6)); }
	inline AugmentedImageDatabase_t1074037598 * get_AugmentedImageDatabase_6() const { return ___AugmentedImageDatabase_6; }
	inline AugmentedImageDatabase_t1074037598 ** get_address_of_AugmentedImageDatabase_6() { return &___AugmentedImageDatabase_6; }
	inline void set_AugmentedImageDatabase_6(AugmentedImageDatabase_t1074037598 * value)
	{
		___AugmentedImageDatabase_6 = value;
		Il2CppCodeGenWriteBarrier((&___AugmentedImageDatabase_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCORESESSIONCONFIG_T3885767874_H
#ifndef TKANYTOUCHRECOGNIZER_T3651961449_H
#define TKANYTOUCHRECOGNIZER_T3651961449_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TKAnyTouchRecognizer
struct  TKAnyTouchRecognizer_t3651961449  : public TKAbstractGestureRecognizer_t1903772339
{
public:
	// System.Action`1<TKAnyTouchRecognizer> TKAnyTouchRecognizer::onEnteredEvent
	Action_1_t3824429044 * ___onEnteredEvent_10;
	// System.Action`1<TKAnyTouchRecognizer> TKAnyTouchRecognizer::onExitedEvent
	Action_1_t3824429044 * ___onExitedEvent_11;

public:
	inline static int32_t get_offset_of_onEnteredEvent_10() { return static_cast<int32_t>(offsetof(TKAnyTouchRecognizer_t3651961449, ___onEnteredEvent_10)); }
	inline Action_1_t3824429044 * get_onEnteredEvent_10() const { return ___onEnteredEvent_10; }
	inline Action_1_t3824429044 ** get_address_of_onEnteredEvent_10() { return &___onEnteredEvent_10; }
	inline void set_onEnteredEvent_10(Action_1_t3824429044 * value)
	{
		___onEnteredEvent_10 = value;
		Il2CppCodeGenWriteBarrier((&___onEnteredEvent_10), value);
	}

	inline static int32_t get_offset_of_onExitedEvent_11() { return static_cast<int32_t>(offsetof(TKAnyTouchRecognizer_t3651961449, ___onExitedEvent_11)); }
	inline Action_1_t3824429044 * get_onExitedEvent_11() const { return ___onExitedEvent_11; }
	inline Action_1_t3824429044 ** get_address_of_onExitedEvent_11() { return &___onExitedEvent_11; }
	inline void set_onExitedEvent_11(Action_1_t3824429044 * value)
	{
		___onExitedEvent_11 = value;
		Il2CppCodeGenWriteBarrier((&___onExitedEvent_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TKANYTOUCHRECOGNIZER_T3651961449_H
#ifndef AUGMENTEDIMAGEDATABASE_T1074037598_H
#define AUGMENTEDIMAGEDATABASE_T1074037598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.AugmentedImageDatabase
struct  AugmentedImageDatabase_t1074037598  : public ScriptableObject_t2528358522
{
public:
	// System.Collections.Generic.List`1<GoogleARCore.AugmentedImageDatabaseEntry> GoogleARCore.AugmentedImageDatabase::m_Images
	List_1_t647451162 * ___m_Images_2;
	// System.Byte[] GoogleARCore.AugmentedImageDatabase::m_RawData
	ByteU5BU5D_t4116647657* ___m_RawData_3;
	// System.Boolean GoogleARCore.AugmentedImageDatabase::m_IsRawDataDirty
	bool ___m_IsRawDataDirty_4;
	// System.String GoogleARCore.AugmentedImageDatabase::m_CliVersion
	String_t* ___m_CliVersion_5;

public:
	inline static int32_t get_offset_of_m_Images_2() { return static_cast<int32_t>(offsetof(AugmentedImageDatabase_t1074037598, ___m_Images_2)); }
	inline List_1_t647451162 * get_m_Images_2() const { return ___m_Images_2; }
	inline List_1_t647451162 ** get_address_of_m_Images_2() { return &___m_Images_2; }
	inline void set_m_Images_2(List_1_t647451162 * value)
	{
		___m_Images_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Images_2), value);
	}

	inline static int32_t get_offset_of_m_RawData_3() { return static_cast<int32_t>(offsetof(AugmentedImageDatabase_t1074037598, ___m_RawData_3)); }
	inline ByteU5BU5D_t4116647657* get_m_RawData_3() const { return ___m_RawData_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_RawData_3() { return &___m_RawData_3; }
	inline void set_m_RawData_3(ByteU5BU5D_t4116647657* value)
	{
		___m_RawData_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_RawData_3), value);
	}

	inline static int32_t get_offset_of_m_IsRawDataDirty_4() { return static_cast<int32_t>(offsetof(AugmentedImageDatabase_t1074037598, ___m_IsRawDataDirty_4)); }
	inline bool get_m_IsRawDataDirty_4() const { return ___m_IsRawDataDirty_4; }
	inline bool* get_address_of_m_IsRawDataDirty_4() { return &___m_IsRawDataDirty_4; }
	inline void set_m_IsRawDataDirty_4(bool value)
	{
		___m_IsRawDataDirty_4 = value;
	}

	inline static int32_t get_offset_of_m_CliVersion_5() { return static_cast<int32_t>(offsetof(AugmentedImageDatabase_t1074037598, ___m_CliVersion_5)); }
	inline String_t* get_m_CliVersion_5() const { return ___m_CliVersion_5; }
	inline String_t** get_address_of_m_CliVersion_5() { return &___m_CliVersion_5; }
	inline void set_m_CliVersion_5(String_t* value)
	{
		___m_CliVersion_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_CliVersion_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUGMENTEDIMAGEDATABASE_T1074037598_H
#ifndef TKANGLESWIPERECOGNIZER_T3666809813_H
#define TKANGLESWIPERECOGNIZER_T3666809813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TKAngleSwipeRecognizer
struct  TKAngleSwipeRecognizer_t3666809813  : public TKAbstractGestureRecognizer_t1903772339
{
public:
	// System.Action`1<TKAngleSwipeRecognizer> TKAngleSwipeRecognizer::gestureRecognizedEvent
	Action_1_t3839277408 * ___gestureRecognizedEvent_10;
	// System.Collections.Generic.List`1<TKAngleSwipeRecognizer/AngleListener> TKAngleSwipeRecognizer::_angleRecognizedEvents
	List_1_t1248166469 * ____angleRecognizedEvents_11;
	// System.Single TKAngleSwipeRecognizer::timeToSwipe
	float ___timeToSwipe_12;
	// System.Single TKAngleSwipeRecognizer::<swipeVelocity>k__BackingField
	float ___U3CswipeVelocityU3Ek__BackingField_13;
	// System.Single TKAngleSwipeRecognizer::<swipeAngle>k__BackingField
	float ___U3CswipeAngleU3Ek__BackingField_14;
	// UnityEngine.Vector2 TKAngleSwipeRecognizer::<swipeVelVector>k__BackingField
	Vector2_t2156229523  ___U3CswipeVelVectorU3Ek__BackingField_15;
	// System.Single TKAngleSwipeRecognizer::minimumDistance
	float ___minimumDistance_16;
	// UnityEngine.Vector2 TKAngleSwipeRecognizer::_startPoint
	Vector2_t2156229523  ____startPoint_17;
	// UnityEngine.Vector2 TKAngleSwipeRecognizer::_endPoint
	Vector2_t2156229523  ____endPoint_18;
	// System.Single TKAngleSwipeRecognizer::_startTime
	float ____startTime_19;

public:
	inline static int32_t get_offset_of_gestureRecognizedEvent_10() { return static_cast<int32_t>(offsetof(TKAngleSwipeRecognizer_t3666809813, ___gestureRecognizedEvent_10)); }
	inline Action_1_t3839277408 * get_gestureRecognizedEvent_10() const { return ___gestureRecognizedEvent_10; }
	inline Action_1_t3839277408 ** get_address_of_gestureRecognizedEvent_10() { return &___gestureRecognizedEvent_10; }
	inline void set_gestureRecognizedEvent_10(Action_1_t3839277408 * value)
	{
		___gestureRecognizedEvent_10 = value;
		Il2CppCodeGenWriteBarrier((&___gestureRecognizedEvent_10), value);
	}

	inline static int32_t get_offset_of__angleRecognizedEvents_11() { return static_cast<int32_t>(offsetof(TKAngleSwipeRecognizer_t3666809813, ____angleRecognizedEvents_11)); }
	inline List_1_t1248166469 * get__angleRecognizedEvents_11() const { return ____angleRecognizedEvents_11; }
	inline List_1_t1248166469 ** get_address_of__angleRecognizedEvents_11() { return &____angleRecognizedEvents_11; }
	inline void set__angleRecognizedEvents_11(List_1_t1248166469 * value)
	{
		____angleRecognizedEvents_11 = value;
		Il2CppCodeGenWriteBarrier((&____angleRecognizedEvents_11), value);
	}

	inline static int32_t get_offset_of_timeToSwipe_12() { return static_cast<int32_t>(offsetof(TKAngleSwipeRecognizer_t3666809813, ___timeToSwipe_12)); }
	inline float get_timeToSwipe_12() const { return ___timeToSwipe_12; }
	inline float* get_address_of_timeToSwipe_12() { return &___timeToSwipe_12; }
	inline void set_timeToSwipe_12(float value)
	{
		___timeToSwipe_12 = value;
	}

	inline static int32_t get_offset_of_U3CswipeVelocityU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(TKAngleSwipeRecognizer_t3666809813, ___U3CswipeVelocityU3Ek__BackingField_13)); }
	inline float get_U3CswipeVelocityU3Ek__BackingField_13() const { return ___U3CswipeVelocityU3Ek__BackingField_13; }
	inline float* get_address_of_U3CswipeVelocityU3Ek__BackingField_13() { return &___U3CswipeVelocityU3Ek__BackingField_13; }
	inline void set_U3CswipeVelocityU3Ek__BackingField_13(float value)
	{
		___U3CswipeVelocityU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CswipeAngleU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(TKAngleSwipeRecognizer_t3666809813, ___U3CswipeAngleU3Ek__BackingField_14)); }
	inline float get_U3CswipeAngleU3Ek__BackingField_14() const { return ___U3CswipeAngleU3Ek__BackingField_14; }
	inline float* get_address_of_U3CswipeAngleU3Ek__BackingField_14() { return &___U3CswipeAngleU3Ek__BackingField_14; }
	inline void set_U3CswipeAngleU3Ek__BackingField_14(float value)
	{
		___U3CswipeAngleU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CswipeVelVectorU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(TKAngleSwipeRecognizer_t3666809813, ___U3CswipeVelVectorU3Ek__BackingField_15)); }
	inline Vector2_t2156229523  get_U3CswipeVelVectorU3Ek__BackingField_15() const { return ___U3CswipeVelVectorU3Ek__BackingField_15; }
	inline Vector2_t2156229523 * get_address_of_U3CswipeVelVectorU3Ek__BackingField_15() { return &___U3CswipeVelVectorU3Ek__BackingField_15; }
	inline void set_U3CswipeVelVectorU3Ek__BackingField_15(Vector2_t2156229523  value)
	{
		___U3CswipeVelVectorU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_minimumDistance_16() { return static_cast<int32_t>(offsetof(TKAngleSwipeRecognizer_t3666809813, ___minimumDistance_16)); }
	inline float get_minimumDistance_16() const { return ___minimumDistance_16; }
	inline float* get_address_of_minimumDistance_16() { return &___minimumDistance_16; }
	inline void set_minimumDistance_16(float value)
	{
		___minimumDistance_16 = value;
	}

	inline static int32_t get_offset_of__startPoint_17() { return static_cast<int32_t>(offsetof(TKAngleSwipeRecognizer_t3666809813, ____startPoint_17)); }
	inline Vector2_t2156229523  get__startPoint_17() const { return ____startPoint_17; }
	inline Vector2_t2156229523 * get_address_of__startPoint_17() { return &____startPoint_17; }
	inline void set__startPoint_17(Vector2_t2156229523  value)
	{
		____startPoint_17 = value;
	}

	inline static int32_t get_offset_of__endPoint_18() { return static_cast<int32_t>(offsetof(TKAngleSwipeRecognizer_t3666809813, ____endPoint_18)); }
	inline Vector2_t2156229523  get__endPoint_18() const { return ____endPoint_18; }
	inline Vector2_t2156229523 * get_address_of__endPoint_18() { return &____endPoint_18; }
	inline void set__endPoint_18(Vector2_t2156229523  value)
	{
		____endPoint_18 = value;
	}

	inline static int32_t get_offset_of__startTime_19() { return static_cast<int32_t>(offsetof(TKAngleSwipeRecognizer_t3666809813, ____startTime_19)); }
	inline float get__startTime_19() const { return ____startTime_19; }
	inline float* get_address_of__startTime_19() { return &____startTime_19; }
	inline void set__startTime_19(float value)
	{
		____startTime_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TKANGLESWIPERECOGNIZER_T3666809813_H
#ifndef ONBEFORERESUMESESSIONCALLBACK_T1391226892_H
#define ONBEFORERESUMESESSIONCALLBACK_T1391226892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ARPrestoCallbackManager/OnBeforeResumeSessionCallback
struct  OnBeforeResumeSessionCallback_t1391226892  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONBEFORERESUMESESSIONCALLBACK_T1391226892_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef ENVIRONMENTALLIGHT_T2927854189_H
#define ENVIRONMENTALLIGHT_T2927854189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.EnvironmentalLight
struct  EnvironmentalLight_t2927854189  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENVIRONMENTALLIGHT_T2927854189_H
#ifndef ARCORESESSION_T3898212540_H
#define ARCORESESSION_T3898212540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.ARCoreSession
struct  ARCoreSession_t3898212540  : public MonoBehaviour_t3962482529
{
public:
	// GoogleARCore.ARCoreSessionConfig GoogleARCore.ARCoreSession::SessionConfig
	ARCoreSessionConfig_t3885767874 * ___SessionConfig_2;

public:
	inline static int32_t get_offset_of_SessionConfig_2() { return static_cast<int32_t>(offsetof(ARCoreSession_t3898212540, ___SessionConfig_2)); }
	inline ARCoreSessionConfig_t3885767874 * get_SessionConfig_2() const { return ___SessionConfig_2; }
	inline ARCoreSessionConfig_t3885767874 ** get_address_of_SessionConfig_2() { return &___SessionConfig_2; }
	inline void set_SessionConfig_2(ARCoreSessionConfig_t3885767874 * value)
	{
		___SessionConfig_2 = value;
		Il2CppCodeGenWriteBarrier((&___SessionConfig_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCORESESSION_T3898212540_H
#ifndef XPANCHOR_T812171902_H
#define XPANCHOR_T812171902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.CrossPlatform.XPAnchor
struct  XPAnchor_t812171902  : public MonoBehaviour_t3962482529
{
public:
	// GoogleARCore.CrossPlatform.XPTrackingState GoogleARCore.CrossPlatform.XPAnchor::m_LastFrameTrackingState
	int32_t ___m_LastFrameTrackingState_3;
	// System.Boolean GoogleARCore.CrossPlatform.XPAnchor::m_IsSessionDestroyed
	bool ___m_IsSessionDestroyed_4;
	// System.String GoogleARCore.CrossPlatform.XPAnchor::<CloudId>k__BackingField
	String_t* ___U3CCloudIdU3Ek__BackingField_5;
	// GoogleARCoreInternal.NativeSession GoogleARCore.CrossPlatform.XPAnchor::<m_NativeSession>k__BackingField
	NativeSession_t1550589577 * ___U3Cm_NativeSessionU3Ek__BackingField_6;
	// System.IntPtr GoogleARCore.CrossPlatform.XPAnchor::<m_NativeHandle>k__BackingField
	intptr_t ___U3Cm_NativeHandleU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_m_LastFrameTrackingState_3() { return static_cast<int32_t>(offsetof(XPAnchor_t812171902, ___m_LastFrameTrackingState_3)); }
	inline int32_t get_m_LastFrameTrackingState_3() const { return ___m_LastFrameTrackingState_3; }
	inline int32_t* get_address_of_m_LastFrameTrackingState_3() { return &___m_LastFrameTrackingState_3; }
	inline void set_m_LastFrameTrackingState_3(int32_t value)
	{
		___m_LastFrameTrackingState_3 = value;
	}

	inline static int32_t get_offset_of_m_IsSessionDestroyed_4() { return static_cast<int32_t>(offsetof(XPAnchor_t812171902, ___m_IsSessionDestroyed_4)); }
	inline bool get_m_IsSessionDestroyed_4() const { return ___m_IsSessionDestroyed_4; }
	inline bool* get_address_of_m_IsSessionDestroyed_4() { return &___m_IsSessionDestroyed_4; }
	inline void set_m_IsSessionDestroyed_4(bool value)
	{
		___m_IsSessionDestroyed_4 = value;
	}

	inline static int32_t get_offset_of_U3CCloudIdU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(XPAnchor_t812171902, ___U3CCloudIdU3Ek__BackingField_5)); }
	inline String_t* get_U3CCloudIdU3Ek__BackingField_5() const { return ___U3CCloudIdU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CCloudIdU3Ek__BackingField_5() { return &___U3CCloudIdU3Ek__BackingField_5; }
	inline void set_U3CCloudIdU3Ek__BackingField_5(String_t* value)
	{
		___U3CCloudIdU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCloudIdU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3Cm_NativeSessionU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(XPAnchor_t812171902, ___U3Cm_NativeSessionU3Ek__BackingField_6)); }
	inline NativeSession_t1550589577 * get_U3Cm_NativeSessionU3Ek__BackingField_6() const { return ___U3Cm_NativeSessionU3Ek__BackingField_6; }
	inline NativeSession_t1550589577 ** get_address_of_U3Cm_NativeSessionU3Ek__BackingField_6() { return &___U3Cm_NativeSessionU3Ek__BackingField_6; }
	inline void set_U3Cm_NativeSessionU3Ek__BackingField_6(NativeSession_t1550589577 * value)
	{
		___U3Cm_NativeSessionU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cm_NativeSessionU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3Cm_NativeHandleU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(XPAnchor_t812171902, ___U3Cm_NativeHandleU3Ek__BackingField_7)); }
	inline intptr_t get_U3Cm_NativeHandleU3Ek__BackingField_7() const { return ___U3Cm_NativeHandleU3Ek__BackingField_7; }
	inline intptr_t* get_address_of_U3Cm_NativeHandleU3Ek__BackingField_7() { return &___U3Cm_NativeHandleU3Ek__BackingField_7; }
	inline void set_U3Cm_NativeHandleU3Ek__BackingField_7(intptr_t value)
	{
		___U3Cm_NativeHandleU3Ek__BackingField_7 = value;
	}
};

struct XPAnchor_t812171902_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.IntPtr,GoogleARCore.CrossPlatform.XPAnchor> GoogleARCore.CrossPlatform.XPAnchor::s_AnchorDict
	Dictionary_2_t3329004917 * ___s_AnchorDict_2;

public:
	inline static int32_t get_offset_of_s_AnchorDict_2() { return static_cast<int32_t>(offsetof(XPAnchor_t812171902_StaticFields, ___s_AnchorDict_2)); }
	inline Dictionary_2_t3329004917 * get_s_AnchorDict_2() const { return ___s_AnchorDict_2; }
	inline Dictionary_2_t3329004917 ** get_address_of_s_AnchorDict_2() { return &___s_AnchorDict_2; }
	inline void set_s_AnchorDict_2(Dictionary_2_t3329004917 * value)
	{
		___s_AnchorDict_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_AnchorDict_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPANCHOR_T812171902_H
#ifndef ARCOREBACKGROUNDRENDERER_T100526854_H
#define ARCOREBACKGROUNDRENDERER_T100526854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.ARCoreBackgroundRenderer
struct  ARCoreBackgroundRenderer_t100526854  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Material GoogleARCore.ARCoreBackgroundRenderer::BackgroundMaterial
	Material_t340375123 * ___BackgroundMaterial_2;
	// UnityEngine.Camera GoogleARCore.ARCoreBackgroundRenderer::m_Camera
	Camera_t4157153871 * ___m_Camera_3;
	// UnityEngine.XR.ARBackgroundRenderer GoogleARCore.ARCoreBackgroundRenderer::m_BackgroundRenderer
	ARBackgroundRenderer_t852496440 * ___m_BackgroundRenderer_4;

public:
	inline static int32_t get_offset_of_BackgroundMaterial_2() { return static_cast<int32_t>(offsetof(ARCoreBackgroundRenderer_t100526854, ___BackgroundMaterial_2)); }
	inline Material_t340375123 * get_BackgroundMaterial_2() const { return ___BackgroundMaterial_2; }
	inline Material_t340375123 ** get_address_of_BackgroundMaterial_2() { return &___BackgroundMaterial_2; }
	inline void set_BackgroundMaterial_2(Material_t340375123 * value)
	{
		___BackgroundMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___BackgroundMaterial_2), value);
	}

	inline static int32_t get_offset_of_m_Camera_3() { return static_cast<int32_t>(offsetof(ARCoreBackgroundRenderer_t100526854, ___m_Camera_3)); }
	inline Camera_t4157153871 * get_m_Camera_3() const { return ___m_Camera_3; }
	inline Camera_t4157153871 ** get_address_of_m_Camera_3() { return &___m_Camera_3; }
	inline void set_m_Camera_3(Camera_t4157153871 * value)
	{
		___m_Camera_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_3), value);
	}

	inline static int32_t get_offset_of_m_BackgroundRenderer_4() { return static_cast<int32_t>(offsetof(ARCoreBackgroundRenderer_t100526854, ___m_BackgroundRenderer_4)); }
	inline ARBackgroundRenderer_t852496440 * get_m_BackgroundRenderer_4() const { return ___m_BackgroundRenderer_4; }
	inline ARBackgroundRenderer_t852496440 ** get_address_of_m_BackgroundRenderer_4() { return &___m_BackgroundRenderer_4; }
	inline void set_m_BackgroundRenderer_4(ARBackgroundRenderer_t852496440 * value)
	{
		___m_BackgroundRenderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_BackgroundRenderer_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCOREBACKGROUNDRENDERER_T100526854_H
#ifndef USAGESAMPLE_T1768752096_H
#define USAGESAMPLE_T1768752096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UsageSample
struct  UsageSample_t1768752096  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button UsageSample::_button
	Button_t4055032469 * ____button_2;
	// UnityEngine.UI.Text UsageSample::_text
	Text_t1901882714 * ____text_3;

public:
	inline static int32_t get_offset_of__button_2() { return static_cast<int32_t>(offsetof(UsageSample_t1768752096, ____button_2)); }
	inline Button_t4055032469 * get__button_2() const { return ____button_2; }
	inline Button_t4055032469 ** get_address_of__button_2() { return &____button_2; }
	inline void set__button_2(Button_t4055032469 * value)
	{
		____button_2 = value;
		Il2CppCodeGenWriteBarrier((&____button_2), value);
	}

	inline static int32_t get_offset_of__text_3() { return static_cast<int32_t>(offsetof(UsageSample_t1768752096, ____text_3)); }
	inline Text_t1901882714 * get__text_3() const { return ____text_3; }
	inline Text_t1901882714 ** get_address_of__text_3() { return &____text_3; }
	inline void set__text_3(Text_t1901882714 * value)
	{
		____text_3 = value;
		Il2CppCodeGenWriteBarrier((&____text_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USAGESAMPLE_T1768752096_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (ExternApi_t4200038123)+ sizeof (RuntimeObject), sizeof(ExternApi_t4200038123 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { sizeof (PoseApi_t3025584507), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2401[1] = 
{
	PoseApi_t3025584507::get_offset_of_m_NativeSession_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (ExternApi_t3853040500)+ sizeof (RuntimeObject), sizeof(ExternApi_t3853040500 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { sizeof (SessionApi_t3641277092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2403[1] = 
{
	SessionApi_t3641277092::get_offset_of_m_NativeSession_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (ExternApi_t1767614075)+ sizeof (RuntimeObject), sizeof(ExternApi_t1767614075 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { sizeof (SessionConfigApi_t3119440216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2405[1] = 
{
	SessionConfigApi_t3119440216::get_offset_of_m_NativeSession_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { sizeof (ExternApi_t1764421814)+ sizeof (RuntimeObject), sizeof(ExternApi_t1764421814 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { sizeof (TrackableApi_t1100424269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2407[1] = 
{
	TrackableApi_t1100424269::get_offset_of_m_NativeSession_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { sizeof (ExternApi_t574091193)+ sizeof (RuntimeObject), sizeof(ExternApi_t574091193 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { sizeof (TrackableListApi_t1931239134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2409[1] = 
{
	TrackableListApi_t1931239134::get_offset_of_m_NativeSession_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { sizeof (ExternApi_t4031421663)+ sizeof (RuntimeObject), sizeof(ExternApi_t4031421663 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (ApkAvailabilityStatus_t2902724566)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2411[8] = 
{
	ApkAvailabilityStatus_t2902724566::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (ApkInstallationStatus_t2302886207)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2412[8] = 
{
	ApkInstallationStatus_t2302886207::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { sizeof (ARCoreBackgroundRenderer_t100526854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2413[3] = 
{
	ARCoreBackgroundRenderer_t100526854::get_offset_of_BackgroundMaterial_2(),
	ARCoreBackgroundRenderer_t100526854::get_offset_of_m_Camera_3(),
	ARCoreBackgroundRenderer_t100526854::get_offset_of_m_BackgroundRenderer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (ARCoreSession_t3898212540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2414[1] = 
{
	ARCoreSession_t3898212540::get_offset_of_SessionConfig_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { sizeof (ARCoreSessionConfig_t3885767874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2415[5] = 
{
	ARCoreSessionConfig_t3885767874::get_offset_of_MatchCameraFramerate_2(),
	ARCoreSessionConfig_t3885767874::get_offset_of_PlaneFindingMode_3(),
	ARCoreSessionConfig_t3885767874::get_offset_of_EnableLightEstimation_4(),
	ARCoreSessionConfig_t3885767874::get_offset_of_EnableCloudAnchor_5(),
	ARCoreSessionConfig_t3885767874::get_offset_of_AugmentedImageDatabase_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2416[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2417[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2418[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (AsyncTask_t419677264), -1, sizeof(AsyncTask_t419677264_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2419[2] = 
{
	AsyncTask_t419677264_StaticFields::get_offset_of_s_UpdateActionQueue_0(),
	AsyncTask_t419677264_StaticFields::get_offset_of_s_LockObject_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (AugmentedImage_t279011686), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (AugmentedImageDatabase_t1074037598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2421[4] = 
{
	AugmentedImageDatabase_t1074037598::get_offset_of_m_Images_2(),
	AugmentedImageDatabase_t1074037598::get_offset_of_m_RawData_3(),
	AugmentedImageDatabase_t1074037598::get_offset_of_m_IsRawDataDirty_4(),
	AugmentedImageDatabase_t1074037598::get_offset_of_m_CliVersion_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { sizeof (AugmentedImageDatabaseEntry_t3470343716)+ sizeof (RuntimeObject), sizeof(AugmentedImageDatabaseEntry_t3470343716_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2422[4] = 
{
	AugmentedImageDatabaseEntry_t3470343716::get_offset_of_Name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AugmentedImageDatabaseEntry_t3470343716::get_offset_of_Width_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AugmentedImageDatabaseEntry_t3470343716::get_offset_of_Quality_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AugmentedImageDatabaseEntry_t3470343716::get_offset_of_TextureGUID_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { sizeof (CameraImageBytes_t853625235)+ sizeof (RuntimeObject), sizeof(CameraImageBytes_t853625235_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2423[10] = 
{
	CameraImageBytes_t853625235::get_offset_of_m_ImageHandle_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraImageBytes_t853625235::get_offset_of_U3CIsAvailableU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraImageBytes_t853625235::get_offset_of_U3CWidthU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraImageBytes_t853625235::get_offset_of_U3CHeightU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraImageBytes_t853625235::get_offset_of_U3CYU3Ek__BackingField_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraImageBytes_t853625235::get_offset_of_U3CUU3Ek__BackingField_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraImageBytes_t853625235::get_offset_of_U3CVU3Ek__BackingField_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraImageBytes_t853625235::get_offset_of_U3CYRowStrideU3Ek__BackingField_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraImageBytes_t853625235::get_offset_of_U3CUVPixelStrideU3Ek__BackingField_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraImageBytes_t853625235::get_offset_of_U3CUVRowStrideU3Ek__BackingField_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { sizeof (CameraIntrinsics_t3368130170)+ sizeof (RuntimeObject), sizeof(CameraIntrinsics_t3368130170 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2424[3] = 
{
	CameraIntrinsics_t3368130170::get_offset_of_FocalLength_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraIntrinsics_t3368130170::get_offset_of_PrincipalPoint_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraIntrinsics_t3368130170::get_offset_of_ImageDimensions_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { sizeof (CameraMetadataTag_t2983628881)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2425[127] = 
{
	CameraMetadataTag_t2983628881::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { sizeof (CameraMetadataValue_t1936134915)+ sizeof (RuntimeObject), sizeof(CameraMetadataValue_t1936134915 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2426[7] = 
{
	CameraMetadataValue_t1936134915::get_offset_of_m_Type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraMetadataValue_t1936134915::get_offset_of_m_ByteValue_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraMetadataValue_t1936134915::get_offset_of_m_IntValue_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraMetadataValue_t1936134915::get_offset_of_m_LongValue_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraMetadataValue_t1936134915::get_offset_of_m_FloatValue_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraMetadataValue_t1936134915::get_offset_of_m_DoubleValue_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraMetadataValue_t1936134915::get_offset_of_m_RationalValue_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { sizeof (CameraMetadataRational_t1052259197)+ sizeof (RuntimeObject), sizeof(CameraMetadataRational_t1052259197 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2427[2] = 
{
	CameraMetadataRational_t1052259197::get_offset_of_Numerator_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraMetadataRational_t1052259197::get_offset_of_Denominator_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { sizeof (CloudAnchorResult_t3447063407)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2428[2] = 
{
	CloudAnchorResult_t3447063407::get_offset_of_Response_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CloudAnchorResult_t3447063407::get_offset_of_Anchor_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { sizeof (CloudServiceResponse_t2141656258)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2429[13] = 
{
	CloudServiceResponse_t2141656258::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { sizeof (TrackedPlane_t1078381965), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { sizeof (TrackedPoint_t724314308), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (TrackedPointOrientationMode_t3853242691)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2432[3] = 
{
	TrackedPointOrientationMode_t3853242691::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { sizeof (DetectedPlane_t4062129378), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { sizeof (DetectedPlaneFindingMode_t2176733542)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2434[5] = 
{
	DetectedPlaneFindingMode_t2176733542::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (DisplayUvCoords_t972795656)+ sizeof (RuntimeObject), sizeof(DisplayUvCoords_t972795656 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2435[4] = 
{
	DisplayUvCoords_t972795656::get_offset_of_TopLeft_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DisplayUvCoords_t972795656::get_offset_of_TopRight_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DisplayUvCoords_t972795656::get_offset_of_BottomLeft_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DisplayUvCoords_t972795656::get_offset_of_BottomRight_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { sizeof (EnvironmentalLight_t2927854189), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { sizeof (ExperimentBase_t510909049), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (ExperimentManager_t29149108), -1, sizeof(ExperimentManager_t29149108_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2438[5] = 
{
	ExperimentManager_t29149108_StaticFields::get_offset_of_s_Instance_0(),
	ExperimentManager_t29149108::get_offset_of_m_Experiments_1(),
	ExperimentManager_t29149108::get_offset_of_U3CIsSessionExperimentalU3Ek__BackingField_2(),
	ExperimentManager_t29149108_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
	ExperimentManager_t29149108_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { sizeof (OnBeforeSetConfigurationCallback_t2381356085), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (FeaturePoint_t2424401674), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { sizeof (FeaturePointOrientationMode_t4095457472)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2441[3] = 
{
	FeaturePointOrientationMode_t4095457472::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { sizeof (Frame_t2961216902), -1, sizeof(Frame_t2961216902_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2442[1] = 
{
	Frame_t2961216902_StaticFields::get_offset_of_s_TmpTrackableHitList_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { sizeof (CameraMetadata_t1438220491), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2444 = { sizeof (PointCloud_t1493100780), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2445 = { sizeof (CameraImage_t3078682607), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2446 = { sizeof (LightEstimate_t1115373647)+ sizeof (RuntimeObject), sizeof(LightEstimate_t1115373647 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2446[3] = 
{
	LightEstimate_t1115373647::get_offset_of_U3CStateU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LightEstimate_t1115373647::get_offset_of_U3CPixelIntensityU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LightEstimate_t1115373647::get_offset_of_U3CColorCorrectionU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2447 = { sizeof (LightEstimateState_t4275751054)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2447[3] = 
{
	LightEstimateState_t4275751054::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2448 = { sizeof (ARCoreAndroidLifecycleManager_t1403231889), -1, sizeof(ARCoreAndroidLifecycleManager_t1403231889_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2448[8] = 
{
	ARCoreAndroidLifecycleManager_t1403231889_StaticFields::get_offset_of_s_Instance_0(),
	ARCoreAndroidLifecycleManager_t1403231889::get_offset_of_m_CachedConfig_1(),
	ARCoreAndroidLifecycleManager_t1403231889::get_offset_of_EarlyUpdate_2(),
	ARCoreAndroidLifecycleManager_t1403231889::get_offset_of_U3CSessionStatusU3Ek__BackingField_3(),
	ARCoreAndroidLifecycleManager_t1403231889::get_offset_of_U3CSessionComponentU3Ek__BackingField_4(),
	ARCoreAndroidLifecycleManager_t1403231889::get_offset_of_U3CNativeSessionU3Ek__BackingField_5(),
	ARCoreAndroidLifecycleManager_t1403231889::get_offset_of_U3CBackgroundTextureU3Ek__BackingField_6(),
	ARCoreAndroidLifecycleManager_t1403231889_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2449 = { sizeof (ExternApi_t713910015)+ sizeof (RuntimeObject), sizeof(ExternApi_t713910015 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2450 = { sizeof (ARCoreIOSLifecycleManager_t2269316356), -1, sizeof(ARCoreIOSLifecycleManager_t2269316356_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2450[11] = 
{
	0,
	ARCoreIOSLifecycleManager_t2269316356_StaticFields::get_offset_of_s_Instance_1(),
	ARCoreIOSLifecycleManager_t2269316356::get_offset_of_m_SessionHandle_2(),
	ARCoreIOSLifecycleManager_t2269316356::get_offset_of_m_CloudServicesApiKey_3(),
	ARCoreIOSLifecycleManager_t2269316356::get_offset_of_m_RealArKitSessionHandle_4(),
	ARCoreIOSLifecycleManager_t2269316356::get_offset_of_m_FrameHandle_5(),
	ARCoreIOSLifecycleManager_t2269316356::get_offset_of_m_SessionEnabled_6(),
	ARCoreIOSLifecycleManager_t2269316356::get_offset_of_EarlyUpdate_7(),
	ARCoreIOSLifecycleManager_t2269316356::get_offset_of_U3CSessionStatusU3Ek__BackingField_8(),
	ARCoreIOSLifecycleManager_t2269316356::get_offset_of_U3CSessionComponentU3Ek__BackingField_9(),
	ARCoreIOSLifecycleManager_t2269316356::get_offset_of_U3CNativeSessionU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2451 = { sizeof (ExternApi_t3785444511)+ sizeof (RuntimeObject), sizeof(ExternApi_t3785444511 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2452 = { sizeof (ARPrestoCallbackManager_t2442480432), -1, sizeof(ARPrestoCallbackManager_t2442480432_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2452[11] = 
{
	ARPrestoCallbackManager_t2442480432_StaticFields::get_offset_of_s_Instance_0(),
	ARPrestoCallbackManager_t2442480432::get_offset_of_m_CheckApkAvailabilityResultCallback_1(),
	ARPrestoCallbackManager_t2442480432::get_offset_of_m_RequestApkInstallationResultCallback_2(),
	ARPrestoCallbackManager_t2442480432::get_offset_of_m_RequestCameraPermissionCallback_3(),
	ARPrestoCallbackManager_t2442480432::get_offset_of_m_EarlyUpdateCallback_4(),
	ARPrestoCallbackManager_t2442480432::get_offset_of_m_OnBeforeSetConfigurationCallback_5(),
	ARPrestoCallbackManager_t2442480432::get_offset_of_m_OnBeforeResumeSessionCallback_6(),
	ARPrestoCallbackManager_t2442480432::get_offset_of_m_PendingAvailabilityCheckCallbacks_7(),
	ARPrestoCallbackManager_t2442480432::get_offset_of_m_PendingInstallationRequestCallbacks_8(),
	ARPrestoCallbackManager_t2442480432::get_offset_of_EarlyUpdate_9(),
	ARPrestoCallbackManager_t2442480432::get_offset_of_BeforeResumeSession_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2453 = { sizeof (EarlyUpdateCallback_t1406590691), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2454 = { sizeof (OnBeforeSetConfigurationCallback_t2829502455), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2455 = { sizeof (OnBeforeResumeSessionCallback_t1391226892), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2456 = { sizeof (CameraPermissionRequestProvider_t2929024609), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2457 = { sizeof (CameraPermissionsResultCallback_t3686029889), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2458 = { sizeof (CheckApkAvailabilityResultCallback_t3176003102), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2459 = { sizeof (RequestApkInstallationResultCallback_t1625331025), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2460 = { sizeof (SessionCreationResultCallback_t1143368000), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2461 = { sizeof (ExternApi_t1655458871)+ sizeof (RuntimeObject), sizeof(ExternApi_t1655458871 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2462 = { sizeof (U3C_RequestCameraPermissionU3Ec__AnonStorey0_t3100848547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2462[2] = 
{
	U3C_RequestCameraPermissionU3Ec__AnonStorey0_t3100848547::get_offset_of_onComplete_0(),
	U3C_RequestCameraPermissionU3Ec__AnonStorey0_t3100848547::get_offset_of_context_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2463 = { sizeof (CloudServiceManager_t1936018857), -1, sizeof(CloudServiceManager_t1936018857_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2463[3] = 
{
	CloudServiceManager_t1936018857_StaticFields::get_offset_of_s_Instance_0(),
	CloudServiceManager_t1936018857::get_offset_of_m_CloudAnchorRequests_1(),
	CloudServiceManager_t1936018857_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2464 = { sizeof (ExternApi_t1122552781)+ sizeof (RuntimeObject), sizeof(ExternApi_t1122552781 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2465 = { sizeof (CloudAnchorRequest_t2589044226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2465[4] = 
{
	CloudAnchorRequest_t2589044226::get_offset_of_IsComplete_0(),
	CloudAnchorRequest_t2589044226::get_offset_of_NativeSession_1(),
	CloudAnchorRequest_t2589044226::get_offset_of_AnchorHandle_2(),
	CloudAnchorRequest_t2589044226::get_offset_of_OnTaskComplete_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2466 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2467 = { sizeof (LifecycleManager_t2413967345), -1, sizeof(LifecycleManager_t2413967345_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2467[1] = 
{
	LifecycleManager_t2413967345_StaticFields::get_offset_of_s_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2468 = { sizeof (TrackableManager_t1722633017), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2468[7] = 
{
	TrackableManager_t1722633017::get_offset_of_m_TrackableDict_0(),
	TrackableManager_t1722633017::get_offset_of_m_NativeSession_1(),
	TrackableManager_t1722633017::get_offset_of_m_LastUpdateFrame_2(),
	TrackableManager_t1722633017::get_offset_of_m_NewTrackables_3(),
	TrackableManager_t1722633017::get_offset_of_m_AllTrackables_4(),
	TrackableManager_t1722633017::get_offset_of_m_UpdatedTrackables_5(),
	TrackableManager_t1722633017::get_offset_of_m_OldTrackables_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2469 = { sizeof (PointCloudPoint_t4057930723)+ sizeof (RuntimeObject), sizeof(PointCloudPoint_t4057930723 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2469[4] = 
{
	PointCloudPoint_t4057930723::get_offset_of_X_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PointCloudPoint_t4057930723::get_offset_of_Y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PointCloudPoint_t4057930723::get_offset_of_Z_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PointCloudPoint_t4057930723::get_offset_of_Confidence_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2470 = { sizeof (Session_t56890619), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2471 = { sizeof (SessionStatus_t551112162)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2471[10] = 
{
	SessionStatus_t551112162::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2472 = { sizeof (SessionStatusExtensions_t2174783218), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2472[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2473 = { sizeof (Trackable_t2214220236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2473[3] = 
{
	Trackable_t2214220236::get_offset_of_m_TrackableNativeHandle_0(),
	Trackable_t2214220236::get_offset_of_m_NativeSession_1(),
	Trackable_t2214220236::get_offset_of_m_IsSessionDestroyed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2474 = { sizeof (TrackableHit_t1913077329)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2474[4] = 
{
	TrackableHit_t1913077329::get_offset_of_U3CPoseU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackableHit_t1913077329::get_offset_of_U3CDistanceU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackableHit_t1913077329::get_offset_of_U3CFlagsU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackableHit_t1913077329::get_offset_of_U3CTrackableU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2475 = { sizeof (TrackableHitFlags_t1358952518)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2475[7] = 
{
	TrackableHitFlags_t1358952518::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2476 = { sizeof (TrackableQueryFilter_t2799753840)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2476[4] = 
{
	TrackableQueryFilter_t2799753840::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2477 = { sizeof (TrackingState_t1099935182)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2477[4] = 
{
	TrackingState_t1099935182::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2478 = { sizeof (ARCoreProjectSettings_t3272920691), -1, sizeof(ARCoreProjectSettings_t3272920691_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2478[8] = 
{
	ARCoreProjectSettings_t3272920691::get_offset_of_Version_0(),
	ARCoreProjectSettings_t3272920691::get_offset_of_IsARCoreRequired_1(),
	ARCoreProjectSettings_t3272920691::get_offset_of_IsInstantPreviewEnabled_2(),
	ARCoreProjectSettings_t3272920691::get_offset_of_CloudServicesApiKey_3(),
	ARCoreProjectSettings_t3272920691::get_offset_of_IosCloudServicesApiKey_4(),
	0,
	0,
	ARCoreProjectSettings_t3272920691_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2479 = { sizeof (ARDebug_t3607742339), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2480 = { sizeof (ConversionHelper_t900371905), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2481 = { sizeof (DllImportNoop_t2428006201), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2482 = { sizeof (IntPtrEqualityComparer_t902448827), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2483 = { sizeof (MarshalingHelper_t999858804), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2484 = { sizeof (ShellHelper_t1127567194), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2485 = { sizeof (U3CRunCommandU3Ec__AnonStorey0_t2963713103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2485[2] = 
{
	U3CRunCommandU3Ec__AnonStorey0_t2963713103::get_offset_of_outputBuilder_0(),
	U3CRunCommandU3Ec__AnonStorey0_t2963713103::get_offset_of_errorBuilder_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2486 = { sizeof (XPAnchor_t812171902), -1, sizeof(XPAnchor_t812171902_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2486[6] = 
{
	XPAnchor_t812171902_StaticFields::get_offset_of_s_AnchorDict_2(),
	XPAnchor_t812171902::get_offset_of_m_LastFrameTrackingState_3(),
	XPAnchor_t812171902::get_offset_of_m_IsSessionDestroyed_4(),
	XPAnchor_t812171902::get_offset_of_U3CCloudIdU3Ek__BackingField_5(),
	XPAnchor_t812171902::get_offset_of_U3Cm_NativeSessionU3Ek__BackingField_6(),
	XPAnchor_t812171902::get_offset_of_U3Cm_NativeHandleU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2487 = { sizeof (XPSession_t3719927413), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2488 = { sizeof (XPSessionStatus_t1871925907)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2488[10] = 
{
	XPSessionStatus_t1871925907::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2489 = { sizeof (XPTrackingState_t2235208166)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2489[4] = 
{
	XPTrackingState_t2235208166::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2490 = { sizeof (iOSCameraPermission_t2172579563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2491 = { sizeof (UsageSample_t1768752096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2491[2] = 
{
	UsageSample_t1768752096::get_offset_of__button_2(),
	UsageSample_t1768752096::get_offset_of__text_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2492 = { sizeof (TKRect_t2880547869)+ sizeof (RuntimeObject), sizeof(TKRect_t2880547869 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2492[4] = 
{
	TKRect_t2880547869::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TKRect_t2880547869::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TKRect_t2880547869::get_offset_of_width_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TKRect_t2880547869::get_offset_of_height_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2493 = { sizeof (TKTouch_t2857605003), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2493[7] = 
{
	TKTouch_t2857605003::get_offset_of_fingerId_0(),
	TKTouch_t2857605003::get_offset_of_position_1(),
	TKTouch_t2857605003::get_offset_of_startPosition_2(),
	TKTouch_t2857605003::get_offset_of_deltaPosition_3(),
	TKTouch_t2857605003::get_offset_of_deltaTime_4(),
	TKTouch_t2857605003::get_offset_of_tapCount_5(),
	TKTouch_t2857605003::get_offset_of_phase_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2494 = { sizeof (TKGestureRecognizerState_t2713539247)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2494[6] = 
{
	TKGestureRecognizerState_t2713539247::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2495 = { sizeof (TKAbstractGestureRecognizer_t1903772339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2495[10] = 
{
	TKAbstractGestureRecognizer_t1903772339::get_offset_of_enabled_0(),
	TKAbstractGestureRecognizer_t1903772339::get_offset_of_boundaryFrame_1(),
	TKAbstractGestureRecognizer_t1903772339::get_offset_of_zIndex_2(),
	TKAbstractGestureRecognizer_t1903772339::get_offset_of__state_3(),
	TKAbstractGestureRecognizer_t1903772339::get_offset_of_alwaysSendTouchesMoved_4(),
	TKAbstractGestureRecognizer_t1903772339::get_offset_of__trackingTouches_5(),
	TKAbstractGestureRecognizer_t1903772339::get_offset_of__subsetOfTouchesBeingTrackedApplicableToCurrentRecognizer_6(),
	TKAbstractGestureRecognizer_t1903772339::get_offset_of__sentTouchesBegan_7(),
	TKAbstractGestureRecognizer_t1903772339::get_offset_of__sentTouchesMoved_8(),
	TKAbstractGestureRecognizer_t1903772339::get_offset_of__sentTouchesEnded_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2496 = { sizeof (TKAngleSwipeRecognizer_t3666809813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2496[10] = 
{
	TKAngleSwipeRecognizer_t3666809813::get_offset_of_gestureRecognizedEvent_10(),
	TKAngleSwipeRecognizer_t3666809813::get_offset_of__angleRecognizedEvents_11(),
	TKAngleSwipeRecognizer_t3666809813::get_offset_of_timeToSwipe_12(),
	TKAngleSwipeRecognizer_t3666809813::get_offset_of_U3CswipeVelocityU3Ek__BackingField_13(),
	TKAngleSwipeRecognizer_t3666809813::get_offset_of_U3CswipeAngleU3Ek__BackingField_14(),
	TKAngleSwipeRecognizer_t3666809813::get_offset_of_U3CswipeVelVectorU3Ek__BackingField_15(),
	TKAngleSwipeRecognizer_t3666809813::get_offset_of_minimumDistance_16(),
	TKAngleSwipeRecognizer_t3666809813::get_offset_of__startPoint_17(),
	TKAngleSwipeRecognizer_t3666809813::get_offset_of__endPoint_18(),
	TKAngleSwipeRecognizer_t3666809813::get_offset_of__startTime_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2497 = { sizeof (AngleListener_t4071059023)+ sizeof (RuntimeObject), sizeof(AngleListener_t4071059023_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2497[3] = 
{
	AngleListener_t4071059023::get_offset_of_Varience_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AngleListener_t4071059023::get_offset_of_Direction_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AngleListener_t4071059023::get_offset_of_Action_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2498 = { sizeof (U3CremoveAngleRecognizedEventsU3Ec__AnonStorey0_t2854203665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2498[1] = 
{
	U3CremoveAngleRecognizedEventsU3Ec__AnonStorey0_t2854203665::get_offset_of_action_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2499 = { sizeof (TKAnyTouchRecognizer_t3651961449), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2499[2] = 
{
	TKAnyTouchRecognizer_t3651961449::get_offset_of_onEnteredEvent_10(),
	TKAnyTouchRecognizer_t3651961449::get_offset_of_onExitedEvent_11(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
