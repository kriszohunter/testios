using System;
using System.Collections.Generic;

//Only for IOS
using UnityEngine.EventSystems;

namespace UnityEngine.XR.iOS
{
	public class UnityARHitTestExample : MonoBehaviour
	{
		public PlaneVisualizationManager planevisualizationmanager;
		public Transform m_HitTransform;//This is the object to be placed
		public float maxRayDistance = 30.0f;
		public LayerMask collisionLayer = 1 << 10;  //ARKitPlane layer

        bool HitTestWithResultType (ARPoint point, ARHitTestResultType resultTypes)
        {
            List<ARHitTestResult> hitResults = UnityARSessionNativeInterface.GetARSessionNativeInterface ().HitTest (point, resultTypes);
            if (hitResults.Count > 0) {
                foreach (var hitResult in hitResults) {
                    Debug.Log ("** Got hit! **");
//                    m_HitTransform.position = UnityARMatrixOps.GetPosition (hitResult.worldTransform);
//                    m_HitTransform.rotation = UnityARMatrixOps.GetRotation (hitResult.worldTransform);
					planevisualizationmanager.CharacterPosIOS = UnityARMatrixOps.GetPosition (hitResult.worldTransform);
					planevisualizationmanager.CharacterRotIOS = UnityARMatrixOps.GetRotation (hitResult.worldTransform);
					planevisualizationmanager.stopPlaneDetection = true;
					planevisualizationmanager.OnPlaneDetectionCharacterInstantiateIOS ();
                    return true;
                }
            }
            return false;
        }


		Vector2 m_ScreenPosition = new Vector2(Screen.width / 2, Screen.height / 2);
		ARPoint point;
		void Start()
		{
			var screenPosition = Camera.main.ScreenToViewportPoint(m_ScreenPosition);
			point = new ARPoint {
								x = screenPosition.x,
								y = screenPosition.y
							};
		}

		// Update is called once per frame
		void Update () {
			if (Application.platform != RuntimePlatform.IPhonePlayer)
				return;
		
			if (m_HitTransform != null)
			{
				if (planevisualizationmanager.stopPlaneDetection)
					return;

                //if (!planevisualizationmanager.isStartDetecting)
                //    return;


				planevisualizationmanager.instructionText.text = "LOOK UP AND TAP ON THE HIGHLIGHTED AREA TO SHOW YOUR AVATAR";
				planevisualizationmanager.instructionText.gameObject.SetActive(true);

				var touch = Input.GetTouch(0);
				if (touch.phase == TouchPhase.Began || touch.phase == TouchPhase.Moved && !IsPointerOverUIObject())
				{
					var screenPosition = Camera.main.ScreenToViewportPoint(touch.position);
					ARPoint point = new ARPoint {
						x = screenPosition.x,
						y = screenPosition.y
					};

                    // prioritize reults types
                    ARHitTestResultType[] resultTypes = {
                        ARHitTestResultType.ARHitTestResultTypeExistingPlaneUsingExtent, 
                        // if you want to use infinite planes use this:
                        //ARHitTestResultType.ARHitTestResultTypeExistingPlane,
                        ARHitTestResultType.ARHitTestResultTypeHorizontalPlane, 
                        ARHitTestResultType.ARHitTestResultTypeFeaturePoint
                    }; 
					
                    foreach (ARHitTestResultType resultType in resultTypes)
                    {
                        if (HitTestWithResultType (point, resultType))
                        {
                            return;
                        }
                    }
				}
			}
//			#endif

		}


		private bool IsPointerOverUIObject()
		{
			PointerEventData eventDataCurrentPOS = new PointerEventData (EventSystem.current);
			eventDataCurrentPOS.position = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
			List<RaycastResult> Results = new List<RaycastResult> ();
			EventSystem.current.RaycastAll (eventDataCurrentPOS, Results);
			return Results.Count > 0;
		}

	
	}
}

