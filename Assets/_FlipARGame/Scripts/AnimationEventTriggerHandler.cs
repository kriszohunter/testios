﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEventTriggerHandler : MonoBehaviour {

    public MainMenuManager mainmenumanger;
    public PlaneVisualizationManager planevisualizationmanager;
	void Start () {
        planevisualizationmanager = FindObjectOfType<PlaneVisualizationManager>();
	}

    public void OnLogoAnimationComplete()
    {
        mainmenumanger.OnLogoAnimationComplete();
    }
    public void OnCardShuffleAnimationComplete()
    {
        Debug.Log("** OnCardShuffleAnimationComplete triggering");
        planevisualizationmanager.OnShuffleComplete();
    }
    public void OnCharacterDeathAnimationComplete()
    {
        //Debug.Log("** OnCharacterDeathAnimationComplete triggering");
        //planevisualizationmanager.OnCharacterAnimComplete();
    }

}
