﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// PlaneVisualizationManager
struct PlaneVisualizationManager_t1447224982;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition[]
struct ReplacementDefinitionU5BU5D_t2596446823;
// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_t32045322;
// PlaneVisualizationManager/<_ShowAndroidToastMessage>c__AnonStoreyA
struct U3C_ShowAndroidToastMessageU3Ec__AnonStoreyA_t3559623145;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t4131667876;
// System.String
struct String_t;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityStandardAssets.Utility.FOVKick
struct FOVKick_t120370150;
// MainMenuManager
struct MainMenuManager_t3185368703;
// UnityStandardAssets.Utility.LerpControlledBob
struct LerpControlledBob_t1895875871;
// UnityEngine.Transform[]
struct TransformU5BU5D_t807237628;
// UnityStandardAssets.Utility.ObjectResetter
struct ObjectResetter_t639177103;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t3089334924;
// UnityStandardAssets.Utility.ParticleSystemDestroyer
struct ParticleSystemDestroyer_t558680695;
// UnityStandardAssets.Utility.WaypointCircuit
struct WaypointCircuit_t445075330;
// UnityStandardAssets.Utility.TimedObjectActivator/Entry
struct Entry_t2725803170;
// UnityStandardAssets.Utility.TimedObjectActivator/Entry[]
struct EntryU5BU5D_t3574483607;
// UnityStandardAssets.CrossPlatformInput.VirtualInput
struct VirtualInput_t2597455733;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// iTween
struct iTween_t770867771;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>
struct Dictionary_2_t3872604895;
// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>
struct Dictionary_2_t2541822629;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityStandardAssets.Utility.DragRigidbody
struct DragRigidbody_t1600652016;
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
struct VirtualAxis_t4087348596;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping
struct AxisMapping_t3982445645;
// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter
struct ThirdPersonCharacter_t1711070432;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.CapsuleCollider
struct CapsuleCollider_t197597763;
// UnityEngine.AI.NavMeshAgent
struct NavMeshAgent_t1276799816;
// UnityEngine.Object
struct Object_t631007953;
// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace
struct Vector3andSpace_t219844479;
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList
struct ReplacementList_t1887104210;
// UnityStandardAssets.Utility.WaypointCircuit/WaypointList
struct WaypointList_t2584574554;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// UnityStandardAssets.Utility.TimedObjectActivator/Entries
struct Entries_t3168066469;
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t2007329276;
// System.String[]
struct StringU5BU5D_t1281789340;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.Material[]
struct MaterialU5BU5D_t561872642;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// System.Collections.Generic.List`1<GoogleARCore.DetectedPlane>
struct List_1_t1239236824;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// TKPinchRecognizer
struct TKPinchRecognizer_t788645703;
// TKSwipeRecognizer
struct TKSwipeRecognizer_t3210539170;
// TKTapRecognizer
struct TKTapRecognizer_t2390504451;
// TKLongPressRecognizer
struct TKLongPressRecognizer_t1475862313;
// UnityEngine.XR.iOS.UnityARHitTestExample
struct UnityARHitTestExample_t457226377;
// PointCloudParticleExample
struct PointCloudParticleExample_t182386800;
// UnityEngine.XR.iOS.UnityARGeneratePlane
struct UnityARGeneratePlane_t272564669;
// UnityPointCloudExample
struct UnityPointCloudExample_t3649008995;
// UnityEngine.XR.iOS.UnityARKitControl
struct UnityARKitControl_t1358211756;
// UnityEngine.Light
struct Light_t3756812086;
// UnityEngine.SpringJoint
struct SpringJoint_t1912369980;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t777473367;
// UnityEngine.GUIText
struct GUIText_t402233326;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745554_H
#define U3CMODULEU3E_T692745554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745554 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745554_H
#ifndef U3CVOLUMEFADEOUTEU3EC__ITERATOR6_T1858533124_H
#define U3CVOLUMEFADEOUTEU3EC__ITERATOR6_T1858533124_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaneVisualizationManager/<VolumeFadeOutE>c__Iterator6
struct  U3CVolumeFadeOutEU3Ec__Iterator6_t1858533124  : public RuntimeObject
{
public:
	// UnityEngine.AudioSource PlaneVisualizationManager/<VolumeFadeOutE>c__Iterator6::audio
	AudioSource_t3935305588 * ___audio_0;
	// System.Single PlaneVisualizationManager/<VolumeFadeOutE>c__Iterator6::<audioVolume>__0
	float ___U3CaudioVolumeU3E__0_1;
	// System.Object PlaneVisualizationManager/<VolumeFadeOutE>c__Iterator6::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean PlaneVisualizationManager/<VolumeFadeOutE>c__Iterator6::$disposing
	bool ___U24disposing_3;
	// System.Int32 PlaneVisualizationManager/<VolumeFadeOutE>c__Iterator6::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_audio_0() { return static_cast<int32_t>(offsetof(U3CVolumeFadeOutEU3Ec__Iterator6_t1858533124, ___audio_0)); }
	inline AudioSource_t3935305588 * get_audio_0() const { return ___audio_0; }
	inline AudioSource_t3935305588 ** get_address_of_audio_0() { return &___audio_0; }
	inline void set_audio_0(AudioSource_t3935305588 * value)
	{
		___audio_0 = value;
		Il2CppCodeGenWriteBarrier((&___audio_0), value);
	}

	inline static int32_t get_offset_of_U3CaudioVolumeU3E__0_1() { return static_cast<int32_t>(offsetof(U3CVolumeFadeOutEU3Ec__Iterator6_t1858533124, ___U3CaudioVolumeU3E__0_1)); }
	inline float get_U3CaudioVolumeU3E__0_1() const { return ___U3CaudioVolumeU3E__0_1; }
	inline float* get_address_of_U3CaudioVolumeU3E__0_1() { return &___U3CaudioVolumeU3E__0_1; }
	inline void set_U3CaudioVolumeU3E__0_1(float value)
	{
		___U3CaudioVolumeU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CVolumeFadeOutEU3Ec__Iterator6_t1858533124, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CVolumeFadeOutEU3Ec__Iterator6_t1858533124, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CVolumeFadeOutEU3Ec__Iterator6_t1858533124, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CVOLUMEFADEOUTEU3EC__ITERATOR6_T1858533124_H
#ifndef U3CRESETCHARACTERAFTERANIMEU3EC__ITERATOR5_T1958411711_H
#define U3CRESETCHARACTERAFTERANIMEU3EC__ITERATOR5_T1958411711_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaneVisualizationManager/<ResetCharacterAfterAnimE>c__Iterator5
struct  U3CResetCharacterAfterAnimEU3Ec__Iterator5_t1958411711  : public RuntimeObject
{
public:
	// UnityEngine.GameObject[] PlaneVisualizationManager/<ResetCharacterAfterAnimE>c__Iterator5::$locvar0
	GameObjectU5BU5D_t3328599146* ___U24locvar0_0;
	// System.Int32 PlaneVisualizationManager/<ResetCharacterAfterAnimE>c__Iterator5::$locvar1
	int32_t ___U24locvar1_1;
	// PlaneVisualizationManager PlaneVisualizationManager/<ResetCharacterAfterAnimE>c__Iterator5::$this
	PlaneVisualizationManager_t1447224982 * ___U24this_2;
	// System.Object PlaneVisualizationManager/<ResetCharacterAfterAnimE>c__Iterator5::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean PlaneVisualizationManager/<ResetCharacterAfterAnimE>c__Iterator5::$disposing
	bool ___U24disposing_4;
	// System.Int32 PlaneVisualizationManager/<ResetCharacterAfterAnimE>c__Iterator5::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CResetCharacterAfterAnimEU3Ec__Iterator5_t1958411711, ___U24locvar0_0)); }
	inline GameObjectU5BU5D_t3328599146* get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(GameObjectU5BU5D_t3328599146* value)
	{
		___U24locvar0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_0), value);
	}

	inline static int32_t get_offset_of_U24locvar1_1() { return static_cast<int32_t>(offsetof(U3CResetCharacterAfterAnimEU3Ec__Iterator5_t1958411711, ___U24locvar1_1)); }
	inline int32_t get_U24locvar1_1() const { return ___U24locvar1_1; }
	inline int32_t* get_address_of_U24locvar1_1() { return &___U24locvar1_1; }
	inline void set_U24locvar1_1(int32_t value)
	{
		___U24locvar1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CResetCharacterAfterAnimEU3Ec__Iterator5_t1958411711, ___U24this_2)); }
	inline PlaneVisualizationManager_t1447224982 * get_U24this_2() const { return ___U24this_2; }
	inline PlaneVisualizationManager_t1447224982 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(PlaneVisualizationManager_t1447224982 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CResetCharacterAfterAnimEU3Ec__Iterator5_t1958411711, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CResetCharacterAfterAnimEU3Ec__Iterator5_t1958411711, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CResetCharacterAfterAnimEU3Ec__Iterator5_t1958411711, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRESETCHARACTERAFTERANIMEU3EC__ITERATOR5_T1958411711_H
#ifndef REPLACEMENTDEFINITION_T2693741842_H
#define REPLACEMENTDEFINITION_T2693741842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition
struct  ReplacementDefinition_t2693741842  : public RuntimeObject
{
public:
	// UnityEngine.Shader UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition::original
	Shader_t4151988712 * ___original_0;
	// UnityEngine.Shader UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition::replacement
	Shader_t4151988712 * ___replacement_1;

public:
	inline static int32_t get_offset_of_original_0() { return static_cast<int32_t>(offsetof(ReplacementDefinition_t2693741842, ___original_0)); }
	inline Shader_t4151988712 * get_original_0() const { return ___original_0; }
	inline Shader_t4151988712 ** get_address_of_original_0() { return &___original_0; }
	inline void set_original_0(Shader_t4151988712 * value)
	{
		___original_0 = value;
		Il2CppCodeGenWriteBarrier((&___original_0), value);
	}

	inline static int32_t get_offset_of_replacement_1() { return static_cast<int32_t>(offsetof(ReplacementDefinition_t2693741842, ___replacement_1)); }
	inline Shader_t4151988712 * get_replacement_1() const { return ___replacement_1; }
	inline Shader_t4151988712 ** get_address_of_replacement_1() { return &___replacement_1; }
	inline void set_replacement_1(Shader_t4151988712 * value)
	{
		___replacement_1 = value;
		Il2CppCodeGenWriteBarrier((&___replacement_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPLACEMENTDEFINITION_T2693741842_H
#ifndef REPLACEMENTLIST_T1887104210_H
#define REPLACEMENTLIST_T1887104210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList
struct  ReplacementList_t1887104210  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition[] UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList::items
	ReplacementDefinitionU5BU5D_t2596446823* ___items_0;

public:
	inline static int32_t get_offset_of_items_0() { return static_cast<int32_t>(offsetof(ReplacementList_t1887104210, ___items_0)); }
	inline ReplacementDefinitionU5BU5D_t2596446823* get_items_0() const { return ___items_0; }
	inline ReplacementDefinitionU5BU5D_t2596446823** get_address_of_items_0() { return &___items_0; }
	inline void set_items_0(ReplacementDefinitionU5BU5D_t2596446823* value)
	{
		___items_0 = value;
		Il2CppCodeGenWriteBarrier((&___items_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPLACEMENTLIST_T1887104210_H
#ifndef U3CPLAYCHARACTERANIMU3EC__ITERATOR4_T447388885_H
#define U3CPLAYCHARACTERANIMU3EC__ITERATOR4_T447388885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaneVisualizationManager/<PlayCharacterAnim>c__Iterator4
struct  U3CPlayCharacterAnimU3Ec__Iterator4_t447388885  : public RuntimeObject
{
public:
	// PlaneVisualizationManager PlaneVisualizationManager/<PlayCharacterAnim>c__Iterator4::$this
	PlaneVisualizationManager_t1447224982 * ___U24this_0;
	// System.Object PlaneVisualizationManager/<PlayCharacterAnim>c__Iterator4::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean PlaneVisualizationManager/<PlayCharacterAnim>c__Iterator4::$disposing
	bool ___U24disposing_2;
	// System.Int32 PlaneVisualizationManager/<PlayCharacterAnim>c__Iterator4::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CPlayCharacterAnimU3Ec__Iterator4_t447388885, ___U24this_0)); }
	inline PlaneVisualizationManager_t1447224982 * get_U24this_0() const { return ___U24this_0; }
	inline PlaneVisualizationManager_t1447224982 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(PlaneVisualizationManager_t1447224982 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CPlayCharacterAnimU3Ec__Iterator4_t447388885, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CPlayCharacterAnimU3Ec__Iterator4_t447388885, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CPlayCharacterAnimU3Ec__Iterator4_t447388885, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPLAYCHARACTERANIMU3EC__ITERATOR4_T447388885_H
#ifndef U3CONCHARACTERANIMCOMPLETEEU3EC__ITERATOR3_T1665021887_H
#define U3CONCHARACTERANIMCOMPLETEEU3EC__ITERATOR3_T1665021887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaneVisualizationManager/<OnCharacterAnimCompleteE>c__Iterator3
struct  U3COnCharacterAnimCompleteEU3Ec__Iterator3_t1665021887  : public RuntimeObject
{
public:
	// PlaneVisualizationManager PlaneVisualizationManager/<OnCharacterAnimCompleteE>c__Iterator3::$this
	PlaneVisualizationManager_t1447224982 * ___U24this_0;
	// System.Object PlaneVisualizationManager/<OnCharacterAnimCompleteE>c__Iterator3::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean PlaneVisualizationManager/<OnCharacterAnimCompleteE>c__Iterator3::$disposing
	bool ___U24disposing_2;
	// System.Int32 PlaneVisualizationManager/<OnCharacterAnimCompleteE>c__Iterator3::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3COnCharacterAnimCompleteEU3Ec__Iterator3_t1665021887, ___U24this_0)); }
	inline PlaneVisualizationManager_t1447224982 * get_U24this_0() const { return ___U24this_0; }
	inline PlaneVisualizationManager_t1447224982 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(PlaneVisualizationManager_t1447224982 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3COnCharacterAnimCompleteEU3Ec__Iterator3_t1665021887, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3COnCharacterAnimCompleteEU3Ec__Iterator3_t1665021887, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3COnCharacterAnimCompleteEU3Ec__Iterator3_t1665021887, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONCHARACTERANIMCOMPLETEEU3EC__ITERATOR3_T1665021887_H
#ifndef U3CSETMYMODELSCALEU3EC__ITERATOR2_T2747975946_H
#define U3CSETMYMODELSCALEU3EC__ITERATOR2_T2747975946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaneVisualizationManager/<SetMyModelScale>c__Iterator2
struct  U3CSetMyModelScaleU3Ec__Iterator2_t2747975946  : public RuntimeObject
{
public:
	// PlaneVisualizationManager PlaneVisualizationManager/<SetMyModelScale>c__Iterator2::$this
	PlaneVisualizationManager_t1447224982 * ___U24this_0;
	// System.Object PlaneVisualizationManager/<SetMyModelScale>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean PlaneVisualizationManager/<SetMyModelScale>c__Iterator2::$disposing
	bool ___U24disposing_2;
	// System.Int32 PlaneVisualizationManager/<SetMyModelScale>c__Iterator2::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CSetMyModelScaleU3Ec__Iterator2_t2747975946, ___U24this_0)); }
	inline PlaneVisualizationManager_t1447224982 * get_U24this_0() const { return ___U24this_0; }
	inline PlaneVisualizationManager_t1447224982 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(PlaneVisualizationManager_t1447224982 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CSetMyModelScaleU3Ec__Iterator2_t2747975946, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CSetMyModelScaleU3Ec__Iterator2_t2747975946, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CSetMyModelScaleU3Ec__Iterator2_t2747975946, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETMYMODELSCALEU3EC__ITERATOR2_T2747975946_H
#ifndef U3C_SHOWANDROIDTOASTMESSAGEU3EC__ANONSTOREY9_T3555953129_H
#define U3C_SHOWANDROIDTOASTMESSAGEU3EC__ANONSTOREY9_T3555953129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaneVisualizationManager/<_ShowAndroidToastMessage>c__AnonStorey9
struct  U3C_ShowAndroidToastMessageU3Ec__AnonStorey9_t3555953129  : public RuntimeObject
{
public:
	// UnityEngine.AndroidJavaClass PlaneVisualizationManager/<_ShowAndroidToastMessage>c__AnonStorey9::toastClass
	AndroidJavaClass_t32045322 * ___toastClass_0;
	// PlaneVisualizationManager/<_ShowAndroidToastMessage>c__AnonStoreyA PlaneVisualizationManager/<_ShowAndroidToastMessage>c__AnonStorey9::<>f__ref$10
	U3C_ShowAndroidToastMessageU3Ec__AnonStoreyA_t3559623145 * ___U3CU3Ef__refU2410_1;

public:
	inline static int32_t get_offset_of_toastClass_0() { return static_cast<int32_t>(offsetof(U3C_ShowAndroidToastMessageU3Ec__AnonStorey9_t3555953129, ___toastClass_0)); }
	inline AndroidJavaClass_t32045322 * get_toastClass_0() const { return ___toastClass_0; }
	inline AndroidJavaClass_t32045322 ** get_address_of_toastClass_0() { return &___toastClass_0; }
	inline void set_toastClass_0(AndroidJavaClass_t32045322 * value)
	{
		___toastClass_0 = value;
		Il2CppCodeGenWriteBarrier((&___toastClass_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2410_1() { return static_cast<int32_t>(offsetof(U3C_ShowAndroidToastMessageU3Ec__AnonStorey9_t3555953129, ___U3CU3Ef__refU2410_1)); }
	inline U3C_ShowAndroidToastMessageU3Ec__AnonStoreyA_t3559623145 * get_U3CU3Ef__refU2410_1() const { return ___U3CU3Ef__refU2410_1; }
	inline U3C_ShowAndroidToastMessageU3Ec__AnonStoreyA_t3559623145 ** get_address_of_U3CU3Ef__refU2410_1() { return &___U3CU3Ef__refU2410_1; }
	inline void set_U3CU3Ef__refU2410_1(U3C_ShowAndroidToastMessageU3Ec__AnonStoreyA_t3559623145 * value)
	{
		___U3CU3Ef__refU2410_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU2410_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_SHOWANDROIDTOASTMESSAGEU3EC__ANONSTOREY9_T3555953129_H
#ifndef U3C_SHOWANDROIDTOASTMESSAGEU3EC__ANONSTOREYA_T3559623145_H
#define U3C_SHOWANDROIDTOASTMESSAGEU3EC__ANONSTOREYA_T3559623145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaneVisualizationManager/<_ShowAndroidToastMessage>c__AnonStoreyA
struct  U3C_ShowAndroidToastMessageU3Ec__AnonStoreyA_t3559623145  : public RuntimeObject
{
public:
	// UnityEngine.AndroidJavaObject PlaneVisualizationManager/<_ShowAndroidToastMessage>c__AnonStoreyA::unityActivity
	AndroidJavaObject_t4131667876 * ___unityActivity_0;
	// System.String PlaneVisualizationManager/<_ShowAndroidToastMessage>c__AnonStoreyA::message
	String_t* ___message_1;

public:
	inline static int32_t get_offset_of_unityActivity_0() { return static_cast<int32_t>(offsetof(U3C_ShowAndroidToastMessageU3Ec__AnonStoreyA_t3559623145, ___unityActivity_0)); }
	inline AndroidJavaObject_t4131667876 * get_unityActivity_0() const { return ___unityActivity_0; }
	inline AndroidJavaObject_t4131667876 ** get_address_of_unityActivity_0() { return &___unityActivity_0; }
	inline void set_unityActivity_0(AndroidJavaObject_t4131667876 * value)
	{
		___unityActivity_0 = value;
		Il2CppCodeGenWriteBarrier((&___unityActivity_0), value);
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(U3C_ShowAndroidToastMessageU3Ec__AnonStoreyA_t3559623145, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier((&___message_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_SHOWANDROIDTOASTMESSAGEU3EC__ANONSTOREYA_T3559623145_H
#ifndef U3CINITIALINSTRUCTIONEU3EC__ITERATOR0_T2270754370_H
#define U3CINITIALINSTRUCTIONEU3EC__ITERATOR0_T2270754370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaneVisualizationManager/<InitialInstructionE>c__Iterator0
struct  U3CInitialInstructionEU3Ec__Iterator0_t2270754370  : public RuntimeObject
{
public:
	// System.Int32 PlaneVisualizationManager/<InitialInstructionE>c__Iterator0::<temp>__1
	int32_t ___U3CtempU3E__1_0;
	// PlaneVisualizationManager PlaneVisualizationManager/<InitialInstructionE>c__Iterator0::$this
	PlaneVisualizationManager_t1447224982 * ___U24this_1;
	// System.Object PlaneVisualizationManager/<InitialInstructionE>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean PlaneVisualizationManager/<InitialInstructionE>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 PlaneVisualizationManager/<InitialInstructionE>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtempU3E__1_0() { return static_cast<int32_t>(offsetof(U3CInitialInstructionEU3Ec__Iterator0_t2270754370, ___U3CtempU3E__1_0)); }
	inline int32_t get_U3CtempU3E__1_0() const { return ___U3CtempU3E__1_0; }
	inline int32_t* get_address_of_U3CtempU3E__1_0() { return &___U3CtempU3E__1_0; }
	inline void set_U3CtempU3E__1_0(int32_t value)
	{
		___U3CtempU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CInitialInstructionEU3Ec__Iterator0_t2270754370, ___U24this_1)); }
	inline PlaneVisualizationManager_t1447224982 * get_U24this_1() const { return ___U24this_1; }
	inline PlaneVisualizationManager_t1447224982 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(PlaneVisualizationManager_t1447224982 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CInitialInstructionEU3Ec__Iterator0_t2270754370, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CInitialInstructionEU3Ec__Iterator0_t2270754370, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CInitialInstructionEU3Ec__Iterator0_t2270754370, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITIALINSTRUCTIONEU3EC__ITERATOR0_T2270754370_H
#ifndef U3CONSELECTBTNCLICKEU3EC__ITERATOR1_T3254601737_H
#define U3CONSELECTBTNCLICKEU3EC__ITERATOR1_T3254601737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainMenuManager/<OnSelectBtnClickE>c__Iterator1
struct  U3COnSelectBtnClickEU3Ec__Iterator1_t3254601737  : public RuntimeObject
{
public:
	// System.Object MainMenuManager/<OnSelectBtnClickE>c__Iterator1::$current
	RuntimeObject * ___U24current_0;
	// System.Boolean MainMenuManager/<OnSelectBtnClickE>c__Iterator1::$disposing
	bool ___U24disposing_1;
	// System.Int32 MainMenuManager/<OnSelectBtnClickE>c__Iterator1::$PC
	int32_t ___U24PC_2;

public:
	inline static int32_t get_offset_of_U24current_0() { return static_cast<int32_t>(offsetof(U3COnSelectBtnClickEU3Ec__Iterator1_t3254601737, ___U24current_0)); }
	inline RuntimeObject * get_U24current_0() const { return ___U24current_0; }
	inline RuntimeObject ** get_address_of_U24current_0() { return &___U24current_0; }
	inline void set_U24current_0(RuntimeObject * value)
	{
		___U24current_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_0), value);
	}

	inline static int32_t get_offset_of_U24disposing_1() { return static_cast<int32_t>(offsetof(U3COnSelectBtnClickEU3Ec__Iterator1_t3254601737, ___U24disposing_1)); }
	inline bool get_U24disposing_1() const { return ___U24disposing_1; }
	inline bool* get_address_of_U24disposing_1() { return &___U24disposing_1; }
	inline void set_U24disposing_1(bool value)
	{
		___U24disposing_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3COnSelectBtnClickEU3Ec__Iterator1_t3254601737, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONSELECTBTNCLICKEU3EC__ITERATOR1_T3254601737_H
#ifndef FOVKICK_T120370150_H
#define FOVKICK_T120370150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FOVKick
struct  FOVKick_t120370150  : public RuntimeObject
{
public:
	// UnityEngine.Camera UnityStandardAssets.Utility.FOVKick::Camera
	Camera_t4157153871 * ___Camera_0;
	// System.Single UnityStandardAssets.Utility.FOVKick::originalFov
	float ___originalFov_1;
	// System.Single UnityStandardAssets.Utility.FOVKick::FOVIncrease
	float ___FOVIncrease_2;
	// System.Single UnityStandardAssets.Utility.FOVKick::TimeToIncrease
	float ___TimeToIncrease_3;
	// System.Single UnityStandardAssets.Utility.FOVKick::TimeToDecrease
	float ___TimeToDecrease_4;
	// UnityEngine.AnimationCurve UnityStandardAssets.Utility.FOVKick::IncreaseCurve
	AnimationCurve_t3046754366 * ___IncreaseCurve_5;

public:
	inline static int32_t get_offset_of_Camera_0() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___Camera_0)); }
	inline Camera_t4157153871 * get_Camera_0() const { return ___Camera_0; }
	inline Camera_t4157153871 ** get_address_of_Camera_0() { return &___Camera_0; }
	inline void set_Camera_0(Camera_t4157153871 * value)
	{
		___Camera_0 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_0), value);
	}

	inline static int32_t get_offset_of_originalFov_1() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___originalFov_1)); }
	inline float get_originalFov_1() const { return ___originalFov_1; }
	inline float* get_address_of_originalFov_1() { return &___originalFov_1; }
	inline void set_originalFov_1(float value)
	{
		___originalFov_1 = value;
	}

	inline static int32_t get_offset_of_FOVIncrease_2() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___FOVIncrease_2)); }
	inline float get_FOVIncrease_2() const { return ___FOVIncrease_2; }
	inline float* get_address_of_FOVIncrease_2() { return &___FOVIncrease_2; }
	inline void set_FOVIncrease_2(float value)
	{
		___FOVIncrease_2 = value;
	}

	inline static int32_t get_offset_of_TimeToIncrease_3() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___TimeToIncrease_3)); }
	inline float get_TimeToIncrease_3() const { return ___TimeToIncrease_3; }
	inline float* get_address_of_TimeToIncrease_3() { return &___TimeToIncrease_3; }
	inline void set_TimeToIncrease_3(float value)
	{
		___TimeToIncrease_3 = value;
	}

	inline static int32_t get_offset_of_TimeToDecrease_4() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___TimeToDecrease_4)); }
	inline float get_TimeToDecrease_4() const { return ___TimeToDecrease_4; }
	inline float* get_address_of_TimeToDecrease_4() { return &___TimeToDecrease_4; }
	inline void set_TimeToDecrease_4(float value)
	{
		___TimeToDecrease_4 = value;
	}

	inline static int32_t get_offset_of_IncreaseCurve_5() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___IncreaseCurve_5)); }
	inline AnimationCurve_t3046754366 * get_IncreaseCurve_5() const { return ___IncreaseCurve_5; }
	inline AnimationCurve_t3046754366 ** get_address_of_IncreaseCurve_5() { return &___IncreaseCurve_5; }
	inline void set_IncreaseCurve_5(AnimationCurve_t3046754366 * value)
	{
		___IncreaseCurve_5 = value;
		Il2CppCodeGenWriteBarrier((&___IncreaseCurve_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOVKICK_T120370150_H
#ifndef U3CFOVKICKUPU3EC__ITERATOR0_T3738408313_H
#define U3CFOVKICKUPU3EC__ITERATOR0_T3738408313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0
struct  U3CFOVKickUpU3Ec__Iterator0_t3738408313  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0::<t>__0
	float ___U3CtU3E__0_0;
	// UnityStandardAssets.Utility.FOVKick UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0::$this
	FOVKick_t120370150 * ___U24this_1;
	// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ec__Iterator0_t3738408313, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ec__Iterator0_t3738408313, ___U24this_1)); }
	inline FOVKick_t120370150 * get_U24this_1() const { return ___U24this_1; }
	inline FOVKick_t120370150 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(FOVKick_t120370150 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ec__Iterator0_t3738408313, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ec__Iterator0_t3738408313, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ec__Iterator0_t3738408313, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFOVKICKUPU3EC__ITERATOR0_T3738408313_H
#ifndef U3CFOVKICKDOWNU3EC__ITERATOR1_T1440840980_H
#define U3CFOVKICKDOWNU3EC__ITERATOR1_T1440840980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1
struct  U3CFOVKickDownU3Ec__Iterator1_t1440840980  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1::<t>__0
	float ___U3CtU3E__0_0;
	// UnityStandardAssets.Utility.FOVKick UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1::$this
	FOVKick_t120370150 * ___U24this_1;
	// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ec__Iterator1_t1440840980, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ec__Iterator1_t1440840980, ___U24this_1)); }
	inline FOVKick_t120370150 * get_U24this_1() const { return ___U24this_1; }
	inline FOVKick_t120370150 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(FOVKick_t120370150 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ec__Iterator1_t1440840980, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ec__Iterator1_t1440840980, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ec__Iterator1_t1440840980, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFOVKICKDOWNU3EC__ITERATOR1_T1440840980_H
#ifndef U3CONSTARTBTNCLICKEU3EC__ITERATOR0_T2041946857_H
#define U3CONSTARTBTNCLICKEU3EC__ITERATOR0_T2041946857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainMenuManager/<OnStartBtnClickE>c__Iterator0
struct  U3COnStartBtnClickEU3Ec__Iterator0_t2041946857  : public RuntimeObject
{
public:
	// MainMenuManager MainMenuManager/<OnStartBtnClickE>c__Iterator0::$this
	MainMenuManager_t3185368703 * ___U24this_0;
	// System.Object MainMenuManager/<OnStartBtnClickE>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean MainMenuManager/<OnStartBtnClickE>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 MainMenuManager/<OnStartBtnClickE>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3COnStartBtnClickEU3Ec__Iterator0_t2041946857, ___U24this_0)); }
	inline MainMenuManager_t3185368703 * get_U24this_0() const { return ___U24this_0; }
	inline MainMenuManager_t3185368703 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(MainMenuManager_t3185368703 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3COnStartBtnClickEU3Ec__Iterator0_t2041946857, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3COnStartBtnClickEU3Ec__Iterator0_t2041946857, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3COnStartBtnClickEU3Ec__Iterator0_t2041946857, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONSTARTBTNCLICKEU3EC__ITERATOR0_T2041946857_H
#ifndef LERPCONTROLLEDBOB_T1895875871_H
#define LERPCONTROLLEDBOB_T1895875871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.LerpControlledBob
struct  LerpControlledBob_t1895875871  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.LerpControlledBob::BobDuration
	float ___BobDuration_0;
	// System.Single UnityStandardAssets.Utility.LerpControlledBob::BobAmount
	float ___BobAmount_1;
	// System.Single UnityStandardAssets.Utility.LerpControlledBob::m_Offset
	float ___m_Offset_2;

public:
	inline static int32_t get_offset_of_BobDuration_0() { return static_cast<int32_t>(offsetof(LerpControlledBob_t1895875871, ___BobDuration_0)); }
	inline float get_BobDuration_0() const { return ___BobDuration_0; }
	inline float* get_address_of_BobDuration_0() { return &___BobDuration_0; }
	inline void set_BobDuration_0(float value)
	{
		___BobDuration_0 = value;
	}

	inline static int32_t get_offset_of_BobAmount_1() { return static_cast<int32_t>(offsetof(LerpControlledBob_t1895875871, ___BobAmount_1)); }
	inline float get_BobAmount_1() const { return ___BobAmount_1; }
	inline float* get_address_of_BobAmount_1() { return &___BobAmount_1; }
	inline void set_BobAmount_1(float value)
	{
		___BobAmount_1 = value;
	}

	inline static int32_t get_offset_of_m_Offset_2() { return static_cast<int32_t>(offsetof(LerpControlledBob_t1895875871, ___m_Offset_2)); }
	inline float get_m_Offset_2() const { return ___m_Offset_2; }
	inline float* get_address_of_m_Offset_2() { return &___m_Offset_2; }
	inline void set_m_Offset_2(float value)
	{
		___m_Offset_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LERPCONTROLLEDBOB_T1895875871_H
#ifndef U3CDOBOBCYCLEU3EC__ITERATOR0_T1149538828_H
#define U3CDOBOBCYCLEU3EC__ITERATOR0_T1149538828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0
struct  U3CDoBobCycleU3Ec__Iterator0_t1149538828  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0::<t>__0
	float ___U3CtU3E__0_0;
	// UnityStandardAssets.Utility.LerpControlledBob UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0::$this
	LerpControlledBob_t1895875871 * ___U24this_1;
	// System.Object UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ec__Iterator0_t1149538828, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ec__Iterator0_t1149538828, ___U24this_1)); }
	inline LerpControlledBob_t1895875871 * get_U24this_1() const { return ___U24this_1; }
	inline LerpControlledBob_t1895875871 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(LerpControlledBob_t1895875871 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ec__Iterator0_t1149538828, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ec__Iterator0_t1149538828, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ec__Iterator0_t1149538828, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOBOBCYCLEU3EC__ITERATOR0_T1149538828_H
#ifndef GAMEMANAGER_T1536523654_H
#define GAMEMANAGER_T1536523654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager
struct  GameManager_t1536523654  : public RuntimeObject
{
public:
	// System.Int32 GameManager::selectedCharacterNumber
	int32_t ___selectedCharacterNumber_1;
	// System.Boolean GameManager::isFromGamePlayToMainMenu
	bool ___isFromGamePlayToMainMenu_2;

public:
	inline static int32_t get_offset_of_selectedCharacterNumber_1() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___selectedCharacterNumber_1)); }
	inline int32_t get_selectedCharacterNumber_1() const { return ___selectedCharacterNumber_1; }
	inline int32_t* get_address_of_selectedCharacterNumber_1() { return &___selectedCharacterNumber_1; }
	inline void set_selectedCharacterNumber_1(int32_t value)
	{
		___selectedCharacterNumber_1 = value;
	}

	inline static int32_t get_offset_of_isFromGamePlayToMainMenu_2() { return static_cast<int32_t>(offsetof(GameManager_t1536523654, ___isFromGamePlayToMainMenu_2)); }
	inline bool get_isFromGamePlayToMainMenu_2() const { return ___isFromGamePlayToMainMenu_2; }
	inline bool* get_address_of_isFromGamePlayToMainMenu_2() { return &___isFromGamePlayToMainMenu_2; }
	inline void set_isFromGamePlayToMainMenu_2(bool value)
	{
		___isFromGamePlayToMainMenu_2 = value;
	}
};

struct GameManager_t1536523654_StaticFields
{
public:
	// GameManager GameManager::instance
	GameManager_t1536523654 * ___instance_0;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(GameManager_t1536523654_StaticFields, ___instance_0)); }
	inline GameManager_t1536523654 * get_instance_0() const { return ___instance_0; }
	inline GameManager_t1536523654 ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(GameManager_t1536523654 * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMANAGER_T1536523654_H
#ifndef U3CRESETCOROUTINEU3EC__ITERATOR0_T3232105836_H
#define U3CRESETCOROUTINEU3EC__ITERATOR0_T3232105836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0
struct  U3CResetCoroutineU3Ec__Iterator0_t3232105836  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::delay
	float ___delay_0;
	// UnityEngine.Transform[] UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$locvar0
	TransformU5BU5D_t807237628* ___U24locvar0_1;
	// System.Int32 UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$locvar1
	int32_t ___U24locvar1_2;
	// UnityStandardAssets.Utility.ObjectResetter UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$this
	ObjectResetter_t639177103 * ___U24this_3;
	// System.Object UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_delay_0() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___delay_0)); }
	inline float get_delay_0() const { return ___delay_0; }
	inline float* get_address_of_delay_0() { return &___delay_0; }
	inline void set_delay_0(float value)
	{
		___delay_0 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___U24locvar0_1)); }
	inline TransformU5BU5D_t807237628* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline TransformU5BU5D_t807237628** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(TransformU5BU5D_t807237628* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___U24this_3)); }
	inline ObjectResetter_t639177103 * get_U24this_3() const { return ___U24this_3; }
	inline ObjectResetter_t639177103 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(ObjectResetter_t639177103 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t3232105836, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRESETCOROUTINEU3EC__ITERATOR0_T3232105836_H
#ifndef U3CEWAITFORANIMATIONTOCOMPLETEU3EC__ITERATOR0_T186264544_H
#define U3CEWAITFORANIMATIONTOCOMPLETEU3EC__ITERATOR0_T186264544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllRef/<EWaitForAnimationToComplete>c__Iterator0
struct  U3CEWaitForAnimationToCompleteU3Ec__Iterator0_t186264544  : public RuntimeObject
{
public:
	// UnityEngine.Animator AllRef/<EWaitForAnimationToComplete>c__Iterator0::anim
	Animator_t434523843 * ___anim_0;
	// System.Object AllRef/<EWaitForAnimationToComplete>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean AllRef/<EWaitForAnimationToComplete>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 AllRef/<EWaitForAnimationToComplete>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_anim_0() { return static_cast<int32_t>(offsetof(U3CEWaitForAnimationToCompleteU3Ec__Iterator0_t186264544, ___anim_0)); }
	inline Animator_t434523843 * get_anim_0() const { return ___anim_0; }
	inline Animator_t434523843 ** get_address_of_anim_0() { return &___anim_0; }
	inline void set_anim_0(Animator_t434523843 * value)
	{
		___anim_0 = value;
		Il2CppCodeGenWriteBarrier((&___anim_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CEWaitForAnimationToCompleteU3Ec__Iterator0_t186264544, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CEWaitForAnimationToCompleteU3Ec__Iterator0_t186264544, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CEWaitForAnimationToCompleteU3Ec__Iterator0_t186264544, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEWAITFORANIMATIONTOCOMPLETEU3EC__ITERATOR0_T186264544_H
#ifndef U3CSTARTU3EC__ITERATOR0_T980021917_H
#define U3CSTARTU3EC__ITERATOR0_T980021917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t980021917  : public RuntimeObject
{
public:
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::<systems>__0
	ParticleSystemU5BU5D_t3089334924* ___U3CsystemsU3E__0_0;
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$locvar0
	ParticleSystemU5BU5D_t3089334924* ___U24locvar0_1;
	// System.Int32 UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$locvar1
	int32_t ___U24locvar1_2;
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::<stopTime>__0
	float ___U3CstopTimeU3E__0_3;
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$locvar2
	ParticleSystemU5BU5D_t3089334924* ___U24locvar2_4;
	// System.Int32 UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$locvar3
	int32_t ___U24locvar3_5;
	// UnityStandardAssets.Utility.ParticleSystemDestroyer UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$this
	ParticleSystemDestroyer_t558680695 * ___U24this_6;
	// System.Object UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CsystemsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U3CsystemsU3E__0_0)); }
	inline ParticleSystemU5BU5D_t3089334924* get_U3CsystemsU3E__0_0() const { return ___U3CsystemsU3E__0_0; }
	inline ParticleSystemU5BU5D_t3089334924** get_address_of_U3CsystemsU3E__0_0() { return &___U3CsystemsU3E__0_0; }
	inline void set_U3CsystemsU3E__0_0(ParticleSystemU5BU5D_t3089334924* value)
	{
		___U3CsystemsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsystemsU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24locvar0_1)); }
	inline ParticleSystemU5BU5D_t3089334924* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline ParticleSystemU5BU5D_t3089334924** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(ParticleSystemU5BU5D_t3089334924* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U3CstopTimeU3E__0_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U3CstopTimeU3E__0_3)); }
	inline float get_U3CstopTimeU3E__0_3() const { return ___U3CstopTimeU3E__0_3; }
	inline float* get_address_of_U3CstopTimeU3E__0_3() { return &___U3CstopTimeU3E__0_3; }
	inline void set_U3CstopTimeU3E__0_3(float value)
	{
		___U3CstopTimeU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U24locvar2_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24locvar2_4)); }
	inline ParticleSystemU5BU5D_t3089334924* get_U24locvar2_4() const { return ___U24locvar2_4; }
	inline ParticleSystemU5BU5D_t3089334924** get_address_of_U24locvar2_4() { return &___U24locvar2_4; }
	inline void set_U24locvar2_4(ParticleSystemU5BU5D_t3089334924* value)
	{
		___U24locvar2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar2_4), value);
	}

	inline static int32_t get_offset_of_U24locvar3_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24locvar3_5)); }
	inline int32_t get_U24locvar3_5() const { return ___U24locvar3_5; }
	inline int32_t* get_address_of_U24locvar3_5() { return &___U24locvar3_5; }
	inline void set_U24locvar3_5(int32_t value)
	{
		___U24locvar3_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24this_6)); }
	inline ParticleSystemDestroyer_t558680695 * get_U24this_6() const { return ___U24this_6; }
	inline ParticleSystemDestroyer_t558680695 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(ParticleSystemDestroyer_t558680695 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t980021917, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T980021917_H
#ifndef WAYPOINTLIST_T2584574554_H
#define WAYPOINTLIST_T2584574554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointCircuit/WaypointList
struct  WaypointList_t2584574554  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.WaypointCircuit UnityStandardAssets.Utility.WaypointCircuit/WaypointList::circuit
	WaypointCircuit_t445075330 * ___circuit_0;
	// UnityEngine.Transform[] UnityStandardAssets.Utility.WaypointCircuit/WaypointList::items
	TransformU5BU5D_t807237628* ___items_1;

public:
	inline static int32_t get_offset_of_circuit_0() { return static_cast<int32_t>(offsetof(WaypointList_t2584574554, ___circuit_0)); }
	inline WaypointCircuit_t445075330 * get_circuit_0() const { return ___circuit_0; }
	inline WaypointCircuit_t445075330 ** get_address_of_circuit_0() { return &___circuit_0; }
	inline void set_circuit_0(WaypointCircuit_t445075330 * value)
	{
		___circuit_0 = value;
		Il2CppCodeGenWriteBarrier((&___circuit_0), value);
	}

	inline static int32_t get_offset_of_items_1() { return static_cast<int32_t>(offsetof(WaypointList_t2584574554, ___items_1)); }
	inline TransformU5BU5D_t807237628* get_items_1() const { return ___items_1; }
	inline TransformU5BU5D_t807237628** get_address_of_items_1() { return &___items_1; }
	inline void set_items_1(TransformU5BU5D_t807237628* value)
	{
		___items_1 = value;
		Il2CppCodeGenWriteBarrier((&___items_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINTLIST_T2584574554_H
#ifndef U3CRELOADLEVELU3EC__ITERATOR2_T2784493974_H
#define U3CRELOADLEVELU3EC__ITERATOR2_T2784493974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__Iterator2
struct  U3CReloadLevelU3Ec__Iterator2_t2784493974  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__Iterator2::entry
	Entry_t2725803170 * ___entry_0;
	// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__Iterator2::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__Iterator2::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_entry_0() { return static_cast<int32_t>(offsetof(U3CReloadLevelU3Ec__Iterator2_t2784493974, ___entry_0)); }
	inline Entry_t2725803170 * get_entry_0() const { return ___entry_0; }
	inline Entry_t2725803170 ** get_address_of_entry_0() { return &___entry_0; }
	inline void set_entry_0(Entry_t2725803170 * value)
	{
		___entry_0 = value;
		Il2CppCodeGenWriteBarrier((&___entry_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CReloadLevelU3Ec__Iterator2_t2784493974, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CReloadLevelU3Ec__Iterator2_t2784493974, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CReloadLevelU3Ec__Iterator2_t2784493974, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRELOADLEVELU3EC__ITERATOR2_T2784493974_H
#ifndef ENTRIES_T3168066469_H
#define ENTRIES_T3168066469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/Entries
struct  Entries_t3168066469  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry[] UnityStandardAssets.Utility.TimedObjectActivator/Entries::entries
	EntryU5BU5D_t3574483607* ___entries_0;

public:
	inline static int32_t get_offset_of_entries_0() { return static_cast<int32_t>(offsetof(Entries_t3168066469, ___entries_0)); }
	inline EntryU5BU5D_t3574483607* get_entries_0() const { return ___entries_0; }
	inline EntryU5BU5D_t3574483607** get_address_of_entries_0() { return &___entries_0; }
	inline void set_entries_0(EntryU5BU5D_t3574483607* value)
	{
		___entries_0 = value;
		Il2CppCodeGenWriteBarrier((&___entries_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRIES_T3168066469_H
#ifndef U3CACTIVATEU3EC__ITERATOR0_T2664723090_H
#define U3CACTIVATEU3EC__ITERATOR0_T2664723090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator0
struct  U3CActivateU3Ec__Iterator0_t2664723090  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator0::entry
	Entry_t2725803170 * ___entry_0;
	// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_entry_0() { return static_cast<int32_t>(offsetof(U3CActivateU3Ec__Iterator0_t2664723090, ___entry_0)); }
	inline Entry_t2725803170 * get_entry_0() const { return ___entry_0; }
	inline Entry_t2725803170 ** get_address_of_entry_0() { return &___entry_0; }
	inline void set_entry_0(Entry_t2725803170 * value)
	{
		___entry_0 = value;
		Il2CppCodeGenWriteBarrier((&___entry_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CActivateU3Ec__Iterator0_t2664723090, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CActivateU3Ec__Iterator0_t2664723090, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CActivateU3Ec__Iterator0_t2664723090, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CACTIVATEU3EC__ITERATOR0_T2664723090_H
#ifndef U3CLOLLIPOPFALLEU3EC__ITERATOR7_T3577267671_H
#define U3CLOLLIPOPFALLEU3EC__ITERATOR7_T3577267671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaneVisualizationManager/<LollipopFallE>c__Iterator7
struct  U3CLollipopFallEU3Ec__Iterator7_t3577267671  : public RuntimeObject
{
public:
	// PlaneVisualizationManager PlaneVisualizationManager/<LollipopFallE>c__Iterator7::$this
	PlaneVisualizationManager_t1447224982 * ___U24this_0;
	// System.Object PlaneVisualizationManager/<LollipopFallE>c__Iterator7::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean PlaneVisualizationManager/<LollipopFallE>c__Iterator7::$disposing
	bool ___U24disposing_2;
	// System.Int32 PlaneVisualizationManager/<LollipopFallE>c__Iterator7::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CLollipopFallEU3Ec__Iterator7_t3577267671, ___U24this_0)); }
	inline PlaneVisualizationManager_t1447224982 * get_U24this_0() const { return ___U24this_0; }
	inline PlaneVisualizationManager_t1447224982 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(PlaneVisualizationManager_t1447224982 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CLollipopFallEU3Ec__Iterator7_t3577267671, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CLollipopFallEU3Ec__Iterator7_t3577267671, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CLollipopFallEU3Ec__Iterator7_t3577267671, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOLLIPOPFALLEU3EC__ITERATOR7_T3577267671_H
#ifndef U3CDEACTIVATEU3EC__ITERATOR1_T730025274_H
#define U3CDEACTIVATEU3EC__ITERATOR1_T730025274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__Iterator1
struct  U3CDeactivateU3Ec__Iterator1_t730025274  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__Iterator1::entry
	Entry_t2725803170 * ___entry_0;
	// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_entry_0() { return static_cast<int32_t>(offsetof(U3CDeactivateU3Ec__Iterator1_t730025274, ___entry_0)); }
	inline Entry_t2725803170 * get_entry_0() const { return ___entry_0; }
	inline Entry_t2725803170 ** get_address_of_entry_0() { return &___entry_0; }
	inline void set_entry_0(Entry_t2725803170 * value)
	{
		___entry_0 = value;
		Il2CppCodeGenWriteBarrier((&___entry_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDeactivateU3Ec__Iterator1_t730025274, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDeactivateU3Ec__Iterator1_t730025274, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDeactivateU3Ec__Iterator1_t730025274, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDEACTIVATEU3EC__ITERATOR1_T730025274_H
#ifndef CROSSPLATFORMINPUTMANAGER_T191731427_H
#define CROSSPLATFORMINPUTMANAGER_T191731427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager
struct  CrossPlatformInputManager_t191731427  : public RuntimeObject
{
public:

public:
};

struct CrossPlatformInputManager_t191731427_StaticFields
{
public:
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::activeInput
	VirtualInput_t2597455733 * ___activeInput_0;
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::s_TouchInput
	VirtualInput_t2597455733 * ___s_TouchInput_1;
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::s_HardwareInput
	VirtualInput_t2597455733 * ___s_HardwareInput_2;

public:
	inline static int32_t get_offset_of_activeInput_0() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t191731427_StaticFields, ___activeInput_0)); }
	inline VirtualInput_t2597455733 * get_activeInput_0() const { return ___activeInput_0; }
	inline VirtualInput_t2597455733 ** get_address_of_activeInput_0() { return &___activeInput_0; }
	inline void set_activeInput_0(VirtualInput_t2597455733 * value)
	{
		___activeInput_0 = value;
		Il2CppCodeGenWriteBarrier((&___activeInput_0), value);
	}

	inline static int32_t get_offset_of_s_TouchInput_1() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t191731427_StaticFields, ___s_TouchInput_1)); }
	inline VirtualInput_t2597455733 * get_s_TouchInput_1() const { return ___s_TouchInput_1; }
	inline VirtualInput_t2597455733 ** get_address_of_s_TouchInput_1() { return &___s_TouchInput_1; }
	inline void set_s_TouchInput_1(VirtualInput_t2597455733 * value)
	{
		___s_TouchInput_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_TouchInput_1), value);
	}

	inline static int32_t get_offset_of_s_HardwareInput_2() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t191731427_StaticFields, ___s_HardwareInput_2)); }
	inline VirtualInput_t2597455733 * get_s_HardwareInput_2() const { return ___s_HardwareInput_2; }
	inline VirtualInput_t2597455733 ** get_address_of_s_HardwareInput_2() { return &___s_HardwareInput_2; }
	inline void set_s_HardwareInput_2(VirtualInput_t2597455733 * value)
	{
		___s_HardwareInput_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_HardwareInput_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CROSSPLATFORMINPUTMANAGER_T191731427_H
#ifndef CRSPLINE_T2815350084_H
#define CRSPLINE_T2815350084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/CRSpline
struct  CRSpline_t2815350084  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] iTween/CRSpline::pts
	Vector3U5BU5D_t1718750761* ___pts_0;

public:
	inline static int32_t get_offset_of_pts_0() { return static_cast<int32_t>(offsetof(CRSpline_t2815350084, ___pts_0)); }
	inline Vector3U5BU5D_t1718750761* get_pts_0() const { return ___pts_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_pts_0() { return &___pts_0; }
	inline void set_pts_0(Vector3U5BU5D_t1718750761* value)
	{
		___pts_0 = value;
		Il2CppCodeGenWriteBarrier((&___pts_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRSPLINE_T2815350084_H
#ifndef U3CSWORDSHIDEEU3EC__ITERATOR8_T3420496710_H
#define U3CSWORDSHIDEEU3EC__ITERATOR8_T3420496710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaneVisualizationManager/<SwordsHideE>c__Iterator8
struct  U3CSwordsHideEU3Ec__Iterator8_t3420496710  : public RuntimeObject
{
public:
	// UnityEngine.GameObject[] PlaneVisualizationManager/<SwordsHideE>c__Iterator8::$locvar0
	GameObjectU5BU5D_t3328599146* ___U24locvar0_0;
	// System.Int32 PlaneVisualizationManager/<SwordsHideE>c__Iterator8::$locvar1
	int32_t ___U24locvar1_1;
	// PlaneVisualizationManager PlaneVisualizationManager/<SwordsHideE>c__Iterator8::$this
	PlaneVisualizationManager_t1447224982 * ___U24this_2;
	// System.Object PlaneVisualizationManager/<SwordsHideE>c__Iterator8::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean PlaneVisualizationManager/<SwordsHideE>c__Iterator8::$disposing
	bool ___U24disposing_4;
	// System.Int32 PlaneVisualizationManager/<SwordsHideE>c__Iterator8::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CSwordsHideEU3Ec__Iterator8_t3420496710, ___U24locvar0_0)); }
	inline GameObjectU5BU5D_t3328599146* get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(GameObjectU5BU5D_t3328599146* value)
	{
		___U24locvar0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_0), value);
	}

	inline static int32_t get_offset_of_U24locvar1_1() { return static_cast<int32_t>(offsetof(U3CSwordsHideEU3Ec__Iterator8_t3420496710, ___U24locvar1_1)); }
	inline int32_t get_U24locvar1_1() const { return ___U24locvar1_1; }
	inline int32_t* get_address_of_U24locvar1_1() { return &___U24locvar1_1; }
	inline void set_U24locvar1_1(int32_t value)
	{
		___U24locvar1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CSwordsHideEU3Ec__Iterator8_t3420496710, ___U24this_2)); }
	inline PlaneVisualizationManager_t1447224982 * get_U24this_2() const { return ___U24this_2; }
	inline PlaneVisualizationManager_t1447224982 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(PlaneVisualizationManager_t1447224982 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CSwordsHideEU3Ec__Iterator8_t3420496710, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CSwordsHideEU3Ec__Iterator8_t3420496710, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CSwordsHideEU3Ec__Iterator8_t3420496710, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSWORDSHIDEEU3EC__ITERATOR8_T3420496710_H
#ifndef U3CTWEENDELAYU3EC__ITERATOR0_T2686771544_H
#define U3CTWEENDELAYU3EC__ITERATOR0_T2686771544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/<TweenDelay>c__Iterator0
struct  U3CTweenDelayU3Ec__Iterator0_t2686771544  : public RuntimeObject
{
public:
	// iTween iTween/<TweenDelay>c__Iterator0::$this
	iTween_t770867771 * ___U24this_0;
	// System.Object iTween/<TweenDelay>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean iTween/<TweenDelay>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 iTween/<TweenDelay>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTweenDelayU3Ec__Iterator0_t2686771544, ___U24this_0)); }
	inline iTween_t770867771 * get_U24this_0() const { return ___U24this_0; }
	inline iTween_t770867771 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(iTween_t770867771 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTweenDelayU3Ec__Iterator0_t2686771544, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTweenDelayU3Ec__Iterator0_t2686771544, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTweenDelayU3Ec__Iterator0_t2686771544, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTWEENDELAYU3EC__ITERATOR0_T2686771544_H
#ifndef U3CTWEENRESTARTU3EC__ITERATOR1_T1737386981_H
#define U3CTWEENRESTARTU3EC__ITERATOR1_T1737386981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/<TweenRestart>c__Iterator1
struct  U3CTweenRestartU3Ec__Iterator1_t1737386981  : public RuntimeObject
{
public:
	// iTween iTween/<TweenRestart>c__Iterator1::$this
	iTween_t770867771 * ___U24this_0;
	// System.Object iTween/<TweenRestart>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean iTween/<TweenRestart>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 iTween/<TweenRestart>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTweenRestartU3Ec__Iterator1_t1737386981, ___U24this_0)); }
	inline iTween_t770867771 * get_U24this_0() const { return ___U24this_0; }
	inline iTween_t770867771 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(iTween_t770867771 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTweenRestartU3Ec__Iterator1_t1737386981, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTweenRestartU3Ec__Iterator1_t1737386981, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTweenRestartU3Ec__Iterator1_t1737386981, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTWEENRESTARTU3EC__ITERATOR1_T1737386981_H
#ifndef PREFERENCES_T313491356_H
#define PREFERENCES_T313491356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Preferences
struct  Preferences_t313491356  : public RuntimeObject
{
public:
	// System.Int32 Preferences::_level
	int32_t ____level_1;
	// System.Int32 Preferences::_coins
	int32_t ____coins_2;
	// System.Int32 Preferences::_rateUs
	int32_t ____rateUs_3;
	// System.Int32 Preferences::_ads
	int32_t ____ads_4;
	// System.Int32 Preferences::_isRate
	int32_t ____isRate_5;
	// System.Int32 Preferences::_score
	int32_t ____score_6;

public:
	inline static int32_t get_offset_of__level_1() { return static_cast<int32_t>(offsetof(Preferences_t313491356, ____level_1)); }
	inline int32_t get__level_1() const { return ____level_1; }
	inline int32_t* get_address_of__level_1() { return &____level_1; }
	inline void set__level_1(int32_t value)
	{
		____level_1 = value;
	}

	inline static int32_t get_offset_of__coins_2() { return static_cast<int32_t>(offsetof(Preferences_t313491356, ____coins_2)); }
	inline int32_t get__coins_2() const { return ____coins_2; }
	inline int32_t* get_address_of__coins_2() { return &____coins_2; }
	inline void set__coins_2(int32_t value)
	{
		____coins_2 = value;
	}

	inline static int32_t get_offset_of__rateUs_3() { return static_cast<int32_t>(offsetof(Preferences_t313491356, ____rateUs_3)); }
	inline int32_t get__rateUs_3() const { return ____rateUs_3; }
	inline int32_t* get_address_of__rateUs_3() { return &____rateUs_3; }
	inline void set__rateUs_3(int32_t value)
	{
		____rateUs_3 = value;
	}

	inline static int32_t get_offset_of__ads_4() { return static_cast<int32_t>(offsetof(Preferences_t313491356, ____ads_4)); }
	inline int32_t get__ads_4() const { return ____ads_4; }
	inline int32_t* get_address_of__ads_4() { return &____ads_4; }
	inline void set__ads_4(int32_t value)
	{
		____ads_4 = value;
	}

	inline static int32_t get_offset_of__isRate_5() { return static_cast<int32_t>(offsetof(Preferences_t313491356, ____isRate_5)); }
	inline int32_t get__isRate_5() const { return ____isRate_5; }
	inline int32_t* get_address_of__isRate_5() { return &____isRate_5; }
	inline void set__isRate_5(int32_t value)
	{
		____isRate_5 = value;
	}

	inline static int32_t get_offset_of__score_6() { return static_cast<int32_t>(offsetof(Preferences_t313491356, ____score_6)); }
	inline int32_t get__score_6() const { return ____score_6; }
	inline int32_t* get_address_of__score_6() { return &____score_6; }
	inline void set__score_6(int32_t value)
	{
		____score_6 = value;
	}
};

struct Preferences_t313491356_StaticFields
{
public:
	// Preferences Preferences::_instance
	Preferences_t313491356 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(Preferences_t313491356_StaticFields, ____instance_0)); }
	inline Preferences_t313491356 * get__instance_0() const { return ____instance_0; }
	inline Preferences_t313491356 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(Preferences_t313491356 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFERENCES_T313491356_H
#ifndef U3CSTARTU3EC__ITERATOR2_T2390838266_H
#define U3CSTARTU3EC__ITERATOR2_T2390838266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/<Start>c__Iterator2
struct  U3CStartU3Ec__Iterator2_t2390838266  : public RuntimeObject
{
public:
	// iTween iTween/<Start>c__Iterator2::$this
	iTween_t770867771 * ___U24this_0;
	// System.Object iTween/<Start>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean iTween/<Start>c__Iterator2::$disposing
	bool ___U24disposing_2;
	// System.Int32 iTween/<Start>c__Iterator2::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator2_t2390838266, ___U24this_0)); }
	inline iTween_t770867771 * get_U24this_0() const { return ___U24this_0; }
	inline iTween_t770867771 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(iTween_t770867771 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator2_t2390838266, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator2_t2390838266, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator2_t2390838266, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR2_T2390838266_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef VIRTUALAXIS_T4087348596_H
#define VIRTUALAXIS_T4087348596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
struct  VirtualAxis_t4087348596  : public RuntimeObject
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::m_Value
	float ___m_Value_1;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::<matchWithInputManager>k__BackingField
	bool ___U3CmatchWithInputManagerU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualAxis_t4087348596, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_m_Value_1() { return static_cast<int32_t>(offsetof(VirtualAxis_t4087348596, ___m_Value_1)); }
	inline float get_m_Value_1() const { return ___m_Value_1; }
	inline float* get_address_of_m_Value_1() { return &___m_Value_1; }
	inline void set_m_Value_1(float value)
	{
		___m_Value_1 = value;
	}

	inline static int32_t get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(VirtualAxis_t4087348596, ___U3CmatchWithInputManagerU3Ek__BackingField_2)); }
	inline bool get_U3CmatchWithInputManagerU3Ek__BackingField_2() const { return ___U3CmatchWithInputManagerU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CmatchWithInputManagerU3Ek__BackingField_2() { return &___U3CmatchWithInputManagerU3Ek__BackingField_2; }
	inline void set_U3CmatchWithInputManagerU3Ek__BackingField_2(bool value)
	{
		___U3CmatchWithInputManagerU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALAXIS_T4087348596_H
#ifndef VIRTUALBUTTON_T2756566330_H
#define VIRTUALBUTTON_T2756566330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton
struct  VirtualButton_t2756566330  : public RuntimeObject
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::<matchWithInputManager>k__BackingField
	bool ___U3CmatchWithInputManagerU3Ek__BackingField_1;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::m_LastPressedFrame
	int32_t ___m_LastPressedFrame_2;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::m_ReleasedFrame
	int32_t ___m_ReleasedFrame_3;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::m_Pressed
	bool ___m_Pressed_4;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualButton_t2756566330, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(VirtualButton_t2756566330, ___U3CmatchWithInputManagerU3Ek__BackingField_1)); }
	inline bool get_U3CmatchWithInputManagerU3Ek__BackingField_1() const { return ___U3CmatchWithInputManagerU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CmatchWithInputManagerU3Ek__BackingField_1() { return &___U3CmatchWithInputManagerU3Ek__BackingField_1; }
	inline void set_U3CmatchWithInputManagerU3Ek__BackingField_1(bool value)
	{
		___U3CmatchWithInputManagerU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_m_LastPressedFrame_2() { return static_cast<int32_t>(offsetof(VirtualButton_t2756566330, ___m_LastPressedFrame_2)); }
	inline int32_t get_m_LastPressedFrame_2() const { return ___m_LastPressedFrame_2; }
	inline int32_t* get_address_of_m_LastPressedFrame_2() { return &___m_LastPressedFrame_2; }
	inline void set_m_LastPressedFrame_2(int32_t value)
	{
		___m_LastPressedFrame_2 = value;
	}

	inline static int32_t get_offset_of_m_ReleasedFrame_3() { return static_cast<int32_t>(offsetof(VirtualButton_t2756566330, ___m_ReleasedFrame_3)); }
	inline int32_t get_m_ReleasedFrame_3() const { return ___m_ReleasedFrame_3; }
	inline int32_t* get_address_of_m_ReleasedFrame_3() { return &___m_ReleasedFrame_3; }
	inline void set_m_ReleasedFrame_3(int32_t value)
	{
		___m_ReleasedFrame_3 = value;
	}

	inline static int32_t get_offset_of_m_Pressed_4() { return static_cast<int32_t>(offsetof(VirtualButton_t2756566330, ___m_Pressed_4)); }
	inline bool get_m_Pressed_4() const { return ___m_Pressed_4; }
	inline bool* get_address_of_m_Pressed_4() { return &___m_Pressed_4; }
	inline void set_m_Pressed_4(bool value)
	{
		___m_Pressed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTON_T2756566330_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef CONTROLSTYLE_T1372986211_H
#define CONTROLSTYLE_T1372986211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle
struct  ControlStyle_t1372986211 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ControlStyle_t1372986211, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLSTYLE_T1372986211_H
#ifndef MAPPINGTYPE_T2039944511_H
#define MAPPINGTYPE_T2039944511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType
struct  MappingType_t2039944511 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MappingType_t2039944511, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPPINGTYPE_T2039944511_H
#ifndef ROUTEPOINT_T3880028948_H
#define ROUTEPOINT_T3880028948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint
struct  RoutePoint_t3880028948 
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit/RoutePoint::position
	Vector3_t3722313464  ___position_0;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit/RoutePoint::direction
	Vector3_t3722313464  ___direction_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(RoutePoint_t3880028948, ___position_0)); }
	inline Vector3_t3722313464  get_position_0() const { return ___position_0; }
	inline Vector3_t3722313464 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t3722313464  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_direction_1() { return static_cast<int32_t>(offsetof(RoutePoint_t3880028948, ___direction_1)); }
	inline Vector3_t3722313464  get_direction_1() const { return ___direction_1; }
	inline Vector3_t3722313464 * get_address_of_direction_1() { return &___direction_1; }
	inline void set_direction_1(Vector3_t3722313464  value)
	{
		___direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROUTEPOINT_T3880028948_H
#ifndef PROGRESSSTYLE_T3254572979_H
#define PROGRESSSTYLE_T3254572979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointProgressTracker/ProgressStyle
struct  ProgressStyle_t3254572979 
{
public:
	// System.Int32 UnityStandardAssets.Utility.WaypointProgressTracker/ProgressStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ProgressStyle_t3254572979, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSSTYLE_T3254572979_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef BUILDTARGETGROUP_T72322187_H
#define BUILDTARGETGROUP_T72322187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup
struct  BuildTargetGroup_t72322187 
{
public:
	// System.Int32 UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BuildTargetGroup_t72322187, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDTARGETGROUP_T72322187_H
#ifndef AXISOPTIONS_T3101732129_H
#define AXISOPTIONS_T3101732129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions
struct  AxisOptions_t3101732129 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisOptions_t3101732129, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTIONS_T3101732129_H
#ifndef SPACE_T654135784_H
#define SPACE_T654135784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Space
struct  Space_t654135784 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Space_t654135784, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACE_T654135784_H
#ifndef VIRTUALINPUT_T2597455733_H
#define VIRTUALINPUT_T2597455733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.VirtualInput
struct  VirtualInput_t2597455733  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.VirtualInput::<virtualMousePosition>k__BackingField
	Vector3_t3722313464  ___U3CvirtualMousePositionU3Ek__BackingField_0;
	// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_VirtualAxes
	Dictionary_2_t3872604895 * ___m_VirtualAxes_1;
	// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_VirtualButtons
	Dictionary_2_t2541822629 * ___m_VirtualButtons_2;
	// System.Collections.Generic.List`1<System.String> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_AlwaysUseVirtual
	List_1_t3319525431 * ___m_AlwaysUseVirtual_3;

public:
	inline static int32_t get_offset_of_U3CvirtualMousePositionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualInput_t2597455733, ___U3CvirtualMousePositionU3Ek__BackingField_0)); }
	inline Vector3_t3722313464  get_U3CvirtualMousePositionU3Ek__BackingField_0() const { return ___U3CvirtualMousePositionU3Ek__BackingField_0; }
	inline Vector3_t3722313464 * get_address_of_U3CvirtualMousePositionU3Ek__BackingField_0() { return &___U3CvirtualMousePositionU3Ek__BackingField_0; }
	inline void set_U3CvirtualMousePositionU3Ek__BackingField_0(Vector3_t3722313464  value)
	{
		___U3CvirtualMousePositionU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_m_VirtualAxes_1() { return static_cast<int32_t>(offsetof(VirtualInput_t2597455733, ___m_VirtualAxes_1)); }
	inline Dictionary_2_t3872604895 * get_m_VirtualAxes_1() const { return ___m_VirtualAxes_1; }
	inline Dictionary_2_t3872604895 ** get_address_of_m_VirtualAxes_1() { return &___m_VirtualAxes_1; }
	inline void set_m_VirtualAxes_1(Dictionary_2_t3872604895 * value)
	{
		___m_VirtualAxes_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_VirtualAxes_1), value);
	}

	inline static int32_t get_offset_of_m_VirtualButtons_2() { return static_cast<int32_t>(offsetof(VirtualInput_t2597455733, ___m_VirtualButtons_2)); }
	inline Dictionary_2_t2541822629 * get_m_VirtualButtons_2() const { return ___m_VirtualButtons_2; }
	inline Dictionary_2_t2541822629 ** get_address_of_m_VirtualButtons_2() { return &___m_VirtualButtons_2; }
	inline void set_m_VirtualButtons_2(Dictionary_2_t2541822629 * value)
	{
		___m_VirtualButtons_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_VirtualButtons_2), value);
	}

	inline static int32_t get_offset_of_m_AlwaysUseVirtual_3() { return static_cast<int32_t>(offsetof(VirtualInput_t2597455733, ___m_AlwaysUseVirtual_3)); }
	inline List_1_t3319525431 * get_m_AlwaysUseVirtual_3() const { return ___m_AlwaysUseVirtual_3; }
	inline List_1_t3319525431 ** get_address_of_m_AlwaysUseVirtual_3() { return &___m_AlwaysUseVirtual_3; }
	inline void set_m_AlwaysUseVirtual_3(List_1_t3319525431 * value)
	{
		___m_AlwaysUseVirtual_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AlwaysUseVirtual_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALINPUT_T2597455733_H
#ifndef NAMEDVALUECOLOR_T1091574706_H
#define NAMEDVALUECOLOR_T1091574706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/NamedValueColor
struct  NamedValueColor_t1091574706 
{
public:
	// System.Int32 iTween/NamedValueColor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NamedValueColor_t1091574706, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEDVALUECOLOR_T1091574706_H
#ifndef LOOPTYPE_T369612249_H
#define LOOPTYPE_T369612249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/LoopType
struct  LoopType_t369612249 
{
public:
	// System.Int32 iTween/LoopType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoopType_t369612249, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPTYPE_T369612249_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef EASETYPE_T2573404410_H
#define EASETYPE_T2573404410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/EaseType
struct  EaseType_t2573404410 
{
public:
	// System.Int32 iTween/EaseType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EaseType_t2573404410, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASETYPE_T2573404410_H
#ifndef CAMERAREFOCUS_T4263235746_H
#define CAMERAREFOCUS_T4263235746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.CameraRefocus
struct  CameraRefocus_t4263235746  : public RuntimeObject
{
public:
	// UnityEngine.Camera UnityStandardAssets.Utility.CameraRefocus::Camera
	Camera_t4157153871 * ___Camera_0;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.CameraRefocus::Lookatpoint
	Vector3_t3722313464  ___Lookatpoint_1;
	// UnityEngine.Transform UnityStandardAssets.Utility.CameraRefocus::Parent
	Transform_t3600365921 * ___Parent_2;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.CameraRefocus::m_OrigCameraPos
	Vector3_t3722313464  ___m_OrigCameraPos_3;
	// System.Boolean UnityStandardAssets.Utility.CameraRefocus::m_Refocus
	bool ___m_Refocus_4;

public:
	inline static int32_t get_offset_of_Camera_0() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___Camera_0)); }
	inline Camera_t4157153871 * get_Camera_0() const { return ___Camera_0; }
	inline Camera_t4157153871 ** get_address_of_Camera_0() { return &___Camera_0; }
	inline void set_Camera_0(Camera_t4157153871 * value)
	{
		___Camera_0 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_0), value);
	}

	inline static int32_t get_offset_of_Lookatpoint_1() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___Lookatpoint_1)); }
	inline Vector3_t3722313464  get_Lookatpoint_1() const { return ___Lookatpoint_1; }
	inline Vector3_t3722313464 * get_address_of_Lookatpoint_1() { return &___Lookatpoint_1; }
	inline void set_Lookatpoint_1(Vector3_t3722313464  value)
	{
		___Lookatpoint_1 = value;
	}

	inline static int32_t get_offset_of_Parent_2() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___Parent_2)); }
	inline Transform_t3600365921 * get_Parent_2() const { return ___Parent_2; }
	inline Transform_t3600365921 ** get_address_of_Parent_2() { return &___Parent_2; }
	inline void set_Parent_2(Transform_t3600365921 * value)
	{
		___Parent_2 = value;
		Il2CppCodeGenWriteBarrier((&___Parent_2), value);
	}

	inline static int32_t get_offset_of_m_OrigCameraPos_3() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___m_OrigCameraPos_3)); }
	inline Vector3_t3722313464  get_m_OrigCameraPos_3() const { return ___m_OrigCameraPos_3; }
	inline Vector3_t3722313464 * get_address_of_m_OrigCameraPos_3() { return &___m_OrigCameraPos_3; }
	inline void set_m_OrigCameraPos_3(Vector3_t3722313464  value)
	{
		___m_OrigCameraPos_3 = value;
	}

	inline static int32_t get_offset_of_m_Refocus_4() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___m_Refocus_4)); }
	inline bool get_m_Refocus_4() const { return ___m_Refocus_4; }
	inline bool* get_address_of_m_Refocus_4() { return &___m_Refocus_4; }
	inline void set_m_Refocus_4(bool value)
	{
		___m_Refocus_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAREFOCUS_T4263235746_H
#ifndef CURVECONTROLLEDBOB_T2679313829_H
#define CURVECONTROLLEDBOB_T2679313829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.CurveControlledBob
struct  CurveControlledBob_t2679313829  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::HorizontalBobRange
	float ___HorizontalBobRange_0;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::VerticalBobRange
	float ___VerticalBobRange_1;
	// UnityEngine.AnimationCurve UnityStandardAssets.Utility.CurveControlledBob::Bobcurve
	AnimationCurve_t3046754366 * ___Bobcurve_2;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::VerticaltoHorizontalRatio
	float ___VerticaltoHorizontalRatio_3;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_CyclePositionX
	float ___m_CyclePositionX_4;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_CyclePositionY
	float ___m_CyclePositionY_5;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_BobBaseInterval
	float ___m_BobBaseInterval_6;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.CurveControlledBob::m_OriginalCameraPosition
	Vector3_t3722313464  ___m_OriginalCameraPosition_7;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_Time
	float ___m_Time_8;

public:
	inline static int32_t get_offset_of_HorizontalBobRange_0() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___HorizontalBobRange_0)); }
	inline float get_HorizontalBobRange_0() const { return ___HorizontalBobRange_0; }
	inline float* get_address_of_HorizontalBobRange_0() { return &___HorizontalBobRange_0; }
	inline void set_HorizontalBobRange_0(float value)
	{
		___HorizontalBobRange_0 = value;
	}

	inline static int32_t get_offset_of_VerticalBobRange_1() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___VerticalBobRange_1)); }
	inline float get_VerticalBobRange_1() const { return ___VerticalBobRange_1; }
	inline float* get_address_of_VerticalBobRange_1() { return &___VerticalBobRange_1; }
	inline void set_VerticalBobRange_1(float value)
	{
		___VerticalBobRange_1 = value;
	}

	inline static int32_t get_offset_of_Bobcurve_2() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___Bobcurve_2)); }
	inline AnimationCurve_t3046754366 * get_Bobcurve_2() const { return ___Bobcurve_2; }
	inline AnimationCurve_t3046754366 ** get_address_of_Bobcurve_2() { return &___Bobcurve_2; }
	inline void set_Bobcurve_2(AnimationCurve_t3046754366 * value)
	{
		___Bobcurve_2 = value;
		Il2CppCodeGenWriteBarrier((&___Bobcurve_2), value);
	}

	inline static int32_t get_offset_of_VerticaltoHorizontalRatio_3() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___VerticaltoHorizontalRatio_3)); }
	inline float get_VerticaltoHorizontalRatio_3() const { return ___VerticaltoHorizontalRatio_3; }
	inline float* get_address_of_VerticaltoHorizontalRatio_3() { return &___VerticaltoHorizontalRatio_3; }
	inline void set_VerticaltoHorizontalRatio_3(float value)
	{
		___VerticaltoHorizontalRatio_3 = value;
	}

	inline static int32_t get_offset_of_m_CyclePositionX_4() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_CyclePositionX_4)); }
	inline float get_m_CyclePositionX_4() const { return ___m_CyclePositionX_4; }
	inline float* get_address_of_m_CyclePositionX_4() { return &___m_CyclePositionX_4; }
	inline void set_m_CyclePositionX_4(float value)
	{
		___m_CyclePositionX_4 = value;
	}

	inline static int32_t get_offset_of_m_CyclePositionY_5() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_CyclePositionY_5)); }
	inline float get_m_CyclePositionY_5() const { return ___m_CyclePositionY_5; }
	inline float* get_address_of_m_CyclePositionY_5() { return &___m_CyclePositionY_5; }
	inline void set_m_CyclePositionY_5(float value)
	{
		___m_CyclePositionY_5 = value;
	}

	inline static int32_t get_offset_of_m_BobBaseInterval_6() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_BobBaseInterval_6)); }
	inline float get_m_BobBaseInterval_6() const { return ___m_BobBaseInterval_6; }
	inline float* get_address_of_m_BobBaseInterval_6() { return &___m_BobBaseInterval_6; }
	inline void set_m_BobBaseInterval_6(float value)
	{
		___m_BobBaseInterval_6 = value;
	}

	inline static int32_t get_offset_of_m_OriginalCameraPosition_7() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_OriginalCameraPosition_7)); }
	inline Vector3_t3722313464  get_m_OriginalCameraPosition_7() const { return ___m_OriginalCameraPosition_7; }
	inline Vector3_t3722313464 * get_address_of_m_OriginalCameraPosition_7() { return &___m_OriginalCameraPosition_7; }
	inline void set_m_OriginalCameraPosition_7(Vector3_t3722313464  value)
	{
		___m_OriginalCameraPosition_7 = value;
	}

	inline static int32_t get_offset_of_m_Time_8() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_Time_8)); }
	inline float get_m_Time_8() const { return ___m_Time_8; }
	inline float* get_address_of_m_Time_8() { return &___m_Time_8; }
	inline void set_m_Time_8(float value)
	{
		___m_Time_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVECONTROLLEDBOB_T2679313829_H
#ifndef AXISOPTION_T1372819835_H
#define AXISOPTION_T1372819835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption
struct  AxisOption_t1372819835 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisOption_t1372819835, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTION_T1372819835_H
#ifndef ACTIVEINPUTMETHOD_T139315314_H
#define ACTIVEINPUTMETHOD_T139315314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/ActiveInputMethod
struct  ActiveInputMethod_t139315314 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/ActiveInputMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ActiveInputMethod_t139315314, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVEINPUTMETHOD_T139315314_H
#ifndef AXISOPTION_T3128671669_H
#define AXISOPTION_T3128671669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption
struct  AxisOption_t3128671669 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisOption_t3128671669, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTION_T3128671669_H
#ifndef ACTION_T837364808_H
#define ACTION_T837364808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/Action
struct  Action_t837364808 
{
public:
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/Action::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Action_t837364808, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T837364808_H
#ifndef MODE_T3024470803_H
#define MODE_T3024470803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ActivateTrigger/Mode
struct  Mode_t3024470803 
{
public:
	// System.Int32 UnityStandardAssets.Utility.ActivateTrigger/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t3024470803, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T3024470803_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef U3CONCARDBTNCLICKEU3EC__ITERATOR1_T1416119937_H
#define U3CONCARDBTNCLICKEU3EC__ITERATOR1_T1416119937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaneVisualizationManager/<OnCardBtnClickE>c__Iterator1
struct  U3COnCardBtnClickEU3Ec__Iterator1_t1416119937  : public RuntimeObject
{
public:
	// UnityEngine.Vector2 PlaneVisualizationManager/<OnCardBtnClickE>c__Iterator1::<m_ScreenPosition>__0
	Vector2_t2156229523  ___U3Cm_ScreenPositionU3E__0_0;
	// UnityEngine.Ray PlaneVisualizationManager/<OnCardBtnClickE>c__Iterator1::<ray>__0
	Ray_t3785851493  ___U3CrayU3E__0_1;
	// UnityEngine.GameObject PlaneVisualizationManager/<OnCardBtnClickE>c__Iterator1::<Card>__0
	GameObject_t1113636619 * ___U3CCardU3E__0_2;
	// System.Single PlaneVisualizationManager/<OnCardBtnClickE>c__Iterator1::<smallestDistance>__0
	float ___U3CsmallestDistanceU3E__0_3;
	// System.Int32 PlaneVisualizationManager/<OnCardBtnClickE>c__Iterator1::<nearestCardPos>__0
	int32_t ___U3CnearestCardPosU3E__0_4;
	// PlaneVisualizationManager PlaneVisualizationManager/<OnCardBtnClickE>c__Iterator1::$this
	PlaneVisualizationManager_t1447224982 * ___U24this_5;
	// System.Object PlaneVisualizationManager/<OnCardBtnClickE>c__Iterator1::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean PlaneVisualizationManager/<OnCardBtnClickE>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 PlaneVisualizationManager/<OnCardBtnClickE>c__Iterator1::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3Cm_ScreenPositionU3E__0_0() { return static_cast<int32_t>(offsetof(U3COnCardBtnClickEU3Ec__Iterator1_t1416119937, ___U3Cm_ScreenPositionU3E__0_0)); }
	inline Vector2_t2156229523  get_U3Cm_ScreenPositionU3E__0_0() const { return ___U3Cm_ScreenPositionU3E__0_0; }
	inline Vector2_t2156229523 * get_address_of_U3Cm_ScreenPositionU3E__0_0() { return &___U3Cm_ScreenPositionU3E__0_0; }
	inline void set_U3Cm_ScreenPositionU3E__0_0(Vector2_t2156229523  value)
	{
		___U3Cm_ScreenPositionU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CrayU3E__0_1() { return static_cast<int32_t>(offsetof(U3COnCardBtnClickEU3Ec__Iterator1_t1416119937, ___U3CrayU3E__0_1)); }
	inline Ray_t3785851493  get_U3CrayU3E__0_1() const { return ___U3CrayU3E__0_1; }
	inline Ray_t3785851493 * get_address_of_U3CrayU3E__0_1() { return &___U3CrayU3E__0_1; }
	inline void set_U3CrayU3E__0_1(Ray_t3785851493  value)
	{
		___U3CrayU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CCardU3E__0_2() { return static_cast<int32_t>(offsetof(U3COnCardBtnClickEU3Ec__Iterator1_t1416119937, ___U3CCardU3E__0_2)); }
	inline GameObject_t1113636619 * get_U3CCardU3E__0_2() const { return ___U3CCardU3E__0_2; }
	inline GameObject_t1113636619 ** get_address_of_U3CCardU3E__0_2() { return &___U3CCardU3E__0_2; }
	inline void set_U3CCardU3E__0_2(GameObject_t1113636619 * value)
	{
		___U3CCardU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCardU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CsmallestDistanceU3E__0_3() { return static_cast<int32_t>(offsetof(U3COnCardBtnClickEU3Ec__Iterator1_t1416119937, ___U3CsmallestDistanceU3E__0_3)); }
	inline float get_U3CsmallestDistanceU3E__0_3() const { return ___U3CsmallestDistanceU3E__0_3; }
	inline float* get_address_of_U3CsmallestDistanceU3E__0_3() { return &___U3CsmallestDistanceU3E__0_3; }
	inline void set_U3CsmallestDistanceU3E__0_3(float value)
	{
		___U3CsmallestDistanceU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CnearestCardPosU3E__0_4() { return static_cast<int32_t>(offsetof(U3COnCardBtnClickEU3Ec__Iterator1_t1416119937, ___U3CnearestCardPosU3E__0_4)); }
	inline int32_t get_U3CnearestCardPosU3E__0_4() const { return ___U3CnearestCardPosU3E__0_4; }
	inline int32_t* get_address_of_U3CnearestCardPosU3E__0_4() { return &___U3CnearestCardPosU3E__0_4; }
	inline void set_U3CnearestCardPosU3E__0_4(int32_t value)
	{
		___U3CnearestCardPosU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3COnCardBtnClickEU3Ec__Iterator1_t1416119937, ___U24this_5)); }
	inline PlaneVisualizationManager_t1447224982 * get_U24this_5() const { return ___U24this_5; }
	inline PlaneVisualizationManager_t1447224982 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(PlaneVisualizationManager_t1447224982 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3COnCardBtnClickEU3Ec__Iterator1_t1416119937, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3COnCardBtnClickEU3Ec__Iterator1_t1416119937, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3COnCardBtnClickEU3Ec__Iterator1_t1416119937, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONCARDBTNCLICKEU3EC__ITERATOR1_T1416119937_H
#ifndef DEFAULTS_T3148213711_H
#define DEFAULTS_T3148213711_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iTween/Defaults
struct  Defaults_t3148213711  : public RuntimeObject
{
public:

public:
};

struct Defaults_t3148213711_StaticFields
{
public:
	// System.Single iTween/Defaults::time
	float ___time_0;
	// System.Single iTween/Defaults::delay
	float ___delay_1;
	// iTween/NamedValueColor iTween/Defaults::namedColorValue
	int32_t ___namedColorValue_2;
	// iTween/LoopType iTween/Defaults::loopType
	int32_t ___loopType_3;
	// iTween/EaseType iTween/Defaults::easeType
	int32_t ___easeType_4;
	// System.Single iTween/Defaults::lookSpeed
	float ___lookSpeed_5;
	// System.Boolean iTween/Defaults::isLocal
	bool ___isLocal_6;
	// UnityEngine.Space iTween/Defaults::space
	int32_t ___space_7;
	// System.Boolean iTween/Defaults::orientToPath
	bool ___orientToPath_8;
	// UnityEngine.Color iTween/Defaults::color
	Color_t2555686324  ___color_9;
	// System.Single iTween/Defaults::updateTimePercentage
	float ___updateTimePercentage_10;
	// System.Single iTween/Defaults::updateTime
	float ___updateTime_11;
	// System.Single iTween/Defaults::lookAhead
	float ___lookAhead_12;
	// System.Boolean iTween/Defaults::useRealTime
	bool ___useRealTime_13;
	// UnityEngine.Vector3 iTween/Defaults::up
	Vector3_t3722313464  ___up_14;

public:
	inline static int32_t get_offset_of_time_0() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___time_0)); }
	inline float get_time_0() const { return ___time_0; }
	inline float* get_address_of_time_0() { return &___time_0; }
	inline void set_time_0(float value)
	{
		___time_0 = value;
	}

	inline static int32_t get_offset_of_delay_1() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___delay_1)); }
	inline float get_delay_1() const { return ___delay_1; }
	inline float* get_address_of_delay_1() { return &___delay_1; }
	inline void set_delay_1(float value)
	{
		___delay_1 = value;
	}

	inline static int32_t get_offset_of_namedColorValue_2() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___namedColorValue_2)); }
	inline int32_t get_namedColorValue_2() const { return ___namedColorValue_2; }
	inline int32_t* get_address_of_namedColorValue_2() { return &___namedColorValue_2; }
	inline void set_namedColorValue_2(int32_t value)
	{
		___namedColorValue_2 = value;
	}

	inline static int32_t get_offset_of_loopType_3() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___loopType_3)); }
	inline int32_t get_loopType_3() const { return ___loopType_3; }
	inline int32_t* get_address_of_loopType_3() { return &___loopType_3; }
	inline void set_loopType_3(int32_t value)
	{
		___loopType_3 = value;
	}

	inline static int32_t get_offset_of_easeType_4() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___easeType_4)); }
	inline int32_t get_easeType_4() const { return ___easeType_4; }
	inline int32_t* get_address_of_easeType_4() { return &___easeType_4; }
	inline void set_easeType_4(int32_t value)
	{
		___easeType_4 = value;
	}

	inline static int32_t get_offset_of_lookSpeed_5() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___lookSpeed_5)); }
	inline float get_lookSpeed_5() const { return ___lookSpeed_5; }
	inline float* get_address_of_lookSpeed_5() { return &___lookSpeed_5; }
	inline void set_lookSpeed_5(float value)
	{
		___lookSpeed_5 = value;
	}

	inline static int32_t get_offset_of_isLocal_6() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___isLocal_6)); }
	inline bool get_isLocal_6() const { return ___isLocal_6; }
	inline bool* get_address_of_isLocal_6() { return &___isLocal_6; }
	inline void set_isLocal_6(bool value)
	{
		___isLocal_6 = value;
	}

	inline static int32_t get_offset_of_space_7() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___space_7)); }
	inline int32_t get_space_7() const { return ___space_7; }
	inline int32_t* get_address_of_space_7() { return &___space_7; }
	inline void set_space_7(int32_t value)
	{
		___space_7 = value;
	}

	inline static int32_t get_offset_of_orientToPath_8() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___orientToPath_8)); }
	inline bool get_orientToPath_8() const { return ___orientToPath_8; }
	inline bool* get_address_of_orientToPath_8() { return &___orientToPath_8; }
	inline void set_orientToPath_8(bool value)
	{
		___orientToPath_8 = value;
	}

	inline static int32_t get_offset_of_color_9() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___color_9)); }
	inline Color_t2555686324  get_color_9() const { return ___color_9; }
	inline Color_t2555686324 * get_address_of_color_9() { return &___color_9; }
	inline void set_color_9(Color_t2555686324  value)
	{
		___color_9 = value;
	}

	inline static int32_t get_offset_of_updateTimePercentage_10() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___updateTimePercentage_10)); }
	inline float get_updateTimePercentage_10() const { return ___updateTimePercentage_10; }
	inline float* get_address_of_updateTimePercentage_10() { return &___updateTimePercentage_10; }
	inline void set_updateTimePercentage_10(float value)
	{
		___updateTimePercentage_10 = value;
	}

	inline static int32_t get_offset_of_updateTime_11() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___updateTime_11)); }
	inline float get_updateTime_11() const { return ___updateTime_11; }
	inline float* get_address_of_updateTime_11() { return &___updateTime_11; }
	inline void set_updateTime_11(float value)
	{
		___updateTime_11 = value;
	}

	inline static int32_t get_offset_of_lookAhead_12() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___lookAhead_12)); }
	inline float get_lookAhead_12() const { return ___lookAhead_12; }
	inline float* get_address_of_lookAhead_12() { return &___lookAhead_12; }
	inline void set_lookAhead_12(float value)
	{
		___lookAhead_12 = value;
	}

	inline static int32_t get_offset_of_useRealTime_13() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___useRealTime_13)); }
	inline bool get_useRealTime_13() const { return ___useRealTime_13; }
	inline bool* get_address_of_useRealTime_13() { return &___useRealTime_13; }
	inline void set_useRealTime_13(bool value)
	{
		___useRealTime_13 = value;
	}

	inline static int32_t get_offset_of_up_14() { return static_cast<int32_t>(offsetof(Defaults_t3148213711_StaticFields, ___up_14)); }
	inline Vector3_t3722313464  get_up_14() const { return ___up_14; }
	inline Vector3_t3722313464 * get_address_of_up_14() { return &___up_14; }
	inline void set_up_14(Vector3_t3722313464  value)
	{
		___up_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTS_T3148213711_H
#ifndef VECTOR3ANDSPACE_T219844479_H
#define VECTOR3ANDSPACE_T219844479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace
struct  Vector3andSpace_t219844479  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace::value
	Vector3_t3722313464  ___value_0;
	// UnityEngine.Space UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace::space
	int32_t ___space_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Vector3andSpace_t219844479, ___value_0)); }
	inline Vector3_t3722313464  get_value_0() const { return ___value_0; }
	inline Vector3_t3722313464 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector3_t3722313464  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_space_1() { return static_cast<int32_t>(offsetof(Vector3andSpace_t219844479, ___space_1)); }
	inline int32_t get_space_1() const { return ___space_1; }
	inline int32_t* get_address_of_space_1() { return &___space_1; }
	inline void set_space_1(int32_t value)
	{
		___space_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3ANDSPACE_T219844479_H
#ifndef U3CDRAGOBJECTU3EC__ITERATOR0_T4151609119_H
#define U3CDRAGOBJECTU3EC__ITERATOR0_T4151609119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0
struct  U3CDragObjectU3Ec__Iterator0_t4151609119  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::<oldDrag>__0
	float ___U3ColdDragU3E__0_0;
	// System.Single UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::<oldAngularDrag>__0
	float ___U3ColdAngularDragU3E__0_1;
	// UnityEngine.Camera UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::<mainCamera>__0
	Camera_t4157153871 * ___U3CmainCameraU3E__0_2;
	// UnityEngine.Ray UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::<ray>__1
	Ray_t3785851493  ___U3CrayU3E__1_3;
	// System.Single UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::distance
	float ___distance_4;
	// UnityStandardAssets.Utility.DragRigidbody UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::$this
	DragRigidbody_t1600652016 * ___U24this_5;
	// System.Object UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3ColdDragU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U3ColdDragU3E__0_0)); }
	inline float get_U3ColdDragU3E__0_0() const { return ___U3ColdDragU3E__0_0; }
	inline float* get_address_of_U3ColdDragU3E__0_0() { return &___U3ColdDragU3E__0_0; }
	inline void set_U3ColdDragU3E__0_0(float value)
	{
		___U3ColdDragU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3ColdAngularDragU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U3ColdAngularDragU3E__0_1)); }
	inline float get_U3ColdAngularDragU3E__0_1() const { return ___U3ColdAngularDragU3E__0_1; }
	inline float* get_address_of_U3ColdAngularDragU3E__0_1() { return &___U3ColdAngularDragU3E__0_1; }
	inline void set_U3ColdAngularDragU3E__0_1(float value)
	{
		___U3ColdAngularDragU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CmainCameraU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U3CmainCameraU3E__0_2)); }
	inline Camera_t4157153871 * get_U3CmainCameraU3E__0_2() const { return ___U3CmainCameraU3E__0_2; }
	inline Camera_t4157153871 ** get_address_of_U3CmainCameraU3E__0_2() { return &___U3CmainCameraU3E__0_2; }
	inline void set_U3CmainCameraU3E__0_2(Camera_t4157153871 * value)
	{
		___U3CmainCameraU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmainCameraU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CrayU3E__1_3() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U3CrayU3E__1_3)); }
	inline Ray_t3785851493  get_U3CrayU3E__1_3() const { return ___U3CrayU3E__1_3; }
	inline Ray_t3785851493 * get_address_of_U3CrayU3E__1_3() { return &___U3CrayU3E__1_3; }
	inline void set_U3CrayU3E__1_3(Ray_t3785851493  value)
	{
		___U3CrayU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_distance_4() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___distance_4)); }
	inline float get_distance_4() const { return ___distance_4; }
	inline float* get_address_of_distance_4() { return &___distance_4; }
	inline void set_distance_4(float value)
	{
		___distance_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U24this_5)); }
	inline DragRigidbody_t1600652016 * get_U24this_5() const { return ___U24this_5; }
	inline DragRigidbody_t1600652016 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(DragRigidbody_t1600652016 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t4151609119, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDRAGOBJECTU3EC__ITERATOR0_T4151609119_H
#ifndef AXISMAPPING_T3982445645_H
#define AXISMAPPING_T3982445645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping
struct  AxisMapping_t3982445645  : public RuntimeObject
{
public:
	// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping::type
	int32_t ___type_0;
	// System.String UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping::axisName
	String_t* ___axisName_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(AxisMapping_t3982445645, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_axisName_1() { return static_cast<int32_t>(offsetof(AxisMapping_t3982445645, ___axisName_1)); }
	inline String_t* get_axisName_1() const { return ___axisName_1; }
	inline String_t** get_address_of_axisName_1() { return &___axisName_1; }
	inline void set_axisName_1(String_t* value)
	{
		___axisName_1 = value;
		Il2CppCodeGenWriteBarrier((&___axisName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISMAPPING_T3982445645_H
#ifndef STANDALONEINPUT_T1343950252_H
#define STANDALONEINPUT_T1343950252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput
struct  StandaloneInput_t1343950252  : public VirtualInput_t2597455733
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDALONEINPUT_T1343950252_H
#ifndef ENTRY_T2725803170_H
#define ENTRY_T2725803170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/Entry
struct  Entry_t2725803170  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityStandardAssets.Utility.TimedObjectActivator/Entry::target
	GameObject_t1113636619 * ___target_0;
	// UnityStandardAssets.Utility.TimedObjectActivator/Action UnityStandardAssets.Utility.TimedObjectActivator/Entry::action
	int32_t ___action_1;
	// System.Single UnityStandardAssets.Utility.TimedObjectActivator/Entry::delay
	float ___delay_2;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(Entry_t2725803170, ___target_0)); }
	inline GameObject_t1113636619 * get_target_0() const { return ___target_0; }
	inline GameObject_t1113636619 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(GameObject_t1113636619 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(Entry_t2725803170, ___action_1)); }
	inline int32_t get_action_1() const { return ___action_1; }
	inline int32_t* get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(int32_t value)
	{
		___action_1 = value;
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(Entry_t2725803170, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T2725803170_H
#ifndef MOBILEINPUT_T2025745297_H
#define MOBILEINPUT_T2025745297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput
struct  MobileInput_t2025745297  : public VirtualInput_t2597455733
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEINPUT_T2025745297_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef STATEMACHINEBEHAVIOUR_T957311111_H
#define STATEMACHINEBEHAVIOUR_T957311111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.StateMachineBehaviour
struct  StateMachineBehaviour_t957311111  : public ScriptableObject_t2528358522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEMACHINEBEHAVIOUR_T957311111_H
#ifndef ANIMATIONSEVENTS_T1170748522_H
#define ANIMATIONSEVENTS_T1170748522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationsEvents
struct  AnimationsEvents_t1170748522  : public StateMachineBehaviour_t957311111
{
public:
	// System.String AnimationsEvents::StateName
	String_t* ___StateName_2;
	// System.Single AnimationsEvents::timeBeforeStateComplete
	float ___timeBeforeStateComplete_3;
	// System.Boolean AnimationsEvents::isFadeIn
	bool ___isFadeIn_4;

public:
	inline static int32_t get_offset_of_StateName_2() { return static_cast<int32_t>(offsetof(AnimationsEvents_t1170748522, ___StateName_2)); }
	inline String_t* get_StateName_2() const { return ___StateName_2; }
	inline String_t** get_address_of_StateName_2() { return &___StateName_2; }
	inline void set_StateName_2(String_t* value)
	{
		___StateName_2 = value;
		Il2CppCodeGenWriteBarrier((&___StateName_2), value);
	}

	inline static int32_t get_offset_of_timeBeforeStateComplete_3() { return static_cast<int32_t>(offsetof(AnimationsEvents_t1170748522, ___timeBeforeStateComplete_3)); }
	inline float get_timeBeforeStateComplete_3() const { return ___timeBeforeStateComplete_3; }
	inline float* get_address_of_timeBeforeStateComplete_3() { return &___timeBeforeStateComplete_3; }
	inline void set_timeBeforeStateComplete_3(float value)
	{
		___timeBeforeStateComplete_3 = value;
	}

	inline static int32_t get_offset_of_isFadeIn_4() { return static_cast<int32_t>(offsetof(AnimationsEvents_t1170748522, ___isFadeIn_4)); }
	inline bool get_isFadeIn_4() const { return ___isFadeIn_4; }
	inline bool* get_address_of_isFadeIn_4() { return &___isFadeIn_4; }
	inline void set_isFadeIn_4(bool value)
	{
		___isFadeIn_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONSEVENTS_T1170748522_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef TIMEDOBJECTDESTRUCTOR_T3438860414_H
#define TIMEDOBJECTDESTRUCTOR_T3438860414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectDestructor
struct  TimedObjectDestructor_t3438860414  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Utility.TimedObjectDestructor::m_TimeOut
	float ___m_TimeOut_2;
	// System.Boolean UnityStandardAssets.Utility.TimedObjectDestructor::m_DetachChildren
	bool ___m_DetachChildren_3;

public:
	inline static int32_t get_offset_of_m_TimeOut_2() { return static_cast<int32_t>(offsetof(TimedObjectDestructor_t3438860414, ___m_TimeOut_2)); }
	inline float get_m_TimeOut_2() const { return ___m_TimeOut_2; }
	inline float* get_address_of_m_TimeOut_2() { return &___m_TimeOut_2; }
	inline void set_m_TimeOut_2(float value)
	{
		___m_TimeOut_2 = value;
	}

	inline static int32_t get_offset_of_m_DetachChildren_3() { return static_cast<int32_t>(offsetof(TimedObjectDestructor_t3438860414, ___m_DetachChildren_3)); }
	inline bool get_m_DetachChildren_3() const { return ___m_DetachChildren_3; }
	inline bool* get_address_of_m_DetachChildren_3() { return &___m_DetachChildren_3; }
	inline void set_m_DetachChildren_3(bool value)
	{
		___m_DetachChildren_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEDOBJECTDESTRUCTOR_T3438860414_H
#ifndef TOUCHPAD_T539039257_H
#define TOUCHPAD_T539039257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad
struct  TouchPad_t539039257  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption UnityStandardAssets.CrossPlatformInput.TouchPad::axesToUse
	int32_t ___axesToUse_2;
	// UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle UnityStandardAssets.CrossPlatformInput.TouchPad::controlStyle
	int32_t ___controlStyle_3;
	// System.String UnityStandardAssets.CrossPlatformInput.TouchPad::horizontalAxisName
	String_t* ___horizontalAxisName_4;
	// System.String UnityStandardAssets.CrossPlatformInput.TouchPad::verticalAxisName
	String_t* ___verticalAxisName_5;
	// System.Single UnityStandardAssets.CrossPlatformInput.TouchPad::Xsensitivity
	float ___Xsensitivity_6;
	// System.Single UnityStandardAssets.CrossPlatformInput.TouchPad::Ysensitivity
	float ___Ysensitivity_7;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_StartPos
	Vector3_t3722313464  ___m_StartPos_8;
	// UnityEngine.Vector2 UnityStandardAssets.CrossPlatformInput.TouchPad::m_PreviousDelta
	Vector2_t2156229523  ___m_PreviousDelta_9;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_JoytickOutput
	Vector3_t3722313464  ___m_JoytickOutput_10;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_UseX
	bool ___m_UseX_11;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_UseY
	bool ___m_UseY_12;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.TouchPad::m_HorizontalVirtualAxis
	VirtualAxis_t4087348596 * ___m_HorizontalVirtualAxis_13;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.TouchPad::m_VerticalVirtualAxis
	VirtualAxis_t4087348596 * ___m_VerticalVirtualAxis_14;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_Dragging
	bool ___m_Dragging_15;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad::m_Id
	int32_t ___m_Id_16;
	// UnityEngine.Vector2 UnityStandardAssets.CrossPlatformInput.TouchPad::m_PreviousTouchPos
	Vector2_t2156229523  ___m_PreviousTouchPos_17;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_Center
	Vector3_t3722313464  ___m_Center_18;
	// UnityEngine.UI.Image UnityStandardAssets.CrossPlatformInput.TouchPad::m_Image
	Image_t2670269651 * ___m_Image_19;

public:
	inline static int32_t get_offset_of_axesToUse_2() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___axesToUse_2)); }
	inline int32_t get_axesToUse_2() const { return ___axesToUse_2; }
	inline int32_t* get_address_of_axesToUse_2() { return &___axesToUse_2; }
	inline void set_axesToUse_2(int32_t value)
	{
		___axesToUse_2 = value;
	}

	inline static int32_t get_offset_of_controlStyle_3() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___controlStyle_3)); }
	inline int32_t get_controlStyle_3() const { return ___controlStyle_3; }
	inline int32_t* get_address_of_controlStyle_3() { return &___controlStyle_3; }
	inline void set_controlStyle_3(int32_t value)
	{
		___controlStyle_3 = value;
	}

	inline static int32_t get_offset_of_horizontalAxisName_4() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___horizontalAxisName_4)); }
	inline String_t* get_horizontalAxisName_4() const { return ___horizontalAxisName_4; }
	inline String_t** get_address_of_horizontalAxisName_4() { return &___horizontalAxisName_4; }
	inline void set_horizontalAxisName_4(String_t* value)
	{
		___horizontalAxisName_4 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxisName_4), value);
	}

	inline static int32_t get_offset_of_verticalAxisName_5() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___verticalAxisName_5)); }
	inline String_t* get_verticalAxisName_5() const { return ___verticalAxisName_5; }
	inline String_t** get_address_of_verticalAxisName_5() { return &___verticalAxisName_5; }
	inline void set_verticalAxisName_5(String_t* value)
	{
		___verticalAxisName_5 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxisName_5), value);
	}

	inline static int32_t get_offset_of_Xsensitivity_6() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___Xsensitivity_6)); }
	inline float get_Xsensitivity_6() const { return ___Xsensitivity_6; }
	inline float* get_address_of_Xsensitivity_6() { return &___Xsensitivity_6; }
	inline void set_Xsensitivity_6(float value)
	{
		___Xsensitivity_6 = value;
	}

	inline static int32_t get_offset_of_Ysensitivity_7() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___Ysensitivity_7)); }
	inline float get_Ysensitivity_7() const { return ___Ysensitivity_7; }
	inline float* get_address_of_Ysensitivity_7() { return &___Ysensitivity_7; }
	inline void set_Ysensitivity_7(float value)
	{
		___Ysensitivity_7 = value;
	}

	inline static int32_t get_offset_of_m_StartPos_8() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_StartPos_8)); }
	inline Vector3_t3722313464  get_m_StartPos_8() const { return ___m_StartPos_8; }
	inline Vector3_t3722313464 * get_address_of_m_StartPos_8() { return &___m_StartPos_8; }
	inline void set_m_StartPos_8(Vector3_t3722313464  value)
	{
		___m_StartPos_8 = value;
	}

	inline static int32_t get_offset_of_m_PreviousDelta_9() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_PreviousDelta_9)); }
	inline Vector2_t2156229523  get_m_PreviousDelta_9() const { return ___m_PreviousDelta_9; }
	inline Vector2_t2156229523 * get_address_of_m_PreviousDelta_9() { return &___m_PreviousDelta_9; }
	inline void set_m_PreviousDelta_9(Vector2_t2156229523  value)
	{
		___m_PreviousDelta_9 = value;
	}

	inline static int32_t get_offset_of_m_JoytickOutput_10() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_JoytickOutput_10)); }
	inline Vector3_t3722313464  get_m_JoytickOutput_10() const { return ___m_JoytickOutput_10; }
	inline Vector3_t3722313464 * get_address_of_m_JoytickOutput_10() { return &___m_JoytickOutput_10; }
	inline void set_m_JoytickOutput_10(Vector3_t3722313464  value)
	{
		___m_JoytickOutput_10 = value;
	}

	inline static int32_t get_offset_of_m_UseX_11() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_UseX_11)); }
	inline bool get_m_UseX_11() const { return ___m_UseX_11; }
	inline bool* get_address_of_m_UseX_11() { return &___m_UseX_11; }
	inline void set_m_UseX_11(bool value)
	{
		___m_UseX_11 = value;
	}

	inline static int32_t get_offset_of_m_UseY_12() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_UseY_12)); }
	inline bool get_m_UseY_12() const { return ___m_UseY_12; }
	inline bool* get_address_of_m_UseY_12() { return &___m_UseY_12; }
	inline void set_m_UseY_12(bool value)
	{
		___m_UseY_12 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalVirtualAxis_13() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_HorizontalVirtualAxis_13)); }
	inline VirtualAxis_t4087348596 * get_m_HorizontalVirtualAxis_13() const { return ___m_HorizontalVirtualAxis_13; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_HorizontalVirtualAxis_13() { return &___m_HorizontalVirtualAxis_13; }
	inline void set_m_HorizontalVirtualAxis_13(VirtualAxis_t4087348596 * value)
	{
		___m_HorizontalVirtualAxis_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalVirtualAxis_13), value);
	}

	inline static int32_t get_offset_of_m_VerticalVirtualAxis_14() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_VerticalVirtualAxis_14)); }
	inline VirtualAxis_t4087348596 * get_m_VerticalVirtualAxis_14() const { return ___m_VerticalVirtualAxis_14; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_VerticalVirtualAxis_14() { return &___m_VerticalVirtualAxis_14; }
	inline void set_m_VerticalVirtualAxis_14(VirtualAxis_t4087348596 * value)
	{
		___m_VerticalVirtualAxis_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalVirtualAxis_14), value);
	}

	inline static int32_t get_offset_of_m_Dragging_15() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_Dragging_15)); }
	inline bool get_m_Dragging_15() const { return ___m_Dragging_15; }
	inline bool* get_address_of_m_Dragging_15() { return &___m_Dragging_15; }
	inline void set_m_Dragging_15(bool value)
	{
		___m_Dragging_15 = value;
	}

	inline static int32_t get_offset_of_m_Id_16() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_Id_16)); }
	inline int32_t get_m_Id_16() const { return ___m_Id_16; }
	inline int32_t* get_address_of_m_Id_16() { return &___m_Id_16; }
	inline void set_m_Id_16(int32_t value)
	{
		___m_Id_16 = value;
	}

	inline static int32_t get_offset_of_m_PreviousTouchPos_17() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_PreviousTouchPos_17)); }
	inline Vector2_t2156229523  get_m_PreviousTouchPos_17() const { return ___m_PreviousTouchPos_17; }
	inline Vector2_t2156229523 * get_address_of_m_PreviousTouchPos_17() { return &___m_PreviousTouchPos_17; }
	inline void set_m_PreviousTouchPos_17(Vector2_t2156229523  value)
	{
		___m_PreviousTouchPos_17 = value;
	}

	inline static int32_t get_offset_of_m_Center_18() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_Center_18)); }
	inline Vector3_t3722313464  get_m_Center_18() const { return ___m_Center_18; }
	inline Vector3_t3722313464 * get_address_of_m_Center_18() { return &___m_Center_18; }
	inline void set_m_Center_18(Vector3_t3722313464  value)
	{
		___m_Center_18 = value;
	}

	inline static int32_t get_offset_of_m_Image_19() { return static_cast<int32_t>(offsetof(TouchPad_t539039257, ___m_Image_19)); }
	inline Image_t2670269651 * get_m_Image_19() const { return ___m_Image_19; }
	inline Image_t2670269651 ** get_address_of_m_Image_19() { return &___m_Image_19; }
	inline void set_m_Image_19(Image_t2670269651 * value)
	{
		___m_Image_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_Image_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPAD_T539039257_H
#ifndef SOUNDMANAGER_T2102329059_H
#define SOUNDMANAGER_T2102329059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundManager
struct  SoundManager_t2102329059  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioSource SoundManager::Chocking
	AudioSource_t3935305588 * ___Chocking_3;
	// UnityEngine.AudioSource SoundManager::DeathBiteAndChew
	AudioSource_t3935305588 * ___DeathBiteAndChew_4;
	// UnityEngine.AudioSource SoundManager::DanceTheWorld
	AudioSource_t3935305588 * ___DanceTheWorld_5;
	// UnityEngine.AudioSource SoundManager::TwoOfSwords
	AudioSource_t3935305588 * ___TwoOfSwords_6;
	// UnityEngine.AudioSource SoundManager::Shuffling
	AudioSource_t3935305588 * ___Shuffling_7;

public:
	inline static int32_t get_offset_of_Chocking_3() { return static_cast<int32_t>(offsetof(SoundManager_t2102329059, ___Chocking_3)); }
	inline AudioSource_t3935305588 * get_Chocking_3() const { return ___Chocking_3; }
	inline AudioSource_t3935305588 ** get_address_of_Chocking_3() { return &___Chocking_3; }
	inline void set_Chocking_3(AudioSource_t3935305588 * value)
	{
		___Chocking_3 = value;
		Il2CppCodeGenWriteBarrier((&___Chocking_3), value);
	}

	inline static int32_t get_offset_of_DeathBiteAndChew_4() { return static_cast<int32_t>(offsetof(SoundManager_t2102329059, ___DeathBiteAndChew_4)); }
	inline AudioSource_t3935305588 * get_DeathBiteAndChew_4() const { return ___DeathBiteAndChew_4; }
	inline AudioSource_t3935305588 ** get_address_of_DeathBiteAndChew_4() { return &___DeathBiteAndChew_4; }
	inline void set_DeathBiteAndChew_4(AudioSource_t3935305588 * value)
	{
		___DeathBiteAndChew_4 = value;
		Il2CppCodeGenWriteBarrier((&___DeathBiteAndChew_4), value);
	}

	inline static int32_t get_offset_of_DanceTheWorld_5() { return static_cast<int32_t>(offsetof(SoundManager_t2102329059, ___DanceTheWorld_5)); }
	inline AudioSource_t3935305588 * get_DanceTheWorld_5() const { return ___DanceTheWorld_5; }
	inline AudioSource_t3935305588 ** get_address_of_DanceTheWorld_5() { return &___DanceTheWorld_5; }
	inline void set_DanceTheWorld_5(AudioSource_t3935305588 * value)
	{
		___DanceTheWorld_5 = value;
		Il2CppCodeGenWriteBarrier((&___DanceTheWorld_5), value);
	}

	inline static int32_t get_offset_of_TwoOfSwords_6() { return static_cast<int32_t>(offsetof(SoundManager_t2102329059, ___TwoOfSwords_6)); }
	inline AudioSource_t3935305588 * get_TwoOfSwords_6() const { return ___TwoOfSwords_6; }
	inline AudioSource_t3935305588 ** get_address_of_TwoOfSwords_6() { return &___TwoOfSwords_6; }
	inline void set_TwoOfSwords_6(AudioSource_t3935305588 * value)
	{
		___TwoOfSwords_6 = value;
		Il2CppCodeGenWriteBarrier((&___TwoOfSwords_6), value);
	}

	inline static int32_t get_offset_of_Shuffling_7() { return static_cast<int32_t>(offsetof(SoundManager_t2102329059, ___Shuffling_7)); }
	inline AudioSource_t3935305588 * get_Shuffling_7() const { return ___Shuffling_7; }
	inline AudioSource_t3935305588 ** get_address_of_Shuffling_7() { return &___Shuffling_7; }
	inline void set_Shuffling_7(AudioSource_t3935305588 * value)
	{
		___Shuffling_7 = value;
		Il2CppCodeGenWriteBarrier((&___Shuffling_7), value);
	}
};

struct SoundManager_t2102329059_StaticFields
{
public:
	// SoundManager SoundManager::Instance
	SoundManager_t2102329059 * ___Instance_2;

public:
	inline static int32_t get_offset_of_Instance_2() { return static_cast<int32_t>(offsetof(SoundManager_t2102329059_StaticFields, ___Instance_2)); }
	inline SoundManager_t2102329059 * get_Instance_2() const { return ___Instance_2; }
	inline SoundManager_t2102329059 ** get_address_of_Instance_2() { return &___Instance_2; }
	inline void set_Instance_2(SoundManager_t2102329059 * value)
	{
		___Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDMANAGER_T2102329059_H
#ifndef TILTINPUT_T1639936653_H
#define TILTINPUT_T1639936653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput
struct  TiltInput_t1639936653  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping UnityStandardAssets.CrossPlatformInput.TiltInput::mapping
	AxisMapping_t3982445645 * ___mapping_2;
	// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions UnityStandardAssets.CrossPlatformInput.TiltInput::tiltAroundAxis
	int32_t ___tiltAroundAxis_3;
	// System.Single UnityStandardAssets.CrossPlatformInput.TiltInput::fullTiltAngle
	float ___fullTiltAngle_4;
	// System.Single UnityStandardAssets.CrossPlatformInput.TiltInput::centreAngleOffset
	float ___centreAngleOffset_5;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.TiltInput::m_SteerAxis
	VirtualAxis_t4087348596 * ___m_SteerAxis_6;

public:
	inline static int32_t get_offset_of_mapping_2() { return static_cast<int32_t>(offsetof(TiltInput_t1639936653, ___mapping_2)); }
	inline AxisMapping_t3982445645 * get_mapping_2() const { return ___mapping_2; }
	inline AxisMapping_t3982445645 ** get_address_of_mapping_2() { return &___mapping_2; }
	inline void set_mapping_2(AxisMapping_t3982445645 * value)
	{
		___mapping_2 = value;
		Il2CppCodeGenWriteBarrier((&___mapping_2), value);
	}

	inline static int32_t get_offset_of_tiltAroundAxis_3() { return static_cast<int32_t>(offsetof(TiltInput_t1639936653, ___tiltAroundAxis_3)); }
	inline int32_t get_tiltAroundAxis_3() const { return ___tiltAroundAxis_3; }
	inline int32_t* get_address_of_tiltAroundAxis_3() { return &___tiltAroundAxis_3; }
	inline void set_tiltAroundAxis_3(int32_t value)
	{
		___tiltAroundAxis_3 = value;
	}

	inline static int32_t get_offset_of_fullTiltAngle_4() { return static_cast<int32_t>(offsetof(TiltInput_t1639936653, ___fullTiltAngle_4)); }
	inline float get_fullTiltAngle_4() const { return ___fullTiltAngle_4; }
	inline float* get_address_of_fullTiltAngle_4() { return &___fullTiltAngle_4; }
	inline void set_fullTiltAngle_4(float value)
	{
		___fullTiltAngle_4 = value;
	}

	inline static int32_t get_offset_of_centreAngleOffset_5() { return static_cast<int32_t>(offsetof(TiltInput_t1639936653, ___centreAngleOffset_5)); }
	inline float get_centreAngleOffset_5() const { return ___centreAngleOffset_5; }
	inline float* get_address_of_centreAngleOffset_5() { return &___centreAngleOffset_5; }
	inline void set_centreAngleOffset_5(float value)
	{
		___centreAngleOffset_5 = value;
	}

	inline static int32_t get_offset_of_m_SteerAxis_6() { return static_cast<int32_t>(offsetof(TiltInput_t1639936653, ___m_SteerAxis_6)); }
	inline VirtualAxis_t4087348596 * get_m_SteerAxis_6() const { return ___m_SteerAxis_6; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_SteerAxis_6() { return &___m_SteerAxis_6; }
	inline void set_m_SteerAxis_6(VirtualAxis_t4087348596 * value)
	{
		___m_SteerAxis_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_SteerAxis_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTINPUT_T1639936653_H
#ifndef MOBILECONTROLRIG_T1964600252_H
#define MOBILECONTROLRIG_T1964600252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.MobileControlRig
struct  MobileControlRig_t1964600252  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILECONTROLRIG_T1964600252_H
#ifndef BUTTONHANDLER_T823762219_H
#define BUTTONHANDLER_T823762219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.ButtonHandler
struct  ButtonHandler_t823762219  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.ButtonHandler::Name
	String_t* ___Name_2;

public:
	inline static int32_t get_offset_of_Name_2() { return static_cast<int32_t>(offsetof(ButtonHandler_t823762219, ___Name_2)); }
	inline String_t* get_Name_2() const { return ___Name_2; }
	inline String_t** get_address_of_Name_2() { return &___Name_2; }
	inline void set_Name_2(String_t* value)
	{
		___Name_2 = value;
		Il2CppCodeGenWriteBarrier((&___Name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONHANDLER_T823762219_H
#ifndef INPUTAXISSCROLLBAR_T457958266_H
#define INPUTAXISSCROLLBAR_T457958266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar
struct  InputAxisScrollbar_t457958266  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::axis
	String_t* ___axis_2;

public:
	inline static int32_t get_offset_of_axis_2() { return static_cast<int32_t>(offsetof(InputAxisScrollbar_t457958266, ___axis_2)); }
	inline String_t* get_axis_2() const { return ___axis_2; }
	inline String_t** get_address_of_axis_2() { return &___axis_2; }
	inline void set_axis_2(String_t* value)
	{
		___axis_2 = value;
		Il2CppCodeGenWriteBarrier((&___axis_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTAXISSCROLLBAR_T457958266_H
#ifndef AXISTOUCHBUTTON_T3522881333_H
#define AXISTOUCHBUTTON_T3522881333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.AxisTouchButton
struct  AxisTouchButton_t3522881333  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.AxisTouchButton::axisName
	String_t* ___axisName_2;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::axisValue
	float ___axisValue_3;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::responseSpeed
	float ___responseSpeed_4;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::returnToCentreSpeed
	float ___returnToCentreSpeed_5;
	// UnityStandardAssets.CrossPlatformInput.AxisTouchButton UnityStandardAssets.CrossPlatformInput.AxisTouchButton::m_PairedWith
	AxisTouchButton_t3522881333 * ___m_PairedWith_6;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.AxisTouchButton::m_Axis
	VirtualAxis_t4087348596 * ___m_Axis_7;

public:
	inline static int32_t get_offset_of_axisName_2() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___axisName_2)); }
	inline String_t* get_axisName_2() const { return ___axisName_2; }
	inline String_t** get_address_of_axisName_2() { return &___axisName_2; }
	inline void set_axisName_2(String_t* value)
	{
		___axisName_2 = value;
		Il2CppCodeGenWriteBarrier((&___axisName_2), value);
	}

	inline static int32_t get_offset_of_axisValue_3() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___axisValue_3)); }
	inline float get_axisValue_3() const { return ___axisValue_3; }
	inline float* get_address_of_axisValue_3() { return &___axisValue_3; }
	inline void set_axisValue_3(float value)
	{
		___axisValue_3 = value;
	}

	inline static int32_t get_offset_of_responseSpeed_4() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___responseSpeed_4)); }
	inline float get_responseSpeed_4() const { return ___responseSpeed_4; }
	inline float* get_address_of_responseSpeed_4() { return &___responseSpeed_4; }
	inline void set_responseSpeed_4(float value)
	{
		___responseSpeed_4 = value;
	}

	inline static int32_t get_offset_of_returnToCentreSpeed_5() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___returnToCentreSpeed_5)); }
	inline float get_returnToCentreSpeed_5() const { return ___returnToCentreSpeed_5; }
	inline float* get_address_of_returnToCentreSpeed_5() { return &___returnToCentreSpeed_5; }
	inline void set_returnToCentreSpeed_5(float value)
	{
		___returnToCentreSpeed_5 = value;
	}

	inline static int32_t get_offset_of_m_PairedWith_6() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___m_PairedWith_6)); }
	inline AxisTouchButton_t3522881333 * get_m_PairedWith_6() const { return ___m_PairedWith_6; }
	inline AxisTouchButton_t3522881333 ** get_address_of_m_PairedWith_6() { return &___m_PairedWith_6; }
	inline void set_m_PairedWith_6(AxisTouchButton_t3522881333 * value)
	{
		___m_PairedWith_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_PairedWith_6), value);
	}

	inline static int32_t get_offset_of_m_Axis_7() { return static_cast<int32_t>(offsetof(AxisTouchButton_t3522881333, ___m_Axis_7)); }
	inline VirtualAxis_t4087348596 * get_m_Axis_7() const { return ___m_Axis_7; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_Axis_7() { return &___m_Axis_7; }
	inline void set_m_Axis_7(VirtualAxis_t4087348596 * value)
	{
		___m_Axis_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Axis_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISTOUCHBUTTON_T3522881333_H
#ifndef THIRDPERSONUSERCONTROL_T1527285130_H
#define THIRDPERSONUSERCONTROL_T1527285130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl
struct  ThirdPersonUserControl_t1527285130  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::m_Character
	ThirdPersonCharacter_t1711070432 * ___m_Character_2;
	// UnityEngine.Transform UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::m_Cam
	Transform_t3600365921 * ___m_Cam_3;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::m_CamForward
	Vector3_t3722313464  ___m_CamForward_4;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::m_Move
	Vector3_t3722313464  ___m_Move_5;
	// System.Boolean UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::m_Jump
	bool ___m_Jump_6;

public:
	inline static int32_t get_offset_of_m_Character_2() { return static_cast<int32_t>(offsetof(ThirdPersonUserControl_t1527285130, ___m_Character_2)); }
	inline ThirdPersonCharacter_t1711070432 * get_m_Character_2() const { return ___m_Character_2; }
	inline ThirdPersonCharacter_t1711070432 ** get_address_of_m_Character_2() { return &___m_Character_2; }
	inline void set_m_Character_2(ThirdPersonCharacter_t1711070432 * value)
	{
		___m_Character_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Character_2), value);
	}

	inline static int32_t get_offset_of_m_Cam_3() { return static_cast<int32_t>(offsetof(ThirdPersonUserControl_t1527285130, ___m_Cam_3)); }
	inline Transform_t3600365921 * get_m_Cam_3() const { return ___m_Cam_3; }
	inline Transform_t3600365921 ** get_address_of_m_Cam_3() { return &___m_Cam_3; }
	inline void set_m_Cam_3(Transform_t3600365921 * value)
	{
		___m_Cam_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Cam_3), value);
	}

	inline static int32_t get_offset_of_m_CamForward_4() { return static_cast<int32_t>(offsetof(ThirdPersonUserControl_t1527285130, ___m_CamForward_4)); }
	inline Vector3_t3722313464  get_m_CamForward_4() const { return ___m_CamForward_4; }
	inline Vector3_t3722313464 * get_address_of_m_CamForward_4() { return &___m_CamForward_4; }
	inline void set_m_CamForward_4(Vector3_t3722313464  value)
	{
		___m_CamForward_4 = value;
	}

	inline static int32_t get_offset_of_m_Move_5() { return static_cast<int32_t>(offsetof(ThirdPersonUserControl_t1527285130, ___m_Move_5)); }
	inline Vector3_t3722313464  get_m_Move_5() const { return ___m_Move_5; }
	inline Vector3_t3722313464 * get_address_of_m_Move_5() { return &___m_Move_5; }
	inline void set_m_Move_5(Vector3_t3722313464  value)
	{
		___m_Move_5 = value;
	}

	inline static int32_t get_offset_of_m_Jump_6() { return static_cast<int32_t>(offsetof(ThirdPersonUserControl_t1527285130, ___m_Jump_6)); }
	inline bool get_m_Jump_6() const { return ___m_Jump_6; }
	inline bool* get_address_of_m_Jump_6() { return &___m_Jump_6; }
	inline void set_m_Jump_6(bool value)
	{
		___m_Jump_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THIRDPERSONUSERCONTROL_T1527285130_H
#ifndef THIRDPERSONCHARACTER_T1711070432_H
#define THIRDPERSONCHARACTER_T1711070432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter
struct  ThirdPersonCharacter_t1711070432  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_MovingTurnSpeed
	float ___m_MovingTurnSpeed_2;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_StationaryTurnSpeed
	float ___m_StationaryTurnSpeed_3;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_JumpPower
	float ___m_JumpPower_4;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_GravityMultiplier
	float ___m_GravityMultiplier_5;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_RunCycleLegOffset
	float ___m_RunCycleLegOffset_6;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_MoveSpeedMultiplier
	float ___m_MoveSpeedMultiplier_7;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_AnimSpeedMultiplier
	float ___m_AnimSpeedMultiplier_8;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_GroundCheckDistance
	float ___m_GroundCheckDistance_9;
	// UnityEngine.Rigidbody UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_Rigidbody
	Rigidbody_t3916780224 * ___m_Rigidbody_10;
	// UnityEngine.Animator UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_Animator
	Animator_t434523843 * ___m_Animator_11;
	// System.Boolean UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_IsGrounded
	bool ___m_IsGrounded_12;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_OrigGroundCheckDistance
	float ___m_OrigGroundCheckDistance_13;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_TurnAmount
	float ___m_TurnAmount_15;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_ForwardAmount
	float ___m_ForwardAmount_16;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_GroundNormal
	Vector3_t3722313464  ___m_GroundNormal_17;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_CapsuleHeight
	float ___m_CapsuleHeight_18;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_CapsuleCenter
	Vector3_t3722313464  ___m_CapsuleCenter_19;
	// UnityEngine.CapsuleCollider UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_Capsule
	CapsuleCollider_t197597763 * ___m_Capsule_20;
	// System.Boolean UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_Crouching
	bool ___m_Crouching_21;

public:
	inline static int32_t get_offset_of_m_MovingTurnSpeed_2() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_MovingTurnSpeed_2)); }
	inline float get_m_MovingTurnSpeed_2() const { return ___m_MovingTurnSpeed_2; }
	inline float* get_address_of_m_MovingTurnSpeed_2() { return &___m_MovingTurnSpeed_2; }
	inline void set_m_MovingTurnSpeed_2(float value)
	{
		___m_MovingTurnSpeed_2 = value;
	}

	inline static int32_t get_offset_of_m_StationaryTurnSpeed_3() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_StationaryTurnSpeed_3)); }
	inline float get_m_StationaryTurnSpeed_3() const { return ___m_StationaryTurnSpeed_3; }
	inline float* get_address_of_m_StationaryTurnSpeed_3() { return &___m_StationaryTurnSpeed_3; }
	inline void set_m_StationaryTurnSpeed_3(float value)
	{
		___m_StationaryTurnSpeed_3 = value;
	}

	inline static int32_t get_offset_of_m_JumpPower_4() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_JumpPower_4)); }
	inline float get_m_JumpPower_4() const { return ___m_JumpPower_4; }
	inline float* get_address_of_m_JumpPower_4() { return &___m_JumpPower_4; }
	inline void set_m_JumpPower_4(float value)
	{
		___m_JumpPower_4 = value;
	}

	inline static int32_t get_offset_of_m_GravityMultiplier_5() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_GravityMultiplier_5)); }
	inline float get_m_GravityMultiplier_5() const { return ___m_GravityMultiplier_5; }
	inline float* get_address_of_m_GravityMultiplier_5() { return &___m_GravityMultiplier_5; }
	inline void set_m_GravityMultiplier_5(float value)
	{
		___m_GravityMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_m_RunCycleLegOffset_6() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_RunCycleLegOffset_6)); }
	inline float get_m_RunCycleLegOffset_6() const { return ___m_RunCycleLegOffset_6; }
	inline float* get_address_of_m_RunCycleLegOffset_6() { return &___m_RunCycleLegOffset_6; }
	inline void set_m_RunCycleLegOffset_6(float value)
	{
		___m_RunCycleLegOffset_6 = value;
	}

	inline static int32_t get_offset_of_m_MoveSpeedMultiplier_7() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_MoveSpeedMultiplier_7)); }
	inline float get_m_MoveSpeedMultiplier_7() const { return ___m_MoveSpeedMultiplier_7; }
	inline float* get_address_of_m_MoveSpeedMultiplier_7() { return &___m_MoveSpeedMultiplier_7; }
	inline void set_m_MoveSpeedMultiplier_7(float value)
	{
		___m_MoveSpeedMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_m_AnimSpeedMultiplier_8() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_AnimSpeedMultiplier_8)); }
	inline float get_m_AnimSpeedMultiplier_8() const { return ___m_AnimSpeedMultiplier_8; }
	inline float* get_address_of_m_AnimSpeedMultiplier_8() { return &___m_AnimSpeedMultiplier_8; }
	inline void set_m_AnimSpeedMultiplier_8(float value)
	{
		___m_AnimSpeedMultiplier_8 = value;
	}

	inline static int32_t get_offset_of_m_GroundCheckDistance_9() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_GroundCheckDistance_9)); }
	inline float get_m_GroundCheckDistance_9() const { return ___m_GroundCheckDistance_9; }
	inline float* get_address_of_m_GroundCheckDistance_9() { return &___m_GroundCheckDistance_9; }
	inline void set_m_GroundCheckDistance_9(float value)
	{
		___m_GroundCheckDistance_9 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_10() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_Rigidbody_10)); }
	inline Rigidbody_t3916780224 * get_m_Rigidbody_10() const { return ___m_Rigidbody_10; }
	inline Rigidbody_t3916780224 ** get_address_of_m_Rigidbody_10() { return &___m_Rigidbody_10; }
	inline void set_m_Rigidbody_10(Rigidbody_t3916780224 * value)
	{
		___m_Rigidbody_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigidbody_10), value);
	}

	inline static int32_t get_offset_of_m_Animator_11() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_Animator_11)); }
	inline Animator_t434523843 * get_m_Animator_11() const { return ___m_Animator_11; }
	inline Animator_t434523843 ** get_address_of_m_Animator_11() { return &___m_Animator_11; }
	inline void set_m_Animator_11(Animator_t434523843 * value)
	{
		___m_Animator_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Animator_11), value);
	}

	inline static int32_t get_offset_of_m_IsGrounded_12() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_IsGrounded_12)); }
	inline bool get_m_IsGrounded_12() const { return ___m_IsGrounded_12; }
	inline bool* get_address_of_m_IsGrounded_12() { return &___m_IsGrounded_12; }
	inline void set_m_IsGrounded_12(bool value)
	{
		___m_IsGrounded_12 = value;
	}

	inline static int32_t get_offset_of_m_OrigGroundCheckDistance_13() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_OrigGroundCheckDistance_13)); }
	inline float get_m_OrigGroundCheckDistance_13() const { return ___m_OrigGroundCheckDistance_13; }
	inline float* get_address_of_m_OrigGroundCheckDistance_13() { return &___m_OrigGroundCheckDistance_13; }
	inline void set_m_OrigGroundCheckDistance_13(float value)
	{
		___m_OrigGroundCheckDistance_13 = value;
	}

	inline static int32_t get_offset_of_m_TurnAmount_15() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_TurnAmount_15)); }
	inline float get_m_TurnAmount_15() const { return ___m_TurnAmount_15; }
	inline float* get_address_of_m_TurnAmount_15() { return &___m_TurnAmount_15; }
	inline void set_m_TurnAmount_15(float value)
	{
		___m_TurnAmount_15 = value;
	}

	inline static int32_t get_offset_of_m_ForwardAmount_16() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_ForwardAmount_16)); }
	inline float get_m_ForwardAmount_16() const { return ___m_ForwardAmount_16; }
	inline float* get_address_of_m_ForwardAmount_16() { return &___m_ForwardAmount_16; }
	inline void set_m_ForwardAmount_16(float value)
	{
		___m_ForwardAmount_16 = value;
	}

	inline static int32_t get_offset_of_m_GroundNormal_17() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_GroundNormal_17)); }
	inline Vector3_t3722313464  get_m_GroundNormal_17() const { return ___m_GroundNormal_17; }
	inline Vector3_t3722313464 * get_address_of_m_GroundNormal_17() { return &___m_GroundNormal_17; }
	inline void set_m_GroundNormal_17(Vector3_t3722313464  value)
	{
		___m_GroundNormal_17 = value;
	}

	inline static int32_t get_offset_of_m_CapsuleHeight_18() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_CapsuleHeight_18)); }
	inline float get_m_CapsuleHeight_18() const { return ___m_CapsuleHeight_18; }
	inline float* get_address_of_m_CapsuleHeight_18() { return &___m_CapsuleHeight_18; }
	inline void set_m_CapsuleHeight_18(float value)
	{
		___m_CapsuleHeight_18 = value;
	}

	inline static int32_t get_offset_of_m_CapsuleCenter_19() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_CapsuleCenter_19)); }
	inline Vector3_t3722313464  get_m_CapsuleCenter_19() const { return ___m_CapsuleCenter_19; }
	inline Vector3_t3722313464 * get_address_of_m_CapsuleCenter_19() { return &___m_CapsuleCenter_19; }
	inline void set_m_CapsuleCenter_19(Vector3_t3722313464  value)
	{
		___m_CapsuleCenter_19 = value;
	}

	inline static int32_t get_offset_of_m_Capsule_20() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_Capsule_20)); }
	inline CapsuleCollider_t197597763 * get_m_Capsule_20() const { return ___m_Capsule_20; }
	inline CapsuleCollider_t197597763 ** get_address_of_m_Capsule_20() { return &___m_Capsule_20; }
	inline void set_m_Capsule_20(CapsuleCollider_t197597763 * value)
	{
		___m_Capsule_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_Capsule_20), value);
	}

	inline static int32_t get_offset_of_m_Crouching_21() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1711070432, ___m_Crouching_21)); }
	inline bool get_m_Crouching_21() const { return ___m_Crouching_21; }
	inline bool* get_address_of_m_Crouching_21() { return &___m_Crouching_21; }
	inline void set_m_Crouching_21(bool value)
	{
		___m_Crouching_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THIRDPERSONCHARACTER_T1711070432_H
#ifndef AICHARACTERCONTROL_T2972373937_H
#define AICHARACTERCONTROL_T2972373937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.ThirdPerson.AICharacterControl
struct  AICharacterControl_t2972373937  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AI.NavMeshAgent UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::<agent>k__BackingField
	NavMeshAgent_t1276799816 * ___U3CagentU3Ek__BackingField_2;
	// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::<character>k__BackingField
	ThirdPersonCharacter_t1711070432 * ___U3CcharacterU3Ek__BackingField_3;
	// UnityEngine.Transform UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::target
	Transform_t3600365921 * ___target_4;

public:
	inline static int32_t get_offset_of_U3CagentU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AICharacterControl_t2972373937, ___U3CagentU3Ek__BackingField_2)); }
	inline NavMeshAgent_t1276799816 * get_U3CagentU3Ek__BackingField_2() const { return ___U3CagentU3Ek__BackingField_2; }
	inline NavMeshAgent_t1276799816 ** get_address_of_U3CagentU3Ek__BackingField_2() { return &___U3CagentU3Ek__BackingField_2; }
	inline void set_U3CagentU3Ek__BackingField_2(NavMeshAgent_t1276799816 * value)
	{
		___U3CagentU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CagentU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CcharacterU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AICharacterControl_t2972373937, ___U3CcharacterU3Ek__BackingField_3)); }
	inline ThirdPersonCharacter_t1711070432 * get_U3CcharacterU3Ek__BackingField_3() const { return ___U3CcharacterU3Ek__BackingField_3; }
	inline ThirdPersonCharacter_t1711070432 ** get_address_of_U3CcharacterU3Ek__BackingField_3() { return &___U3CcharacterU3Ek__BackingField_3; }
	inline void set_U3CcharacterU3Ek__BackingField_3(ThirdPersonCharacter_t1711070432 * value)
	{
		___U3CcharacterU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcharacterU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(AICharacterControl_t2972373937, ___target_4)); }
	inline Transform_t3600365921 * get_target_4() const { return ___target_4; }
	inline Transform_t3600365921 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_t3600365921 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AICHARACTERCONTROL_T2972373937_H
#ifndef SAMPLEINFO_T3693012684_H
#define SAMPLEINFO_T3693012684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SampleInfo
struct  SampleInfo_t3693012684  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLEINFO_T3693012684_H
#ifndef ROTATESAMPLE_T3002381122_H
#define ROTATESAMPLE_T3002381122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotateSample
struct  RotateSample_t3002381122  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATESAMPLE_T3002381122_H
#ifndef MOVESAMPLE_T3412539464_H
#define MOVESAMPLE_T3412539464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveSample
struct  MoveSample_t3412539464  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVESAMPLE_T3412539464_H
#ifndef JOYSTICK_T2204371675_H
#define JOYSTICK_T2204371675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.Joystick
struct  Joystick_t2204371675  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.Joystick::MovementRange
	int32_t ___MovementRange_2;
	// UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption UnityStandardAssets.CrossPlatformInput.Joystick::axesToUse
	int32_t ___axesToUse_3;
	// System.String UnityStandardAssets.CrossPlatformInput.Joystick::horizontalAxisName
	String_t* ___horizontalAxisName_4;
	// System.String UnityStandardAssets.CrossPlatformInput.Joystick::verticalAxisName
	String_t* ___verticalAxisName_5;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.Joystick::m_StartPos
	Vector3_t3722313464  ___m_StartPos_6;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.Joystick::m_UseX
	bool ___m_UseX_7;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.Joystick::m_UseY
	bool ___m_UseY_8;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.Joystick::m_HorizontalVirtualAxis
	VirtualAxis_t4087348596 * ___m_HorizontalVirtualAxis_9;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.Joystick::m_VerticalVirtualAxis
	VirtualAxis_t4087348596 * ___m_VerticalVirtualAxis_10;

public:
	inline static int32_t get_offset_of_MovementRange_2() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___MovementRange_2)); }
	inline int32_t get_MovementRange_2() const { return ___MovementRange_2; }
	inline int32_t* get_address_of_MovementRange_2() { return &___MovementRange_2; }
	inline void set_MovementRange_2(int32_t value)
	{
		___MovementRange_2 = value;
	}

	inline static int32_t get_offset_of_axesToUse_3() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___axesToUse_3)); }
	inline int32_t get_axesToUse_3() const { return ___axesToUse_3; }
	inline int32_t* get_address_of_axesToUse_3() { return &___axesToUse_3; }
	inline void set_axesToUse_3(int32_t value)
	{
		___axesToUse_3 = value;
	}

	inline static int32_t get_offset_of_horizontalAxisName_4() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___horizontalAxisName_4)); }
	inline String_t* get_horizontalAxisName_4() const { return ___horizontalAxisName_4; }
	inline String_t** get_address_of_horizontalAxisName_4() { return &___horizontalAxisName_4; }
	inline void set_horizontalAxisName_4(String_t* value)
	{
		___horizontalAxisName_4 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxisName_4), value);
	}

	inline static int32_t get_offset_of_verticalAxisName_5() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___verticalAxisName_5)); }
	inline String_t* get_verticalAxisName_5() const { return ___verticalAxisName_5; }
	inline String_t** get_address_of_verticalAxisName_5() { return &___verticalAxisName_5; }
	inline void set_verticalAxisName_5(String_t* value)
	{
		___verticalAxisName_5 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxisName_5), value);
	}

	inline static int32_t get_offset_of_m_StartPos_6() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___m_StartPos_6)); }
	inline Vector3_t3722313464  get_m_StartPos_6() const { return ___m_StartPos_6; }
	inline Vector3_t3722313464 * get_address_of_m_StartPos_6() { return &___m_StartPos_6; }
	inline void set_m_StartPos_6(Vector3_t3722313464  value)
	{
		___m_StartPos_6 = value;
	}

	inline static int32_t get_offset_of_m_UseX_7() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___m_UseX_7)); }
	inline bool get_m_UseX_7() const { return ___m_UseX_7; }
	inline bool* get_address_of_m_UseX_7() { return &___m_UseX_7; }
	inline void set_m_UseX_7(bool value)
	{
		___m_UseX_7 = value;
	}

	inline static int32_t get_offset_of_m_UseY_8() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___m_UseY_8)); }
	inline bool get_m_UseY_8() const { return ___m_UseY_8; }
	inline bool* get_address_of_m_UseY_8() { return &___m_UseY_8; }
	inline void set_m_UseY_8(bool value)
	{
		___m_UseY_8 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalVirtualAxis_9() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___m_HorizontalVirtualAxis_9)); }
	inline VirtualAxis_t4087348596 * get_m_HorizontalVirtualAxis_9() const { return ___m_HorizontalVirtualAxis_9; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_HorizontalVirtualAxis_9() { return &___m_HorizontalVirtualAxis_9; }
	inline void set_m_HorizontalVirtualAxis_9(VirtualAxis_t4087348596 * value)
	{
		___m_HorizontalVirtualAxis_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalVirtualAxis_9), value);
	}

	inline static int32_t get_offset_of_m_VerticalVirtualAxis_10() { return static_cast<int32_t>(offsetof(Joystick_t2204371675, ___m_VerticalVirtualAxis_10)); }
	inline VirtualAxis_t4087348596 * get_m_VerticalVirtualAxis_10() const { return ___m_VerticalVirtualAxis_10; }
	inline VirtualAxis_t4087348596 ** get_address_of_m_VerticalVirtualAxis_10() { return &___m_VerticalVirtualAxis_10; }
	inline void set_m_VerticalVirtualAxis_10(VirtualAxis_t4087348596 * value)
	{
		___m_VerticalVirtualAxis_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalVirtualAxis_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICK_T2204371675_H
#ifndef ACTIVATETRIGGER_T3349759092_H
#define ACTIVATETRIGGER_T3349759092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ActivateTrigger
struct  ActivateTrigger_t3349759092  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.ActivateTrigger/Mode UnityStandardAssets.Utility.ActivateTrigger::action
	int32_t ___action_2;
	// UnityEngine.Object UnityStandardAssets.Utility.ActivateTrigger::target
	Object_t631007953 * ___target_3;
	// UnityEngine.GameObject UnityStandardAssets.Utility.ActivateTrigger::source
	GameObject_t1113636619 * ___source_4;
	// System.Int32 UnityStandardAssets.Utility.ActivateTrigger::triggerCount
	int32_t ___triggerCount_5;
	// System.Boolean UnityStandardAssets.Utility.ActivateTrigger::repeatTrigger
	bool ___repeatTrigger_6;

public:
	inline static int32_t get_offset_of_action_2() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___action_2)); }
	inline int32_t get_action_2() const { return ___action_2; }
	inline int32_t* get_address_of_action_2() { return &___action_2; }
	inline void set_action_2(int32_t value)
	{
		___action_2 = value;
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___target_3)); }
	inline Object_t631007953 * get_target_3() const { return ___target_3; }
	inline Object_t631007953 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(Object_t631007953 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_source_4() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___source_4)); }
	inline GameObject_t1113636619 * get_source_4() const { return ___source_4; }
	inline GameObject_t1113636619 ** get_address_of_source_4() { return &___source_4; }
	inline void set_source_4(GameObject_t1113636619 * value)
	{
		___source_4 = value;
		Il2CppCodeGenWriteBarrier((&___source_4), value);
	}

	inline static int32_t get_offset_of_triggerCount_5() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___triggerCount_5)); }
	inline int32_t get_triggerCount_5() const { return ___triggerCount_5; }
	inline int32_t* get_address_of_triggerCount_5() { return &___triggerCount_5; }
	inline void set_triggerCount_5(int32_t value)
	{
		___triggerCount_5 = value;
	}

	inline static int32_t get_offset_of_repeatTrigger_6() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___repeatTrigger_6)); }
	inline bool get_repeatTrigger_6() const { return ___repeatTrigger_6; }
	inline bool* get_address_of_repeatTrigger_6() { return &___repeatTrigger_6; }
	inline void set_repeatTrigger_6(bool value)
	{
		___repeatTrigger_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATETRIGGER_T3349759092_H
#ifndef AUTOMOVEANDROTATE_T2437913015_H
#define AUTOMOVEANDROTATE_T2437913015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMoveAndRotate
struct  AutoMoveAndRotate_t2437913015  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace UnityStandardAssets.Utility.AutoMoveAndRotate::moveUnitsPerSecond
	Vector3andSpace_t219844479 * ___moveUnitsPerSecond_2;
	// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace UnityStandardAssets.Utility.AutoMoveAndRotate::rotateDegreesPerSecond
	Vector3andSpace_t219844479 * ___rotateDegreesPerSecond_3;
	// System.Boolean UnityStandardAssets.Utility.AutoMoveAndRotate::ignoreTimescale
	bool ___ignoreTimescale_4;
	// System.Single UnityStandardAssets.Utility.AutoMoveAndRotate::m_LastRealTime
	float ___m_LastRealTime_5;

public:
	inline static int32_t get_offset_of_moveUnitsPerSecond_2() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t2437913015, ___moveUnitsPerSecond_2)); }
	inline Vector3andSpace_t219844479 * get_moveUnitsPerSecond_2() const { return ___moveUnitsPerSecond_2; }
	inline Vector3andSpace_t219844479 ** get_address_of_moveUnitsPerSecond_2() { return &___moveUnitsPerSecond_2; }
	inline void set_moveUnitsPerSecond_2(Vector3andSpace_t219844479 * value)
	{
		___moveUnitsPerSecond_2 = value;
		Il2CppCodeGenWriteBarrier((&___moveUnitsPerSecond_2), value);
	}

	inline static int32_t get_offset_of_rotateDegreesPerSecond_3() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t2437913015, ___rotateDegreesPerSecond_3)); }
	inline Vector3andSpace_t219844479 * get_rotateDegreesPerSecond_3() const { return ___rotateDegreesPerSecond_3; }
	inline Vector3andSpace_t219844479 ** get_address_of_rotateDegreesPerSecond_3() { return &___rotateDegreesPerSecond_3; }
	inline void set_rotateDegreesPerSecond_3(Vector3andSpace_t219844479 * value)
	{
		___rotateDegreesPerSecond_3 = value;
		Il2CppCodeGenWriteBarrier((&___rotateDegreesPerSecond_3), value);
	}

	inline static int32_t get_offset_of_ignoreTimescale_4() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t2437913015, ___ignoreTimescale_4)); }
	inline bool get_ignoreTimescale_4() const { return ___ignoreTimescale_4; }
	inline bool* get_address_of_ignoreTimescale_4() { return &___ignoreTimescale_4; }
	inline void set_ignoreTimescale_4(bool value)
	{
		___ignoreTimescale_4 = value;
	}

	inline static int32_t get_offset_of_m_LastRealTime_5() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t2437913015, ___m_LastRealTime_5)); }
	inline float get_m_LastRealTime_5() const { return ___m_LastRealTime_5; }
	inline float* get_address_of_m_LastRealTime_5() { return &___m_LastRealTime_5; }
	inline void set_m_LastRealTime_5(float value)
	{
		___m_LastRealTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOMOVEANDROTATE_T2437913015_H
#ifndef AUTOMOBILESHADERSWITCH_T568447889_H
#define AUTOMOBILESHADERSWITCH_T568447889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMobileShaderSwitch
struct  AutoMobileShaderSwitch_t568447889  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList UnityStandardAssets.Utility.AutoMobileShaderSwitch::m_ReplacementList
	ReplacementList_t1887104210 * ___m_ReplacementList_2;

public:
	inline static int32_t get_offset_of_m_ReplacementList_2() { return static_cast<int32_t>(offsetof(AutoMobileShaderSwitch_t568447889, ___m_ReplacementList_2)); }
	inline ReplacementList_t1887104210 * get_m_ReplacementList_2() const { return ___m_ReplacementList_2; }
	inline ReplacementList_t1887104210 ** get_address_of_m_ReplacementList_2() { return &___m_ReplacementList_2; }
	inline void set_m_ReplacementList_2(ReplacementList_t1887104210 * value)
	{
		___m_ReplacementList_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReplacementList_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOMOBILESHADERSWITCH_T568447889_H
#ifndef WAYPOINTCIRCUIT_T445075330_H
#define WAYPOINTCIRCUIT_T445075330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointCircuit
struct  WaypointCircuit_t445075330  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.WaypointCircuit/WaypointList UnityStandardAssets.Utility.WaypointCircuit::waypointList
	WaypointList_t2584574554 * ___waypointList_2;
	// System.Boolean UnityStandardAssets.Utility.WaypointCircuit::smoothRoute
	bool ___smoothRoute_3;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::numPoints
	int32_t ___numPoints_4;
	// UnityEngine.Vector3[] UnityStandardAssets.Utility.WaypointCircuit::points
	Vector3U5BU5D_t1718750761* ___points_5;
	// System.Single[] UnityStandardAssets.Utility.WaypointCircuit::distances
	SingleU5BU5D_t1444911251* ___distances_6;
	// System.Single UnityStandardAssets.Utility.WaypointCircuit::editorVisualisationSubsteps
	float ___editorVisualisationSubsteps_7;
	// System.Single UnityStandardAssets.Utility.WaypointCircuit::<Length>k__BackingField
	float ___U3CLengthU3Ek__BackingField_8;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p0n
	int32_t ___p0n_9;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p1n
	int32_t ___p1n_10;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p2n
	int32_t ___p2n_11;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p3n
	int32_t ___p3n_12;
	// System.Single UnityStandardAssets.Utility.WaypointCircuit::i
	float ___i_13;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P0
	Vector3_t3722313464  ___P0_14;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P1
	Vector3_t3722313464  ___P1_15;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P2
	Vector3_t3722313464  ___P2_16;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P3
	Vector3_t3722313464  ___P3_17;

public:
	inline static int32_t get_offset_of_waypointList_2() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___waypointList_2)); }
	inline WaypointList_t2584574554 * get_waypointList_2() const { return ___waypointList_2; }
	inline WaypointList_t2584574554 ** get_address_of_waypointList_2() { return &___waypointList_2; }
	inline void set_waypointList_2(WaypointList_t2584574554 * value)
	{
		___waypointList_2 = value;
		Il2CppCodeGenWriteBarrier((&___waypointList_2), value);
	}

	inline static int32_t get_offset_of_smoothRoute_3() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___smoothRoute_3)); }
	inline bool get_smoothRoute_3() const { return ___smoothRoute_3; }
	inline bool* get_address_of_smoothRoute_3() { return &___smoothRoute_3; }
	inline void set_smoothRoute_3(bool value)
	{
		___smoothRoute_3 = value;
	}

	inline static int32_t get_offset_of_numPoints_4() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___numPoints_4)); }
	inline int32_t get_numPoints_4() const { return ___numPoints_4; }
	inline int32_t* get_address_of_numPoints_4() { return &___numPoints_4; }
	inline void set_numPoints_4(int32_t value)
	{
		___numPoints_4 = value;
	}

	inline static int32_t get_offset_of_points_5() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___points_5)); }
	inline Vector3U5BU5D_t1718750761* get_points_5() const { return ___points_5; }
	inline Vector3U5BU5D_t1718750761** get_address_of_points_5() { return &___points_5; }
	inline void set_points_5(Vector3U5BU5D_t1718750761* value)
	{
		___points_5 = value;
		Il2CppCodeGenWriteBarrier((&___points_5), value);
	}

	inline static int32_t get_offset_of_distances_6() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___distances_6)); }
	inline SingleU5BU5D_t1444911251* get_distances_6() const { return ___distances_6; }
	inline SingleU5BU5D_t1444911251** get_address_of_distances_6() { return &___distances_6; }
	inline void set_distances_6(SingleU5BU5D_t1444911251* value)
	{
		___distances_6 = value;
		Il2CppCodeGenWriteBarrier((&___distances_6), value);
	}

	inline static int32_t get_offset_of_editorVisualisationSubsteps_7() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___editorVisualisationSubsteps_7)); }
	inline float get_editorVisualisationSubsteps_7() const { return ___editorVisualisationSubsteps_7; }
	inline float* get_address_of_editorVisualisationSubsteps_7() { return &___editorVisualisationSubsteps_7; }
	inline void set_editorVisualisationSubsteps_7(float value)
	{
		___editorVisualisationSubsteps_7 = value;
	}

	inline static int32_t get_offset_of_U3CLengthU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___U3CLengthU3Ek__BackingField_8)); }
	inline float get_U3CLengthU3Ek__BackingField_8() const { return ___U3CLengthU3Ek__BackingField_8; }
	inline float* get_address_of_U3CLengthU3Ek__BackingField_8() { return &___U3CLengthU3Ek__BackingField_8; }
	inline void set_U3CLengthU3Ek__BackingField_8(float value)
	{
		___U3CLengthU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_p0n_9() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___p0n_9)); }
	inline int32_t get_p0n_9() const { return ___p0n_9; }
	inline int32_t* get_address_of_p0n_9() { return &___p0n_9; }
	inline void set_p0n_9(int32_t value)
	{
		___p0n_9 = value;
	}

	inline static int32_t get_offset_of_p1n_10() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___p1n_10)); }
	inline int32_t get_p1n_10() const { return ___p1n_10; }
	inline int32_t* get_address_of_p1n_10() { return &___p1n_10; }
	inline void set_p1n_10(int32_t value)
	{
		___p1n_10 = value;
	}

	inline static int32_t get_offset_of_p2n_11() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___p2n_11)); }
	inline int32_t get_p2n_11() const { return ___p2n_11; }
	inline int32_t* get_address_of_p2n_11() { return &___p2n_11; }
	inline void set_p2n_11(int32_t value)
	{
		___p2n_11 = value;
	}

	inline static int32_t get_offset_of_p3n_12() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___p3n_12)); }
	inline int32_t get_p3n_12() const { return ___p3n_12; }
	inline int32_t* get_address_of_p3n_12() { return &___p3n_12; }
	inline void set_p3n_12(int32_t value)
	{
		___p3n_12 = value;
	}

	inline static int32_t get_offset_of_i_13() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___i_13)); }
	inline float get_i_13() const { return ___i_13; }
	inline float* get_address_of_i_13() { return &___i_13; }
	inline void set_i_13(float value)
	{
		___i_13 = value;
	}

	inline static int32_t get_offset_of_P0_14() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___P0_14)); }
	inline Vector3_t3722313464  get_P0_14() const { return ___P0_14; }
	inline Vector3_t3722313464 * get_address_of_P0_14() { return &___P0_14; }
	inline void set_P0_14(Vector3_t3722313464  value)
	{
		___P0_14 = value;
	}

	inline static int32_t get_offset_of_P1_15() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___P1_15)); }
	inline Vector3_t3722313464  get_P1_15() const { return ___P1_15; }
	inline Vector3_t3722313464 * get_address_of_P1_15() { return &___P1_15; }
	inline void set_P1_15(Vector3_t3722313464  value)
	{
		___P1_15 = value;
	}

	inline static int32_t get_offset_of_P2_16() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___P2_16)); }
	inline Vector3_t3722313464  get_P2_16() const { return ___P2_16; }
	inline Vector3_t3722313464 * get_address_of_P2_16() { return &___P2_16; }
	inline void set_P2_16(Vector3_t3722313464  value)
	{
		___P2_16 = value;
	}

	inline static int32_t get_offset_of_P3_17() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___P3_17)); }
	inline Vector3_t3722313464  get_P3_17() const { return ___P3_17; }
	inline Vector3_t3722313464 * get_address_of_P3_17() { return &___P3_17; }
	inline void set_P3_17(Vector3_t3722313464  value)
	{
		___P3_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINTCIRCUIT_T445075330_H
#ifndef TIMEDOBJECTACTIVATOR_T1846709985_H
#define TIMEDOBJECTACTIVATOR_T1846709985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator
struct  TimedObjectActivator_t1846709985  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.TimedObjectActivator/Entries UnityStandardAssets.Utility.TimedObjectActivator::entries
	Entries_t3168066469 * ___entries_2;

public:
	inline static int32_t get_offset_of_entries_2() { return static_cast<int32_t>(offsetof(TimedObjectActivator_t1846709985, ___entries_2)); }
	inline Entries_t3168066469 * get_entries_2() const { return ___entries_2; }
	inline Entries_t3168066469 ** get_address_of_entries_2() { return &___entries_2; }
	inline void set_entries_2(Entries_t3168066469 * value)
	{
		___entries_2 = value;
		Il2CppCodeGenWriteBarrier((&___entries_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEDOBJECTACTIVATOR_T1846709985_H
#ifndef SMOOTHFOLLOW_T4204731361_H
#define SMOOTHFOLLOW_T4204731361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.SmoothFollow
struct  SmoothFollow_t4204731361  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UnityStandardAssets.Utility.SmoothFollow::target
	Transform_t3600365921 * ___target_2;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::distance
	float ___distance_3;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::height
	float ___height_4;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::rotationDamping
	float ___rotationDamping_5;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::heightDamping
	float ___heightDamping_6;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(SmoothFollow_t4204731361, ___target_2)); }
	inline Transform_t3600365921 * get_target_2() const { return ___target_2; }
	inline Transform_t3600365921 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t3600365921 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_distance_3() { return static_cast<int32_t>(offsetof(SmoothFollow_t4204731361, ___distance_3)); }
	inline float get_distance_3() const { return ___distance_3; }
	inline float* get_address_of_distance_3() { return &___distance_3; }
	inline void set_distance_3(float value)
	{
		___distance_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(SmoothFollow_t4204731361, ___height_4)); }
	inline float get_height_4() const { return ___height_4; }
	inline float* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(float value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_rotationDamping_5() { return static_cast<int32_t>(offsetof(SmoothFollow_t4204731361, ___rotationDamping_5)); }
	inline float get_rotationDamping_5() const { return ___rotationDamping_5; }
	inline float* get_address_of_rotationDamping_5() { return &___rotationDamping_5; }
	inline void set_rotationDamping_5(float value)
	{
		___rotationDamping_5 = value;
	}

	inline static int32_t get_offset_of_heightDamping_6() { return static_cast<int32_t>(offsetof(SmoothFollow_t4204731361, ___heightDamping_6)); }
	inline float get_heightDamping_6() const { return ___heightDamping_6; }
	inline float* get_address_of_heightDamping_6() { return &___heightDamping_6; }
	inline void set_heightDamping_6(float value)
	{
		___heightDamping_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOOTHFOLLOW_T4204731361_H
#ifndef WAYPOINTPROGRESSTRACKER_T1841386251_H
#define WAYPOINTPROGRESSTRACKER_T1841386251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointProgressTracker
struct  WaypointProgressTracker_t1841386251  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.WaypointCircuit UnityStandardAssets.Utility.WaypointProgressTracker::circuit
	WaypointCircuit_t445075330 * ___circuit_2;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::lookAheadForTargetOffset
	float ___lookAheadForTargetOffset_3;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::lookAheadForTargetFactor
	float ___lookAheadForTargetFactor_4;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::lookAheadForSpeedOffset
	float ___lookAheadForSpeedOffset_5;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::lookAheadForSpeedFactor
	float ___lookAheadForSpeedFactor_6;
	// UnityStandardAssets.Utility.WaypointProgressTracker/ProgressStyle UnityStandardAssets.Utility.WaypointProgressTracker::progressStyle
	int32_t ___progressStyle_7;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::pointToPointThreshold
	float ___pointToPointThreshold_8;
	// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::<targetPoint>k__BackingField
	RoutePoint_t3880028948  ___U3CtargetPointU3Ek__BackingField_9;
	// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::<speedPoint>k__BackingField
	RoutePoint_t3880028948  ___U3CspeedPointU3Ek__BackingField_10;
	// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::<progressPoint>k__BackingField
	RoutePoint_t3880028948  ___U3CprogressPointU3Ek__BackingField_11;
	// UnityEngine.Transform UnityStandardAssets.Utility.WaypointProgressTracker::target
	Transform_t3600365921 * ___target_12;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::progressDistance
	float ___progressDistance_13;
	// System.Int32 UnityStandardAssets.Utility.WaypointProgressTracker::progressNum
	int32_t ___progressNum_14;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointProgressTracker::lastPosition
	Vector3_t3722313464  ___lastPosition_15;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::speed
	float ___speed_16;

public:
	inline static int32_t get_offset_of_circuit_2() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___circuit_2)); }
	inline WaypointCircuit_t445075330 * get_circuit_2() const { return ___circuit_2; }
	inline WaypointCircuit_t445075330 ** get_address_of_circuit_2() { return &___circuit_2; }
	inline void set_circuit_2(WaypointCircuit_t445075330 * value)
	{
		___circuit_2 = value;
		Il2CppCodeGenWriteBarrier((&___circuit_2), value);
	}

	inline static int32_t get_offset_of_lookAheadForTargetOffset_3() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___lookAheadForTargetOffset_3)); }
	inline float get_lookAheadForTargetOffset_3() const { return ___lookAheadForTargetOffset_3; }
	inline float* get_address_of_lookAheadForTargetOffset_3() { return &___lookAheadForTargetOffset_3; }
	inline void set_lookAheadForTargetOffset_3(float value)
	{
		___lookAheadForTargetOffset_3 = value;
	}

	inline static int32_t get_offset_of_lookAheadForTargetFactor_4() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___lookAheadForTargetFactor_4)); }
	inline float get_lookAheadForTargetFactor_4() const { return ___lookAheadForTargetFactor_4; }
	inline float* get_address_of_lookAheadForTargetFactor_4() { return &___lookAheadForTargetFactor_4; }
	inline void set_lookAheadForTargetFactor_4(float value)
	{
		___lookAheadForTargetFactor_4 = value;
	}

	inline static int32_t get_offset_of_lookAheadForSpeedOffset_5() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___lookAheadForSpeedOffset_5)); }
	inline float get_lookAheadForSpeedOffset_5() const { return ___lookAheadForSpeedOffset_5; }
	inline float* get_address_of_lookAheadForSpeedOffset_5() { return &___lookAheadForSpeedOffset_5; }
	inline void set_lookAheadForSpeedOffset_5(float value)
	{
		___lookAheadForSpeedOffset_5 = value;
	}

	inline static int32_t get_offset_of_lookAheadForSpeedFactor_6() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___lookAheadForSpeedFactor_6)); }
	inline float get_lookAheadForSpeedFactor_6() const { return ___lookAheadForSpeedFactor_6; }
	inline float* get_address_of_lookAheadForSpeedFactor_6() { return &___lookAheadForSpeedFactor_6; }
	inline void set_lookAheadForSpeedFactor_6(float value)
	{
		___lookAheadForSpeedFactor_6 = value;
	}

	inline static int32_t get_offset_of_progressStyle_7() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___progressStyle_7)); }
	inline int32_t get_progressStyle_7() const { return ___progressStyle_7; }
	inline int32_t* get_address_of_progressStyle_7() { return &___progressStyle_7; }
	inline void set_progressStyle_7(int32_t value)
	{
		___progressStyle_7 = value;
	}

	inline static int32_t get_offset_of_pointToPointThreshold_8() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___pointToPointThreshold_8)); }
	inline float get_pointToPointThreshold_8() const { return ___pointToPointThreshold_8; }
	inline float* get_address_of_pointToPointThreshold_8() { return &___pointToPointThreshold_8; }
	inline void set_pointToPointThreshold_8(float value)
	{
		___pointToPointThreshold_8 = value;
	}

	inline static int32_t get_offset_of_U3CtargetPointU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___U3CtargetPointU3Ek__BackingField_9)); }
	inline RoutePoint_t3880028948  get_U3CtargetPointU3Ek__BackingField_9() const { return ___U3CtargetPointU3Ek__BackingField_9; }
	inline RoutePoint_t3880028948 * get_address_of_U3CtargetPointU3Ek__BackingField_9() { return &___U3CtargetPointU3Ek__BackingField_9; }
	inline void set_U3CtargetPointU3Ek__BackingField_9(RoutePoint_t3880028948  value)
	{
		___U3CtargetPointU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CspeedPointU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___U3CspeedPointU3Ek__BackingField_10)); }
	inline RoutePoint_t3880028948  get_U3CspeedPointU3Ek__BackingField_10() const { return ___U3CspeedPointU3Ek__BackingField_10; }
	inline RoutePoint_t3880028948 * get_address_of_U3CspeedPointU3Ek__BackingField_10() { return &___U3CspeedPointU3Ek__BackingField_10; }
	inline void set_U3CspeedPointU3Ek__BackingField_10(RoutePoint_t3880028948  value)
	{
		___U3CspeedPointU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CprogressPointU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___U3CprogressPointU3Ek__BackingField_11)); }
	inline RoutePoint_t3880028948  get_U3CprogressPointU3Ek__BackingField_11() const { return ___U3CprogressPointU3Ek__BackingField_11; }
	inline RoutePoint_t3880028948 * get_address_of_U3CprogressPointU3Ek__BackingField_11() { return &___U3CprogressPointU3Ek__BackingField_11; }
	inline void set_U3CprogressPointU3Ek__BackingField_11(RoutePoint_t3880028948  value)
	{
		___U3CprogressPointU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_target_12() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___target_12)); }
	inline Transform_t3600365921 * get_target_12() const { return ___target_12; }
	inline Transform_t3600365921 ** get_address_of_target_12() { return &___target_12; }
	inline void set_target_12(Transform_t3600365921 * value)
	{
		___target_12 = value;
		Il2CppCodeGenWriteBarrier((&___target_12), value);
	}

	inline static int32_t get_offset_of_progressDistance_13() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___progressDistance_13)); }
	inline float get_progressDistance_13() const { return ___progressDistance_13; }
	inline float* get_address_of_progressDistance_13() { return &___progressDistance_13; }
	inline void set_progressDistance_13(float value)
	{
		___progressDistance_13 = value;
	}

	inline static int32_t get_offset_of_progressNum_14() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___progressNum_14)); }
	inline int32_t get_progressNum_14() const { return ___progressNum_14; }
	inline int32_t* get_address_of_progressNum_14() { return &___progressNum_14; }
	inline void set_progressNum_14(int32_t value)
	{
		___progressNum_14 = value;
	}

	inline static int32_t get_offset_of_lastPosition_15() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___lastPosition_15)); }
	inline Vector3_t3722313464  get_lastPosition_15() const { return ___lastPosition_15; }
	inline Vector3_t3722313464 * get_address_of_lastPosition_15() { return &___lastPosition_15; }
	inline void set_lastPosition_15(Vector3_t3722313464  value)
	{
		___lastPosition_15 = value;
	}

	inline static int32_t get_offset_of_speed_16() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t1841386251, ___speed_16)); }
	inline float get_speed_16() const { return ___speed_16; }
	inline float* get_address_of_speed_16() { return &___speed_16; }
	inline void set_speed_16(float value)
	{
		___speed_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINTPROGRESSTRACKER_T1841386251_H
#ifndef SIMPLEMOUSEROTATOR_T2364742953_H
#define SIMPLEMOUSEROTATOR_T2364742953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.SimpleMouseRotator
struct  SimpleMouseRotator_t2364742953  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector2 UnityStandardAssets.Utility.SimpleMouseRotator::rotationRange
	Vector2_t2156229523  ___rotationRange_2;
	// System.Single UnityStandardAssets.Utility.SimpleMouseRotator::rotationSpeed
	float ___rotationSpeed_3;
	// System.Single UnityStandardAssets.Utility.SimpleMouseRotator::dampingTime
	float ___dampingTime_4;
	// System.Boolean UnityStandardAssets.Utility.SimpleMouseRotator::autoZeroVerticalOnMobile
	bool ___autoZeroVerticalOnMobile_5;
	// System.Boolean UnityStandardAssets.Utility.SimpleMouseRotator::autoZeroHorizontalOnMobile
	bool ___autoZeroHorizontalOnMobile_6;
	// System.Boolean UnityStandardAssets.Utility.SimpleMouseRotator::relative
	bool ___relative_7;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.SimpleMouseRotator::m_TargetAngles
	Vector3_t3722313464  ___m_TargetAngles_8;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.SimpleMouseRotator::m_FollowAngles
	Vector3_t3722313464  ___m_FollowAngles_9;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.SimpleMouseRotator::m_FollowVelocity
	Vector3_t3722313464  ___m_FollowVelocity_10;
	// UnityEngine.Quaternion UnityStandardAssets.Utility.SimpleMouseRotator::m_OriginalRotation
	Quaternion_t2301928331  ___m_OriginalRotation_11;

public:
	inline static int32_t get_offset_of_rotationRange_2() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___rotationRange_2)); }
	inline Vector2_t2156229523  get_rotationRange_2() const { return ___rotationRange_2; }
	inline Vector2_t2156229523 * get_address_of_rotationRange_2() { return &___rotationRange_2; }
	inline void set_rotationRange_2(Vector2_t2156229523  value)
	{
		___rotationRange_2 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_3() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___rotationSpeed_3)); }
	inline float get_rotationSpeed_3() const { return ___rotationSpeed_3; }
	inline float* get_address_of_rotationSpeed_3() { return &___rotationSpeed_3; }
	inline void set_rotationSpeed_3(float value)
	{
		___rotationSpeed_3 = value;
	}

	inline static int32_t get_offset_of_dampingTime_4() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___dampingTime_4)); }
	inline float get_dampingTime_4() const { return ___dampingTime_4; }
	inline float* get_address_of_dampingTime_4() { return &___dampingTime_4; }
	inline void set_dampingTime_4(float value)
	{
		___dampingTime_4 = value;
	}

	inline static int32_t get_offset_of_autoZeroVerticalOnMobile_5() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___autoZeroVerticalOnMobile_5)); }
	inline bool get_autoZeroVerticalOnMobile_5() const { return ___autoZeroVerticalOnMobile_5; }
	inline bool* get_address_of_autoZeroVerticalOnMobile_5() { return &___autoZeroVerticalOnMobile_5; }
	inline void set_autoZeroVerticalOnMobile_5(bool value)
	{
		___autoZeroVerticalOnMobile_5 = value;
	}

	inline static int32_t get_offset_of_autoZeroHorizontalOnMobile_6() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___autoZeroHorizontalOnMobile_6)); }
	inline bool get_autoZeroHorizontalOnMobile_6() const { return ___autoZeroHorizontalOnMobile_6; }
	inline bool* get_address_of_autoZeroHorizontalOnMobile_6() { return &___autoZeroHorizontalOnMobile_6; }
	inline void set_autoZeroHorizontalOnMobile_6(bool value)
	{
		___autoZeroHorizontalOnMobile_6 = value;
	}

	inline static int32_t get_offset_of_relative_7() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___relative_7)); }
	inline bool get_relative_7() const { return ___relative_7; }
	inline bool* get_address_of_relative_7() { return &___relative_7; }
	inline void set_relative_7(bool value)
	{
		___relative_7 = value;
	}

	inline static int32_t get_offset_of_m_TargetAngles_8() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___m_TargetAngles_8)); }
	inline Vector3_t3722313464  get_m_TargetAngles_8() const { return ___m_TargetAngles_8; }
	inline Vector3_t3722313464 * get_address_of_m_TargetAngles_8() { return &___m_TargetAngles_8; }
	inline void set_m_TargetAngles_8(Vector3_t3722313464  value)
	{
		___m_TargetAngles_8 = value;
	}

	inline static int32_t get_offset_of_m_FollowAngles_9() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___m_FollowAngles_9)); }
	inline Vector3_t3722313464  get_m_FollowAngles_9() const { return ___m_FollowAngles_9; }
	inline Vector3_t3722313464 * get_address_of_m_FollowAngles_9() { return &___m_FollowAngles_9; }
	inline void set_m_FollowAngles_9(Vector3_t3722313464  value)
	{
		___m_FollowAngles_9 = value;
	}

	inline static int32_t get_offset_of_m_FollowVelocity_10() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___m_FollowVelocity_10)); }
	inline Vector3_t3722313464  get_m_FollowVelocity_10() const { return ___m_FollowVelocity_10; }
	inline Vector3_t3722313464 * get_address_of_m_FollowVelocity_10() { return &___m_FollowVelocity_10; }
	inline void set_m_FollowVelocity_10(Vector3_t3722313464  value)
	{
		___m_FollowVelocity_10 = value;
	}

	inline static int32_t get_offset_of_m_OriginalRotation_11() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___m_OriginalRotation_11)); }
	inline Quaternion_t2301928331  get_m_OriginalRotation_11() const { return ___m_OriginalRotation_11; }
	inline Quaternion_t2301928331 * get_address_of_m_OriginalRotation_11() { return &___m_OriginalRotation_11; }
	inline void set_m_OriginalRotation_11(Quaternion_t2301928331  value)
	{
		___m_OriginalRotation_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEMOUSEROTATOR_T2364742953_H
#ifndef PLATFORMSPECIFICCONTENT_T1404549723_H
#define PLATFORMSPECIFICCONTENT_T1404549723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.PlatformSpecificContent
struct  PlatformSpecificContent_t1404549723  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup UnityStandardAssets.Utility.PlatformSpecificContent::m_BuildTargetGroup
	int32_t ___m_BuildTargetGroup_2;
	// UnityEngine.GameObject[] UnityStandardAssets.Utility.PlatformSpecificContent::m_Content
	GameObjectU5BU5D_t3328599146* ___m_Content_3;
	// UnityEngine.MonoBehaviour[] UnityStandardAssets.Utility.PlatformSpecificContent::m_MonoBehaviours
	MonoBehaviourU5BU5D_t2007329276* ___m_MonoBehaviours_4;
	// System.Boolean UnityStandardAssets.Utility.PlatformSpecificContent::m_ChildrenOfThisObject
	bool ___m_ChildrenOfThisObject_5;

public:
	inline static int32_t get_offset_of_m_BuildTargetGroup_2() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t1404549723, ___m_BuildTargetGroup_2)); }
	inline int32_t get_m_BuildTargetGroup_2() const { return ___m_BuildTargetGroup_2; }
	inline int32_t* get_address_of_m_BuildTargetGroup_2() { return &___m_BuildTargetGroup_2; }
	inline void set_m_BuildTargetGroup_2(int32_t value)
	{
		___m_BuildTargetGroup_2 = value;
	}

	inline static int32_t get_offset_of_m_Content_3() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t1404549723, ___m_Content_3)); }
	inline GameObjectU5BU5D_t3328599146* get_m_Content_3() const { return ___m_Content_3; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_Content_3() { return &___m_Content_3; }
	inline void set_m_Content_3(GameObjectU5BU5D_t3328599146* value)
	{
		___m_Content_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Content_3), value);
	}

	inline static int32_t get_offset_of_m_MonoBehaviours_4() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t1404549723, ___m_MonoBehaviours_4)); }
	inline MonoBehaviourU5BU5D_t2007329276* get_m_MonoBehaviours_4() const { return ___m_MonoBehaviours_4; }
	inline MonoBehaviourU5BU5D_t2007329276** get_address_of_m_MonoBehaviours_4() { return &___m_MonoBehaviours_4; }
	inline void set_m_MonoBehaviours_4(MonoBehaviourU5BU5D_t2007329276* value)
	{
		___m_MonoBehaviours_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_MonoBehaviours_4), value);
	}

	inline static int32_t get_offset_of_m_ChildrenOfThisObject_5() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t1404549723, ___m_ChildrenOfThisObject_5)); }
	inline bool get_m_ChildrenOfThisObject_5() const { return ___m_ChildrenOfThisObject_5; }
	inline bool* get_address_of_m_ChildrenOfThisObject_5() { return &___m_ChildrenOfThisObject_5; }
	inline void set_m_ChildrenOfThisObject_5(bool value)
	{
		___m_ChildrenOfThisObject_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMSPECIFICCONTENT_T1404549723_H
#ifndef ALLREF_T460770394_H
#define ALLREF_T460770394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllRef
struct  AllRef_t460770394  : public MonoBehaviour_t3962482529
{
public:
	// System.String[] AllRef::inAppPackages
	StringU5BU5D_t1281789340* ___inAppPackages_2;
	// System.String AllRef::inAPP_KEY
	String_t* ___inAPP_KEY_3;
	// UnityEngine.AudioSource AllRef::FxPuzzleComplete
	AudioSource_t3935305588 * ___FxPuzzleComplete_6;
	// UnityEngine.AudioSource AllRef::FXCoin
	AudioSource_t3935305588 * ___FXCoin_7;

public:
	inline static int32_t get_offset_of_inAppPackages_2() { return static_cast<int32_t>(offsetof(AllRef_t460770394, ___inAppPackages_2)); }
	inline StringU5BU5D_t1281789340* get_inAppPackages_2() const { return ___inAppPackages_2; }
	inline StringU5BU5D_t1281789340** get_address_of_inAppPackages_2() { return &___inAppPackages_2; }
	inline void set_inAppPackages_2(StringU5BU5D_t1281789340* value)
	{
		___inAppPackages_2 = value;
		Il2CppCodeGenWriteBarrier((&___inAppPackages_2), value);
	}

	inline static int32_t get_offset_of_inAPP_KEY_3() { return static_cast<int32_t>(offsetof(AllRef_t460770394, ___inAPP_KEY_3)); }
	inline String_t* get_inAPP_KEY_3() const { return ___inAPP_KEY_3; }
	inline String_t** get_address_of_inAPP_KEY_3() { return &___inAPP_KEY_3; }
	inline void set_inAPP_KEY_3(String_t* value)
	{
		___inAPP_KEY_3 = value;
		Il2CppCodeGenWriteBarrier((&___inAPP_KEY_3), value);
	}

	inline static int32_t get_offset_of_FxPuzzleComplete_6() { return static_cast<int32_t>(offsetof(AllRef_t460770394, ___FxPuzzleComplete_6)); }
	inline AudioSource_t3935305588 * get_FxPuzzleComplete_6() const { return ___FxPuzzleComplete_6; }
	inline AudioSource_t3935305588 ** get_address_of_FxPuzzleComplete_6() { return &___FxPuzzleComplete_6; }
	inline void set_FxPuzzleComplete_6(AudioSource_t3935305588 * value)
	{
		___FxPuzzleComplete_6 = value;
		Il2CppCodeGenWriteBarrier((&___FxPuzzleComplete_6), value);
	}

	inline static int32_t get_offset_of_FXCoin_7() { return static_cast<int32_t>(offsetof(AllRef_t460770394, ___FXCoin_7)); }
	inline AudioSource_t3935305588 * get_FXCoin_7() const { return ___FXCoin_7; }
	inline AudioSource_t3935305588 ** get_address_of_FXCoin_7() { return &___FXCoin_7; }
	inline void set_FXCoin_7(AudioSource_t3935305588 * value)
	{
		___FXCoin_7 = value;
		Il2CppCodeGenWriteBarrier((&___FXCoin_7), value);
	}
};

struct AllRef_t460770394_StaticFields
{
public:
	// System.Int32 AllRef::finishAnimationNumber
	int32_t ___finishAnimationNumber_4;
	// AllRef AllRef::_instance
	AllRef_t460770394 * ____instance_5;

public:
	inline static int32_t get_offset_of_finishAnimationNumber_4() { return static_cast<int32_t>(offsetof(AllRef_t460770394_StaticFields, ___finishAnimationNumber_4)); }
	inline int32_t get_finishAnimationNumber_4() const { return ___finishAnimationNumber_4; }
	inline int32_t* get_address_of_finishAnimationNumber_4() { return &___finishAnimationNumber_4; }
	inline void set_finishAnimationNumber_4(int32_t value)
	{
		___finishAnimationNumber_4 = value;
	}

	inline static int32_t get_offset_of__instance_5() { return static_cast<int32_t>(offsetof(AllRef_t460770394_StaticFields, ____instance_5)); }
	inline AllRef_t460770394 * get__instance_5() const { return ____instance_5; }
	inline AllRef_t460770394 ** get_address_of__instance_5() { return &____instance_5; }
	inline void set__instance_5(AllRef_t460770394 * value)
	{
		____instance_5 = value;
		Il2CppCodeGenWriteBarrier((&____instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLREF_T460770394_H
#ifndef PARTICLESYSTEMDESTROYER_T558680695_H
#define PARTICLESYSTEMDESTROYER_T558680695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ParticleSystemDestroyer
struct  ParticleSystemDestroyer_t558680695  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer::minDuration
	float ___minDuration_2;
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer::maxDuration
	float ___maxDuration_3;
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer::m_MaxLifetime
	float ___m_MaxLifetime_4;
	// System.Boolean UnityStandardAssets.Utility.ParticleSystemDestroyer::m_EarlyStop
	bool ___m_EarlyStop_5;

public:
	inline static int32_t get_offset_of_minDuration_2() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t558680695, ___minDuration_2)); }
	inline float get_minDuration_2() const { return ___minDuration_2; }
	inline float* get_address_of_minDuration_2() { return &___minDuration_2; }
	inline void set_minDuration_2(float value)
	{
		___minDuration_2 = value;
	}

	inline static int32_t get_offset_of_maxDuration_3() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t558680695, ___maxDuration_3)); }
	inline float get_maxDuration_3() const { return ___maxDuration_3; }
	inline float* get_address_of_maxDuration_3() { return &___maxDuration_3; }
	inline void set_maxDuration_3(float value)
	{
		___maxDuration_3 = value;
	}

	inline static int32_t get_offset_of_m_MaxLifetime_4() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t558680695, ___m_MaxLifetime_4)); }
	inline float get_m_MaxLifetime_4() const { return ___m_MaxLifetime_4; }
	inline float* get_address_of_m_MaxLifetime_4() { return &___m_MaxLifetime_4; }
	inline void set_m_MaxLifetime_4(float value)
	{
		___m_MaxLifetime_4 = value;
	}

	inline static int32_t get_offset_of_m_EarlyStop_5() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t558680695, ___m_EarlyStop_5)); }
	inline bool get_m_EarlyStop_5() const { return ___m_EarlyStop_5; }
	inline bool* get_address_of_m_EarlyStop_5() { return &___m_EarlyStop_5; }
	inline void set_m_EarlyStop_5(bool value)
	{
		___m_EarlyStop_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMDESTROYER_T558680695_H
#ifndef ANIMATIONEVENTTRIGGERHANDLER_T349007468_H
#define ANIMATIONEVENTTRIGGERHANDLER_T349007468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationEventTriggerHandler
struct  AnimationEventTriggerHandler_t349007468  : public MonoBehaviour_t3962482529
{
public:
	// MainMenuManager AnimationEventTriggerHandler::mainmenumanger
	MainMenuManager_t3185368703 * ___mainmenumanger_2;
	// PlaneVisualizationManager AnimationEventTriggerHandler::planevisualizationmanager
	PlaneVisualizationManager_t1447224982 * ___planevisualizationmanager_3;

public:
	inline static int32_t get_offset_of_mainmenumanger_2() { return static_cast<int32_t>(offsetof(AnimationEventTriggerHandler_t349007468, ___mainmenumanger_2)); }
	inline MainMenuManager_t3185368703 * get_mainmenumanger_2() const { return ___mainmenumanger_2; }
	inline MainMenuManager_t3185368703 ** get_address_of_mainmenumanger_2() { return &___mainmenumanger_2; }
	inline void set_mainmenumanger_2(MainMenuManager_t3185368703 * value)
	{
		___mainmenumanger_2 = value;
		Il2CppCodeGenWriteBarrier((&___mainmenumanger_2), value);
	}

	inline static int32_t get_offset_of_planevisualizationmanager_3() { return static_cast<int32_t>(offsetof(AnimationEventTriggerHandler_t349007468, ___planevisualizationmanager_3)); }
	inline PlaneVisualizationManager_t1447224982 * get_planevisualizationmanager_3() const { return ___planevisualizationmanager_3; }
	inline PlaneVisualizationManager_t1447224982 ** get_address_of_planevisualizationmanager_3() { return &___planevisualizationmanager_3; }
	inline void set_planevisualizationmanager_3(PlaneVisualizationManager_t1447224982 * value)
	{
		___planevisualizationmanager_3 = value;
		Il2CppCodeGenWriteBarrier((&___planevisualizationmanager_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONEVENTTRIGGERHANDLER_T349007468_H
#ifndef ALPHABUTTONCLICKMASK_T141136539_H
#define ALPHABUTTONCLICKMASK_T141136539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AlphaButtonClickMask
struct  AlphaButtonClickMask_t141136539  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image AlphaButtonClickMask::_image
	Image_t2670269651 * ____image_2;

public:
	inline static int32_t get_offset_of__image_2() { return static_cast<int32_t>(offsetof(AlphaButtonClickMask_t141136539, ____image_2)); }
	inline Image_t2670269651 * get__image_2() const { return ____image_2; }
	inline Image_t2670269651 ** get_address_of__image_2() { return &____image_2; }
	inline void set__image_2(Image_t2670269651 * value)
	{
		____image_2 = value;
		Il2CppCodeGenWriteBarrier((&____image_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALPHABUTTONCLICKMASK_T141136539_H
#ifndef CHARACTERSCRIPT_T4133963155_H
#define CHARACTERSCRIPT_T4133963155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterScript
struct  CharacterScript_t4133963155  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] CharacterScript::cardPositions
	GameObjectU5BU5D_t3328599146* ___cardPositions_2;
	// UnityEngine.Material CharacterScript::characterMaterial
	Material_t340375123 * ___characterMaterial_3;
	// UnityEngine.GameObject CharacterScript::LollipopObj
	GameObject_t1113636619 * ___LollipopObj_4;
	// UnityEngine.GameObject CharacterScript::Shadow
	GameObject_t1113636619 * ___Shadow_5;
	// UnityEngine.GameObject[] CharacterScript::Swords
	GameObjectU5BU5D_t3328599146* ___Swords_6;
	// UnityEngine.GameObject CharacterScript::CharacterGlasses
	GameObject_t1113636619 * ___CharacterGlasses_7;

public:
	inline static int32_t get_offset_of_cardPositions_2() { return static_cast<int32_t>(offsetof(CharacterScript_t4133963155, ___cardPositions_2)); }
	inline GameObjectU5BU5D_t3328599146* get_cardPositions_2() const { return ___cardPositions_2; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_cardPositions_2() { return &___cardPositions_2; }
	inline void set_cardPositions_2(GameObjectU5BU5D_t3328599146* value)
	{
		___cardPositions_2 = value;
		Il2CppCodeGenWriteBarrier((&___cardPositions_2), value);
	}

	inline static int32_t get_offset_of_characterMaterial_3() { return static_cast<int32_t>(offsetof(CharacterScript_t4133963155, ___characterMaterial_3)); }
	inline Material_t340375123 * get_characterMaterial_3() const { return ___characterMaterial_3; }
	inline Material_t340375123 ** get_address_of_characterMaterial_3() { return &___characterMaterial_3; }
	inline void set_characterMaterial_3(Material_t340375123 * value)
	{
		___characterMaterial_3 = value;
		Il2CppCodeGenWriteBarrier((&___characterMaterial_3), value);
	}

	inline static int32_t get_offset_of_LollipopObj_4() { return static_cast<int32_t>(offsetof(CharacterScript_t4133963155, ___LollipopObj_4)); }
	inline GameObject_t1113636619 * get_LollipopObj_4() const { return ___LollipopObj_4; }
	inline GameObject_t1113636619 ** get_address_of_LollipopObj_4() { return &___LollipopObj_4; }
	inline void set_LollipopObj_4(GameObject_t1113636619 * value)
	{
		___LollipopObj_4 = value;
		Il2CppCodeGenWriteBarrier((&___LollipopObj_4), value);
	}

	inline static int32_t get_offset_of_Shadow_5() { return static_cast<int32_t>(offsetof(CharacterScript_t4133963155, ___Shadow_5)); }
	inline GameObject_t1113636619 * get_Shadow_5() const { return ___Shadow_5; }
	inline GameObject_t1113636619 ** get_address_of_Shadow_5() { return &___Shadow_5; }
	inline void set_Shadow_5(GameObject_t1113636619 * value)
	{
		___Shadow_5 = value;
		Il2CppCodeGenWriteBarrier((&___Shadow_5), value);
	}

	inline static int32_t get_offset_of_Swords_6() { return static_cast<int32_t>(offsetof(CharacterScript_t4133963155, ___Swords_6)); }
	inline GameObjectU5BU5D_t3328599146* get_Swords_6() const { return ___Swords_6; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_Swords_6() { return &___Swords_6; }
	inline void set_Swords_6(GameObjectU5BU5D_t3328599146* value)
	{
		___Swords_6 = value;
		Il2CppCodeGenWriteBarrier((&___Swords_6), value);
	}

	inline static int32_t get_offset_of_CharacterGlasses_7() { return static_cast<int32_t>(offsetof(CharacterScript_t4133963155, ___CharacterGlasses_7)); }
	inline GameObject_t1113636619 * get_CharacterGlasses_7() const { return ___CharacterGlasses_7; }
	inline GameObject_t1113636619 ** get_address_of_CharacterGlasses_7() { return &___CharacterGlasses_7; }
	inline void set_CharacterGlasses_7(GameObject_t1113636619 * value)
	{
		___CharacterGlasses_7 = value;
		Il2CppCodeGenWriteBarrier((&___CharacterGlasses_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERSCRIPT_T4133963155_H
#ifndef MAINMENUMANAGER_T3185368703_H
#define MAINMENUMANAGER_T3185368703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainMenuManager
struct  MainMenuManager_t3185368703  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject MainMenuManager::startButton
	GameObject_t1113636619 * ___startButton_2;
	// UnityEngine.UI.Text MainMenuManager::startBtnText
	Text_t1901882714 * ___startBtnText_3;
	// UnityEngine.GameObject MainMenuManager::mainMenuPanel
	GameObject_t1113636619 * ___mainMenuPanel_4;
	// UnityEngine.GameObject MainMenuManager::selectionPanel
	GameObject_t1113636619 * ___selectionPanel_5;
	// UnityEngine.GameObject MainMenuManager::LoadingObj
	GameObject_t1113636619 * ___LoadingObj_6;
	// UnityEngine.GameObject MainMenuManager::LoadingPanel
	GameObject_t1113636619 * ___LoadingPanel_7;
	// UnityEngine.GameObject MainMenuManager::LogoBtn
	GameObject_t1113636619 * ___LogoBtn_8;
	// UnityEngine.UI.Text MainMenuManager::CharacterNameText
	Text_t1901882714 * ___CharacterNameText_9;
	// System.String[] MainMenuManager::characternames
	StringU5BU5D_t1281789340* ___characternames_10;
	// UnityEngine.Material[] MainMenuManager::CharacterMaterials
	MaterialU5BU5D_t561872642* ___CharacterMaterials_11;
	// UnityEngine.GameObject[] MainMenuManager::characters
	GameObjectU5BU5D_t3328599146* ___characters_12;
	// UnityEngine.Vector3 MainMenuManager::cameraOffset
	Vector3_t3722313464  ___cameraOffset_13;
	// System.Single MainMenuManager::scrollTime
	float ___scrollTime_14;
	// System.Single MainMenuManager::distanceBtwCharacters
	float ___distanceBtwCharacters_15;
	// UnityEngine.Vector3 MainMenuManager::activePos
	Vector3_t3722313464  ___activePos_16;

public:
	inline static int32_t get_offset_of_startButton_2() { return static_cast<int32_t>(offsetof(MainMenuManager_t3185368703, ___startButton_2)); }
	inline GameObject_t1113636619 * get_startButton_2() const { return ___startButton_2; }
	inline GameObject_t1113636619 ** get_address_of_startButton_2() { return &___startButton_2; }
	inline void set_startButton_2(GameObject_t1113636619 * value)
	{
		___startButton_2 = value;
		Il2CppCodeGenWriteBarrier((&___startButton_2), value);
	}

	inline static int32_t get_offset_of_startBtnText_3() { return static_cast<int32_t>(offsetof(MainMenuManager_t3185368703, ___startBtnText_3)); }
	inline Text_t1901882714 * get_startBtnText_3() const { return ___startBtnText_3; }
	inline Text_t1901882714 ** get_address_of_startBtnText_3() { return &___startBtnText_3; }
	inline void set_startBtnText_3(Text_t1901882714 * value)
	{
		___startBtnText_3 = value;
		Il2CppCodeGenWriteBarrier((&___startBtnText_3), value);
	}

	inline static int32_t get_offset_of_mainMenuPanel_4() { return static_cast<int32_t>(offsetof(MainMenuManager_t3185368703, ___mainMenuPanel_4)); }
	inline GameObject_t1113636619 * get_mainMenuPanel_4() const { return ___mainMenuPanel_4; }
	inline GameObject_t1113636619 ** get_address_of_mainMenuPanel_4() { return &___mainMenuPanel_4; }
	inline void set_mainMenuPanel_4(GameObject_t1113636619 * value)
	{
		___mainMenuPanel_4 = value;
		Il2CppCodeGenWriteBarrier((&___mainMenuPanel_4), value);
	}

	inline static int32_t get_offset_of_selectionPanel_5() { return static_cast<int32_t>(offsetof(MainMenuManager_t3185368703, ___selectionPanel_5)); }
	inline GameObject_t1113636619 * get_selectionPanel_5() const { return ___selectionPanel_5; }
	inline GameObject_t1113636619 ** get_address_of_selectionPanel_5() { return &___selectionPanel_5; }
	inline void set_selectionPanel_5(GameObject_t1113636619 * value)
	{
		___selectionPanel_5 = value;
		Il2CppCodeGenWriteBarrier((&___selectionPanel_5), value);
	}

	inline static int32_t get_offset_of_LoadingObj_6() { return static_cast<int32_t>(offsetof(MainMenuManager_t3185368703, ___LoadingObj_6)); }
	inline GameObject_t1113636619 * get_LoadingObj_6() const { return ___LoadingObj_6; }
	inline GameObject_t1113636619 ** get_address_of_LoadingObj_6() { return &___LoadingObj_6; }
	inline void set_LoadingObj_6(GameObject_t1113636619 * value)
	{
		___LoadingObj_6 = value;
		Il2CppCodeGenWriteBarrier((&___LoadingObj_6), value);
	}

	inline static int32_t get_offset_of_LoadingPanel_7() { return static_cast<int32_t>(offsetof(MainMenuManager_t3185368703, ___LoadingPanel_7)); }
	inline GameObject_t1113636619 * get_LoadingPanel_7() const { return ___LoadingPanel_7; }
	inline GameObject_t1113636619 ** get_address_of_LoadingPanel_7() { return &___LoadingPanel_7; }
	inline void set_LoadingPanel_7(GameObject_t1113636619 * value)
	{
		___LoadingPanel_7 = value;
		Il2CppCodeGenWriteBarrier((&___LoadingPanel_7), value);
	}

	inline static int32_t get_offset_of_LogoBtn_8() { return static_cast<int32_t>(offsetof(MainMenuManager_t3185368703, ___LogoBtn_8)); }
	inline GameObject_t1113636619 * get_LogoBtn_8() const { return ___LogoBtn_8; }
	inline GameObject_t1113636619 ** get_address_of_LogoBtn_8() { return &___LogoBtn_8; }
	inline void set_LogoBtn_8(GameObject_t1113636619 * value)
	{
		___LogoBtn_8 = value;
		Il2CppCodeGenWriteBarrier((&___LogoBtn_8), value);
	}

	inline static int32_t get_offset_of_CharacterNameText_9() { return static_cast<int32_t>(offsetof(MainMenuManager_t3185368703, ___CharacterNameText_9)); }
	inline Text_t1901882714 * get_CharacterNameText_9() const { return ___CharacterNameText_9; }
	inline Text_t1901882714 ** get_address_of_CharacterNameText_9() { return &___CharacterNameText_9; }
	inline void set_CharacterNameText_9(Text_t1901882714 * value)
	{
		___CharacterNameText_9 = value;
		Il2CppCodeGenWriteBarrier((&___CharacterNameText_9), value);
	}

	inline static int32_t get_offset_of_characternames_10() { return static_cast<int32_t>(offsetof(MainMenuManager_t3185368703, ___characternames_10)); }
	inline StringU5BU5D_t1281789340* get_characternames_10() const { return ___characternames_10; }
	inline StringU5BU5D_t1281789340** get_address_of_characternames_10() { return &___characternames_10; }
	inline void set_characternames_10(StringU5BU5D_t1281789340* value)
	{
		___characternames_10 = value;
		Il2CppCodeGenWriteBarrier((&___characternames_10), value);
	}

	inline static int32_t get_offset_of_CharacterMaterials_11() { return static_cast<int32_t>(offsetof(MainMenuManager_t3185368703, ___CharacterMaterials_11)); }
	inline MaterialU5BU5D_t561872642* get_CharacterMaterials_11() const { return ___CharacterMaterials_11; }
	inline MaterialU5BU5D_t561872642** get_address_of_CharacterMaterials_11() { return &___CharacterMaterials_11; }
	inline void set_CharacterMaterials_11(MaterialU5BU5D_t561872642* value)
	{
		___CharacterMaterials_11 = value;
		Il2CppCodeGenWriteBarrier((&___CharacterMaterials_11), value);
	}

	inline static int32_t get_offset_of_characters_12() { return static_cast<int32_t>(offsetof(MainMenuManager_t3185368703, ___characters_12)); }
	inline GameObjectU5BU5D_t3328599146* get_characters_12() const { return ___characters_12; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_characters_12() { return &___characters_12; }
	inline void set_characters_12(GameObjectU5BU5D_t3328599146* value)
	{
		___characters_12 = value;
		Il2CppCodeGenWriteBarrier((&___characters_12), value);
	}

	inline static int32_t get_offset_of_cameraOffset_13() { return static_cast<int32_t>(offsetof(MainMenuManager_t3185368703, ___cameraOffset_13)); }
	inline Vector3_t3722313464  get_cameraOffset_13() const { return ___cameraOffset_13; }
	inline Vector3_t3722313464 * get_address_of_cameraOffset_13() { return &___cameraOffset_13; }
	inline void set_cameraOffset_13(Vector3_t3722313464  value)
	{
		___cameraOffset_13 = value;
	}

	inline static int32_t get_offset_of_scrollTime_14() { return static_cast<int32_t>(offsetof(MainMenuManager_t3185368703, ___scrollTime_14)); }
	inline float get_scrollTime_14() const { return ___scrollTime_14; }
	inline float* get_address_of_scrollTime_14() { return &___scrollTime_14; }
	inline void set_scrollTime_14(float value)
	{
		___scrollTime_14 = value;
	}

	inline static int32_t get_offset_of_distanceBtwCharacters_15() { return static_cast<int32_t>(offsetof(MainMenuManager_t3185368703, ___distanceBtwCharacters_15)); }
	inline float get_distanceBtwCharacters_15() const { return ___distanceBtwCharacters_15; }
	inline float* get_address_of_distanceBtwCharacters_15() { return &___distanceBtwCharacters_15; }
	inline void set_distanceBtwCharacters_15(float value)
	{
		___distanceBtwCharacters_15 = value;
	}

	inline static int32_t get_offset_of_activePos_16() { return static_cast<int32_t>(offsetof(MainMenuManager_t3185368703, ___activePos_16)); }
	inline Vector3_t3722313464  get_activePos_16() const { return ___activePos_16; }
	inline Vector3_t3722313464 * get_address_of_activePos_16() { return &___activePos_16; }
	inline void set_activePos_16(Vector3_t3722313464  value)
	{
		___activePos_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINMENUMANAGER_T3185368703_H
#ifndef FPSCOUNTER_T2351221284_H
#define FPSCOUNTER_T2351221284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FPSCounter
struct  FPSCounter_t2351221284  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UnityStandardAssets.Utility.FPSCounter::m_FpsAccumulator
	int32_t ___m_FpsAccumulator_3;
	// System.Single UnityStandardAssets.Utility.FPSCounter::m_FpsNextPeriod
	float ___m_FpsNextPeriod_4;
	// System.Int32 UnityStandardAssets.Utility.FPSCounter::m_CurrentFps
	int32_t ___m_CurrentFps_5;
	// UnityEngine.UI.Text UnityStandardAssets.Utility.FPSCounter::m_Text
	Text_t1901882714 * ___m_Text_7;

public:
	inline static int32_t get_offset_of_m_FpsAccumulator_3() { return static_cast<int32_t>(offsetof(FPSCounter_t2351221284, ___m_FpsAccumulator_3)); }
	inline int32_t get_m_FpsAccumulator_3() const { return ___m_FpsAccumulator_3; }
	inline int32_t* get_address_of_m_FpsAccumulator_3() { return &___m_FpsAccumulator_3; }
	inline void set_m_FpsAccumulator_3(int32_t value)
	{
		___m_FpsAccumulator_3 = value;
	}

	inline static int32_t get_offset_of_m_FpsNextPeriod_4() { return static_cast<int32_t>(offsetof(FPSCounter_t2351221284, ___m_FpsNextPeriod_4)); }
	inline float get_m_FpsNextPeriod_4() const { return ___m_FpsNextPeriod_4; }
	inline float* get_address_of_m_FpsNextPeriod_4() { return &___m_FpsNextPeriod_4; }
	inline void set_m_FpsNextPeriod_4(float value)
	{
		___m_FpsNextPeriod_4 = value;
	}

	inline static int32_t get_offset_of_m_CurrentFps_5() { return static_cast<int32_t>(offsetof(FPSCounter_t2351221284, ___m_CurrentFps_5)); }
	inline int32_t get_m_CurrentFps_5() const { return ___m_CurrentFps_5; }
	inline int32_t* get_address_of_m_CurrentFps_5() { return &___m_CurrentFps_5; }
	inline void set_m_CurrentFps_5(int32_t value)
	{
		___m_CurrentFps_5 = value;
	}

	inline static int32_t get_offset_of_m_Text_7() { return static_cast<int32_t>(offsetof(FPSCounter_t2351221284, ___m_Text_7)); }
	inline Text_t1901882714 * get_m_Text_7() const { return ___m_Text_7; }
	inline Text_t1901882714 ** get_address_of_m_Text_7() { return &___m_Text_7; }
	inline void set_m_Text_7(Text_t1901882714 * value)
	{
		___m_Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNTER_T2351221284_H
#ifndef FORCEDRESET_T301124368_H
#define FORCEDRESET_T301124368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ForcedReset
struct  ForcedReset_t301124368  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORCEDRESET_T301124368_H
#ifndef MYITWEENTEST_T2779311591_H
#define MYITWEENTEST_T2779311591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyItweenTest
struct  MyItweenTest_t2779311591  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject MyItweenTest::TargetPos
	GameObject_t1113636619 * ___TargetPos_2;

public:
	inline static int32_t get_offset_of_TargetPos_2() { return static_cast<int32_t>(offsetof(MyItweenTest_t2779311591, ___TargetPos_2)); }
	inline GameObject_t1113636619 * get_TargetPos_2() const { return ___TargetPos_2; }
	inline GameObject_t1113636619 ** get_address_of_TargetPos_2() { return &___TargetPos_2; }
	inline void set_TargetPos_2(GameObject_t1113636619 * value)
	{
		___TargetPos_2 = value;
		Il2CppCodeGenWriteBarrier((&___TargetPos_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MYITWEENTEST_T2779311591_H
#ifndef PLANEVISUALIZATIONMANAGER_T1447224982_H
#define PLANEVISUALIZATIONMANAGER_T1447224982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaneVisualizationManager
struct  PlaneVisualizationManager_t1447224982  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text PlaneVisualizationManager::scaleText
	Text_t1901882714 * ___scaleText_2;
	// UnityEngine.UI.Text PlaneVisualizationManager::scaleText2
	Text_t1901882714 * ___scaleText2_3;
	// UnityEngine.UI.Slider PlaneVisualizationManager::slider
	Slider_t3903728902 * ___slider_4;
	// UnityEngine.GameObject PlaneVisualizationManager::TrackedPlanePrefab
	GameObject_t1113636619 * ___TrackedPlanePrefab_5;
	// UnityEngine.Camera PlaneVisualizationManager::m_firstPersonCamera
	Camera_t4157153871 * ___m_firstPersonCamera_6;
	// System.Collections.Generic.List`1<GoogleARCore.DetectedPlane> PlaneVisualizationManager::_newPlanes
	List_1_t1239236824 * ____newPlanes_7;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> PlaneVisualizationManager::PlaneObject
	List_1_t2585711361 * ___PlaneObject_8;
	// System.Boolean PlaneVisualizationManager::isStopInitialInstruction
	bool ___isStopInitialInstruction_9;
	// UnityEngine.GameObject[] PlaneVisualizationManager::Characters
	GameObjectU5BU5D_t3328599146* ___Characters_10;
	// UnityEngine.GameObject[] PlaneVisualizationManager::cardImages
	GameObjectU5BU5D_t3328599146* ___cardImages_11;
	// UnityEngine.GameObject[] PlaneVisualizationManager::cardPrefabs
	GameObjectU5BU5D_t3328599146* ___cardPrefabs_12;
	// UnityEngine.Material[] PlaneVisualizationManager::CharacterMaterials
	MaterialU5BU5D_t561872642* ___CharacterMaterials_13;
	// UnityEngine.Material[] PlaneVisualizationManager::SwordMaterials
	MaterialU5BU5D_t561872642* ___SwordMaterials_14;
	// System.String[] PlaneVisualizationManager::cardNames
	StringU5BU5D_t1281789340* ___cardNames_15;
	// UnityEngine.GameObject PlaneVisualizationManager::cardPrefabToInstantiate
	GameObject_t1113636619 * ___cardPrefabToInstantiate_16;
	// UnityEngine.Animator PlaneVisualizationManager::cardShuffleAnim
	Animator_t434523843 * ___cardShuffleAnim_17;
	// UnityEngine.UI.Text PlaneVisualizationManager::instructionText
	Text_t1901882714 * ___instructionText_18;
	// UnityEngine.GameObject[] PlaneVisualizationManager::CardDescriptionPopUpObj
	GameObjectU5BU5D_t3328599146* ___CardDescriptionPopUpObj_19;
	// UnityEngine.GameObject PlaneVisualizationManager::CardDeckObj
	GameObject_t1113636619 * ___CardDeckObj_20;
	// UnityEngine.GameObject PlaneVisualizationManager::Instruction1Obj
	GameObject_t1113636619 * ___Instruction1Obj_21;
	// UnityEngine.GameObject PlaneVisualizationManager::CharacterToInstantiate
	GameObject_t1113636619 * ___CharacterToInstantiate_22;
	// UnityEngine.Animator PlaneVisualizationManager::anim
	Animator_t434523843 * ___anim_23;
	// UnityEngine.GameObject PlaneVisualizationManager::currentCard
	GameObject_t1113636619 * ___currentCard_24;
	// System.String PlaneVisualizationManager::currentCardName
	String_t* ___currentCardName_25;
	// UnityEngine.GameObject[] PlaneVisualizationManager::cardPositions
	GameObjectU5BU5D_t3328599146* ___cardPositions_26;
	// System.Single PlaneVisualizationManager::lerpSpeed
	float ___lerpSpeed_27;
	// UnityEngine.AnimationCurve PlaneVisualizationManager::curveFadeIn
	AnimationCurve_t3046754366 * ___curveFadeIn_28;
	// UnityEngine.AnimationCurve PlaneVisualizationManager::curveFadeOut
	AnimationCurve_t3046754366 * ___curveFadeOut_29;
	// UnityEngine.GameObject PlaneVisualizationManager::myCurrentCharacter
	GameObject_t1113636619 * ___myCurrentCharacter_30;
	// UnityEngine.Material PlaneVisualizationManager::myCurrentCharacterMaterial
	Material_t340375123 * ___myCurrentCharacterMaterial_31;
	// System.Boolean PlaneVisualizationManager::isAlreadyCreated
	bool ___isAlreadyCreated_32;
	// System.Boolean PlaneVisualizationManager::stopPlaneDetection
	bool ___stopPlaneDetection_33;
	// System.Boolean PlaneVisualizationManager::isSwipeAllowed
	bool ___isSwipeAllowed_34;
	// System.Boolean PlaneVisualizationManager::isTapAllowed
	bool ___isTapAllowed_35;
	// System.Int32 PlaneVisualizationManager::randomNumber
	int32_t ___randomNumber_36;
	// System.Int32 PlaneVisualizationManager::previousrandomNumber
	int32_t ___previousrandomNumber_37;
	// System.Boolean PlaneVisualizationManager::isFadeIn
	bool ___isFadeIn_38;
	// System.Boolean PlaneVisualizationManager::isFadeOut
	bool ___isFadeOut_39;
	// System.Single PlaneVisualizationManager::currentCurveValue
	float ___currentCurveValue_40;
	// System.Single PlaneVisualizationManager::rotationAngle
	float ___rotationAngle_41;
	// TKPinchRecognizer PlaneVisualizationManager::pinchRecognizer
	TKPinchRecognizer_t788645703 * ___pinchRecognizer_43;
	// TKSwipeRecognizer PlaneVisualizationManager::swipeRecognizer
	TKSwipeRecognizer_t3210539170 * ___swipeRecognizer_44;
	// TKTapRecognizer PlaneVisualizationManager::tapRecognizer
	TKTapRecognizer_t2390504451 * ___tapRecognizer_45;
	// TKLongPressRecognizer PlaneVisualizationManager::longPressRecognizer
	TKLongPressRecognizer_t1475862313 * ___longPressRecognizer_46;
	// UnityEngine.XR.iOS.UnityARHitTestExample PlaneVisualizationManager::unityARHitTestExample
	UnityARHitTestExample_t457226377 * ___unityARHitTestExample_47;
	// UnityEngine.GameObject[] PlaneVisualizationManager::toSwitchOff
	GameObjectU5BU5D_t3328599146* ___toSwitchOff_48;
	// PointCloudParticleExample PlaneVisualizationManager::_PointCloudParticleExample
	PointCloudParticleExample_t182386800 * ____PointCloudParticleExample_49;
	// UnityEngine.XR.iOS.UnityARGeneratePlane PlaneVisualizationManager::_UnityARGeneratePlane
	UnityARGeneratePlane_t272564669 * ____UnityARGeneratePlane_50;
	// UnityPointCloudExample PlaneVisualizationManager::_UnityPointCloudExample
	UnityPointCloudExample_t3649008995 * ____UnityPointCloudExample_51;
	// UnityEngine.XR.iOS.UnityARKitControl PlaneVisualizationManager::_UnityARKitControl
	UnityARKitControl_t1358211756 * ____UnityARKitControl_52;
	// UnityEngine.Vector3 PlaneVisualizationManager::CharacterPosIOS
	Vector3_t3722313464  ___CharacterPosIOS_53;
	// UnityEngine.Quaternion PlaneVisualizationManager::CharacterRotIOS
	Quaternion_t2301928331  ___CharacterRotIOS_54;
	// System.Boolean PlaneVisualizationManager::m_IsQuitting
	bool ___m_IsQuitting_55;
	// System.Boolean PlaneVisualizationManager::onScreen
	bool ___onScreen_56;
	// System.Boolean PlaneVisualizationManager::isPinching
	bool ___isPinching_57;
	// System.Boolean PlaneVisualizationManager::isResetPinching
	bool ___isResetPinching_58;
	// System.Boolean PlaneVisualizationManager::isLongTouchDetectedAlready
	bool ___isLongTouchDetectedAlready_59;
	// System.Boolean PlaneVisualizationManager::isRecieveAnimationCallback
	bool ___isRecieveAnimationCallback_60;
	// System.String PlaneVisualizationManager::previousCardGame
	String_t* ___previousCardGame_61;
	// UnityEngine.GameObject PlaneVisualizationManager::LoadingPanel
	GameObject_t1113636619 * ___LoadingPanel_62;

public:
	inline static int32_t get_offset_of_scaleText_2() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___scaleText_2)); }
	inline Text_t1901882714 * get_scaleText_2() const { return ___scaleText_2; }
	inline Text_t1901882714 ** get_address_of_scaleText_2() { return &___scaleText_2; }
	inline void set_scaleText_2(Text_t1901882714 * value)
	{
		___scaleText_2 = value;
		Il2CppCodeGenWriteBarrier((&___scaleText_2), value);
	}

	inline static int32_t get_offset_of_scaleText2_3() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___scaleText2_3)); }
	inline Text_t1901882714 * get_scaleText2_3() const { return ___scaleText2_3; }
	inline Text_t1901882714 ** get_address_of_scaleText2_3() { return &___scaleText2_3; }
	inline void set_scaleText2_3(Text_t1901882714 * value)
	{
		___scaleText2_3 = value;
		Il2CppCodeGenWriteBarrier((&___scaleText2_3), value);
	}

	inline static int32_t get_offset_of_slider_4() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___slider_4)); }
	inline Slider_t3903728902 * get_slider_4() const { return ___slider_4; }
	inline Slider_t3903728902 ** get_address_of_slider_4() { return &___slider_4; }
	inline void set_slider_4(Slider_t3903728902 * value)
	{
		___slider_4 = value;
		Il2CppCodeGenWriteBarrier((&___slider_4), value);
	}

	inline static int32_t get_offset_of_TrackedPlanePrefab_5() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___TrackedPlanePrefab_5)); }
	inline GameObject_t1113636619 * get_TrackedPlanePrefab_5() const { return ___TrackedPlanePrefab_5; }
	inline GameObject_t1113636619 ** get_address_of_TrackedPlanePrefab_5() { return &___TrackedPlanePrefab_5; }
	inline void set_TrackedPlanePrefab_5(GameObject_t1113636619 * value)
	{
		___TrackedPlanePrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrackedPlanePrefab_5), value);
	}

	inline static int32_t get_offset_of_m_firstPersonCamera_6() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___m_firstPersonCamera_6)); }
	inline Camera_t4157153871 * get_m_firstPersonCamera_6() const { return ___m_firstPersonCamera_6; }
	inline Camera_t4157153871 ** get_address_of_m_firstPersonCamera_6() { return &___m_firstPersonCamera_6; }
	inline void set_m_firstPersonCamera_6(Camera_t4157153871 * value)
	{
		___m_firstPersonCamera_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_firstPersonCamera_6), value);
	}

	inline static int32_t get_offset_of__newPlanes_7() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ____newPlanes_7)); }
	inline List_1_t1239236824 * get__newPlanes_7() const { return ____newPlanes_7; }
	inline List_1_t1239236824 ** get_address_of__newPlanes_7() { return &____newPlanes_7; }
	inline void set__newPlanes_7(List_1_t1239236824 * value)
	{
		____newPlanes_7 = value;
		Il2CppCodeGenWriteBarrier((&____newPlanes_7), value);
	}

	inline static int32_t get_offset_of_PlaneObject_8() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___PlaneObject_8)); }
	inline List_1_t2585711361 * get_PlaneObject_8() const { return ___PlaneObject_8; }
	inline List_1_t2585711361 ** get_address_of_PlaneObject_8() { return &___PlaneObject_8; }
	inline void set_PlaneObject_8(List_1_t2585711361 * value)
	{
		___PlaneObject_8 = value;
		Il2CppCodeGenWriteBarrier((&___PlaneObject_8), value);
	}

	inline static int32_t get_offset_of_isStopInitialInstruction_9() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___isStopInitialInstruction_9)); }
	inline bool get_isStopInitialInstruction_9() const { return ___isStopInitialInstruction_9; }
	inline bool* get_address_of_isStopInitialInstruction_9() { return &___isStopInitialInstruction_9; }
	inline void set_isStopInitialInstruction_9(bool value)
	{
		___isStopInitialInstruction_9 = value;
	}

	inline static int32_t get_offset_of_Characters_10() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___Characters_10)); }
	inline GameObjectU5BU5D_t3328599146* get_Characters_10() const { return ___Characters_10; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_Characters_10() { return &___Characters_10; }
	inline void set_Characters_10(GameObjectU5BU5D_t3328599146* value)
	{
		___Characters_10 = value;
		Il2CppCodeGenWriteBarrier((&___Characters_10), value);
	}

	inline static int32_t get_offset_of_cardImages_11() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___cardImages_11)); }
	inline GameObjectU5BU5D_t3328599146* get_cardImages_11() const { return ___cardImages_11; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_cardImages_11() { return &___cardImages_11; }
	inline void set_cardImages_11(GameObjectU5BU5D_t3328599146* value)
	{
		___cardImages_11 = value;
		Il2CppCodeGenWriteBarrier((&___cardImages_11), value);
	}

	inline static int32_t get_offset_of_cardPrefabs_12() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___cardPrefabs_12)); }
	inline GameObjectU5BU5D_t3328599146* get_cardPrefabs_12() const { return ___cardPrefabs_12; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_cardPrefabs_12() { return &___cardPrefabs_12; }
	inline void set_cardPrefabs_12(GameObjectU5BU5D_t3328599146* value)
	{
		___cardPrefabs_12 = value;
		Il2CppCodeGenWriteBarrier((&___cardPrefabs_12), value);
	}

	inline static int32_t get_offset_of_CharacterMaterials_13() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___CharacterMaterials_13)); }
	inline MaterialU5BU5D_t561872642* get_CharacterMaterials_13() const { return ___CharacterMaterials_13; }
	inline MaterialU5BU5D_t561872642** get_address_of_CharacterMaterials_13() { return &___CharacterMaterials_13; }
	inline void set_CharacterMaterials_13(MaterialU5BU5D_t561872642* value)
	{
		___CharacterMaterials_13 = value;
		Il2CppCodeGenWriteBarrier((&___CharacterMaterials_13), value);
	}

	inline static int32_t get_offset_of_SwordMaterials_14() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___SwordMaterials_14)); }
	inline MaterialU5BU5D_t561872642* get_SwordMaterials_14() const { return ___SwordMaterials_14; }
	inline MaterialU5BU5D_t561872642** get_address_of_SwordMaterials_14() { return &___SwordMaterials_14; }
	inline void set_SwordMaterials_14(MaterialU5BU5D_t561872642* value)
	{
		___SwordMaterials_14 = value;
		Il2CppCodeGenWriteBarrier((&___SwordMaterials_14), value);
	}

	inline static int32_t get_offset_of_cardNames_15() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___cardNames_15)); }
	inline StringU5BU5D_t1281789340* get_cardNames_15() const { return ___cardNames_15; }
	inline StringU5BU5D_t1281789340** get_address_of_cardNames_15() { return &___cardNames_15; }
	inline void set_cardNames_15(StringU5BU5D_t1281789340* value)
	{
		___cardNames_15 = value;
		Il2CppCodeGenWriteBarrier((&___cardNames_15), value);
	}

	inline static int32_t get_offset_of_cardPrefabToInstantiate_16() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___cardPrefabToInstantiate_16)); }
	inline GameObject_t1113636619 * get_cardPrefabToInstantiate_16() const { return ___cardPrefabToInstantiate_16; }
	inline GameObject_t1113636619 ** get_address_of_cardPrefabToInstantiate_16() { return &___cardPrefabToInstantiate_16; }
	inline void set_cardPrefabToInstantiate_16(GameObject_t1113636619 * value)
	{
		___cardPrefabToInstantiate_16 = value;
		Il2CppCodeGenWriteBarrier((&___cardPrefabToInstantiate_16), value);
	}

	inline static int32_t get_offset_of_cardShuffleAnim_17() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___cardShuffleAnim_17)); }
	inline Animator_t434523843 * get_cardShuffleAnim_17() const { return ___cardShuffleAnim_17; }
	inline Animator_t434523843 ** get_address_of_cardShuffleAnim_17() { return &___cardShuffleAnim_17; }
	inline void set_cardShuffleAnim_17(Animator_t434523843 * value)
	{
		___cardShuffleAnim_17 = value;
		Il2CppCodeGenWriteBarrier((&___cardShuffleAnim_17), value);
	}

	inline static int32_t get_offset_of_instructionText_18() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___instructionText_18)); }
	inline Text_t1901882714 * get_instructionText_18() const { return ___instructionText_18; }
	inline Text_t1901882714 ** get_address_of_instructionText_18() { return &___instructionText_18; }
	inline void set_instructionText_18(Text_t1901882714 * value)
	{
		___instructionText_18 = value;
		Il2CppCodeGenWriteBarrier((&___instructionText_18), value);
	}

	inline static int32_t get_offset_of_CardDescriptionPopUpObj_19() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___CardDescriptionPopUpObj_19)); }
	inline GameObjectU5BU5D_t3328599146* get_CardDescriptionPopUpObj_19() const { return ___CardDescriptionPopUpObj_19; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_CardDescriptionPopUpObj_19() { return &___CardDescriptionPopUpObj_19; }
	inline void set_CardDescriptionPopUpObj_19(GameObjectU5BU5D_t3328599146* value)
	{
		___CardDescriptionPopUpObj_19 = value;
		Il2CppCodeGenWriteBarrier((&___CardDescriptionPopUpObj_19), value);
	}

	inline static int32_t get_offset_of_CardDeckObj_20() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___CardDeckObj_20)); }
	inline GameObject_t1113636619 * get_CardDeckObj_20() const { return ___CardDeckObj_20; }
	inline GameObject_t1113636619 ** get_address_of_CardDeckObj_20() { return &___CardDeckObj_20; }
	inline void set_CardDeckObj_20(GameObject_t1113636619 * value)
	{
		___CardDeckObj_20 = value;
		Il2CppCodeGenWriteBarrier((&___CardDeckObj_20), value);
	}

	inline static int32_t get_offset_of_Instruction1Obj_21() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___Instruction1Obj_21)); }
	inline GameObject_t1113636619 * get_Instruction1Obj_21() const { return ___Instruction1Obj_21; }
	inline GameObject_t1113636619 ** get_address_of_Instruction1Obj_21() { return &___Instruction1Obj_21; }
	inline void set_Instruction1Obj_21(GameObject_t1113636619 * value)
	{
		___Instruction1Obj_21 = value;
		Il2CppCodeGenWriteBarrier((&___Instruction1Obj_21), value);
	}

	inline static int32_t get_offset_of_CharacterToInstantiate_22() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___CharacterToInstantiate_22)); }
	inline GameObject_t1113636619 * get_CharacterToInstantiate_22() const { return ___CharacterToInstantiate_22; }
	inline GameObject_t1113636619 ** get_address_of_CharacterToInstantiate_22() { return &___CharacterToInstantiate_22; }
	inline void set_CharacterToInstantiate_22(GameObject_t1113636619 * value)
	{
		___CharacterToInstantiate_22 = value;
		Il2CppCodeGenWriteBarrier((&___CharacterToInstantiate_22), value);
	}

	inline static int32_t get_offset_of_anim_23() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___anim_23)); }
	inline Animator_t434523843 * get_anim_23() const { return ___anim_23; }
	inline Animator_t434523843 ** get_address_of_anim_23() { return &___anim_23; }
	inline void set_anim_23(Animator_t434523843 * value)
	{
		___anim_23 = value;
		Il2CppCodeGenWriteBarrier((&___anim_23), value);
	}

	inline static int32_t get_offset_of_currentCard_24() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___currentCard_24)); }
	inline GameObject_t1113636619 * get_currentCard_24() const { return ___currentCard_24; }
	inline GameObject_t1113636619 ** get_address_of_currentCard_24() { return &___currentCard_24; }
	inline void set_currentCard_24(GameObject_t1113636619 * value)
	{
		___currentCard_24 = value;
		Il2CppCodeGenWriteBarrier((&___currentCard_24), value);
	}

	inline static int32_t get_offset_of_currentCardName_25() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___currentCardName_25)); }
	inline String_t* get_currentCardName_25() const { return ___currentCardName_25; }
	inline String_t** get_address_of_currentCardName_25() { return &___currentCardName_25; }
	inline void set_currentCardName_25(String_t* value)
	{
		___currentCardName_25 = value;
		Il2CppCodeGenWriteBarrier((&___currentCardName_25), value);
	}

	inline static int32_t get_offset_of_cardPositions_26() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___cardPositions_26)); }
	inline GameObjectU5BU5D_t3328599146* get_cardPositions_26() const { return ___cardPositions_26; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_cardPositions_26() { return &___cardPositions_26; }
	inline void set_cardPositions_26(GameObjectU5BU5D_t3328599146* value)
	{
		___cardPositions_26 = value;
		Il2CppCodeGenWriteBarrier((&___cardPositions_26), value);
	}

	inline static int32_t get_offset_of_lerpSpeed_27() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___lerpSpeed_27)); }
	inline float get_lerpSpeed_27() const { return ___lerpSpeed_27; }
	inline float* get_address_of_lerpSpeed_27() { return &___lerpSpeed_27; }
	inline void set_lerpSpeed_27(float value)
	{
		___lerpSpeed_27 = value;
	}

	inline static int32_t get_offset_of_curveFadeIn_28() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___curveFadeIn_28)); }
	inline AnimationCurve_t3046754366 * get_curveFadeIn_28() const { return ___curveFadeIn_28; }
	inline AnimationCurve_t3046754366 ** get_address_of_curveFadeIn_28() { return &___curveFadeIn_28; }
	inline void set_curveFadeIn_28(AnimationCurve_t3046754366 * value)
	{
		___curveFadeIn_28 = value;
		Il2CppCodeGenWriteBarrier((&___curveFadeIn_28), value);
	}

	inline static int32_t get_offset_of_curveFadeOut_29() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___curveFadeOut_29)); }
	inline AnimationCurve_t3046754366 * get_curveFadeOut_29() const { return ___curveFadeOut_29; }
	inline AnimationCurve_t3046754366 ** get_address_of_curveFadeOut_29() { return &___curveFadeOut_29; }
	inline void set_curveFadeOut_29(AnimationCurve_t3046754366 * value)
	{
		___curveFadeOut_29 = value;
		Il2CppCodeGenWriteBarrier((&___curveFadeOut_29), value);
	}

	inline static int32_t get_offset_of_myCurrentCharacter_30() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___myCurrentCharacter_30)); }
	inline GameObject_t1113636619 * get_myCurrentCharacter_30() const { return ___myCurrentCharacter_30; }
	inline GameObject_t1113636619 ** get_address_of_myCurrentCharacter_30() { return &___myCurrentCharacter_30; }
	inline void set_myCurrentCharacter_30(GameObject_t1113636619 * value)
	{
		___myCurrentCharacter_30 = value;
		Il2CppCodeGenWriteBarrier((&___myCurrentCharacter_30), value);
	}

	inline static int32_t get_offset_of_myCurrentCharacterMaterial_31() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___myCurrentCharacterMaterial_31)); }
	inline Material_t340375123 * get_myCurrentCharacterMaterial_31() const { return ___myCurrentCharacterMaterial_31; }
	inline Material_t340375123 ** get_address_of_myCurrentCharacterMaterial_31() { return &___myCurrentCharacterMaterial_31; }
	inline void set_myCurrentCharacterMaterial_31(Material_t340375123 * value)
	{
		___myCurrentCharacterMaterial_31 = value;
		Il2CppCodeGenWriteBarrier((&___myCurrentCharacterMaterial_31), value);
	}

	inline static int32_t get_offset_of_isAlreadyCreated_32() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___isAlreadyCreated_32)); }
	inline bool get_isAlreadyCreated_32() const { return ___isAlreadyCreated_32; }
	inline bool* get_address_of_isAlreadyCreated_32() { return &___isAlreadyCreated_32; }
	inline void set_isAlreadyCreated_32(bool value)
	{
		___isAlreadyCreated_32 = value;
	}

	inline static int32_t get_offset_of_stopPlaneDetection_33() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___stopPlaneDetection_33)); }
	inline bool get_stopPlaneDetection_33() const { return ___stopPlaneDetection_33; }
	inline bool* get_address_of_stopPlaneDetection_33() { return &___stopPlaneDetection_33; }
	inline void set_stopPlaneDetection_33(bool value)
	{
		___stopPlaneDetection_33 = value;
	}

	inline static int32_t get_offset_of_isSwipeAllowed_34() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___isSwipeAllowed_34)); }
	inline bool get_isSwipeAllowed_34() const { return ___isSwipeAllowed_34; }
	inline bool* get_address_of_isSwipeAllowed_34() { return &___isSwipeAllowed_34; }
	inline void set_isSwipeAllowed_34(bool value)
	{
		___isSwipeAllowed_34 = value;
	}

	inline static int32_t get_offset_of_isTapAllowed_35() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___isTapAllowed_35)); }
	inline bool get_isTapAllowed_35() const { return ___isTapAllowed_35; }
	inline bool* get_address_of_isTapAllowed_35() { return &___isTapAllowed_35; }
	inline void set_isTapAllowed_35(bool value)
	{
		___isTapAllowed_35 = value;
	}

	inline static int32_t get_offset_of_randomNumber_36() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___randomNumber_36)); }
	inline int32_t get_randomNumber_36() const { return ___randomNumber_36; }
	inline int32_t* get_address_of_randomNumber_36() { return &___randomNumber_36; }
	inline void set_randomNumber_36(int32_t value)
	{
		___randomNumber_36 = value;
	}

	inline static int32_t get_offset_of_previousrandomNumber_37() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___previousrandomNumber_37)); }
	inline int32_t get_previousrandomNumber_37() const { return ___previousrandomNumber_37; }
	inline int32_t* get_address_of_previousrandomNumber_37() { return &___previousrandomNumber_37; }
	inline void set_previousrandomNumber_37(int32_t value)
	{
		___previousrandomNumber_37 = value;
	}

	inline static int32_t get_offset_of_isFadeIn_38() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___isFadeIn_38)); }
	inline bool get_isFadeIn_38() const { return ___isFadeIn_38; }
	inline bool* get_address_of_isFadeIn_38() { return &___isFadeIn_38; }
	inline void set_isFadeIn_38(bool value)
	{
		___isFadeIn_38 = value;
	}

	inline static int32_t get_offset_of_isFadeOut_39() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___isFadeOut_39)); }
	inline bool get_isFadeOut_39() const { return ___isFadeOut_39; }
	inline bool* get_address_of_isFadeOut_39() { return &___isFadeOut_39; }
	inline void set_isFadeOut_39(bool value)
	{
		___isFadeOut_39 = value;
	}

	inline static int32_t get_offset_of_currentCurveValue_40() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___currentCurveValue_40)); }
	inline float get_currentCurveValue_40() const { return ___currentCurveValue_40; }
	inline float* get_address_of_currentCurveValue_40() { return &___currentCurveValue_40; }
	inline void set_currentCurveValue_40(float value)
	{
		___currentCurveValue_40 = value;
	}

	inline static int32_t get_offset_of_rotationAngle_41() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___rotationAngle_41)); }
	inline float get_rotationAngle_41() const { return ___rotationAngle_41; }
	inline float* get_address_of_rotationAngle_41() { return &___rotationAngle_41; }
	inline void set_rotationAngle_41(float value)
	{
		___rotationAngle_41 = value;
	}

	inline static int32_t get_offset_of_pinchRecognizer_43() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___pinchRecognizer_43)); }
	inline TKPinchRecognizer_t788645703 * get_pinchRecognizer_43() const { return ___pinchRecognizer_43; }
	inline TKPinchRecognizer_t788645703 ** get_address_of_pinchRecognizer_43() { return &___pinchRecognizer_43; }
	inline void set_pinchRecognizer_43(TKPinchRecognizer_t788645703 * value)
	{
		___pinchRecognizer_43 = value;
		Il2CppCodeGenWriteBarrier((&___pinchRecognizer_43), value);
	}

	inline static int32_t get_offset_of_swipeRecognizer_44() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___swipeRecognizer_44)); }
	inline TKSwipeRecognizer_t3210539170 * get_swipeRecognizer_44() const { return ___swipeRecognizer_44; }
	inline TKSwipeRecognizer_t3210539170 ** get_address_of_swipeRecognizer_44() { return &___swipeRecognizer_44; }
	inline void set_swipeRecognizer_44(TKSwipeRecognizer_t3210539170 * value)
	{
		___swipeRecognizer_44 = value;
		Il2CppCodeGenWriteBarrier((&___swipeRecognizer_44), value);
	}

	inline static int32_t get_offset_of_tapRecognizer_45() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___tapRecognizer_45)); }
	inline TKTapRecognizer_t2390504451 * get_tapRecognizer_45() const { return ___tapRecognizer_45; }
	inline TKTapRecognizer_t2390504451 ** get_address_of_tapRecognizer_45() { return &___tapRecognizer_45; }
	inline void set_tapRecognizer_45(TKTapRecognizer_t2390504451 * value)
	{
		___tapRecognizer_45 = value;
		Il2CppCodeGenWriteBarrier((&___tapRecognizer_45), value);
	}

	inline static int32_t get_offset_of_longPressRecognizer_46() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___longPressRecognizer_46)); }
	inline TKLongPressRecognizer_t1475862313 * get_longPressRecognizer_46() const { return ___longPressRecognizer_46; }
	inline TKLongPressRecognizer_t1475862313 ** get_address_of_longPressRecognizer_46() { return &___longPressRecognizer_46; }
	inline void set_longPressRecognizer_46(TKLongPressRecognizer_t1475862313 * value)
	{
		___longPressRecognizer_46 = value;
		Il2CppCodeGenWriteBarrier((&___longPressRecognizer_46), value);
	}

	inline static int32_t get_offset_of_unityARHitTestExample_47() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___unityARHitTestExample_47)); }
	inline UnityARHitTestExample_t457226377 * get_unityARHitTestExample_47() const { return ___unityARHitTestExample_47; }
	inline UnityARHitTestExample_t457226377 ** get_address_of_unityARHitTestExample_47() { return &___unityARHitTestExample_47; }
	inline void set_unityARHitTestExample_47(UnityARHitTestExample_t457226377 * value)
	{
		___unityARHitTestExample_47 = value;
		Il2CppCodeGenWriteBarrier((&___unityARHitTestExample_47), value);
	}

	inline static int32_t get_offset_of_toSwitchOff_48() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___toSwitchOff_48)); }
	inline GameObjectU5BU5D_t3328599146* get_toSwitchOff_48() const { return ___toSwitchOff_48; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_toSwitchOff_48() { return &___toSwitchOff_48; }
	inline void set_toSwitchOff_48(GameObjectU5BU5D_t3328599146* value)
	{
		___toSwitchOff_48 = value;
		Il2CppCodeGenWriteBarrier((&___toSwitchOff_48), value);
	}

	inline static int32_t get_offset_of__PointCloudParticleExample_49() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ____PointCloudParticleExample_49)); }
	inline PointCloudParticleExample_t182386800 * get__PointCloudParticleExample_49() const { return ____PointCloudParticleExample_49; }
	inline PointCloudParticleExample_t182386800 ** get_address_of__PointCloudParticleExample_49() { return &____PointCloudParticleExample_49; }
	inline void set__PointCloudParticleExample_49(PointCloudParticleExample_t182386800 * value)
	{
		____PointCloudParticleExample_49 = value;
		Il2CppCodeGenWriteBarrier((&____PointCloudParticleExample_49), value);
	}

	inline static int32_t get_offset_of__UnityARGeneratePlane_50() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ____UnityARGeneratePlane_50)); }
	inline UnityARGeneratePlane_t272564669 * get__UnityARGeneratePlane_50() const { return ____UnityARGeneratePlane_50; }
	inline UnityARGeneratePlane_t272564669 ** get_address_of__UnityARGeneratePlane_50() { return &____UnityARGeneratePlane_50; }
	inline void set__UnityARGeneratePlane_50(UnityARGeneratePlane_t272564669 * value)
	{
		____UnityARGeneratePlane_50 = value;
		Il2CppCodeGenWriteBarrier((&____UnityARGeneratePlane_50), value);
	}

	inline static int32_t get_offset_of__UnityPointCloudExample_51() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ____UnityPointCloudExample_51)); }
	inline UnityPointCloudExample_t3649008995 * get__UnityPointCloudExample_51() const { return ____UnityPointCloudExample_51; }
	inline UnityPointCloudExample_t3649008995 ** get_address_of__UnityPointCloudExample_51() { return &____UnityPointCloudExample_51; }
	inline void set__UnityPointCloudExample_51(UnityPointCloudExample_t3649008995 * value)
	{
		____UnityPointCloudExample_51 = value;
		Il2CppCodeGenWriteBarrier((&____UnityPointCloudExample_51), value);
	}

	inline static int32_t get_offset_of__UnityARKitControl_52() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ____UnityARKitControl_52)); }
	inline UnityARKitControl_t1358211756 * get__UnityARKitControl_52() const { return ____UnityARKitControl_52; }
	inline UnityARKitControl_t1358211756 ** get_address_of__UnityARKitControl_52() { return &____UnityARKitControl_52; }
	inline void set__UnityARKitControl_52(UnityARKitControl_t1358211756 * value)
	{
		____UnityARKitControl_52 = value;
		Il2CppCodeGenWriteBarrier((&____UnityARKitControl_52), value);
	}

	inline static int32_t get_offset_of_CharacterPosIOS_53() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___CharacterPosIOS_53)); }
	inline Vector3_t3722313464  get_CharacterPosIOS_53() const { return ___CharacterPosIOS_53; }
	inline Vector3_t3722313464 * get_address_of_CharacterPosIOS_53() { return &___CharacterPosIOS_53; }
	inline void set_CharacterPosIOS_53(Vector3_t3722313464  value)
	{
		___CharacterPosIOS_53 = value;
	}

	inline static int32_t get_offset_of_CharacterRotIOS_54() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___CharacterRotIOS_54)); }
	inline Quaternion_t2301928331  get_CharacterRotIOS_54() const { return ___CharacterRotIOS_54; }
	inline Quaternion_t2301928331 * get_address_of_CharacterRotIOS_54() { return &___CharacterRotIOS_54; }
	inline void set_CharacterRotIOS_54(Quaternion_t2301928331  value)
	{
		___CharacterRotIOS_54 = value;
	}

	inline static int32_t get_offset_of_m_IsQuitting_55() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___m_IsQuitting_55)); }
	inline bool get_m_IsQuitting_55() const { return ___m_IsQuitting_55; }
	inline bool* get_address_of_m_IsQuitting_55() { return &___m_IsQuitting_55; }
	inline void set_m_IsQuitting_55(bool value)
	{
		___m_IsQuitting_55 = value;
	}

	inline static int32_t get_offset_of_onScreen_56() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___onScreen_56)); }
	inline bool get_onScreen_56() const { return ___onScreen_56; }
	inline bool* get_address_of_onScreen_56() { return &___onScreen_56; }
	inline void set_onScreen_56(bool value)
	{
		___onScreen_56 = value;
	}

	inline static int32_t get_offset_of_isPinching_57() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___isPinching_57)); }
	inline bool get_isPinching_57() const { return ___isPinching_57; }
	inline bool* get_address_of_isPinching_57() { return &___isPinching_57; }
	inline void set_isPinching_57(bool value)
	{
		___isPinching_57 = value;
	}

	inline static int32_t get_offset_of_isResetPinching_58() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___isResetPinching_58)); }
	inline bool get_isResetPinching_58() const { return ___isResetPinching_58; }
	inline bool* get_address_of_isResetPinching_58() { return &___isResetPinching_58; }
	inline void set_isResetPinching_58(bool value)
	{
		___isResetPinching_58 = value;
	}

	inline static int32_t get_offset_of_isLongTouchDetectedAlready_59() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___isLongTouchDetectedAlready_59)); }
	inline bool get_isLongTouchDetectedAlready_59() const { return ___isLongTouchDetectedAlready_59; }
	inline bool* get_address_of_isLongTouchDetectedAlready_59() { return &___isLongTouchDetectedAlready_59; }
	inline void set_isLongTouchDetectedAlready_59(bool value)
	{
		___isLongTouchDetectedAlready_59 = value;
	}

	inline static int32_t get_offset_of_isRecieveAnimationCallback_60() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___isRecieveAnimationCallback_60)); }
	inline bool get_isRecieveAnimationCallback_60() const { return ___isRecieveAnimationCallback_60; }
	inline bool* get_address_of_isRecieveAnimationCallback_60() { return &___isRecieveAnimationCallback_60; }
	inline void set_isRecieveAnimationCallback_60(bool value)
	{
		___isRecieveAnimationCallback_60 = value;
	}

	inline static int32_t get_offset_of_previousCardGame_61() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___previousCardGame_61)); }
	inline String_t* get_previousCardGame_61() const { return ___previousCardGame_61; }
	inline String_t** get_address_of_previousCardGame_61() { return &___previousCardGame_61; }
	inline void set_previousCardGame_61(String_t* value)
	{
		___previousCardGame_61 = value;
		Il2CppCodeGenWriteBarrier((&___previousCardGame_61), value);
	}

	inline static int32_t get_offset_of_LoadingPanel_62() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982, ___LoadingPanel_62)); }
	inline GameObject_t1113636619 * get_LoadingPanel_62() const { return ___LoadingPanel_62; }
	inline GameObject_t1113636619 ** get_address_of_LoadingPanel_62() { return &___LoadingPanel_62; }
	inline void set_LoadingPanel_62(GameObject_t1113636619 * value)
	{
		___LoadingPanel_62 = value;
		Il2CppCodeGenWriteBarrier((&___LoadingPanel_62), value);
	}
};

struct PlaneVisualizationManager_t1447224982_StaticFields
{
public:
	// PlaneVisualizationManager PlaneVisualizationManager::Instance
	PlaneVisualizationManager_t1447224982 * ___Instance_42;

public:
	inline static int32_t get_offset_of_Instance_42() { return static_cast<int32_t>(offsetof(PlaneVisualizationManager_t1447224982_StaticFields, ___Instance_42)); }
	inline PlaneVisualizationManager_t1447224982 * get_Instance_42() const { return ___Instance_42; }
	inline PlaneVisualizationManager_t1447224982 ** get_address_of_Instance_42() { return &___Instance_42; }
	inline void set_Instance_42(PlaneVisualizationManager_t1447224982 * value)
	{
		___Instance_42 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_42), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANEVISUALIZATIONMANAGER_T1447224982_H
#ifndef FOLLOWTARGET_T166153614_H
#define FOLLOWTARGET_T166153614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FollowTarget
struct  FollowTarget_t166153614  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UnityStandardAssets.Utility.FollowTarget::target
	Transform_t3600365921 * ___target_2;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.FollowTarget::offset
	Vector3_t3722313464  ___offset_3;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(FollowTarget_t166153614, ___target_2)); }
	inline Transform_t3600365921 * get_target_2() const { return ___target_2; }
	inline Transform_t3600365921 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t3600365921 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_offset_3() { return static_cast<int32_t>(offsetof(FollowTarget_t166153614, ___offset_3)); }
	inline Vector3_t3722313464  get_offset_3() const { return ___offset_3; }
	inline Vector3_t3722313464 * get_address_of_offset_3() { return &___offset_3; }
	inline void set_offset_3(Vector3_t3722313464  value)
	{
		___offset_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLLOWTARGET_T166153614_H
#ifndef EVENTSYSTEMCHECKER_T1882757729_H
#define EVENTSYSTEMCHECKER_T1882757729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EventSystemChecker
struct  EventSystemChecker_t1882757729  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTSYSTEMCHECKER_T1882757729_H
#ifndef DYNAMICSHADOWSETTINGS_T59119858_H
#define DYNAMICSHADOWSETTINGS_T59119858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.DynamicShadowSettings
struct  DynamicShadowSettings_t59119858  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Light UnityStandardAssets.Utility.DynamicShadowSettings::sunLight
	Light_t3756812086 * ___sunLight_2;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::minHeight
	float ___minHeight_3;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::minShadowDistance
	float ___minShadowDistance_4;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::minShadowBias
	float ___minShadowBias_5;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::maxHeight
	float ___maxHeight_6;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::maxShadowDistance
	float ___maxShadowDistance_7;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::maxShadowBias
	float ___maxShadowBias_8;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::adaptTime
	float ___adaptTime_9;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::m_SmoothHeight
	float ___m_SmoothHeight_10;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::m_ChangeSpeed
	float ___m_ChangeSpeed_11;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::m_OriginalStrength
	float ___m_OriginalStrength_12;

public:
	inline static int32_t get_offset_of_sunLight_2() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___sunLight_2)); }
	inline Light_t3756812086 * get_sunLight_2() const { return ___sunLight_2; }
	inline Light_t3756812086 ** get_address_of_sunLight_2() { return &___sunLight_2; }
	inline void set_sunLight_2(Light_t3756812086 * value)
	{
		___sunLight_2 = value;
		Il2CppCodeGenWriteBarrier((&___sunLight_2), value);
	}

	inline static int32_t get_offset_of_minHeight_3() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___minHeight_3)); }
	inline float get_minHeight_3() const { return ___minHeight_3; }
	inline float* get_address_of_minHeight_3() { return &___minHeight_3; }
	inline void set_minHeight_3(float value)
	{
		___minHeight_3 = value;
	}

	inline static int32_t get_offset_of_minShadowDistance_4() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___minShadowDistance_4)); }
	inline float get_minShadowDistance_4() const { return ___minShadowDistance_4; }
	inline float* get_address_of_minShadowDistance_4() { return &___minShadowDistance_4; }
	inline void set_minShadowDistance_4(float value)
	{
		___minShadowDistance_4 = value;
	}

	inline static int32_t get_offset_of_minShadowBias_5() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___minShadowBias_5)); }
	inline float get_minShadowBias_5() const { return ___minShadowBias_5; }
	inline float* get_address_of_minShadowBias_5() { return &___minShadowBias_5; }
	inline void set_minShadowBias_5(float value)
	{
		___minShadowBias_5 = value;
	}

	inline static int32_t get_offset_of_maxHeight_6() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___maxHeight_6)); }
	inline float get_maxHeight_6() const { return ___maxHeight_6; }
	inline float* get_address_of_maxHeight_6() { return &___maxHeight_6; }
	inline void set_maxHeight_6(float value)
	{
		___maxHeight_6 = value;
	}

	inline static int32_t get_offset_of_maxShadowDistance_7() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___maxShadowDistance_7)); }
	inline float get_maxShadowDistance_7() const { return ___maxShadowDistance_7; }
	inline float* get_address_of_maxShadowDistance_7() { return &___maxShadowDistance_7; }
	inline void set_maxShadowDistance_7(float value)
	{
		___maxShadowDistance_7 = value;
	}

	inline static int32_t get_offset_of_maxShadowBias_8() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___maxShadowBias_8)); }
	inline float get_maxShadowBias_8() const { return ___maxShadowBias_8; }
	inline float* get_address_of_maxShadowBias_8() { return &___maxShadowBias_8; }
	inline void set_maxShadowBias_8(float value)
	{
		___maxShadowBias_8 = value;
	}

	inline static int32_t get_offset_of_adaptTime_9() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___adaptTime_9)); }
	inline float get_adaptTime_9() const { return ___adaptTime_9; }
	inline float* get_address_of_adaptTime_9() { return &___adaptTime_9; }
	inline void set_adaptTime_9(float value)
	{
		___adaptTime_9 = value;
	}

	inline static int32_t get_offset_of_m_SmoothHeight_10() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___m_SmoothHeight_10)); }
	inline float get_m_SmoothHeight_10() const { return ___m_SmoothHeight_10; }
	inline float* get_address_of_m_SmoothHeight_10() { return &___m_SmoothHeight_10; }
	inline void set_m_SmoothHeight_10(float value)
	{
		___m_SmoothHeight_10 = value;
	}

	inline static int32_t get_offset_of_m_ChangeSpeed_11() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___m_ChangeSpeed_11)); }
	inline float get_m_ChangeSpeed_11() const { return ___m_ChangeSpeed_11; }
	inline float* get_address_of_m_ChangeSpeed_11() { return &___m_ChangeSpeed_11; }
	inline void set_m_ChangeSpeed_11(float value)
	{
		___m_ChangeSpeed_11 = value;
	}

	inline static int32_t get_offset_of_m_OriginalStrength_12() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___m_OriginalStrength_12)); }
	inline float get_m_OriginalStrength_12() const { return ___m_OriginalStrength_12; }
	inline float* get_address_of_m_OriginalStrength_12() { return &___m_OriginalStrength_12; }
	inline void set_m_OriginalStrength_12(float value)
	{
		___m_OriginalStrength_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICSHADOWSETTINGS_T59119858_H
#ifndef DRAGRIGIDBODY_T1600652016_H
#define DRAGRIGIDBODY_T1600652016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.DragRigidbody
struct  DragRigidbody_t1600652016  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.SpringJoint UnityStandardAssets.Utility.DragRigidbody::m_SpringJoint
	SpringJoint_t1912369980 * ___m_SpringJoint_8;

public:
	inline static int32_t get_offset_of_m_SpringJoint_8() { return static_cast<int32_t>(offsetof(DragRigidbody_t1600652016, ___m_SpringJoint_8)); }
	inline SpringJoint_t1912369980 * get_m_SpringJoint_8() const { return ___m_SpringJoint_8; }
	inline SpringJoint_t1912369980 ** get_address_of_m_SpringJoint_8() { return &___m_SpringJoint_8; }
	inline void set_m_SpringJoint_8(SpringJoint_t1912369980 * value)
	{
		___m_SpringJoint_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpringJoint_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGRIGIDBODY_T1600652016_H
#ifndef OBJECTRESETTER_T639177103_H
#define OBJECTRESETTER_T639177103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ObjectResetter
struct  ObjectResetter_t639177103  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.Utility.ObjectResetter::originalPosition
	Vector3_t3722313464  ___originalPosition_2;
	// UnityEngine.Quaternion UnityStandardAssets.Utility.ObjectResetter::originalRotation
	Quaternion_t2301928331  ___originalRotation_3;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UnityStandardAssets.Utility.ObjectResetter::originalStructure
	List_1_t777473367 * ___originalStructure_4;
	// UnityEngine.Rigidbody UnityStandardAssets.Utility.ObjectResetter::Rigidbody
	Rigidbody_t3916780224 * ___Rigidbody_5;

public:
	inline static int32_t get_offset_of_originalPosition_2() { return static_cast<int32_t>(offsetof(ObjectResetter_t639177103, ___originalPosition_2)); }
	inline Vector3_t3722313464  get_originalPosition_2() const { return ___originalPosition_2; }
	inline Vector3_t3722313464 * get_address_of_originalPosition_2() { return &___originalPosition_2; }
	inline void set_originalPosition_2(Vector3_t3722313464  value)
	{
		___originalPosition_2 = value;
	}

	inline static int32_t get_offset_of_originalRotation_3() { return static_cast<int32_t>(offsetof(ObjectResetter_t639177103, ___originalRotation_3)); }
	inline Quaternion_t2301928331  get_originalRotation_3() const { return ___originalRotation_3; }
	inline Quaternion_t2301928331 * get_address_of_originalRotation_3() { return &___originalRotation_3; }
	inline void set_originalRotation_3(Quaternion_t2301928331  value)
	{
		___originalRotation_3 = value;
	}

	inline static int32_t get_offset_of_originalStructure_4() { return static_cast<int32_t>(offsetof(ObjectResetter_t639177103, ___originalStructure_4)); }
	inline List_1_t777473367 * get_originalStructure_4() const { return ___originalStructure_4; }
	inline List_1_t777473367 ** get_address_of_originalStructure_4() { return &___originalStructure_4; }
	inline void set_originalStructure_4(List_1_t777473367 * value)
	{
		___originalStructure_4 = value;
		Il2CppCodeGenWriteBarrier((&___originalStructure_4), value);
	}

	inline static int32_t get_offset_of_Rigidbody_5() { return static_cast<int32_t>(offsetof(ObjectResetter_t639177103, ___Rigidbody_5)); }
	inline Rigidbody_t3916780224 * get_Rigidbody_5() const { return ___Rigidbody_5; }
	inline Rigidbody_t3916780224 ** get_address_of_Rigidbody_5() { return &___Rigidbody_5; }
	inline void set_Rigidbody_5(Rigidbody_t3916780224 * value)
	{
		___Rigidbody_5 = value;
		Il2CppCodeGenWriteBarrier((&___Rigidbody_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTRESETTER_T639177103_H
#ifndef SIMPLEACTIVATORMENU_T1387811551_H
#define SIMPLEACTIVATORMENU_T1387811551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.SimpleActivatorMenu
struct  SimpleActivatorMenu_t1387811551  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GUIText UnityStandardAssets.Utility.SimpleActivatorMenu::camSwitchButton
	GUIText_t402233326 * ___camSwitchButton_2;
	// UnityEngine.GameObject[] UnityStandardAssets.Utility.SimpleActivatorMenu::objects
	GameObjectU5BU5D_t3328599146* ___objects_3;
	// System.Int32 UnityStandardAssets.Utility.SimpleActivatorMenu::m_CurrentActiveObject
	int32_t ___m_CurrentActiveObject_4;

public:
	inline static int32_t get_offset_of_camSwitchButton_2() { return static_cast<int32_t>(offsetof(SimpleActivatorMenu_t1387811551, ___camSwitchButton_2)); }
	inline GUIText_t402233326 * get_camSwitchButton_2() const { return ___camSwitchButton_2; }
	inline GUIText_t402233326 ** get_address_of_camSwitchButton_2() { return &___camSwitchButton_2; }
	inline void set_camSwitchButton_2(GUIText_t402233326 * value)
	{
		___camSwitchButton_2 = value;
		Il2CppCodeGenWriteBarrier((&___camSwitchButton_2), value);
	}

	inline static int32_t get_offset_of_objects_3() { return static_cast<int32_t>(offsetof(SimpleActivatorMenu_t1387811551, ___objects_3)); }
	inline GameObjectU5BU5D_t3328599146* get_objects_3() const { return ___objects_3; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_objects_3() { return &___objects_3; }
	inline void set_objects_3(GameObjectU5BU5D_t3328599146* value)
	{
		___objects_3 = value;
		Il2CppCodeGenWriteBarrier((&___objects_3), value);
	}

	inline static int32_t get_offset_of_m_CurrentActiveObject_4() { return static_cast<int32_t>(offsetof(SimpleActivatorMenu_t1387811551, ___m_CurrentActiveObject_4)); }
	inline int32_t get_m_CurrentActiveObject_4() const { return ___m_CurrentActiveObject_4; }
	inline int32_t* get_address_of_m_CurrentActiveObject_4() { return &___m_CurrentActiveObject_4; }
	inline void set_m_CurrentActiveObject_4(int32_t value)
	{
		___m_CurrentActiveObject_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEACTIVATORMENU_T1387811551_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (Defaults_t3148213711), -1, sizeof(Defaults_t3148213711_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2200[15] = 
{
	Defaults_t3148213711_StaticFields::get_offset_of_time_0(),
	Defaults_t3148213711_StaticFields::get_offset_of_delay_1(),
	Defaults_t3148213711_StaticFields::get_offset_of_namedColorValue_2(),
	Defaults_t3148213711_StaticFields::get_offset_of_loopType_3(),
	Defaults_t3148213711_StaticFields::get_offset_of_easeType_4(),
	Defaults_t3148213711_StaticFields::get_offset_of_lookSpeed_5(),
	Defaults_t3148213711_StaticFields::get_offset_of_isLocal_6(),
	Defaults_t3148213711_StaticFields::get_offset_of_space_7(),
	Defaults_t3148213711_StaticFields::get_offset_of_orientToPath_8(),
	Defaults_t3148213711_StaticFields::get_offset_of_color_9(),
	Defaults_t3148213711_StaticFields::get_offset_of_updateTimePercentage_10(),
	Defaults_t3148213711_StaticFields::get_offset_of_updateTime_11(),
	Defaults_t3148213711_StaticFields::get_offset_of_lookAhead_12(),
	Defaults_t3148213711_StaticFields::get_offset_of_useRealTime_13(),
	Defaults_t3148213711_StaticFields::get_offset_of_up_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (CRSpline_t2815350084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2201[1] = 
{
	CRSpline_t2815350084::get_offset_of_pts_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (U3CTweenDelayU3Ec__Iterator0_t2686771544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2202[4] = 
{
	U3CTweenDelayU3Ec__Iterator0_t2686771544::get_offset_of_U24this_0(),
	U3CTweenDelayU3Ec__Iterator0_t2686771544::get_offset_of_U24current_1(),
	U3CTweenDelayU3Ec__Iterator0_t2686771544::get_offset_of_U24disposing_2(),
	U3CTweenDelayU3Ec__Iterator0_t2686771544::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (U3CTweenRestartU3Ec__Iterator1_t1737386981), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2203[4] = 
{
	U3CTweenRestartU3Ec__Iterator1_t1737386981::get_offset_of_U24this_0(),
	U3CTweenRestartU3Ec__Iterator1_t1737386981::get_offset_of_U24current_1(),
	U3CTweenRestartU3Ec__Iterator1_t1737386981::get_offset_of_U24disposing_2(),
	U3CTweenRestartU3Ec__Iterator1_t1737386981::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (U3CStartU3Ec__Iterator2_t2390838266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2204[4] = 
{
	U3CStartU3Ec__Iterator2_t2390838266::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator2_t2390838266::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator2_t2390838266::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator2_t2390838266::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (MoveSample_t3412539464), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (RotateSample_t3002381122), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (SampleInfo_t3693012684), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (AICharacterControl_t2972373937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2208[3] = 
{
	AICharacterControl_t2972373937::get_offset_of_U3CagentU3Ek__BackingField_2(),
	AICharacterControl_t2972373937::get_offset_of_U3CcharacterU3Ek__BackingField_3(),
	AICharacterControl_t2972373937::get_offset_of_target_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (ThirdPersonCharacter_t1711070432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2209[20] = 
{
	ThirdPersonCharacter_t1711070432::get_offset_of_m_MovingTurnSpeed_2(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_StationaryTurnSpeed_3(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_JumpPower_4(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_GravityMultiplier_5(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_RunCycleLegOffset_6(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_MoveSpeedMultiplier_7(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_AnimSpeedMultiplier_8(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_GroundCheckDistance_9(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_Rigidbody_10(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_Animator_11(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_IsGrounded_12(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_OrigGroundCheckDistance_13(),
	0,
	ThirdPersonCharacter_t1711070432::get_offset_of_m_TurnAmount_15(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_ForwardAmount_16(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_GroundNormal_17(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_CapsuleHeight_18(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_CapsuleCenter_19(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_Capsule_20(),
	ThirdPersonCharacter_t1711070432::get_offset_of_m_Crouching_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (ThirdPersonUserControl_t1527285130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2210[5] = 
{
	ThirdPersonUserControl_t1527285130::get_offset_of_m_Character_2(),
	ThirdPersonUserControl_t1527285130::get_offset_of_m_Cam_3(),
	ThirdPersonUserControl_t1527285130::get_offset_of_m_CamForward_4(),
	ThirdPersonUserControl_t1527285130::get_offset_of_m_Move_5(),
	ThirdPersonUserControl_t1527285130::get_offset_of_m_Jump_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (AxisTouchButton_t3522881333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2211[6] = 
{
	AxisTouchButton_t3522881333::get_offset_of_axisName_2(),
	AxisTouchButton_t3522881333::get_offset_of_axisValue_3(),
	AxisTouchButton_t3522881333::get_offset_of_responseSpeed_4(),
	AxisTouchButton_t3522881333::get_offset_of_returnToCentreSpeed_5(),
	AxisTouchButton_t3522881333::get_offset_of_m_PairedWith_6(),
	AxisTouchButton_t3522881333::get_offset_of_m_Axis_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (ButtonHandler_t823762219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2212[1] = 
{
	ButtonHandler_t823762219::get_offset_of_Name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (CrossPlatformInputManager_t191731427), -1, sizeof(CrossPlatformInputManager_t191731427_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2213[3] = 
{
	CrossPlatformInputManager_t191731427_StaticFields::get_offset_of_activeInput_0(),
	CrossPlatformInputManager_t191731427_StaticFields::get_offset_of_s_TouchInput_1(),
	CrossPlatformInputManager_t191731427_StaticFields::get_offset_of_s_HardwareInput_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (ActiveInputMethod_t139315314)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2214[3] = 
{
	ActiveInputMethod_t139315314::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (VirtualAxis_t4087348596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2215[3] = 
{
	VirtualAxis_t4087348596::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualAxis_t4087348596::get_offset_of_m_Value_1(),
	VirtualAxis_t4087348596::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (VirtualButton_t2756566330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2216[5] = 
{
	VirtualButton_t2756566330::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualButton_t2756566330::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_1(),
	VirtualButton_t2756566330::get_offset_of_m_LastPressedFrame_2(),
	VirtualButton_t2756566330::get_offset_of_m_ReleasedFrame_3(),
	VirtualButton_t2756566330::get_offset_of_m_Pressed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (InputAxisScrollbar_t457958266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2217[1] = 
{
	InputAxisScrollbar_t457958266::get_offset_of_axis_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (Joystick_t2204371675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2218[9] = 
{
	Joystick_t2204371675::get_offset_of_MovementRange_2(),
	Joystick_t2204371675::get_offset_of_axesToUse_3(),
	Joystick_t2204371675::get_offset_of_horizontalAxisName_4(),
	Joystick_t2204371675::get_offset_of_verticalAxisName_5(),
	Joystick_t2204371675::get_offset_of_m_StartPos_6(),
	Joystick_t2204371675::get_offset_of_m_UseX_7(),
	Joystick_t2204371675::get_offset_of_m_UseY_8(),
	Joystick_t2204371675::get_offset_of_m_HorizontalVirtualAxis_9(),
	Joystick_t2204371675::get_offset_of_m_VerticalVirtualAxis_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (AxisOption_t3128671669)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2219[4] = 
{
	AxisOption_t3128671669::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (MobileControlRig_t1964600252), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (MobileInput_t2025745297), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (StandaloneInput_t1343950252), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (TiltInput_t1639936653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2223[5] = 
{
	TiltInput_t1639936653::get_offset_of_mapping_2(),
	TiltInput_t1639936653::get_offset_of_tiltAroundAxis_3(),
	TiltInput_t1639936653::get_offset_of_fullTiltAngle_4(),
	TiltInput_t1639936653::get_offset_of_centreAngleOffset_5(),
	TiltInput_t1639936653::get_offset_of_m_SteerAxis_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (AxisOptions_t3101732129)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2224[3] = 
{
	AxisOptions_t3101732129::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (AxisMapping_t3982445645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2225[2] = 
{
	AxisMapping_t3982445645::get_offset_of_type_0(),
	AxisMapping_t3982445645::get_offset_of_axisName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (MappingType_t2039944511)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2226[5] = 
{
	MappingType_t2039944511::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (TouchPad_t539039257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2227[18] = 
{
	TouchPad_t539039257::get_offset_of_axesToUse_2(),
	TouchPad_t539039257::get_offset_of_controlStyle_3(),
	TouchPad_t539039257::get_offset_of_horizontalAxisName_4(),
	TouchPad_t539039257::get_offset_of_verticalAxisName_5(),
	TouchPad_t539039257::get_offset_of_Xsensitivity_6(),
	TouchPad_t539039257::get_offset_of_Ysensitivity_7(),
	TouchPad_t539039257::get_offset_of_m_StartPos_8(),
	TouchPad_t539039257::get_offset_of_m_PreviousDelta_9(),
	TouchPad_t539039257::get_offset_of_m_JoytickOutput_10(),
	TouchPad_t539039257::get_offset_of_m_UseX_11(),
	TouchPad_t539039257::get_offset_of_m_UseY_12(),
	TouchPad_t539039257::get_offset_of_m_HorizontalVirtualAxis_13(),
	TouchPad_t539039257::get_offset_of_m_VerticalVirtualAxis_14(),
	TouchPad_t539039257::get_offset_of_m_Dragging_15(),
	TouchPad_t539039257::get_offset_of_m_Id_16(),
	TouchPad_t539039257::get_offset_of_m_PreviousTouchPos_17(),
	TouchPad_t539039257::get_offset_of_m_Center_18(),
	TouchPad_t539039257::get_offset_of_m_Image_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (AxisOption_t1372819835)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2228[4] = 
{
	AxisOption_t1372819835::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (ControlStyle_t1372986211)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2229[4] = 
{
	ControlStyle_t1372986211::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (VirtualInput_t2597455733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2230[4] = 
{
	VirtualInput_t2597455733::get_offset_of_U3CvirtualMousePositionU3Ek__BackingField_0(),
	VirtualInput_t2597455733::get_offset_of_m_VirtualAxes_1(),
	VirtualInput_t2597455733::get_offset_of_m_VirtualButtons_2(),
	VirtualInput_t2597455733::get_offset_of_m_AlwaysUseVirtual_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (ActivateTrigger_t3349759092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2231[5] = 
{
	ActivateTrigger_t3349759092::get_offset_of_action_2(),
	ActivateTrigger_t3349759092::get_offset_of_target_3(),
	ActivateTrigger_t3349759092::get_offset_of_source_4(),
	ActivateTrigger_t3349759092::get_offset_of_triggerCount_5(),
	ActivateTrigger_t3349759092::get_offset_of_repeatTrigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (Mode_t3024470803)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2232[7] = 
{
	Mode_t3024470803::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (AlphaButtonClickMask_t141136539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2233[1] = 
{
	AlphaButtonClickMask_t141136539::get_offset_of__image_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (AutoMobileShaderSwitch_t568447889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2234[1] = 
{
	AutoMobileShaderSwitch_t568447889::get_offset_of_m_ReplacementList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (ReplacementDefinition_t2693741842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2235[2] = 
{
	ReplacementDefinition_t2693741842::get_offset_of_original_0(),
	ReplacementDefinition_t2693741842::get_offset_of_replacement_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (ReplacementList_t1887104210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2236[1] = 
{
	ReplacementList_t1887104210::get_offset_of_items_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (AutoMoveAndRotate_t2437913015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2237[4] = 
{
	AutoMoveAndRotate_t2437913015::get_offset_of_moveUnitsPerSecond_2(),
	AutoMoveAndRotate_t2437913015::get_offset_of_rotateDegreesPerSecond_3(),
	AutoMoveAndRotate_t2437913015::get_offset_of_ignoreTimescale_4(),
	AutoMoveAndRotate_t2437913015::get_offset_of_m_LastRealTime_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (Vector3andSpace_t219844479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2238[2] = 
{
	Vector3andSpace_t219844479::get_offset_of_value_0(),
	Vector3andSpace_t219844479::get_offset_of_space_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (CameraRefocus_t4263235746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2239[5] = 
{
	CameraRefocus_t4263235746::get_offset_of_Camera_0(),
	CameraRefocus_t4263235746::get_offset_of_Lookatpoint_1(),
	CameraRefocus_t4263235746::get_offset_of_Parent_2(),
	CameraRefocus_t4263235746::get_offset_of_m_OrigCameraPos_3(),
	CameraRefocus_t4263235746::get_offset_of_m_Refocus_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (CurveControlledBob_t2679313829), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2240[9] = 
{
	CurveControlledBob_t2679313829::get_offset_of_HorizontalBobRange_0(),
	CurveControlledBob_t2679313829::get_offset_of_VerticalBobRange_1(),
	CurveControlledBob_t2679313829::get_offset_of_Bobcurve_2(),
	CurveControlledBob_t2679313829::get_offset_of_VerticaltoHorizontalRatio_3(),
	CurveControlledBob_t2679313829::get_offset_of_m_CyclePositionX_4(),
	CurveControlledBob_t2679313829::get_offset_of_m_CyclePositionY_5(),
	CurveControlledBob_t2679313829::get_offset_of_m_BobBaseInterval_6(),
	CurveControlledBob_t2679313829::get_offset_of_m_OriginalCameraPosition_7(),
	CurveControlledBob_t2679313829::get_offset_of_m_Time_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (DragRigidbody_t1600652016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2241[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	DragRigidbody_t1600652016::get_offset_of_m_SpringJoint_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (U3CDragObjectU3Ec__Iterator0_t4151609119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2242[9] = 
{
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U3ColdDragU3E__0_0(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U3ColdAngularDragU3E__0_1(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U3CmainCameraU3E__0_2(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U3CrayU3E__1_3(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_distance_4(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U24this_5(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U24current_6(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U24disposing_7(),
	U3CDragObjectU3Ec__Iterator0_t4151609119::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (DynamicShadowSettings_t59119858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2243[11] = 
{
	DynamicShadowSettings_t59119858::get_offset_of_sunLight_2(),
	DynamicShadowSettings_t59119858::get_offset_of_minHeight_3(),
	DynamicShadowSettings_t59119858::get_offset_of_minShadowDistance_4(),
	DynamicShadowSettings_t59119858::get_offset_of_minShadowBias_5(),
	DynamicShadowSettings_t59119858::get_offset_of_maxHeight_6(),
	DynamicShadowSettings_t59119858::get_offset_of_maxShadowDistance_7(),
	DynamicShadowSettings_t59119858::get_offset_of_maxShadowBias_8(),
	DynamicShadowSettings_t59119858::get_offset_of_adaptTime_9(),
	DynamicShadowSettings_t59119858::get_offset_of_m_SmoothHeight_10(),
	DynamicShadowSettings_t59119858::get_offset_of_m_ChangeSpeed_11(),
	DynamicShadowSettings_t59119858::get_offset_of_m_OriginalStrength_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (EventSystemChecker_t1882757729), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (FollowTarget_t166153614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2245[2] = 
{
	FollowTarget_t166153614::get_offset_of_target_2(),
	FollowTarget_t166153614::get_offset_of_offset_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (ForcedReset_t301124368), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (FOVKick_t120370150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2247[6] = 
{
	FOVKick_t120370150::get_offset_of_Camera_0(),
	FOVKick_t120370150::get_offset_of_originalFov_1(),
	FOVKick_t120370150::get_offset_of_FOVIncrease_2(),
	FOVKick_t120370150::get_offset_of_TimeToIncrease_3(),
	FOVKick_t120370150::get_offset_of_TimeToDecrease_4(),
	FOVKick_t120370150::get_offset_of_IncreaseCurve_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (U3CFOVKickUpU3Ec__Iterator0_t3738408313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2248[5] = 
{
	U3CFOVKickUpU3Ec__Iterator0_t3738408313::get_offset_of_U3CtU3E__0_0(),
	U3CFOVKickUpU3Ec__Iterator0_t3738408313::get_offset_of_U24this_1(),
	U3CFOVKickUpU3Ec__Iterator0_t3738408313::get_offset_of_U24current_2(),
	U3CFOVKickUpU3Ec__Iterator0_t3738408313::get_offset_of_U24disposing_3(),
	U3CFOVKickUpU3Ec__Iterator0_t3738408313::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (U3CFOVKickDownU3Ec__Iterator1_t1440840980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2249[5] = 
{
	U3CFOVKickDownU3Ec__Iterator1_t1440840980::get_offset_of_U3CtU3E__0_0(),
	U3CFOVKickDownU3Ec__Iterator1_t1440840980::get_offset_of_U24this_1(),
	U3CFOVKickDownU3Ec__Iterator1_t1440840980::get_offset_of_U24current_2(),
	U3CFOVKickDownU3Ec__Iterator1_t1440840980::get_offset_of_U24disposing_3(),
	U3CFOVKickDownU3Ec__Iterator1_t1440840980::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (FPSCounter_t2351221284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2250[6] = 
{
	0,
	FPSCounter_t2351221284::get_offset_of_m_FpsAccumulator_3(),
	FPSCounter_t2351221284::get_offset_of_m_FpsNextPeriod_4(),
	FPSCounter_t2351221284::get_offset_of_m_CurrentFps_5(),
	0,
	FPSCounter_t2351221284::get_offset_of_m_Text_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (LerpControlledBob_t1895875871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2251[3] = 
{
	LerpControlledBob_t1895875871::get_offset_of_BobDuration_0(),
	LerpControlledBob_t1895875871::get_offset_of_BobAmount_1(),
	LerpControlledBob_t1895875871::get_offset_of_m_Offset_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (U3CDoBobCycleU3Ec__Iterator0_t1149538828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2252[5] = 
{
	U3CDoBobCycleU3Ec__Iterator0_t1149538828::get_offset_of_U3CtU3E__0_0(),
	U3CDoBobCycleU3Ec__Iterator0_t1149538828::get_offset_of_U24this_1(),
	U3CDoBobCycleU3Ec__Iterator0_t1149538828::get_offset_of_U24current_2(),
	U3CDoBobCycleU3Ec__Iterator0_t1149538828::get_offset_of_U24disposing_3(),
	U3CDoBobCycleU3Ec__Iterator0_t1149538828::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (ObjectResetter_t639177103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2253[4] = 
{
	ObjectResetter_t639177103::get_offset_of_originalPosition_2(),
	ObjectResetter_t639177103::get_offset_of_originalRotation_3(),
	ObjectResetter_t639177103::get_offset_of_originalStructure_4(),
	ObjectResetter_t639177103::get_offset_of_Rigidbody_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (U3CResetCoroutineU3Ec__Iterator0_t3232105836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2254[7] = 
{
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_delay_0(),
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_U24locvar0_1(),
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_U24locvar1_2(),
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_U24this_3(),
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_U24current_4(),
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_U24disposing_5(),
	U3CResetCoroutineU3Ec__Iterator0_t3232105836::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (ParticleSystemDestroyer_t558680695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2255[4] = 
{
	ParticleSystemDestroyer_t558680695::get_offset_of_minDuration_2(),
	ParticleSystemDestroyer_t558680695::get_offset_of_maxDuration_3(),
	ParticleSystemDestroyer_t558680695::get_offset_of_m_MaxLifetime_4(),
	ParticleSystemDestroyer_t558680695::get_offset_of_m_EarlyStop_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (U3CStartU3Ec__Iterator0_t980021917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2256[10] = 
{
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U3CsystemsU3E__0_0(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24locvar0_1(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24locvar1_2(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U3CstopTimeU3E__0_3(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24locvar2_4(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24locvar3_5(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24this_6(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24current_7(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24disposing_8(),
	U3CStartU3Ec__Iterator0_t980021917::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (PlatformSpecificContent_t1404549723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2257[4] = 
{
	PlatformSpecificContent_t1404549723::get_offset_of_m_BuildTargetGroup_2(),
	PlatformSpecificContent_t1404549723::get_offset_of_m_Content_3(),
	PlatformSpecificContent_t1404549723::get_offset_of_m_MonoBehaviours_4(),
	PlatformSpecificContent_t1404549723::get_offset_of_m_ChildrenOfThisObject_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (BuildTargetGroup_t72322187)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2258[3] = 
{
	BuildTargetGroup_t72322187::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (SimpleActivatorMenu_t1387811551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2259[3] = 
{
	SimpleActivatorMenu_t1387811551::get_offset_of_camSwitchButton_2(),
	SimpleActivatorMenu_t1387811551::get_offset_of_objects_3(),
	SimpleActivatorMenu_t1387811551::get_offset_of_m_CurrentActiveObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (SimpleMouseRotator_t2364742953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2260[10] = 
{
	SimpleMouseRotator_t2364742953::get_offset_of_rotationRange_2(),
	SimpleMouseRotator_t2364742953::get_offset_of_rotationSpeed_3(),
	SimpleMouseRotator_t2364742953::get_offset_of_dampingTime_4(),
	SimpleMouseRotator_t2364742953::get_offset_of_autoZeroVerticalOnMobile_5(),
	SimpleMouseRotator_t2364742953::get_offset_of_autoZeroHorizontalOnMobile_6(),
	SimpleMouseRotator_t2364742953::get_offset_of_relative_7(),
	SimpleMouseRotator_t2364742953::get_offset_of_m_TargetAngles_8(),
	SimpleMouseRotator_t2364742953::get_offset_of_m_FollowAngles_9(),
	SimpleMouseRotator_t2364742953::get_offset_of_m_FollowVelocity_10(),
	SimpleMouseRotator_t2364742953::get_offset_of_m_OriginalRotation_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (SmoothFollow_t4204731361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2261[5] = 
{
	SmoothFollow_t4204731361::get_offset_of_target_2(),
	SmoothFollow_t4204731361::get_offset_of_distance_3(),
	SmoothFollow_t4204731361::get_offset_of_height_4(),
	SmoothFollow_t4204731361::get_offset_of_rotationDamping_5(),
	SmoothFollow_t4204731361::get_offset_of_heightDamping_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (TimedObjectActivator_t1846709985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2262[1] = 
{
	TimedObjectActivator_t1846709985::get_offset_of_entries_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (Action_t837364808)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2263[6] = 
{
	Action_t837364808::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (Entry_t2725803170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2264[3] = 
{
	Entry_t2725803170::get_offset_of_target_0(),
	Entry_t2725803170::get_offset_of_action_1(),
	Entry_t2725803170::get_offset_of_delay_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (Entries_t3168066469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2265[1] = 
{
	Entries_t3168066469::get_offset_of_entries_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (U3CActivateU3Ec__Iterator0_t2664723090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2266[4] = 
{
	U3CActivateU3Ec__Iterator0_t2664723090::get_offset_of_entry_0(),
	U3CActivateU3Ec__Iterator0_t2664723090::get_offset_of_U24current_1(),
	U3CActivateU3Ec__Iterator0_t2664723090::get_offset_of_U24disposing_2(),
	U3CActivateU3Ec__Iterator0_t2664723090::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (U3CDeactivateU3Ec__Iterator1_t730025274), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2267[4] = 
{
	U3CDeactivateU3Ec__Iterator1_t730025274::get_offset_of_entry_0(),
	U3CDeactivateU3Ec__Iterator1_t730025274::get_offset_of_U24current_1(),
	U3CDeactivateU3Ec__Iterator1_t730025274::get_offset_of_U24disposing_2(),
	U3CDeactivateU3Ec__Iterator1_t730025274::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (U3CReloadLevelU3Ec__Iterator2_t2784493974), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2268[4] = 
{
	U3CReloadLevelU3Ec__Iterator2_t2784493974::get_offset_of_entry_0(),
	U3CReloadLevelU3Ec__Iterator2_t2784493974::get_offset_of_U24current_1(),
	U3CReloadLevelU3Ec__Iterator2_t2784493974::get_offset_of_U24disposing_2(),
	U3CReloadLevelU3Ec__Iterator2_t2784493974::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (TimedObjectDestructor_t3438860414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2269[2] = 
{
	TimedObjectDestructor_t3438860414::get_offset_of_m_TimeOut_2(),
	TimedObjectDestructor_t3438860414::get_offset_of_m_DetachChildren_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (WaypointCircuit_t445075330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2270[16] = 
{
	WaypointCircuit_t445075330::get_offset_of_waypointList_2(),
	WaypointCircuit_t445075330::get_offset_of_smoothRoute_3(),
	WaypointCircuit_t445075330::get_offset_of_numPoints_4(),
	WaypointCircuit_t445075330::get_offset_of_points_5(),
	WaypointCircuit_t445075330::get_offset_of_distances_6(),
	WaypointCircuit_t445075330::get_offset_of_editorVisualisationSubsteps_7(),
	WaypointCircuit_t445075330::get_offset_of_U3CLengthU3Ek__BackingField_8(),
	WaypointCircuit_t445075330::get_offset_of_p0n_9(),
	WaypointCircuit_t445075330::get_offset_of_p1n_10(),
	WaypointCircuit_t445075330::get_offset_of_p2n_11(),
	WaypointCircuit_t445075330::get_offset_of_p3n_12(),
	WaypointCircuit_t445075330::get_offset_of_i_13(),
	WaypointCircuit_t445075330::get_offset_of_P0_14(),
	WaypointCircuit_t445075330::get_offset_of_P1_15(),
	WaypointCircuit_t445075330::get_offset_of_P2_16(),
	WaypointCircuit_t445075330::get_offset_of_P3_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (WaypointList_t2584574554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2271[2] = 
{
	WaypointList_t2584574554::get_offset_of_circuit_0(),
	WaypointList_t2584574554::get_offset_of_items_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (RoutePoint_t3880028948)+ sizeof (RuntimeObject), sizeof(RoutePoint_t3880028948 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2272[2] = 
{
	RoutePoint_t3880028948::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoutePoint_t3880028948::get_offset_of_direction_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (WaypointProgressTracker_t1841386251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2273[15] = 
{
	WaypointProgressTracker_t1841386251::get_offset_of_circuit_2(),
	WaypointProgressTracker_t1841386251::get_offset_of_lookAheadForTargetOffset_3(),
	WaypointProgressTracker_t1841386251::get_offset_of_lookAheadForTargetFactor_4(),
	WaypointProgressTracker_t1841386251::get_offset_of_lookAheadForSpeedOffset_5(),
	WaypointProgressTracker_t1841386251::get_offset_of_lookAheadForSpeedFactor_6(),
	WaypointProgressTracker_t1841386251::get_offset_of_progressStyle_7(),
	WaypointProgressTracker_t1841386251::get_offset_of_pointToPointThreshold_8(),
	WaypointProgressTracker_t1841386251::get_offset_of_U3CtargetPointU3Ek__BackingField_9(),
	WaypointProgressTracker_t1841386251::get_offset_of_U3CspeedPointU3Ek__BackingField_10(),
	WaypointProgressTracker_t1841386251::get_offset_of_U3CprogressPointU3Ek__BackingField_11(),
	WaypointProgressTracker_t1841386251::get_offset_of_target_12(),
	WaypointProgressTracker_t1841386251::get_offset_of_progressDistance_13(),
	WaypointProgressTracker_t1841386251::get_offset_of_progressNum_14(),
	WaypointProgressTracker_t1841386251::get_offset_of_lastPosition_15(),
	WaypointProgressTracker_t1841386251::get_offset_of_speed_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (ProgressStyle_t3254572979)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2274[3] = 
{
	ProgressStyle_t3254572979::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (U3CModuleU3E_t692745554), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (AllRef_t460770394), -1, sizeof(AllRef_t460770394_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2276[6] = 
{
	AllRef_t460770394::get_offset_of_inAppPackages_2(),
	AllRef_t460770394::get_offset_of_inAPP_KEY_3(),
	AllRef_t460770394_StaticFields::get_offset_of_finishAnimationNumber_4(),
	AllRef_t460770394_StaticFields::get_offset_of__instance_5(),
	AllRef_t460770394::get_offset_of_FxPuzzleComplete_6(),
	AllRef_t460770394::get_offset_of_FXCoin_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (U3CEWaitForAnimationToCompleteU3Ec__Iterator0_t186264544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2277[4] = 
{
	U3CEWaitForAnimationToCompleteU3Ec__Iterator0_t186264544::get_offset_of_anim_0(),
	U3CEWaitForAnimationToCompleteU3Ec__Iterator0_t186264544::get_offset_of_U24current_1(),
	U3CEWaitForAnimationToCompleteU3Ec__Iterator0_t186264544::get_offset_of_U24disposing_2(),
	U3CEWaitForAnimationToCompleteU3Ec__Iterator0_t186264544::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (AnimationEventTriggerHandler_t349007468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2278[2] = 
{
	AnimationEventTriggerHandler_t349007468::get_offset_of_mainmenumanger_2(),
	AnimationEventTriggerHandler_t349007468::get_offset_of_planevisualizationmanager_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (AnimationsEvents_t1170748522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2279[3] = 
{
	AnimationsEvents_t1170748522::get_offset_of_StateName_2(),
	AnimationsEvents_t1170748522::get_offset_of_timeBeforeStateComplete_3(),
	AnimationsEvents_t1170748522::get_offset_of_isFadeIn_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (CharacterScript_t4133963155), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2280[6] = 
{
	CharacterScript_t4133963155::get_offset_of_cardPositions_2(),
	CharacterScript_t4133963155::get_offset_of_characterMaterial_3(),
	CharacterScript_t4133963155::get_offset_of_LollipopObj_4(),
	CharacterScript_t4133963155::get_offset_of_Shadow_5(),
	CharacterScript_t4133963155::get_offset_of_Swords_6(),
	CharacterScript_t4133963155::get_offset_of_CharacterGlasses_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (GameManager_t1536523654), -1, sizeof(GameManager_t1536523654_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2281[3] = 
{
	GameManager_t1536523654_StaticFields::get_offset_of_instance_0(),
	GameManager_t1536523654::get_offset_of_selectedCharacterNumber_1(),
	GameManager_t1536523654::get_offset_of_isFromGamePlayToMainMenu_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (MainMenuManager_t3185368703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2282[15] = 
{
	MainMenuManager_t3185368703::get_offset_of_startButton_2(),
	MainMenuManager_t3185368703::get_offset_of_startBtnText_3(),
	MainMenuManager_t3185368703::get_offset_of_mainMenuPanel_4(),
	MainMenuManager_t3185368703::get_offset_of_selectionPanel_5(),
	MainMenuManager_t3185368703::get_offset_of_LoadingObj_6(),
	MainMenuManager_t3185368703::get_offset_of_LoadingPanel_7(),
	MainMenuManager_t3185368703::get_offset_of_LogoBtn_8(),
	MainMenuManager_t3185368703::get_offset_of_CharacterNameText_9(),
	MainMenuManager_t3185368703::get_offset_of_characternames_10(),
	MainMenuManager_t3185368703::get_offset_of_CharacterMaterials_11(),
	MainMenuManager_t3185368703::get_offset_of_characters_12(),
	MainMenuManager_t3185368703::get_offset_of_cameraOffset_13(),
	MainMenuManager_t3185368703::get_offset_of_scrollTime_14(),
	MainMenuManager_t3185368703::get_offset_of_distanceBtwCharacters_15(),
	MainMenuManager_t3185368703::get_offset_of_activePos_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (U3COnStartBtnClickEU3Ec__Iterator0_t2041946857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2283[4] = 
{
	U3COnStartBtnClickEU3Ec__Iterator0_t2041946857::get_offset_of_U24this_0(),
	U3COnStartBtnClickEU3Ec__Iterator0_t2041946857::get_offset_of_U24current_1(),
	U3COnStartBtnClickEU3Ec__Iterator0_t2041946857::get_offset_of_U24disposing_2(),
	U3COnStartBtnClickEU3Ec__Iterator0_t2041946857::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (U3COnSelectBtnClickEU3Ec__Iterator1_t3254601737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2284[3] = 
{
	U3COnSelectBtnClickEU3Ec__Iterator1_t3254601737::get_offset_of_U24current_0(),
	U3COnSelectBtnClickEU3Ec__Iterator1_t3254601737::get_offset_of_U24disposing_1(),
	U3COnSelectBtnClickEU3Ec__Iterator1_t3254601737::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (MyItweenTest_t2779311591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2285[1] = 
{
	MyItweenTest_t2779311591::get_offset_of_TargetPos_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (PlaneVisualizationManager_t1447224982), -1, sizeof(PlaneVisualizationManager_t1447224982_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2286[61] = 
{
	PlaneVisualizationManager_t1447224982::get_offset_of_scaleText_2(),
	PlaneVisualizationManager_t1447224982::get_offset_of_scaleText2_3(),
	PlaneVisualizationManager_t1447224982::get_offset_of_slider_4(),
	PlaneVisualizationManager_t1447224982::get_offset_of_TrackedPlanePrefab_5(),
	PlaneVisualizationManager_t1447224982::get_offset_of_m_firstPersonCamera_6(),
	PlaneVisualizationManager_t1447224982::get_offset_of__newPlanes_7(),
	PlaneVisualizationManager_t1447224982::get_offset_of_PlaneObject_8(),
	PlaneVisualizationManager_t1447224982::get_offset_of_isStopInitialInstruction_9(),
	PlaneVisualizationManager_t1447224982::get_offset_of_Characters_10(),
	PlaneVisualizationManager_t1447224982::get_offset_of_cardImages_11(),
	PlaneVisualizationManager_t1447224982::get_offset_of_cardPrefabs_12(),
	PlaneVisualizationManager_t1447224982::get_offset_of_CharacterMaterials_13(),
	PlaneVisualizationManager_t1447224982::get_offset_of_SwordMaterials_14(),
	PlaneVisualizationManager_t1447224982::get_offset_of_cardNames_15(),
	PlaneVisualizationManager_t1447224982::get_offset_of_cardPrefabToInstantiate_16(),
	PlaneVisualizationManager_t1447224982::get_offset_of_cardShuffleAnim_17(),
	PlaneVisualizationManager_t1447224982::get_offset_of_instructionText_18(),
	PlaneVisualizationManager_t1447224982::get_offset_of_CardDescriptionPopUpObj_19(),
	PlaneVisualizationManager_t1447224982::get_offset_of_CardDeckObj_20(),
	PlaneVisualizationManager_t1447224982::get_offset_of_Instruction1Obj_21(),
	PlaneVisualizationManager_t1447224982::get_offset_of_CharacterToInstantiate_22(),
	PlaneVisualizationManager_t1447224982::get_offset_of_anim_23(),
	PlaneVisualizationManager_t1447224982::get_offset_of_currentCard_24(),
	PlaneVisualizationManager_t1447224982::get_offset_of_currentCardName_25(),
	PlaneVisualizationManager_t1447224982::get_offset_of_cardPositions_26(),
	PlaneVisualizationManager_t1447224982::get_offset_of_lerpSpeed_27(),
	PlaneVisualizationManager_t1447224982::get_offset_of_curveFadeIn_28(),
	PlaneVisualizationManager_t1447224982::get_offset_of_curveFadeOut_29(),
	PlaneVisualizationManager_t1447224982::get_offset_of_myCurrentCharacter_30(),
	PlaneVisualizationManager_t1447224982::get_offset_of_myCurrentCharacterMaterial_31(),
	PlaneVisualizationManager_t1447224982::get_offset_of_isAlreadyCreated_32(),
	PlaneVisualizationManager_t1447224982::get_offset_of_stopPlaneDetection_33(),
	PlaneVisualizationManager_t1447224982::get_offset_of_isSwipeAllowed_34(),
	PlaneVisualizationManager_t1447224982::get_offset_of_isTapAllowed_35(),
	PlaneVisualizationManager_t1447224982::get_offset_of_randomNumber_36(),
	PlaneVisualizationManager_t1447224982::get_offset_of_previousrandomNumber_37(),
	PlaneVisualizationManager_t1447224982::get_offset_of_isFadeIn_38(),
	PlaneVisualizationManager_t1447224982::get_offset_of_isFadeOut_39(),
	PlaneVisualizationManager_t1447224982::get_offset_of_currentCurveValue_40(),
	PlaneVisualizationManager_t1447224982::get_offset_of_rotationAngle_41(),
	PlaneVisualizationManager_t1447224982_StaticFields::get_offset_of_Instance_42(),
	PlaneVisualizationManager_t1447224982::get_offset_of_pinchRecognizer_43(),
	PlaneVisualizationManager_t1447224982::get_offset_of_swipeRecognizer_44(),
	PlaneVisualizationManager_t1447224982::get_offset_of_tapRecognizer_45(),
	PlaneVisualizationManager_t1447224982::get_offset_of_longPressRecognizer_46(),
	PlaneVisualizationManager_t1447224982::get_offset_of_unityARHitTestExample_47(),
	PlaneVisualizationManager_t1447224982::get_offset_of_toSwitchOff_48(),
	PlaneVisualizationManager_t1447224982::get_offset_of__PointCloudParticleExample_49(),
	PlaneVisualizationManager_t1447224982::get_offset_of__UnityARGeneratePlane_50(),
	PlaneVisualizationManager_t1447224982::get_offset_of__UnityPointCloudExample_51(),
	PlaneVisualizationManager_t1447224982::get_offset_of__UnityARKitControl_52(),
	PlaneVisualizationManager_t1447224982::get_offset_of_CharacterPosIOS_53(),
	PlaneVisualizationManager_t1447224982::get_offset_of_CharacterRotIOS_54(),
	PlaneVisualizationManager_t1447224982::get_offset_of_m_IsQuitting_55(),
	PlaneVisualizationManager_t1447224982::get_offset_of_onScreen_56(),
	PlaneVisualizationManager_t1447224982::get_offset_of_isPinching_57(),
	PlaneVisualizationManager_t1447224982::get_offset_of_isResetPinching_58(),
	PlaneVisualizationManager_t1447224982::get_offset_of_isLongTouchDetectedAlready_59(),
	PlaneVisualizationManager_t1447224982::get_offset_of_isRecieveAnimationCallback_60(),
	PlaneVisualizationManager_t1447224982::get_offset_of_previousCardGame_61(),
	PlaneVisualizationManager_t1447224982::get_offset_of_LoadingPanel_62(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (U3CInitialInstructionEU3Ec__Iterator0_t2270754370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2287[5] = 
{
	U3CInitialInstructionEU3Ec__Iterator0_t2270754370::get_offset_of_U3CtempU3E__1_0(),
	U3CInitialInstructionEU3Ec__Iterator0_t2270754370::get_offset_of_U24this_1(),
	U3CInitialInstructionEU3Ec__Iterator0_t2270754370::get_offset_of_U24current_2(),
	U3CInitialInstructionEU3Ec__Iterator0_t2270754370::get_offset_of_U24disposing_3(),
	U3CInitialInstructionEU3Ec__Iterator0_t2270754370::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (U3C_ShowAndroidToastMessageU3Ec__AnonStoreyA_t3559623145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2288[2] = 
{
	U3C_ShowAndroidToastMessageU3Ec__AnonStoreyA_t3559623145::get_offset_of_unityActivity_0(),
	U3C_ShowAndroidToastMessageU3Ec__AnonStoreyA_t3559623145::get_offset_of_message_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (U3C_ShowAndroidToastMessageU3Ec__AnonStorey9_t3555953129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2289[2] = 
{
	U3C_ShowAndroidToastMessageU3Ec__AnonStorey9_t3555953129::get_offset_of_toastClass_0(),
	U3C_ShowAndroidToastMessageU3Ec__AnonStorey9_t3555953129::get_offset_of_U3CU3Ef__refU2410_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (U3COnCardBtnClickEU3Ec__Iterator1_t1416119937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2290[9] = 
{
	U3COnCardBtnClickEU3Ec__Iterator1_t1416119937::get_offset_of_U3Cm_ScreenPositionU3E__0_0(),
	U3COnCardBtnClickEU3Ec__Iterator1_t1416119937::get_offset_of_U3CrayU3E__0_1(),
	U3COnCardBtnClickEU3Ec__Iterator1_t1416119937::get_offset_of_U3CCardU3E__0_2(),
	U3COnCardBtnClickEU3Ec__Iterator1_t1416119937::get_offset_of_U3CsmallestDistanceU3E__0_3(),
	U3COnCardBtnClickEU3Ec__Iterator1_t1416119937::get_offset_of_U3CnearestCardPosU3E__0_4(),
	U3COnCardBtnClickEU3Ec__Iterator1_t1416119937::get_offset_of_U24this_5(),
	U3COnCardBtnClickEU3Ec__Iterator1_t1416119937::get_offset_of_U24current_6(),
	U3COnCardBtnClickEU3Ec__Iterator1_t1416119937::get_offset_of_U24disposing_7(),
	U3COnCardBtnClickEU3Ec__Iterator1_t1416119937::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (U3CSetMyModelScaleU3Ec__Iterator2_t2747975946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2291[4] = 
{
	U3CSetMyModelScaleU3Ec__Iterator2_t2747975946::get_offset_of_U24this_0(),
	U3CSetMyModelScaleU3Ec__Iterator2_t2747975946::get_offset_of_U24current_1(),
	U3CSetMyModelScaleU3Ec__Iterator2_t2747975946::get_offset_of_U24disposing_2(),
	U3CSetMyModelScaleU3Ec__Iterator2_t2747975946::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (U3COnCharacterAnimCompleteEU3Ec__Iterator3_t1665021887), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2292[4] = 
{
	U3COnCharacterAnimCompleteEU3Ec__Iterator3_t1665021887::get_offset_of_U24this_0(),
	U3COnCharacterAnimCompleteEU3Ec__Iterator3_t1665021887::get_offset_of_U24current_1(),
	U3COnCharacterAnimCompleteEU3Ec__Iterator3_t1665021887::get_offset_of_U24disposing_2(),
	U3COnCharacterAnimCompleteEU3Ec__Iterator3_t1665021887::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (U3CPlayCharacterAnimU3Ec__Iterator4_t447388885), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2293[4] = 
{
	U3CPlayCharacterAnimU3Ec__Iterator4_t447388885::get_offset_of_U24this_0(),
	U3CPlayCharacterAnimU3Ec__Iterator4_t447388885::get_offset_of_U24current_1(),
	U3CPlayCharacterAnimU3Ec__Iterator4_t447388885::get_offset_of_U24disposing_2(),
	U3CPlayCharacterAnimU3Ec__Iterator4_t447388885::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (U3CResetCharacterAfterAnimEU3Ec__Iterator5_t1958411711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2294[6] = 
{
	U3CResetCharacterAfterAnimEU3Ec__Iterator5_t1958411711::get_offset_of_U24locvar0_0(),
	U3CResetCharacterAfterAnimEU3Ec__Iterator5_t1958411711::get_offset_of_U24locvar1_1(),
	U3CResetCharacterAfterAnimEU3Ec__Iterator5_t1958411711::get_offset_of_U24this_2(),
	U3CResetCharacterAfterAnimEU3Ec__Iterator5_t1958411711::get_offset_of_U24current_3(),
	U3CResetCharacterAfterAnimEU3Ec__Iterator5_t1958411711::get_offset_of_U24disposing_4(),
	U3CResetCharacterAfterAnimEU3Ec__Iterator5_t1958411711::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (U3CVolumeFadeOutEU3Ec__Iterator6_t1858533124), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2295[5] = 
{
	U3CVolumeFadeOutEU3Ec__Iterator6_t1858533124::get_offset_of_audio_0(),
	U3CVolumeFadeOutEU3Ec__Iterator6_t1858533124::get_offset_of_U3CaudioVolumeU3E__0_1(),
	U3CVolumeFadeOutEU3Ec__Iterator6_t1858533124::get_offset_of_U24current_2(),
	U3CVolumeFadeOutEU3Ec__Iterator6_t1858533124::get_offset_of_U24disposing_3(),
	U3CVolumeFadeOutEU3Ec__Iterator6_t1858533124::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (U3CLollipopFallEU3Ec__Iterator7_t3577267671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2296[4] = 
{
	U3CLollipopFallEU3Ec__Iterator7_t3577267671::get_offset_of_U24this_0(),
	U3CLollipopFallEU3Ec__Iterator7_t3577267671::get_offset_of_U24current_1(),
	U3CLollipopFallEU3Ec__Iterator7_t3577267671::get_offset_of_U24disposing_2(),
	U3CLollipopFallEU3Ec__Iterator7_t3577267671::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (U3CSwordsHideEU3Ec__Iterator8_t3420496710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2297[6] = 
{
	U3CSwordsHideEU3Ec__Iterator8_t3420496710::get_offset_of_U24locvar0_0(),
	U3CSwordsHideEU3Ec__Iterator8_t3420496710::get_offset_of_U24locvar1_1(),
	U3CSwordsHideEU3Ec__Iterator8_t3420496710::get_offset_of_U24this_2(),
	U3CSwordsHideEU3Ec__Iterator8_t3420496710::get_offset_of_U24current_3(),
	U3CSwordsHideEU3Ec__Iterator8_t3420496710::get_offset_of_U24disposing_4(),
	U3CSwordsHideEU3Ec__Iterator8_t3420496710::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (Preferences_t313491356), -1, sizeof(Preferences_t313491356_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2298[7] = 
{
	Preferences_t313491356_StaticFields::get_offset_of__instance_0(),
	Preferences_t313491356::get_offset_of__level_1(),
	Preferences_t313491356::get_offset_of__coins_2(),
	Preferences_t313491356::get_offset_of__rateUs_3(),
	Preferences_t313491356::get_offset_of__ads_4(),
	Preferences_t313491356::get_offset_of__isRate_5(),
	Preferences_t313491356::get_offset_of__score_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (SoundManager_t2102329059), -1, sizeof(SoundManager_t2102329059_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2299[6] = 
{
	SoundManager_t2102329059_StaticFields::get_offset_of_Instance_2(),
	SoundManager_t2102329059::get_offset_of_Chocking_3(),
	SoundManager_t2102329059::get_offset_of_DeathBiteAndChew_4(),
	SoundManager_t2102329059::get_offset_of_DanceTheWorld_5(),
	SoundManager_t2102329059::get_offset_of_TwoOfSwords_6(),
	SoundManager_t2102329059::get_offset_of_Shuffling_7(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
