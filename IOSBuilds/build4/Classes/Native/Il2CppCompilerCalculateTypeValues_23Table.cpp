﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t1314943911;
// GoogleARCoreInternal.NativeSession
struct NativeSession_t1550589577;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// GoogleARCoreInternal.InstantPreviewManager/Result
struct Result_t3334322572;
// System.Threading.Thread
struct Thread_t2300836069;
// GoogleARCoreInternal.InstantPreviewManager/<InstallApkAndRunIfConnected>c__Iterator2/<InstallApkAndRunIfConnected>c__AnonStorey3
struct U3CInstallApkAndRunIfConnectedU3Ec__AnonStorey3_t1278641102;
// UnityEngine.Touch[]
struct TouchU5BU5D_t1849554061;
// System.Collections.Generic.List`1<UnityEngine.Touch>
struct List_1_t3393931610;
// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_t32045322;
// GoogleARCore.Examples.HelloAR.HelloARController/<_ShowAndroidToastMessage>c__AnonStorey1
struct U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t1208848115;
// GoogleARCore.Examples.CloudAnchor.CloudAnchorController/<_ShowAndroidToastMessage>c__AnonStorey1
struct U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t908968643;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t4131667876;
// GoogleARCore.Examples.ComputerVision.ComputerVisionController/<_ShowAndroidToastMessage>c__AnonStorey1
struct U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t3919803072;
// System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult>
struct List_1_t2751098672;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Void
struct Void_t1185182177;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Boolean[]
struct BooleanU5BU5D_t2897418192;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.GlobalJavaObjectRef
struct GlobalJavaObjectRef_t3225273728;
// GoogleARCoreInternal.TrackableManager
struct TrackableManager_t1722633017;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>
struct Dictionary_2_t1172811472;
// GoogleARCoreInternal.AnchorApi
struct AnchorApi_t2165463;
// GoogleARCoreInternal.AugmentedImageApi
struct AugmentedImageApi_t1163146062;
// GoogleARCoreInternal.AugmentedImageDatabaseApi
struct AugmentedImageDatabaseApi_t4243295728;
// GoogleARCoreInternal.CameraApi
struct CameraApi_t274148848;
// GoogleARCoreInternal.CameraMetadataApi
struct CameraMetadataApi_t4067407319;
// GoogleARCoreInternal.FrameApi
struct FrameApi_t2778645972;
// GoogleARCoreInternal.HitTestApi
struct HitTestApi_t363854212;
// GoogleARCoreInternal.ImageApi
struct ImageApi_t1356767549;
// GoogleARCoreInternal.LightEstimateApi
struct LightEstimateApi_t745071092;
// GoogleARCoreInternal.PlaneApi
struct PlaneApi_t1746941456;
// GoogleARCoreInternal.PointApi
struct PointApi_t3912385759;
// GoogleARCoreInternal.PointCloudApi
struct PointCloudApi_t650451515;
// GoogleARCoreInternal.PoseApi
struct PoseApi_t3025584507;
// GoogleARCoreInternal.SessionApi
struct SessionApi_t3641277092;
// GoogleARCoreInternal.SessionConfigApi
struct SessionConfigApi_t3119440216;
// GoogleARCoreInternal.TrackableApi
struct TrackableApi_t1100424269;
// GoogleARCoreInternal.TrackableListApi
struct TrackableListApi_t1931239134;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// UnityEngine.Texture
struct Texture_t3661962703;
// System.Type
struct Type_t;
// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkClient>
struct List_1_t935303414;
// UnityEngine.Networking.HostTopology
struct HostTopology_t4059263395;
// System.Net.EndPoint
struct EndPoint_t982345378;
// UnityEngine.Networking.NetworkSystem.CRCMessage
struct CRCMessage_t4148217304;
// UnityEngine.Networking.NetworkMessageHandlers
struct NetworkMessageHandlers_t82575973;
// UnityEngine.Networking.NetworkConnection
struct NetworkConnection_t2705220091;
// UnityEngine.Networking.NetworkReader
struct NetworkReader_t1574750186;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.Networking.NetworkMessageDelegate
struct NetworkMessageDelegate_t360140524;
// GoogleARCore.AsyncTask`1<GoogleARCore.AndroidPermissionsRequestResult>
struct AsyncTask_1_t3089477160;
// System.Action`1<GoogleARCore.AndroidPermissionsRequestResult>
struct Action_1_t4082770468;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// GoogleARCore.Examples.CloudAnchor.RoomSharingClient/GetAnchorIdFromRoomDelegate
struct GetAnchorIdFromRoomDelegate_t3759139021;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// GoogleARCore.Examples.ComputerVision.TextureReader
struct TextureReader_t2342948801;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// GoogleARCore.DetectedPlane
struct DetectedPlane_t4062129378;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t4027761066;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Collections.Generic.List`1<GoogleARCore.DetectedPlane>
struct List_1_t1239236824;
// System.Collections.Generic.Dictionary`2<System.Int32,GoogleARCore.CrossPlatform.XPAnchor>
struct Dictionary_2_t3995852529;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.UI.Toggle
struct Toggle_t2735377061;
// System.Collections.Generic.Dictionary`2<System.IntPtr,GoogleARCore.Anchor>
struct Dictionary_2_t3286351191;
// GoogleARCore.Examples.CloudAnchor.RoomSharingServer
struct RoomSharingServer_t2010038578;
// GoogleARCore.Examples.CloudAnchor.CloudAnchorUIController
struct CloudAnchorUIController_t97785020;
// GoogleARCore.Examples.CloudAnchor.ARKitHelper
struct ARKitHelper_t1470960758;
// UnityEngine.Component
struct Component_t1923634451;
// GoogleARCore.CrossPlatform.XPAnchor
struct XPAnchor_t812171902;
// GoogleARCore.AugmentedImage
struct AugmentedImage_t279011686;
// GoogleARCore.Examples.ComputerVision.TextureReaderApi
struct TextureReaderApi_t3129393145;
// GoogleARCore.Examples.ComputerVision.TextureReader/OnImageAvailableCallbackFunc
struct OnImageAvailableCallbackFunc_t2664114036;
// GoogleARCore.Examples.AugmentedImage.AugmentedImageVisualizer
struct AugmentedImageVisualizer_t1303630779;
// System.Collections.Generic.Dictionary`2<System.Int32,GoogleARCore.Examples.AugmentedImage.AugmentedImageVisualizer>
struct Dictionary_2_t192344110;
// System.Collections.Generic.List`1<GoogleARCore.AugmentedImage>
struct List_1_t1751086428;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef APIDISPLAYUVCOORDSEXTENSIONS_T1347642370_H
#define APIDISPLAYUVCOORDSEXTENSIONS_T1347642370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ApiDisplayUvCoordsExtensions
struct  ApiDisplayUvCoordsExtensions_t1347642370  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIDISPLAYUVCOORDSEXTENSIONS_T1347642370_H
#ifndef MESSAGEBASE_T3584795631_H
#define MESSAGEBASE_T3584795631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.MessageBase
struct  MessageBase_t3584795631  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEBASE_T3584795631_H
#ifndef INSTANTPREVIEWMANAGER_T448487770_H
#define INSTANTPREVIEWMANAGER_T448487770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.InstantPreviewManager
struct  InstantPreviewManager_t448487770  : public RuntimeObject
{
public:

public:
};

struct InstantPreviewManager_t448487770_StaticFields
{
public:
	// UnityEngine.WaitForEndOfFrame GoogleARCoreInternal.InstantPreviewManager::k_WaitForEndOfFrame
	WaitForEndOfFrame_t1314943911 * ___k_WaitForEndOfFrame_5;
	// System.Boolean GoogleARCoreInternal.InstantPreviewManager::s_PauseWarned
	bool ___s_PauseWarned_6;
	// System.Boolean GoogleARCoreInternal.InstantPreviewManager::s_DisabledLightEstimationWarned
	bool ___s_DisabledLightEstimationWarned_7;
	// System.Boolean GoogleARCoreInternal.InstantPreviewManager::s_DisabledPlaneFindingWarned
	bool ___s_DisabledPlaneFindingWarned_8;

public:
	inline static int32_t get_offset_of_k_WaitForEndOfFrame_5() { return static_cast<int32_t>(offsetof(InstantPreviewManager_t448487770_StaticFields, ___k_WaitForEndOfFrame_5)); }
	inline WaitForEndOfFrame_t1314943911 * get_k_WaitForEndOfFrame_5() const { return ___k_WaitForEndOfFrame_5; }
	inline WaitForEndOfFrame_t1314943911 ** get_address_of_k_WaitForEndOfFrame_5() { return &___k_WaitForEndOfFrame_5; }
	inline void set_k_WaitForEndOfFrame_5(WaitForEndOfFrame_t1314943911 * value)
	{
		___k_WaitForEndOfFrame_5 = value;
		Il2CppCodeGenWriteBarrier((&___k_WaitForEndOfFrame_5), value);
	}

	inline static int32_t get_offset_of_s_PauseWarned_6() { return static_cast<int32_t>(offsetof(InstantPreviewManager_t448487770_StaticFields, ___s_PauseWarned_6)); }
	inline bool get_s_PauseWarned_6() const { return ___s_PauseWarned_6; }
	inline bool* get_address_of_s_PauseWarned_6() { return &___s_PauseWarned_6; }
	inline void set_s_PauseWarned_6(bool value)
	{
		___s_PauseWarned_6 = value;
	}

	inline static int32_t get_offset_of_s_DisabledLightEstimationWarned_7() { return static_cast<int32_t>(offsetof(InstantPreviewManager_t448487770_StaticFields, ___s_DisabledLightEstimationWarned_7)); }
	inline bool get_s_DisabledLightEstimationWarned_7() const { return ___s_DisabledLightEstimationWarned_7; }
	inline bool* get_address_of_s_DisabledLightEstimationWarned_7() { return &___s_DisabledLightEstimationWarned_7; }
	inline void set_s_DisabledLightEstimationWarned_7(bool value)
	{
		___s_DisabledLightEstimationWarned_7 = value;
	}

	inline static int32_t get_offset_of_s_DisabledPlaneFindingWarned_8() { return static_cast<int32_t>(offsetof(InstantPreviewManager_t448487770_StaticFields, ___s_DisabledPlaneFindingWarned_8)); }
	inline bool get_s_DisabledPlaneFindingWarned_8() const { return ___s_DisabledPlaneFindingWarned_8; }
	inline bool* get_address_of_s_DisabledPlaneFindingWarned_8() { return &___s_DisabledPlaneFindingWarned_8; }
	inline void set_s_DisabledPlaneFindingWarned_8(bool value)
	{
		___s_DisabledPlaneFindingWarned_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANTPREVIEWMANAGER_T448487770_H
#ifndef RESULT_T3334322572_H
#define RESULT_T3334322572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.InstantPreviewManager/Result
struct  Result_t3334322572  : public RuntimeObject
{
public:
	// System.Boolean GoogleARCoreInternal.InstantPreviewManager/Result::ShouldPromptForInstall
	bool ___ShouldPromptForInstall_0;

public:
	inline static int32_t get_offset_of_ShouldPromptForInstall_0() { return static_cast<int32_t>(offsetof(Result_t3334322572, ___ShouldPromptForInstall_0)); }
	inline bool get_ShouldPromptForInstall_0() const { return ___ShouldPromptForInstall_0; }
	inline bool* get_address_of_ShouldPromptForInstall_0() { return &___ShouldPromptForInstall_0; }
	inline void set_ShouldPromptForInstall_0(bool value)
	{
		___ShouldPromptForInstall_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULT_T3334322572_H
#ifndef APITRACKINGSTATEEXTENSIONS_T806433345_H
#define APITRACKINGSTATEEXTENSIONS_T806433345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ApiTrackingStateExtensions
struct  ApiTrackingStateExtensions_t806433345  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APITRACKINGSTATEEXTENSIONS_T806433345_H
#ifndef POINTCLOUDAPI_T650451515_H
#define POINTCLOUDAPI_T650451515_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.PointCloudApi
struct  PointCloudApi_t650451515  : public RuntimeObject
{
public:
	// GoogleARCoreInternal.NativeSession GoogleARCoreInternal.PointCloudApi::m_NativeSession
	NativeSession_t1550589577 * ___m_NativeSession_0;
	// System.Single[] GoogleARCoreInternal.PointCloudApi::m_CachedVector
	SingleU5BU5D_t1444911251* ___m_CachedVector_1;

public:
	inline static int32_t get_offset_of_m_NativeSession_0() { return static_cast<int32_t>(offsetof(PointCloudApi_t650451515, ___m_NativeSession_0)); }
	inline NativeSession_t1550589577 * get_m_NativeSession_0() const { return ___m_NativeSession_0; }
	inline NativeSession_t1550589577 ** get_address_of_m_NativeSession_0() { return &___m_NativeSession_0; }
	inline void set_m_NativeSession_0(NativeSession_t1550589577 * value)
	{
		___m_NativeSession_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_NativeSession_0), value);
	}

	inline static int32_t get_offset_of_m_CachedVector_1() { return static_cast<int32_t>(offsetof(PointCloudApi_t650451515, ___m_CachedVector_1)); }
	inline SingleU5BU5D_t1444911251* get_m_CachedVector_1() const { return ___m_CachedVector_1; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_CachedVector_1() { return &___m_CachedVector_1; }
	inline void set_m_CachedVector_1(SingleU5BU5D_t1444911251* value)
	{
		___m_CachedVector_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachedVector_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTCLOUDAPI_T650451515_H
#ifndef U3CINSTALLAPKANDRUNIFCONNECTEDU3EC__ITERATOR2_T1973695815_H
#define U3CINSTALLAPKANDRUNIFCONNECTEDU3EC__ITERATOR2_T1973695815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.InstantPreviewManager/<InstallApkAndRunIfConnected>c__Iterator2
struct  U3CInstallApkAndRunIfConnectedU3Ec__Iterator2_t1973695815  : public RuntimeObject
{
public:
	// GoogleARCoreInternal.InstantPreviewManager/Result GoogleARCoreInternal.InstantPreviewManager/<InstallApkAndRunIfConnected>c__Iterator2::<result>__0
	Result_t3334322572 * ___U3CresultU3E__0_0;
	// System.String GoogleARCoreInternal.InstantPreviewManager/<InstallApkAndRunIfConnected>c__Iterator2::adbPath
	String_t* ___adbPath_1;
	// System.String GoogleARCoreInternal.InstantPreviewManager/<InstallApkAndRunIfConnected>c__Iterator2::localVersion
	String_t* ___localVersion_2;
	// System.Threading.Thread GoogleARCoreInternal.InstantPreviewManager/<InstallApkAndRunIfConnected>c__Iterator2::<checkAdbThread>__0
	Thread_t2300836069 * ___U3CcheckAdbThreadU3E__0_3;
	// System.Threading.Thread GoogleARCoreInternal.InstantPreviewManager/<InstallApkAndRunIfConnected>c__Iterator2::<installThread>__1
	Thread_t2300836069 * ___U3CinstallThreadU3E__1_4;
	// System.Object GoogleARCoreInternal.InstantPreviewManager/<InstallApkAndRunIfConnected>c__Iterator2::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean GoogleARCoreInternal.InstantPreviewManager/<InstallApkAndRunIfConnected>c__Iterator2::$disposing
	bool ___U24disposing_6;
	// System.Int32 GoogleARCoreInternal.InstantPreviewManager/<InstallApkAndRunIfConnected>c__Iterator2::$PC
	int32_t ___U24PC_7;
	// GoogleARCoreInternal.InstantPreviewManager/<InstallApkAndRunIfConnected>c__Iterator2/<InstallApkAndRunIfConnected>c__AnonStorey3 GoogleARCoreInternal.InstantPreviewManager/<InstallApkAndRunIfConnected>c__Iterator2::$locvar0
	U3CInstallApkAndRunIfConnectedU3Ec__AnonStorey3_t1278641102 * ___U24locvar0_8;

public:
	inline static int32_t get_offset_of_U3CresultU3E__0_0() { return static_cast<int32_t>(offsetof(U3CInstallApkAndRunIfConnectedU3Ec__Iterator2_t1973695815, ___U3CresultU3E__0_0)); }
	inline Result_t3334322572 * get_U3CresultU3E__0_0() const { return ___U3CresultU3E__0_0; }
	inline Result_t3334322572 ** get_address_of_U3CresultU3E__0_0() { return &___U3CresultU3E__0_0; }
	inline void set_U3CresultU3E__0_0(Result_t3334322572 * value)
	{
		___U3CresultU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresultU3E__0_0), value);
	}

	inline static int32_t get_offset_of_adbPath_1() { return static_cast<int32_t>(offsetof(U3CInstallApkAndRunIfConnectedU3Ec__Iterator2_t1973695815, ___adbPath_1)); }
	inline String_t* get_adbPath_1() const { return ___adbPath_1; }
	inline String_t** get_address_of_adbPath_1() { return &___adbPath_1; }
	inline void set_adbPath_1(String_t* value)
	{
		___adbPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___adbPath_1), value);
	}

	inline static int32_t get_offset_of_localVersion_2() { return static_cast<int32_t>(offsetof(U3CInstallApkAndRunIfConnectedU3Ec__Iterator2_t1973695815, ___localVersion_2)); }
	inline String_t* get_localVersion_2() const { return ___localVersion_2; }
	inline String_t** get_address_of_localVersion_2() { return &___localVersion_2; }
	inline void set_localVersion_2(String_t* value)
	{
		___localVersion_2 = value;
		Il2CppCodeGenWriteBarrier((&___localVersion_2), value);
	}

	inline static int32_t get_offset_of_U3CcheckAdbThreadU3E__0_3() { return static_cast<int32_t>(offsetof(U3CInstallApkAndRunIfConnectedU3Ec__Iterator2_t1973695815, ___U3CcheckAdbThreadU3E__0_3)); }
	inline Thread_t2300836069 * get_U3CcheckAdbThreadU3E__0_3() const { return ___U3CcheckAdbThreadU3E__0_3; }
	inline Thread_t2300836069 ** get_address_of_U3CcheckAdbThreadU3E__0_3() { return &___U3CcheckAdbThreadU3E__0_3; }
	inline void set_U3CcheckAdbThreadU3E__0_3(Thread_t2300836069 * value)
	{
		___U3CcheckAdbThreadU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcheckAdbThreadU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CinstallThreadU3E__1_4() { return static_cast<int32_t>(offsetof(U3CInstallApkAndRunIfConnectedU3Ec__Iterator2_t1973695815, ___U3CinstallThreadU3E__1_4)); }
	inline Thread_t2300836069 * get_U3CinstallThreadU3E__1_4() const { return ___U3CinstallThreadU3E__1_4; }
	inline Thread_t2300836069 ** get_address_of_U3CinstallThreadU3E__1_4() { return &___U3CinstallThreadU3E__1_4; }
	inline void set_U3CinstallThreadU3E__1_4(Thread_t2300836069 * value)
	{
		___U3CinstallThreadU3E__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinstallThreadU3E__1_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CInstallApkAndRunIfConnectedU3Ec__Iterator2_t1973695815, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CInstallApkAndRunIfConnectedU3Ec__Iterator2_t1973695815, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CInstallApkAndRunIfConnectedU3Ec__Iterator2_t1973695815, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_8() { return static_cast<int32_t>(offsetof(U3CInstallApkAndRunIfConnectedU3Ec__Iterator2_t1973695815, ___U24locvar0_8)); }
	inline U3CInstallApkAndRunIfConnectedU3Ec__AnonStorey3_t1278641102 * get_U24locvar0_8() const { return ___U24locvar0_8; }
	inline U3CInstallApkAndRunIfConnectedU3Ec__AnonStorey3_t1278641102 ** get_address_of_U24locvar0_8() { return &___U24locvar0_8; }
	inline void set_U24locvar0_8(U3CInstallApkAndRunIfConnectedU3Ec__AnonStorey3_t1278641102 * value)
	{
		___U24locvar0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINSTALLAPKANDRUNIFCONNECTEDU3EC__ITERATOR2_T1973695815_H
#ifndef U3CINSTALLAPKANDRUNIFCONNECTEDU3EC__ANONSTOREY3_T1278641102_H
#define U3CINSTALLAPKANDRUNIFCONNECTEDU3EC__ANONSTOREY3_T1278641102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.InstantPreviewManager/<InstallApkAndRunIfConnected>c__Iterator2/<InstallApkAndRunIfConnected>c__AnonStorey3
struct  U3CInstallApkAndRunIfConnectedU3Ec__AnonStorey3_t1278641102  : public RuntimeObject
{
public:
	// System.String GoogleARCoreInternal.InstantPreviewManager/<InstallApkAndRunIfConnected>c__Iterator2/<InstallApkAndRunIfConnected>c__AnonStorey3::adbPath
	String_t* ___adbPath_0;
	// System.String GoogleARCoreInternal.InstantPreviewManager/<InstallApkAndRunIfConnected>c__Iterator2/<InstallApkAndRunIfConnected>c__AnonStorey3::apkPath
	String_t* ___apkPath_1;
	// System.String GoogleARCoreInternal.InstantPreviewManager/<InstallApkAndRunIfConnected>c__Iterator2/<InstallApkAndRunIfConnected>c__AnonStorey3::localVersion
	String_t* ___localVersion_2;

public:
	inline static int32_t get_offset_of_adbPath_0() { return static_cast<int32_t>(offsetof(U3CInstallApkAndRunIfConnectedU3Ec__AnonStorey3_t1278641102, ___adbPath_0)); }
	inline String_t* get_adbPath_0() const { return ___adbPath_0; }
	inline String_t** get_address_of_adbPath_0() { return &___adbPath_0; }
	inline void set_adbPath_0(String_t* value)
	{
		___adbPath_0 = value;
		Il2CppCodeGenWriteBarrier((&___adbPath_0), value);
	}

	inline static int32_t get_offset_of_apkPath_1() { return static_cast<int32_t>(offsetof(U3CInstallApkAndRunIfConnectedU3Ec__AnonStorey3_t1278641102, ___apkPath_1)); }
	inline String_t* get_apkPath_1() const { return ___apkPath_1; }
	inline String_t** get_address_of_apkPath_1() { return &___apkPath_1; }
	inline void set_apkPath_1(String_t* value)
	{
		___apkPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___apkPath_1), value);
	}

	inline static int32_t get_offset_of_localVersion_2() { return static_cast<int32_t>(offsetof(U3CInstallApkAndRunIfConnectedU3Ec__AnonStorey3_t1278641102, ___localVersion_2)); }
	inline String_t* get_localVersion_2() const { return ___localVersion_2; }
	inline String_t** get_address_of_localVersion_2() { return &___localVersion_2; }
	inline void set_localVersion_2(String_t* value)
	{
		___localVersion_2 = value;
		Il2CppCodeGenWriteBarrier((&___localVersion_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINSTALLAPKANDRUNIFCONNECTEDU3EC__ANONSTOREY3_T1278641102_H
#ifndef POINTAPI_T3912385759_H
#define POINTAPI_T3912385759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.PointApi
struct  PointApi_t3912385759  : public RuntimeObject
{
public:
	// GoogleARCoreInternal.NativeSession GoogleARCoreInternal.PointApi::m_NativeSession
	NativeSession_t1550589577 * ___m_NativeSession_0;

public:
	inline static int32_t get_offset_of_m_NativeSession_0() { return static_cast<int32_t>(offsetof(PointApi_t3912385759, ___m_NativeSession_0)); }
	inline NativeSession_t1550589577 * get_m_NativeSession_0() const { return ___m_NativeSession_0; }
	inline NativeSession_t1550589577 ** get_address_of_m_NativeSession_0() { return &___m_NativeSession_0; }
	inline void set_m_NativeSession_0(NativeSession_t1550589577 * value)
	{
		___m_NativeSession_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_NativeSession_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTAPI_T3912385759_H
#ifndef ANCHORAPI_T2165463_H
#define ANCHORAPI_T2165463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.AnchorApi
struct  AnchorApi_t2165463  : public RuntimeObject
{
public:
	// GoogleARCoreInternal.NativeSession GoogleARCoreInternal.AnchorApi::m_NativeSession
	NativeSession_t1550589577 * ___m_NativeSession_0;

public:
	inline static int32_t get_offset_of_m_NativeSession_0() { return static_cast<int32_t>(offsetof(AnchorApi_t2165463, ___m_NativeSession_0)); }
	inline NativeSession_t1550589577 * get_m_NativeSession_0() const { return ___m_NativeSession_0; }
	inline NativeSession_t1550589577 ** get_address_of_m_NativeSession_0() { return &___m_NativeSession_0; }
	inline void set_m_NativeSession_0(NativeSession_t1550589577 * value)
	{
		___m_NativeSession_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_NativeSession_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHORAPI_T2165463_H
#ifndef APIAPKINSTALLSTATUSEXTENSIONS_T1203352906_H
#define APIAPKINSTALLSTATUSEXTENSIONS_T1203352906_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ApiApkInstallStatusExtensions
struct  ApiApkInstallStatusExtensions_t1203352906  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIAPKINSTALLSTATUSEXTENSIONS_T1203352906_H
#ifndef APIARSTATUSEXTENSIONS_T2816364904_H
#define APIARSTATUSEXTENSIONS_T2816364904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.CrossPlatform.ApiArStatusExtensions
struct  ApiArStatusExtensions_t2816364904  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIARSTATUSEXTENSIONS_T2816364904_H
#ifndef INSTANTPREVIEWINPUT_T3423396443_H
#define INSTANTPREVIEWINPUT_T3423396443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.InstantPreviewInput
struct  InstantPreviewInput_t3423396443  : public RuntimeObject
{
public:

public:
};

struct InstantPreviewInput_t3423396443_StaticFields
{
public:
	// UnityEngine.Touch[] GoogleARCore.InstantPreviewInput::s_Touches
	TouchU5BU5D_t1849554061* ___s_Touches_0;
	// System.Collections.Generic.List`1<UnityEngine.Touch> GoogleARCore.InstantPreviewInput::s_TouchList
	List_1_t3393931610 * ___s_TouchList_1;

public:
	inline static int32_t get_offset_of_s_Touches_0() { return static_cast<int32_t>(offsetof(InstantPreviewInput_t3423396443_StaticFields, ___s_Touches_0)); }
	inline TouchU5BU5D_t1849554061* get_s_Touches_0() const { return ___s_Touches_0; }
	inline TouchU5BU5D_t1849554061** get_address_of_s_Touches_0() { return &___s_Touches_0; }
	inline void set_s_Touches_0(TouchU5BU5D_t1849554061* value)
	{
		___s_Touches_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Touches_0), value);
	}

	inline static int32_t get_offset_of_s_TouchList_1() { return static_cast<int32_t>(offsetof(InstantPreviewInput_t3423396443_StaticFields, ___s_TouchList_1)); }
	inline List_1_t3393931610 * get_s_TouchList_1() const { return ___s_TouchList_1; }
	inline List_1_t3393931610 ** get_address_of_s_TouchList_1() { return &___s_TouchList_1; }
	inline void set_s_TouchList_1(List_1_t3393931610 * value)
	{
		___s_TouchList_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_TouchList_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANTPREVIEWINPUT_T3423396443_H
#ifndef APIAVAILABILITYEXTENSIONS_T1605762449_H
#define APIAVAILABILITYEXTENSIONS_T1605762449_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ApiAvailabilityExtensions
struct  ApiAvailabilityExtensions_t1605762449  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIAVAILABILITYEXTENSIONS_T1605762449_H
#ifndef IMAGEAPI_T1356767549_H
#define IMAGEAPI_T1356767549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ImageApi
struct  ImageApi_t1356767549  : public RuntimeObject
{
public:
	// GoogleARCoreInternal.NativeSession GoogleARCoreInternal.ImageApi::m_NativeSession
	NativeSession_t1550589577 * ___m_NativeSession_0;

public:
	inline static int32_t get_offset_of_m_NativeSession_0() { return static_cast<int32_t>(offsetof(ImageApi_t1356767549, ___m_NativeSession_0)); }
	inline NativeSession_t1550589577 * get_m_NativeSession_0() const { return ___m_NativeSession_0; }
	inline NativeSession_t1550589577 ** get_address_of_m_NativeSession_0() { return &___m_NativeSession_0; }
	inline void set_m_NativeSession_0(NativeSession_t1550589577 * value)
	{
		___m_NativeSession_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_NativeSession_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEAPI_T1356767549_H
#ifndef HITTESTAPI_T363854212_H
#define HITTESTAPI_T363854212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.HitTestApi
struct  HitTestApi_t363854212  : public RuntimeObject
{
public:
	// GoogleARCoreInternal.NativeSession GoogleARCoreInternal.HitTestApi::m_NativeSession
	NativeSession_t1550589577 * ___m_NativeSession_0;

public:
	inline static int32_t get_offset_of_m_NativeSession_0() { return static_cast<int32_t>(offsetof(HitTestApi_t363854212, ___m_NativeSession_0)); }
	inline NativeSession_t1550589577 * get_m_NativeSession_0() const { return ___m_NativeSession_0; }
	inline NativeSession_t1550589577 ** get_address_of_m_NativeSession_0() { return &___m_NativeSession_0; }
	inline void set_m_NativeSession_0(NativeSession_t1550589577 * value)
	{
		___m_NativeSession_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_NativeSession_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HITTESTAPI_T363854212_H
#ifndef APICLOUDANCHORSTATEEXTENSIONS_T2681117418_H
#define APICLOUDANCHORSTATEEXTENSIONS_T2681117418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.CrossPlatform.ApiCloudAnchorStateExtensions
struct  ApiCloudAnchorStateExtensions_t2681117418  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APICLOUDANCHORSTATEEXTENSIONS_T2681117418_H
#ifndef APICONSTANTS_T3669054205_H
#define APICONSTANTS_T3669054205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ApiConstants
struct  ApiConstants_t3669054205  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APICONSTANTS_T3669054205_H
#ifndef FRAMEAPI_T2778645972_H
#define FRAMEAPI_T2778645972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.FrameApi
struct  FrameApi_t2778645972  : public RuntimeObject
{
public:
	// GoogleARCoreInternal.NativeSession GoogleARCoreInternal.FrameApi::m_NativeSession
	NativeSession_t1550589577 * ___m_NativeSession_0;

public:
	inline static int32_t get_offset_of_m_NativeSession_0() { return static_cast<int32_t>(offsetof(FrameApi_t2778645972, ___m_NativeSession_0)); }
	inline NativeSession_t1550589577 * get_m_NativeSession_0() const { return ___m_NativeSession_0; }
	inline NativeSession_t1550589577 ** get_address_of_m_NativeSession_0() { return &___m_NativeSession_0; }
	inline void set_m_NativeSession_0(NativeSession_t1550589577 * value)
	{
		___m_NativeSession_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_NativeSession_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEAPI_T2778645972_H
#ifndef APIFEATUREPOINTORIENTATIONMODEEXTENSIONS_T2507422198_H
#define APIFEATUREPOINTORIENTATIONMODEEXTENSIONS_T2507422198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ApiFeaturePointOrientationModeExtensions
struct  ApiFeaturePointOrientationModeExtensions_t2507422198  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIFEATUREPOINTORIENTATIONMODEEXTENSIONS_T2507422198_H
#ifndef CAMERAMETADATAAPI_T4067407319_H
#define CAMERAMETADATAAPI_T4067407319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.CameraMetadataApi
struct  CameraMetadataApi_t4067407319  : public RuntimeObject
{
public:
	// GoogleARCoreInternal.NativeSession GoogleARCoreInternal.CameraMetadataApi::m_NativeSession
	NativeSession_t1550589577 * ___m_NativeSession_0;

public:
	inline static int32_t get_offset_of_m_NativeSession_0() { return static_cast<int32_t>(offsetof(CameraMetadataApi_t4067407319, ___m_NativeSession_0)); }
	inline NativeSession_t1550589577 * get_m_NativeSession_0() const { return ___m_NativeSession_0; }
	inline NativeSession_t1550589577 ** get_address_of_m_NativeSession_0() { return &___m_NativeSession_0; }
	inline void set_m_NativeSession_0(NativeSession_t1550589577 * value)
	{
		___m_NativeSession_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_NativeSession_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAMETADATAAPI_T4067407319_H
#ifndef APILIGHTESTIMATESTATEEXTENSIONS_T164677992_H
#define APILIGHTESTIMATESTATEEXTENSIONS_T164677992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ApiLightEstimateStateExtensions
struct  ApiLightEstimateStateExtensions_t164677992  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APILIGHTESTIMATESTATEEXTENSIONS_T164677992_H
#ifndef CAMERAAPI_T274148848_H
#define CAMERAAPI_T274148848_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.CameraApi
struct  CameraApi_t274148848  : public RuntimeObject
{
public:
	// GoogleARCoreInternal.NativeSession GoogleARCoreInternal.CameraApi::m_NativeSession
	NativeSession_t1550589577 * ___m_NativeSession_0;

public:
	inline static int32_t get_offset_of_m_NativeSession_0() { return static_cast<int32_t>(offsetof(CameraApi_t274148848, ___m_NativeSession_0)); }
	inline NativeSession_t1550589577 * get_m_NativeSession_0() const { return ___m_NativeSession_0; }
	inline NativeSession_t1550589577 ** get_address_of_m_NativeSession_0() { return &___m_NativeSession_0; }
	inline void set_m_NativeSession_0(NativeSession_t1550589577 * value)
	{
		___m_NativeSession_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_NativeSession_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAAPI_T274148848_H
#ifndef AUGMENTEDIMAGEDATABASEAPI_T4243295728_H
#define AUGMENTEDIMAGEDATABASEAPI_T4243295728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.AugmentedImageDatabaseApi
struct  AugmentedImageDatabaseApi_t4243295728  : public RuntimeObject
{
public:
	// GoogleARCoreInternal.NativeSession GoogleARCoreInternal.AugmentedImageDatabaseApi::m_NativeSession
	NativeSession_t1550589577 * ___m_NativeSession_0;

public:
	inline static int32_t get_offset_of_m_NativeSession_0() { return static_cast<int32_t>(offsetof(AugmentedImageDatabaseApi_t4243295728, ___m_NativeSession_0)); }
	inline NativeSession_t1550589577 * get_m_NativeSession_0() const { return ___m_NativeSession_0; }
	inline NativeSession_t1550589577 ** get_address_of_m_NativeSession_0() { return &___m_NativeSession_0; }
	inline void set_m_NativeSession_0(NativeSession_t1550589577 * value)
	{
		___m_NativeSession_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_NativeSession_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUGMENTEDIMAGEDATABASEAPI_T4243295728_H
#ifndef APIPRESTOSTATUSEXTENSIONS_T2873628449_H
#define APIPRESTOSTATUSEXTENSIONS_T2873628449_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ApiPrestoStatusExtensions
struct  ApiPrestoStatusExtensions_t2873628449  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIPRESTOSTATUSEXTENSIONS_T2873628449_H
#ifndef AUGMENTEDIMAGEAPI_T1163146062_H
#define AUGMENTEDIMAGEAPI_T1163146062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.AugmentedImageApi
struct  AugmentedImageApi_t1163146062  : public RuntimeObject
{
public:
	// GoogleARCoreInternal.NativeSession GoogleARCoreInternal.AugmentedImageApi::m_NativeSession
	NativeSession_t1550589577 * ___m_NativeSession_0;

public:
	inline static int32_t get_offset_of_m_NativeSession_0() { return static_cast<int32_t>(offsetof(AugmentedImageApi_t1163146062, ___m_NativeSession_0)); }
	inline NativeSession_t1550589577 * get_m_NativeSession_0() const { return ___m_NativeSession_0; }
	inline NativeSession_t1550589577 ** get_address_of_m_NativeSession_0() { return &___m_NativeSession_0; }
	inline void set_m_NativeSession_0(NativeSession_t1550589577 * value)
	{
		___m_NativeSession_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_NativeSession_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUGMENTEDIMAGEAPI_T1163146062_H
#ifndef LIGHTESTIMATEAPI_T745071092_H
#define LIGHTESTIMATEAPI_T745071092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.LightEstimateApi
struct  LightEstimateApi_t745071092  : public RuntimeObject
{
public:
	// GoogleARCoreInternal.NativeSession GoogleARCoreInternal.LightEstimateApi::m_NativeSession
	NativeSession_t1550589577 * ___m_NativeSession_0;

public:
	inline static int32_t get_offset_of_m_NativeSession_0() { return static_cast<int32_t>(offsetof(LightEstimateApi_t745071092, ___m_NativeSession_0)); }
	inline NativeSession_t1550589577 * get_m_NativeSession_0() const { return ___m_NativeSession_0; }
	inline NativeSession_t1550589577 ** get_address_of_m_NativeSession_0() { return &___m_NativeSession_0; }
	inline void set_m_NativeSession_0(NativeSession_t1550589577 * value)
	{
		___m_NativeSession_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_NativeSession_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTESTIMATEAPI_T745071092_H
#ifndef U3C_SHOWANDROIDTOASTMESSAGEU3EC__ANONSTOREY0_T3165163251_H
#define U3C_SHOWANDROIDTOASTMESSAGEU3EC__ANONSTOREY0_T3165163251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.HelloAR.HelloARController/<_ShowAndroidToastMessage>c__AnonStorey0
struct  U3C_ShowAndroidToastMessageU3Ec__AnonStorey0_t3165163251  : public RuntimeObject
{
public:
	// UnityEngine.AndroidJavaClass GoogleARCore.Examples.HelloAR.HelloARController/<_ShowAndroidToastMessage>c__AnonStorey0::toastClass
	AndroidJavaClass_t32045322 * ___toastClass_0;
	// GoogleARCore.Examples.HelloAR.HelloARController/<_ShowAndroidToastMessage>c__AnonStorey1 GoogleARCore.Examples.HelloAR.HelloARController/<_ShowAndroidToastMessage>c__AnonStorey0::<>f__ref$1
	U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t1208848115 * ___U3CU3Ef__refU241_1;

public:
	inline static int32_t get_offset_of_toastClass_0() { return static_cast<int32_t>(offsetof(U3C_ShowAndroidToastMessageU3Ec__AnonStorey0_t3165163251, ___toastClass_0)); }
	inline AndroidJavaClass_t32045322 * get_toastClass_0() const { return ___toastClass_0; }
	inline AndroidJavaClass_t32045322 ** get_address_of_toastClass_0() { return &___toastClass_0; }
	inline void set_toastClass_0(AndroidJavaClass_t32045322 * value)
	{
		___toastClass_0 = value;
		Il2CppCodeGenWriteBarrier((&___toastClass_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_1() { return static_cast<int32_t>(offsetof(U3C_ShowAndroidToastMessageU3Ec__AnonStorey0_t3165163251, ___U3CU3Ef__refU241_1)); }
	inline U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t1208848115 * get_U3CU3Ef__refU241_1() const { return ___U3CU3Ef__refU241_1; }
	inline U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t1208848115 ** get_address_of_U3CU3Ef__refU241_1() { return &___U3CU3Ef__refU241_1; }
	inline void set_U3CU3Ef__refU241_1(U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t1208848115 * value)
	{
		___U3CU3Ef__refU241_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU241_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_SHOWANDROIDTOASTMESSAGEU3EC__ANONSTOREY0_T3165163251_H
#ifndef U3CINITIALIZEIFNEEDEDU3EC__ITERATOR0_T700312033_H
#define U3CINITIALIZEIFNEEDEDU3EC__ITERATOR0_T700312033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.InstantPreviewManager/<InitializeIfNeeded>c__Iterator0
struct  U3CInitializeIfNeededU3Ec__Iterator0_t700312033  : public RuntimeObject
{
public:
	// System.String GoogleARCoreInternal.InstantPreviewManager/<InitializeIfNeeded>c__Iterator0::<adbPath>__0
	String_t* ___U3CadbPathU3E__0_0;
	// System.String GoogleARCoreInternal.InstantPreviewManager/<InitializeIfNeeded>c__Iterator0::<localVersion>__0
	String_t* ___U3ClocalVersionU3E__0_1;
	// System.Object GoogleARCoreInternal.InstantPreviewManager/<InitializeIfNeeded>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean GoogleARCoreInternal.InstantPreviewManager/<InitializeIfNeeded>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 GoogleARCoreInternal.InstantPreviewManager/<InitializeIfNeeded>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CadbPathU3E__0_0() { return static_cast<int32_t>(offsetof(U3CInitializeIfNeededU3Ec__Iterator0_t700312033, ___U3CadbPathU3E__0_0)); }
	inline String_t* get_U3CadbPathU3E__0_0() const { return ___U3CadbPathU3E__0_0; }
	inline String_t** get_address_of_U3CadbPathU3E__0_0() { return &___U3CadbPathU3E__0_0; }
	inline void set_U3CadbPathU3E__0_0(String_t* value)
	{
		___U3CadbPathU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CadbPathU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3ClocalVersionU3E__0_1() { return static_cast<int32_t>(offsetof(U3CInitializeIfNeededU3Ec__Iterator0_t700312033, ___U3ClocalVersionU3E__0_1)); }
	inline String_t* get_U3ClocalVersionU3E__0_1() const { return ___U3ClocalVersionU3E__0_1; }
	inline String_t** get_address_of_U3ClocalVersionU3E__0_1() { return &___U3ClocalVersionU3E__0_1; }
	inline void set_U3ClocalVersionU3E__0_1(String_t* value)
	{
		___U3ClocalVersionU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClocalVersionU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CInitializeIfNeededU3Ec__Iterator0_t700312033, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CInitializeIfNeededU3Ec__Iterator0_t700312033, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CInitializeIfNeededU3Ec__Iterator0_t700312033, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITIALIZEIFNEEDEDU3EC__ITERATOR0_T700312033_H
#ifndef APITRACKINGSTATEXPEXTENSIONS_T102880617_H
#define APITRACKINGSTATEXPEXTENSIONS_T102880617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.CrossPlatform.ApiTrackingStateXPExtensions
struct  ApiTrackingStateXPExtensions_t102880617  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APITRACKINGSTATEXPEXTENSIONS_T102880617_H
#ifndef U3C_SHOWANDROIDTOASTMESSAGEU3EC__ANONSTOREY0_T2865283779_H
#define U3C_SHOWANDROIDTOASTMESSAGEU3EC__ANONSTOREY0_T2865283779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.CloudAnchor.CloudAnchorController/<_ShowAndroidToastMessage>c__AnonStorey0
struct  U3C_ShowAndroidToastMessageU3Ec__AnonStorey0_t2865283779  : public RuntimeObject
{
public:
	// UnityEngine.AndroidJavaClass GoogleARCore.Examples.CloudAnchor.CloudAnchorController/<_ShowAndroidToastMessage>c__AnonStorey0::toastClass
	AndroidJavaClass_t32045322 * ___toastClass_0;
	// GoogleARCore.Examples.CloudAnchor.CloudAnchorController/<_ShowAndroidToastMessage>c__AnonStorey1 GoogleARCore.Examples.CloudAnchor.CloudAnchorController/<_ShowAndroidToastMessage>c__AnonStorey0::<>f__ref$1
	U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t908968643 * ___U3CU3Ef__refU241_1;

public:
	inline static int32_t get_offset_of_toastClass_0() { return static_cast<int32_t>(offsetof(U3C_ShowAndroidToastMessageU3Ec__AnonStorey0_t2865283779, ___toastClass_0)); }
	inline AndroidJavaClass_t32045322 * get_toastClass_0() const { return ___toastClass_0; }
	inline AndroidJavaClass_t32045322 ** get_address_of_toastClass_0() { return &___toastClass_0; }
	inline void set_toastClass_0(AndroidJavaClass_t32045322 * value)
	{
		___toastClass_0 = value;
		Il2CppCodeGenWriteBarrier((&___toastClass_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_1() { return static_cast<int32_t>(offsetof(U3C_ShowAndroidToastMessageU3Ec__AnonStorey0_t2865283779, ___U3CU3Ef__refU241_1)); }
	inline U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t908968643 * get_U3CU3Ef__refU241_1() const { return ___U3CU3Ef__refU241_1; }
	inline U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t908968643 ** get_address_of_U3CU3Ef__refU241_1() { return &___U3CU3Ef__refU241_1; }
	inline void set_U3CU3Ef__refU241_1(U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t908968643 * value)
	{
		___U3CU3Ef__refU241_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU241_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_SHOWANDROIDTOASTMESSAGEU3EC__ANONSTOREY0_T2865283779_H
#ifndef U3C_SHOWANDROIDTOASTMESSAGEU3EC__ANONSTOREY1_T3919803072_H
#define U3C_SHOWANDROIDTOASTMESSAGEU3EC__ANONSTOREY1_T3919803072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.ComputerVision.ComputerVisionController/<_ShowAndroidToastMessage>c__AnonStorey1
struct  U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t3919803072  : public RuntimeObject
{
public:
	// UnityEngine.AndroidJavaObject GoogleARCore.Examples.ComputerVision.ComputerVisionController/<_ShowAndroidToastMessage>c__AnonStorey1::unityActivity
	AndroidJavaObject_t4131667876 * ___unityActivity_0;
	// System.String GoogleARCore.Examples.ComputerVision.ComputerVisionController/<_ShowAndroidToastMessage>c__AnonStorey1::message
	String_t* ___message_1;

public:
	inline static int32_t get_offset_of_unityActivity_0() { return static_cast<int32_t>(offsetof(U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t3919803072, ___unityActivity_0)); }
	inline AndroidJavaObject_t4131667876 * get_unityActivity_0() const { return ___unityActivity_0; }
	inline AndroidJavaObject_t4131667876 ** get_address_of_unityActivity_0() { return &___unityActivity_0; }
	inline void set_unityActivity_0(AndroidJavaObject_t4131667876 * value)
	{
		___unityActivity_0 = value;
		Il2CppCodeGenWriteBarrier((&___unityActivity_0), value);
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t3919803072, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier((&___message_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_SHOWANDROIDTOASTMESSAGEU3EC__ANONSTOREY1_T3919803072_H
#ifndef U3C_SHOWANDROIDTOASTMESSAGEU3EC__ANONSTOREY1_T908968643_H
#define U3C_SHOWANDROIDTOASTMESSAGEU3EC__ANONSTOREY1_T908968643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.CloudAnchor.CloudAnchorController/<_ShowAndroidToastMessage>c__AnonStorey1
struct  U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t908968643  : public RuntimeObject
{
public:
	// UnityEngine.AndroidJavaObject GoogleARCore.Examples.CloudAnchor.CloudAnchorController/<_ShowAndroidToastMessage>c__AnonStorey1::unityActivity
	AndroidJavaObject_t4131667876 * ___unityActivity_0;
	// System.String GoogleARCore.Examples.CloudAnchor.CloudAnchorController/<_ShowAndroidToastMessage>c__AnonStorey1::message
	String_t* ___message_1;

public:
	inline static int32_t get_offset_of_unityActivity_0() { return static_cast<int32_t>(offsetof(U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t908968643, ___unityActivity_0)); }
	inline AndroidJavaObject_t4131667876 * get_unityActivity_0() const { return ___unityActivity_0; }
	inline AndroidJavaObject_t4131667876 ** get_address_of_unityActivity_0() { return &___unityActivity_0; }
	inline void set_unityActivity_0(AndroidJavaObject_t4131667876 * value)
	{
		___unityActivity_0 = value;
		Il2CppCodeGenWriteBarrier((&___unityActivity_0), value);
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t908968643, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier((&___message_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_SHOWANDROIDTOASTMESSAGEU3EC__ANONSTOREY1_T908968643_H
#ifndef U3C_SHOWANDROIDTOASTMESSAGEU3EC__ANONSTOREY0_T3919737536_H
#define U3C_SHOWANDROIDTOASTMESSAGEU3EC__ANONSTOREY0_T3919737536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.ComputerVision.ComputerVisionController/<_ShowAndroidToastMessage>c__AnonStorey0
struct  U3C_ShowAndroidToastMessageU3Ec__AnonStorey0_t3919737536  : public RuntimeObject
{
public:
	// UnityEngine.AndroidJavaClass GoogleARCore.Examples.ComputerVision.ComputerVisionController/<_ShowAndroidToastMessage>c__AnonStorey0::toastClass
	AndroidJavaClass_t32045322 * ___toastClass_0;
	// GoogleARCore.Examples.ComputerVision.ComputerVisionController/<_ShowAndroidToastMessage>c__AnonStorey1 GoogleARCore.Examples.ComputerVision.ComputerVisionController/<_ShowAndroidToastMessage>c__AnonStorey0::<>f__ref$1
	U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t3919803072 * ___U3CU3Ef__refU241_1;

public:
	inline static int32_t get_offset_of_toastClass_0() { return static_cast<int32_t>(offsetof(U3C_ShowAndroidToastMessageU3Ec__AnonStorey0_t3919737536, ___toastClass_0)); }
	inline AndroidJavaClass_t32045322 * get_toastClass_0() const { return ___toastClass_0; }
	inline AndroidJavaClass_t32045322 ** get_address_of_toastClass_0() { return &___toastClass_0; }
	inline void set_toastClass_0(AndroidJavaClass_t32045322 * value)
	{
		___toastClass_0 = value;
		Il2CppCodeGenWriteBarrier((&___toastClass_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_1() { return static_cast<int32_t>(offsetof(U3C_ShowAndroidToastMessageU3Ec__AnonStorey0_t3919737536, ___U3CU3Ef__refU241_1)); }
	inline U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t3919803072 * get_U3CU3Ef__refU241_1() const { return ___U3CU3Ef__refU241_1; }
	inline U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t3919803072 ** get_address_of_U3CU3Ef__refU241_1() { return &___U3CU3Ef__refU241_1; }
	inline void set_U3CU3Ef__refU241_1(U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t3919803072 * value)
	{
		___U3CU3Ef__refU241_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU241_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_SHOWANDROIDTOASTMESSAGEU3EC__ANONSTOREY0_T3919737536_H
#ifndef ARKITHELPER_T1470960758_H
#define ARKITHELPER_T1470960758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.CloudAnchor.ARKitHelper
struct  ARKitHelper_t1470960758  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult> GoogleARCore.Examples.CloudAnchor.ARKitHelper::m_HitResultList
	List_1_t2751098672 * ___m_HitResultList_0;

public:
	inline static int32_t get_offset_of_m_HitResultList_0() { return static_cast<int32_t>(offsetof(ARKitHelper_t1470960758, ___m_HitResultList_0)); }
	inline List_1_t2751098672 * get_m_HitResultList_0() const { return ___m_HitResultList_0; }
	inline List_1_t2751098672 ** get_address_of_m_HitResultList_0() { return &___m_HitResultList_0; }
	inline void set_m_HitResultList_0(List_1_t2751098672 * value)
	{
		___m_HitResultList_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HitResultList_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARKITHELPER_T1470960758_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef EDGEDETECTOR_T1627390372_H
#define EDGEDETECTOR_T1627390372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.ComputerVision.EdgeDetector
struct  EdgeDetector_t1627390372  : public RuntimeObject
{
public:

public:
};

struct EdgeDetector_t1627390372_StaticFields
{
public:
	// System.Byte[] GoogleARCore.Examples.ComputerVision.EdgeDetector::s_ImageBuffer
	ByteU5BU5D_t4116647657* ___s_ImageBuffer_0;
	// System.Int32 GoogleARCore.Examples.ComputerVision.EdgeDetector::s_ImageBufferSize
	int32_t ___s_ImageBufferSize_1;

public:
	inline static int32_t get_offset_of_s_ImageBuffer_0() { return static_cast<int32_t>(offsetof(EdgeDetector_t1627390372_StaticFields, ___s_ImageBuffer_0)); }
	inline ByteU5BU5D_t4116647657* get_s_ImageBuffer_0() const { return ___s_ImageBuffer_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_s_ImageBuffer_0() { return &___s_ImageBuffer_0; }
	inline void set_s_ImageBuffer_0(ByteU5BU5D_t4116647657* value)
	{
		___s_ImageBuffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_ImageBuffer_0), value);
	}

	inline static int32_t get_offset_of_s_ImageBufferSize_1() { return static_cast<int32_t>(offsetof(EdgeDetector_t1627390372_StaticFields, ___s_ImageBufferSize_1)); }
	inline int32_t get_s_ImageBufferSize_1() const { return ___s_ImageBufferSize_1; }
	inline int32_t* get_address_of_s_ImageBufferSize_1() { return &___s_ImageBufferSize_1; }
	inline void set_s_ImageBufferSize_1(int32_t value)
	{
		___s_ImageBufferSize_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDGEDETECTOR_T1627390372_H
#ifndef TEXTUREREADERAPI_T3129393145_H
#define TEXTUREREADERAPI_T3129393145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.ComputerVision.TextureReaderApi
struct  TextureReaderApi_t3129393145  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREREADERAPI_T3129393145_H
#ifndef U3C_SHOWANDROIDTOASTMESSAGEU3EC__ANONSTOREY1_T1208848115_H
#define U3C_SHOWANDROIDTOASTMESSAGEU3EC__ANONSTOREY1_T1208848115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.HelloAR.HelloARController/<_ShowAndroidToastMessage>c__AnonStorey1
struct  U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t1208848115  : public RuntimeObject
{
public:
	// UnityEngine.AndroidJavaObject GoogleARCore.Examples.HelloAR.HelloARController/<_ShowAndroidToastMessage>c__AnonStorey1::unityActivity
	AndroidJavaObject_t4131667876 * ___unityActivity_0;
	// System.String GoogleARCore.Examples.HelloAR.HelloARController/<_ShowAndroidToastMessage>c__AnonStorey1::message
	String_t* ___message_1;

public:
	inline static int32_t get_offset_of_unityActivity_0() { return static_cast<int32_t>(offsetof(U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t1208848115, ___unityActivity_0)); }
	inline AndroidJavaObject_t4131667876 * get_unityActivity_0() const { return ___unityActivity_0; }
	inline AndroidJavaObject_t4131667876 ** get_address_of_unityActivity_0() { return &___unityActivity_0; }
	inline void set_unityActivity_0(AndroidJavaObject_t4131667876 * value)
	{
		___unityActivity_0 = value;
		Il2CppCodeGenWriteBarrier((&___unityActivity_0), value);
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t1208848115, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier((&___message_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3C_SHOWANDROIDTOASTMESSAGEU3EC__ANONSTOREY1_T1208848115_H
#ifndef EXTERNAPI_T4165057079_H
#define EXTERNAPI_T4165057079_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.AugmentedImageDatabaseApi/ExternApi
struct  ExternApi_t4165057079 
{
public:
	union
	{
		struct
		{
		};
		uint8_t ExternApi_t4165057079__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNAPI_T4165057079_H
#ifndef EXTERNAPI_T2424721670_H
#define EXTERNAPI_T2424721670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.CameraApi/ExternApi
struct  ExternApi_t2424721670 
{
public:
	union
	{
		struct
		{
		};
		uint8_t ExternApi_t2424721670__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNAPI_T2424721670_H
#ifndef EXTERNAPI_T1609953988_H
#define EXTERNAPI_T1609953988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ImageApi/ExternApi
struct  ExternApi_t1609953988 
{
public:
	union
	{
		struct
		{
		};
		uint8_t ExternApi_t1609953988__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNAPI_T1609953988_H
#ifndef ANCHORIDFROMROOMRESPONSEMESSAGE_T1194490927_H
#define ANCHORIDFROMROOMRESPONSEMESSAGE_T1194490927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.CloudAnchor.AnchorIdFromRoomResponseMessage
struct  AnchorIdFromRoomResponseMessage_t1194490927  : public MessageBase_t3584795631
{
public:
	// System.Boolean GoogleARCore.Examples.CloudAnchor.AnchorIdFromRoomResponseMessage::Found
	bool ___Found_0;
	// System.String GoogleARCore.Examples.CloudAnchor.AnchorIdFromRoomResponseMessage::AnchorId
	String_t* ___AnchorId_1;

public:
	inline static int32_t get_offset_of_Found_0() { return static_cast<int32_t>(offsetof(AnchorIdFromRoomResponseMessage_t1194490927, ___Found_0)); }
	inline bool get_Found_0() const { return ___Found_0; }
	inline bool* get_address_of_Found_0() { return &___Found_0; }
	inline void set_Found_0(bool value)
	{
		___Found_0 = value;
	}

	inline static int32_t get_offset_of_AnchorId_1() { return static_cast<int32_t>(offsetof(AnchorIdFromRoomResponseMessage_t1194490927, ___AnchorId_1)); }
	inline String_t* get_AnchorId_1() const { return ___AnchorId_1; }
	inline String_t** get_address_of_AnchorId_1() { return &___AnchorId_1; }
	inline void set_AnchorId_1(String_t* value)
	{
		___AnchorId_1 = value;
		Il2CppCodeGenWriteBarrier((&___AnchorId_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHORIDFROMROOMRESPONSEMESSAGE_T1194490927_H
#ifndef ANCHORIDFROMROOMREQUESTMESSAGE_T2946441754_H
#define ANCHORIDFROMROOMREQUESTMESSAGE_T2946441754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.CloudAnchor.AnchorIdFromRoomRequestMessage
struct  AnchorIdFromRoomRequestMessage_t2946441754  : public MessageBase_t3584795631
{
public:
	// System.Int32 GoogleARCore.Examples.CloudAnchor.AnchorIdFromRoomRequestMessage::RoomId
	int32_t ___RoomId_0;

public:
	inline static int32_t get_offset_of_RoomId_0() { return static_cast<int32_t>(offsetof(AnchorIdFromRoomRequestMessage_t2946441754, ___RoomId_0)); }
	inline int32_t get_RoomId_0() const { return ___RoomId_0; }
	inline int32_t* get_address_of_RoomId_0() { return &___RoomId_0; }
	inline void set_RoomId_0(int32_t value)
	{
		___RoomId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHORIDFROMROOMREQUESTMESSAGE_T2946441754_H
#ifndef EXTERNAPI_T3709617879_H
#define EXTERNAPI_T3709617879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.CameraMetadataApi/ExternApi
struct  ExternApi_t3709617879 
{
public:
	union
	{
		struct
		{
		};
		uint8_t ExternApi_t3709617879__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNAPI_T3709617879_H
#ifndef EXTERNAPI_T181214333_H
#define EXTERNAPI_T181214333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.AugmentedImageApi/ExternApi
struct  ExternApi_t181214333 
{
public:
	union
	{
		struct
		{
		};
		uint8_t ExternApi_t181214333__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNAPI_T181214333_H
#ifndef EXTERNAPI_T903494213_H
#define EXTERNAPI_T903494213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.FrameApi/ExternApi
struct  ExternApi_t903494213 
{
public:
	union
	{
		struct
		{
		};
		uint8_t ExternApi_t903494213__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNAPI_T903494213_H
#ifndef GCHANDLE_T3351438187_H
#define GCHANDLE_T3351438187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.GCHandle
struct  GCHandle_t3351438187 
{
public:
	// System.Int32 System.Runtime.InteropServices.GCHandle::handle
	int32_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(GCHandle_t3351438187, ___handle_0)); }
	inline int32_t get_handle_0() const { return ___handle_0; }
	inline int32_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(int32_t value)
	{
		___handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCHANDLE_T3351438187_H
#ifndef EXTERNAPI_T1588697414_H
#define EXTERNAPI_T1588697414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.HitTestApi/ExternApi
struct  ExternApi_t1588697414 
{
public:
	union
	{
		struct
		{
		};
		uint8_t ExternApi_t1588697414__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNAPI_T1588697414_H
#ifndef ROOMSHARINGMSGTYPE_T467542496_H
#define ROOMSHARINGMSGTYPE_T467542496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.CloudAnchor.RoomSharingMsgType
struct  RoomSharingMsgType_t467542496 
{
public:
	union
	{
		struct
		{
		};
		uint8_t RoomSharingMsgType_t467542496__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOMSHARINGMSGTYPE_T467542496_H
#ifndef APIPOSEDATA_T4278576221_H
#define APIPOSEDATA_T4278576221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ApiPoseData
struct  ApiPoseData_t4278576221 
{
public:
	// System.Single GoogleARCoreInternal.ApiPoseData::Qx
	float ___Qx_0;
	// System.Single GoogleARCoreInternal.ApiPoseData::Qy
	float ___Qy_1;
	// System.Single GoogleARCoreInternal.ApiPoseData::Qz
	float ___Qz_2;
	// System.Single GoogleARCoreInternal.ApiPoseData::Qw
	float ___Qw_3;
	// System.Single GoogleARCoreInternal.ApiPoseData::X
	float ___X_4;
	// System.Single GoogleARCoreInternal.ApiPoseData::Y
	float ___Y_5;
	// System.Single GoogleARCoreInternal.ApiPoseData::Z
	float ___Z_6;

public:
	inline static int32_t get_offset_of_Qx_0() { return static_cast<int32_t>(offsetof(ApiPoseData_t4278576221, ___Qx_0)); }
	inline float get_Qx_0() const { return ___Qx_0; }
	inline float* get_address_of_Qx_0() { return &___Qx_0; }
	inline void set_Qx_0(float value)
	{
		___Qx_0 = value;
	}

	inline static int32_t get_offset_of_Qy_1() { return static_cast<int32_t>(offsetof(ApiPoseData_t4278576221, ___Qy_1)); }
	inline float get_Qy_1() const { return ___Qy_1; }
	inline float* get_address_of_Qy_1() { return &___Qy_1; }
	inline void set_Qy_1(float value)
	{
		___Qy_1 = value;
	}

	inline static int32_t get_offset_of_Qz_2() { return static_cast<int32_t>(offsetof(ApiPoseData_t4278576221, ___Qz_2)); }
	inline float get_Qz_2() const { return ___Qz_2; }
	inline float* get_address_of_Qz_2() { return &___Qz_2; }
	inline void set_Qz_2(float value)
	{
		___Qz_2 = value;
	}

	inline static int32_t get_offset_of_Qw_3() { return static_cast<int32_t>(offsetof(ApiPoseData_t4278576221, ___Qw_3)); }
	inline float get_Qw_3() const { return ___Qw_3; }
	inline float* get_address_of_Qw_3() { return &___Qw_3; }
	inline void set_Qw_3(float value)
	{
		___Qw_3 = value;
	}

	inline static int32_t get_offset_of_X_4() { return static_cast<int32_t>(offsetof(ApiPoseData_t4278576221, ___X_4)); }
	inline float get_X_4() const { return ___X_4; }
	inline float* get_address_of_X_4() { return &___X_4; }
	inline void set_X_4(float value)
	{
		___X_4 = value;
	}

	inline static int32_t get_offset_of_Y_5() { return static_cast<int32_t>(offsetof(ApiPoseData_t4278576221, ___Y_5)); }
	inline float get_Y_5() const { return ___Y_5; }
	inline float* get_address_of_Y_5() { return &___Y_5; }
	inline void set_Y_5(float value)
	{
		___Y_5 = value;
	}

	inline static int32_t get_offset_of_Z_6() { return static_cast<int32_t>(offsetof(ApiPoseData_t4278576221, ___Z_6)); }
	inline float get_Z_6() const { return ___Z_6; }
	inline float* get_address_of_Z_6() { return &___Z_6; }
	inline void set_Z_6(float value)
	{
		___Z_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIPOSEDATA_T4278576221_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef EXTERNAPI_T2048605508_H
#define EXTERNAPI_T2048605508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.ComputerVision.TextureReaderApi/ExternApi
struct  ExternApi_t2048605508 
{
public:
	union
	{
		struct
		{
		};
		uint8_t ExternApi_t2048605508__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNAPI_T2048605508_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef NATIVEAPI_T4275999757_H
#define NATIVEAPI_T4275999757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.InstantPreviewInput/NativeApi
struct  NativeApi_t4275999757 
{
public:
	union
	{
		struct
		{
		};
		uint8_t NativeApi_t4275999757__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEAPI_T4275999757_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef NATIVEAPI_T4070010807_H
#define NATIVEAPI_T4070010807_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.InstantPreviewManager/NativeApi
struct  NativeApi_t4070010807 
{
public:
	union
	{
		struct
		{
		};
		uint8_t NativeApi_t4070010807__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEAPI_T4070010807_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef EXTERNAPI_T3383842396_H
#define EXTERNAPI_T3383842396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.LightEstimateApi/ExternApi
struct  ExternApi_t3383842396 
{
public:
	union
	{
		struct
		{
		};
		uint8_t ExternApi_t3383842396__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNAPI_T3383842396_H
#ifndef EXTERNAPI_T1430713662_H
#define EXTERNAPI_T1430713662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.PlaneApi/ExternApi
struct  ExternApi_t1430713662 
{
public:
	union
	{
		struct
		{
		};
		uint8_t ExternApi_t1430713662__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNAPI_T1430713662_H
#ifndef EXTERNAPI_T1551582343_H
#define EXTERNAPI_T1551582343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.PointApi/ExternApi
struct  ExternApi_t1551582343 
{
public:
	union
	{
		struct
		{
		};
		uint8_t ExternApi_t1551582343__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNAPI_T1551582343_H
#ifndef EXTERNAPI_T2991644367_H
#define EXTERNAPI_T2991644367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.AnchorApi/ExternApi
struct  ExternApi_t2991644367 
{
public:
	union
	{
		struct
		{
		};
		uint8_t ExternApi_t2991644367__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNAPI_T2991644367_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef ANDROIDPERMISSIONSREQUESTRESULT_T3910302873_H
#define ANDROIDPERMISSIONSREQUESTRESULT_T3910302873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.AndroidPermissionsRequestResult
struct  AndroidPermissionsRequestResult_t3910302873 
{
public:
	union
	{
		struct
		{
			// System.String[] GoogleARCore.AndroidPermissionsRequestResult::<PermissionNames>k__BackingField
			StringU5BU5D_t1281789340* ___U3CPermissionNamesU3Ek__BackingField_0;
			// System.Boolean[] GoogleARCore.AndroidPermissionsRequestResult::<GrantResults>k__BackingField
			BooleanU5BU5D_t2897418192* ___U3CGrantResultsU3Ek__BackingField_1;
		};
		uint8_t AndroidPermissionsRequestResult_t3910302873__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CPermissionNamesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AndroidPermissionsRequestResult_t3910302873, ___U3CPermissionNamesU3Ek__BackingField_0)); }
	inline StringU5BU5D_t1281789340* get_U3CPermissionNamesU3Ek__BackingField_0() const { return ___U3CPermissionNamesU3Ek__BackingField_0; }
	inline StringU5BU5D_t1281789340** get_address_of_U3CPermissionNamesU3Ek__BackingField_0() { return &___U3CPermissionNamesU3Ek__BackingField_0; }
	inline void set_U3CPermissionNamesU3Ek__BackingField_0(StringU5BU5D_t1281789340* value)
	{
		___U3CPermissionNamesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPermissionNamesU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CGrantResultsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AndroidPermissionsRequestResult_t3910302873, ___U3CGrantResultsU3Ek__BackingField_1)); }
	inline BooleanU5BU5D_t2897418192* get_U3CGrantResultsU3Ek__BackingField_1() const { return ___U3CGrantResultsU3Ek__BackingField_1; }
	inline BooleanU5BU5D_t2897418192** get_address_of_U3CGrantResultsU3Ek__BackingField_1() { return &___U3CGrantResultsU3Ek__BackingField_1; }
	inline void set_U3CGrantResultsU3Ek__BackingField_1(BooleanU5BU5D_t2897418192* value)
	{
		___U3CGrantResultsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGrantResultsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GoogleARCore.AndroidPermissionsRequestResult
struct AndroidPermissionsRequestResult_t3910302873_marshaled_pinvoke
{
	union
	{
		struct
		{
			char** ___U3CPermissionNamesU3Ek__BackingField_0;
			int32_t* ___U3CGrantResultsU3Ek__BackingField_1;
		};
		uint8_t AndroidPermissionsRequestResult_t3910302873__padding[1];
	};
};
// Native definition for COM marshalling of GoogleARCore.AndroidPermissionsRequestResult
struct AndroidPermissionsRequestResult_t3910302873_marshaled_com
{
	union
	{
		struct
		{
			Il2CppChar** ___U3CPermissionNamesU3Ek__BackingField_0;
			int32_t* ___U3CGrantResultsU3Ek__BackingField_1;
		};
		uint8_t AndroidPermissionsRequestResult_t3910302873__padding[1];
	};
};
#endif // ANDROIDPERMISSIONSREQUESTRESULT_T3910302873_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef SCREENORIENTATION_T1705519499_H
#define SCREENORIENTATION_T1705519499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScreenOrientation
struct  ScreenOrientation_t1705519499 
{
public:
	// System.Int32 UnityEngine.ScreenOrientation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScreenOrientation_t1705519499, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENORIENTATION_T1705519499_H
#ifndef CONNECTSTATE_T1049972864_H
#define CONNECTSTATE_T1049972864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkClient/ConnectState
struct  ConnectState_t1049972864 
{
public:
	// System.Int32 UnityEngine.Networking.NetworkClient/ConnectState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConnectState_t1049972864, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTSTATE_T1049972864_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef DISPLAYUVCOORDS_T972795656_H
#define DISPLAYUVCOORDS_T972795656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.DisplayUvCoords
struct  DisplayUvCoords_t972795656 
{
public:
	// UnityEngine.Vector2 GoogleARCore.DisplayUvCoords::TopLeft
	Vector2_t2156229523  ___TopLeft_0;
	// UnityEngine.Vector2 GoogleARCore.DisplayUvCoords::TopRight
	Vector2_t2156229523  ___TopRight_1;
	// UnityEngine.Vector2 GoogleARCore.DisplayUvCoords::BottomLeft
	Vector2_t2156229523  ___BottomLeft_2;
	// UnityEngine.Vector2 GoogleARCore.DisplayUvCoords::BottomRight
	Vector2_t2156229523  ___BottomRight_3;

public:
	inline static int32_t get_offset_of_TopLeft_0() { return static_cast<int32_t>(offsetof(DisplayUvCoords_t972795656, ___TopLeft_0)); }
	inline Vector2_t2156229523  get_TopLeft_0() const { return ___TopLeft_0; }
	inline Vector2_t2156229523 * get_address_of_TopLeft_0() { return &___TopLeft_0; }
	inline void set_TopLeft_0(Vector2_t2156229523  value)
	{
		___TopLeft_0 = value;
	}

	inline static int32_t get_offset_of_TopRight_1() { return static_cast<int32_t>(offsetof(DisplayUvCoords_t972795656, ___TopRight_1)); }
	inline Vector2_t2156229523  get_TopRight_1() const { return ___TopRight_1; }
	inline Vector2_t2156229523 * get_address_of_TopRight_1() { return &___TopRight_1; }
	inline void set_TopRight_1(Vector2_t2156229523  value)
	{
		___TopRight_1 = value;
	}

	inline static int32_t get_offset_of_BottomLeft_2() { return static_cast<int32_t>(offsetof(DisplayUvCoords_t972795656, ___BottomLeft_2)); }
	inline Vector2_t2156229523  get_BottomLeft_2() const { return ___BottomLeft_2; }
	inline Vector2_t2156229523 * get_address_of_BottomLeft_2() { return &___BottomLeft_2; }
	inline void set_BottomLeft_2(Vector2_t2156229523  value)
	{
		___BottomLeft_2 = value;
	}

	inline static int32_t get_offset_of_BottomRight_3() { return static_cast<int32_t>(offsetof(DisplayUvCoords_t972795656, ___BottomRight_3)); }
	inline Vector2_t2156229523  get_BottomRight_3() const { return ___BottomRight_3; }
	inline Vector2_t2156229523 * get_address_of_BottomRight_3() { return &___BottomRight_3; }
	inline void set_BottomRight_3(Vector2_t2156229523  value)
	{
		___BottomRight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAYUVCOORDS_T972795656_H
#ifndef ANDROIDJAVAPROXY_T2835824643_H
#define ANDROIDJAVAPROXY_T2835824643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AndroidJavaProxy
struct  AndroidJavaProxy_t2835824643  : public RuntimeObject
{
public:
	// UnityEngine.AndroidJavaClass UnityEngine.AndroidJavaProxy::javaInterface
	AndroidJavaClass_t32045322 * ___javaInterface_0;
	// UnityEngine.AndroidJavaObject UnityEngine.AndroidJavaProxy::proxyObject
	AndroidJavaObject_t4131667876 * ___proxyObject_1;

public:
	inline static int32_t get_offset_of_javaInterface_0() { return static_cast<int32_t>(offsetof(AndroidJavaProxy_t2835824643, ___javaInterface_0)); }
	inline AndroidJavaClass_t32045322 * get_javaInterface_0() const { return ___javaInterface_0; }
	inline AndroidJavaClass_t32045322 ** get_address_of_javaInterface_0() { return &___javaInterface_0; }
	inline void set_javaInterface_0(AndroidJavaClass_t32045322 * value)
	{
		___javaInterface_0 = value;
		Il2CppCodeGenWriteBarrier((&___javaInterface_0), value);
	}

	inline static int32_t get_offset_of_proxyObject_1() { return static_cast<int32_t>(offsetof(AndroidJavaProxy_t2835824643, ___proxyObject_1)); }
	inline AndroidJavaObject_t4131667876 * get_proxyObject_1() const { return ___proxyObject_1; }
	inline AndroidJavaObject_t4131667876 ** get_address_of_proxyObject_1() { return &___proxyObject_1; }
	inline void set_proxyObject_1(AndroidJavaObject_t4131667876 * value)
	{
		___proxyObject_1 = value;
		Il2CppCodeGenWriteBarrier((&___proxyObject_1), value);
	}
};

struct AndroidJavaProxy_t2835824643_StaticFields
{
public:
	// UnityEngine.GlobalJavaObjectRef UnityEngine.AndroidJavaProxy::s_JavaLangSystemClass
	GlobalJavaObjectRef_t3225273728 * ___s_JavaLangSystemClass_2;
	// System.IntPtr UnityEngine.AndroidJavaProxy::s_HashCodeMethodID
	intptr_t ___s_HashCodeMethodID_3;

public:
	inline static int32_t get_offset_of_s_JavaLangSystemClass_2() { return static_cast<int32_t>(offsetof(AndroidJavaProxy_t2835824643_StaticFields, ___s_JavaLangSystemClass_2)); }
	inline GlobalJavaObjectRef_t3225273728 * get_s_JavaLangSystemClass_2() const { return ___s_JavaLangSystemClass_2; }
	inline GlobalJavaObjectRef_t3225273728 ** get_address_of_s_JavaLangSystemClass_2() { return &___s_JavaLangSystemClass_2; }
	inline void set_s_JavaLangSystemClass_2(GlobalJavaObjectRef_t3225273728 * value)
	{
		___s_JavaLangSystemClass_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_JavaLangSystemClass_2), value);
	}

	inline static int32_t get_offset_of_s_HashCodeMethodID_3() { return static_cast<int32_t>(offsetof(AndroidJavaProxy_t2835824643_StaticFields, ___s_HashCodeMethodID_3)); }
	inline intptr_t get_s_HashCodeMethodID_3() const { return ___s_HashCodeMethodID_3; }
	inline intptr_t* get_address_of_s_HashCodeMethodID_3() { return &___s_HashCodeMethodID_3; }
	inline void set_s_HashCodeMethodID_3(intptr_t value)
	{
		___s_HashCodeMethodID_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDJAVAPROXY_T2835824643_H
#ifndef TRACKINGSTATE_T1099935182_H
#define TRACKINGSTATE_T1099935182_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.TrackingState
struct  TrackingState_t1099935182 
{
public:
	// System.Int32 GoogleARCore.TrackingState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TrackingState_t1099935182, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKINGSTATE_T1099935182_H
#ifndef PLANEAPI_T1746941456_H
#define PLANEAPI_T1746941456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.PlaneApi
struct  PlaneApi_t1746941456  : public RuntimeObject
{
public:
	// GoogleARCoreInternal.NativeSession GoogleARCoreInternal.PlaneApi::m_NativeSession
	NativeSession_t1550589577 * ___m_NativeSession_1;
	// System.Single[] GoogleARCoreInternal.PlaneApi::m_TmpPoints
	SingleU5BU5D_t1444911251* ___m_TmpPoints_2;
	// System.Runtime.InteropServices.GCHandle GoogleARCoreInternal.PlaneApi::m_TmpPointsHandle
	GCHandle_t3351438187  ___m_TmpPointsHandle_3;

public:
	inline static int32_t get_offset_of_m_NativeSession_1() { return static_cast<int32_t>(offsetof(PlaneApi_t1746941456, ___m_NativeSession_1)); }
	inline NativeSession_t1550589577 * get_m_NativeSession_1() const { return ___m_NativeSession_1; }
	inline NativeSession_t1550589577 ** get_address_of_m_NativeSession_1() { return &___m_NativeSession_1; }
	inline void set_m_NativeSession_1(NativeSession_t1550589577 * value)
	{
		___m_NativeSession_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_NativeSession_1), value);
	}

	inline static int32_t get_offset_of_m_TmpPoints_2() { return static_cast<int32_t>(offsetof(PlaneApi_t1746941456, ___m_TmpPoints_2)); }
	inline SingleU5BU5D_t1444911251* get_m_TmpPoints_2() const { return ___m_TmpPoints_2; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_TmpPoints_2() { return &___m_TmpPoints_2; }
	inline void set_m_TmpPoints_2(SingleU5BU5D_t1444911251* value)
	{
		___m_TmpPoints_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TmpPoints_2), value);
	}

	inline static int32_t get_offset_of_m_TmpPointsHandle_3() { return static_cast<int32_t>(offsetof(PlaneApi_t1746941456, ___m_TmpPointsHandle_3)); }
	inline GCHandle_t3351438187  get_m_TmpPointsHandle_3() const { return ___m_TmpPointsHandle_3; }
	inline GCHandle_t3351438187 * get_address_of_m_TmpPointsHandle_3() { return &___m_TmpPointsHandle_3; }
	inline void set_m_TmpPointsHandle_3(GCHandle_t3351438187  value)
	{
		___m_TmpPointsHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANEAPI_T1746941456_H
#ifndef NATIVESESSION_T1550589577_H
#define NATIVESESSION_T1550589577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.NativeSession
struct  NativeSession_t1550589577  : public RuntimeObject
{
public:
	// System.Single GoogleARCoreInternal.NativeSession::m_LastReleasedPointcloudTimestamp
	float ___m_LastReleasedPointcloudTimestamp_1;
	// GoogleARCoreInternal.TrackableManager GoogleARCoreInternal.NativeSession::m_TrackableManager
	TrackableManager_t1722633017 * ___m_TrackableManager_2;
	// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32> GoogleARCoreInternal.NativeSession::m_AcquiredHandleCounts
	Dictionary_2_t1172811472 * ___m_AcquiredHandleCounts_3;
	// System.IntPtr GoogleARCoreInternal.NativeSession::<SessionHandle>k__BackingField
	intptr_t ___U3CSessionHandleU3Ek__BackingField_4;
	// System.IntPtr GoogleARCoreInternal.NativeSession::<FrameHandle>k__BackingField
	intptr_t ___U3CFrameHandleU3Ek__BackingField_5;
	// System.IntPtr GoogleARCoreInternal.NativeSession::<PointCloudHandle>k__BackingField
	intptr_t ___U3CPointCloudHandleU3Ek__BackingField_6;
	// GoogleARCoreInternal.AnchorApi GoogleARCoreInternal.NativeSession::<AnchorApi>k__BackingField
	AnchorApi_t2165463 * ___U3CAnchorApiU3Ek__BackingField_7;
	// GoogleARCoreInternal.AugmentedImageApi GoogleARCoreInternal.NativeSession::<AugmentedImageApi>k__BackingField
	AugmentedImageApi_t1163146062 * ___U3CAugmentedImageApiU3Ek__BackingField_8;
	// GoogleARCoreInternal.AugmentedImageDatabaseApi GoogleARCoreInternal.NativeSession::<AugmentedImageDatabaseApi>k__BackingField
	AugmentedImageDatabaseApi_t4243295728 * ___U3CAugmentedImageDatabaseApiU3Ek__BackingField_9;
	// GoogleARCoreInternal.CameraApi GoogleARCoreInternal.NativeSession::<CameraApi>k__BackingField
	CameraApi_t274148848 * ___U3CCameraApiU3Ek__BackingField_10;
	// GoogleARCoreInternal.CameraMetadataApi GoogleARCoreInternal.NativeSession::<CameraMetadataApi>k__BackingField
	CameraMetadataApi_t4067407319 * ___U3CCameraMetadataApiU3Ek__BackingField_11;
	// GoogleARCoreInternal.FrameApi GoogleARCoreInternal.NativeSession::<FrameApi>k__BackingField
	FrameApi_t2778645972 * ___U3CFrameApiU3Ek__BackingField_12;
	// GoogleARCoreInternal.HitTestApi GoogleARCoreInternal.NativeSession::<HitTestApi>k__BackingField
	HitTestApi_t363854212 * ___U3CHitTestApiU3Ek__BackingField_13;
	// GoogleARCoreInternal.ImageApi GoogleARCoreInternal.NativeSession::<ImageApi>k__BackingField
	ImageApi_t1356767549 * ___U3CImageApiU3Ek__BackingField_14;
	// GoogleARCoreInternal.LightEstimateApi GoogleARCoreInternal.NativeSession::<LightEstimateApi>k__BackingField
	LightEstimateApi_t745071092 * ___U3CLightEstimateApiU3Ek__BackingField_15;
	// GoogleARCoreInternal.PlaneApi GoogleARCoreInternal.NativeSession::<PlaneApi>k__BackingField
	PlaneApi_t1746941456 * ___U3CPlaneApiU3Ek__BackingField_16;
	// GoogleARCoreInternal.PointApi GoogleARCoreInternal.NativeSession::<PointApi>k__BackingField
	PointApi_t3912385759 * ___U3CPointApiU3Ek__BackingField_17;
	// GoogleARCoreInternal.PointCloudApi GoogleARCoreInternal.NativeSession::<PointCloudApi>k__BackingField
	PointCloudApi_t650451515 * ___U3CPointCloudApiU3Ek__BackingField_18;
	// GoogleARCoreInternal.PoseApi GoogleARCoreInternal.NativeSession::<PoseApi>k__BackingField
	PoseApi_t3025584507 * ___U3CPoseApiU3Ek__BackingField_19;
	// GoogleARCoreInternal.SessionApi GoogleARCoreInternal.NativeSession::<SessionApi>k__BackingField
	SessionApi_t3641277092 * ___U3CSessionApiU3Ek__BackingField_20;
	// GoogleARCoreInternal.SessionConfigApi GoogleARCoreInternal.NativeSession::<SessionConfigApi>k__BackingField
	SessionConfigApi_t3119440216 * ___U3CSessionConfigApiU3Ek__BackingField_21;
	// GoogleARCoreInternal.TrackableApi GoogleARCoreInternal.NativeSession::<TrackableApi>k__BackingField
	TrackableApi_t1100424269 * ___U3CTrackableApiU3Ek__BackingField_22;
	// GoogleARCoreInternal.TrackableListApi GoogleARCoreInternal.NativeSession::<TrackableListApi>k__BackingField
	TrackableListApi_t1931239134 * ___U3CTrackableListApiU3Ek__BackingField_23;

public:
	inline static int32_t get_offset_of_m_LastReleasedPointcloudTimestamp_1() { return static_cast<int32_t>(offsetof(NativeSession_t1550589577, ___m_LastReleasedPointcloudTimestamp_1)); }
	inline float get_m_LastReleasedPointcloudTimestamp_1() const { return ___m_LastReleasedPointcloudTimestamp_1; }
	inline float* get_address_of_m_LastReleasedPointcloudTimestamp_1() { return &___m_LastReleasedPointcloudTimestamp_1; }
	inline void set_m_LastReleasedPointcloudTimestamp_1(float value)
	{
		___m_LastReleasedPointcloudTimestamp_1 = value;
	}

	inline static int32_t get_offset_of_m_TrackableManager_2() { return static_cast<int32_t>(offsetof(NativeSession_t1550589577, ___m_TrackableManager_2)); }
	inline TrackableManager_t1722633017 * get_m_TrackableManager_2() const { return ___m_TrackableManager_2; }
	inline TrackableManager_t1722633017 ** get_address_of_m_TrackableManager_2() { return &___m_TrackableManager_2; }
	inline void set_m_TrackableManager_2(TrackableManager_t1722633017 * value)
	{
		___m_TrackableManager_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackableManager_2), value);
	}

	inline static int32_t get_offset_of_m_AcquiredHandleCounts_3() { return static_cast<int32_t>(offsetof(NativeSession_t1550589577, ___m_AcquiredHandleCounts_3)); }
	inline Dictionary_2_t1172811472 * get_m_AcquiredHandleCounts_3() const { return ___m_AcquiredHandleCounts_3; }
	inline Dictionary_2_t1172811472 ** get_address_of_m_AcquiredHandleCounts_3() { return &___m_AcquiredHandleCounts_3; }
	inline void set_m_AcquiredHandleCounts_3(Dictionary_2_t1172811472 * value)
	{
		___m_AcquiredHandleCounts_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AcquiredHandleCounts_3), value);
	}

	inline static int32_t get_offset_of_U3CSessionHandleU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(NativeSession_t1550589577, ___U3CSessionHandleU3Ek__BackingField_4)); }
	inline intptr_t get_U3CSessionHandleU3Ek__BackingField_4() const { return ___U3CSessionHandleU3Ek__BackingField_4; }
	inline intptr_t* get_address_of_U3CSessionHandleU3Ek__BackingField_4() { return &___U3CSessionHandleU3Ek__BackingField_4; }
	inline void set_U3CSessionHandleU3Ek__BackingField_4(intptr_t value)
	{
		___U3CSessionHandleU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CFrameHandleU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(NativeSession_t1550589577, ___U3CFrameHandleU3Ek__BackingField_5)); }
	inline intptr_t get_U3CFrameHandleU3Ek__BackingField_5() const { return ___U3CFrameHandleU3Ek__BackingField_5; }
	inline intptr_t* get_address_of_U3CFrameHandleU3Ek__BackingField_5() { return &___U3CFrameHandleU3Ek__BackingField_5; }
	inline void set_U3CFrameHandleU3Ek__BackingField_5(intptr_t value)
	{
		___U3CFrameHandleU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CPointCloudHandleU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(NativeSession_t1550589577, ___U3CPointCloudHandleU3Ek__BackingField_6)); }
	inline intptr_t get_U3CPointCloudHandleU3Ek__BackingField_6() const { return ___U3CPointCloudHandleU3Ek__BackingField_6; }
	inline intptr_t* get_address_of_U3CPointCloudHandleU3Ek__BackingField_6() { return &___U3CPointCloudHandleU3Ek__BackingField_6; }
	inline void set_U3CPointCloudHandleU3Ek__BackingField_6(intptr_t value)
	{
		___U3CPointCloudHandleU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CAnchorApiU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(NativeSession_t1550589577, ___U3CAnchorApiU3Ek__BackingField_7)); }
	inline AnchorApi_t2165463 * get_U3CAnchorApiU3Ek__BackingField_7() const { return ___U3CAnchorApiU3Ek__BackingField_7; }
	inline AnchorApi_t2165463 ** get_address_of_U3CAnchorApiU3Ek__BackingField_7() { return &___U3CAnchorApiU3Ek__BackingField_7; }
	inline void set_U3CAnchorApiU3Ek__BackingField_7(AnchorApi_t2165463 * value)
	{
		___U3CAnchorApiU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAnchorApiU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CAugmentedImageApiU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(NativeSession_t1550589577, ___U3CAugmentedImageApiU3Ek__BackingField_8)); }
	inline AugmentedImageApi_t1163146062 * get_U3CAugmentedImageApiU3Ek__BackingField_8() const { return ___U3CAugmentedImageApiU3Ek__BackingField_8; }
	inline AugmentedImageApi_t1163146062 ** get_address_of_U3CAugmentedImageApiU3Ek__BackingField_8() { return &___U3CAugmentedImageApiU3Ek__BackingField_8; }
	inline void set_U3CAugmentedImageApiU3Ek__BackingField_8(AugmentedImageApi_t1163146062 * value)
	{
		___U3CAugmentedImageApiU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAugmentedImageApiU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CAugmentedImageDatabaseApiU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(NativeSession_t1550589577, ___U3CAugmentedImageDatabaseApiU3Ek__BackingField_9)); }
	inline AugmentedImageDatabaseApi_t4243295728 * get_U3CAugmentedImageDatabaseApiU3Ek__BackingField_9() const { return ___U3CAugmentedImageDatabaseApiU3Ek__BackingField_9; }
	inline AugmentedImageDatabaseApi_t4243295728 ** get_address_of_U3CAugmentedImageDatabaseApiU3Ek__BackingField_9() { return &___U3CAugmentedImageDatabaseApiU3Ek__BackingField_9; }
	inline void set_U3CAugmentedImageDatabaseApiU3Ek__BackingField_9(AugmentedImageDatabaseApi_t4243295728 * value)
	{
		___U3CAugmentedImageDatabaseApiU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAugmentedImageDatabaseApiU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CCameraApiU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(NativeSession_t1550589577, ___U3CCameraApiU3Ek__BackingField_10)); }
	inline CameraApi_t274148848 * get_U3CCameraApiU3Ek__BackingField_10() const { return ___U3CCameraApiU3Ek__BackingField_10; }
	inline CameraApi_t274148848 ** get_address_of_U3CCameraApiU3Ek__BackingField_10() { return &___U3CCameraApiU3Ek__BackingField_10; }
	inline void set_U3CCameraApiU3Ek__BackingField_10(CameraApi_t274148848 * value)
	{
		___U3CCameraApiU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCameraApiU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CCameraMetadataApiU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(NativeSession_t1550589577, ___U3CCameraMetadataApiU3Ek__BackingField_11)); }
	inline CameraMetadataApi_t4067407319 * get_U3CCameraMetadataApiU3Ek__BackingField_11() const { return ___U3CCameraMetadataApiU3Ek__BackingField_11; }
	inline CameraMetadataApi_t4067407319 ** get_address_of_U3CCameraMetadataApiU3Ek__BackingField_11() { return &___U3CCameraMetadataApiU3Ek__BackingField_11; }
	inline void set_U3CCameraMetadataApiU3Ek__BackingField_11(CameraMetadataApi_t4067407319 * value)
	{
		___U3CCameraMetadataApiU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCameraMetadataApiU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CFrameApiU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(NativeSession_t1550589577, ___U3CFrameApiU3Ek__BackingField_12)); }
	inline FrameApi_t2778645972 * get_U3CFrameApiU3Ek__BackingField_12() const { return ___U3CFrameApiU3Ek__BackingField_12; }
	inline FrameApi_t2778645972 ** get_address_of_U3CFrameApiU3Ek__BackingField_12() { return &___U3CFrameApiU3Ek__BackingField_12; }
	inline void set_U3CFrameApiU3Ek__BackingField_12(FrameApi_t2778645972 * value)
	{
		___U3CFrameApiU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFrameApiU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CHitTestApiU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(NativeSession_t1550589577, ___U3CHitTestApiU3Ek__BackingField_13)); }
	inline HitTestApi_t363854212 * get_U3CHitTestApiU3Ek__BackingField_13() const { return ___U3CHitTestApiU3Ek__BackingField_13; }
	inline HitTestApi_t363854212 ** get_address_of_U3CHitTestApiU3Ek__BackingField_13() { return &___U3CHitTestApiU3Ek__BackingField_13; }
	inline void set_U3CHitTestApiU3Ek__BackingField_13(HitTestApi_t363854212 * value)
	{
		___U3CHitTestApiU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHitTestApiU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CImageApiU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(NativeSession_t1550589577, ___U3CImageApiU3Ek__BackingField_14)); }
	inline ImageApi_t1356767549 * get_U3CImageApiU3Ek__BackingField_14() const { return ___U3CImageApiU3Ek__BackingField_14; }
	inline ImageApi_t1356767549 ** get_address_of_U3CImageApiU3Ek__BackingField_14() { return &___U3CImageApiU3Ek__BackingField_14; }
	inline void set_U3CImageApiU3Ek__BackingField_14(ImageApi_t1356767549 * value)
	{
		___U3CImageApiU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CImageApiU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CLightEstimateApiU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(NativeSession_t1550589577, ___U3CLightEstimateApiU3Ek__BackingField_15)); }
	inline LightEstimateApi_t745071092 * get_U3CLightEstimateApiU3Ek__BackingField_15() const { return ___U3CLightEstimateApiU3Ek__BackingField_15; }
	inline LightEstimateApi_t745071092 ** get_address_of_U3CLightEstimateApiU3Ek__BackingField_15() { return &___U3CLightEstimateApiU3Ek__BackingField_15; }
	inline void set_U3CLightEstimateApiU3Ek__BackingField_15(LightEstimateApi_t745071092 * value)
	{
		___U3CLightEstimateApiU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLightEstimateApiU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CPlaneApiU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(NativeSession_t1550589577, ___U3CPlaneApiU3Ek__BackingField_16)); }
	inline PlaneApi_t1746941456 * get_U3CPlaneApiU3Ek__BackingField_16() const { return ___U3CPlaneApiU3Ek__BackingField_16; }
	inline PlaneApi_t1746941456 ** get_address_of_U3CPlaneApiU3Ek__BackingField_16() { return &___U3CPlaneApiU3Ek__BackingField_16; }
	inline void set_U3CPlaneApiU3Ek__BackingField_16(PlaneApi_t1746941456 * value)
	{
		___U3CPlaneApiU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPlaneApiU3Ek__BackingField_16), value);
	}

	inline static int32_t get_offset_of_U3CPointApiU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(NativeSession_t1550589577, ___U3CPointApiU3Ek__BackingField_17)); }
	inline PointApi_t3912385759 * get_U3CPointApiU3Ek__BackingField_17() const { return ___U3CPointApiU3Ek__BackingField_17; }
	inline PointApi_t3912385759 ** get_address_of_U3CPointApiU3Ek__BackingField_17() { return &___U3CPointApiU3Ek__BackingField_17; }
	inline void set_U3CPointApiU3Ek__BackingField_17(PointApi_t3912385759 * value)
	{
		___U3CPointApiU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPointApiU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_U3CPointCloudApiU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(NativeSession_t1550589577, ___U3CPointCloudApiU3Ek__BackingField_18)); }
	inline PointCloudApi_t650451515 * get_U3CPointCloudApiU3Ek__BackingField_18() const { return ___U3CPointCloudApiU3Ek__BackingField_18; }
	inline PointCloudApi_t650451515 ** get_address_of_U3CPointCloudApiU3Ek__BackingField_18() { return &___U3CPointCloudApiU3Ek__BackingField_18; }
	inline void set_U3CPointCloudApiU3Ek__BackingField_18(PointCloudApi_t650451515 * value)
	{
		___U3CPointCloudApiU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPointCloudApiU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_U3CPoseApiU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(NativeSession_t1550589577, ___U3CPoseApiU3Ek__BackingField_19)); }
	inline PoseApi_t3025584507 * get_U3CPoseApiU3Ek__BackingField_19() const { return ___U3CPoseApiU3Ek__BackingField_19; }
	inline PoseApi_t3025584507 ** get_address_of_U3CPoseApiU3Ek__BackingField_19() { return &___U3CPoseApiU3Ek__BackingField_19; }
	inline void set_U3CPoseApiU3Ek__BackingField_19(PoseApi_t3025584507 * value)
	{
		___U3CPoseApiU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPoseApiU3Ek__BackingField_19), value);
	}

	inline static int32_t get_offset_of_U3CSessionApiU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(NativeSession_t1550589577, ___U3CSessionApiU3Ek__BackingField_20)); }
	inline SessionApi_t3641277092 * get_U3CSessionApiU3Ek__BackingField_20() const { return ___U3CSessionApiU3Ek__BackingField_20; }
	inline SessionApi_t3641277092 ** get_address_of_U3CSessionApiU3Ek__BackingField_20() { return &___U3CSessionApiU3Ek__BackingField_20; }
	inline void set_U3CSessionApiU3Ek__BackingField_20(SessionApi_t3641277092 * value)
	{
		___U3CSessionApiU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSessionApiU3Ek__BackingField_20), value);
	}

	inline static int32_t get_offset_of_U3CSessionConfigApiU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(NativeSession_t1550589577, ___U3CSessionConfigApiU3Ek__BackingField_21)); }
	inline SessionConfigApi_t3119440216 * get_U3CSessionConfigApiU3Ek__BackingField_21() const { return ___U3CSessionConfigApiU3Ek__BackingField_21; }
	inline SessionConfigApi_t3119440216 ** get_address_of_U3CSessionConfigApiU3Ek__BackingField_21() { return &___U3CSessionConfigApiU3Ek__BackingField_21; }
	inline void set_U3CSessionConfigApiU3Ek__BackingField_21(SessionConfigApi_t3119440216 * value)
	{
		___U3CSessionConfigApiU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSessionConfigApiU3Ek__BackingField_21), value);
	}

	inline static int32_t get_offset_of_U3CTrackableApiU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(NativeSession_t1550589577, ___U3CTrackableApiU3Ek__BackingField_22)); }
	inline TrackableApi_t1100424269 * get_U3CTrackableApiU3Ek__BackingField_22() const { return ___U3CTrackableApiU3Ek__BackingField_22; }
	inline TrackableApi_t1100424269 ** get_address_of_U3CTrackableApiU3Ek__BackingField_22() { return &___U3CTrackableApiU3Ek__BackingField_22; }
	inline void set_U3CTrackableApiU3Ek__BackingField_22(TrackableApi_t1100424269 * value)
	{
		___U3CTrackableApiU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrackableApiU3Ek__BackingField_22), value);
	}

	inline static int32_t get_offset_of_U3CTrackableListApiU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(NativeSession_t1550589577, ___U3CTrackableListApiU3Ek__BackingField_23)); }
	inline TrackableListApi_t1931239134 * get_U3CTrackableListApiU3Ek__BackingField_23() const { return ___U3CTrackableListApiU3Ek__BackingField_23; }
	inline TrackableListApi_t1931239134 ** get_address_of_U3CTrackableListApiU3Ek__BackingField_23() { return &___U3CTrackableListApiU3Ek__BackingField_23; }
	inline void set_U3CTrackableListApiU3Ek__BackingField_23(TrackableListApi_t1931239134 * value)
	{
		___U3CTrackableListApiU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrackableListApiU3Ek__BackingField_23), value);
	}
};

struct NativeSession_t1550589577_StaticFields
{
public:
	// System.Boolean GoogleARCoreInternal.NativeSession::s_ReportedEngineType
	bool ___s_ReportedEngineType_0;

public:
	inline static int32_t get_offset_of_s_ReportedEngineType_0() { return static_cast<int32_t>(offsetof(NativeSession_t1550589577_StaticFields, ___s_ReportedEngineType_0)); }
	inline bool get_s_ReportedEngineType_0() const { return ___s_ReportedEngineType_0; }
	inline bool* get_address_of_s_ReportedEngineType_0() { return &___s_ReportedEngineType_0; }
	inline void set_s_ReportedEngineType_0(bool value)
	{
		___s_ReportedEngineType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVESESSION_T1550589577_H
#ifndef TOUCHPHASE_T72348083_H
#define TOUCHPHASE_T72348083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t72348083 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t72348083, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T72348083_H
#ifndef TOUCHTYPE_T2034578258_H
#define TOUCHTYPE_T2034578258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t2034578258 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchType_t2034578258, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T2034578258_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef APITRACKINGSTATE_T3318626383_H
#define APITRACKINGSTATE_T3318626383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ApiTrackingState
struct  ApiTrackingState_t3318626383 
{
public:
	// System.Int32 GoogleARCoreInternal.ApiTrackingState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ApiTrackingState_t3318626383, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APITRACKINGSTATE_T3318626383_H
#ifndef COMMANDTYPE_T2410186500_H
#define COMMANDTYPE_T2410186500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.ComputerVision.TextureReader/CommandType
struct  CommandType_t2410186500 
{
public:
	// System.Int32 GoogleARCore.Examples.ComputerVision.TextureReader/CommandType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CommandType_t2410186500, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDTYPE_T2410186500_H
#ifndef IMAGEFORMATTYPE_T3162659879_H
#define IMAGEFORMATTYPE_T3162659879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.ComputerVision.TextureReaderApi/ImageFormatType
struct  ImageFormatType_t3162659879 
{
public:
	// System.Int32 GoogleARCore.Examples.ComputerVision.TextureReaderApi/ImageFormatType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ImageFormatType_t3162659879, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEFORMATTYPE_T3162659879_H
#ifndef APIUPDATEMODE_T3440359817_H
#define APIUPDATEMODE_T3440359817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ApiUpdateMode
struct  ApiUpdateMode_t3440359817 
{
public:
	// System.Int32 GoogleARCoreInternal.ApiUpdateMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ApiUpdateMode_t3440359817, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIUPDATEMODE_T3440359817_H
#ifndef U3CUPDATELOOPU3EC__ITERATOR1_T1915294616_H
#define U3CUPDATELOOPU3EC__ITERATOR1_T1915294616_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.InstantPreviewManager/<UpdateLoop>c__Iterator1
struct  U3CUpdateLoopU3Ec__Iterator1_t1915294616  : public RuntimeObject
{
public:
	// System.IntPtr GoogleARCoreInternal.InstantPreviewManager/<UpdateLoop>c__Iterator1::<renderEventFunc>__0
	intptr_t ___U3CrenderEventFuncU3E__0_0;
	// System.Boolean GoogleARCoreInternal.InstantPreviewManager/<UpdateLoop>c__Iterator1::<shouldConvertToBgra>__0
	bool ___U3CshouldConvertToBgraU3E__0_1;
	// System.Boolean GoogleARCoreInternal.InstantPreviewManager/<UpdateLoop>c__Iterator1::<loggedAspectRatioWarning>__0
	bool ___U3CloggedAspectRatioWarningU3E__0_2;
	// System.Int32 GoogleARCoreInternal.InstantPreviewManager/<UpdateLoop>c__Iterator1::<currentWidth>__0
	int32_t ___U3CcurrentWidthU3E__0_3;
	// System.Int32 GoogleARCoreInternal.InstantPreviewManager/<UpdateLoop>c__Iterator1::<currentHeight>__0
	int32_t ___U3CcurrentHeightU3E__0_4;
	// System.Boolean GoogleARCoreInternal.InstantPreviewManager/<UpdateLoop>c__Iterator1::<needToStartActivity>__0
	bool ___U3CneedToStartActivityU3E__0_5;
	// System.Boolean GoogleARCoreInternal.InstantPreviewManager/<UpdateLoop>c__Iterator1::<prevFrameLandscape>__0
	bool ___U3CprevFrameLandscapeU3E__0_6;
	// UnityEngine.RenderTexture GoogleARCoreInternal.InstantPreviewManager/<UpdateLoop>c__Iterator1::<screenTexture>__0
	RenderTexture_t2108887433 * ___U3CscreenTextureU3E__0_7;
	// UnityEngine.RenderTexture GoogleARCoreInternal.InstantPreviewManager/<UpdateLoop>c__Iterator1::<targetTexture>__0
	RenderTexture_t2108887433 * ___U3CtargetTextureU3E__0_8;
	// UnityEngine.RenderTexture GoogleARCoreInternal.InstantPreviewManager/<UpdateLoop>c__Iterator1::<bgrTexture>__0
	RenderTexture_t2108887433 * ___U3CbgrTextureU3E__0_9;
	// System.Boolean GoogleARCoreInternal.InstantPreviewManager/<UpdateLoop>c__Iterator1::<curFrameLandscape>__1
	bool ___U3CcurFrameLandscapeU3E__1_10;
	// System.String GoogleARCoreInternal.InstantPreviewManager/<UpdateLoop>c__Iterator1::adbPath
	String_t* ___adbPath_11;
	// System.Int32 GoogleARCoreInternal.InstantPreviewManager/<UpdateLoop>c__Iterator1::<targetWidth>__1
	int32_t ___U3CtargetWidthU3E__1_12;
	// System.Int32 GoogleARCoreInternal.InstantPreviewManager/<UpdateLoop>c__Iterator1::<targetHeight>__1
	int32_t ___U3CtargetHeightU3E__1_13;
	// UnityEngine.Texture GoogleARCoreInternal.InstantPreviewManager/<UpdateLoop>c__Iterator1::<cameraTexture>__1
	Texture_t3661962703 * ___U3CcameraTextureU3E__1_14;
	// System.Object GoogleARCoreInternal.InstantPreviewManager/<UpdateLoop>c__Iterator1::$current
	RuntimeObject * ___U24current_15;
	// System.Boolean GoogleARCoreInternal.InstantPreviewManager/<UpdateLoop>c__Iterator1::$disposing
	bool ___U24disposing_16;
	// System.Int32 GoogleARCoreInternal.InstantPreviewManager/<UpdateLoop>c__Iterator1::$PC
	int32_t ___U24PC_17;

public:
	inline static int32_t get_offset_of_U3CrenderEventFuncU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUpdateLoopU3Ec__Iterator1_t1915294616, ___U3CrenderEventFuncU3E__0_0)); }
	inline intptr_t get_U3CrenderEventFuncU3E__0_0() const { return ___U3CrenderEventFuncU3E__0_0; }
	inline intptr_t* get_address_of_U3CrenderEventFuncU3E__0_0() { return &___U3CrenderEventFuncU3E__0_0; }
	inline void set_U3CrenderEventFuncU3E__0_0(intptr_t value)
	{
		___U3CrenderEventFuncU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CshouldConvertToBgraU3E__0_1() { return static_cast<int32_t>(offsetof(U3CUpdateLoopU3Ec__Iterator1_t1915294616, ___U3CshouldConvertToBgraU3E__0_1)); }
	inline bool get_U3CshouldConvertToBgraU3E__0_1() const { return ___U3CshouldConvertToBgraU3E__0_1; }
	inline bool* get_address_of_U3CshouldConvertToBgraU3E__0_1() { return &___U3CshouldConvertToBgraU3E__0_1; }
	inline void set_U3CshouldConvertToBgraU3E__0_1(bool value)
	{
		___U3CshouldConvertToBgraU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CloggedAspectRatioWarningU3E__0_2() { return static_cast<int32_t>(offsetof(U3CUpdateLoopU3Ec__Iterator1_t1915294616, ___U3CloggedAspectRatioWarningU3E__0_2)); }
	inline bool get_U3CloggedAspectRatioWarningU3E__0_2() const { return ___U3CloggedAspectRatioWarningU3E__0_2; }
	inline bool* get_address_of_U3CloggedAspectRatioWarningU3E__0_2() { return &___U3CloggedAspectRatioWarningU3E__0_2; }
	inline void set_U3CloggedAspectRatioWarningU3E__0_2(bool value)
	{
		___U3CloggedAspectRatioWarningU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentWidthU3E__0_3() { return static_cast<int32_t>(offsetof(U3CUpdateLoopU3Ec__Iterator1_t1915294616, ___U3CcurrentWidthU3E__0_3)); }
	inline int32_t get_U3CcurrentWidthU3E__0_3() const { return ___U3CcurrentWidthU3E__0_3; }
	inline int32_t* get_address_of_U3CcurrentWidthU3E__0_3() { return &___U3CcurrentWidthU3E__0_3; }
	inline void set_U3CcurrentWidthU3E__0_3(int32_t value)
	{
		___U3CcurrentWidthU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentHeightU3E__0_4() { return static_cast<int32_t>(offsetof(U3CUpdateLoopU3Ec__Iterator1_t1915294616, ___U3CcurrentHeightU3E__0_4)); }
	inline int32_t get_U3CcurrentHeightU3E__0_4() const { return ___U3CcurrentHeightU3E__0_4; }
	inline int32_t* get_address_of_U3CcurrentHeightU3E__0_4() { return &___U3CcurrentHeightU3E__0_4; }
	inline void set_U3CcurrentHeightU3E__0_4(int32_t value)
	{
		___U3CcurrentHeightU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CneedToStartActivityU3E__0_5() { return static_cast<int32_t>(offsetof(U3CUpdateLoopU3Ec__Iterator1_t1915294616, ___U3CneedToStartActivityU3E__0_5)); }
	inline bool get_U3CneedToStartActivityU3E__0_5() const { return ___U3CneedToStartActivityU3E__0_5; }
	inline bool* get_address_of_U3CneedToStartActivityU3E__0_5() { return &___U3CneedToStartActivityU3E__0_5; }
	inline void set_U3CneedToStartActivityU3E__0_5(bool value)
	{
		___U3CneedToStartActivityU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U3CprevFrameLandscapeU3E__0_6() { return static_cast<int32_t>(offsetof(U3CUpdateLoopU3Ec__Iterator1_t1915294616, ___U3CprevFrameLandscapeU3E__0_6)); }
	inline bool get_U3CprevFrameLandscapeU3E__0_6() const { return ___U3CprevFrameLandscapeU3E__0_6; }
	inline bool* get_address_of_U3CprevFrameLandscapeU3E__0_6() { return &___U3CprevFrameLandscapeU3E__0_6; }
	inline void set_U3CprevFrameLandscapeU3E__0_6(bool value)
	{
		___U3CprevFrameLandscapeU3E__0_6 = value;
	}

	inline static int32_t get_offset_of_U3CscreenTextureU3E__0_7() { return static_cast<int32_t>(offsetof(U3CUpdateLoopU3Ec__Iterator1_t1915294616, ___U3CscreenTextureU3E__0_7)); }
	inline RenderTexture_t2108887433 * get_U3CscreenTextureU3E__0_7() const { return ___U3CscreenTextureU3E__0_7; }
	inline RenderTexture_t2108887433 ** get_address_of_U3CscreenTextureU3E__0_7() { return &___U3CscreenTextureU3E__0_7; }
	inline void set_U3CscreenTextureU3E__0_7(RenderTexture_t2108887433 * value)
	{
		___U3CscreenTextureU3E__0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CscreenTextureU3E__0_7), value);
	}

	inline static int32_t get_offset_of_U3CtargetTextureU3E__0_8() { return static_cast<int32_t>(offsetof(U3CUpdateLoopU3Ec__Iterator1_t1915294616, ___U3CtargetTextureU3E__0_8)); }
	inline RenderTexture_t2108887433 * get_U3CtargetTextureU3E__0_8() const { return ___U3CtargetTextureU3E__0_8; }
	inline RenderTexture_t2108887433 ** get_address_of_U3CtargetTextureU3E__0_8() { return &___U3CtargetTextureU3E__0_8; }
	inline void set_U3CtargetTextureU3E__0_8(RenderTexture_t2108887433 * value)
	{
		___U3CtargetTextureU3E__0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtargetTextureU3E__0_8), value);
	}

	inline static int32_t get_offset_of_U3CbgrTextureU3E__0_9() { return static_cast<int32_t>(offsetof(U3CUpdateLoopU3Ec__Iterator1_t1915294616, ___U3CbgrTextureU3E__0_9)); }
	inline RenderTexture_t2108887433 * get_U3CbgrTextureU3E__0_9() const { return ___U3CbgrTextureU3E__0_9; }
	inline RenderTexture_t2108887433 ** get_address_of_U3CbgrTextureU3E__0_9() { return &___U3CbgrTextureU3E__0_9; }
	inline void set_U3CbgrTextureU3E__0_9(RenderTexture_t2108887433 * value)
	{
		___U3CbgrTextureU3E__0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbgrTextureU3E__0_9), value);
	}

	inline static int32_t get_offset_of_U3CcurFrameLandscapeU3E__1_10() { return static_cast<int32_t>(offsetof(U3CUpdateLoopU3Ec__Iterator1_t1915294616, ___U3CcurFrameLandscapeU3E__1_10)); }
	inline bool get_U3CcurFrameLandscapeU3E__1_10() const { return ___U3CcurFrameLandscapeU3E__1_10; }
	inline bool* get_address_of_U3CcurFrameLandscapeU3E__1_10() { return &___U3CcurFrameLandscapeU3E__1_10; }
	inline void set_U3CcurFrameLandscapeU3E__1_10(bool value)
	{
		___U3CcurFrameLandscapeU3E__1_10 = value;
	}

	inline static int32_t get_offset_of_adbPath_11() { return static_cast<int32_t>(offsetof(U3CUpdateLoopU3Ec__Iterator1_t1915294616, ___adbPath_11)); }
	inline String_t* get_adbPath_11() const { return ___adbPath_11; }
	inline String_t** get_address_of_adbPath_11() { return &___adbPath_11; }
	inline void set_adbPath_11(String_t* value)
	{
		___adbPath_11 = value;
		Il2CppCodeGenWriteBarrier((&___adbPath_11), value);
	}

	inline static int32_t get_offset_of_U3CtargetWidthU3E__1_12() { return static_cast<int32_t>(offsetof(U3CUpdateLoopU3Ec__Iterator1_t1915294616, ___U3CtargetWidthU3E__1_12)); }
	inline int32_t get_U3CtargetWidthU3E__1_12() const { return ___U3CtargetWidthU3E__1_12; }
	inline int32_t* get_address_of_U3CtargetWidthU3E__1_12() { return &___U3CtargetWidthU3E__1_12; }
	inline void set_U3CtargetWidthU3E__1_12(int32_t value)
	{
		___U3CtargetWidthU3E__1_12 = value;
	}

	inline static int32_t get_offset_of_U3CtargetHeightU3E__1_13() { return static_cast<int32_t>(offsetof(U3CUpdateLoopU3Ec__Iterator1_t1915294616, ___U3CtargetHeightU3E__1_13)); }
	inline int32_t get_U3CtargetHeightU3E__1_13() const { return ___U3CtargetHeightU3E__1_13; }
	inline int32_t* get_address_of_U3CtargetHeightU3E__1_13() { return &___U3CtargetHeightU3E__1_13; }
	inline void set_U3CtargetHeightU3E__1_13(int32_t value)
	{
		___U3CtargetHeightU3E__1_13 = value;
	}

	inline static int32_t get_offset_of_U3CcameraTextureU3E__1_14() { return static_cast<int32_t>(offsetof(U3CUpdateLoopU3Ec__Iterator1_t1915294616, ___U3CcameraTextureU3E__1_14)); }
	inline Texture_t3661962703 * get_U3CcameraTextureU3E__1_14() const { return ___U3CcameraTextureU3E__1_14; }
	inline Texture_t3661962703 ** get_address_of_U3CcameraTextureU3E__1_14() { return &___U3CcameraTextureU3E__1_14; }
	inline void set_U3CcameraTextureU3E__1_14(Texture_t3661962703 * value)
	{
		___U3CcameraTextureU3E__1_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcameraTextureU3E__1_14), value);
	}

	inline static int32_t get_offset_of_U24current_15() { return static_cast<int32_t>(offsetof(U3CUpdateLoopU3Ec__Iterator1_t1915294616, ___U24current_15)); }
	inline RuntimeObject * get_U24current_15() const { return ___U24current_15; }
	inline RuntimeObject ** get_address_of_U24current_15() { return &___U24current_15; }
	inline void set_U24current_15(RuntimeObject * value)
	{
		___U24current_15 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_15), value);
	}

	inline static int32_t get_offset_of_U24disposing_16() { return static_cast<int32_t>(offsetof(U3CUpdateLoopU3Ec__Iterator1_t1915294616, ___U24disposing_16)); }
	inline bool get_U24disposing_16() const { return ___U24disposing_16; }
	inline bool* get_address_of_U24disposing_16() { return &___U24disposing_16; }
	inline void set_U24disposing_16(bool value)
	{
		___U24disposing_16 = value;
	}

	inline static int32_t get_offset_of_U24PC_17() { return static_cast<int32_t>(offsetof(U3CUpdateLoopU3Ec__Iterator1_t1915294616, ___U24PC_17)); }
	inline int32_t get_U24PC_17() const { return ___U24PC_17; }
	inline int32_t* get_address_of_U24PC_17() { return &___U24PC_17; }
	inline void set_U24PC_17(int32_t value)
	{
		___U24PC_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATELOOPU3EC__ITERATOR1_T1915294616_H
#ifndef APPLICATIONMODE_T2292997937_H
#define APPLICATIONMODE_T2292997937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.CloudAnchor.CloudAnchorController/ApplicationMode
struct  ApplicationMode_t2292997937 
{
public:
	// System.Int32 GoogleARCore.Examples.CloudAnchor.CloudAnchorController/ApplicationMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ApplicationMode_t2292997937, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATIONMODE_T2292997937_H
#ifndef APIAPKINSTALLATIONSTATUS_T33526722_H
#define APIAPKINSTALLATIONSTATUS_T33526722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ApiApkInstallationStatus
struct  ApiApkInstallationStatus_t33526722 
{
public:
	// System.Int32 GoogleARCoreInternal.ApiApkInstallationStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ApiApkInstallationStatus_t33526722, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIAPKINSTALLATIONSTATUS_T33526722_H
#ifndef APIARSTATUS_T1919843852_H
#define APIARSTATUS_T1919843852_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ApiArStatus
struct  ApiArStatus_t1919843852 
{
public:
	// System.Int32 GoogleARCoreInternal.ApiArStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ApiArStatus_t1919843852, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIARSTATUS_T1919843852_H
#ifndef SAMPLEMODE_T259167292_H
#define SAMPLEMODE_T259167292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.ComputerVision.TextureReader/SampleMode
struct  SampleMode_t259167292 
{
public:
	// System.Int32 GoogleARCore.Examples.ComputerVision.TextureReader/SampleMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SampleMode_t259167292, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLEMODE_T259167292_H
#ifndef NDKCAMERAMETADATATYPE_T306478796_H
#define NDKCAMERAMETADATATYPE_T306478796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.NdkCameraMetadataType
struct  NdkCameraMetadataType_t306478796 
{
public:
	// System.Int32 GoogleARCoreInternal.NdkCameraMetadataType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NdkCameraMetadataType_t306478796, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NDKCAMERAMETADATATYPE_T306478796_H
#ifndef NDKCAMERASTATUS_T2797764638_H
#define NDKCAMERASTATUS_T2797764638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.NdkCameraStatus
struct  NdkCameraStatus_t2797764638 
{
public:
	// System.Int32 GoogleARCoreInternal.NdkCameraStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NdkCameraStatus_t2797764638, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NDKCAMERASTATUS_T2797764638_H
#ifndef APIAVAILABILITY_T1149954610_H
#define APIAVAILABILITY_T1149954610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ApiAvailability
struct  ApiAvailability_t1149954610 
{
public:
	// System.Int32 GoogleARCoreInternal.ApiAvailability::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ApiAvailability_t1149954610, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIAVAILABILITY_T1149954610_H
#ifndef APIPLANEFINDINGMODE_T590523534_H
#define APIPLANEFINDINGMODE_T590523534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ApiPlaneFindingMode
struct  ApiPlaneFindingMode_t590523534 
{
public:
	// System.Int32 GoogleARCoreInternal.ApiPlaneFindingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ApiPlaneFindingMode_t590523534, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIPLANEFINDINGMODE_T590523534_H
#ifndef APICLOUDANCHORSTATE_T449523374_H
#define APICLOUDANCHORSTATE_T449523374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.CrossPlatform.ApiCloudAnchorState
struct  ApiCloudAnchorState_t449523374 
{
public:
	// System.Int32 GoogleARCoreInternal.CrossPlatform.ApiCloudAnchorState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ApiCloudAnchorState_t449523374, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APICLOUDANCHORSTATE_T449523374_H
#ifndef APIDISPLAYUVCOORDS_T2116852691_H
#define APIDISPLAYUVCOORDS_T2116852691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ApiDisplayUvCoords
struct  ApiDisplayUvCoords_t2116852691 
{
public:
	// UnityEngine.Vector2 GoogleARCoreInternal.ApiDisplayUvCoords::TopLeft
	Vector2_t2156229523  ___TopLeft_1;
	// UnityEngine.Vector2 GoogleARCoreInternal.ApiDisplayUvCoords::TopRight
	Vector2_t2156229523  ___TopRight_2;
	// UnityEngine.Vector2 GoogleARCoreInternal.ApiDisplayUvCoords::BottomLeft
	Vector2_t2156229523  ___BottomLeft_3;
	// UnityEngine.Vector2 GoogleARCoreInternal.ApiDisplayUvCoords::BottomRight
	Vector2_t2156229523  ___BottomRight_4;

public:
	inline static int32_t get_offset_of_TopLeft_1() { return static_cast<int32_t>(offsetof(ApiDisplayUvCoords_t2116852691, ___TopLeft_1)); }
	inline Vector2_t2156229523  get_TopLeft_1() const { return ___TopLeft_1; }
	inline Vector2_t2156229523 * get_address_of_TopLeft_1() { return &___TopLeft_1; }
	inline void set_TopLeft_1(Vector2_t2156229523  value)
	{
		___TopLeft_1 = value;
	}

	inline static int32_t get_offset_of_TopRight_2() { return static_cast<int32_t>(offsetof(ApiDisplayUvCoords_t2116852691, ___TopRight_2)); }
	inline Vector2_t2156229523  get_TopRight_2() const { return ___TopRight_2; }
	inline Vector2_t2156229523 * get_address_of_TopRight_2() { return &___TopRight_2; }
	inline void set_TopRight_2(Vector2_t2156229523  value)
	{
		___TopRight_2 = value;
	}

	inline static int32_t get_offset_of_BottomLeft_3() { return static_cast<int32_t>(offsetof(ApiDisplayUvCoords_t2116852691, ___BottomLeft_3)); }
	inline Vector2_t2156229523  get_BottomLeft_3() const { return ___BottomLeft_3; }
	inline Vector2_t2156229523 * get_address_of_BottomLeft_3() { return &___BottomLeft_3; }
	inline void set_BottomLeft_3(Vector2_t2156229523  value)
	{
		___BottomLeft_3 = value;
	}

	inline static int32_t get_offset_of_BottomRight_4() { return static_cast<int32_t>(offsetof(ApiDisplayUvCoords_t2116852691, ___BottomRight_4)); }
	inline Vector2_t2156229523  get_BottomRight_4() const { return ___BottomRight_4; }
	inline Vector2_t2156229523 * get_address_of_BottomRight_4() { return &___BottomRight_4; }
	inline void set_BottomRight_4(Vector2_t2156229523  value)
	{
		___BottomRight_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIDISPLAYUVCOORDS_T2116852691_H
#ifndef APITRACKABLETYPE_T702053436_H
#define APITRACKABLETYPE_T702053436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ApiTrackableType
struct  ApiTrackableType_t702053436 
{
public:
	// System.Int32 GoogleARCoreInternal.ApiTrackableType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ApiTrackableType_t702053436, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APITRACKABLETYPE_T702053436_H
#ifndef APIFEATUREPOINTORIENTATIONMODE_T2490105243_H
#define APIFEATUREPOINTORIENTATIONMODE_T2490105243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ApiFeaturePointOrientationMode
struct  ApiFeaturePointOrientationMode_t2490105243 
{
public:
	// System.Int32 GoogleARCoreInternal.ApiFeaturePointOrientationMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ApiFeaturePointOrientationMode_t2490105243, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIFEATUREPOINTORIENTATIONMODE_T2490105243_H
#ifndef APILIGHTESTIMATESTATE_T4039747232_H
#define APILIGHTESTIMATESTATE_T4039747232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ApiLightEstimateState
struct  ApiLightEstimateState_t4039747232 
{
public:
	// System.Int32 GoogleARCoreInternal.ApiLightEstimateState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ApiLightEstimateState_t4039747232, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APILIGHTESTIMATESTATE_T4039747232_H
#ifndef APIPRESTOSTATUS_T902283214_H
#define APIPRESTOSTATUS_T902283214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ApiPrestoStatus
struct  ApiPrestoStatus_t902283214 
{
public:
	// System.Int32 GoogleARCoreInternal.ApiPrestoStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ApiPrestoStatus_t902283214, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIPRESTOSTATUS_T902283214_H
#ifndef APILIGHTESTIMATIONMODE_T4137554712_H
#define APILIGHTESTIMATIONMODE_T4137554712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ApiLightEstimationMode
struct  ApiLightEstimationMode_t4137554712 
{
public:
	// System.Int32 GoogleARCoreInternal.ApiLightEstimationMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ApiLightEstimationMode_t4137554712, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APILIGHTESTIMATIONMODE_T4137554712_H
#ifndef APICLOUDANCHORMODE_T1322772406_H
#define APICLOUDANCHORMODE_T1322772406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.CrossPlatform.ApiCloudAnchorMode
struct  ApiCloudAnchorMode_t1322772406 
{
public:
	// System.Int32 GoogleARCoreInternal.CrossPlatform.ApiCloudAnchorMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ApiCloudAnchorMode_t1322772406, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APICLOUDANCHORMODE_T1322772406_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef NETWORKCLIENT_T3758195968_H
#define NETWORKCLIENT_T3758195968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkClient
struct  NetworkClient_t3758195968  : public RuntimeObject
{
public:
	// System.Type UnityEngine.Networking.NetworkClient::m_NetworkConnectionClass
	Type_t * ___m_NetworkConnectionClass_0;
	// UnityEngine.Networking.HostTopology UnityEngine.Networking.NetworkClient::m_HostTopology
	HostTopology_t4059263395 * ___m_HostTopology_4;
	// System.Int32 UnityEngine.Networking.NetworkClient::m_HostPort
	int32_t ___m_HostPort_5;
	// System.Boolean UnityEngine.Networking.NetworkClient::m_UseSimulator
	bool ___m_UseSimulator_6;
	// System.Int32 UnityEngine.Networking.NetworkClient::m_SimulatedLatency
	int32_t ___m_SimulatedLatency_7;
	// System.Single UnityEngine.Networking.NetworkClient::m_PacketLoss
	float ___m_PacketLoss_8;
	// System.String UnityEngine.Networking.NetworkClient::m_ServerIp
	String_t* ___m_ServerIp_9;
	// System.Int32 UnityEngine.Networking.NetworkClient::m_ServerPort
	int32_t ___m_ServerPort_10;
	// System.Int32 UnityEngine.Networking.NetworkClient::m_ClientId
	int32_t ___m_ClientId_11;
	// System.Int32 UnityEngine.Networking.NetworkClient::m_ClientConnectionId
	int32_t ___m_ClientConnectionId_12;
	// System.Int32 UnityEngine.Networking.NetworkClient::m_StatResetTime
	int32_t ___m_StatResetTime_13;
	// System.Net.EndPoint UnityEngine.Networking.NetworkClient::m_RemoteEndPoint
	EndPoint_t982345378 * ___m_RemoteEndPoint_14;
	// UnityEngine.Networking.NetworkMessageHandlers UnityEngine.Networking.NetworkClient::m_MessageHandlers
	NetworkMessageHandlers_t82575973 * ___m_MessageHandlers_16;
	// UnityEngine.Networking.NetworkConnection UnityEngine.Networking.NetworkClient::m_Connection
	NetworkConnection_t2705220091 * ___m_Connection_17;
	// System.Byte[] UnityEngine.Networking.NetworkClient::m_MsgBuffer
	ByteU5BU5D_t4116647657* ___m_MsgBuffer_18;
	// UnityEngine.Networking.NetworkReader UnityEngine.Networking.NetworkClient::m_MsgReader
	NetworkReader_t1574750186 * ___m_MsgReader_19;
	// UnityEngine.Networking.NetworkClient/ConnectState UnityEngine.Networking.NetworkClient::m_AsyncConnect
	int32_t ___m_AsyncConnect_20;
	// System.String UnityEngine.Networking.NetworkClient::m_RequestedServerHost
	String_t* ___m_RequestedServerHost_21;

public:
	inline static int32_t get_offset_of_m_NetworkConnectionClass_0() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_NetworkConnectionClass_0)); }
	inline Type_t * get_m_NetworkConnectionClass_0() const { return ___m_NetworkConnectionClass_0; }
	inline Type_t ** get_address_of_m_NetworkConnectionClass_0() { return &___m_NetworkConnectionClass_0; }
	inline void set_m_NetworkConnectionClass_0(Type_t * value)
	{
		___m_NetworkConnectionClass_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_NetworkConnectionClass_0), value);
	}

	inline static int32_t get_offset_of_m_HostTopology_4() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_HostTopology_4)); }
	inline HostTopology_t4059263395 * get_m_HostTopology_4() const { return ___m_HostTopology_4; }
	inline HostTopology_t4059263395 ** get_address_of_m_HostTopology_4() { return &___m_HostTopology_4; }
	inline void set_m_HostTopology_4(HostTopology_t4059263395 * value)
	{
		___m_HostTopology_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_HostTopology_4), value);
	}

	inline static int32_t get_offset_of_m_HostPort_5() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_HostPort_5)); }
	inline int32_t get_m_HostPort_5() const { return ___m_HostPort_5; }
	inline int32_t* get_address_of_m_HostPort_5() { return &___m_HostPort_5; }
	inline void set_m_HostPort_5(int32_t value)
	{
		___m_HostPort_5 = value;
	}

	inline static int32_t get_offset_of_m_UseSimulator_6() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_UseSimulator_6)); }
	inline bool get_m_UseSimulator_6() const { return ___m_UseSimulator_6; }
	inline bool* get_address_of_m_UseSimulator_6() { return &___m_UseSimulator_6; }
	inline void set_m_UseSimulator_6(bool value)
	{
		___m_UseSimulator_6 = value;
	}

	inline static int32_t get_offset_of_m_SimulatedLatency_7() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_SimulatedLatency_7)); }
	inline int32_t get_m_SimulatedLatency_7() const { return ___m_SimulatedLatency_7; }
	inline int32_t* get_address_of_m_SimulatedLatency_7() { return &___m_SimulatedLatency_7; }
	inline void set_m_SimulatedLatency_7(int32_t value)
	{
		___m_SimulatedLatency_7 = value;
	}

	inline static int32_t get_offset_of_m_PacketLoss_8() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_PacketLoss_8)); }
	inline float get_m_PacketLoss_8() const { return ___m_PacketLoss_8; }
	inline float* get_address_of_m_PacketLoss_8() { return &___m_PacketLoss_8; }
	inline void set_m_PacketLoss_8(float value)
	{
		___m_PacketLoss_8 = value;
	}

	inline static int32_t get_offset_of_m_ServerIp_9() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_ServerIp_9)); }
	inline String_t* get_m_ServerIp_9() const { return ___m_ServerIp_9; }
	inline String_t** get_address_of_m_ServerIp_9() { return &___m_ServerIp_9; }
	inline void set_m_ServerIp_9(String_t* value)
	{
		___m_ServerIp_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_ServerIp_9), value);
	}

	inline static int32_t get_offset_of_m_ServerPort_10() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_ServerPort_10)); }
	inline int32_t get_m_ServerPort_10() const { return ___m_ServerPort_10; }
	inline int32_t* get_address_of_m_ServerPort_10() { return &___m_ServerPort_10; }
	inline void set_m_ServerPort_10(int32_t value)
	{
		___m_ServerPort_10 = value;
	}

	inline static int32_t get_offset_of_m_ClientId_11() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_ClientId_11)); }
	inline int32_t get_m_ClientId_11() const { return ___m_ClientId_11; }
	inline int32_t* get_address_of_m_ClientId_11() { return &___m_ClientId_11; }
	inline void set_m_ClientId_11(int32_t value)
	{
		___m_ClientId_11 = value;
	}

	inline static int32_t get_offset_of_m_ClientConnectionId_12() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_ClientConnectionId_12)); }
	inline int32_t get_m_ClientConnectionId_12() const { return ___m_ClientConnectionId_12; }
	inline int32_t* get_address_of_m_ClientConnectionId_12() { return &___m_ClientConnectionId_12; }
	inline void set_m_ClientConnectionId_12(int32_t value)
	{
		___m_ClientConnectionId_12 = value;
	}

	inline static int32_t get_offset_of_m_StatResetTime_13() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_StatResetTime_13)); }
	inline int32_t get_m_StatResetTime_13() const { return ___m_StatResetTime_13; }
	inline int32_t* get_address_of_m_StatResetTime_13() { return &___m_StatResetTime_13; }
	inline void set_m_StatResetTime_13(int32_t value)
	{
		___m_StatResetTime_13 = value;
	}

	inline static int32_t get_offset_of_m_RemoteEndPoint_14() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_RemoteEndPoint_14)); }
	inline EndPoint_t982345378 * get_m_RemoteEndPoint_14() const { return ___m_RemoteEndPoint_14; }
	inline EndPoint_t982345378 ** get_address_of_m_RemoteEndPoint_14() { return &___m_RemoteEndPoint_14; }
	inline void set_m_RemoteEndPoint_14(EndPoint_t982345378 * value)
	{
		___m_RemoteEndPoint_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_RemoteEndPoint_14), value);
	}

	inline static int32_t get_offset_of_m_MessageHandlers_16() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_MessageHandlers_16)); }
	inline NetworkMessageHandlers_t82575973 * get_m_MessageHandlers_16() const { return ___m_MessageHandlers_16; }
	inline NetworkMessageHandlers_t82575973 ** get_address_of_m_MessageHandlers_16() { return &___m_MessageHandlers_16; }
	inline void set_m_MessageHandlers_16(NetworkMessageHandlers_t82575973 * value)
	{
		___m_MessageHandlers_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_MessageHandlers_16), value);
	}

	inline static int32_t get_offset_of_m_Connection_17() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_Connection_17)); }
	inline NetworkConnection_t2705220091 * get_m_Connection_17() const { return ___m_Connection_17; }
	inline NetworkConnection_t2705220091 ** get_address_of_m_Connection_17() { return &___m_Connection_17; }
	inline void set_m_Connection_17(NetworkConnection_t2705220091 * value)
	{
		___m_Connection_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_Connection_17), value);
	}

	inline static int32_t get_offset_of_m_MsgBuffer_18() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_MsgBuffer_18)); }
	inline ByteU5BU5D_t4116647657* get_m_MsgBuffer_18() const { return ___m_MsgBuffer_18; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_MsgBuffer_18() { return &___m_MsgBuffer_18; }
	inline void set_m_MsgBuffer_18(ByteU5BU5D_t4116647657* value)
	{
		___m_MsgBuffer_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_MsgBuffer_18), value);
	}

	inline static int32_t get_offset_of_m_MsgReader_19() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_MsgReader_19)); }
	inline NetworkReader_t1574750186 * get_m_MsgReader_19() const { return ___m_MsgReader_19; }
	inline NetworkReader_t1574750186 ** get_address_of_m_MsgReader_19() { return &___m_MsgReader_19; }
	inline void set_m_MsgReader_19(NetworkReader_t1574750186 * value)
	{
		___m_MsgReader_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_MsgReader_19), value);
	}

	inline static int32_t get_offset_of_m_AsyncConnect_20() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_AsyncConnect_20)); }
	inline int32_t get_m_AsyncConnect_20() const { return ___m_AsyncConnect_20; }
	inline int32_t* get_address_of_m_AsyncConnect_20() { return &___m_AsyncConnect_20; }
	inline void set_m_AsyncConnect_20(int32_t value)
	{
		___m_AsyncConnect_20 = value;
	}

	inline static int32_t get_offset_of_m_RequestedServerHost_21() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968, ___m_RequestedServerHost_21)); }
	inline String_t* get_m_RequestedServerHost_21() const { return ___m_RequestedServerHost_21; }
	inline String_t** get_address_of_m_RequestedServerHost_21() { return &___m_RequestedServerHost_21; }
	inline void set_m_RequestedServerHost_21(String_t* value)
	{
		___m_RequestedServerHost_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_RequestedServerHost_21), value);
	}
};

struct NetworkClient_t3758195968_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkClient> UnityEngine.Networking.NetworkClient::s_Clients
	List_1_t935303414 * ___s_Clients_2;
	// System.Boolean UnityEngine.Networking.NetworkClient::s_IsActive
	bool ___s_IsActive_3;
	// UnityEngine.Networking.NetworkSystem.CRCMessage UnityEngine.Networking.NetworkClient::s_CRCMessage
	CRCMessage_t4148217304 * ___s_CRCMessage_15;
	// System.AsyncCallback UnityEngine.Networking.NetworkClient::<>f__mg$cache0
	AsyncCallback_t3962456242 * ___U3CU3Ef__mgU24cache0_22;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.NetworkClient::<>f__mg$cache1
	NetworkMessageDelegate_t360140524 * ___U3CU3Ef__mgU24cache1_23;

public:
	inline static int32_t get_offset_of_s_Clients_2() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968_StaticFields, ___s_Clients_2)); }
	inline List_1_t935303414 * get_s_Clients_2() const { return ___s_Clients_2; }
	inline List_1_t935303414 ** get_address_of_s_Clients_2() { return &___s_Clients_2; }
	inline void set_s_Clients_2(List_1_t935303414 * value)
	{
		___s_Clients_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Clients_2), value);
	}

	inline static int32_t get_offset_of_s_IsActive_3() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968_StaticFields, ___s_IsActive_3)); }
	inline bool get_s_IsActive_3() const { return ___s_IsActive_3; }
	inline bool* get_address_of_s_IsActive_3() { return &___s_IsActive_3; }
	inline void set_s_IsActive_3(bool value)
	{
		___s_IsActive_3 = value;
	}

	inline static int32_t get_offset_of_s_CRCMessage_15() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968_StaticFields, ___s_CRCMessage_15)); }
	inline CRCMessage_t4148217304 * get_s_CRCMessage_15() const { return ___s_CRCMessage_15; }
	inline CRCMessage_t4148217304 ** get_address_of_s_CRCMessage_15() { return &___s_CRCMessage_15; }
	inline void set_s_CRCMessage_15(CRCMessage_t4148217304 * value)
	{
		___s_CRCMessage_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_CRCMessage_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_22() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968_StaticFields, ___U3CU3Ef__mgU24cache0_22)); }
	inline AsyncCallback_t3962456242 * get_U3CU3Ef__mgU24cache0_22() const { return ___U3CU3Ef__mgU24cache0_22; }
	inline AsyncCallback_t3962456242 ** get_address_of_U3CU3Ef__mgU24cache0_22() { return &___U3CU3Ef__mgU24cache0_22; }
	inline void set_U3CU3Ef__mgU24cache0_22(AsyncCallback_t3962456242 * value)
	{
		___U3CU3Ef__mgU24cache0_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_23() { return static_cast<int32_t>(offsetof(NetworkClient_t3758195968_StaticFields, ___U3CU3Ef__mgU24cache1_23)); }
	inline NetworkMessageDelegate_t360140524 * get_U3CU3Ef__mgU24cache1_23() const { return ___U3CU3Ef__mgU24cache1_23; }
	inline NetworkMessageDelegate_t360140524 ** get_address_of_U3CU3Ef__mgU24cache1_23() { return &___U3CU3Ef__mgU24cache1_23; }
	inline void set_U3CU3Ef__mgU24cache1_23(NetworkMessageDelegate_t360140524 * value)
	{
		___U3CU3Ef__mgU24cache1_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKCLIENT_T3758195968_H
#ifndef NATIVETOUCH_T337753862_H
#define NATIVETOUCH_T337753862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.InstantPreviewInput/NativeTouch
struct  NativeTouch_t337753862 
{
public:
	// UnityEngine.TouchPhase GoogleARCore.InstantPreviewInput/NativeTouch::Phase
	int32_t ___Phase_0;
	// System.Single GoogleARCore.InstantPreviewInput/NativeTouch::X
	float ___X_1;
	// System.Single GoogleARCore.InstantPreviewInput/NativeTouch::Y
	float ___Y_2;
	// System.Single GoogleARCore.InstantPreviewInput/NativeTouch::Pressure
	float ___Pressure_3;
	// System.Int32 GoogleARCore.InstantPreviewInput/NativeTouch::Id
	int32_t ___Id_4;

public:
	inline static int32_t get_offset_of_Phase_0() { return static_cast<int32_t>(offsetof(NativeTouch_t337753862, ___Phase_0)); }
	inline int32_t get_Phase_0() const { return ___Phase_0; }
	inline int32_t* get_address_of_Phase_0() { return &___Phase_0; }
	inline void set_Phase_0(int32_t value)
	{
		___Phase_0 = value;
	}

	inline static int32_t get_offset_of_X_1() { return static_cast<int32_t>(offsetof(NativeTouch_t337753862, ___X_1)); }
	inline float get_X_1() const { return ___X_1; }
	inline float* get_address_of_X_1() { return &___X_1; }
	inline void set_X_1(float value)
	{
		___X_1 = value;
	}

	inline static int32_t get_offset_of_Y_2() { return static_cast<int32_t>(offsetof(NativeTouch_t337753862, ___Y_2)); }
	inline float get_Y_2() const { return ___Y_2; }
	inline float* get_address_of_Y_2() { return &___Y_2; }
	inline void set_Y_2(float value)
	{
		___Y_2 = value;
	}

	inline static int32_t get_offset_of_Pressure_3() { return static_cast<int32_t>(offsetof(NativeTouch_t337753862, ___Pressure_3)); }
	inline float get_Pressure_3() const { return ___Pressure_3; }
	inline float* get_address_of_Pressure_3() { return &___Pressure_3; }
	inline void set_Pressure_3(float value)
	{
		___Pressure_3 = value;
	}

	inline static int32_t get_offset_of_Id_4() { return static_cast<int32_t>(offsetof(NativeTouch_t337753862, ___Id_4)); }
	inline int32_t get_Id_4() const { return ___Id_4; }
	inline int32_t* get_address_of_Id_4() { return &___Id_4; }
	inline void set_Id_4(int32_t value)
	{
		___Id_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVETOUCH_T337753862_H
#ifndef NDKCAMERAMETADATA_T2368224975_H
#define NDKCAMERAMETADATA_T2368224975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.NdkCameraMetadata
struct  NdkCameraMetadata_t2368224975 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 GoogleARCoreInternal.NdkCameraMetadata::Tag
			int32_t ___Tag_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___Tag_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Type_1_OffsetPadding[4];
			// GoogleARCoreInternal.NdkCameraMetadataType GoogleARCoreInternal.NdkCameraMetadata::Type
			int32_t ___Type_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Type_1_OffsetPadding_forAlignmentOnly[4];
			int32_t ___Type_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Count_2_OffsetPadding[8];
			// System.Int32 GoogleARCoreInternal.NdkCameraMetadata::Count
			int32_t ___Count_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Count_2_OffsetPadding_forAlignmentOnly[8];
			int32_t ___Count_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Value_3_OffsetPadding[12];
			// System.IntPtr GoogleARCoreInternal.NdkCameraMetadata::Value
			intptr_t ___Value_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Value_3_OffsetPadding_forAlignmentOnly[12];
			intptr_t ___Value_3_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_Tag_0() { return static_cast<int32_t>(offsetof(NdkCameraMetadata_t2368224975, ___Tag_0)); }
	inline int32_t get_Tag_0() const { return ___Tag_0; }
	inline int32_t* get_address_of_Tag_0() { return &___Tag_0; }
	inline void set_Tag_0(int32_t value)
	{
		___Tag_0 = value;
	}

	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(NdkCameraMetadata_t2368224975, ___Type_1)); }
	inline int32_t get_Type_1() const { return ___Type_1; }
	inline int32_t* get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(int32_t value)
	{
		___Type_1 = value;
	}

	inline static int32_t get_offset_of_Count_2() { return static_cast<int32_t>(offsetof(NdkCameraMetadata_t2368224975, ___Count_2)); }
	inline int32_t get_Count_2() const { return ___Count_2; }
	inline int32_t* get_address_of_Count_2() { return &___Count_2; }
	inline void set_Count_2(int32_t value)
	{
		___Count_2 = value;
	}

	inline static int32_t get_offset_of_Value_3() { return static_cast<int32_t>(offsetof(NdkCameraMetadata_t2368224975, ___Value_3)); }
	inline intptr_t get_Value_3() const { return ___Value_3; }
	inline intptr_t* get_address_of_Value_3() { return &___Value_3; }
	inline void set_Value_3(intptr_t value)
	{
		___Value_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GoogleARCoreInternal.NdkCameraMetadata
struct NdkCameraMetadata_t2368224975_marshaled_pinvoke
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___Tag_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___Tag_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Type_1_OffsetPadding[4];
			int8_t ___Type_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Type_1_OffsetPadding_forAlignmentOnly[4];
			int8_t ___Type_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Count_2_OffsetPadding[8];
			int32_t ___Count_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Count_2_OffsetPadding_forAlignmentOnly[8];
			int32_t ___Count_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Value_3_OffsetPadding[12];
			intptr_t ___Value_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Value_3_OffsetPadding_forAlignmentOnly[12];
			intptr_t ___Value_3_forAlignmentOnly;
		};
	};
};
// Native definition for COM marshalling of GoogleARCoreInternal.NdkCameraMetadata
struct NdkCameraMetadata_t2368224975_marshaled_com
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___Tag_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___Tag_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Type_1_OffsetPadding[4];
			int8_t ___Type_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Type_1_OffsetPadding_forAlignmentOnly[4];
			int8_t ___Type_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Count_2_OffsetPadding[8];
			int32_t ___Count_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Count_2_OffsetPadding_forAlignmentOnly[8];
			int32_t ___Count_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___Value_3_OffsetPadding[12];
			intptr_t ___Value_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___Value_3_OffsetPadding_forAlignmentOnly[12];
			intptr_t ___Value_3_forAlignmentOnly;
		};
	};
};
#endif // NDKCAMERAMETADATA_T2368224975_H
#ifndef ANDROIDPERMISSIONSMANAGER_T3082189368_H
#define ANDROIDPERMISSIONSMANAGER_T3082189368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.AndroidPermissionsManager
struct  AndroidPermissionsManager_t3082189368  : public AndroidJavaProxy_t2835824643
{
public:

public:
};

struct AndroidPermissionsManager_t3082189368_StaticFields
{
public:
	// GoogleARCore.AndroidPermissionsManager GoogleARCore.AndroidPermissionsManager::s_Instance
	AndroidPermissionsManager_t3082189368 * ___s_Instance_4;
	// UnityEngine.AndroidJavaObject GoogleARCore.AndroidPermissionsManager::s_Activity
	AndroidJavaObject_t4131667876 * ___s_Activity_5;
	// UnityEngine.AndroidJavaObject GoogleARCore.AndroidPermissionsManager::s_PermissionService
	AndroidJavaObject_t4131667876 * ___s_PermissionService_6;
	// GoogleARCore.AsyncTask`1<GoogleARCore.AndroidPermissionsRequestResult> GoogleARCore.AndroidPermissionsManager::s_CurrentRequest
	AsyncTask_1_t3089477160 * ___s_CurrentRequest_7;
	// System.Action`1<GoogleARCore.AndroidPermissionsRequestResult> GoogleARCore.AndroidPermissionsManager::s_OnPermissionsRequestFinished
	Action_1_t4082770468 * ___s_OnPermissionsRequestFinished_8;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(AndroidPermissionsManager_t3082189368_StaticFields, ___s_Instance_4)); }
	inline AndroidPermissionsManager_t3082189368 * get_s_Instance_4() const { return ___s_Instance_4; }
	inline AndroidPermissionsManager_t3082189368 ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(AndroidPermissionsManager_t3082189368 * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}

	inline static int32_t get_offset_of_s_Activity_5() { return static_cast<int32_t>(offsetof(AndroidPermissionsManager_t3082189368_StaticFields, ___s_Activity_5)); }
	inline AndroidJavaObject_t4131667876 * get_s_Activity_5() const { return ___s_Activity_5; }
	inline AndroidJavaObject_t4131667876 ** get_address_of_s_Activity_5() { return &___s_Activity_5; }
	inline void set_s_Activity_5(AndroidJavaObject_t4131667876 * value)
	{
		___s_Activity_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_Activity_5), value);
	}

	inline static int32_t get_offset_of_s_PermissionService_6() { return static_cast<int32_t>(offsetof(AndroidPermissionsManager_t3082189368_StaticFields, ___s_PermissionService_6)); }
	inline AndroidJavaObject_t4131667876 * get_s_PermissionService_6() const { return ___s_PermissionService_6; }
	inline AndroidJavaObject_t4131667876 ** get_address_of_s_PermissionService_6() { return &___s_PermissionService_6; }
	inline void set_s_PermissionService_6(AndroidJavaObject_t4131667876 * value)
	{
		___s_PermissionService_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_PermissionService_6), value);
	}

	inline static int32_t get_offset_of_s_CurrentRequest_7() { return static_cast<int32_t>(offsetof(AndroidPermissionsManager_t3082189368_StaticFields, ___s_CurrentRequest_7)); }
	inline AsyncTask_1_t3089477160 * get_s_CurrentRequest_7() const { return ___s_CurrentRequest_7; }
	inline AsyncTask_1_t3089477160 ** get_address_of_s_CurrentRequest_7() { return &___s_CurrentRequest_7; }
	inline void set_s_CurrentRequest_7(AsyncTask_1_t3089477160 * value)
	{
		___s_CurrentRequest_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_CurrentRequest_7), value);
	}

	inline static int32_t get_offset_of_s_OnPermissionsRequestFinished_8() { return static_cast<int32_t>(offsetof(AndroidPermissionsManager_t3082189368_StaticFields, ___s_OnPermissionsRequestFinished_8)); }
	inline Action_1_t4082770468 * get_s_OnPermissionsRequestFinished_8() const { return ___s_OnPermissionsRequestFinished_8; }
	inline Action_1_t4082770468 ** get_address_of_s_OnPermissionsRequestFinished_8() { return &___s_OnPermissionsRequestFinished_8; }
	inline void set_s_OnPermissionsRequestFinished_8(Action_1_t4082770468 * value)
	{
		___s_OnPermissionsRequestFinished_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_OnPermissionsRequestFinished_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDPERMISSIONSMANAGER_T3082189368_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef APIPRESTOCONFIG_T2282708041_H
#define APIPRESTOCONFIG_T2282708041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCoreInternal.ApiPrestoConfig
struct  ApiPrestoConfig_t2282708041 
{
public:
	// GoogleARCoreInternal.ApiUpdateMode GoogleARCoreInternal.ApiPrestoConfig::UpdateMode
	int32_t ___UpdateMode_0;
	// GoogleARCoreInternal.ApiPlaneFindingMode GoogleARCoreInternal.ApiPrestoConfig::PlaneFindingMode
	int32_t ___PlaneFindingMode_1;
	// GoogleARCoreInternal.ApiLightEstimationMode GoogleARCoreInternal.ApiPrestoConfig::LightEstimationMode
	int32_t ___LightEstimationMode_2;
	// GoogleARCoreInternal.CrossPlatform.ApiCloudAnchorMode GoogleARCoreInternal.ApiPrestoConfig::CloudAnchorMode
	int32_t ___CloudAnchorMode_3;
	// System.IntPtr GoogleARCoreInternal.ApiPrestoConfig::AugmentedImageDatabaseBytes
	intptr_t ___AugmentedImageDatabaseBytes_4;
	// System.Int64 GoogleARCoreInternal.ApiPrestoConfig::AugmentedImageDatabaseSize
	int64_t ___AugmentedImageDatabaseSize_5;

public:
	inline static int32_t get_offset_of_UpdateMode_0() { return static_cast<int32_t>(offsetof(ApiPrestoConfig_t2282708041, ___UpdateMode_0)); }
	inline int32_t get_UpdateMode_0() const { return ___UpdateMode_0; }
	inline int32_t* get_address_of_UpdateMode_0() { return &___UpdateMode_0; }
	inline void set_UpdateMode_0(int32_t value)
	{
		___UpdateMode_0 = value;
	}

	inline static int32_t get_offset_of_PlaneFindingMode_1() { return static_cast<int32_t>(offsetof(ApiPrestoConfig_t2282708041, ___PlaneFindingMode_1)); }
	inline int32_t get_PlaneFindingMode_1() const { return ___PlaneFindingMode_1; }
	inline int32_t* get_address_of_PlaneFindingMode_1() { return &___PlaneFindingMode_1; }
	inline void set_PlaneFindingMode_1(int32_t value)
	{
		___PlaneFindingMode_1 = value;
	}

	inline static int32_t get_offset_of_LightEstimationMode_2() { return static_cast<int32_t>(offsetof(ApiPrestoConfig_t2282708041, ___LightEstimationMode_2)); }
	inline int32_t get_LightEstimationMode_2() const { return ___LightEstimationMode_2; }
	inline int32_t* get_address_of_LightEstimationMode_2() { return &___LightEstimationMode_2; }
	inline void set_LightEstimationMode_2(int32_t value)
	{
		___LightEstimationMode_2 = value;
	}

	inline static int32_t get_offset_of_CloudAnchorMode_3() { return static_cast<int32_t>(offsetof(ApiPrestoConfig_t2282708041, ___CloudAnchorMode_3)); }
	inline int32_t get_CloudAnchorMode_3() const { return ___CloudAnchorMode_3; }
	inline int32_t* get_address_of_CloudAnchorMode_3() { return &___CloudAnchorMode_3; }
	inline void set_CloudAnchorMode_3(int32_t value)
	{
		___CloudAnchorMode_3 = value;
	}

	inline static int32_t get_offset_of_AugmentedImageDatabaseBytes_4() { return static_cast<int32_t>(offsetof(ApiPrestoConfig_t2282708041, ___AugmentedImageDatabaseBytes_4)); }
	inline intptr_t get_AugmentedImageDatabaseBytes_4() const { return ___AugmentedImageDatabaseBytes_4; }
	inline intptr_t* get_address_of_AugmentedImageDatabaseBytes_4() { return &___AugmentedImageDatabaseBytes_4; }
	inline void set_AugmentedImageDatabaseBytes_4(intptr_t value)
	{
		___AugmentedImageDatabaseBytes_4 = value;
	}

	inline static int32_t get_offset_of_AugmentedImageDatabaseSize_5() { return static_cast<int32_t>(offsetof(ApiPrestoConfig_t2282708041, ___AugmentedImageDatabaseSize_5)); }
	inline int64_t get_AugmentedImageDatabaseSize_5() const { return ___AugmentedImageDatabaseSize_5; }
	inline int64_t* get_address_of_AugmentedImageDatabaseSize_5() { return &___AugmentedImageDatabaseSize_5; }
	inline void set_AugmentedImageDatabaseSize_5(int64_t value)
	{
		___AugmentedImageDatabaseSize_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIPRESTOCONFIG_T2282708041_H
#ifndef TOUCH_T1921856868_H
#define TOUCH_T1921856868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t1921856868 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_t2156229523  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_t2156229523  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_t2156229523  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Position_1)); }
	inline Vector2_t2156229523  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_t2156229523 * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_t2156229523  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_RawPosition_2)); }
	inline Vector2_t2156229523  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_t2156229523 * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_t2156229523  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_PositionDelta_3)); }
	inline Vector2_t2156229523  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_t2156229523 * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_t2156229523  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T1921856868_H
#ifndef GETANCHORIDFROMROOMDELEGATE_T3759139021_H
#define GETANCHORIDFROMROOMDELEGATE_T3759139021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.CloudAnchor.RoomSharingClient/GetAnchorIdFromRoomDelegate
struct  GetAnchorIdFromRoomDelegate_t3759139021  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANCHORIDFROMROOMDELEGATE_T3759139021_H
#ifndef ROOMSHARINGCLIENT_T3825783562_H
#define ROOMSHARINGCLIENT_T3825783562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.CloudAnchor.RoomSharingClient
struct  RoomSharingClient_t3825783562  : public NetworkClient_t3758195968
{
public:
	// GoogleARCore.Examples.CloudAnchor.RoomSharingClient/GetAnchorIdFromRoomDelegate GoogleARCore.Examples.CloudAnchor.RoomSharingClient::m_GetAnchorIdFromRoomCallback
	GetAnchorIdFromRoomDelegate_t3759139021 * ___m_GetAnchorIdFromRoomCallback_24;
	// System.Int32 GoogleARCore.Examples.CloudAnchor.RoomSharingClient::m_RoomId
	int32_t ___m_RoomId_25;

public:
	inline static int32_t get_offset_of_m_GetAnchorIdFromRoomCallback_24() { return static_cast<int32_t>(offsetof(RoomSharingClient_t3825783562, ___m_GetAnchorIdFromRoomCallback_24)); }
	inline GetAnchorIdFromRoomDelegate_t3759139021 * get_m_GetAnchorIdFromRoomCallback_24() const { return ___m_GetAnchorIdFromRoomCallback_24; }
	inline GetAnchorIdFromRoomDelegate_t3759139021 ** get_address_of_m_GetAnchorIdFromRoomCallback_24() { return &___m_GetAnchorIdFromRoomCallback_24; }
	inline void set_m_GetAnchorIdFromRoomCallback_24(GetAnchorIdFromRoomDelegate_t3759139021 * value)
	{
		___m_GetAnchorIdFromRoomCallback_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_GetAnchorIdFromRoomCallback_24), value);
	}

	inline static int32_t get_offset_of_m_RoomId_25() { return static_cast<int32_t>(offsetof(RoomSharingClient_t3825783562, ___m_RoomId_25)); }
	inline int32_t get_m_RoomId_25() const { return ___m_RoomId_25; }
	inline int32_t* get_address_of_m_RoomId_25() { return &___m_RoomId_25; }
	inline void set_m_RoomId_25(int32_t value)
	{
		___m_RoomId_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOMSHARINGCLIENT_T3825783562_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef U3CUPDATEU3EC__ANONSTOREY0_T3581202692_H
#define U3CUPDATEU3EC__ANONSTOREY0_T3581202692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.InstantPreviewInput/<Update>c__AnonStorey0
struct  U3CUpdateU3Ec__AnonStorey0_t3581202692  : public RuntimeObject
{
public:
	// UnityEngine.Touch GoogleARCore.InstantPreviewInput/<Update>c__AnonStorey0::newTouch
	Touch_t1921856868  ___newTouch_0;

public:
	inline static int32_t get_offset_of_newTouch_0() { return static_cast<int32_t>(offsetof(U3CUpdateU3Ec__AnonStorey0_t3581202692, ___newTouch_0)); }
	inline Touch_t1921856868  get_newTouch_0() const { return ___newTouch_0; }
	inline Touch_t1921856868 * get_address_of_newTouch_0() { return &___newTouch_0; }
	inline void set_newTouch_0(Touch_t1921856868  value)
	{
		___newTouch_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEU3EC__ANONSTOREY0_T3581202692_H
#ifndef ONIMAGEAVAILABLECALLBACKFUNC_T2664114036_H
#define ONIMAGEAVAILABLECALLBACKFUNC_T2664114036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.ComputerVision.TextureReader/OnImageAvailableCallbackFunc
struct  OnImageAvailableCallbackFunc_t2664114036  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONIMAGEAVAILABLECALLBACKFUNC_T2664114036_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef COMPUTERVISIONCONTROLLER_T3046897977_H
#define COMPUTERVISIONCONTROLLER_T3046897977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.ComputerVision.ComputerVisionController
struct  ComputerVisionController_t3046897977  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image GoogleARCore.Examples.ComputerVision.ComputerVisionController::EdgeDetectionBackgroundImage
	Image_t2670269651 * ___EdgeDetectionBackgroundImage_2;
	// System.Boolean GoogleARCore.Examples.ComputerVision.ComputerVisionController::UseCustomResolutionImage
	bool ___UseCustomResolutionImage_3;
	// UnityEngine.UI.Text GoogleARCore.Examples.ComputerVision.ComputerVisionController::CameraIntrinsicsOutput
	Text_t1901882714 * ___CameraIntrinsicsOutput_4;
	// System.Byte[] GoogleARCore.Examples.ComputerVision.ComputerVisionController::m_EdgeDetectionResultImage
	ByteU5BU5D_t4116647657* ___m_EdgeDetectionResultImage_5;
	// UnityEngine.Texture2D GoogleARCore.Examples.ComputerVision.ComputerVisionController::m_EdgeDetectionBackgroundTexture
	Texture2D_t3840446185 * ___m_EdgeDetectionBackgroundTexture_6;
	// GoogleARCore.DisplayUvCoords GoogleARCore.Examples.ComputerVision.ComputerVisionController::m_CameraImageToDisplayUvTransformation
	DisplayUvCoords_t972795656  ___m_CameraImageToDisplayUvTransformation_7;
	// GoogleARCore.Examples.ComputerVision.TextureReader GoogleARCore.Examples.ComputerVision.ComputerVisionController::m_CachedTextureReader
	TextureReader_t2342948801 * ___m_CachedTextureReader_8;
	// UnityEngine.ScreenOrientation GoogleARCore.Examples.ComputerVision.ComputerVisionController::m_CachedOrientation
	int32_t ___m_CachedOrientation_9;
	// UnityEngine.Vector2 GoogleARCore.Examples.ComputerVision.ComputerVisionController::m_CachedScreenDimensions
	Vector2_t2156229523  ___m_CachedScreenDimensions_10;
	// System.Boolean GoogleARCore.Examples.ComputerVision.ComputerVisionController::m_IsQuitting
	bool ___m_IsQuitting_11;

public:
	inline static int32_t get_offset_of_EdgeDetectionBackgroundImage_2() { return static_cast<int32_t>(offsetof(ComputerVisionController_t3046897977, ___EdgeDetectionBackgroundImage_2)); }
	inline Image_t2670269651 * get_EdgeDetectionBackgroundImage_2() const { return ___EdgeDetectionBackgroundImage_2; }
	inline Image_t2670269651 ** get_address_of_EdgeDetectionBackgroundImage_2() { return &___EdgeDetectionBackgroundImage_2; }
	inline void set_EdgeDetectionBackgroundImage_2(Image_t2670269651 * value)
	{
		___EdgeDetectionBackgroundImage_2 = value;
		Il2CppCodeGenWriteBarrier((&___EdgeDetectionBackgroundImage_2), value);
	}

	inline static int32_t get_offset_of_UseCustomResolutionImage_3() { return static_cast<int32_t>(offsetof(ComputerVisionController_t3046897977, ___UseCustomResolutionImage_3)); }
	inline bool get_UseCustomResolutionImage_3() const { return ___UseCustomResolutionImage_3; }
	inline bool* get_address_of_UseCustomResolutionImage_3() { return &___UseCustomResolutionImage_3; }
	inline void set_UseCustomResolutionImage_3(bool value)
	{
		___UseCustomResolutionImage_3 = value;
	}

	inline static int32_t get_offset_of_CameraIntrinsicsOutput_4() { return static_cast<int32_t>(offsetof(ComputerVisionController_t3046897977, ___CameraIntrinsicsOutput_4)); }
	inline Text_t1901882714 * get_CameraIntrinsicsOutput_4() const { return ___CameraIntrinsicsOutput_4; }
	inline Text_t1901882714 ** get_address_of_CameraIntrinsicsOutput_4() { return &___CameraIntrinsicsOutput_4; }
	inline void set_CameraIntrinsicsOutput_4(Text_t1901882714 * value)
	{
		___CameraIntrinsicsOutput_4 = value;
		Il2CppCodeGenWriteBarrier((&___CameraIntrinsicsOutput_4), value);
	}

	inline static int32_t get_offset_of_m_EdgeDetectionResultImage_5() { return static_cast<int32_t>(offsetof(ComputerVisionController_t3046897977, ___m_EdgeDetectionResultImage_5)); }
	inline ByteU5BU5D_t4116647657* get_m_EdgeDetectionResultImage_5() const { return ___m_EdgeDetectionResultImage_5; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_EdgeDetectionResultImage_5() { return &___m_EdgeDetectionResultImage_5; }
	inline void set_m_EdgeDetectionResultImage_5(ByteU5BU5D_t4116647657* value)
	{
		___m_EdgeDetectionResultImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_EdgeDetectionResultImage_5), value);
	}

	inline static int32_t get_offset_of_m_EdgeDetectionBackgroundTexture_6() { return static_cast<int32_t>(offsetof(ComputerVisionController_t3046897977, ___m_EdgeDetectionBackgroundTexture_6)); }
	inline Texture2D_t3840446185 * get_m_EdgeDetectionBackgroundTexture_6() const { return ___m_EdgeDetectionBackgroundTexture_6; }
	inline Texture2D_t3840446185 ** get_address_of_m_EdgeDetectionBackgroundTexture_6() { return &___m_EdgeDetectionBackgroundTexture_6; }
	inline void set_m_EdgeDetectionBackgroundTexture_6(Texture2D_t3840446185 * value)
	{
		___m_EdgeDetectionBackgroundTexture_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_EdgeDetectionBackgroundTexture_6), value);
	}

	inline static int32_t get_offset_of_m_CameraImageToDisplayUvTransformation_7() { return static_cast<int32_t>(offsetof(ComputerVisionController_t3046897977, ___m_CameraImageToDisplayUvTransformation_7)); }
	inline DisplayUvCoords_t972795656  get_m_CameraImageToDisplayUvTransformation_7() const { return ___m_CameraImageToDisplayUvTransformation_7; }
	inline DisplayUvCoords_t972795656 * get_address_of_m_CameraImageToDisplayUvTransformation_7() { return &___m_CameraImageToDisplayUvTransformation_7; }
	inline void set_m_CameraImageToDisplayUvTransformation_7(DisplayUvCoords_t972795656  value)
	{
		___m_CameraImageToDisplayUvTransformation_7 = value;
	}

	inline static int32_t get_offset_of_m_CachedTextureReader_8() { return static_cast<int32_t>(offsetof(ComputerVisionController_t3046897977, ___m_CachedTextureReader_8)); }
	inline TextureReader_t2342948801 * get_m_CachedTextureReader_8() const { return ___m_CachedTextureReader_8; }
	inline TextureReader_t2342948801 ** get_address_of_m_CachedTextureReader_8() { return &___m_CachedTextureReader_8; }
	inline void set_m_CachedTextureReader_8(TextureReader_t2342948801 * value)
	{
		___m_CachedTextureReader_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachedTextureReader_8), value);
	}

	inline static int32_t get_offset_of_m_CachedOrientation_9() { return static_cast<int32_t>(offsetof(ComputerVisionController_t3046897977, ___m_CachedOrientation_9)); }
	inline int32_t get_m_CachedOrientation_9() const { return ___m_CachedOrientation_9; }
	inline int32_t* get_address_of_m_CachedOrientation_9() { return &___m_CachedOrientation_9; }
	inline void set_m_CachedOrientation_9(int32_t value)
	{
		___m_CachedOrientation_9 = value;
	}

	inline static int32_t get_offset_of_m_CachedScreenDimensions_10() { return static_cast<int32_t>(offsetof(ComputerVisionController_t3046897977, ___m_CachedScreenDimensions_10)); }
	inline Vector2_t2156229523  get_m_CachedScreenDimensions_10() const { return ___m_CachedScreenDimensions_10; }
	inline Vector2_t2156229523 * get_address_of_m_CachedScreenDimensions_10() { return &___m_CachedScreenDimensions_10; }
	inline void set_m_CachedScreenDimensions_10(Vector2_t2156229523  value)
	{
		___m_CachedScreenDimensions_10 = value;
	}

	inline static int32_t get_offset_of_m_IsQuitting_11() { return static_cast<int32_t>(offsetof(ComputerVisionController_t3046897977, ___m_IsQuitting_11)); }
	inline bool get_m_IsQuitting_11() const { return ___m_IsQuitting_11; }
	inline bool* get_address_of_m_IsQuitting_11() { return &___m_IsQuitting_11; }
	inline void set_m_IsQuitting_11(bool value)
	{
		___m_IsQuitting_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPUTERVISIONCONTROLLER_T3046897977_H
#ifndef POINTCLOUDVISUALIZER_T1422988745_H
#define POINTCLOUDVISUALIZER_T1422988745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.Common.PointcloudVisualizer
struct  PointcloudVisualizer_t1422988745  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Mesh GoogleARCore.Examples.Common.PointcloudVisualizer::m_Mesh
	Mesh_t3648964284 * ___m_Mesh_3;
	// UnityEngine.Vector3[] GoogleARCore.Examples.Common.PointcloudVisualizer::m_Points
	Vector3U5BU5D_t1718750761* ___m_Points_4;
	// System.Boolean GoogleARCore.Examples.Common.PointcloudVisualizer::isVisualize
	bool ___isVisualize_5;

public:
	inline static int32_t get_offset_of_m_Mesh_3() { return static_cast<int32_t>(offsetof(PointcloudVisualizer_t1422988745, ___m_Mesh_3)); }
	inline Mesh_t3648964284 * get_m_Mesh_3() const { return ___m_Mesh_3; }
	inline Mesh_t3648964284 ** get_address_of_m_Mesh_3() { return &___m_Mesh_3; }
	inline void set_m_Mesh_3(Mesh_t3648964284 * value)
	{
		___m_Mesh_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Mesh_3), value);
	}

	inline static int32_t get_offset_of_m_Points_4() { return static_cast<int32_t>(offsetof(PointcloudVisualizer_t1422988745, ___m_Points_4)); }
	inline Vector3U5BU5D_t1718750761* get_m_Points_4() const { return ___m_Points_4; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Points_4() { return &___m_Points_4; }
	inline void set_m_Points_4(Vector3U5BU5D_t1718750761* value)
	{
		___m_Points_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Points_4), value);
	}

	inline static int32_t get_offset_of_isVisualize_5() { return static_cast<int32_t>(offsetof(PointcloudVisualizer_t1422988745, ___isVisualize_5)); }
	inline bool get_isVisualize_5() const { return ___isVisualize_5; }
	inline bool* get_address_of_isVisualize_5() { return &___isVisualize_5; }
	inline void set_isVisualize_5(bool value)
	{
		___isVisualize_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTCLOUDVISUALIZER_T1422988745_H
#ifndef DETECTEDPLANEVISUALIZER_T2496913565_H
#define DETECTEDPLANEVISUALIZER_T2496913565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.Common.DetectedPlaneVisualizer
struct  DetectedPlaneVisualizer_t2496913565  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color[] GoogleARCore.Examples.Common.DetectedPlaneVisualizer::k_PlaneColors
	ColorU5BU5D_t941916413* ___k_PlaneColors_3;
	// GoogleARCore.DetectedPlane GoogleARCore.Examples.Common.DetectedPlaneVisualizer::m_DetectedPlane
	DetectedPlane_t4062129378 * ___m_DetectedPlane_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> GoogleARCore.Examples.Common.DetectedPlaneVisualizer::m_PreviousFrameMeshVertices
	List_1_t899420910 * ___m_PreviousFrameMeshVertices_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> GoogleARCore.Examples.Common.DetectedPlaneVisualizer::m_MeshVertices
	List_1_t899420910 * ___m_MeshVertices_6;
	// UnityEngine.Vector3 GoogleARCore.Examples.Common.DetectedPlaneVisualizer::m_PlaneCenter
	Vector3_t3722313464  ___m_PlaneCenter_7;
	// System.Collections.Generic.List`1<UnityEngine.Color> GoogleARCore.Examples.Common.DetectedPlaneVisualizer::m_MeshColors
	List_1_t4027761066 * ___m_MeshColors_8;
	// System.Collections.Generic.List`1<System.Int32> GoogleARCore.Examples.Common.DetectedPlaneVisualizer::m_MeshIndices
	List_1_t128053199 * ___m_MeshIndices_9;
	// UnityEngine.Mesh GoogleARCore.Examples.Common.DetectedPlaneVisualizer::m_Mesh
	Mesh_t3648964284 * ___m_Mesh_10;
	// UnityEngine.MeshRenderer GoogleARCore.Examples.Common.DetectedPlaneVisualizer::m_MeshRenderer
	MeshRenderer_t587009260 * ___m_MeshRenderer_11;

public:
	inline static int32_t get_offset_of_k_PlaneColors_3() { return static_cast<int32_t>(offsetof(DetectedPlaneVisualizer_t2496913565, ___k_PlaneColors_3)); }
	inline ColorU5BU5D_t941916413* get_k_PlaneColors_3() const { return ___k_PlaneColors_3; }
	inline ColorU5BU5D_t941916413** get_address_of_k_PlaneColors_3() { return &___k_PlaneColors_3; }
	inline void set_k_PlaneColors_3(ColorU5BU5D_t941916413* value)
	{
		___k_PlaneColors_3 = value;
		Il2CppCodeGenWriteBarrier((&___k_PlaneColors_3), value);
	}

	inline static int32_t get_offset_of_m_DetectedPlane_4() { return static_cast<int32_t>(offsetof(DetectedPlaneVisualizer_t2496913565, ___m_DetectedPlane_4)); }
	inline DetectedPlane_t4062129378 * get_m_DetectedPlane_4() const { return ___m_DetectedPlane_4; }
	inline DetectedPlane_t4062129378 ** get_address_of_m_DetectedPlane_4() { return &___m_DetectedPlane_4; }
	inline void set_m_DetectedPlane_4(DetectedPlane_t4062129378 * value)
	{
		___m_DetectedPlane_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_DetectedPlane_4), value);
	}

	inline static int32_t get_offset_of_m_PreviousFrameMeshVertices_5() { return static_cast<int32_t>(offsetof(DetectedPlaneVisualizer_t2496913565, ___m_PreviousFrameMeshVertices_5)); }
	inline List_1_t899420910 * get_m_PreviousFrameMeshVertices_5() const { return ___m_PreviousFrameMeshVertices_5; }
	inline List_1_t899420910 ** get_address_of_m_PreviousFrameMeshVertices_5() { return &___m_PreviousFrameMeshVertices_5; }
	inline void set_m_PreviousFrameMeshVertices_5(List_1_t899420910 * value)
	{
		___m_PreviousFrameMeshVertices_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PreviousFrameMeshVertices_5), value);
	}

	inline static int32_t get_offset_of_m_MeshVertices_6() { return static_cast<int32_t>(offsetof(DetectedPlaneVisualizer_t2496913565, ___m_MeshVertices_6)); }
	inline List_1_t899420910 * get_m_MeshVertices_6() const { return ___m_MeshVertices_6; }
	inline List_1_t899420910 ** get_address_of_m_MeshVertices_6() { return &___m_MeshVertices_6; }
	inline void set_m_MeshVertices_6(List_1_t899420910 * value)
	{
		___m_MeshVertices_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_MeshVertices_6), value);
	}

	inline static int32_t get_offset_of_m_PlaneCenter_7() { return static_cast<int32_t>(offsetof(DetectedPlaneVisualizer_t2496913565, ___m_PlaneCenter_7)); }
	inline Vector3_t3722313464  get_m_PlaneCenter_7() const { return ___m_PlaneCenter_7; }
	inline Vector3_t3722313464 * get_address_of_m_PlaneCenter_7() { return &___m_PlaneCenter_7; }
	inline void set_m_PlaneCenter_7(Vector3_t3722313464  value)
	{
		___m_PlaneCenter_7 = value;
	}

	inline static int32_t get_offset_of_m_MeshColors_8() { return static_cast<int32_t>(offsetof(DetectedPlaneVisualizer_t2496913565, ___m_MeshColors_8)); }
	inline List_1_t4027761066 * get_m_MeshColors_8() const { return ___m_MeshColors_8; }
	inline List_1_t4027761066 ** get_address_of_m_MeshColors_8() { return &___m_MeshColors_8; }
	inline void set_m_MeshColors_8(List_1_t4027761066 * value)
	{
		___m_MeshColors_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_MeshColors_8), value);
	}

	inline static int32_t get_offset_of_m_MeshIndices_9() { return static_cast<int32_t>(offsetof(DetectedPlaneVisualizer_t2496913565, ___m_MeshIndices_9)); }
	inline List_1_t128053199 * get_m_MeshIndices_9() const { return ___m_MeshIndices_9; }
	inline List_1_t128053199 ** get_address_of_m_MeshIndices_9() { return &___m_MeshIndices_9; }
	inline void set_m_MeshIndices_9(List_1_t128053199 * value)
	{
		___m_MeshIndices_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_MeshIndices_9), value);
	}

	inline static int32_t get_offset_of_m_Mesh_10() { return static_cast<int32_t>(offsetof(DetectedPlaneVisualizer_t2496913565, ___m_Mesh_10)); }
	inline Mesh_t3648964284 * get_m_Mesh_10() const { return ___m_Mesh_10; }
	inline Mesh_t3648964284 ** get_address_of_m_Mesh_10() { return &___m_Mesh_10; }
	inline void set_m_Mesh_10(Mesh_t3648964284 * value)
	{
		___m_Mesh_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Mesh_10), value);
	}

	inline static int32_t get_offset_of_m_MeshRenderer_11() { return static_cast<int32_t>(offsetof(DetectedPlaneVisualizer_t2496913565, ___m_MeshRenderer_11)); }
	inline MeshRenderer_t587009260 * get_m_MeshRenderer_11() const { return ___m_MeshRenderer_11; }
	inline MeshRenderer_t587009260 ** get_address_of_m_MeshRenderer_11() { return &___m_MeshRenderer_11; }
	inline void set_m_MeshRenderer_11(MeshRenderer_t587009260 * value)
	{
		___m_MeshRenderer_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_MeshRenderer_11), value);
	}
};

struct DetectedPlaneVisualizer_t2496913565_StaticFields
{
public:
	// System.Int32 GoogleARCore.Examples.Common.DetectedPlaneVisualizer::s_PlaneCount
	int32_t ___s_PlaneCount_2;

public:
	inline static int32_t get_offset_of_s_PlaneCount_2() { return static_cast<int32_t>(offsetof(DetectedPlaneVisualizer_t2496913565_StaticFields, ___s_PlaneCount_2)); }
	inline int32_t get_s_PlaneCount_2() const { return ___s_PlaneCount_2; }
	inline int32_t* get_address_of_s_PlaneCount_2() { return &___s_PlaneCount_2; }
	inline void set_s_PlaneCount_2(int32_t value)
	{
		___s_PlaneCount_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DETECTEDPLANEVISUALIZER_T2496913565_H
#ifndef DETECTEDPLANEGENERATOR_T1310336384_H
#define DETECTEDPLANEGENERATOR_T1310336384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.Common.DetectedPlaneGenerator
struct  DetectedPlaneGenerator_t1310336384  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject GoogleARCore.Examples.Common.DetectedPlaneGenerator::DetectedPlanePrefab
	GameObject_t1113636619 * ___DetectedPlanePrefab_2;
	// System.Collections.Generic.List`1<GoogleARCore.DetectedPlane> GoogleARCore.Examples.Common.DetectedPlaneGenerator::m_NewPlanes
	List_1_t1239236824 * ___m_NewPlanes_3;

public:
	inline static int32_t get_offset_of_DetectedPlanePrefab_2() { return static_cast<int32_t>(offsetof(DetectedPlaneGenerator_t1310336384, ___DetectedPlanePrefab_2)); }
	inline GameObject_t1113636619 * get_DetectedPlanePrefab_2() const { return ___DetectedPlanePrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_DetectedPlanePrefab_2() { return &___DetectedPlanePrefab_2; }
	inline void set_DetectedPlanePrefab_2(GameObject_t1113636619 * value)
	{
		___DetectedPlanePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___DetectedPlanePrefab_2), value);
	}

	inline static int32_t get_offset_of_m_NewPlanes_3() { return static_cast<int32_t>(offsetof(DetectedPlaneGenerator_t1310336384, ___m_NewPlanes_3)); }
	inline List_1_t1239236824 * get_m_NewPlanes_3() const { return ___m_NewPlanes_3; }
	inline List_1_t1239236824 ** get_address_of_m_NewPlanes_3() { return &___m_NewPlanes_3; }
	inline void set_m_NewPlanes_3(List_1_t1239236824 * value)
	{
		___m_NewPlanes_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_NewPlanes_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DETECTEDPLANEGENERATOR_T1310336384_H
#ifndef ROOMSHARINGSERVER_T2010038578_H
#define ROOMSHARINGSERVER_T2010038578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.CloudAnchor.RoomSharingServer
struct  RoomSharingServer_t2010038578  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,GoogleARCore.CrossPlatform.XPAnchor> GoogleARCore.Examples.CloudAnchor.RoomSharingServer::m_RoomAnchorsDict
	Dictionary_2_t3995852529 * ___m_RoomAnchorsDict_2;

public:
	inline static int32_t get_offset_of_m_RoomAnchorsDict_2() { return static_cast<int32_t>(offsetof(RoomSharingServer_t2010038578, ___m_RoomAnchorsDict_2)); }
	inline Dictionary_2_t3995852529 * get_m_RoomAnchorsDict_2() const { return ___m_RoomAnchorsDict_2; }
	inline Dictionary_2_t3995852529 ** get_address_of_m_RoomAnchorsDict_2() { return &___m_RoomAnchorsDict_2; }
	inline void set_m_RoomAnchorsDict_2(Dictionary_2_t3995852529 * value)
	{
		___m_RoomAnchorsDict_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_RoomAnchorsDict_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOMSHARINGSERVER_T2010038578_H
#ifndef HELLOARCONTROLLER_T1659088615_H
#define HELLOARCONTROLLER_T1659088615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.HelloAR.HelloARController
struct  HelloARController_t1659088615  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera GoogleARCore.Examples.HelloAR.HelloARController::FirstPersonCamera
	Camera_t4157153871 * ___FirstPersonCamera_2;
	// UnityEngine.GameObject GoogleARCore.Examples.HelloAR.HelloARController::DetectedPlanePrefab
	GameObject_t1113636619 * ___DetectedPlanePrefab_3;
	// UnityEngine.GameObject GoogleARCore.Examples.HelloAR.HelloARController::AndyAndroidPrefab
	GameObject_t1113636619 * ___AndyAndroidPrefab_4;
	// UnityEngine.GameObject GoogleARCore.Examples.HelloAR.HelloARController::SearchingForPlaneUI
	GameObject_t1113636619 * ___SearchingForPlaneUI_5;
	// System.Collections.Generic.List`1<GoogleARCore.DetectedPlane> GoogleARCore.Examples.HelloAR.HelloARController::m_AllPlanes
	List_1_t1239236824 * ___m_AllPlanes_7;
	// System.Boolean GoogleARCore.Examples.HelloAR.HelloARController::m_IsQuitting
	bool ___m_IsQuitting_8;

public:
	inline static int32_t get_offset_of_FirstPersonCamera_2() { return static_cast<int32_t>(offsetof(HelloARController_t1659088615, ___FirstPersonCamera_2)); }
	inline Camera_t4157153871 * get_FirstPersonCamera_2() const { return ___FirstPersonCamera_2; }
	inline Camera_t4157153871 ** get_address_of_FirstPersonCamera_2() { return &___FirstPersonCamera_2; }
	inline void set_FirstPersonCamera_2(Camera_t4157153871 * value)
	{
		___FirstPersonCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___FirstPersonCamera_2), value);
	}

	inline static int32_t get_offset_of_DetectedPlanePrefab_3() { return static_cast<int32_t>(offsetof(HelloARController_t1659088615, ___DetectedPlanePrefab_3)); }
	inline GameObject_t1113636619 * get_DetectedPlanePrefab_3() const { return ___DetectedPlanePrefab_3; }
	inline GameObject_t1113636619 ** get_address_of_DetectedPlanePrefab_3() { return &___DetectedPlanePrefab_3; }
	inline void set_DetectedPlanePrefab_3(GameObject_t1113636619 * value)
	{
		___DetectedPlanePrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___DetectedPlanePrefab_3), value);
	}

	inline static int32_t get_offset_of_AndyAndroidPrefab_4() { return static_cast<int32_t>(offsetof(HelloARController_t1659088615, ___AndyAndroidPrefab_4)); }
	inline GameObject_t1113636619 * get_AndyAndroidPrefab_4() const { return ___AndyAndroidPrefab_4; }
	inline GameObject_t1113636619 ** get_address_of_AndyAndroidPrefab_4() { return &___AndyAndroidPrefab_4; }
	inline void set_AndyAndroidPrefab_4(GameObject_t1113636619 * value)
	{
		___AndyAndroidPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___AndyAndroidPrefab_4), value);
	}

	inline static int32_t get_offset_of_SearchingForPlaneUI_5() { return static_cast<int32_t>(offsetof(HelloARController_t1659088615, ___SearchingForPlaneUI_5)); }
	inline GameObject_t1113636619 * get_SearchingForPlaneUI_5() const { return ___SearchingForPlaneUI_5; }
	inline GameObject_t1113636619 ** get_address_of_SearchingForPlaneUI_5() { return &___SearchingForPlaneUI_5; }
	inline void set_SearchingForPlaneUI_5(GameObject_t1113636619 * value)
	{
		___SearchingForPlaneUI_5 = value;
		Il2CppCodeGenWriteBarrier((&___SearchingForPlaneUI_5), value);
	}

	inline static int32_t get_offset_of_m_AllPlanes_7() { return static_cast<int32_t>(offsetof(HelloARController_t1659088615, ___m_AllPlanes_7)); }
	inline List_1_t1239236824 * get_m_AllPlanes_7() const { return ___m_AllPlanes_7; }
	inline List_1_t1239236824 ** get_address_of_m_AllPlanes_7() { return &___m_AllPlanes_7; }
	inline void set_m_AllPlanes_7(List_1_t1239236824 * value)
	{
		___m_AllPlanes_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AllPlanes_7), value);
	}

	inline static int32_t get_offset_of_m_IsQuitting_8() { return static_cast<int32_t>(offsetof(HelloARController_t1659088615, ___m_IsQuitting_8)); }
	inline bool get_m_IsQuitting_8() const { return ___m_IsQuitting_8; }
	inline bool* get_address_of_m_IsQuitting_8() { return &___m_IsQuitting_8; }
	inline void set_m_IsQuitting_8(bool value)
	{
		___m_IsQuitting_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HELLOARCONTROLLER_T1659088615_H
#ifndef CLOUDANCHORUICONTROLLER_T97785020_H
#define CLOUDANCHORUICONTROLLER_T97785020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.CloudAnchor.CloudAnchorUIController
struct  CloudAnchorUIController_t97785020  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text GoogleARCore.Examples.CloudAnchor.CloudAnchorUIController::SnackbarText
	Text_t1901882714 * ___SnackbarText_2;
	// UnityEngine.UI.Text GoogleARCore.Examples.CloudAnchor.CloudAnchorUIController::RoomText
	Text_t1901882714 * ___RoomText_3;
	// UnityEngine.UI.Text GoogleARCore.Examples.CloudAnchor.CloudAnchorUIController::IPAddressText
	Text_t1901882714 * ___IPAddressText_4;
	// UnityEngine.UI.Button GoogleARCore.Examples.CloudAnchor.CloudAnchorUIController::HostAnchorModeButton
	Button_t4055032469 * ___HostAnchorModeButton_5;
	// UnityEngine.UI.Button GoogleARCore.Examples.CloudAnchor.CloudAnchorUIController::ResolveAnchorModeButton
	Button_t4055032469 * ___ResolveAnchorModeButton_6;
	// UnityEngine.GameObject GoogleARCore.Examples.CloudAnchor.CloudAnchorUIController::InputRoot
	GameObject_t1113636619 * ___InputRoot_7;
	// UnityEngine.UI.InputField GoogleARCore.Examples.CloudAnchor.CloudAnchorUIController::RoomInputField
	InputField_t3762917431 * ___RoomInputField_8;
	// UnityEngine.UI.InputField GoogleARCore.Examples.CloudAnchor.CloudAnchorUIController::IpAddressInputField
	InputField_t3762917431 * ___IpAddressInputField_9;
	// UnityEngine.UI.Toggle GoogleARCore.Examples.CloudAnchor.CloudAnchorUIController::ResolveOnDeviceToggle
	Toggle_t2735377061 * ___ResolveOnDeviceToggle_10;

public:
	inline static int32_t get_offset_of_SnackbarText_2() { return static_cast<int32_t>(offsetof(CloudAnchorUIController_t97785020, ___SnackbarText_2)); }
	inline Text_t1901882714 * get_SnackbarText_2() const { return ___SnackbarText_2; }
	inline Text_t1901882714 ** get_address_of_SnackbarText_2() { return &___SnackbarText_2; }
	inline void set_SnackbarText_2(Text_t1901882714 * value)
	{
		___SnackbarText_2 = value;
		Il2CppCodeGenWriteBarrier((&___SnackbarText_2), value);
	}

	inline static int32_t get_offset_of_RoomText_3() { return static_cast<int32_t>(offsetof(CloudAnchorUIController_t97785020, ___RoomText_3)); }
	inline Text_t1901882714 * get_RoomText_3() const { return ___RoomText_3; }
	inline Text_t1901882714 ** get_address_of_RoomText_3() { return &___RoomText_3; }
	inline void set_RoomText_3(Text_t1901882714 * value)
	{
		___RoomText_3 = value;
		Il2CppCodeGenWriteBarrier((&___RoomText_3), value);
	}

	inline static int32_t get_offset_of_IPAddressText_4() { return static_cast<int32_t>(offsetof(CloudAnchorUIController_t97785020, ___IPAddressText_4)); }
	inline Text_t1901882714 * get_IPAddressText_4() const { return ___IPAddressText_4; }
	inline Text_t1901882714 ** get_address_of_IPAddressText_4() { return &___IPAddressText_4; }
	inline void set_IPAddressText_4(Text_t1901882714 * value)
	{
		___IPAddressText_4 = value;
		Il2CppCodeGenWriteBarrier((&___IPAddressText_4), value);
	}

	inline static int32_t get_offset_of_HostAnchorModeButton_5() { return static_cast<int32_t>(offsetof(CloudAnchorUIController_t97785020, ___HostAnchorModeButton_5)); }
	inline Button_t4055032469 * get_HostAnchorModeButton_5() const { return ___HostAnchorModeButton_5; }
	inline Button_t4055032469 ** get_address_of_HostAnchorModeButton_5() { return &___HostAnchorModeButton_5; }
	inline void set_HostAnchorModeButton_5(Button_t4055032469 * value)
	{
		___HostAnchorModeButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___HostAnchorModeButton_5), value);
	}

	inline static int32_t get_offset_of_ResolveAnchorModeButton_6() { return static_cast<int32_t>(offsetof(CloudAnchorUIController_t97785020, ___ResolveAnchorModeButton_6)); }
	inline Button_t4055032469 * get_ResolveAnchorModeButton_6() const { return ___ResolveAnchorModeButton_6; }
	inline Button_t4055032469 ** get_address_of_ResolveAnchorModeButton_6() { return &___ResolveAnchorModeButton_6; }
	inline void set_ResolveAnchorModeButton_6(Button_t4055032469 * value)
	{
		___ResolveAnchorModeButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___ResolveAnchorModeButton_6), value);
	}

	inline static int32_t get_offset_of_InputRoot_7() { return static_cast<int32_t>(offsetof(CloudAnchorUIController_t97785020, ___InputRoot_7)); }
	inline GameObject_t1113636619 * get_InputRoot_7() const { return ___InputRoot_7; }
	inline GameObject_t1113636619 ** get_address_of_InputRoot_7() { return &___InputRoot_7; }
	inline void set_InputRoot_7(GameObject_t1113636619 * value)
	{
		___InputRoot_7 = value;
		Il2CppCodeGenWriteBarrier((&___InputRoot_7), value);
	}

	inline static int32_t get_offset_of_RoomInputField_8() { return static_cast<int32_t>(offsetof(CloudAnchorUIController_t97785020, ___RoomInputField_8)); }
	inline InputField_t3762917431 * get_RoomInputField_8() const { return ___RoomInputField_8; }
	inline InputField_t3762917431 ** get_address_of_RoomInputField_8() { return &___RoomInputField_8; }
	inline void set_RoomInputField_8(InputField_t3762917431 * value)
	{
		___RoomInputField_8 = value;
		Il2CppCodeGenWriteBarrier((&___RoomInputField_8), value);
	}

	inline static int32_t get_offset_of_IpAddressInputField_9() { return static_cast<int32_t>(offsetof(CloudAnchorUIController_t97785020, ___IpAddressInputField_9)); }
	inline InputField_t3762917431 * get_IpAddressInputField_9() const { return ___IpAddressInputField_9; }
	inline InputField_t3762917431 ** get_address_of_IpAddressInputField_9() { return &___IpAddressInputField_9; }
	inline void set_IpAddressInputField_9(InputField_t3762917431 * value)
	{
		___IpAddressInputField_9 = value;
		Il2CppCodeGenWriteBarrier((&___IpAddressInputField_9), value);
	}

	inline static int32_t get_offset_of_ResolveOnDeviceToggle_10() { return static_cast<int32_t>(offsetof(CloudAnchorUIController_t97785020, ___ResolveOnDeviceToggle_10)); }
	inline Toggle_t2735377061 * get_ResolveOnDeviceToggle_10() const { return ___ResolveOnDeviceToggle_10; }
	inline Toggle_t2735377061 ** get_address_of_ResolveOnDeviceToggle_10() { return &___ResolveOnDeviceToggle_10; }
	inline void set_ResolveOnDeviceToggle_10(Toggle_t2735377061 * value)
	{
		___ResolveOnDeviceToggle_10 = value;
		Il2CppCodeGenWriteBarrier((&___ResolveOnDeviceToggle_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDANCHORUICONTROLLER_T97785020_H
#ifndef INSTANTPREVIEWTRACKEDPOSEDRIVER_T3813668419_H
#define INSTANTPREVIEWTRACKEDPOSEDRIVER_T3813668419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.InstantPreviewTrackedPoseDriver
struct  InstantPreviewTrackedPoseDriver_t3813668419  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANTPREVIEWTRACKEDPOSEDRIVER_T3813668419_H
#ifndef ANCHOR_T769518176_H
#define ANCHOR_T769518176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Anchor
struct  Anchor_t769518176  : public MonoBehaviour_t3962482529
{
public:
	// GoogleARCore.TrackingState GoogleARCore.Anchor::m_LastFrameTrackingState
	int32_t ___m_LastFrameTrackingState_3;
	// System.Boolean GoogleARCore.Anchor::m_IsSessionDestroyed
	bool ___m_IsSessionDestroyed_4;
	// GoogleARCoreInternal.NativeSession GoogleARCore.Anchor::<m_NativeSession>k__BackingField
	NativeSession_t1550589577 * ___U3Cm_NativeSessionU3Ek__BackingField_5;
	// System.IntPtr GoogleARCore.Anchor::<m_NativeHandle>k__BackingField
	intptr_t ___U3Cm_NativeHandleU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_m_LastFrameTrackingState_3() { return static_cast<int32_t>(offsetof(Anchor_t769518176, ___m_LastFrameTrackingState_3)); }
	inline int32_t get_m_LastFrameTrackingState_3() const { return ___m_LastFrameTrackingState_3; }
	inline int32_t* get_address_of_m_LastFrameTrackingState_3() { return &___m_LastFrameTrackingState_3; }
	inline void set_m_LastFrameTrackingState_3(int32_t value)
	{
		___m_LastFrameTrackingState_3 = value;
	}

	inline static int32_t get_offset_of_m_IsSessionDestroyed_4() { return static_cast<int32_t>(offsetof(Anchor_t769518176, ___m_IsSessionDestroyed_4)); }
	inline bool get_m_IsSessionDestroyed_4() const { return ___m_IsSessionDestroyed_4; }
	inline bool* get_address_of_m_IsSessionDestroyed_4() { return &___m_IsSessionDestroyed_4; }
	inline void set_m_IsSessionDestroyed_4(bool value)
	{
		___m_IsSessionDestroyed_4 = value;
	}

	inline static int32_t get_offset_of_U3Cm_NativeSessionU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Anchor_t769518176, ___U3Cm_NativeSessionU3Ek__BackingField_5)); }
	inline NativeSession_t1550589577 * get_U3Cm_NativeSessionU3Ek__BackingField_5() const { return ___U3Cm_NativeSessionU3Ek__BackingField_5; }
	inline NativeSession_t1550589577 ** get_address_of_U3Cm_NativeSessionU3Ek__BackingField_5() { return &___U3Cm_NativeSessionU3Ek__BackingField_5; }
	inline void set_U3Cm_NativeSessionU3Ek__BackingField_5(NativeSession_t1550589577 * value)
	{
		___U3Cm_NativeSessionU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cm_NativeSessionU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3Cm_NativeHandleU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Anchor_t769518176, ___U3Cm_NativeHandleU3Ek__BackingField_6)); }
	inline intptr_t get_U3Cm_NativeHandleU3Ek__BackingField_6() const { return ___U3Cm_NativeHandleU3Ek__BackingField_6; }
	inline intptr_t* get_address_of_U3Cm_NativeHandleU3Ek__BackingField_6() { return &___U3Cm_NativeHandleU3Ek__BackingField_6; }
	inline void set_U3Cm_NativeHandleU3Ek__BackingField_6(intptr_t value)
	{
		___U3Cm_NativeHandleU3Ek__BackingField_6 = value;
	}
};

struct Anchor_t769518176_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.IntPtr,GoogleARCore.Anchor> GoogleARCore.Anchor::s_AnchorDict
	Dictionary_2_t3286351191 * ___s_AnchorDict_2;

public:
	inline static int32_t get_offset_of_s_AnchorDict_2() { return static_cast<int32_t>(offsetof(Anchor_t769518176_StaticFields, ___s_AnchorDict_2)); }
	inline Dictionary_2_t3286351191 * get_s_AnchorDict_2() const { return ___s_AnchorDict_2; }
	inline Dictionary_2_t3286351191 ** get_address_of_s_AnchorDict_2() { return &___s_AnchorDict_2; }
	inline void set_s_AnchorDict_2(Dictionary_2_t3286351191 * value)
	{
		___s_AnchorDict_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_AnchorDict_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHOR_T769518176_H
#ifndef CLOUDANCHORCONTROLLER_T754558460_H
#define CLOUDANCHORCONTROLLER_T754558460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.CloudAnchor.CloudAnchorController
struct  CloudAnchorController_t754558460  : public MonoBehaviour_t3962482529
{
public:
	// GoogleARCore.Examples.CloudAnchor.RoomSharingServer GoogleARCore.Examples.CloudAnchor.CloudAnchorController::RoomSharingServer
	RoomSharingServer_t2010038578 * ___RoomSharingServer_2;
	// GoogleARCore.Examples.CloudAnchor.CloudAnchorUIController GoogleARCore.Examples.CloudAnchor.CloudAnchorController::UIController
	CloudAnchorUIController_t97785020 * ___UIController_3;
	// UnityEngine.GameObject GoogleARCore.Examples.CloudAnchor.CloudAnchorController::ARCoreRoot
	GameObject_t1113636619 * ___ARCoreRoot_4;
	// UnityEngine.GameObject GoogleARCore.Examples.CloudAnchor.CloudAnchorController::ARCoreAndyAndroidPrefab
	GameObject_t1113636619 * ___ARCoreAndyAndroidPrefab_5;
	// UnityEngine.GameObject GoogleARCore.Examples.CloudAnchor.CloudAnchorController::ARKitRoot
	GameObject_t1113636619 * ___ARKitRoot_6;
	// UnityEngine.Camera GoogleARCore.Examples.CloudAnchor.CloudAnchorController::ARKitFirstPersonCamera
	Camera_t4157153871 * ___ARKitFirstPersonCamera_7;
	// UnityEngine.GameObject GoogleARCore.Examples.CloudAnchor.CloudAnchorController::ARKitAndyAndroidPrefab
	GameObject_t1113636619 * ___ARKitAndyAndroidPrefab_8;
	// GoogleARCore.Examples.CloudAnchor.ARKitHelper GoogleARCore.Examples.CloudAnchor.CloudAnchorController::m_ARKit
	ARKitHelper_t1470960758 * ___m_ARKit_11;
	// System.Boolean GoogleARCore.Examples.CloudAnchor.CloudAnchorController::m_IsQuitting
	bool ___m_IsQuitting_12;
	// UnityEngine.Component GoogleARCore.Examples.CloudAnchor.CloudAnchorController::m_LastPlacedAnchor
	Component_t1923634451 * ___m_LastPlacedAnchor_13;
	// GoogleARCore.CrossPlatform.XPAnchor GoogleARCore.Examples.CloudAnchor.CloudAnchorController::m_LastResolvedAnchor
	XPAnchor_t812171902 * ___m_LastResolvedAnchor_14;
	// GoogleARCore.Examples.CloudAnchor.CloudAnchorController/ApplicationMode GoogleARCore.Examples.CloudAnchor.CloudAnchorController::m_CurrentMode
	int32_t ___m_CurrentMode_15;
	// System.Int32 GoogleARCore.Examples.CloudAnchor.CloudAnchorController::m_CurrentRoom
	int32_t ___m_CurrentRoom_16;

public:
	inline static int32_t get_offset_of_RoomSharingServer_2() { return static_cast<int32_t>(offsetof(CloudAnchorController_t754558460, ___RoomSharingServer_2)); }
	inline RoomSharingServer_t2010038578 * get_RoomSharingServer_2() const { return ___RoomSharingServer_2; }
	inline RoomSharingServer_t2010038578 ** get_address_of_RoomSharingServer_2() { return &___RoomSharingServer_2; }
	inline void set_RoomSharingServer_2(RoomSharingServer_t2010038578 * value)
	{
		___RoomSharingServer_2 = value;
		Il2CppCodeGenWriteBarrier((&___RoomSharingServer_2), value);
	}

	inline static int32_t get_offset_of_UIController_3() { return static_cast<int32_t>(offsetof(CloudAnchorController_t754558460, ___UIController_3)); }
	inline CloudAnchorUIController_t97785020 * get_UIController_3() const { return ___UIController_3; }
	inline CloudAnchorUIController_t97785020 ** get_address_of_UIController_3() { return &___UIController_3; }
	inline void set_UIController_3(CloudAnchorUIController_t97785020 * value)
	{
		___UIController_3 = value;
		Il2CppCodeGenWriteBarrier((&___UIController_3), value);
	}

	inline static int32_t get_offset_of_ARCoreRoot_4() { return static_cast<int32_t>(offsetof(CloudAnchorController_t754558460, ___ARCoreRoot_4)); }
	inline GameObject_t1113636619 * get_ARCoreRoot_4() const { return ___ARCoreRoot_4; }
	inline GameObject_t1113636619 ** get_address_of_ARCoreRoot_4() { return &___ARCoreRoot_4; }
	inline void set_ARCoreRoot_4(GameObject_t1113636619 * value)
	{
		___ARCoreRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&___ARCoreRoot_4), value);
	}

	inline static int32_t get_offset_of_ARCoreAndyAndroidPrefab_5() { return static_cast<int32_t>(offsetof(CloudAnchorController_t754558460, ___ARCoreAndyAndroidPrefab_5)); }
	inline GameObject_t1113636619 * get_ARCoreAndyAndroidPrefab_5() const { return ___ARCoreAndyAndroidPrefab_5; }
	inline GameObject_t1113636619 ** get_address_of_ARCoreAndyAndroidPrefab_5() { return &___ARCoreAndyAndroidPrefab_5; }
	inline void set_ARCoreAndyAndroidPrefab_5(GameObject_t1113636619 * value)
	{
		___ARCoreAndyAndroidPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___ARCoreAndyAndroidPrefab_5), value);
	}

	inline static int32_t get_offset_of_ARKitRoot_6() { return static_cast<int32_t>(offsetof(CloudAnchorController_t754558460, ___ARKitRoot_6)); }
	inline GameObject_t1113636619 * get_ARKitRoot_6() const { return ___ARKitRoot_6; }
	inline GameObject_t1113636619 ** get_address_of_ARKitRoot_6() { return &___ARKitRoot_6; }
	inline void set_ARKitRoot_6(GameObject_t1113636619 * value)
	{
		___ARKitRoot_6 = value;
		Il2CppCodeGenWriteBarrier((&___ARKitRoot_6), value);
	}

	inline static int32_t get_offset_of_ARKitFirstPersonCamera_7() { return static_cast<int32_t>(offsetof(CloudAnchorController_t754558460, ___ARKitFirstPersonCamera_7)); }
	inline Camera_t4157153871 * get_ARKitFirstPersonCamera_7() const { return ___ARKitFirstPersonCamera_7; }
	inline Camera_t4157153871 ** get_address_of_ARKitFirstPersonCamera_7() { return &___ARKitFirstPersonCamera_7; }
	inline void set_ARKitFirstPersonCamera_7(Camera_t4157153871 * value)
	{
		___ARKitFirstPersonCamera_7 = value;
		Il2CppCodeGenWriteBarrier((&___ARKitFirstPersonCamera_7), value);
	}

	inline static int32_t get_offset_of_ARKitAndyAndroidPrefab_8() { return static_cast<int32_t>(offsetof(CloudAnchorController_t754558460, ___ARKitAndyAndroidPrefab_8)); }
	inline GameObject_t1113636619 * get_ARKitAndyAndroidPrefab_8() const { return ___ARKitAndyAndroidPrefab_8; }
	inline GameObject_t1113636619 ** get_address_of_ARKitAndyAndroidPrefab_8() { return &___ARKitAndyAndroidPrefab_8; }
	inline void set_ARKitAndyAndroidPrefab_8(GameObject_t1113636619 * value)
	{
		___ARKitAndyAndroidPrefab_8 = value;
		Il2CppCodeGenWriteBarrier((&___ARKitAndyAndroidPrefab_8), value);
	}

	inline static int32_t get_offset_of_m_ARKit_11() { return static_cast<int32_t>(offsetof(CloudAnchorController_t754558460, ___m_ARKit_11)); }
	inline ARKitHelper_t1470960758 * get_m_ARKit_11() const { return ___m_ARKit_11; }
	inline ARKitHelper_t1470960758 ** get_address_of_m_ARKit_11() { return &___m_ARKit_11; }
	inline void set_m_ARKit_11(ARKitHelper_t1470960758 * value)
	{
		___m_ARKit_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_ARKit_11), value);
	}

	inline static int32_t get_offset_of_m_IsQuitting_12() { return static_cast<int32_t>(offsetof(CloudAnchorController_t754558460, ___m_IsQuitting_12)); }
	inline bool get_m_IsQuitting_12() const { return ___m_IsQuitting_12; }
	inline bool* get_address_of_m_IsQuitting_12() { return &___m_IsQuitting_12; }
	inline void set_m_IsQuitting_12(bool value)
	{
		___m_IsQuitting_12 = value;
	}

	inline static int32_t get_offset_of_m_LastPlacedAnchor_13() { return static_cast<int32_t>(offsetof(CloudAnchorController_t754558460, ___m_LastPlacedAnchor_13)); }
	inline Component_t1923634451 * get_m_LastPlacedAnchor_13() const { return ___m_LastPlacedAnchor_13; }
	inline Component_t1923634451 ** get_address_of_m_LastPlacedAnchor_13() { return &___m_LastPlacedAnchor_13; }
	inline void set_m_LastPlacedAnchor_13(Component_t1923634451 * value)
	{
		___m_LastPlacedAnchor_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_LastPlacedAnchor_13), value);
	}

	inline static int32_t get_offset_of_m_LastResolvedAnchor_14() { return static_cast<int32_t>(offsetof(CloudAnchorController_t754558460, ___m_LastResolvedAnchor_14)); }
	inline XPAnchor_t812171902 * get_m_LastResolvedAnchor_14() const { return ___m_LastResolvedAnchor_14; }
	inline XPAnchor_t812171902 ** get_address_of_m_LastResolvedAnchor_14() { return &___m_LastResolvedAnchor_14; }
	inline void set_m_LastResolvedAnchor_14(XPAnchor_t812171902 * value)
	{
		___m_LastResolvedAnchor_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_LastResolvedAnchor_14), value);
	}

	inline static int32_t get_offset_of_m_CurrentMode_15() { return static_cast<int32_t>(offsetof(CloudAnchorController_t754558460, ___m_CurrentMode_15)); }
	inline int32_t get_m_CurrentMode_15() const { return ___m_CurrentMode_15; }
	inline int32_t* get_address_of_m_CurrentMode_15() { return &___m_CurrentMode_15; }
	inline void set_m_CurrentMode_15(int32_t value)
	{
		___m_CurrentMode_15 = value;
	}

	inline static int32_t get_offset_of_m_CurrentRoom_16() { return static_cast<int32_t>(offsetof(CloudAnchorController_t754558460, ___m_CurrentRoom_16)); }
	inline int32_t get_m_CurrentRoom_16() const { return ___m_CurrentRoom_16; }
	inline int32_t* get_address_of_m_CurrentRoom_16() { return &___m_CurrentRoom_16; }
	inline void set_m_CurrentRoom_16(int32_t value)
	{
		___m_CurrentRoom_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDANCHORCONTROLLER_T754558460_H
#ifndef AUGMENTEDIMAGEVISUALIZER_T1303630779_H
#define AUGMENTEDIMAGEVISUALIZER_T1303630779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.AugmentedImage.AugmentedImageVisualizer
struct  AugmentedImageVisualizer_t1303630779  : public MonoBehaviour_t3962482529
{
public:
	// GoogleARCore.AugmentedImage GoogleARCore.Examples.AugmentedImage.AugmentedImageVisualizer::Image
	AugmentedImage_t279011686 * ___Image_2;
	// UnityEngine.GameObject GoogleARCore.Examples.AugmentedImage.AugmentedImageVisualizer::FrameLowerLeft
	GameObject_t1113636619 * ___FrameLowerLeft_3;
	// UnityEngine.GameObject GoogleARCore.Examples.AugmentedImage.AugmentedImageVisualizer::FrameLowerRight
	GameObject_t1113636619 * ___FrameLowerRight_4;
	// UnityEngine.GameObject GoogleARCore.Examples.AugmentedImage.AugmentedImageVisualizer::FrameUpperLeft
	GameObject_t1113636619 * ___FrameUpperLeft_5;
	// UnityEngine.GameObject GoogleARCore.Examples.AugmentedImage.AugmentedImageVisualizer::FrameUpperRight
	GameObject_t1113636619 * ___FrameUpperRight_6;

public:
	inline static int32_t get_offset_of_Image_2() { return static_cast<int32_t>(offsetof(AugmentedImageVisualizer_t1303630779, ___Image_2)); }
	inline AugmentedImage_t279011686 * get_Image_2() const { return ___Image_2; }
	inline AugmentedImage_t279011686 ** get_address_of_Image_2() { return &___Image_2; }
	inline void set_Image_2(AugmentedImage_t279011686 * value)
	{
		___Image_2 = value;
		Il2CppCodeGenWriteBarrier((&___Image_2), value);
	}

	inline static int32_t get_offset_of_FrameLowerLeft_3() { return static_cast<int32_t>(offsetof(AugmentedImageVisualizer_t1303630779, ___FrameLowerLeft_3)); }
	inline GameObject_t1113636619 * get_FrameLowerLeft_3() const { return ___FrameLowerLeft_3; }
	inline GameObject_t1113636619 ** get_address_of_FrameLowerLeft_3() { return &___FrameLowerLeft_3; }
	inline void set_FrameLowerLeft_3(GameObject_t1113636619 * value)
	{
		___FrameLowerLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___FrameLowerLeft_3), value);
	}

	inline static int32_t get_offset_of_FrameLowerRight_4() { return static_cast<int32_t>(offsetof(AugmentedImageVisualizer_t1303630779, ___FrameLowerRight_4)); }
	inline GameObject_t1113636619 * get_FrameLowerRight_4() const { return ___FrameLowerRight_4; }
	inline GameObject_t1113636619 ** get_address_of_FrameLowerRight_4() { return &___FrameLowerRight_4; }
	inline void set_FrameLowerRight_4(GameObject_t1113636619 * value)
	{
		___FrameLowerRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___FrameLowerRight_4), value);
	}

	inline static int32_t get_offset_of_FrameUpperLeft_5() { return static_cast<int32_t>(offsetof(AugmentedImageVisualizer_t1303630779, ___FrameUpperLeft_5)); }
	inline GameObject_t1113636619 * get_FrameUpperLeft_5() const { return ___FrameUpperLeft_5; }
	inline GameObject_t1113636619 ** get_address_of_FrameUpperLeft_5() { return &___FrameUpperLeft_5; }
	inline void set_FrameUpperLeft_5(GameObject_t1113636619 * value)
	{
		___FrameUpperLeft_5 = value;
		Il2CppCodeGenWriteBarrier((&___FrameUpperLeft_5), value);
	}

	inline static int32_t get_offset_of_FrameUpperRight_6() { return static_cast<int32_t>(offsetof(AugmentedImageVisualizer_t1303630779, ___FrameUpperRight_6)); }
	inline GameObject_t1113636619 * get_FrameUpperRight_6() const { return ___FrameUpperRight_6; }
	inline GameObject_t1113636619 ** get_address_of_FrameUpperRight_6() { return &___FrameUpperRight_6; }
	inline void set_FrameUpperRight_6(GameObject_t1113636619 * value)
	{
		___FrameUpperRight_6 = value;
		Il2CppCodeGenWriteBarrier((&___FrameUpperRight_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUGMENTEDIMAGEVISUALIZER_T1303630779_H
#ifndef TEXTUREREADER_T2342948801_H
#define TEXTUREREADER_T2342948801_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.ComputerVision.TextureReader
struct  TextureReader_t2342948801  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 GoogleARCore.Examples.ComputerVision.TextureReader::ImageWidth
	int32_t ___ImageWidth_2;
	// System.Int32 GoogleARCore.Examples.ComputerVision.TextureReader::ImageHeight
	int32_t ___ImageHeight_3;
	// GoogleARCore.Examples.ComputerVision.TextureReader/SampleMode GoogleARCore.Examples.ComputerVision.TextureReader::ImageSampleMode
	int32_t ___ImageSampleMode_4;
	// GoogleARCore.Examples.ComputerVision.TextureReaderApi/ImageFormatType GoogleARCore.Examples.ComputerVision.TextureReader::ImageFormat
	int32_t ___ImageFormat_5;
	// GoogleARCore.Examples.ComputerVision.TextureReaderApi GoogleARCore.Examples.ComputerVision.TextureReader::m_TextureReaderApi
	TextureReaderApi_t3129393145 * ___m_TextureReaderApi_8;
	// GoogleARCore.Examples.ComputerVision.TextureReader/CommandType GoogleARCore.Examples.ComputerVision.TextureReader::m_Command
	int32_t ___m_Command_9;
	// System.Int32 GoogleARCore.Examples.ComputerVision.TextureReader::m_ImageBufferIndex
	int32_t ___m_ImageBufferIndex_10;
	// GoogleARCore.Examples.ComputerVision.TextureReader/OnImageAvailableCallbackFunc GoogleARCore.Examples.ComputerVision.TextureReader::OnImageAvailableCallback
	OnImageAvailableCallbackFunc_t2664114036 * ___OnImageAvailableCallback_11;

public:
	inline static int32_t get_offset_of_ImageWidth_2() { return static_cast<int32_t>(offsetof(TextureReader_t2342948801, ___ImageWidth_2)); }
	inline int32_t get_ImageWidth_2() const { return ___ImageWidth_2; }
	inline int32_t* get_address_of_ImageWidth_2() { return &___ImageWidth_2; }
	inline void set_ImageWidth_2(int32_t value)
	{
		___ImageWidth_2 = value;
	}

	inline static int32_t get_offset_of_ImageHeight_3() { return static_cast<int32_t>(offsetof(TextureReader_t2342948801, ___ImageHeight_3)); }
	inline int32_t get_ImageHeight_3() const { return ___ImageHeight_3; }
	inline int32_t* get_address_of_ImageHeight_3() { return &___ImageHeight_3; }
	inline void set_ImageHeight_3(int32_t value)
	{
		___ImageHeight_3 = value;
	}

	inline static int32_t get_offset_of_ImageSampleMode_4() { return static_cast<int32_t>(offsetof(TextureReader_t2342948801, ___ImageSampleMode_4)); }
	inline int32_t get_ImageSampleMode_4() const { return ___ImageSampleMode_4; }
	inline int32_t* get_address_of_ImageSampleMode_4() { return &___ImageSampleMode_4; }
	inline void set_ImageSampleMode_4(int32_t value)
	{
		___ImageSampleMode_4 = value;
	}

	inline static int32_t get_offset_of_ImageFormat_5() { return static_cast<int32_t>(offsetof(TextureReader_t2342948801, ___ImageFormat_5)); }
	inline int32_t get_ImageFormat_5() const { return ___ImageFormat_5; }
	inline int32_t* get_address_of_ImageFormat_5() { return &___ImageFormat_5; }
	inline void set_ImageFormat_5(int32_t value)
	{
		___ImageFormat_5 = value;
	}

	inline static int32_t get_offset_of_m_TextureReaderApi_8() { return static_cast<int32_t>(offsetof(TextureReader_t2342948801, ___m_TextureReaderApi_8)); }
	inline TextureReaderApi_t3129393145 * get_m_TextureReaderApi_8() const { return ___m_TextureReaderApi_8; }
	inline TextureReaderApi_t3129393145 ** get_address_of_m_TextureReaderApi_8() { return &___m_TextureReaderApi_8; }
	inline void set_m_TextureReaderApi_8(TextureReaderApi_t3129393145 * value)
	{
		___m_TextureReaderApi_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextureReaderApi_8), value);
	}

	inline static int32_t get_offset_of_m_Command_9() { return static_cast<int32_t>(offsetof(TextureReader_t2342948801, ___m_Command_9)); }
	inline int32_t get_m_Command_9() const { return ___m_Command_9; }
	inline int32_t* get_address_of_m_Command_9() { return &___m_Command_9; }
	inline void set_m_Command_9(int32_t value)
	{
		___m_Command_9 = value;
	}

	inline static int32_t get_offset_of_m_ImageBufferIndex_10() { return static_cast<int32_t>(offsetof(TextureReader_t2342948801, ___m_ImageBufferIndex_10)); }
	inline int32_t get_m_ImageBufferIndex_10() const { return ___m_ImageBufferIndex_10; }
	inline int32_t* get_address_of_m_ImageBufferIndex_10() { return &___m_ImageBufferIndex_10; }
	inline void set_m_ImageBufferIndex_10(int32_t value)
	{
		___m_ImageBufferIndex_10 = value;
	}

	inline static int32_t get_offset_of_OnImageAvailableCallback_11() { return static_cast<int32_t>(offsetof(TextureReader_t2342948801, ___OnImageAvailableCallback_11)); }
	inline OnImageAvailableCallbackFunc_t2664114036 * get_OnImageAvailableCallback_11() const { return ___OnImageAvailableCallback_11; }
	inline OnImageAvailableCallbackFunc_t2664114036 ** get_address_of_OnImageAvailableCallback_11() { return &___OnImageAvailableCallback_11; }
	inline void set_OnImageAvailableCallback_11(OnImageAvailableCallbackFunc_t2664114036 * value)
	{
		___OnImageAvailableCallback_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnImageAvailableCallback_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREREADER_T2342948801_H
#ifndef AUGMENTEDIMAGEEXAMPLECONTROLLER_T2191676296_H
#define AUGMENTEDIMAGEEXAMPLECONTROLLER_T2191676296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleARCore.Examples.AugmentedImage.AugmentedImageExampleController
struct  AugmentedImageExampleController_t2191676296  : public MonoBehaviour_t3962482529
{
public:
	// GoogleARCore.Examples.AugmentedImage.AugmentedImageVisualizer GoogleARCore.Examples.AugmentedImage.AugmentedImageExampleController::AugmentedImageVisualizerPrefab
	AugmentedImageVisualizer_t1303630779 * ___AugmentedImageVisualizerPrefab_2;
	// UnityEngine.GameObject GoogleARCore.Examples.AugmentedImage.AugmentedImageExampleController::FitToScanOverlay
	GameObject_t1113636619 * ___FitToScanOverlay_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,GoogleARCore.Examples.AugmentedImage.AugmentedImageVisualizer> GoogleARCore.Examples.AugmentedImage.AugmentedImageExampleController::m_Visualizers
	Dictionary_2_t192344110 * ___m_Visualizers_4;
	// System.Collections.Generic.List`1<GoogleARCore.AugmentedImage> GoogleARCore.Examples.AugmentedImage.AugmentedImageExampleController::m_TempAugmentedImages
	List_1_t1751086428 * ___m_TempAugmentedImages_5;

public:
	inline static int32_t get_offset_of_AugmentedImageVisualizerPrefab_2() { return static_cast<int32_t>(offsetof(AugmentedImageExampleController_t2191676296, ___AugmentedImageVisualizerPrefab_2)); }
	inline AugmentedImageVisualizer_t1303630779 * get_AugmentedImageVisualizerPrefab_2() const { return ___AugmentedImageVisualizerPrefab_2; }
	inline AugmentedImageVisualizer_t1303630779 ** get_address_of_AugmentedImageVisualizerPrefab_2() { return &___AugmentedImageVisualizerPrefab_2; }
	inline void set_AugmentedImageVisualizerPrefab_2(AugmentedImageVisualizer_t1303630779 * value)
	{
		___AugmentedImageVisualizerPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___AugmentedImageVisualizerPrefab_2), value);
	}

	inline static int32_t get_offset_of_FitToScanOverlay_3() { return static_cast<int32_t>(offsetof(AugmentedImageExampleController_t2191676296, ___FitToScanOverlay_3)); }
	inline GameObject_t1113636619 * get_FitToScanOverlay_3() const { return ___FitToScanOverlay_3; }
	inline GameObject_t1113636619 ** get_address_of_FitToScanOverlay_3() { return &___FitToScanOverlay_3; }
	inline void set_FitToScanOverlay_3(GameObject_t1113636619 * value)
	{
		___FitToScanOverlay_3 = value;
		Il2CppCodeGenWriteBarrier((&___FitToScanOverlay_3), value);
	}

	inline static int32_t get_offset_of_m_Visualizers_4() { return static_cast<int32_t>(offsetof(AugmentedImageExampleController_t2191676296, ___m_Visualizers_4)); }
	inline Dictionary_2_t192344110 * get_m_Visualizers_4() const { return ___m_Visualizers_4; }
	inline Dictionary_2_t192344110 ** get_address_of_m_Visualizers_4() { return &___m_Visualizers_4; }
	inline void set_m_Visualizers_4(Dictionary_2_t192344110 * value)
	{
		___m_Visualizers_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Visualizers_4), value);
	}

	inline static int32_t get_offset_of_m_TempAugmentedImages_5() { return static_cast<int32_t>(offsetof(AugmentedImageExampleController_t2191676296, ___m_TempAugmentedImages_5)); }
	inline List_1_t1751086428 * get_m_TempAugmentedImages_5() const { return ___m_TempAugmentedImages_5; }
	inline List_1_t1751086428 ** get_address_of_m_TempAugmentedImages_5() { return &___m_TempAugmentedImages_5; }
	inline void set_m_TempAugmentedImages_5(List_1_t1751086428 * value)
	{
		___m_TempAugmentedImages_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempAugmentedImages_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUGMENTEDIMAGEEXAMPLECONTROLLER_T2191676296_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (AugmentedImageExampleController_t2191676296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2300[4] = 
{
	AugmentedImageExampleController_t2191676296::get_offset_of_AugmentedImageVisualizerPrefab_2(),
	AugmentedImageExampleController_t2191676296::get_offset_of_FitToScanOverlay_3(),
	AugmentedImageExampleController_t2191676296::get_offset_of_m_Visualizers_4(),
	AugmentedImageExampleController_t2191676296::get_offset_of_m_TempAugmentedImages_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (AugmentedImageVisualizer_t1303630779), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2301[5] = 
{
	AugmentedImageVisualizer_t1303630779::get_offset_of_Image_2(),
	AugmentedImageVisualizer_t1303630779::get_offset_of_FrameLowerLeft_3(),
	AugmentedImageVisualizer_t1303630779::get_offset_of_FrameLowerRight_4(),
	AugmentedImageVisualizer_t1303630779::get_offset_of_FrameUpperLeft_5(),
	AugmentedImageVisualizer_t1303630779::get_offset_of_FrameUpperRight_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (AnchorIdFromRoomRequestMessage_t2946441754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2302[1] = 
{
	AnchorIdFromRoomRequestMessage_t2946441754::get_offset_of_RoomId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (AnchorIdFromRoomResponseMessage_t1194490927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2303[2] = 
{
	AnchorIdFromRoomResponseMessage_t1194490927::get_offset_of_Found_0(),
	AnchorIdFromRoomResponseMessage_t1194490927::get_offset_of_AnchorId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (ARKitHelper_t1470960758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2304[1] = 
{
	ARKitHelper_t1470960758::get_offset_of_m_HitResultList_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (CloudAnchorController_t754558460), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2305[15] = 
{
	CloudAnchorController_t754558460::get_offset_of_RoomSharingServer_2(),
	CloudAnchorController_t754558460::get_offset_of_UIController_3(),
	CloudAnchorController_t754558460::get_offset_of_ARCoreRoot_4(),
	CloudAnchorController_t754558460::get_offset_of_ARCoreAndyAndroidPrefab_5(),
	CloudAnchorController_t754558460::get_offset_of_ARKitRoot_6(),
	CloudAnchorController_t754558460::get_offset_of_ARKitFirstPersonCamera_7(),
	CloudAnchorController_t754558460::get_offset_of_ARKitAndyAndroidPrefab_8(),
	0,
	0,
	CloudAnchorController_t754558460::get_offset_of_m_ARKit_11(),
	CloudAnchorController_t754558460::get_offset_of_m_IsQuitting_12(),
	CloudAnchorController_t754558460::get_offset_of_m_LastPlacedAnchor_13(),
	CloudAnchorController_t754558460::get_offset_of_m_LastResolvedAnchor_14(),
	CloudAnchorController_t754558460::get_offset_of_m_CurrentMode_15(),
	CloudAnchorController_t754558460::get_offset_of_m_CurrentRoom_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (ApplicationMode_t2292997937)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2306[4] = 
{
	ApplicationMode_t2292997937::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t908968643), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2307[2] = 
{
	U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t908968643::get_offset_of_unityActivity_0(),
	U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t908968643::get_offset_of_message_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (U3C_ShowAndroidToastMessageU3Ec__AnonStorey0_t2865283779), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2308[2] = 
{
	U3C_ShowAndroidToastMessageU3Ec__AnonStorey0_t2865283779::get_offset_of_toastClass_0(),
	U3C_ShowAndroidToastMessageU3Ec__AnonStorey0_t2865283779::get_offset_of_U3CU3Ef__refU241_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (CloudAnchorUIController_t97785020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2309[9] = 
{
	CloudAnchorUIController_t97785020::get_offset_of_SnackbarText_2(),
	CloudAnchorUIController_t97785020::get_offset_of_RoomText_3(),
	CloudAnchorUIController_t97785020::get_offset_of_IPAddressText_4(),
	CloudAnchorUIController_t97785020::get_offset_of_HostAnchorModeButton_5(),
	CloudAnchorUIController_t97785020::get_offset_of_ResolveAnchorModeButton_6(),
	CloudAnchorUIController_t97785020::get_offset_of_InputRoot_7(),
	CloudAnchorUIController_t97785020::get_offset_of_RoomInputField_8(),
	CloudAnchorUIController_t97785020::get_offset_of_IpAddressInputField_9(),
	CloudAnchorUIController_t97785020::get_offset_of_ResolveOnDeviceToggle_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (RoomSharingClient_t3825783562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2310[2] = 
{
	RoomSharingClient_t3825783562::get_offset_of_m_GetAnchorIdFromRoomCallback_24(),
	RoomSharingClient_t3825783562::get_offset_of_m_RoomId_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (GetAnchorIdFromRoomDelegate_t3759139021), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (RoomSharingMsgType_t467542496)+ sizeof (RuntimeObject), sizeof(RoomSharingMsgType_t467542496 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2312[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (RoomSharingServer_t2010038578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2313[1] = 
{
	RoomSharingServer_t2010038578::get_offset_of_m_RoomAnchorsDict_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (DetectedPlaneGenerator_t1310336384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2314[2] = 
{
	DetectedPlaneGenerator_t1310336384::get_offset_of_DetectedPlanePrefab_2(),
	DetectedPlaneGenerator_t1310336384::get_offset_of_m_NewPlanes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (DetectedPlaneVisualizer_t2496913565), -1, sizeof(DetectedPlaneVisualizer_t2496913565_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2315[10] = 
{
	DetectedPlaneVisualizer_t2496913565_StaticFields::get_offset_of_s_PlaneCount_2(),
	DetectedPlaneVisualizer_t2496913565::get_offset_of_k_PlaneColors_3(),
	DetectedPlaneVisualizer_t2496913565::get_offset_of_m_DetectedPlane_4(),
	DetectedPlaneVisualizer_t2496913565::get_offset_of_m_PreviousFrameMeshVertices_5(),
	DetectedPlaneVisualizer_t2496913565::get_offset_of_m_MeshVertices_6(),
	DetectedPlaneVisualizer_t2496913565::get_offset_of_m_PlaneCenter_7(),
	DetectedPlaneVisualizer_t2496913565::get_offset_of_m_MeshColors_8(),
	DetectedPlaneVisualizer_t2496913565::get_offset_of_m_MeshIndices_9(),
	DetectedPlaneVisualizer_t2496913565::get_offset_of_m_Mesh_10(),
	DetectedPlaneVisualizer_t2496913565::get_offset_of_m_MeshRenderer_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (PointcloudVisualizer_t1422988745), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2316[4] = 
{
	0,
	PointcloudVisualizer_t1422988745::get_offset_of_m_Mesh_3(),
	PointcloudVisualizer_t1422988745::get_offset_of_m_Points_4(),
	PointcloudVisualizer_t1422988745::get_offset_of_isVisualize_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (ComputerVisionController_t3046897977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2317[10] = 
{
	ComputerVisionController_t3046897977::get_offset_of_EdgeDetectionBackgroundImage_2(),
	ComputerVisionController_t3046897977::get_offset_of_UseCustomResolutionImage_3(),
	ComputerVisionController_t3046897977::get_offset_of_CameraIntrinsicsOutput_4(),
	ComputerVisionController_t3046897977::get_offset_of_m_EdgeDetectionResultImage_5(),
	ComputerVisionController_t3046897977::get_offset_of_m_EdgeDetectionBackgroundTexture_6(),
	ComputerVisionController_t3046897977::get_offset_of_m_CameraImageToDisplayUvTransformation_7(),
	ComputerVisionController_t3046897977::get_offset_of_m_CachedTextureReader_8(),
	ComputerVisionController_t3046897977::get_offset_of_m_CachedOrientation_9(),
	ComputerVisionController_t3046897977::get_offset_of_m_CachedScreenDimensions_10(),
	ComputerVisionController_t3046897977::get_offset_of_m_IsQuitting_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t3919803072), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2318[2] = 
{
	U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t3919803072::get_offset_of_unityActivity_0(),
	U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t3919803072::get_offset_of_message_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (U3C_ShowAndroidToastMessageU3Ec__AnonStorey0_t3919737536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2319[2] = 
{
	U3C_ShowAndroidToastMessageU3Ec__AnonStorey0_t3919737536::get_offset_of_toastClass_0(),
	U3C_ShowAndroidToastMessageU3Ec__AnonStorey0_t3919737536::get_offset_of_U3CU3Ef__refU241_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (EdgeDetector_t1627390372), -1, sizeof(EdgeDetector_t1627390372_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2320[2] = 
{
	EdgeDetector_t1627390372_StaticFields::get_offset_of_s_ImageBuffer_0(),
	EdgeDetector_t1627390372_StaticFields::get_offset_of_s_ImageBufferSize_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (TextureReader_t2342948801), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2321[10] = 
{
	TextureReader_t2342948801::get_offset_of_ImageWidth_2(),
	TextureReader_t2342948801::get_offset_of_ImageHeight_3(),
	TextureReader_t2342948801::get_offset_of_ImageSampleMode_4(),
	TextureReader_t2342948801::get_offset_of_ImageFormat_5(),
	0,
	0,
	TextureReader_t2342948801::get_offset_of_m_TextureReaderApi_8(),
	TextureReader_t2342948801::get_offset_of_m_Command_9(),
	TextureReader_t2342948801::get_offset_of_m_ImageBufferIndex_10(),
	TextureReader_t2342948801::get_offset_of_OnImageAvailableCallback_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (OnImageAvailableCallbackFunc_t2664114036), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (SampleMode_t259167292)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2323[3] = 
{
	SampleMode_t259167292::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (CommandType_t2410186500)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2324[6] = 
{
	CommandType_t2410186500::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (TextureReaderApi_t3129393145), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (ImageFormatType_t3162659879)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2326[3] = 
{
	ImageFormatType_t3162659879::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (ExternApi_t2048605508)+ sizeof (RuntimeObject), sizeof(ExternApi_t2048605508 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2327[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (HelloARController_t1659088615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2328[7] = 
{
	HelloARController_t1659088615::get_offset_of_FirstPersonCamera_2(),
	HelloARController_t1659088615::get_offset_of_DetectedPlanePrefab_3(),
	HelloARController_t1659088615::get_offset_of_AndyAndroidPrefab_4(),
	HelloARController_t1659088615::get_offset_of_SearchingForPlaneUI_5(),
	0,
	HelloARController_t1659088615::get_offset_of_m_AllPlanes_7(),
	HelloARController_t1659088615::get_offset_of_m_IsQuitting_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t1208848115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2329[2] = 
{
	U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t1208848115::get_offset_of_unityActivity_0(),
	U3C_ShowAndroidToastMessageU3Ec__AnonStorey1_t1208848115::get_offset_of_message_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (U3C_ShowAndroidToastMessageU3Ec__AnonStorey0_t3165163251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2330[2] = 
{
	U3C_ShowAndroidToastMessageU3Ec__AnonStorey0_t3165163251::get_offset_of_toastClass_0(),
	U3C_ShowAndroidToastMessageU3Ec__AnonStorey0_t3165163251::get_offset_of_U3CU3Ef__refU241_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (InstantPreviewInput_t3423396443), -1, sizeof(InstantPreviewInput_t3423396443_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2331[2] = 
{
	InstantPreviewInput_t3423396443_StaticFields::get_offset_of_s_Touches_0(),
	InstantPreviewInput_t3423396443_StaticFields::get_offset_of_s_TouchList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (NativeTouch_t337753862)+ sizeof (RuntimeObject), sizeof(NativeTouch_t337753862 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2332[5] = 
{
	NativeTouch_t337753862::get_offset_of_Phase_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NativeTouch_t337753862::get_offset_of_X_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NativeTouch_t337753862::get_offset_of_Y_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NativeTouch_t337753862::get_offset_of_Pressure_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NativeTouch_t337753862::get_offset_of_Id_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (NativeApi_t4275999757)+ sizeof (RuntimeObject), sizeof(NativeApi_t4275999757 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (U3CUpdateU3Ec__AnonStorey0_t3581202692), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2334[1] = 
{
	U3CUpdateU3Ec__AnonStorey0_t3581202692::get_offset_of_newTouch_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (InstantPreviewManager_t448487770), -1, sizeof(InstantPreviewManager_t448487770_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2335[9] = 
{
	0,
	0,
	0,
	0,
	0,
	InstantPreviewManager_t448487770_StaticFields::get_offset_of_k_WaitForEndOfFrame_5(),
	InstantPreviewManager_t448487770_StaticFields::get_offset_of_s_PauseWarned_6(),
	InstantPreviewManager_t448487770_StaticFields::get_offset_of_s_DisabledLightEstimationWarned_7(),
	InstantPreviewManager_t448487770_StaticFields::get_offset_of_s_DisabledPlaneFindingWarned_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (NativeApi_t4070010807)+ sizeof (RuntimeObject), sizeof(NativeApi_t4070010807 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (Result_t3334322572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2337[1] = 
{
	Result_t3334322572::get_offset_of_ShouldPromptForInstall_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (U3CInitializeIfNeededU3Ec__Iterator0_t700312033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2338[5] = 
{
	U3CInitializeIfNeededU3Ec__Iterator0_t700312033::get_offset_of_U3CadbPathU3E__0_0(),
	U3CInitializeIfNeededU3Ec__Iterator0_t700312033::get_offset_of_U3ClocalVersionU3E__0_1(),
	U3CInitializeIfNeededU3Ec__Iterator0_t700312033::get_offset_of_U24current_2(),
	U3CInitializeIfNeededU3Ec__Iterator0_t700312033::get_offset_of_U24disposing_3(),
	U3CInitializeIfNeededU3Ec__Iterator0_t700312033::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (U3CUpdateLoopU3Ec__Iterator1_t1915294616), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2339[18] = 
{
	U3CUpdateLoopU3Ec__Iterator1_t1915294616::get_offset_of_U3CrenderEventFuncU3E__0_0(),
	U3CUpdateLoopU3Ec__Iterator1_t1915294616::get_offset_of_U3CshouldConvertToBgraU3E__0_1(),
	U3CUpdateLoopU3Ec__Iterator1_t1915294616::get_offset_of_U3CloggedAspectRatioWarningU3E__0_2(),
	U3CUpdateLoopU3Ec__Iterator1_t1915294616::get_offset_of_U3CcurrentWidthU3E__0_3(),
	U3CUpdateLoopU3Ec__Iterator1_t1915294616::get_offset_of_U3CcurrentHeightU3E__0_4(),
	U3CUpdateLoopU3Ec__Iterator1_t1915294616::get_offset_of_U3CneedToStartActivityU3E__0_5(),
	U3CUpdateLoopU3Ec__Iterator1_t1915294616::get_offset_of_U3CprevFrameLandscapeU3E__0_6(),
	U3CUpdateLoopU3Ec__Iterator1_t1915294616::get_offset_of_U3CscreenTextureU3E__0_7(),
	U3CUpdateLoopU3Ec__Iterator1_t1915294616::get_offset_of_U3CtargetTextureU3E__0_8(),
	U3CUpdateLoopU3Ec__Iterator1_t1915294616::get_offset_of_U3CbgrTextureU3E__0_9(),
	U3CUpdateLoopU3Ec__Iterator1_t1915294616::get_offset_of_U3CcurFrameLandscapeU3E__1_10(),
	U3CUpdateLoopU3Ec__Iterator1_t1915294616::get_offset_of_adbPath_11(),
	U3CUpdateLoopU3Ec__Iterator1_t1915294616::get_offset_of_U3CtargetWidthU3E__1_12(),
	U3CUpdateLoopU3Ec__Iterator1_t1915294616::get_offset_of_U3CtargetHeightU3E__1_13(),
	U3CUpdateLoopU3Ec__Iterator1_t1915294616::get_offset_of_U3CcameraTextureU3E__1_14(),
	U3CUpdateLoopU3Ec__Iterator1_t1915294616::get_offset_of_U24current_15(),
	U3CUpdateLoopU3Ec__Iterator1_t1915294616::get_offset_of_U24disposing_16(),
	U3CUpdateLoopU3Ec__Iterator1_t1915294616::get_offset_of_U24PC_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (U3CInstallApkAndRunIfConnectedU3Ec__Iterator2_t1973695815), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2340[9] = 
{
	U3CInstallApkAndRunIfConnectedU3Ec__Iterator2_t1973695815::get_offset_of_U3CresultU3E__0_0(),
	U3CInstallApkAndRunIfConnectedU3Ec__Iterator2_t1973695815::get_offset_of_adbPath_1(),
	U3CInstallApkAndRunIfConnectedU3Ec__Iterator2_t1973695815::get_offset_of_localVersion_2(),
	U3CInstallApkAndRunIfConnectedU3Ec__Iterator2_t1973695815::get_offset_of_U3CcheckAdbThreadU3E__0_3(),
	U3CInstallApkAndRunIfConnectedU3Ec__Iterator2_t1973695815::get_offset_of_U3CinstallThreadU3E__1_4(),
	U3CInstallApkAndRunIfConnectedU3Ec__Iterator2_t1973695815::get_offset_of_U24current_5(),
	U3CInstallApkAndRunIfConnectedU3Ec__Iterator2_t1973695815::get_offset_of_U24disposing_6(),
	U3CInstallApkAndRunIfConnectedU3Ec__Iterator2_t1973695815::get_offset_of_U24PC_7(),
	U3CInstallApkAndRunIfConnectedU3Ec__Iterator2_t1973695815::get_offset_of_U24locvar0_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (U3CInstallApkAndRunIfConnectedU3Ec__AnonStorey3_t1278641102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2341[3] = 
{
	U3CInstallApkAndRunIfConnectedU3Ec__AnonStorey3_t1278641102::get_offset_of_adbPath_0(),
	U3CInstallApkAndRunIfConnectedU3Ec__AnonStorey3_t1278641102::get_offset_of_apkPath_1(),
	U3CInstallApkAndRunIfConnectedU3Ec__AnonStorey3_t1278641102::get_offset_of_localVersion_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (InstantPreviewTrackedPoseDriver_t3813668419), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (Anchor_t769518176), -1, sizeof(Anchor_t769518176_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2343[5] = 
{
	Anchor_t769518176_StaticFields::get_offset_of_s_AnchorDict_2(),
	Anchor_t769518176::get_offset_of_m_LastFrameTrackingState_3(),
	Anchor_t769518176::get_offset_of_m_IsSessionDestroyed_4(),
	Anchor_t769518176::get_offset_of_U3Cm_NativeSessionU3Ek__BackingField_5(),
	Anchor_t769518176::get_offset_of_U3Cm_NativeHandleU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (AndroidPermissionsManager_t3082189368), -1, sizeof(AndroidPermissionsManager_t3082189368_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2344[5] = 
{
	AndroidPermissionsManager_t3082189368_StaticFields::get_offset_of_s_Instance_4(),
	AndroidPermissionsManager_t3082189368_StaticFields::get_offset_of_s_Activity_5(),
	AndroidPermissionsManager_t3082189368_StaticFields::get_offset_of_s_PermissionService_6(),
	AndroidPermissionsManager_t3082189368_StaticFields::get_offset_of_s_CurrentRequest_7(),
	AndroidPermissionsManager_t3082189368_StaticFields::get_offset_of_s_OnPermissionsRequestFinished_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (AndroidPermissionsRequestResult_t3910302873)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2345[2] = 
{
	AndroidPermissionsRequestResult_t3910302873::get_offset_of_U3CPermissionNamesU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AndroidPermissionsRequestResult_t3910302873::get_offset_of_U3CGrantResultsU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (AnchorApi_t2165463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2346[1] = 
{
	AnchorApi_t2165463::get_offset_of_m_NativeSession_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (ExternApi_t2991644367)+ sizeof (RuntimeObject), sizeof(ExternApi_t2991644367 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (ApiApkInstallationStatus_t33526722)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2348[7] = 
{
	ApiApkInstallationStatus_t33526722::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (ApiApkInstallStatusExtensions_t1203352906), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (ApiArStatus_t1919843852)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2350[26] = 
{
	ApiArStatus_t1919843852::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (ApiArStatusExtensions_t2816364904), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (ApiAvailability_t1149954610)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2352[8] = 
{
	ApiAvailability_t1149954610::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (ApiAvailabilityExtensions_t1605762449), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (NdkCameraMetadataType_t306478796)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2354[8] = 
{
	NdkCameraMetadataType_t306478796::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (NdkCameraStatus_t2797764638)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2355[6] = 
{
	NdkCameraStatus_t2797764638::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (NdkCameraMetadata_t2368224975)+ sizeof (RuntimeObject), sizeof(NdkCameraMetadata_t2368224975_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2356[4] = 
{
	NdkCameraMetadata_t2368224975::get_offset_of_Tag_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NdkCameraMetadata_t2368224975::get_offset_of_Type_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NdkCameraMetadata_t2368224975::get_offset_of_Count_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NdkCameraMetadata_t2368224975::get_offset_of_Value_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (ApiCloudAnchorMode_t1322772406)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2357[3] = 
{
	ApiCloudAnchorMode_t1322772406::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (ApiCloudAnchorState_t449523374)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2358[13] = 
{
	ApiCloudAnchorState_t449523374::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (ApiCloudAnchorStateExtensions_t2681117418), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (ApiConstants_t3669054205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2360[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (ApiDisplayUvCoords_t2116852691)+ sizeof (RuntimeObject), sizeof(ApiDisplayUvCoords_t2116852691 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2361[5] = 
{
	0,
	ApiDisplayUvCoords_t2116852691::get_offset_of_TopLeft_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ApiDisplayUvCoords_t2116852691::get_offset_of_TopRight_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ApiDisplayUvCoords_t2116852691::get_offset_of_BottomLeft_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ApiDisplayUvCoords_t2116852691::get_offset_of_BottomRight_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (ApiDisplayUvCoordsExtensions_t1347642370), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (ApiFeaturePointOrientationMode_t2490105243)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2363[3] = 
{
	ApiFeaturePointOrientationMode_t2490105243::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (ApiFeaturePointOrientationModeExtensions_t2507422198), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (ApiLightEstimateState_t4039747232)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2365[3] = 
{
	ApiLightEstimateState_t4039747232::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (ApiLightEstimateStateExtensions_t164677992), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (ApiLightEstimationMode_t4137554712)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2367[3] = 
{
	ApiLightEstimationMode_t4137554712::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (ApiPlaneFindingMode_t590523534)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2368[5] = 
{
	ApiPlaneFindingMode_t590523534::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (ApiPoseData_t4278576221)+ sizeof (RuntimeObject), sizeof(ApiPoseData_t4278576221 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2369[7] = 
{
	ApiPoseData_t4278576221::get_offset_of_Qx_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ApiPoseData_t4278576221::get_offset_of_Qy_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ApiPoseData_t4278576221::get_offset_of_Qz_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ApiPoseData_t4278576221::get_offset_of_Qw_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ApiPoseData_t4278576221::get_offset_of_X_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ApiPoseData_t4278576221::get_offset_of_Y_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ApiPoseData_t4278576221::get_offset_of_Z_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (ApiPrestoConfig_t2282708041)+ sizeof (RuntimeObject), sizeof(ApiPrestoConfig_t2282708041 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2370[6] = 
{
	ApiPrestoConfig_t2282708041::get_offset_of_UpdateMode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ApiPrestoConfig_t2282708041::get_offset_of_PlaneFindingMode_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ApiPrestoConfig_t2282708041::get_offset_of_LightEstimationMode_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ApiPrestoConfig_t2282708041::get_offset_of_CloudAnchorMode_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ApiPrestoConfig_t2282708041::get_offset_of_AugmentedImageDatabaseBytes_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ApiPrestoConfig_t2282708041::get_offset_of_AugmentedImageDatabaseSize_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (ApiPrestoStatus_t902283214)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2371[11] = 
{
	ApiPrestoStatus_t902283214::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (ApiPrestoStatusExtensions_t2873628449), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (ApiTrackableType_t702053436)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2373[6] = 
{
	ApiTrackableType_t702053436::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (ApiTrackingState_t3318626383)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2374[4] = 
{
	ApiTrackingState_t3318626383::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (ApiTrackingStateExtensions_t806433345), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (ApiTrackingStateXPExtensions_t102880617), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (ApiUpdateMode_t3440359817)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2377[3] = 
{
	ApiUpdateMode_t3440359817::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (AugmentedImageApi_t1163146062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2378[1] = 
{
	AugmentedImageApi_t1163146062::get_offset_of_m_NativeSession_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (ExternApi_t181214333)+ sizeof (RuntimeObject), sizeof(ExternApi_t181214333 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (AugmentedImageDatabaseApi_t4243295728), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2380[1] = 
{
	AugmentedImageDatabaseApi_t4243295728::get_offset_of_m_NativeSession_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (ExternApi_t4165057079)+ sizeof (RuntimeObject), sizeof(ExternApi_t4165057079 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (CameraApi_t274148848), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2382[1] = 
{
	CameraApi_t274148848::get_offset_of_m_NativeSession_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (ExternApi_t2424721670)+ sizeof (RuntimeObject), sizeof(ExternApi_t2424721670 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (CameraMetadataApi_t4067407319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2384[1] = 
{
	CameraMetadataApi_t4067407319::get_offset_of_m_NativeSession_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (ExternApi_t3709617879)+ sizeof (RuntimeObject), sizeof(ExternApi_t3709617879 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (FrameApi_t2778645972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2386[1] = 
{
	FrameApi_t2778645972::get_offset_of_m_NativeSession_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (ExternApi_t903494213)+ sizeof (RuntimeObject), sizeof(ExternApi_t903494213 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (HitTestApi_t363854212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2388[1] = 
{
	HitTestApi_t363854212::get_offset_of_m_NativeSession_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (ExternApi_t1588697414)+ sizeof (RuntimeObject), sizeof(ExternApi_t1588697414 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (ImageApi_t1356767549), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2390[1] = 
{
	ImageApi_t1356767549::get_offset_of_m_NativeSession_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (ExternApi_t1609953988)+ sizeof (RuntimeObject), sizeof(ExternApi_t1609953988 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (LightEstimateApi_t745071092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2392[1] = 
{
	LightEstimateApi_t745071092::get_offset_of_m_NativeSession_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (ExternApi_t3383842396)+ sizeof (RuntimeObject), sizeof(ExternApi_t3383842396 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (NativeSession_t1550589577), -1, sizeof(NativeSession_t1550589577_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2394[24] = 
{
	NativeSession_t1550589577_StaticFields::get_offset_of_s_ReportedEngineType_0(),
	NativeSession_t1550589577::get_offset_of_m_LastReleasedPointcloudTimestamp_1(),
	NativeSession_t1550589577::get_offset_of_m_TrackableManager_2(),
	NativeSession_t1550589577::get_offset_of_m_AcquiredHandleCounts_3(),
	NativeSession_t1550589577::get_offset_of_U3CSessionHandleU3Ek__BackingField_4(),
	NativeSession_t1550589577::get_offset_of_U3CFrameHandleU3Ek__BackingField_5(),
	NativeSession_t1550589577::get_offset_of_U3CPointCloudHandleU3Ek__BackingField_6(),
	NativeSession_t1550589577::get_offset_of_U3CAnchorApiU3Ek__BackingField_7(),
	NativeSession_t1550589577::get_offset_of_U3CAugmentedImageApiU3Ek__BackingField_8(),
	NativeSession_t1550589577::get_offset_of_U3CAugmentedImageDatabaseApiU3Ek__BackingField_9(),
	NativeSession_t1550589577::get_offset_of_U3CCameraApiU3Ek__BackingField_10(),
	NativeSession_t1550589577::get_offset_of_U3CCameraMetadataApiU3Ek__BackingField_11(),
	NativeSession_t1550589577::get_offset_of_U3CFrameApiU3Ek__BackingField_12(),
	NativeSession_t1550589577::get_offset_of_U3CHitTestApiU3Ek__BackingField_13(),
	NativeSession_t1550589577::get_offset_of_U3CImageApiU3Ek__BackingField_14(),
	NativeSession_t1550589577::get_offset_of_U3CLightEstimateApiU3Ek__BackingField_15(),
	NativeSession_t1550589577::get_offset_of_U3CPlaneApiU3Ek__BackingField_16(),
	NativeSession_t1550589577::get_offset_of_U3CPointApiU3Ek__BackingField_17(),
	NativeSession_t1550589577::get_offset_of_U3CPointCloudApiU3Ek__BackingField_18(),
	NativeSession_t1550589577::get_offset_of_U3CPoseApiU3Ek__BackingField_19(),
	NativeSession_t1550589577::get_offset_of_U3CSessionApiU3Ek__BackingField_20(),
	NativeSession_t1550589577::get_offset_of_U3CSessionConfigApiU3Ek__BackingField_21(),
	NativeSession_t1550589577::get_offset_of_U3CTrackableApiU3Ek__BackingField_22(),
	NativeSession_t1550589577::get_offset_of_U3CTrackableListApiU3Ek__BackingField_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { sizeof (PlaneApi_t1746941456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2395[4] = 
{
	0,
	PlaneApi_t1746941456::get_offset_of_m_NativeSession_1(),
	PlaneApi_t1746941456::get_offset_of_m_TmpPoints_2(),
	PlaneApi_t1746941456::get_offset_of_m_TmpPointsHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (ExternApi_t1430713662)+ sizeof (RuntimeObject), sizeof(ExternApi_t1430713662 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (PointApi_t3912385759), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2397[1] = 
{
	PointApi_t3912385759::get_offset_of_m_NativeSession_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (ExternApi_t1551582343)+ sizeof (RuntimeObject), sizeof(ExternApi_t1551582343 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { sizeof (PointCloudApi_t650451515), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2399[2] = 
{
	PointCloudApi_t650451515::get_offset_of_m_NativeSession_0(),
	PointCloudApi_t650451515::get_offset_of_m_CachedVector_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
