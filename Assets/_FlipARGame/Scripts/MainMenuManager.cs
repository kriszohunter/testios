﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour {

    [Header("Main Menu Elements")]
    public GameObject startButton;
    public Text startBtnText;
    public GameObject mainMenuPanel;
    public GameObject selectionPanel;
    public GameObject LoadingObj;
    public GameObject LoadingPanel;
    public GameObject LogoBtn;
    public Text CharacterNameText;
    public string[] characternames;
    public Material[] CharacterMaterials;
    [Header("Other")]
    public GameObject[] characters;
    public Vector3 cameraOffset;
    public float scrollTime = 5f;
    public float distanceBtwCharacters = 5f;

    //IOS ONLY
    public void VerifyPermission()
    {
        Debug.Log("VerifyPermission()");

        // Use native UI to request camera permission.
        iOSCameraPermission.VerifyPermission(gameObject.name, "SampleCallback");
    }
    private void SampleCallback(string permissionWasGranted)
    {
        Debug.Log("Callback.permissionWasGranted = " + permissionWasGranted);

        if (permissionWasGranted == "true")
        {
            Debug.LogError("****** Camera Permission granted");
            startButton.GetComponent<Button>().enabled = true;
            startBtnText.text = "Tap to start";
        }
        else
        {
            Debug.LogError("****** Camera Permission NOT granted");
            startButton.GetComponent<Button>().enabled = false;
            startBtnText.text = "Please grant camera access to Flip Game from settings to proceed";
        }
    }

    #region Mono Fucntions    \

    public void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            Debug.Log("** app is paused");
        }
        else
        {
            Debug.Log("** app is Resumed");
            if (Application.platform == RuntimePlatform.IPhonePlayer)
                VerifyPermission();
        }
    }

    void Start () {

        if (Application.platform == RuntimePlatform.IPhonePlayer)
            VerifyPermission();

        GameManager.Instance.selectedCharacterNumber = 1;
        if (GameManager.Instance.isFromGamePlayToMainMenu)
        {
            mainMenuPanel.SetActive(false);
            selectionPanel.SetActive(true);
            characters[GameManager.Instance.selectedCharacterNumber - 1].SetActive(true);            
        }
        else
        {
            mainMenuPanel.SetActive(true);
            selectionPanel.SetActive(false);
        }
        Debug.Log("Start of Main MEnu RUN");
        CharacterNameText.text = characternames[GameManager.Instance.selectedCharacterNumber - 1];
        foreach (Material item in CharacterMaterials)
        {
            item.SetFloat("_Transparency", 0);
        }
	}
    void Update()
    {
        if (selectionPanel.activeInHierarchy)
        {
            Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, characters[GameManager.Instance.selectedCharacterNumber - 1].transform.position + cameraOffset, Time.deltaTime * scrollTime);
            Camera.main.transform.rotation = characters[GameManager.Instance.selectedCharacterNumber - 1].transform.rotation;
        }
    }
    #endregion


    #region Main Menu Functions

    public void OnLogoAnimationComplete () {
        startButton.SetActive(true);
        LoadingObj.SetActive(false);
        //iTween.MoveTo(go.gameObject, iTween.Hash("position", new Vector3(myOBJ.transform.position.x - 0.05f, myOBJ.transform.position.y, myOBJ.transform.position.z), "time", 0.8f, "easeType", iTween.EaseType.easeOutCirc));
	}
    public void OnStartBtnClick()
    {
        LogoBtn.GetComponent<Animator>().Play("LogoHide");
        StartCoroutine(OnStartBtnClickE());       
    }
    IEnumerator OnStartBtnClickE()
    {
        yield return new WaitForSeconds(0.6f);
        mainMenuPanel.SetActive(false);
        selectionPanel.SetActive(true);
        characters[GameManager.Instance.selectedCharacterNumber - 1].SetActive(true);
    }

    #endregion

    #region Selection Menu Functions
    public void OnRightBtnClick()
    {
        //characters[GameManager.Instance.selectedCharacterNumber-1].SetActive(false);
        GameManager.Instance.selectedCharacterNumber++;
        if (GameManager.Instance.selectedCharacterNumber == characters.Length+1)
        {
            GameManager.Instance.selectedCharacterNumber = 1;
            characters[GameManager.Instance.selectedCharacterNumber - 1].transform.position = Vector3.right * (characters[characters.Length - 1].transform.position.x + distanceBtwCharacters);
        }
        else
            characters[GameManager.Instance.selectedCharacterNumber-1].transform.position = Vector3.right* (characters[GameManager.Instance.selectedCharacterNumber-2].transform.position.x + distanceBtwCharacters);
        characters[GameManager.Instance.selectedCharacterNumber-1].SetActive(true);
        activePos = characters[GameManager.Instance.selectedCharacterNumber - 1].transform.position;
        CharacterNameText.text = characternames[GameManager.Instance.selectedCharacterNumber - 1];
    }
    public void OnLeftBtnClick()
    {
        //characters[GameManager.Instance.selectedCharacterNumber - 1].SetActive(false);
        GameManager.Instance.selectedCharacterNumber--;
     
        if (GameManager.Instance.selectedCharacterNumber == 0)
        {
            GameManager.Instance.selectedCharacterNumber = characters.Length;
            characters[GameManager.Instance.selectedCharacterNumber - 1].transform.position = Vector3.right * (activePos.x - distanceBtwCharacters);
        }
        else
            characters[GameManager.Instance.selectedCharacterNumber - 1].transform.position = Vector3.right * (activePos.x - distanceBtwCharacters);
        characters[GameManager.Instance.selectedCharacterNumber - 1].SetActive(true);
        activePos = characters[GameManager.Instance.selectedCharacterNumber - 1].transform.position;
        CharacterNameText.text = characternames[GameManager.Instance.selectedCharacterNumber - 1];
    }
    Vector3 activePos = Vector3.zero;
   
    public void OnSelectBtnClick()
    {
        foreach (GameObject item in characters)
        {
            item.SetActive(false);
        }
        LoadingPanel.SetActive(true);
        StartCoroutine(OnSelectBtnClickE());
    }
    IEnumerator OnSelectBtnClickE()
    {
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);      
    }
    #endregion






}
