﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Byte[]
struct ByteU5BU5D_t4116647657;
// Utils.serializableUnityARMatrix4x4
struct serializableUnityARMatrix4x4_t78255337;
// Utils.serializableFaceGeometry
struct serializableFaceGeometry_t157334219;
// System.Collections.Generic.Dictionary`2<System.String,System.Single>
struct Dictionary_2_t1182523073;
// TKAnyTouchRecognizer
struct TKAnyTouchRecognizer_t3651961449;
// TKButtonRecognizer
struct TKButtonRecognizer_t691501956;
// TKCurveRecognizer
struct TKCurveRecognizer_t1881817312;
// DemoOne
struct DemoOne_t3048645529;
// TKOneFingerRotationRecognizer
struct TKOneFingerRotationRecognizer_t564097970;
// Utils.SerializableVector4
struct SerializableVector4_t1862640084;
// TKPinchRecognizer
struct TKPinchRecognizer_t788645703;
// TKRotationRecognizer
struct TKRotationRecognizer_t651141287;
// UnityEngine.MeshCollider
struct MeshCollider_t903564387;
// UnityEngine.MeshFilter
struct MeshFilter_t3523625662;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// TKLongPressRecognizer
struct TKLongPressRecognizer_t1475862313;
// System.String
struct String_t;
// TKPanRecognizer
struct TKPanRecognizer_t1603940453;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>
struct Dictionary_2_t1732976114;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Char[]
struct CharU5BU5D_t3528271667;
// UnityEngine.Sprite
struct Sprite_t280657092;
// System.Void
struct Void_t1185182177;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t386037858;
// Utils.serializableARKitInit
struct serializableARKitInit_t3885066048;
// System.Collections.Generic.List`1<TKTouch>
struct List_1_t34712449;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;
// Utils.serializableARSessionConfiguration
struct serializableARSessionConfiguration_t1467016906;
// Utils.serializableUnityARLightData
struct serializableUnityARLightData_t3935513283;
// Utils.serializablePointCloud
struct serializablePointCloud_t455238287;
// Utils.serializableSHC
struct serializableSHC_t2667429767;
// UnityEngine.XR.iOS.ARFaceAnchor/DictionaryVisitorHandler
struct DictionaryVisitorHandler_t414487210;
// System.Action`1<TKButtonRecognizer>
struct Action_1_t863969551;
// System.Action`1<TKTapRecognizer>
struct Action_1_t2562972046;
// System.Action`1<TKCurveRecognizer>
struct Action_1_t2054284907;
// System.Action`1<TKLongPressRecognizer>
struct Action_1_t1648329908;
// System.Action`1<TKPanRecognizer>
struct Action_1_t1776408048;
// System.Action`1<TKPinchRecognizer>
struct Action_1_t961113298;
// System.Action`1<TKRotationRecognizer>
struct Action_1_t823608882;
// System.Action`1<TKSwipeRecognizer>
struct Action_1_t3383006765;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// System.Action`1<TKTouchPadRecognizer>
struct Action_1_t1666246729;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// System.Action`1<TKOneFingerRotationRecognizer>
struct Action_1_t736565565;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t412400163;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_t245602842;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t2206337031;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.XR.iOS.ConnectToEditor
struct ConnectToEditor_t595742893;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface
struct UnityARSessionNativeInterface_t3929719369;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.Collections.Generic.List`1<TKAbstractGestureRecognizer>
struct List_1_t3375847081;
// TKTouch[]
struct TKTouchU5BU5D_t3441859050;
// System.Collections.Generic.List`1<Prime31.TKLTouch>
struct List_1_t661897764;
// Prime31.TKLTouch[]
struct TKLTouchU5BU5D_t3381730651;
// System.Action`1<Prime31.SwipeDirection>
struct Action_1_t3637933324;
// VirtualControls
struct VirtualControls_t3611233992;
// System.Action`1<TKAngleSwipeRecognizer>
struct Action_1_t3839277408;
// System.Action`1<TKAnyTouchRecognizer>
struct Action_1_t3824429044;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.Networking.PlayerConnection.PlayerConnection
struct PlayerConnection_t3081694049;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t3213117958;
// ColorPicker
struct ColorPicker_t228004619;
// UnityEngine.UI.BoxSlider
struct BoxSlider_t2380464200;
// UnityEngine.UI.RawImage
struct RawImage_t3182918964;
// UnityEngine.Light[]
struct LightU5BU5D_t3678959411;
// UnityEngine.XR.iOS.UnityARSessionRunOption[]
struct UnityARSessionRunOptionU5BU5D_t4225291891;
// UnityEngine.XR.iOS.UnityARAlignment[]
struct UnityARAlignmentU5BU5D_t3682394155;
// UnityEngine.XR.iOS.UnityARPlaneDetection[]
struct UnityARPlaneDetectionU5BU5D_t3458580926;
// PlaneVisualizationManager
struct PlaneVisualizationManager_t1447224982;
// UnityEngine.XR.iOS.UnityARAnchorManager
struct UnityARAnchorManager_t1557554123;
// UnityEngine.Light
struct Light_t3756812086;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1800779281;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_t3069227754;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<UnityEngine.ParticleSystem>
struct List_1_t3272854023;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.UI.Image
struct Image_t2670269651;
// ColorChangedEvent
struct ColorChangedEvent_t3019780707;
// HSVChangedEvent
struct HSVChangedEvent_t911780251;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t427135887;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2532145056;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1260619206;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.UI.BoxSlider/BoxSliderEvent
struct BoxSliderEvent_t439394298;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef OBJECTSERIALIZATIONEXTENSION_T1046383205_H
#define OBJECTSERIALIZATIONEXTENSION_T1046383205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.ObjectSerializationExtension
struct  ObjectSerializationExtension_t1046383205  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTSERIALIZATIONEXTENSION_T1046383205_H
#ifndef SERIALIZABLEVECTOR4_T1862640084_H
#define SERIALIZABLEVECTOR4_T1862640084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.SerializableVector4
struct  SerializableVector4_t1862640084  : public RuntimeObject
{
public:
	// System.Single Utils.SerializableVector4::x
	float ___x_0;
	// System.Single Utils.SerializableVector4::y
	float ___y_1;
	// System.Single Utils.SerializableVector4::z
	float ___z_2;
	// System.Single Utils.SerializableVector4::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SerializableVector4_t1862640084, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(SerializableVector4_t1862640084, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(SerializableVector4_t1862640084, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(SerializableVector4_t1862640084, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEVECTOR4_T1862640084_H
#ifndef HSVUTIL_T1472193074_H
#define HSVUTIL_T1472193074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HSVUtil
struct  HSVUtil_t1472193074  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVUTIL_T1472193074_H
#ifndef SERIALIZABLEFACEGEOMETRY_T157334219_H
#define SERIALIZABLEFACEGEOMETRY_T157334219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableFaceGeometry
struct  serializableFaceGeometry_t157334219  : public RuntimeObject
{
public:
	// System.Byte[] Utils.serializableFaceGeometry::vertices
	ByteU5BU5D_t4116647657* ___vertices_0;
	// System.Byte[] Utils.serializableFaceGeometry::texCoords
	ByteU5BU5D_t4116647657* ___texCoords_1;
	// System.Byte[] Utils.serializableFaceGeometry::triIndices
	ByteU5BU5D_t4116647657* ___triIndices_2;

public:
	inline static int32_t get_offset_of_vertices_0() { return static_cast<int32_t>(offsetof(serializableFaceGeometry_t157334219, ___vertices_0)); }
	inline ByteU5BU5D_t4116647657* get_vertices_0() const { return ___vertices_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_vertices_0() { return &___vertices_0; }
	inline void set_vertices_0(ByteU5BU5D_t4116647657* value)
	{
		___vertices_0 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_0), value);
	}

	inline static int32_t get_offset_of_texCoords_1() { return static_cast<int32_t>(offsetof(serializableFaceGeometry_t157334219, ___texCoords_1)); }
	inline ByteU5BU5D_t4116647657* get_texCoords_1() const { return ___texCoords_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_texCoords_1() { return &___texCoords_1; }
	inline void set_texCoords_1(ByteU5BU5D_t4116647657* value)
	{
		___texCoords_1 = value;
		Il2CppCodeGenWriteBarrier((&___texCoords_1), value);
	}

	inline static int32_t get_offset_of_triIndices_2() { return static_cast<int32_t>(offsetof(serializableFaceGeometry_t157334219, ___triIndices_2)); }
	inline ByteU5BU5D_t4116647657* get_triIndices_2() const { return ___triIndices_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_triIndices_2() { return &___triIndices_2; }
	inline void set_triIndices_2(ByteU5BU5D_t4116647657* value)
	{
		___triIndices_2 = value;
		Il2CppCodeGenWriteBarrier((&___triIndices_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEFACEGEOMETRY_T157334219_H
#ifndef SERIALIZABLEUNITYARFACEANCHOR_T2162490026_H
#define SERIALIZABLEUNITYARFACEANCHOR_T2162490026_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableUnityARFaceAnchor
struct  serializableUnityARFaceAnchor_t2162490026  : public RuntimeObject
{
public:
	// Utils.serializableUnityARMatrix4x4 Utils.serializableUnityARFaceAnchor::worldTransform
	serializableUnityARMatrix4x4_t78255337 * ___worldTransform_0;
	// Utils.serializableFaceGeometry Utils.serializableUnityARFaceAnchor::faceGeometry
	serializableFaceGeometry_t157334219 * ___faceGeometry_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Single> Utils.serializableUnityARFaceAnchor::arBlendShapes
	Dictionary_2_t1182523073 * ___arBlendShapes_2;
	// System.Byte[] Utils.serializableUnityARFaceAnchor::identifierStr
	ByteU5BU5D_t4116647657* ___identifierStr_3;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(serializableUnityARFaceAnchor_t2162490026, ___worldTransform_0)); }
	inline serializableUnityARMatrix4x4_t78255337 * get_worldTransform_0() const { return ___worldTransform_0; }
	inline serializableUnityARMatrix4x4_t78255337 ** get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(serializableUnityARMatrix4x4_t78255337 * value)
	{
		___worldTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___worldTransform_0), value);
	}

	inline static int32_t get_offset_of_faceGeometry_1() { return static_cast<int32_t>(offsetof(serializableUnityARFaceAnchor_t2162490026, ___faceGeometry_1)); }
	inline serializableFaceGeometry_t157334219 * get_faceGeometry_1() const { return ___faceGeometry_1; }
	inline serializableFaceGeometry_t157334219 ** get_address_of_faceGeometry_1() { return &___faceGeometry_1; }
	inline void set_faceGeometry_1(serializableFaceGeometry_t157334219 * value)
	{
		___faceGeometry_1 = value;
		Il2CppCodeGenWriteBarrier((&___faceGeometry_1), value);
	}

	inline static int32_t get_offset_of_arBlendShapes_2() { return static_cast<int32_t>(offsetof(serializableUnityARFaceAnchor_t2162490026, ___arBlendShapes_2)); }
	inline Dictionary_2_t1182523073 * get_arBlendShapes_2() const { return ___arBlendShapes_2; }
	inline Dictionary_2_t1182523073 ** get_address_of_arBlendShapes_2() { return &___arBlendShapes_2; }
	inline void set_arBlendShapes_2(Dictionary_2_t1182523073 * value)
	{
		___arBlendShapes_2 = value;
		Il2CppCodeGenWriteBarrier((&___arBlendShapes_2), value);
	}

	inline static int32_t get_offset_of_identifierStr_3() { return static_cast<int32_t>(offsetof(serializableUnityARFaceAnchor_t2162490026, ___identifierStr_3)); }
	inline ByteU5BU5D_t4116647657* get_identifierStr_3() const { return ___identifierStr_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_identifierStr_3() { return &___identifierStr_3; }
	inline void set_identifierStr_3(ByteU5BU5D_t4116647657* value)
	{
		___identifierStr_3 = value;
		Il2CppCodeGenWriteBarrier((&___identifierStr_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEUNITYARFACEANCHOR_T2162490026_H
#ifndef SUBMESSAGEIDS_T1008824323_H
#define SUBMESSAGEIDS_T1008824323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.SubMessageIds
struct  SubMessageIds_t1008824323  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBMESSAGEIDS_T1008824323_H
#ifndef CONNECTIONMESSAGEIDS_T1387126779_H
#define CONNECTIONMESSAGEIDS_T1387126779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ConnectionMessageIds
struct  ConnectionMessageIds_t1387126779  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONMESSAGEIDS_T1387126779_H
#ifndef SERIALIZABLEPOINTCLOUD_T455238287_H
#define SERIALIZABLEPOINTCLOUD_T455238287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializablePointCloud
struct  serializablePointCloud_t455238287  : public RuntimeObject
{
public:
	// System.Byte[] Utils.serializablePointCloud::pointCloudData
	ByteU5BU5D_t4116647657* ___pointCloudData_0;

public:
	inline static int32_t get_offset_of_pointCloudData_0() { return static_cast<int32_t>(offsetof(serializablePointCloud_t455238287, ___pointCloudData_0)); }
	inline ByteU5BU5D_t4116647657* get_pointCloudData_0() const { return ___pointCloudData_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_pointCloudData_0() { return &___pointCloudData_0; }
	inline void set_pointCloudData_0(ByteU5BU5D_t4116647657* value)
	{
		___pointCloudData_0 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudData_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEPOINTCLOUD_T455238287_H
#ifndef VIRTUALCONTROLS_T3611233992_H
#define VIRTUALCONTROLS_T3611233992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VirtualControls
struct  VirtualControls_t3611233992  : public RuntimeObject
{
public:
	// System.Boolean VirtualControls::leftDown
	bool ___leftDown_0;
	// System.Boolean VirtualControls::upDown
	bool ___upDown_1;
	// System.Boolean VirtualControls::rightDown
	bool ___rightDown_2;
	// System.Boolean VirtualControls::attackDown
	bool ___attackDown_3;
	// System.Boolean VirtualControls::jumpDown
	bool ___jumpDown_4;
	// System.Single VirtualControls::buttonWidth
	float ___buttonWidth_5;
	// System.Single VirtualControls::buttonHeight
	float ___buttonHeight_6;
	// TKAnyTouchRecognizer VirtualControls::_leftRecognizer
	TKAnyTouchRecognizer_t3651961449 * ____leftRecognizer_7;
	// TKAnyTouchRecognizer VirtualControls::_rightRecognizer
	TKAnyTouchRecognizer_t3651961449 * ____rightRecognizer_8;
	// TKAnyTouchRecognizer VirtualControls::_upRecognizer
	TKAnyTouchRecognizer_t3651961449 * ____upRecognizer_9;
	// TKButtonRecognizer VirtualControls::_attackRecognizer
	TKButtonRecognizer_t691501956 * ____attackRecognizer_10;
	// TKButtonRecognizer VirtualControls::_jumpRecognizer
	TKButtonRecognizer_t691501956 * ____jumpRecognizer_11;

public:
	inline static int32_t get_offset_of_leftDown_0() { return static_cast<int32_t>(offsetof(VirtualControls_t3611233992, ___leftDown_0)); }
	inline bool get_leftDown_0() const { return ___leftDown_0; }
	inline bool* get_address_of_leftDown_0() { return &___leftDown_0; }
	inline void set_leftDown_0(bool value)
	{
		___leftDown_0 = value;
	}

	inline static int32_t get_offset_of_upDown_1() { return static_cast<int32_t>(offsetof(VirtualControls_t3611233992, ___upDown_1)); }
	inline bool get_upDown_1() const { return ___upDown_1; }
	inline bool* get_address_of_upDown_1() { return &___upDown_1; }
	inline void set_upDown_1(bool value)
	{
		___upDown_1 = value;
	}

	inline static int32_t get_offset_of_rightDown_2() { return static_cast<int32_t>(offsetof(VirtualControls_t3611233992, ___rightDown_2)); }
	inline bool get_rightDown_2() const { return ___rightDown_2; }
	inline bool* get_address_of_rightDown_2() { return &___rightDown_2; }
	inline void set_rightDown_2(bool value)
	{
		___rightDown_2 = value;
	}

	inline static int32_t get_offset_of_attackDown_3() { return static_cast<int32_t>(offsetof(VirtualControls_t3611233992, ___attackDown_3)); }
	inline bool get_attackDown_3() const { return ___attackDown_3; }
	inline bool* get_address_of_attackDown_3() { return &___attackDown_3; }
	inline void set_attackDown_3(bool value)
	{
		___attackDown_3 = value;
	}

	inline static int32_t get_offset_of_jumpDown_4() { return static_cast<int32_t>(offsetof(VirtualControls_t3611233992, ___jumpDown_4)); }
	inline bool get_jumpDown_4() const { return ___jumpDown_4; }
	inline bool* get_address_of_jumpDown_4() { return &___jumpDown_4; }
	inline void set_jumpDown_4(bool value)
	{
		___jumpDown_4 = value;
	}

	inline static int32_t get_offset_of_buttonWidth_5() { return static_cast<int32_t>(offsetof(VirtualControls_t3611233992, ___buttonWidth_5)); }
	inline float get_buttonWidth_5() const { return ___buttonWidth_5; }
	inline float* get_address_of_buttonWidth_5() { return &___buttonWidth_5; }
	inline void set_buttonWidth_5(float value)
	{
		___buttonWidth_5 = value;
	}

	inline static int32_t get_offset_of_buttonHeight_6() { return static_cast<int32_t>(offsetof(VirtualControls_t3611233992, ___buttonHeight_6)); }
	inline float get_buttonHeight_6() const { return ___buttonHeight_6; }
	inline float* get_address_of_buttonHeight_6() { return &___buttonHeight_6; }
	inline void set_buttonHeight_6(float value)
	{
		___buttonHeight_6 = value;
	}

	inline static int32_t get_offset_of__leftRecognizer_7() { return static_cast<int32_t>(offsetof(VirtualControls_t3611233992, ____leftRecognizer_7)); }
	inline TKAnyTouchRecognizer_t3651961449 * get__leftRecognizer_7() const { return ____leftRecognizer_7; }
	inline TKAnyTouchRecognizer_t3651961449 ** get_address_of__leftRecognizer_7() { return &____leftRecognizer_7; }
	inline void set__leftRecognizer_7(TKAnyTouchRecognizer_t3651961449 * value)
	{
		____leftRecognizer_7 = value;
		Il2CppCodeGenWriteBarrier((&____leftRecognizer_7), value);
	}

	inline static int32_t get_offset_of__rightRecognizer_8() { return static_cast<int32_t>(offsetof(VirtualControls_t3611233992, ____rightRecognizer_8)); }
	inline TKAnyTouchRecognizer_t3651961449 * get__rightRecognizer_8() const { return ____rightRecognizer_8; }
	inline TKAnyTouchRecognizer_t3651961449 ** get_address_of__rightRecognizer_8() { return &____rightRecognizer_8; }
	inline void set__rightRecognizer_8(TKAnyTouchRecognizer_t3651961449 * value)
	{
		____rightRecognizer_8 = value;
		Il2CppCodeGenWriteBarrier((&____rightRecognizer_8), value);
	}

	inline static int32_t get_offset_of__upRecognizer_9() { return static_cast<int32_t>(offsetof(VirtualControls_t3611233992, ____upRecognizer_9)); }
	inline TKAnyTouchRecognizer_t3651961449 * get__upRecognizer_9() const { return ____upRecognizer_9; }
	inline TKAnyTouchRecognizer_t3651961449 ** get_address_of__upRecognizer_9() { return &____upRecognizer_9; }
	inline void set__upRecognizer_9(TKAnyTouchRecognizer_t3651961449 * value)
	{
		____upRecognizer_9 = value;
		Il2CppCodeGenWriteBarrier((&____upRecognizer_9), value);
	}

	inline static int32_t get_offset_of__attackRecognizer_10() { return static_cast<int32_t>(offsetof(VirtualControls_t3611233992, ____attackRecognizer_10)); }
	inline TKButtonRecognizer_t691501956 * get__attackRecognizer_10() const { return ____attackRecognizer_10; }
	inline TKButtonRecognizer_t691501956 ** get_address_of__attackRecognizer_10() { return &____attackRecognizer_10; }
	inline void set__attackRecognizer_10(TKButtonRecognizer_t691501956 * value)
	{
		____attackRecognizer_10 = value;
		Il2CppCodeGenWriteBarrier((&____attackRecognizer_10), value);
	}

	inline static int32_t get_offset_of__jumpRecognizer_11() { return static_cast<int32_t>(offsetof(VirtualControls_t3611233992, ____jumpRecognizer_11)); }
	inline TKButtonRecognizer_t691501956 * get__jumpRecognizer_11() const { return ____jumpRecognizer_11; }
	inline TKButtonRecognizer_t691501956 ** get_address_of__jumpRecognizer_11() { return &____jumpRecognizer_11; }
	inline void set__jumpRecognizer_11(TKButtonRecognizer_t691501956 * value)
	{
		____jumpRecognizer_11 = value;
		Il2CppCodeGenWriteBarrier((&____jumpRecognizer_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALCONTROLS_T3611233992_H
#ifndef U3CONGUIU3EC__ANONSTOREY4_T592312661_H
#define U3CONGUIU3EC__ANONSTOREY4_T592312661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoOne/<OnGUI>c__AnonStorey4
struct  U3COnGUIU3Ec__AnonStorey4_t592312661  : public RuntimeObject
{
public:
	// TKCurveRecognizer DemoOne/<OnGUI>c__AnonStorey4::recognizer
	TKCurveRecognizer_t1881817312 * ___recognizer_0;
	// DemoOne DemoOne/<OnGUI>c__AnonStorey4::$this
	DemoOne_t3048645529 * ___U24this_1;

public:
	inline static int32_t get_offset_of_recognizer_0() { return static_cast<int32_t>(offsetof(U3COnGUIU3Ec__AnonStorey4_t592312661, ___recognizer_0)); }
	inline TKCurveRecognizer_t1881817312 * get_recognizer_0() const { return ___recognizer_0; }
	inline TKCurveRecognizer_t1881817312 ** get_address_of_recognizer_0() { return &___recognizer_0; }
	inline void set_recognizer_0(TKCurveRecognizer_t1881817312 * value)
	{
		___recognizer_0 = value;
		Il2CppCodeGenWriteBarrier((&___recognizer_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3COnGUIU3Ec__AnonStorey4_t592312661, ___U24this_1)); }
	inline DemoOne_t3048645529 * get_U24this_1() const { return ___U24this_1; }
	inline DemoOne_t3048645529 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(DemoOne_t3048645529 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONGUIU3EC__ANONSTOREY4_T592312661_H
#ifndef U3CONGUIU3EC__ANONSTOREY3_T592312654_H
#define U3CONGUIU3EC__ANONSTOREY3_T592312654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoOne/<OnGUI>c__AnonStorey3
struct  U3COnGUIU3Ec__AnonStorey3_t592312654  : public RuntimeObject
{
public:
	// TKOneFingerRotationRecognizer DemoOne/<OnGUI>c__AnonStorey3::recognizer
	TKOneFingerRotationRecognizer_t564097970 * ___recognizer_0;
	// DemoOne DemoOne/<OnGUI>c__AnonStorey3::$this
	DemoOne_t3048645529 * ___U24this_1;

public:
	inline static int32_t get_offset_of_recognizer_0() { return static_cast<int32_t>(offsetof(U3COnGUIU3Ec__AnonStorey3_t592312654, ___recognizer_0)); }
	inline TKOneFingerRotationRecognizer_t564097970 * get_recognizer_0() const { return ___recognizer_0; }
	inline TKOneFingerRotationRecognizer_t564097970 ** get_address_of_recognizer_0() { return &___recognizer_0; }
	inline void set_recognizer_0(TKOneFingerRotationRecognizer_t564097970 * value)
	{
		___recognizer_0 = value;
		Il2CppCodeGenWriteBarrier((&___recognizer_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3COnGUIU3Ec__AnonStorey3_t592312654, ___U24this_1)); }
	inline DemoOne_t3048645529 * get_U24this_1() const { return ___U24this_1; }
	inline DemoOne_t3048645529 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(DemoOne_t3048645529 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONGUIU3EC__ANONSTOREY3_T592312654_H
#ifndef SERIALIZABLEUNITYARMATRIX4X4_T78255337_H
#define SERIALIZABLEUNITYARMATRIX4X4_T78255337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableUnityARMatrix4x4
struct  serializableUnityARMatrix4x4_t78255337  : public RuntimeObject
{
public:
	// Utils.SerializableVector4 Utils.serializableUnityARMatrix4x4::column0
	SerializableVector4_t1862640084 * ___column0_0;
	// Utils.SerializableVector4 Utils.serializableUnityARMatrix4x4::column1
	SerializableVector4_t1862640084 * ___column1_1;
	// Utils.SerializableVector4 Utils.serializableUnityARMatrix4x4::column2
	SerializableVector4_t1862640084 * ___column2_2;
	// Utils.SerializableVector4 Utils.serializableUnityARMatrix4x4::column3
	SerializableVector4_t1862640084 * ___column3_3;

public:
	inline static int32_t get_offset_of_column0_0() { return static_cast<int32_t>(offsetof(serializableUnityARMatrix4x4_t78255337, ___column0_0)); }
	inline SerializableVector4_t1862640084 * get_column0_0() const { return ___column0_0; }
	inline SerializableVector4_t1862640084 ** get_address_of_column0_0() { return &___column0_0; }
	inline void set_column0_0(SerializableVector4_t1862640084 * value)
	{
		___column0_0 = value;
		Il2CppCodeGenWriteBarrier((&___column0_0), value);
	}

	inline static int32_t get_offset_of_column1_1() { return static_cast<int32_t>(offsetof(serializableUnityARMatrix4x4_t78255337, ___column1_1)); }
	inline SerializableVector4_t1862640084 * get_column1_1() const { return ___column1_1; }
	inline SerializableVector4_t1862640084 ** get_address_of_column1_1() { return &___column1_1; }
	inline void set_column1_1(SerializableVector4_t1862640084 * value)
	{
		___column1_1 = value;
		Il2CppCodeGenWriteBarrier((&___column1_1), value);
	}

	inline static int32_t get_offset_of_column2_2() { return static_cast<int32_t>(offsetof(serializableUnityARMatrix4x4_t78255337, ___column2_2)); }
	inline SerializableVector4_t1862640084 * get_column2_2() const { return ___column2_2; }
	inline SerializableVector4_t1862640084 ** get_address_of_column2_2() { return &___column2_2; }
	inline void set_column2_2(SerializableVector4_t1862640084 * value)
	{
		___column2_2 = value;
		Il2CppCodeGenWriteBarrier((&___column2_2), value);
	}

	inline static int32_t get_offset_of_column3_3() { return static_cast<int32_t>(offsetof(serializableUnityARMatrix4x4_t78255337, ___column3_3)); }
	inline SerializableVector4_t1862640084 * get_column3_3() const { return ___column3_3; }
	inline SerializableVector4_t1862640084 ** get_address_of_column3_3() { return &___column3_3; }
	inline void set_column3_3(SerializableVector4_t1862640084 * value)
	{
		___column3_3 = value;
		Il2CppCodeGenWriteBarrier((&___column3_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEUNITYARMATRIX4X4_T78255337_H
#ifndef U3CONGUIU3EC__ANONSTOREY1_T592312656_H
#define U3CONGUIU3EC__ANONSTOREY1_T592312656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoOne/<OnGUI>c__AnonStorey1
struct  U3COnGUIU3Ec__AnonStorey1_t592312656  : public RuntimeObject
{
public:
	// TKPinchRecognizer DemoOne/<OnGUI>c__AnonStorey1::recognizer
	TKPinchRecognizer_t788645703 * ___recognizer_0;
	// DemoOne DemoOne/<OnGUI>c__AnonStorey1::$this
	DemoOne_t3048645529 * ___U24this_1;

public:
	inline static int32_t get_offset_of_recognizer_0() { return static_cast<int32_t>(offsetof(U3COnGUIU3Ec__AnonStorey1_t592312656, ___recognizer_0)); }
	inline TKPinchRecognizer_t788645703 * get_recognizer_0() const { return ___recognizer_0; }
	inline TKPinchRecognizer_t788645703 ** get_address_of_recognizer_0() { return &___recognizer_0; }
	inline void set_recognizer_0(TKPinchRecognizer_t788645703 * value)
	{
		___recognizer_0 = value;
		Il2CppCodeGenWriteBarrier((&___recognizer_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3COnGUIU3Ec__AnonStorey1_t592312656, ___U24this_1)); }
	inline DemoOne_t3048645529 * get_U24this_1() const { return ___U24this_1; }
	inline DemoOne_t3048645529 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(DemoOne_t3048645529 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONGUIU3EC__ANONSTOREY1_T592312656_H
#ifndef U3CONGUIU3EC__ANONSTOREY2_T592312655_H
#define U3CONGUIU3EC__ANONSTOREY2_T592312655_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoOne/<OnGUI>c__AnonStorey2
struct  U3COnGUIU3Ec__AnonStorey2_t592312655  : public RuntimeObject
{
public:
	// TKRotationRecognizer DemoOne/<OnGUI>c__AnonStorey2::recognizer
	TKRotationRecognizer_t651141287 * ___recognizer_0;
	// DemoOne DemoOne/<OnGUI>c__AnonStorey2::$this
	DemoOne_t3048645529 * ___U24this_1;

public:
	inline static int32_t get_offset_of_recognizer_0() { return static_cast<int32_t>(offsetof(U3COnGUIU3Ec__AnonStorey2_t592312655, ___recognizer_0)); }
	inline TKRotationRecognizer_t651141287 * get_recognizer_0() const { return ___recognizer_0; }
	inline TKRotationRecognizer_t651141287 ** get_address_of_recognizer_0() { return &___recognizer_0; }
	inline void set_recognizer_0(TKRotationRecognizer_t651141287 * value)
	{
		___recognizer_0 = value;
		Il2CppCodeGenWriteBarrier((&___recognizer_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3COnGUIU3Ec__AnonStorey2_t592312655, ___U24this_1)); }
	inline DemoOne_t3048645529 * get_U24this_1() const { return ___U24this_1; }
	inline DemoOne_t3048645529 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(DemoOne_t3048645529 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONGUIU3EC__ANONSTOREY2_T592312655_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef UNITYARUTILITY_T2509807446_H
#define UNITYARUTILITY_T2509807446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARUtility
struct  UnityARUtility_t2509807446  : public RuntimeObject
{
public:
	// UnityEngine.MeshCollider UnityEngine.XR.iOS.UnityARUtility::meshCollider
	MeshCollider_t903564387 * ___meshCollider_0;
	// UnityEngine.MeshFilter UnityEngine.XR.iOS.UnityARUtility::meshFilter
	MeshFilter_t3523625662 * ___meshFilter_1;

public:
	inline static int32_t get_offset_of_meshCollider_0() { return static_cast<int32_t>(offsetof(UnityARUtility_t2509807446, ___meshCollider_0)); }
	inline MeshCollider_t903564387 * get_meshCollider_0() const { return ___meshCollider_0; }
	inline MeshCollider_t903564387 ** get_address_of_meshCollider_0() { return &___meshCollider_0; }
	inline void set_meshCollider_0(MeshCollider_t903564387 * value)
	{
		___meshCollider_0 = value;
		Il2CppCodeGenWriteBarrier((&___meshCollider_0), value);
	}

	inline static int32_t get_offset_of_meshFilter_1() { return static_cast<int32_t>(offsetof(UnityARUtility_t2509807446, ___meshFilter_1)); }
	inline MeshFilter_t3523625662 * get_meshFilter_1() const { return ___meshFilter_1; }
	inline MeshFilter_t3523625662 ** get_address_of_meshFilter_1() { return &___meshFilter_1; }
	inline void set_meshFilter_1(MeshFilter_t3523625662 * value)
	{
		___meshFilter_1 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_1), value);
	}
};

struct UnityARUtility_t2509807446_StaticFields
{
public:
	// UnityEngine.GameObject UnityEngine.XR.iOS.UnityARUtility::planePrefab
	GameObject_t1113636619 * ___planePrefab_2;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.XR.iOS.UnityARUtility::planes
	List_1_t2585711361 * ___planes_3;

public:
	inline static int32_t get_offset_of_planePrefab_2() { return static_cast<int32_t>(offsetof(UnityARUtility_t2509807446_StaticFields, ___planePrefab_2)); }
	inline GameObject_t1113636619 * get_planePrefab_2() const { return ___planePrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_planePrefab_2() { return &___planePrefab_2; }
	inline void set_planePrefab_2(GameObject_t1113636619 * value)
	{
		___planePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___planePrefab_2), value);
	}

	inline static int32_t get_offset_of_planes_3() { return static_cast<int32_t>(offsetof(UnityARUtility_t2509807446_StaticFields, ___planes_3)); }
	inline List_1_t2585711361 * get_planes_3() const { return ___planes_3; }
	inline List_1_t2585711361 ** get_address_of_planes_3() { return &___planes_3; }
	inline void set_planes_3(List_1_t2585711361 * value)
	{
		___planes_3 = value;
		Il2CppCodeGenWriteBarrier((&___planes_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARUTILITY_T2509807446_H
#ifndef U3CBEGINGESTUREU3EC__ITERATOR0_T696754078_H
#define U3CBEGINGESTUREU3EC__ITERATOR0_T696754078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TKLongPressRecognizer/<beginGesture>c__Iterator0
struct  U3CbeginGestureU3Ec__Iterator0_t696754078  : public RuntimeObject
{
public:
	// System.Single TKLongPressRecognizer/<beginGesture>c__Iterator0::<endTime>__0
	float ___U3CendTimeU3E__0_0;
	// TKLongPressRecognizer TKLongPressRecognizer/<beginGesture>c__Iterator0::$this
	TKLongPressRecognizer_t1475862313 * ___U24this_1;
	// System.Object TKLongPressRecognizer/<beginGesture>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TKLongPressRecognizer/<beginGesture>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TKLongPressRecognizer/<beginGesture>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CendTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CbeginGestureU3Ec__Iterator0_t696754078, ___U3CendTimeU3E__0_0)); }
	inline float get_U3CendTimeU3E__0_0() const { return ___U3CendTimeU3E__0_0; }
	inline float* get_address_of_U3CendTimeU3E__0_0() { return &___U3CendTimeU3E__0_0; }
	inline void set_U3CendTimeU3E__0_0(float value)
	{
		___U3CendTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CbeginGestureU3Ec__Iterator0_t696754078, ___U24this_1)); }
	inline TKLongPressRecognizer_t1475862313 * get_U24this_1() const { return ___U24this_1; }
	inline TKLongPressRecognizer_t1475862313 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(TKLongPressRecognizer_t1475862313 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CbeginGestureU3Ec__Iterator0_t696754078, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CbeginGestureU3Ec__Iterator0_t696754078, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CbeginGestureU3Ec__Iterator0_t696754078, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBEGINGESTUREU3EC__ITERATOR0_T696754078_H
#ifndef UNITYARMATRIXOPS_T2790111267_H
#define UNITYARMATRIXOPS_T2790111267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARMatrixOps
struct  UnityARMatrixOps_t2790111267  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARMATRIXOPS_T2790111267_H
#ifndef ARBLENDSHAPELOCATION_T2653069299_H
#define ARBLENDSHAPELOCATION_T2653069299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARBlendShapeLocation
struct  ARBlendShapeLocation_t2653069299  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARBLENDSHAPELOCATION_T2653069299_H
#ifndef U3CONGUIU3EC__ANONSTOREY0_T592312657_H
#define U3CONGUIU3EC__ANONSTOREY0_T592312657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoOne/<OnGUI>c__AnonStorey0
struct  U3COnGUIU3Ec__AnonStorey0_t592312657  : public RuntimeObject
{
public:
	// TKPanRecognizer DemoOne/<OnGUI>c__AnonStorey0::recognizer
	TKPanRecognizer_t1603940453 * ___recognizer_0;

public:
	inline static int32_t get_offset_of_recognizer_0() { return static_cast<int32_t>(offsetof(U3COnGUIU3Ec__AnonStorey0_t592312657, ___recognizer_0)); }
	inline TKPanRecognizer_t1603940453 * get_recognizer_0() const { return ___recognizer_0; }
	inline TKPanRecognizer_t1603940453 ** get_address_of_recognizer_0() { return &___recognizer_0; }
	inline void set_recognizer_0(TKPanRecognizer_t1603940453 * value)
	{
		___recognizer_0 = value;
		Il2CppCodeGenWriteBarrier((&___recognizer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONGUIU3EC__ANONSTOREY0_T592312657_H
#ifndef SERIALIZABLESHC_T2667429767_H
#define SERIALIZABLESHC_T2667429767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableSHC
struct  serializableSHC_t2667429767  : public RuntimeObject
{
public:
	// System.Byte[] Utils.serializableSHC::shcData
	ByteU5BU5D_t4116647657* ___shcData_0;

public:
	inline static int32_t get_offset_of_shcData_0() { return static_cast<int32_t>(offsetof(serializableSHC_t2667429767, ___shcData_0)); }
	inline ByteU5BU5D_t4116647657* get_shcData_0() const { return ___shcData_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_shcData_0() { return &___shcData_0; }
	inline void set_shcData_0(ByteU5BU5D_t4116647657* value)
	{
		___shcData_0 = value;
		Il2CppCodeGenWriteBarrier((&___shcData_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLESHC_T2667429767_H
#ifndef UNITYARANCHORMANAGER_T1557554123_H
#define UNITYARANCHORMANAGER_T1557554123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARAnchorManager
struct  UnityARAnchorManager_t1557554123  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject> UnityEngine.XR.iOS.UnityARAnchorManager::planeAnchorMap
	Dictionary_2_t1732976114 * ___planeAnchorMap_0;

public:
	inline static int32_t get_offset_of_planeAnchorMap_0() { return static_cast<int32_t>(offsetof(UnityARAnchorManager_t1557554123, ___planeAnchorMap_0)); }
	inline Dictionary_2_t1732976114 * get_planeAnchorMap_0() const { return ___planeAnchorMap_0; }
	inline Dictionary_2_t1732976114 ** get_address_of_planeAnchorMap_0() { return &___planeAnchorMap_0; }
	inline void set_planeAnchorMap_0(Dictionary_2_t1732976114 * value)
	{
		___planeAnchorMap_0 = value;
		Il2CppCodeGenWriteBarrier((&___planeAnchorMap_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARANCHORMANAGER_T1557554123_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef SPHERICALHARMONICSL2_T3220866195_H
#define SPHERICALHARMONICSL2_T3220866195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.SphericalHarmonicsL2
struct  SphericalHarmonicsL2_t3220866195 
{
public:
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shr0
	float ___shr0_0;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shr1
	float ___shr1_1;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shr2
	float ___shr2_2;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shr3
	float ___shr3_3;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shr4
	float ___shr4_4;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shr5
	float ___shr5_5;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shr6
	float ___shr6_6;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shr7
	float ___shr7_7;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shr8
	float ___shr8_8;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shg0
	float ___shg0_9;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shg1
	float ___shg1_10;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shg2
	float ___shg2_11;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shg3
	float ___shg3_12;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shg4
	float ___shg4_13;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shg5
	float ___shg5_14;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shg6
	float ___shg6_15;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shg7
	float ___shg7_16;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shg8
	float ___shg8_17;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shb0
	float ___shb0_18;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shb1
	float ___shb1_19;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shb2
	float ___shb2_20;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shb3
	float ___shb3_21;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shb4
	float ___shb4_22;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shb5
	float ___shb5_23;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shb6
	float ___shb6_24;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shb7
	float ___shb7_25;
	// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::shb8
	float ___shb8_26;

public:
	inline static int32_t get_offset_of_shr0_0() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t3220866195, ___shr0_0)); }
	inline float get_shr0_0() const { return ___shr0_0; }
	inline float* get_address_of_shr0_0() { return &___shr0_0; }
	inline void set_shr0_0(float value)
	{
		___shr0_0 = value;
	}

	inline static int32_t get_offset_of_shr1_1() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t3220866195, ___shr1_1)); }
	inline float get_shr1_1() const { return ___shr1_1; }
	inline float* get_address_of_shr1_1() { return &___shr1_1; }
	inline void set_shr1_1(float value)
	{
		___shr1_1 = value;
	}

	inline static int32_t get_offset_of_shr2_2() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t3220866195, ___shr2_2)); }
	inline float get_shr2_2() const { return ___shr2_2; }
	inline float* get_address_of_shr2_2() { return &___shr2_2; }
	inline void set_shr2_2(float value)
	{
		___shr2_2 = value;
	}

	inline static int32_t get_offset_of_shr3_3() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t3220866195, ___shr3_3)); }
	inline float get_shr3_3() const { return ___shr3_3; }
	inline float* get_address_of_shr3_3() { return &___shr3_3; }
	inline void set_shr3_3(float value)
	{
		___shr3_3 = value;
	}

	inline static int32_t get_offset_of_shr4_4() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t3220866195, ___shr4_4)); }
	inline float get_shr4_4() const { return ___shr4_4; }
	inline float* get_address_of_shr4_4() { return &___shr4_4; }
	inline void set_shr4_4(float value)
	{
		___shr4_4 = value;
	}

	inline static int32_t get_offset_of_shr5_5() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t3220866195, ___shr5_5)); }
	inline float get_shr5_5() const { return ___shr5_5; }
	inline float* get_address_of_shr5_5() { return &___shr5_5; }
	inline void set_shr5_5(float value)
	{
		___shr5_5 = value;
	}

	inline static int32_t get_offset_of_shr6_6() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t3220866195, ___shr6_6)); }
	inline float get_shr6_6() const { return ___shr6_6; }
	inline float* get_address_of_shr6_6() { return &___shr6_6; }
	inline void set_shr6_6(float value)
	{
		___shr6_6 = value;
	}

	inline static int32_t get_offset_of_shr7_7() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t3220866195, ___shr7_7)); }
	inline float get_shr7_7() const { return ___shr7_7; }
	inline float* get_address_of_shr7_7() { return &___shr7_7; }
	inline void set_shr7_7(float value)
	{
		___shr7_7 = value;
	}

	inline static int32_t get_offset_of_shr8_8() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t3220866195, ___shr8_8)); }
	inline float get_shr8_8() const { return ___shr8_8; }
	inline float* get_address_of_shr8_8() { return &___shr8_8; }
	inline void set_shr8_8(float value)
	{
		___shr8_8 = value;
	}

	inline static int32_t get_offset_of_shg0_9() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t3220866195, ___shg0_9)); }
	inline float get_shg0_9() const { return ___shg0_9; }
	inline float* get_address_of_shg0_9() { return &___shg0_9; }
	inline void set_shg0_9(float value)
	{
		___shg0_9 = value;
	}

	inline static int32_t get_offset_of_shg1_10() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t3220866195, ___shg1_10)); }
	inline float get_shg1_10() const { return ___shg1_10; }
	inline float* get_address_of_shg1_10() { return &___shg1_10; }
	inline void set_shg1_10(float value)
	{
		___shg1_10 = value;
	}

	inline static int32_t get_offset_of_shg2_11() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t3220866195, ___shg2_11)); }
	inline float get_shg2_11() const { return ___shg2_11; }
	inline float* get_address_of_shg2_11() { return &___shg2_11; }
	inline void set_shg2_11(float value)
	{
		___shg2_11 = value;
	}

	inline static int32_t get_offset_of_shg3_12() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t3220866195, ___shg3_12)); }
	inline float get_shg3_12() const { return ___shg3_12; }
	inline float* get_address_of_shg3_12() { return &___shg3_12; }
	inline void set_shg3_12(float value)
	{
		___shg3_12 = value;
	}

	inline static int32_t get_offset_of_shg4_13() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t3220866195, ___shg4_13)); }
	inline float get_shg4_13() const { return ___shg4_13; }
	inline float* get_address_of_shg4_13() { return &___shg4_13; }
	inline void set_shg4_13(float value)
	{
		___shg4_13 = value;
	}

	inline static int32_t get_offset_of_shg5_14() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t3220866195, ___shg5_14)); }
	inline float get_shg5_14() const { return ___shg5_14; }
	inline float* get_address_of_shg5_14() { return &___shg5_14; }
	inline void set_shg5_14(float value)
	{
		___shg5_14 = value;
	}

	inline static int32_t get_offset_of_shg6_15() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t3220866195, ___shg6_15)); }
	inline float get_shg6_15() const { return ___shg6_15; }
	inline float* get_address_of_shg6_15() { return &___shg6_15; }
	inline void set_shg6_15(float value)
	{
		___shg6_15 = value;
	}

	inline static int32_t get_offset_of_shg7_16() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t3220866195, ___shg7_16)); }
	inline float get_shg7_16() const { return ___shg7_16; }
	inline float* get_address_of_shg7_16() { return &___shg7_16; }
	inline void set_shg7_16(float value)
	{
		___shg7_16 = value;
	}

	inline static int32_t get_offset_of_shg8_17() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t3220866195, ___shg8_17)); }
	inline float get_shg8_17() const { return ___shg8_17; }
	inline float* get_address_of_shg8_17() { return &___shg8_17; }
	inline void set_shg8_17(float value)
	{
		___shg8_17 = value;
	}

	inline static int32_t get_offset_of_shb0_18() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t3220866195, ___shb0_18)); }
	inline float get_shb0_18() const { return ___shb0_18; }
	inline float* get_address_of_shb0_18() { return &___shb0_18; }
	inline void set_shb0_18(float value)
	{
		___shb0_18 = value;
	}

	inline static int32_t get_offset_of_shb1_19() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t3220866195, ___shb1_19)); }
	inline float get_shb1_19() const { return ___shb1_19; }
	inline float* get_address_of_shb1_19() { return &___shb1_19; }
	inline void set_shb1_19(float value)
	{
		___shb1_19 = value;
	}

	inline static int32_t get_offset_of_shb2_20() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t3220866195, ___shb2_20)); }
	inline float get_shb2_20() const { return ___shb2_20; }
	inline float* get_address_of_shb2_20() { return &___shb2_20; }
	inline void set_shb2_20(float value)
	{
		___shb2_20 = value;
	}

	inline static int32_t get_offset_of_shb3_21() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t3220866195, ___shb3_21)); }
	inline float get_shb3_21() const { return ___shb3_21; }
	inline float* get_address_of_shb3_21() { return &___shb3_21; }
	inline void set_shb3_21(float value)
	{
		___shb3_21 = value;
	}

	inline static int32_t get_offset_of_shb4_22() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t3220866195, ___shb4_22)); }
	inline float get_shb4_22() const { return ___shb4_22; }
	inline float* get_address_of_shb4_22() { return &___shb4_22; }
	inline void set_shb4_22(float value)
	{
		___shb4_22 = value;
	}

	inline static int32_t get_offset_of_shb5_23() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t3220866195, ___shb5_23)); }
	inline float get_shb5_23() const { return ___shb5_23; }
	inline float* get_address_of_shb5_23() { return &___shb5_23; }
	inline void set_shb5_23(float value)
	{
		___shb5_23 = value;
	}

	inline static int32_t get_offset_of_shb6_24() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t3220866195, ___shb6_24)); }
	inline float get_shb6_24() const { return ___shb6_24; }
	inline float* get_address_of_shb6_24() { return &___shb6_24; }
	inline void set_shb6_24(float value)
	{
		___shb6_24 = value;
	}

	inline static int32_t get_offset_of_shb7_25() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t3220866195, ___shb7_25)); }
	inline float get_shb7_25() const { return ___shb7_25; }
	inline float* get_address_of_shb7_25() { return &___shb7_25; }
	inline void set_shb7_25(float value)
	{
		___shb7_25 = value;
	}

	inline static int32_t get_offset_of_shb8_26() { return static_cast<int32_t>(offsetof(SphericalHarmonicsL2_t3220866195, ___shb8_26)); }
	inline float get_shb8_26() const { return ___shb8_26; }
	inline float* get_address_of_shb8_26() { return &___shb8_26; }
	inline void set_shb8_26(float value)
	{
		___shb8_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPHERICALHARMONICSL2_T3220866195_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#define DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t2562230146 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef UNITYEVENT_3_T1697774568_H
#define UNITYEVENT_3_T1697774568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.Single,System.Single,System.Single>
struct  UnityEvent_3_t1697774568  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t1697774568, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T1697774568_H
#ifndef UNITYEVENT_1_T3437345828_H
#define UNITYEVENT_1_T3437345828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
struct  UnityEvent_1_t3437345828  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3437345828, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3437345828_H
#ifndef UNITYEVENT_2_T1827368229_H
#define UNITYEVENT_2_T1827368229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.Single,System.Single>
struct  UnityEvent_2_t1827368229  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t1827368229, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T1827368229_H
#ifndef ARPOINT_T499615819_H
#define ARPOINT_T499615819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARPoint
struct  ARPoint_t499615819 
{
public:
	// System.Double UnityEngine.XR.iOS.ARPoint::x
	double ___x_0;
	// System.Double UnityEngine.XR.iOS.ARPoint::y
	double ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(ARPoint_t499615819, ___x_0)); }
	inline double get_x_0() const { return ___x_0; }
	inline double* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(double value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(ARPoint_t499615819, ___y_1)); }
	inline double get_y_1() const { return ___y_1; }
	inline double* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(double value)
	{
		___y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARPOINT_T499615819_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef SPRITESTATE_T1362986479_H
#define SPRITESTATE_T1362986479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1362986479 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t280657092 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t280657092 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_HighlightedSprite_0)); }
	inline Sprite_t280657092 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t280657092 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t280657092 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_PressedSprite_1)); }
	inline Sprite_t280657092 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t280657092 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t280657092 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_DisabledSprite_2)); }
	inline Sprite_t280657092 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t280657092 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t280657092 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_pinvoke
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_com
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1362986479_H
#ifndef TKRECT_T2880547869_H
#define TKRECT_T2880547869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TKRect
struct  TKRect_t2880547869 
{
public:
	// System.Single TKRect::x
	float ___x_0;
	// System.Single TKRect::y
	float ___y_1;
	// System.Single TKRect::width
	float ___width_2;
	// System.Single TKRect::height
	float ___height_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(TKRect_t2880547869, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(TKRect_t2880547869, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_width_2() { return static_cast<int32_t>(offsetof(TKRect_t2880547869, ___width_2)); }
	inline float get_width_2() const { return ___width_2; }
	inline float* get_address_of_width_2() { return &___width_2; }
	inline void set_width_2(float value)
	{
		___width_2 = value;
	}

	inline static int32_t get_offset_of_height_3() { return static_cast<int32_t>(offsetof(TKRect_t2880547869, ___height_3)); }
	inline float get_height_3() const { return ___height_3; }
	inline float* get_address_of_height_3() { return &___height_3; }
	inline void set_height_3(float value)
	{
		___height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TKRECT_T2880547869_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef GCHANDLE_T3351438187_H
#define GCHANDLE_T3351438187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.GCHandle
struct  GCHandle_t3351438187 
{
public:
	// System.Int32 System.Runtime.InteropServices.GCHandle::handle
	int32_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(GCHandle_t3351438187, ___handle_0)); }
	inline int32_t get_handle_0() const { return ___handle_0; }
	inline int32_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(int32_t value)
	{
		___handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCHANDLE_T3351438187_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_0;
	// System.Int16 System.Guid::_b
	int16_t ____b_1;
	// System.Int16 System.Guid::_c
	int16_t ____c_2;
	// System.Byte System.Guid::_d
	uint8_t ____d_3;
	// System.Byte System.Guid::_e
	uint8_t ____e_4;
	// System.Byte System.Guid::_f
	uint8_t ____f_5;
	// System.Byte System.Guid::_g
	uint8_t ____g_6;
	// System.Byte System.Guid::_h
	uint8_t ____h_7;
	// System.Byte System.Guid::_i
	uint8_t ____i_8;
	// System.Byte System.Guid::_j
	uint8_t ____j_9;
	// System.Byte System.Guid::_k
	uint8_t ____k_10;

public:
	inline static int32_t get_offset_of__a_0() { return static_cast<int32_t>(offsetof(Guid_t, ____a_0)); }
	inline int32_t get__a_0() const { return ____a_0; }
	inline int32_t* get_address_of__a_0() { return &____a_0; }
	inline void set__a_0(int32_t value)
	{
		____a_0 = value;
	}

	inline static int32_t get_offset_of__b_1() { return static_cast<int32_t>(offsetof(Guid_t, ____b_1)); }
	inline int16_t get__b_1() const { return ____b_1; }
	inline int16_t* get_address_of__b_1() { return &____b_1; }
	inline void set__b_1(int16_t value)
	{
		____b_1 = value;
	}

	inline static int32_t get_offset_of__c_2() { return static_cast<int32_t>(offsetof(Guid_t, ____c_2)); }
	inline int16_t get__c_2() const { return ____c_2; }
	inline int16_t* get_address_of__c_2() { return &____c_2; }
	inline void set__c_2(int16_t value)
	{
		____c_2 = value;
	}

	inline static int32_t get_offset_of__d_3() { return static_cast<int32_t>(offsetof(Guid_t, ____d_3)); }
	inline uint8_t get__d_3() const { return ____d_3; }
	inline uint8_t* get_address_of__d_3() { return &____d_3; }
	inline void set__d_3(uint8_t value)
	{
		____d_3 = value;
	}

	inline static int32_t get_offset_of__e_4() { return static_cast<int32_t>(offsetof(Guid_t, ____e_4)); }
	inline uint8_t get__e_4() const { return ____e_4; }
	inline uint8_t* get_address_of__e_4() { return &____e_4; }
	inline void set__e_4(uint8_t value)
	{
		____e_4 = value;
	}

	inline static int32_t get_offset_of__f_5() { return static_cast<int32_t>(offsetof(Guid_t, ____f_5)); }
	inline uint8_t get__f_5() const { return ____f_5; }
	inline uint8_t* get_address_of__f_5() { return &____f_5; }
	inline void set__f_5(uint8_t value)
	{
		____f_5 = value;
	}

	inline static int32_t get_offset_of__g_6() { return static_cast<int32_t>(offsetof(Guid_t, ____g_6)); }
	inline uint8_t get__g_6() const { return ____g_6; }
	inline uint8_t* get_address_of__g_6() { return &____g_6; }
	inline void set__g_6(uint8_t value)
	{
		____g_6 = value;
	}

	inline static int32_t get_offset_of__h_7() { return static_cast<int32_t>(offsetof(Guid_t, ____h_7)); }
	inline uint8_t get__h_7() const { return ____h_7; }
	inline uint8_t* get_address_of__h_7() { return &____h_7; }
	inline void set__h_7(uint8_t value)
	{
		____h_7 = value;
	}

	inline static int32_t get_offset_of__i_8() { return static_cast<int32_t>(offsetof(Guid_t, ____i_8)); }
	inline uint8_t get__i_8() const { return ____i_8; }
	inline uint8_t* get_address_of__i_8() { return &____i_8; }
	inline void set__i_8(uint8_t value)
	{
		____i_8 = value;
	}

	inline static int32_t get_offset_of__j_9() { return static_cast<int32_t>(offsetof(Guid_t, ____j_9)); }
	inline uint8_t get__j_9() const { return ____j_9; }
	inline uint8_t* get_address_of__j_9() { return &____j_9; }
	inline void set__j_9(uint8_t value)
	{
		____j_9 = value;
	}

	inline static int32_t get_offset_of__k_10() { return static_cast<int32_t>(offsetof(Guid_t, ____k_10)); }
	inline uint8_t get__k_10() const { return ____k_10; }
	inline uint8_t* get_address_of__k_10() { return &____k_10; }
	inline void set__k_10(uint8_t value)
	{
		____k_10 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_11;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t386037858 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t386037858 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_11() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_11)); }
	inline Guid_t  get_Empty_11() const { return ___Empty_11; }
	inline Guid_t * get_address_of_Empty_11() { return &___Empty_11; }
	inline void set_Empty_11(Guid_t  value)
	{
		___Empty_11 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t386037858 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t386037858 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t386037858 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t386037858 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef ARSIZE_T208719028_H
#define ARSIZE_T208719028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARSize
struct  ARSize_t208719028 
{
public:
	// System.Double UnityEngine.XR.iOS.ARSize::width
	double ___width_0;
	// System.Double UnityEngine.XR.iOS.ARSize::height
	double ___height_1;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(ARSize_t208719028, ___width_0)); }
	inline double get_width_0() const { return ___width_0; }
	inline double* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(double value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(ARSize_t208719028, ___height_1)); }
	inline double get_height_1() const { return ___height_1; }
	inline double* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(double value)
	{
		___height_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARSIZE_T208719028_H
#ifndef HSVCOLOR_T2280895388_H
#define HSVCOLOR_T2280895388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HsvColor
struct  HsvColor_t2280895388 
{
public:
	// System.Double HsvColor::H
	double ___H_0;
	// System.Double HsvColor::S
	double ___S_1;
	// System.Double HsvColor::V
	double ___V_2;

public:
	inline static int32_t get_offset_of_H_0() { return static_cast<int32_t>(offsetof(HsvColor_t2280895388, ___H_0)); }
	inline double get_H_0() const { return ___H_0; }
	inline double* get_address_of_H_0() { return &___H_0; }
	inline void set_H_0(double value)
	{
		___H_0 = value;
	}

	inline static int32_t get_offset_of_S_1() { return static_cast<int32_t>(offsetof(HsvColor_t2280895388, ___S_1)); }
	inline double get_S_1() const { return ___S_1; }
	inline double* get_address_of_S_1() { return &___S_1; }
	inline void set_S_1(double value)
	{
		___S_1 = value;
	}

	inline static int32_t get_offset_of_V_2() { return static_cast<int32_t>(offsetof(HsvColor_t2280895388, ___V_2)); }
	inline double get_V_2() const { return ___V_2; }
	inline double* get_address_of_V_2() { return &___V_2; }
	inline void set_V_2(double value)
	{
		___V_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVCOLOR_T2280895388_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef MODE_T1066900953_H
#define MODE_T1066900953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1066900953 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1066900953, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1066900953_H
#ifndef UNITYARSESSIONRUNOPTION_T942967030_H
#define UNITYARSESSIONRUNOPTION_T942967030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionRunOption
struct  UnityARSessionRunOption_t942967030 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityARSessionRunOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UnityARSessionRunOption_t942967030, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARSESSIONRUNOPTION_T942967030_H
#ifndef UNITYARPLANEDETECTION_T1367733575_H
#define UNITYARPLANEDETECTION_T1367733575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARPlaneDetection
struct  UnityARPlaneDetection_t1367733575 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityARPlaneDetection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UnityARPlaneDetection_t1367733575, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARPLANEDETECTION_T1367733575_H
#ifndef UNITYARALIGNMENT_T3792119710_H
#define UNITYARALIGNMENT_T3792119710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARAlignment
struct  UnityARAlignment_t3792119710 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityARAlignment::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UnityARAlignment_t3792119710, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARALIGNMENT_T3792119710_H
#ifndef BOXSLIDEREVENT_T439394298_H
#define BOXSLIDEREVENT_T439394298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BoxSlider/BoxSliderEvent
struct  BoxSliderEvent_t439394298  : public UnityEvent_2_t1827368229
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXSLIDEREVENT_T439394298_H
#ifndef ARERRORCODE_T1180871917_H
#define ARERRORCODE_T1180871917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARErrorCode
struct  ARErrorCode_t1180871917 
{
public:
	// System.Int64 UnityEngine.XR.iOS.ARErrorCode::value__
	int64_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARErrorCode_t1180871917, ___value___1)); }
	inline int64_t get_value___1() const { return ___value___1; }
	inline int64_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int64_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARERRORCODE_T1180871917_H
#ifndef UNITYVIDEOPARAMS_T4155354995_H
#define UNITYVIDEOPARAMS_T4155354995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityVideoParams
struct  UnityVideoParams_t4155354995 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityVideoParams::yWidth
	int32_t ___yWidth_0;
	// System.Int32 UnityEngine.XR.iOS.UnityVideoParams::yHeight
	int32_t ___yHeight_1;
	// System.Int32 UnityEngine.XR.iOS.UnityVideoParams::screenOrientation
	int32_t ___screenOrientation_2;
	// System.Single UnityEngine.XR.iOS.UnityVideoParams::texCoordScale
	float ___texCoordScale_3;
	// System.IntPtr UnityEngine.XR.iOS.UnityVideoParams::cvPixelBufferPtr
	intptr_t ___cvPixelBufferPtr_4;

public:
	inline static int32_t get_offset_of_yWidth_0() { return static_cast<int32_t>(offsetof(UnityVideoParams_t4155354995, ___yWidth_0)); }
	inline int32_t get_yWidth_0() const { return ___yWidth_0; }
	inline int32_t* get_address_of_yWidth_0() { return &___yWidth_0; }
	inline void set_yWidth_0(int32_t value)
	{
		___yWidth_0 = value;
	}

	inline static int32_t get_offset_of_yHeight_1() { return static_cast<int32_t>(offsetof(UnityVideoParams_t4155354995, ___yHeight_1)); }
	inline int32_t get_yHeight_1() const { return ___yHeight_1; }
	inline int32_t* get_address_of_yHeight_1() { return &___yHeight_1; }
	inline void set_yHeight_1(int32_t value)
	{
		___yHeight_1 = value;
	}

	inline static int32_t get_offset_of_screenOrientation_2() { return static_cast<int32_t>(offsetof(UnityVideoParams_t4155354995, ___screenOrientation_2)); }
	inline int32_t get_screenOrientation_2() const { return ___screenOrientation_2; }
	inline int32_t* get_address_of_screenOrientation_2() { return &___screenOrientation_2; }
	inline void set_screenOrientation_2(int32_t value)
	{
		___screenOrientation_2 = value;
	}

	inline static int32_t get_offset_of_texCoordScale_3() { return static_cast<int32_t>(offsetof(UnityVideoParams_t4155354995, ___texCoordScale_3)); }
	inline float get_texCoordScale_3() const { return ___texCoordScale_3; }
	inline float* get_address_of_texCoordScale_3() { return &___texCoordScale_3; }
	inline void set_texCoordScale_3(float value)
	{
		___texCoordScale_3 = value;
	}

	inline static int32_t get_offset_of_cvPixelBufferPtr_4() { return static_cast<int32_t>(offsetof(UnityVideoParams_t4155354995, ___cvPixelBufferPtr_4)); }
	inline intptr_t get_cvPixelBufferPtr_4() const { return ___cvPixelBufferPtr_4; }
	inline intptr_t* get_address_of_cvPixelBufferPtr_4() { return &___cvPixelBufferPtr_4; }
	inline void set_cvPixelBufferPtr_4(intptr_t value)
	{
		___cvPixelBufferPtr_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYVIDEOPARAMS_T4155354995_H
#ifndef LIGHTDATATYPE_T2323651587_H
#define LIGHTDATATYPE_T2323651587_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.LightDataType
struct  LightDataType_t2323651587 
{
public:
	// System.Int32 UnityEngine.XR.iOS.LightDataType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LightDataType_t2323651587, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTDATATYPE_T2323651587_H
#ifndef UNITYARFACEGEOMETRY_T4178775532_H
#define UNITYARFACEGEOMETRY_T4178775532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARFaceGeometry
struct  UnityARFaceGeometry_t4178775532 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityARFaceGeometry::vertexCount
	int32_t ___vertexCount_0;
	// System.IntPtr UnityEngine.XR.iOS.UnityARFaceGeometry::vertices
	intptr_t ___vertices_1;
	// System.Int32 UnityEngine.XR.iOS.UnityARFaceGeometry::textureCoordinateCount
	int32_t ___textureCoordinateCount_2;
	// System.IntPtr UnityEngine.XR.iOS.UnityARFaceGeometry::textureCoordinates
	intptr_t ___textureCoordinates_3;
	// System.Int32 UnityEngine.XR.iOS.UnityARFaceGeometry::triangleCount
	int32_t ___triangleCount_4;
	// System.IntPtr UnityEngine.XR.iOS.UnityARFaceGeometry::triangleIndices
	intptr_t ___triangleIndices_5;

public:
	inline static int32_t get_offset_of_vertexCount_0() { return static_cast<int32_t>(offsetof(UnityARFaceGeometry_t4178775532, ___vertexCount_0)); }
	inline int32_t get_vertexCount_0() const { return ___vertexCount_0; }
	inline int32_t* get_address_of_vertexCount_0() { return &___vertexCount_0; }
	inline void set_vertexCount_0(int32_t value)
	{
		___vertexCount_0 = value;
	}

	inline static int32_t get_offset_of_vertices_1() { return static_cast<int32_t>(offsetof(UnityARFaceGeometry_t4178775532, ___vertices_1)); }
	inline intptr_t get_vertices_1() const { return ___vertices_1; }
	inline intptr_t* get_address_of_vertices_1() { return &___vertices_1; }
	inline void set_vertices_1(intptr_t value)
	{
		___vertices_1 = value;
	}

	inline static int32_t get_offset_of_textureCoordinateCount_2() { return static_cast<int32_t>(offsetof(UnityARFaceGeometry_t4178775532, ___textureCoordinateCount_2)); }
	inline int32_t get_textureCoordinateCount_2() const { return ___textureCoordinateCount_2; }
	inline int32_t* get_address_of_textureCoordinateCount_2() { return &___textureCoordinateCount_2; }
	inline void set_textureCoordinateCount_2(int32_t value)
	{
		___textureCoordinateCount_2 = value;
	}

	inline static int32_t get_offset_of_textureCoordinates_3() { return static_cast<int32_t>(offsetof(UnityARFaceGeometry_t4178775532, ___textureCoordinates_3)); }
	inline intptr_t get_textureCoordinates_3() const { return ___textureCoordinates_3; }
	inline intptr_t* get_address_of_textureCoordinates_3() { return &___textureCoordinates_3; }
	inline void set_textureCoordinates_3(intptr_t value)
	{
		___textureCoordinates_3 = value;
	}

	inline static int32_t get_offset_of_triangleCount_4() { return static_cast<int32_t>(offsetof(UnityARFaceGeometry_t4178775532, ___triangleCount_4)); }
	inline int32_t get_triangleCount_4() const { return ___triangleCount_4; }
	inline int32_t* get_address_of_triangleCount_4() { return &___triangleCount_4; }
	inline void set_triangleCount_4(int32_t value)
	{
		___triangleCount_4 = value;
	}

	inline static int32_t get_offset_of_triangleIndices_5() { return static_cast<int32_t>(offsetof(UnityARFaceGeometry_t4178775532, ___triangleIndices_5)); }
	inline intptr_t get_triangleIndices_5() const { return ___triangleIndices_5; }
	inline intptr_t* get_address_of_triangleIndices_5() { return &___triangleIndices_5; }
	inline void set_triangleIndices_5(intptr_t value)
	{
		___triangleIndices_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARFACEGEOMETRY_T4178775532_H
#ifndef DIRECTION_T524882829_H
#define DIRECTION_T524882829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BoxSlider/Direction
struct  Direction_t524882829 
{
public:
	// System.Int32 UnityEngine.UI.BoxSlider/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t524882829, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T524882829_H
#ifndef ARTRACKINGSTATEREASON_T2348933773_H
#define ARTRACKINGSTATEREASON_T2348933773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTrackingStateReason
struct  ARTrackingStateReason_t2348933773 
{
public:
	// System.Int32 UnityEngine.XR.iOS.ARTrackingStateReason::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARTrackingStateReason_t2348933773, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKINGSTATEREASON_T2348933773_H
#ifndef ARTRACKINGSTATE_T3182235352_H
#define ARTRACKINGSTATE_T3182235352_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTrackingState
struct  ARTrackingState_t3182235352 
{
public:
	// System.Int32 UnityEngine.XR.iOS.ARTrackingState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARTrackingState_t3182235352, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKINGSTATE_T3182235352_H
#ifndef TOUCHPHASE_T72348083_H
#define TOUCHPHASE_T72348083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t72348083 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t72348083, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T72348083_H
#ifndef ARPLANEANCHORALIGNMENT_T2311256121_H
#define ARPLANEANCHORALIGNMENT_T2311256121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARPlaneAnchorAlignment
struct  ARPlaneAnchorAlignment_t2311256121 
{
public:
	// System.Int64 UnityEngine.XR.iOS.ARPlaneAnchorAlignment::value__
	int64_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARPlaneAnchorAlignment_t2311256121, ___value___1)); }
	inline int64_t get_value___1() const { return ___value___1; }
	inline int64_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int64_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARPLANEANCHORALIGNMENT_T2311256121_H
#ifndef AXIS_T1354568546_H
#define AXIS_T1354568546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BoxSlider/Axis
struct  Axis_t1354568546 
{
public:
	// System.Int32 UnityEngine.UI.BoxSlider/Axis::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Axis_t1354568546, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T1354568546_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef COLORCHANGEDEVENT_T3019780707_H
#define COLORCHANGEDEVENT_T3019780707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorChangedEvent
struct  ColorChangedEvent_t3019780707  : public UnityEvent_1_t3437345828
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORCHANGEDEVENT_T3019780707_H
#ifndef SELECTIONSTATE_T2656606514_H
#define SELECTIONSTATE_T2656606514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t2656606514 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SelectionState_t2656606514, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T2656606514_H
#ifndef COLORBLOCK_T2139031574_H
#define COLORBLOCK_T2139031574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2139031574 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2555686324  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2555686324  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2555686324  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2555686324  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_NormalColor_0)); }
	inline Color_t2555686324  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2555686324 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2555686324  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_HighlightedColor_1)); }
	inline Color_t2555686324  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2555686324 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2555686324  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_PressedColor_2)); }
	inline Color_t2555686324  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2555686324 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2555686324  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_DisabledColor_3)); }
	inline Color_t2555686324  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2555686324 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2555686324  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2139031574_H
#ifndef TRANSITION_T1769908631_H
#define TRANSITION_T1769908631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t1769908631 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t1769908631, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1769908631_H
#ifndef TKSWIPEDIRECTION_T58737069_H
#define TKSWIPEDIRECTION_T58737069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TKSwipeDirection
struct  TKSwipeDirection_t58737069 
{
public:
	// System.Int32 TKSwipeDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TKSwipeDirection_t58737069, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TKSWIPEDIRECTION_T58737069_H
#ifndef TKGESTURERECOGNIZERSTATE_T2713539247_H
#define TKGESTURERECOGNIZERSTATE_T2713539247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TKGestureRecognizerState
struct  TKGestureRecognizerState_t2713539247 
{
public:
	// System.Int32 TKGestureRecognizerState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TKGestureRecognizerState_t2713539247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TKGESTURERECOGNIZERSTATE_T2713539247_H
#ifndef NULLABLE_1_T308142655_H
#define NULLABLE_1_T308142655_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<TKRect>
struct  Nullable_1_t308142655 
{
public:
	// T System.Nullable`1::value
	TKRect_t2880547869  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t308142655, ___value_0)); }
	inline TKRect_t2880547869  get_value_0() const { return ___value_0; }
	inline TKRect_t2880547869 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(TKRect_t2880547869  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t308142655, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T308142655_H
#ifndef HSVCHANGEDEVENT_T911780251_H
#define HSVCHANGEDEVENT_T911780251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HSVChangedEvent
struct  HSVChangedEvent_t911780251  : public UnityEvent_3_t1697774568
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVCHANGEDEVENT_T911780251_H
#ifndef SWIPEDIRECTION_T3465465729_H
#define SWIPEDIRECTION_T3465465729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.SwipeDirection
struct  SwipeDirection_t3465465729 
{
public:
	// System.Int32 Prime31.SwipeDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SwipeDirection_t3465465729, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPEDIRECTION_T3465465729_H
#ifndef ARTRACKINGQUALITY_T1229573376_H
#define ARTRACKINGQUALITY_T1229573376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTrackingQuality
struct  ARTrackingQuality_t1229573376 
{
public:
	// System.Int64 UnityEngine.XR.iOS.ARTrackingQuality::value__
	int64_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARTrackingQuality_t1229573376, ___value___1)); }
	inline int64_t get_value___1() const { return ___value___1; }
	inline int64_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int64_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKINGQUALITY_T1229573376_H
#ifndef UNITYARMATRIX4X4_T4073345847_H
#define UNITYARMATRIX4X4_T4073345847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARMatrix4x4
struct  UnityARMatrix4x4_t4073345847 
{
public:
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column0
	Vector4_t3319028937  ___column0_0;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column1
	Vector4_t3319028937  ___column1_1;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column2
	Vector4_t3319028937  ___column2_2;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column3
	Vector4_t3319028937  ___column3_3;

public:
	inline static int32_t get_offset_of_column0_0() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t4073345847, ___column0_0)); }
	inline Vector4_t3319028937  get_column0_0() const { return ___column0_0; }
	inline Vector4_t3319028937 * get_address_of_column0_0() { return &___column0_0; }
	inline void set_column0_0(Vector4_t3319028937  value)
	{
		___column0_0 = value;
	}

	inline static int32_t get_offset_of_column1_1() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t4073345847, ___column1_1)); }
	inline Vector4_t3319028937  get_column1_1() const { return ___column1_1; }
	inline Vector4_t3319028937 * get_address_of_column1_1() { return &___column1_1; }
	inline void set_column1_1(Vector4_t3319028937  value)
	{
		___column1_1 = value;
	}

	inline static int32_t get_offset_of_column2_2() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t4073345847, ___column2_2)); }
	inline Vector4_t3319028937  get_column2_2() const { return ___column2_2; }
	inline Vector4_t3319028937 * get_address_of_column2_2() { return &___column2_2; }
	inline void set_column2_2(Vector4_t3319028937  value)
	{
		___column2_2 = value;
	}

	inline static int32_t get_offset_of_column3_3() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t4073345847, ___column3_3)); }
	inline Vector4_t3319028937  get_column3_3() const { return ___column3_3; }
	inline Vector4_t3319028937 * get_address_of_column3_3() { return &___column3_3; }
	inline void set_column3_3(Vector4_t3319028937  value)
	{
		___column3_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARMATRIX4X4_T4073345847_H
#ifndef ARANCHOR_T362826948_H
#define ARANCHOR_T362826948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARAnchor
struct  ARAnchor_t362826948 
{
public:
	// System.String UnityEngine.XR.iOS.ARAnchor::identifier
	String_t* ___identifier_0;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARAnchor::transform
	Matrix4x4_t1817901843  ___transform_1;

public:
	inline static int32_t get_offset_of_identifier_0() { return static_cast<int32_t>(offsetof(ARAnchor_t362826948, ___identifier_0)); }
	inline String_t* get_identifier_0() const { return ___identifier_0; }
	inline String_t** get_address_of_identifier_0() { return &___identifier_0; }
	inline void set_identifier_0(String_t* value)
	{
		___identifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_0), value);
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(ARAnchor_t362826948, ___transform_1)); }
	inline Matrix4x4_t1817901843  get_transform_1() const { return ___transform_1; }
	inline Matrix4x4_t1817901843 * get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(Matrix4x4_t1817901843  value)
	{
		___transform_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARAnchor
struct ARAnchor_t362826948_marshaled_pinvoke
{
	char* ___identifier_0;
	Matrix4x4_t1817901843  ___transform_1;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARAnchor
struct ARAnchor_t362826948_marshaled_com
{
	Il2CppChar* ___identifier_0;
	Matrix4x4_t1817901843  ___transform_1;
};
#endif // ARANCHOR_T362826948_H
#ifndef COLORVALUES_T1603089408_H
#define COLORVALUES_T1603089408_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorValues
struct  ColorValues_t1603089408 
{
public:
	// System.Int32 ColorValues::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorValues_t1603089408, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORVALUES_T1603089408_H
#ifndef DIRECTION_T337909235_H
#define DIRECTION_T337909235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Slider/Direction
struct  Direction_t337909235 
{
public:
	// System.Int32 UnityEngine.UI.Slider/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t337909235, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T337909235_H
#ifndef FOCUSSTATE_T138798281_H
#define FOCUSSTATE_T138798281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FocusSquare/FocusState
struct  FocusState_t138798281 
{
public:
	// System.Int32 FocusSquare/FocusState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FocusState_t138798281, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOCUSSTATE_T138798281_H
#ifndef SERIALIZABLEFROMEDITORMESSAGE_T3245497382_H
#define SERIALIZABLEFROMEDITORMESSAGE_T3245497382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableFromEditorMessage
struct  serializableFromEditorMessage_t3245497382  : public RuntimeObject
{
public:
	// System.Guid Utils.serializableFromEditorMessage::subMessageId
	Guid_t  ___subMessageId_0;
	// Utils.serializableARKitInit Utils.serializableFromEditorMessage::arkitConfigMsg
	serializableARKitInit_t3885066048 * ___arkitConfigMsg_1;

public:
	inline static int32_t get_offset_of_subMessageId_0() { return static_cast<int32_t>(offsetof(serializableFromEditorMessage_t3245497382, ___subMessageId_0)); }
	inline Guid_t  get_subMessageId_0() const { return ___subMessageId_0; }
	inline Guid_t * get_address_of_subMessageId_0() { return &___subMessageId_0; }
	inline void set_subMessageId_0(Guid_t  value)
	{
		___subMessageId_0 = value;
	}

	inline static int32_t get_offset_of_arkitConfigMsg_1() { return static_cast<int32_t>(offsetof(serializableFromEditorMessage_t3245497382, ___arkitConfigMsg_1)); }
	inline serializableARKitInit_t3885066048 * get_arkitConfigMsg_1() const { return ___arkitConfigMsg_1; }
	inline serializableARKitInit_t3885066048 ** get_address_of_arkitConfigMsg_1() { return &___arkitConfigMsg_1; }
	inline void set_arkitConfigMsg_1(serializableARKitInit_t3885066048 * value)
	{
		___arkitConfigMsg_1 = value;
		Il2CppCodeGenWriteBarrier((&___arkitConfigMsg_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEFROMEDITORMESSAGE_T3245497382_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef ARCAMERA_T2831687281_H
#define ARCAMERA_T2831687281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARCamera
struct  ARCamera_t2831687281 
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARCamera::worldTransform
	Matrix4x4_t1817901843  ___worldTransform_0;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARCamera::eulerAngles
	Vector3_t3722313464  ___eulerAngles_1;
	// UnityEngine.XR.iOS.ARTrackingQuality UnityEngine.XR.iOS.ARCamera::trackingQuality
	int64_t ___trackingQuality_2;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARCamera::intrinsics_row1
	Vector3_t3722313464  ___intrinsics_row1_3;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARCamera::intrinsics_row2
	Vector3_t3722313464  ___intrinsics_row2_4;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARCamera::intrinsics_row3
	Vector3_t3722313464  ___intrinsics_row3_5;
	// UnityEngine.XR.iOS.ARSize UnityEngine.XR.iOS.ARCamera::imageResolution
	ARSize_t208719028  ___imageResolution_6;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(ARCamera_t2831687281, ___worldTransform_0)); }
	inline Matrix4x4_t1817901843  get_worldTransform_0() const { return ___worldTransform_0; }
	inline Matrix4x4_t1817901843 * get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(Matrix4x4_t1817901843  value)
	{
		___worldTransform_0 = value;
	}

	inline static int32_t get_offset_of_eulerAngles_1() { return static_cast<int32_t>(offsetof(ARCamera_t2831687281, ___eulerAngles_1)); }
	inline Vector3_t3722313464  get_eulerAngles_1() const { return ___eulerAngles_1; }
	inline Vector3_t3722313464 * get_address_of_eulerAngles_1() { return &___eulerAngles_1; }
	inline void set_eulerAngles_1(Vector3_t3722313464  value)
	{
		___eulerAngles_1 = value;
	}

	inline static int32_t get_offset_of_trackingQuality_2() { return static_cast<int32_t>(offsetof(ARCamera_t2831687281, ___trackingQuality_2)); }
	inline int64_t get_trackingQuality_2() const { return ___trackingQuality_2; }
	inline int64_t* get_address_of_trackingQuality_2() { return &___trackingQuality_2; }
	inline void set_trackingQuality_2(int64_t value)
	{
		___trackingQuality_2 = value;
	}

	inline static int32_t get_offset_of_intrinsics_row1_3() { return static_cast<int32_t>(offsetof(ARCamera_t2831687281, ___intrinsics_row1_3)); }
	inline Vector3_t3722313464  get_intrinsics_row1_3() const { return ___intrinsics_row1_3; }
	inline Vector3_t3722313464 * get_address_of_intrinsics_row1_3() { return &___intrinsics_row1_3; }
	inline void set_intrinsics_row1_3(Vector3_t3722313464  value)
	{
		___intrinsics_row1_3 = value;
	}

	inline static int32_t get_offset_of_intrinsics_row2_4() { return static_cast<int32_t>(offsetof(ARCamera_t2831687281, ___intrinsics_row2_4)); }
	inline Vector3_t3722313464  get_intrinsics_row2_4() const { return ___intrinsics_row2_4; }
	inline Vector3_t3722313464 * get_address_of_intrinsics_row2_4() { return &___intrinsics_row2_4; }
	inline void set_intrinsics_row2_4(Vector3_t3722313464  value)
	{
		___intrinsics_row2_4 = value;
	}

	inline static int32_t get_offset_of_intrinsics_row3_5() { return static_cast<int32_t>(offsetof(ARCamera_t2831687281, ___intrinsics_row3_5)); }
	inline Vector3_t3722313464  get_intrinsics_row3_5() const { return ___intrinsics_row3_5; }
	inline Vector3_t3722313464 * get_address_of_intrinsics_row3_5() { return &___intrinsics_row3_5; }
	inline void set_intrinsics_row3_5(Vector3_t3722313464  value)
	{
		___intrinsics_row3_5 = value;
	}

	inline static int32_t get_offset_of_imageResolution_6() { return static_cast<int32_t>(offsetof(ARCamera_t2831687281, ___imageResolution_6)); }
	inline ARSize_t208719028  get_imageResolution_6() const { return ___imageResolution_6; }
	inline ARSize_t208719028 * get_address_of_imageResolution_6() { return &___imageResolution_6; }
	inline void set_imageResolution_6(ARSize_t208719028  value)
	{
		___imageResolution_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCAMERA_T2831687281_H
#ifndef UNITYARFACEANCHORDATA_T2028622935_H
#define UNITYARFACEANCHORDATA_T2028622935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARFaceAnchorData
struct  UnityARFaceAnchorData_t2028622935 
{
public:
	// System.IntPtr UnityEngine.XR.iOS.UnityARFaceAnchorData::ptrIdentifier
	intptr_t ___ptrIdentifier_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARFaceAnchorData::transform
	UnityARMatrix4x4_t4073345847  ___transform_1;
	// UnityEngine.XR.iOS.UnityARFaceGeometry UnityEngine.XR.iOS.UnityARFaceAnchorData::faceGeometry
	UnityARFaceGeometry_t4178775532  ___faceGeometry_2;
	// System.IntPtr UnityEngine.XR.iOS.UnityARFaceAnchorData::blendShapes
	intptr_t ___blendShapes_3;

public:
	inline static int32_t get_offset_of_ptrIdentifier_0() { return static_cast<int32_t>(offsetof(UnityARFaceAnchorData_t2028622935, ___ptrIdentifier_0)); }
	inline intptr_t get_ptrIdentifier_0() const { return ___ptrIdentifier_0; }
	inline intptr_t* get_address_of_ptrIdentifier_0() { return &___ptrIdentifier_0; }
	inline void set_ptrIdentifier_0(intptr_t value)
	{
		___ptrIdentifier_0 = value;
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(UnityARFaceAnchorData_t2028622935, ___transform_1)); }
	inline UnityARMatrix4x4_t4073345847  get_transform_1() const { return ___transform_1; }
	inline UnityARMatrix4x4_t4073345847 * get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(UnityARMatrix4x4_t4073345847  value)
	{
		___transform_1 = value;
	}

	inline static int32_t get_offset_of_faceGeometry_2() { return static_cast<int32_t>(offsetof(UnityARFaceAnchorData_t2028622935, ___faceGeometry_2)); }
	inline UnityARFaceGeometry_t4178775532  get_faceGeometry_2() const { return ___faceGeometry_2; }
	inline UnityARFaceGeometry_t4178775532 * get_address_of_faceGeometry_2() { return &___faceGeometry_2; }
	inline void set_faceGeometry_2(UnityARFaceGeometry_t4178775532  value)
	{
		___faceGeometry_2 = value;
	}

	inline static int32_t get_offset_of_blendShapes_3() { return static_cast<int32_t>(offsetof(UnityARFaceAnchorData_t2028622935, ___blendShapes_3)); }
	inline intptr_t get_blendShapes_3() const { return ___blendShapes_3; }
	inline intptr_t* get_address_of_blendShapes_3() { return &___blendShapes_3; }
	inline void set_blendShapes_3(intptr_t value)
	{
		___blendShapes_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARFACEANCHORDATA_T2028622935_H
#ifndef ARFACEGEOMETRY_T5139606_H
#define ARFACEGEOMETRY_T5139606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARFaceGeometry
struct  ARFaceGeometry_t5139606  : public RuntimeObject
{
public:
	// UnityEngine.XR.iOS.UnityARFaceGeometry UnityEngine.XR.iOS.ARFaceGeometry::uFaceGeometry
	UnityARFaceGeometry_t4178775532  ___uFaceGeometry_0;

public:
	inline static int32_t get_offset_of_uFaceGeometry_0() { return static_cast<int32_t>(offsetof(ARFaceGeometry_t5139606, ___uFaceGeometry_0)); }
	inline UnityARFaceGeometry_t4178775532  get_uFaceGeometry_0() const { return ___uFaceGeometry_0; }
	inline UnityARFaceGeometry_t4178775532 * get_address_of_uFaceGeometry_0() { return &___uFaceGeometry_0; }
	inline void set_uFaceGeometry_0(UnityARFaceGeometry_t4178775532  value)
	{
		___uFaceGeometry_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARFACEGEOMETRY_T5139606_H
#ifndef TKABSTRACTGESTURERECOGNIZER_T1903772339_H
#define TKABSTRACTGESTURERECOGNIZER_T1903772339_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TKAbstractGestureRecognizer
struct  TKAbstractGestureRecognizer_t1903772339  : public RuntimeObject
{
public:
	// System.Boolean TKAbstractGestureRecognizer::enabled
	bool ___enabled_0;
	// System.Nullable`1<TKRect> TKAbstractGestureRecognizer::boundaryFrame
	Nullable_1_t308142655  ___boundaryFrame_1;
	// System.UInt32 TKAbstractGestureRecognizer::zIndex
	uint32_t ___zIndex_2;
	// TKGestureRecognizerState TKAbstractGestureRecognizer::_state
	int32_t ____state_3;
	// System.Boolean TKAbstractGestureRecognizer::alwaysSendTouchesMoved
	bool ___alwaysSendTouchesMoved_4;
	// System.Collections.Generic.List`1<TKTouch> TKAbstractGestureRecognizer::_trackingTouches
	List_1_t34712449 * ____trackingTouches_5;
	// System.Collections.Generic.List`1<TKTouch> TKAbstractGestureRecognizer::_subsetOfTouchesBeingTrackedApplicableToCurrentRecognizer
	List_1_t34712449 * ____subsetOfTouchesBeingTrackedApplicableToCurrentRecognizer_6;
	// System.Boolean TKAbstractGestureRecognizer::_sentTouchesBegan
	bool ____sentTouchesBegan_7;
	// System.Boolean TKAbstractGestureRecognizer::_sentTouchesMoved
	bool ____sentTouchesMoved_8;
	// System.Boolean TKAbstractGestureRecognizer::_sentTouchesEnded
	bool ____sentTouchesEnded_9;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(TKAbstractGestureRecognizer_t1903772339, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_boundaryFrame_1() { return static_cast<int32_t>(offsetof(TKAbstractGestureRecognizer_t1903772339, ___boundaryFrame_1)); }
	inline Nullable_1_t308142655  get_boundaryFrame_1() const { return ___boundaryFrame_1; }
	inline Nullable_1_t308142655 * get_address_of_boundaryFrame_1() { return &___boundaryFrame_1; }
	inline void set_boundaryFrame_1(Nullable_1_t308142655  value)
	{
		___boundaryFrame_1 = value;
	}

	inline static int32_t get_offset_of_zIndex_2() { return static_cast<int32_t>(offsetof(TKAbstractGestureRecognizer_t1903772339, ___zIndex_2)); }
	inline uint32_t get_zIndex_2() const { return ___zIndex_2; }
	inline uint32_t* get_address_of_zIndex_2() { return &___zIndex_2; }
	inline void set_zIndex_2(uint32_t value)
	{
		___zIndex_2 = value;
	}

	inline static int32_t get_offset_of__state_3() { return static_cast<int32_t>(offsetof(TKAbstractGestureRecognizer_t1903772339, ____state_3)); }
	inline int32_t get__state_3() const { return ____state_3; }
	inline int32_t* get_address_of__state_3() { return &____state_3; }
	inline void set__state_3(int32_t value)
	{
		____state_3 = value;
	}

	inline static int32_t get_offset_of_alwaysSendTouchesMoved_4() { return static_cast<int32_t>(offsetof(TKAbstractGestureRecognizer_t1903772339, ___alwaysSendTouchesMoved_4)); }
	inline bool get_alwaysSendTouchesMoved_4() const { return ___alwaysSendTouchesMoved_4; }
	inline bool* get_address_of_alwaysSendTouchesMoved_4() { return &___alwaysSendTouchesMoved_4; }
	inline void set_alwaysSendTouchesMoved_4(bool value)
	{
		___alwaysSendTouchesMoved_4 = value;
	}

	inline static int32_t get_offset_of__trackingTouches_5() { return static_cast<int32_t>(offsetof(TKAbstractGestureRecognizer_t1903772339, ____trackingTouches_5)); }
	inline List_1_t34712449 * get__trackingTouches_5() const { return ____trackingTouches_5; }
	inline List_1_t34712449 ** get_address_of__trackingTouches_5() { return &____trackingTouches_5; }
	inline void set__trackingTouches_5(List_1_t34712449 * value)
	{
		____trackingTouches_5 = value;
		Il2CppCodeGenWriteBarrier((&____trackingTouches_5), value);
	}

	inline static int32_t get_offset_of__subsetOfTouchesBeingTrackedApplicableToCurrentRecognizer_6() { return static_cast<int32_t>(offsetof(TKAbstractGestureRecognizer_t1903772339, ____subsetOfTouchesBeingTrackedApplicableToCurrentRecognizer_6)); }
	inline List_1_t34712449 * get__subsetOfTouchesBeingTrackedApplicableToCurrentRecognizer_6() const { return ____subsetOfTouchesBeingTrackedApplicableToCurrentRecognizer_6; }
	inline List_1_t34712449 ** get_address_of__subsetOfTouchesBeingTrackedApplicableToCurrentRecognizer_6() { return &____subsetOfTouchesBeingTrackedApplicableToCurrentRecognizer_6; }
	inline void set__subsetOfTouchesBeingTrackedApplicableToCurrentRecognizer_6(List_1_t34712449 * value)
	{
		____subsetOfTouchesBeingTrackedApplicableToCurrentRecognizer_6 = value;
		Il2CppCodeGenWriteBarrier((&____subsetOfTouchesBeingTrackedApplicableToCurrentRecognizer_6), value);
	}

	inline static int32_t get_offset_of__sentTouchesBegan_7() { return static_cast<int32_t>(offsetof(TKAbstractGestureRecognizer_t1903772339, ____sentTouchesBegan_7)); }
	inline bool get__sentTouchesBegan_7() const { return ____sentTouchesBegan_7; }
	inline bool* get_address_of__sentTouchesBegan_7() { return &____sentTouchesBegan_7; }
	inline void set__sentTouchesBegan_7(bool value)
	{
		____sentTouchesBegan_7 = value;
	}

	inline static int32_t get_offset_of__sentTouchesMoved_8() { return static_cast<int32_t>(offsetof(TKAbstractGestureRecognizer_t1903772339, ____sentTouchesMoved_8)); }
	inline bool get__sentTouchesMoved_8() const { return ____sentTouchesMoved_8; }
	inline bool* get_address_of__sentTouchesMoved_8() { return &____sentTouchesMoved_8; }
	inline void set__sentTouchesMoved_8(bool value)
	{
		____sentTouchesMoved_8 = value;
	}

	inline static int32_t get_offset_of__sentTouchesEnded_9() { return static_cast<int32_t>(offsetof(TKAbstractGestureRecognizer_t1903772339, ____sentTouchesEnded_9)); }
	inline bool get__sentTouchesEnded_9() const { return ____sentTouchesEnded_9; }
	inline bool* get_address_of__sentTouchesEnded_9() { return &____sentTouchesEnded_9; }
	inline void set__sentTouchesEnded_9(bool value)
	{
		____sentTouchesEnded_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TKABSTRACTGESTURERECOGNIZER_T1903772339_H
#ifndef NAVIGATION_T3049316579_H
#define NAVIGATION_T3049316579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t3049316579 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3250028441 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnUp_1)); }
	inline Selectable_t3250028441 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3250028441 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnDown_2)); }
	inline Selectable_t3250028441 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3250028441 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnLeft_3)); }
	inline Selectable_t3250028441 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3250028441 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnRight_4)); }
	inline Selectable_t3250028441 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3250028441 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T3049316579_H
#ifndef ARPLANEANCHOR_T2049372221_H
#define ARPLANEANCHOR_T2049372221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARPlaneAnchor
struct  ARPlaneAnchor_t2049372221 
{
public:
	// System.String UnityEngine.XR.iOS.ARPlaneAnchor::identifier
	String_t* ___identifier_0;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARPlaneAnchor::transform
	Matrix4x4_t1817901843  ___transform_1;
	// UnityEngine.XR.iOS.ARPlaneAnchorAlignment UnityEngine.XR.iOS.ARPlaneAnchor::alignment
	int64_t ___alignment_2;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARPlaneAnchor::center
	Vector3_t3722313464  ___center_3;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARPlaneAnchor::extent
	Vector3_t3722313464  ___extent_4;

public:
	inline static int32_t get_offset_of_identifier_0() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t2049372221, ___identifier_0)); }
	inline String_t* get_identifier_0() const { return ___identifier_0; }
	inline String_t** get_address_of_identifier_0() { return &___identifier_0; }
	inline void set_identifier_0(String_t* value)
	{
		___identifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_0), value);
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t2049372221, ___transform_1)); }
	inline Matrix4x4_t1817901843  get_transform_1() const { return ___transform_1; }
	inline Matrix4x4_t1817901843 * get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(Matrix4x4_t1817901843  value)
	{
		___transform_1 = value;
	}

	inline static int32_t get_offset_of_alignment_2() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t2049372221, ___alignment_2)); }
	inline int64_t get_alignment_2() const { return ___alignment_2; }
	inline int64_t* get_address_of_alignment_2() { return &___alignment_2; }
	inline void set_alignment_2(int64_t value)
	{
		___alignment_2 = value;
	}

	inline static int32_t get_offset_of_center_3() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t2049372221, ___center_3)); }
	inline Vector3_t3722313464  get_center_3() const { return ___center_3; }
	inline Vector3_t3722313464 * get_address_of_center_3() { return &___center_3; }
	inline void set_center_3(Vector3_t3722313464  value)
	{
		___center_3 = value;
	}

	inline static int32_t get_offset_of_extent_4() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t2049372221, ___extent_4)); }
	inline Vector3_t3722313464  get_extent_4() const { return ___extent_4; }
	inline Vector3_t3722313464 * get_address_of_extent_4() { return &___extent_4; }
	inline void set_extent_4(Vector3_t3722313464  value)
	{
		___extent_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARPlaneAnchor
struct ARPlaneAnchor_t2049372221_marshaled_pinvoke
{
	char* ___identifier_0;
	Matrix4x4_t1817901843  ___transform_1;
	int64_t ___alignment_2;
	Vector3_t3722313464  ___center_3;
	Vector3_t3722313464  ___extent_4;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARPlaneAnchor
struct ARPlaneAnchor_t2049372221_marshaled_com
{
	Il2CppChar* ___identifier_0;
	Matrix4x4_t1817901843  ___transform_1;
	int64_t ___alignment_2;
	Vector3_t3722313464  ___center_3;
	Vector3_t3722313464  ___extent_4;
};
#endif // ARPLANEANCHOR_T2049372221_H
#ifndef SERIALIZABLEARKITINIT_T3885066048_H
#define SERIALIZABLEARKITINIT_T3885066048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableARKitInit
struct  serializableARKitInit_t3885066048  : public RuntimeObject
{
public:
	// Utils.serializableARSessionConfiguration Utils.serializableARKitInit::config
	serializableARSessionConfiguration_t1467016906 * ___config_0;
	// UnityEngine.XR.iOS.UnityARSessionRunOption Utils.serializableARKitInit::runOption
	int32_t ___runOption_1;

public:
	inline static int32_t get_offset_of_config_0() { return static_cast<int32_t>(offsetof(serializableARKitInit_t3885066048, ___config_0)); }
	inline serializableARSessionConfiguration_t1467016906 * get_config_0() const { return ___config_0; }
	inline serializableARSessionConfiguration_t1467016906 ** get_address_of_config_0() { return &___config_0; }
	inline void set_config_0(serializableARSessionConfiguration_t1467016906 * value)
	{
		___config_0 = value;
		Il2CppCodeGenWriteBarrier((&___config_0), value);
	}

	inline static int32_t get_offset_of_runOption_1() { return static_cast<int32_t>(offsetof(serializableARKitInit_t3885066048, ___runOption_1)); }
	inline int32_t get_runOption_1() const { return ___runOption_1; }
	inline int32_t* get_address_of_runOption_1() { return &___runOption_1; }
	inline void set_runOption_1(int32_t value)
	{
		___runOption_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEARKITINIT_T3885066048_H
#ifndef SERIALIZABLEARSESSIONCONFIGURATION_T1467016906_H
#define SERIALIZABLEARSESSIONCONFIGURATION_T1467016906_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableARSessionConfiguration
struct  serializableARSessionConfiguration_t1467016906  : public RuntimeObject
{
public:
	// UnityEngine.XR.iOS.UnityARAlignment Utils.serializableARSessionConfiguration::alignment
	int32_t ___alignment_0;
	// UnityEngine.XR.iOS.UnityARPlaneDetection Utils.serializableARSessionConfiguration::planeDetection
	int32_t ___planeDetection_1;
	// System.Boolean Utils.serializableARSessionConfiguration::getPointCloudData
	bool ___getPointCloudData_2;
	// System.Boolean Utils.serializableARSessionConfiguration::enableLightEstimation
	bool ___enableLightEstimation_3;

public:
	inline static int32_t get_offset_of_alignment_0() { return static_cast<int32_t>(offsetof(serializableARSessionConfiguration_t1467016906, ___alignment_0)); }
	inline int32_t get_alignment_0() const { return ___alignment_0; }
	inline int32_t* get_address_of_alignment_0() { return &___alignment_0; }
	inline void set_alignment_0(int32_t value)
	{
		___alignment_0 = value;
	}

	inline static int32_t get_offset_of_planeDetection_1() { return static_cast<int32_t>(offsetof(serializableARSessionConfiguration_t1467016906, ___planeDetection_1)); }
	inline int32_t get_planeDetection_1() const { return ___planeDetection_1; }
	inline int32_t* get_address_of_planeDetection_1() { return &___planeDetection_1; }
	inline void set_planeDetection_1(int32_t value)
	{
		___planeDetection_1 = value;
	}

	inline static int32_t get_offset_of_getPointCloudData_2() { return static_cast<int32_t>(offsetof(serializableARSessionConfiguration_t1467016906, ___getPointCloudData_2)); }
	inline bool get_getPointCloudData_2() const { return ___getPointCloudData_2; }
	inline bool* get_address_of_getPointCloudData_2() { return &___getPointCloudData_2; }
	inline void set_getPointCloudData_2(bool value)
	{
		___getPointCloudData_2 = value;
	}

	inline static int32_t get_offset_of_enableLightEstimation_3() { return static_cast<int32_t>(offsetof(serializableARSessionConfiguration_t1467016906, ___enableLightEstimation_3)); }
	inline bool get_enableLightEstimation_3() const { return ___enableLightEstimation_3; }
	inline bool* get_address_of_enableLightEstimation_3() { return &___enableLightEstimation_3; }
	inline void set_enableLightEstimation_3(bool value)
	{
		___enableLightEstimation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEARSESSIONCONFIGURATION_T1467016906_H
#ifndef TKLTOUCH_T3484790318_H
#define TKLTOUCH_T3484790318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.TKLTouch
struct  TKLTouch_t3484790318  : public RuntimeObject
{
public:
	// System.Int32 Prime31.TKLTouch::fingerId
	int32_t ___fingerId_0;
	// UnityEngine.Vector2 Prime31.TKLTouch::position
	Vector2_t2156229523  ___position_1;
	// UnityEngine.Vector2 Prime31.TKLTouch::deltaPosition
	Vector2_t2156229523  ___deltaPosition_2;
	// System.Single Prime31.TKLTouch::deltaTime
	float ___deltaTime_3;
	// System.Int32 Prime31.TKLTouch::tapCount
	int32_t ___tapCount_4;
	// UnityEngine.TouchPhase Prime31.TKLTouch::phase
	int32_t ___phase_5;

public:
	inline static int32_t get_offset_of_fingerId_0() { return static_cast<int32_t>(offsetof(TKLTouch_t3484790318, ___fingerId_0)); }
	inline int32_t get_fingerId_0() const { return ___fingerId_0; }
	inline int32_t* get_address_of_fingerId_0() { return &___fingerId_0; }
	inline void set_fingerId_0(int32_t value)
	{
		___fingerId_0 = value;
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(TKLTouch_t3484790318, ___position_1)); }
	inline Vector2_t2156229523  get_position_1() const { return ___position_1; }
	inline Vector2_t2156229523 * get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(Vector2_t2156229523  value)
	{
		___position_1 = value;
	}

	inline static int32_t get_offset_of_deltaPosition_2() { return static_cast<int32_t>(offsetof(TKLTouch_t3484790318, ___deltaPosition_2)); }
	inline Vector2_t2156229523  get_deltaPosition_2() const { return ___deltaPosition_2; }
	inline Vector2_t2156229523 * get_address_of_deltaPosition_2() { return &___deltaPosition_2; }
	inline void set_deltaPosition_2(Vector2_t2156229523  value)
	{
		___deltaPosition_2 = value;
	}

	inline static int32_t get_offset_of_deltaTime_3() { return static_cast<int32_t>(offsetof(TKLTouch_t3484790318, ___deltaTime_3)); }
	inline float get_deltaTime_3() const { return ___deltaTime_3; }
	inline float* get_address_of_deltaTime_3() { return &___deltaTime_3; }
	inline void set_deltaTime_3(float value)
	{
		___deltaTime_3 = value;
	}

	inline static int32_t get_offset_of_tapCount_4() { return static_cast<int32_t>(offsetof(TKLTouch_t3484790318, ___tapCount_4)); }
	inline int32_t get_tapCount_4() const { return ___tapCount_4; }
	inline int32_t* get_address_of_tapCount_4() { return &___tapCount_4; }
	inline void set_tapCount_4(int32_t value)
	{
		___tapCount_4 = value;
	}

	inline static int32_t get_offset_of_phase_5() { return static_cast<int32_t>(offsetof(TKLTouch_t3484790318, ___phase_5)); }
	inline int32_t get_phase_5() const { return ___phase_5; }
	inline int32_t* get_address_of_phase_5() { return &___phase_5; }
	inline void set_phase_5(int32_t value)
	{
		___phase_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TKLTOUCH_T3484790318_H
#ifndef SERIALIZABLEUNITYARPLANEANCHOR_T1446774435_H
#define SERIALIZABLEUNITYARPLANEANCHOR_T1446774435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableUnityARPlaneAnchor
struct  serializableUnityARPlaneAnchor_t1446774435  : public RuntimeObject
{
public:
	// Utils.serializableUnityARMatrix4x4 Utils.serializableUnityARPlaneAnchor::worldTransform
	serializableUnityARMatrix4x4_t78255337 * ___worldTransform_0;
	// Utils.SerializableVector4 Utils.serializableUnityARPlaneAnchor::center
	SerializableVector4_t1862640084 * ___center_1;
	// Utils.SerializableVector4 Utils.serializableUnityARPlaneAnchor::extent
	SerializableVector4_t1862640084 * ___extent_2;
	// UnityEngine.XR.iOS.ARPlaneAnchorAlignment Utils.serializableUnityARPlaneAnchor::planeAlignment
	int64_t ___planeAlignment_3;
	// System.Byte[] Utils.serializableUnityARPlaneAnchor::identifierStr
	ByteU5BU5D_t4116647657* ___identifierStr_4;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(serializableUnityARPlaneAnchor_t1446774435, ___worldTransform_0)); }
	inline serializableUnityARMatrix4x4_t78255337 * get_worldTransform_0() const { return ___worldTransform_0; }
	inline serializableUnityARMatrix4x4_t78255337 ** get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(serializableUnityARMatrix4x4_t78255337 * value)
	{
		___worldTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___worldTransform_0), value);
	}

	inline static int32_t get_offset_of_center_1() { return static_cast<int32_t>(offsetof(serializableUnityARPlaneAnchor_t1446774435, ___center_1)); }
	inline SerializableVector4_t1862640084 * get_center_1() const { return ___center_1; }
	inline SerializableVector4_t1862640084 ** get_address_of_center_1() { return &___center_1; }
	inline void set_center_1(SerializableVector4_t1862640084 * value)
	{
		___center_1 = value;
		Il2CppCodeGenWriteBarrier((&___center_1), value);
	}

	inline static int32_t get_offset_of_extent_2() { return static_cast<int32_t>(offsetof(serializableUnityARPlaneAnchor_t1446774435, ___extent_2)); }
	inline SerializableVector4_t1862640084 * get_extent_2() const { return ___extent_2; }
	inline SerializableVector4_t1862640084 ** get_address_of_extent_2() { return &___extent_2; }
	inline void set_extent_2(SerializableVector4_t1862640084 * value)
	{
		___extent_2 = value;
		Il2CppCodeGenWriteBarrier((&___extent_2), value);
	}

	inline static int32_t get_offset_of_planeAlignment_3() { return static_cast<int32_t>(offsetof(serializableUnityARPlaneAnchor_t1446774435, ___planeAlignment_3)); }
	inline int64_t get_planeAlignment_3() const { return ___planeAlignment_3; }
	inline int64_t* get_address_of_planeAlignment_3() { return &___planeAlignment_3; }
	inline void set_planeAlignment_3(int64_t value)
	{
		___planeAlignment_3 = value;
	}

	inline static int32_t get_offset_of_identifierStr_4() { return static_cast<int32_t>(offsetof(serializableUnityARPlaneAnchor_t1446774435, ___identifierStr_4)); }
	inline ByteU5BU5D_t4116647657* get_identifierStr_4() const { return ___identifierStr_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_identifierStr_4() { return &___identifierStr_4; }
	inline void set_identifierStr_4(ByteU5BU5D_t4116647657* value)
	{
		___identifierStr_4 = value;
		Il2CppCodeGenWriteBarrier((&___identifierStr_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEUNITYARPLANEANCHOR_T1446774435_H
#ifndef SERIALIZABLEUNITYARCAMERA_T4158151215_H
#define SERIALIZABLEUNITYARCAMERA_T4158151215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableUnityARCamera
struct  serializableUnityARCamera_t4158151215  : public RuntimeObject
{
public:
	// Utils.serializableUnityARMatrix4x4 Utils.serializableUnityARCamera::worldTransform
	serializableUnityARMatrix4x4_t78255337 * ___worldTransform_0;
	// Utils.serializableUnityARMatrix4x4 Utils.serializableUnityARCamera::projectionMatrix
	serializableUnityARMatrix4x4_t78255337 * ___projectionMatrix_1;
	// UnityEngine.XR.iOS.ARTrackingState Utils.serializableUnityARCamera::trackingState
	int32_t ___trackingState_2;
	// UnityEngine.XR.iOS.ARTrackingStateReason Utils.serializableUnityARCamera::trackingReason
	int32_t ___trackingReason_3;
	// UnityEngine.XR.iOS.UnityVideoParams Utils.serializableUnityARCamera::videoParams
	UnityVideoParams_t4155354995  ___videoParams_4;
	// Utils.serializableUnityARLightData Utils.serializableUnityARCamera::lightData
	serializableUnityARLightData_t3935513283 * ___lightData_5;
	// Utils.serializablePointCloud Utils.serializableUnityARCamera::pointCloud
	serializablePointCloud_t455238287 * ___pointCloud_6;
	// Utils.serializableUnityARMatrix4x4 Utils.serializableUnityARCamera::displayTransform
	serializableUnityARMatrix4x4_t78255337 * ___displayTransform_7;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t4158151215, ___worldTransform_0)); }
	inline serializableUnityARMatrix4x4_t78255337 * get_worldTransform_0() const { return ___worldTransform_0; }
	inline serializableUnityARMatrix4x4_t78255337 ** get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(serializableUnityARMatrix4x4_t78255337 * value)
	{
		___worldTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___worldTransform_0), value);
	}

	inline static int32_t get_offset_of_projectionMatrix_1() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t4158151215, ___projectionMatrix_1)); }
	inline serializableUnityARMatrix4x4_t78255337 * get_projectionMatrix_1() const { return ___projectionMatrix_1; }
	inline serializableUnityARMatrix4x4_t78255337 ** get_address_of_projectionMatrix_1() { return &___projectionMatrix_1; }
	inline void set_projectionMatrix_1(serializableUnityARMatrix4x4_t78255337 * value)
	{
		___projectionMatrix_1 = value;
		Il2CppCodeGenWriteBarrier((&___projectionMatrix_1), value);
	}

	inline static int32_t get_offset_of_trackingState_2() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t4158151215, ___trackingState_2)); }
	inline int32_t get_trackingState_2() const { return ___trackingState_2; }
	inline int32_t* get_address_of_trackingState_2() { return &___trackingState_2; }
	inline void set_trackingState_2(int32_t value)
	{
		___trackingState_2 = value;
	}

	inline static int32_t get_offset_of_trackingReason_3() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t4158151215, ___trackingReason_3)); }
	inline int32_t get_trackingReason_3() const { return ___trackingReason_3; }
	inline int32_t* get_address_of_trackingReason_3() { return &___trackingReason_3; }
	inline void set_trackingReason_3(int32_t value)
	{
		___trackingReason_3 = value;
	}

	inline static int32_t get_offset_of_videoParams_4() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t4158151215, ___videoParams_4)); }
	inline UnityVideoParams_t4155354995  get_videoParams_4() const { return ___videoParams_4; }
	inline UnityVideoParams_t4155354995 * get_address_of_videoParams_4() { return &___videoParams_4; }
	inline void set_videoParams_4(UnityVideoParams_t4155354995  value)
	{
		___videoParams_4 = value;
	}

	inline static int32_t get_offset_of_lightData_5() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t4158151215, ___lightData_5)); }
	inline serializableUnityARLightData_t3935513283 * get_lightData_5() const { return ___lightData_5; }
	inline serializableUnityARLightData_t3935513283 ** get_address_of_lightData_5() { return &___lightData_5; }
	inline void set_lightData_5(serializableUnityARLightData_t3935513283 * value)
	{
		___lightData_5 = value;
		Il2CppCodeGenWriteBarrier((&___lightData_5), value);
	}

	inline static int32_t get_offset_of_pointCloud_6() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t4158151215, ___pointCloud_6)); }
	inline serializablePointCloud_t455238287 * get_pointCloud_6() const { return ___pointCloud_6; }
	inline serializablePointCloud_t455238287 ** get_address_of_pointCloud_6() { return &___pointCloud_6; }
	inline void set_pointCloud_6(serializablePointCloud_t455238287 * value)
	{
		___pointCloud_6 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloud_6), value);
	}

	inline static int32_t get_offset_of_displayTransform_7() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t4158151215, ___displayTransform_7)); }
	inline serializableUnityARMatrix4x4_t78255337 * get_displayTransform_7() const { return ___displayTransform_7; }
	inline serializableUnityARMatrix4x4_t78255337 ** get_address_of_displayTransform_7() { return &___displayTransform_7; }
	inline void set_displayTransform_7(serializableUnityARMatrix4x4_t78255337 * value)
	{
		___displayTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___displayTransform_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEUNITYARCAMERA_T4158151215_H
#ifndef SERIALIZABLEUNITYARLIGHTDATA_T3935513283_H
#define SERIALIZABLEUNITYARLIGHTDATA_T3935513283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils.serializableUnityARLightData
struct  serializableUnityARLightData_t3935513283  : public RuntimeObject
{
public:
	// UnityEngine.XR.iOS.LightDataType Utils.serializableUnityARLightData::whichLight
	int32_t ___whichLight_0;
	// Utils.serializableSHC Utils.serializableUnityARLightData::lightSHC
	serializableSHC_t2667429767 * ___lightSHC_1;
	// Utils.SerializableVector4 Utils.serializableUnityARLightData::primaryLightDirAndIntensity
	SerializableVector4_t1862640084 * ___primaryLightDirAndIntensity_2;
	// System.Single Utils.serializableUnityARLightData::ambientIntensity
	float ___ambientIntensity_3;
	// System.Single Utils.serializableUnityARLightData::ambientColorTemperature
	float ___ambientColorTemperature_4;

public:
	inline static int32_t get_offset_of_whichLight_0() { return static_cast<int32_t>(offsetof(serializableUnityARLightData_t3935513283, ___whichLight_0)); }
	inline int32_t get_whichLight_0() const { return ___whichLight_0; }
	inline int32_t* get_address_of_whichLight_0() { return &___whichLight_0; }
	inline void set_whichLight_0(int32_t value)
	{
		___whichLight_0 = value;
	}

	inline static int32_t get_offset_of_lightSHC_1() { return static_cast<int32_t>(offsetof(serializableUnityARLightData_t3935513283, ___lightSHC_1)); }
	inline serializableSHC_t2667429767 * get_lightSHC_1() const { return ___lightSHC_1; }
	inline serializableSHC_t2667429767 ** get_address_of_lightSHC_1() { return &___lightSHC_1; }
	inline void set_lightSHC_1(serializableSHC_t2667429767 * value)
	{
		___lightSHC_1 = value;
		Il2CppCodeGenWriteBarrier((&___lightSHC_1), value);
	}

	inline static int32_t get_offset_of_primaryLightDirAndIntensity_2() { return static_cast<int32_t>(offsetof(serializableUnityARLightData_t3935513283, ___primaryLightDirAndIntensity_2)); }
	inline SerializableVector4_t1862640084 * get_primaryLightDirAndIntensity_2() const { return ___primaryLightDirAndIntensity_2; }
	inline SerializableVector4_t1862640084 ** get_address_of_primaryLightDirAndIntensity_2() { return &___primaryLightDirAndIntensity_2; }
	inline void set_primaryLightDirAndIntensity_2(SerializableVector4_t1862640084 * value)
	{
		___primaryLightDirAndIntensity_2 = value;
		Il2CppCodeGenWriteBarrier((&___primaryLightDirAndIntensity_2), value);
	}

	inline static int32_t get_offset_of_ambientIntensity_3() { return static_cast<int32_t>(offsetof(serializableUnityARLightData_t3935513283, ___ambientIntensity_3)); }
	inline float get_ambientIntensity_3() const { return ___ambientIntensity_3; }
	inline float* get_address_of_ambientIntensity_3() { return &___ambientIntensity_3; }
	inline void set_ambientIntensity_3(float value)
	{
		___ambientIntensity_3 = value;
	}

	inline static int32_t get_offset_of_ambientColorTemperature_4() { return static_cast<int32_t>(offsetof(serializableUnityARLightData_t3935513283, ___ambientColorTemperature_4)); }
	inline float get_ambientColorTemperature_4() const { return ___ambientColorTemperature_4; }
	inline float* get_address_of_ambientColorTemperature_4() { return &___ambientColorTemperature_4; }
	inline void set_ambientColorTemperature_4(float value)
	{
		___ambientColorTemperature_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEUNITYARLIGHTDATA_T3935513283_H
#ifndef ARFACEANCHOR_T1844206636_H
#define ARFACEANCHOR_T1844206636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARFaceAnchor
struct  ARFaceAnchor_t1844206636  : public RuntimeObject
{
public:
	// UnityEngine.XR.iOS.UnityARFaceAnchorData UnityEngine.XR.iOS.ARFaceAnchor::faceAnchorData
	UnityARFaceAnchorData_t2028622935  ___faceAnchorData_0;

public:
	inline static int32_t get_offset_of_faceAnchorData_0() { return static_cast<int32_t>(offsetof(ARFaceAnchor_t1844206636, ___faceAnchorData_0)); }
	inline UnityARFaceAnchorData_t2028622935  get_faceAnchorData_0() const { return ___faceAnchorData_0; }
	inline UnityARFaceAnchorData_t2028622935 * get_address_of_faceAnchorData_0() { return &___faceAnchorData_0; }
	inline void set_faceAnchorData_0(UnityARFaceAnchorData_t2028622935  value)
	{
		___faceAnchorData_0 = value;
	}
};

struct ARFaceAnchor_t1844206636_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Single> UnityEngine.XR.iOS.ARFaceAnchor::blendshapesDictionary
	Dictionary_2_t1182523073 * ___blendshapesDictionary_1;
	// UnityEngine.XR.iOS.ARFaceAnchor/DictionaryVisitorHandler UnityEngine.XR.iOS.ARFaceAnchor::<>f__mg$cache0
	DictionaryVisitorHandler_t414487210 * ___U3CU3Ef__mgU24cache0_2;

public:
	inline static int32_t get_offset_of_blendshapesDictionary_1() { return static_cast<int32_t>(offsetof(ARFaceAnchor_t1844206636_StaticFields, ___blendshapesDictionary_1)); }
	inline Dictionary_2_t1182523073 * get_blendshapesDictionary_1() const { return ___blendshapesDictionary_1; }
	inline Dictionary_2_t1182523073 ** get_address_of_blendshapesDictionary_1() { return &___blendshapesDictionary_1; }
	inline void set_blendshapesDictionary_1(Dictionary_2_t1182523073 * value)
	{
		___blendshapesDictionary_1 = value;
		Il2CppCodeGenWriteBarrier((&___blendshapesDictionary_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_2() { return static_cast<int32_t>(offsetof(ARFaceAnchor_t1844206636_StaticFields, ___U3CU3Ef__mgU24cache0_2)); }
	inline DictionaryVisitorHandler_t414487210 * get_U3CU3Ef__mgU24cache0_2() const { return ___U3CU3Ef__mgU24cache0_2; }
	inline DictionaryVisitorHandler_t414487210 ** get_address_of_U3CU3Ef__mgU24cache0_2() { return &___U3CU3Ef__mgU24cache0_2; }
	inline void set_U3CU3Ef__mgU24cache0_2(DictionaryVisitorHandler_t414487210 * value)
	{
		___U3CU3Ef__mgU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARFACEANCHOR_T1844206636_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef TKBUTTONRECOGNIZER_T691501956_H
#define TKBUTTONRECOGNIZER_T691501956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TKButtonRecognizer
struct  TKButtonRecognizer_t691501956  : public TKAbstractGestureRecognizer_t1903772339
{
public:
	// System.Action`1<TKButtonRecognizer> TKButtonRecognizer::onSelectedEvent
	Action_1_t863969551 * ___onSelectedEvent_10;
	// System.Action`1<TKButtonRecognizer> TKButtonRecognizer::onDeselectedEvent
	Action_1_t863969551 * ___onDeselectedEvent_11;
	// System.Action`1<TKButtonRecognizer> TKButtonRecognizer::onTouchUpInsideEvent
	Action_1_t863969551 * ___onTouchUpInsideEvent_12;
	// TKRect TKButtonRecognizer::_defaultFrame
	TKRect_t2880547869  ____defaultFrame_13;
	// TKRect TKButtonRecognizer::_highlightedFrame
	TKRect_t2880547869  ____highlightedFrame_14;

public:
	inline static int32_t get_offset_of_onSelectedEvent_10() { return static_cast<int32_t>(offsetof(TKButtonRecognizer_t691501956, ___onSelectedEvent_10)); }
	inline Action_1_t863969551 * get_onSelectedEvent_10() const { return ___onSelectedEvent_10; }
	inline Action_1_t863969551 ** get_address_of_onSelectedEvent_10() { return &___onSelectedEvent_10; }
	inline void set_onSelectedEvent_10(Action_1_t863969551 * value)
	{
		___onSelectedEvent_10 = value;
		Il2CppCodeGenWriteBarrier((&___onSelectedEvent_10), value);
	}

	inline static int32_t get_offset_of_onDeselectedEvent_11() { return static_cast<int32_t>(offsetof(TKButtonRecognizer_t691501956, ___onDeselectedEvent_11)); }
	inline Action_1_t863969551 * get_onDeselectedEvent_11() const { return ___onDeselectedEvent_11; }
	inline Action_1_t863969551 ** get_address_of_onDeselectedEvent_11() { return &___onDeselectedEvent_11; }
	inline void set_onDeselectedEvent_11(Action_1_t863969551 * value)
	{
		___onDeselectedEvent_11 = value;
		Il2CppCodeGenWriteBarrier((&___onDeselectedEvent_11), value);
	}

	inline static int32_t get_offset_of_onTouchUpInsideEvent_12() { return static_cast<int32_t>(offsetof(TKButtonRecognizer_t691501956, ___onTouchUpInsideEvent_12)); }
	inline Action_1_t863969551 * get_onTouchUpInsideEvent_12() const { return ___onTouchUpInsideEvent_12; }
	inline Action_1_t863969551 ** get_address_of_onTouchUpInsideEvent_12() { return &___onTouchUpInsideEvent_12; }
	inline void set_onTouchUpInsideEvent_12(Action_1_t863969551 * value)
	{
		___onTouchUpInsideEvent_12 = value;
		Il2CppCodeGenWriteBarrier((&___onTouchUpInsideEvent_12), value);
	}

	inline static int32_t get_offset_of__defaultFrame_13() { return static_cast<int32_t>(offsetof(TKButtonRecognizer_t691501956, ____defaultFrame_13)); }
	inline TKRect_t2880547869  get__defaultFrame_13() const { return ____defaultFrame_13; }
	inline TKRect_t2880547869 * get_address_of__defaultFrame_13() { return &____defaultFrame_13; }
	inline void set__defaultFrame_13(TKRect_t2880547869  value)
	{
		____defaultFrame_13 = value;
	}

	inline static int32_t get_offset_of__highlightedFrame_14() { return static_cast<int32_t>(offsetof(TKButtonRecognizer_t691501956, ____highlightedFrame_14)); }
	inline TKRect_t2880547869  get__highlightedFrame_14() const { return ____highlightedFrame_14; }
	inline TKRect_t2880547869 * get_address_of__highlightedFrame_14() { return &____highlightedFrame_14; }
	inline void set__highlightedFrame_14(TKRect_t2880547869  value)
	{
		____highlightedFrame_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TKBUTTONRECOGNIZER_T691501956_H
#ifndef TKTAPRECOGNIZER_T2390504451_H
#define TKTAPRECOGNIZER_T2390504451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TKTapRecognizer
struct  TKTapRecognizer_t2390504451  : public TKAbstractGestureRecognizer_t1903772339
{
public:
	// System.Action`1<TKTapRecognizer> TKTapRecognizer::gestureRecognizedEvent
	Action_1_t2562972046 * ___gestureRecognizedEvent_10;
	// System.Int32 TKTapRecognizer::numberOfTapsRequired
	int32_t ___numberOfTapsRequired_11;
	// System.Int32 TKTapRecognizer::numberOfTouchesRequired
	int32_t ___numberOfTouchesRequired_12;
	// System.Single TKTapRecognizer::_maxDurationForTapConsideration
	float ____maxDurationForTapConsideration_13;
	// System.Single TKTapRecognizer::_maxDeltaMovementForTapConsideration
	float ____maxDeltaMovementForTapConsideration_14;
	// System.Single TKTapRecognizer::_touchBeganTime
	float ____touchBeganTime_15;
	// System.Int32 TKTapRecognizer::_preformedTapsCount
	int32_t ____preformedTapsCount_16;

public:
	inline static int32_t get_offset_of_gestureRecognizedEvent_10() { return static_cast<int32_t>(offsetof(TKTapRecognizer_t2390504451, ___gestureRecognizedEvent_10)); }
	inline Action_1_t2562972046 * get_gestureRecognizedEvent_10() const { return ___gestureRecognizedEvent_10; }
	inline Action_1_t2562972046 ** get_address_of_gestureRecognizedEvent_10() { return &___gestureRecognizedEvent_10; }
	inline void set_gestureRecognizedEvent_10(Action_1_t2562972046 * value)
	{
		___gestureRecognizedEvent_10 = value;
		Il2CppCodeGenWriteBarrier((&___gestureRecognizedEvent_10), value);
	}

	inline static int32_t get_offset_of_numberOfTapsRequired_11() { return static_cast<int32_t>(offsetof(TKTapRecognizer_t2390504451, ___numberOfTapsRequired_11)); }
	inline int32_t get_numberOfTapsRequired_11() const { return ___numberOfTapsRequired_11; }
	inline int32_t* get_address_of_numberOfTapsRequired_11() { return &___numberOfTapsRequired_11; }
	inline void set_numberOfTapsRequired_11(int32_t value)
	{
		___numberOfTapsRequired_11 = value;
	}

	inline static int32_t get_offset_of_numberOfTouchesRequired_12() { return static_cast<int32_t>(offsetof(TKTapRecognizer_t2390504451, ___numberOfTouchesRequired_12)); }
	inline int32_t get_numberOfTouchesRequired_12() const { return ___numberOfTouchesRequired_12; }
	inline int32_t* get_address_of_numberOfTouchesRequired_12() { return &___numberOfTouchesRequired_12; }
	inline void set_numberOfTouchesRequired_12(int32_t value)
	{
		___numberOfTouchesRequired_12 = value;
	}

	inline static int32_t get_offset_of__maxDurationForTapConsideration_13() { return static_cast<int32_t>(offsetof(TKTapRecognizer_t2390504451, ____maxDurationForTapConsideration_13)); }
	inline float get__maxDurationForTapConsideration_13() const { return ____maxDurationForTapConsideration_13; }
	inline float* get_address_of__maxDurationForTapConsideration_13() { return &____maxDurationForTapConsideration_13; }
	inline void set__maxDurationForTapConsideration_13(float value)
	{
		____maxDurationForTapConsideration_13 = value;
	}

	inline static int32_t get_offset_of__maxDeltaMovementForTapConsideration_14() { return static_cast<int32_t>(offsetof(TKTapRecognizer_t2390504451, ____maxDeltaMovementForTapConsideration_14)); }
	inline float get__maxDeltaMovementForTapConsideration_14() const { return ____maxDeltaMovementForTapConsideration_14; }
	inline float* get_address_of__maxDeltaMovementForTapConsideration_14() { return &____maxDeltaMovementForTapConsideration_14; }
	inline void set__maxDeltaMovementForTapConsideration_14(float value)
	{
		____maxDeltaMovementForTapConsideration_14 = value;
	}

	inline static int32_t get_offset_of__touchBeganTime_15() { return static_cast<int32_t>(offsetof(TKTapRecognizer_t2390504451, ____touchBeganTime_15)); }
	inline float get__touchBeganTime_15() const { return ____touchBeganTime_15; }
	inline float* get_address_of__touchBeganTime_15() { return &____touchBeganTime_15; }
	inline void set__touchBeganTime_15(float value)
	{
		____touchBeganTime_15 = value;
	}

	inline static int32_t get_offset_of__preformedTapsCount_16() { return static_cast<int32_t>(offsetof(TKTapRecognizer_t2390504451, ____preformedTapsCount_16)); }
	inline int32_t get__preformedTapsCount_16() const { return ____preformedTapsCount_16; }
	inline int32_t* get_address_of__preformedTapsCount_16() { return &____preformedTapsCount_16; }
	inline void set__preformedTapsCount_16(int32_t value)
	{
		____preformedTapsCount_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TKTAPRECOGNIZER_T2390504451_H
#ifndef TKCURVERECOGNIZER_T1881817312_H
#define TKCURVERECOGNIZER_T1881817312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TKCurveRecognizer
struct  TKCurveRecognizer_t1881817312  : public TKAbstractGestureRecognizer_t1903772339
{
public:
	// System.Action`1<TKCurveRecognizer> TKCurveRecognizer::gestureRecognizedEvent
	Action_1_t2054284907 * ___gestureRecognizedEvent_10;
	// System.Action`1<TKCurveRecognizer> TKCurveRecognizer::gestureCompleteEvent
	Action_1_t2054284907 * ___gestureCompleteEvent_11;
	// System.Single TKCurveRecognizer::reportRotationStep
	float ___reportRotationStep_12;
	// System.Single TKCurveRecognizer::squareDistance
	float ___squareDistance_13;
	// System.Single TKCurveRecognizer::maxSharpnes
	float ___maxSharpnes_14;
	// System.Int32 TKCurveRecognizer::minimumNumberOfTouches
	int32_t ___minimumNumberOfTouches_15;
	// System.Int32 TKCurveRecognizer::maximumNumberOfTouches
	int32_t ___maximumNumberOfTouches_16;
	// System.Single TKCurveRecognizer::deltaRotation
	float ___deltaRotation_17;
	// UnityEngine.Vector2 TKCurveRecognizer::_previousLocation
	Vector2_t2156229523  ____previousLocation_18;
	// UnityEngine.Vector2 TKCurveRecognizer::_deltaTranslation
	Vector2_t2156229523  ____deltaTranslation_19;
	// UnityEngine.Vector2 TKCurveRecognizer::_previousDeltaTranslation
	Vector2_t2156229523  ____previousDeltaTranslation_20;

public:
	inline static int32_t get_offset_of_gestureRecognizedEvent_10() { return static_cast<int32_t>(offsetof(TKCurveRecognizer_t1881817312, ___gestureRecognizedEvent_10)); }
	inline Action_1_t2054284907 * get_gestureRecognizedEvent_10() const { return ___gestureRecognizedEvent_10; }
	inline Action_1_t2054284907 ** get_address_of_gestureRecognizedEvent_10() { return &___gestureRecognizedEvent_10; }
	inline void set_gestureRecognizedEvent_10(Action_1_t2054284907 * value)
	{
		___gestureRecognizedEvent_10 = value;
		Il2CppCodeGenWriteBarrier((&___gestureRecognizedEvent_10), value);
	}

	inline static int32_t get_offset_of_gestureCompleteEvent_11() { return static_cast<int32_t>(offsetof(TKCurveRecognizer_t1881817312, ___gestureCompleteEvent_11)); }
	inline Action_1_t2054284907 * get_gestureCompleteEvent_11() const { return ___gestureCompleteEvent_11; }
	inline Action_1_t2054284907 ** get_address_of_gestureCompleteEvent_11() { return &___gestureCompleteEvent_11; }
	inline void set_gestureCompleteEvent_11(Action_1_t2054284907 * value)
	{
		___gestureCompleteEvent_11 = value;
		Il2CppCodeGenWriteBarrier((&___gestureCompleteEvent_11), value);
	}

	inline static int32_t get_offset_of_reportRotationStep_12() { return static_cast<int32_t>(offsetof(TKCurveRecognizer_t1881817312, ___reportRotationStep_12)); }
	inline float get_reportRotationStep_12() const { return ___reportRotationStep_12; }
	inline float* get_address_of_reportRotationStep_12() { return &___reportRotationStep_12; }
	inline void set_reportRotationStep_12(float value)
	{
		___reportRotationStep_12 = value;
	}

	inline static int32_t get_offset_of_squareDistance_13() { return static_cast<int32_t>(offsetof(TKCurveRecognizer_t1881817312, ___squareDistance_13)); }
	inline float get_squareDistance_13() const { return ___squareDistance_13; }
	inline float* get_address_of_squareDistance_13() { return &___squareDistance_13; }
	inline void set_squareDistance_13(float value)
	{
		___squareDistance_13 = value;
	}

	inline static int32_t get_offset_of_maxSharpnes_14() { return static_cast<int32_t>(offsetof(TKCurveRecognizer_t1881817312, ___maxSharpnes_14)); }
	inline float get_maxSharpnes_14() const { return ___maxSharpnes_14; }
	inline float* get_address_of_maxSharpnes_14() { return &___maxSharpnes_14; }
	inline void set_maxSharpnes_14(float value)
	{
		___maxSharpnes_14 = value;
	}

	inline static int32_t get_offset_of_minimumNumberOfTouches_15() { return static_cast<int32_t>(offsetof(TKCurveRecognizer_t1881817312, ___minimumNumberOfTouches_15)); }
	inline int32_t get_minimumNumberOfTouches_15() const { return ___minimumNumberOfTouches_15; }
	inline int32_t* get_address_of_minimumNumberOfTouches_15() { return &___minimumNumberOfTouches_15; }
	inline void set_minimumNumberOfTouches_15(int32_t value)
	{
		___minimumNumberOfTouches_15 = value;
	}

	inline static int32_t get_offset_of_maximumNumberOfTouches_16() { return static_cast<int32_t>(offsetof(TKCurveRecognizer_t1881817312, ___maximumNumberOfTouches_16)); }
	inline int32_t get_maximumNumberOfTouches_16() const { return ___maximumNumberOfTouches_16; }
	inline int32_t* get_address_of_maximumNumberOfTouches_16() { return &___maximumNumberOfTouches_16; }
	inline void set_maximumNumberOfTouches_16(int32_t value)
	{
		___maximumNumberOfTouches_16 = value;
	}

	inline static int32_t get_offset_of_deltaRotation_17() { return static_cast<int32_t>(offsetof(TKCurveRecognizer_t1881817312, ___deltaRotation_17)); }
	inline float get_deltaRotation_17() const { return ___deltaRotation_17; }
	inline float* get_address_of_deltaRotation_17() { return &___deltaRotation_17; }
	inline void set_deltaRotation_17(float value)
	{
		___deltaRotation_17 = value;
	}

	inline static int32_t get_offset_of__previousLocation_18() { return static_cast<int32_t>(offsetof(TKCurveRecognizer_t1881817312, ____previousLocation_18)); }
	inline Vector2_t2156229523  get__previousLocation_18() const { return ____previousLocation_18; }
	inline Vector2_t2156229523 * get_address_of__previousLocation_18() { return &____previousLocation_18; }
	inline void set__previousLocation_18(Vector2_t2156229523  value)
	{
		____previousLocation_18 = value;
	}

	inline static int32_t get_offset_of__deltaTranslation_19() { return static_cast<int32_t>(offsetof(TKCurveRecognizer_t1881817312, ____deltaTranslation_19)); }
	inline Vector2_t2156229523  get__deltaTranslation_19() const { return ____deltaTranslation_19; }
	inline Vector2_t2156229523 * get_address_of__deltaTranslation_19() { return &____deltaTranslation_19; }
	inline void set__deltaTranslation_19(Vector2_t2156229523  value)
	{
		____deltaTranslation_19 = value;
	}

	inline static int32_t get_offset_of__previousDeltaTranslation_20() { return static_cast<int32_t>(offsetof(TKCurveRecognizer_t1881817312, ____previousDeltaTranslation_20)); }
	inline Vector2_t2156229523  get__previousDeltaTranslation_20() const { return ____previousDeltaTranslation_20; }
	inline Vector2_t2156229523 * get_address_of__previousDeltaTranslation_20() { return &____previousDeltaTranslation_20; }
	inline void set__previousDeltaTranslation_20(Vector2_t2156229523  value)
	{
		____previousDeltaTranslation_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TKCURVERECOGNIZER_T1881817312_H
#ifndef TKLONGPRESSRECOGNIZER_T1475862313_H
#define TKLONGPRESSRECOGNIZER_T1475862313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TKLongPressRecognizer
struct  TKLongPressRecognizer_t1475862313  : public TKAbstractGestureRecognizer_t1903772339
{
public:
	// System.Action`1<TKLongPressRecognizer> TKLongPressRecognizer::gestureRecognizedEvent
	Action_1_t1648329908 * ___gestureRecognizedEvent_10;
	// System.Action`1<TKLongPressRecognizer> TKLongPressRecognizer::gestureCompleteEvent
	Action_1_t1648329908 * ___gestureCompleteEvent_11;
	// System.Single TKLongPressRecognizer::minimumPressDuration
	float ___minimumPressDuration_12;
	// System.Int32 TKLongPressRecognizer::requiredTouchesCount
	int32_t ___requiredTouchesCount_13;
	// System.Single TKLongPressRecognizer::allowableMovementCm
	float ___allowableMovementCm_14;
	// UnityEngine.Vector2 TKLongPressRecognizer::_beginLocation
	Vector2_t2156229523  ____beginLocation_15;
	// System.Boolean TKLongPressRecognizer::_waiting
	bool ____waiting_16;

public:
	inline static int32_t get_offset_of_gestureRecognizedEvent_10() { return static_cast<int32_t>(offsetof(TKLongPressRecognizer_t1475862313, ___gestureRecognizedEvent_10)); }
	inline Action_1_t1648329908 * get_gestureRecognizedEvent_10() const { return ___gestureRecognizedEvent_10; }
	inline Action_1_t1648329908 ** get_address_of_gestureRecognizedEvent_10() { return &___gestureRecognizedEvent_10; }
	inline void set_gestureRecognizedEvent_10(Action_1_t1648329908 * value)
	{
		___gestureRecognizedEvent_10 = value;
		Il2CppCodeGenWriteBarrier((&___gestureRecognizedEvent_10), value);
	}

	inline static int32_t get_offset_of_gestureCompleteEvent_11() { return static_cast<int32_t>(offsetof(TKLongPressRecognizer_t1475862313, ___gestureCompleteEvent_11)); }
	inline Action_1_t1648329908 * get_gestureCompleteEvent_11() const { return ___gestureCompleteEvent_11; }
	inline Action_1_t1648329908 ** get_address_of_gestureCompleteEvent_11() { return &___gestureCompleteEvent_11; }
	inline void set_gestureCompleteEvent_11(Action_1_t1648329908 * value)
	{
		___gestureCompleteEvent_11 = value;
		Il2CppCodeGenWriteBarrier((&___gestureCompleteEvent_11), value);
	}

	inline static int32_t get_offset_of_minimumPressDuration_12() { return static_cast<int32_t>(offsetof(TKLongPressRecognizer_t1475862313, ___minimumPressDuration_12)); }
	inline float get_minimumPressDuration_12() const { return ___minimumPressDuration_12; }
	inline float* get_address_of_minimumPressDuration_12() { return &___minimumPressDuration_12; }
	inline void set_minimumPressDuration_12(float value)
	{
		___minimumPressDuration_12 = value;
	}

	inline static int32_t get_offset_of_requiredTouchesCount_13() { return static_cast<int32_t>(offsetof(TKLongPressRecognizer_t1475862313, ___requiredTouchesCount_13)); }
	inline int32_t get_requiredTouchesCount_13() const { return ___requiredTouchesCount_13; }
	inline int32_t* get_address_of_requiredTouchesCount_13() { return &___requiredTouchesCount_13; }
	inline void set_requiredTouchesCount_13(int32_t value)
	{
		___requiredTouchesCount_13 = value;
	}

	inline static int32_t get_offset_of_allowableMovementCm_14() { return static_cast<int32_t>(offsetof(TKLongPressRecognizer_t1475862313, ___allowableMovementCm_14)); }
	inline float get_allowableMovementCm_14() const { return ___allowableMovementCm_14; }
	inline float* get_address_of_allowableMovementCm_14() { return &___allowableMovementCm_14; }
	inline void set_allowableMovementCm_14(float value)
	{
		___allowableMovementCm_14 = value;
	}

	inline static int32_t get_offset_of__beginLocation_15() { return static_cast<int32_t>(offsetof(TKLongPressRecognizer_t1475862313, ____beginLocation_15)); }
	inline Vector2_t2156229523  get__beginLocation_15() const { return ____beginLocation_15; }
	inline Vector2_t2156229523 * get_address_of__beginLocation_15() { return &____beginLocation_15; }
	inline void set__beginLocation_15(Vector2_t2156229523  value)
	{
		____beginLocation_15 = value;
	}

	inline static int32_t get_offset_of__waiting_16() { return static_cast<int32_t>(offsetof(TKLongPressRecognizer_t1475862313, ____waiting_16)); }
	inline bool get__waiting_16() const { return ____waiting_16; }
	inline bool* get_address_of__waiting_16() { return &____waiting_16; }
	inline void set__waiting_16(bool value)
	{
		____waiting_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TKLONGPRESSRECOGNIZER_T1475862313_H
#ifndef TKPANRECOGNIZER_T1603940453_H
#define TKPANRECOGNIZER_T1603940453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TKPanRecognizer
struct  TKPanRecognizer_t1603940453  : public TKAbstractGestureRecognizer_t1903772339
{
public:
	// System.Action`1<TKPanRecognizer> TKPanRecognizer::gestureRecognizedEvent
	Action_1_t1776408048 * ___gestureRecognizedEvent_10;
	// System.Action`1<TKPanRecognizer> TKPanRecognizer::gestureCompleteEvent
	Action_1_t1776408048 * ___gestureCompleteEvent_11;
	// UnityEngine.Vector2 TKPanRecognizer::deltaTranslation
	Vector2_t2156229523  ___deltaTranslation_12;
	// System.Single TKPanRecognizer::deltaTranslationCm
	float ___deltaTranslationCm_13;
	// System.Int32 TKPanRecognizer::minimumNumberOfTouches
	int32_t ___minimumNumberOfTouches_14;
	// System.Int32 TKPanRecognizer::maximumNumberOfTouches
	int32_t ___maximumNumberOfTouches_15;
	// System.Single TKPanRecognizer::totalDeltaMovementInCm
	float ___totalDeltaMovementInCm_16;
	// UnityEngine.Vector2 TKPanRecognizer::_previousLocation
	Vector2_t2156229523  ____previousLocation_17;
	// System.Single TKPanRecognizer::_minDistanceToPanCm
	float ____minDistanceToPanCm_18;
	// UnityEngine.Vector2 TKPanRecognizer::_startPoint
	Vector2_t2156229523  ____startPoint_19;
	// UnityEngine.Vector2 TKPanRecognizer::_endPoint
	Vector2_t2156229523  ____endPoint_20;

public:
	inline static int32_t get_offset_of_gestureRecognizedEvent_10() { return static_cast<int32_t>(offsetof(TKPanRecognizer_t1603940453, ___gestureRecognizedEvent_10)); }
	inline Action_1_t1776408048 * get_gestureRecognizedEvent_10() const { return ___gestureRecognizedEvent_10; }
	inline Action_1_t1776408048 ** get_address_of_gestureRecognizedEvent_10() { return &___gestureRecognizedEvent_10; }
	inline void set_gestureRecognizedEvent_10(Action_1_t1776408048 * value)
	{
		___gestureRecognizedEvent_10 = value;
		Il2CppCodeGenWriteBarrier((&___gestureRecognizedEvent_10), value);
	}

	inline static int32_t get_offset_of_gestureCompleteEvent_11() { return static_cast<int32_t>(offsetof(TKPanRecognizer_t1603940453, ___gestureCompleteEvent_11)); }
	inline Action_1_t1776408048 * get_gestureCompleteEvent_11() const { return ___gestureCompleteEvent_11; }
	inline Action_1_t1776408048 ** get_address_of_gestureCompleteEvent_11() { return &___gestureCompleteEvent_11; }
	inline void set_gestureCompleteEvent_11(Action_1_t1776408048 * value)
	{
		___gestureCompleteEvent_11 = value;
		Il2CppCodeGenWriteBarrier((&___gestureCompleteEvent_11), value);
	}

	inline static int32_t get_offset_of_deltaTranslation_12() { return static_cast<int32_t>(offsetof(TKPanRecognizer_t1603940453, ___deltaTranslation_12)); }
	inline Vector2_t2156229523  get_deltaTranslation_12() const { return ___deltaTranslation_12; }
	inline Vector2_t2156229523 * get_address_of_deltaTranslation_12() { return &___deltaTranslation_12; }
	inline void set_deltaTranslation_12(Vector2_t2156229523  value)
	{
		___deltaTranslation_12 = value;
	}

	inline static int32_t get_offset_of_deltaTranslationCm_13() { return static_cast<int32_t>(offsetof(TKPanRecognizer_t1603940453, ___deltaTranslationCm_13)); }
	inline float get_deltaTranslationCm_13() const { return ___deltaTranslationCm_13; }
	inline float* get_address_of_deltaTranslationCm_13() { return &___deltaTranslationCm_13; }
	inline void set_deltaTranslationCm_13(float value)
	{
		___deltaTranslationCm_13 = value;
	}

	inline static int32_t get_offset_of_minimumNumberOfTouches_14() { return static_cast<int32_t>(offsetof(TKPanRecognizer_t1603940453, ___minimumNumberOfTouches_14)); }
	inline int32_t get_minimumNumberOfTouches_14() const { return ___minimumNumberOfTouches_14; }
	inline int32_t* get_address_of_minimumNumberOfTouches_14() { return &___minimumNumberOfTouches_14; }
	inline void set_minimumNumberOfTouches_14(int32_t value)
	{
		___minimumNumberOfTouches_14 = value;
	}

	inline static int32_t get_offset_of_maximumNumberOfTouches_15() { return static_cast<int32_t>(offsetof(TKPanRecognizer_t1603940453, ___maximumNumberOfTouches_15)); }
	inline int32_t get_maximumNumberOfTouches_15() const { return ___maximumNumberOfTouches_15; }
	inline int32_t* get_address_of_maximumNumberOfTouches_15() { return &___maximumNumberOfTouches_15; }
	inline void set_maximumNumberOfTouches_15(int32_t value)
	{
		___maximumNumberOfTouches_15 = value;
	}

	inline static int32_t get_offset_of_totalDeltaMovementInCm_16() { return static_cast<int32_t>(offsetof(TKPanRecognizer_t1603940453, ___totalDeltaMovementInCm_16)); }
	inline float get_totalDeltaMovementInCm_16() const { return ___totalDeltaMovementInCm_16; }
	inline float* get_address_of_totalDeltaMovementInCm_16() { return &___totalDeltaMovementInCm_16; }
	inline void set_totalDeltaMovementInCm_16(float value)
	{
		___totalDeltaMovementInCm_16 = value;
	}

	inline static int32_t get_offset_of__previousLocation_17() { return static_cast<int32_t>(offsetof(TKPanRecognizer_t1603940453, ____previousLocation_17)); }
	inline Vector2_t2156229523  get__previousLocation_17() const { return ____previousLocation_17; }
	inline Vector2_t2156229523 * get_address_of__previousLocation_17() { return &____previousLocation_17; }
	inline void set__previousLocation_17(Vector2_t2156229523  value)
	{
		____previousLocation_17 = value;
	}

	inline static int32_t get_offset_of__minDistanceToPanCm_18() { return static_cast<int32_t>(offsetof(TKPanRecognizer_t1603940453, ____minDistanceToPanCm_18)); }
	inline float get__minDistanceToPanCm_18() const { return ____minDistanceToPanCm_18; }
	inline float* get_address_of__minDistanceToPanCm_18() { return &____minDistanceToPanCm_18; }
	inline void set__minDistanceToPanCm_18(float value)
	{
		____minDistanceToPanCm_18 = value;
	}

	inline static int32_t get_offset_of__startPoint_19() { return static_cast<int32_t>(offsetof(TKPanRecognizer_t1603940453, ____startPoint_19)); }
	inline Vector2_t2156229523  get__startPoint_19() const { return ____startPoint_19; }
	inline Vector2_t2156229523 * get_address_of__startPoint_19() { return &____startPoint_19; }
	inline void set__startPoint_19(Vector2_t2156229523  value)
	{
		____startPoint_19 = value;
	}

	inline static int32_t get_offset_of__endPoint_20() { return static_cast<int32_t>(offsetof(TKPanRecognizer_t1603940453, ____endPoint_20)); }
	inline Vector2_t2156229523  get__endPoint_20() const { return ____endPoint_20; }
	inline Vector2_t2156229523 * get_address_of__endPoint_20() { return &____endPoint_20; }
	inline void set__endPoint_20(Vector2_t2156229523  value)
	{
		____endPoint_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TKPANRECOGNIZER_T1603940453_H
#ifndef TKPINCHRECOGNIZER_T788645703_H
#define TKPINCHRECOGNIZER_T788645703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TKPinchRecognizer
struct  TKPinchRecognizer_t788645703  : public TKAbstractGestureRecognizer_t1903772339
{
public:
	// System.Action`1<TKPinchRecognizer> TKPinchRecognizer::gestureRecognizedEvent
	Action_1_t961113298 * ___gestureRecognizedEvent_10;
	// System.Action`1<TKPinchRecognizer> TKPinchRecognizer::gestureCompleteEvent
	Action_1_t961113298 * ___gestureCompleteEvent_11;
	// System.Single TKPinchRecognizer::minimumScaleDistanceToRecognize
	float ___minimumScaleDistanceToRecognize_12;
	// System.Single TKPinchRecognizer::deltaScale
	float ___deltaScale_13;
	// System.Single TKPinchRecognizer::_intialDistance
	float ____intialDistance_14;
	// System.Single TKPinchRecognizer::_firstDistance
	float ____firstDistance_15;
	// System.Single TKPinchRecognizer::_previousDistance
	float ____previousDistance_16;

public:
	inline static int32_t get_offset_of_gestureRecognizedEvent_10() { return static_cast<int32_t>(offsetof(TKPinchRecognizer_t788645703, ___gestureRecognizedEvent_10)); }
	inline Action_1_t961113298 * get_gestureRecognizedEvent_10() const { return ___gestureRecognizedEvent_10; }
	inline Action_1_t961113298 ** get_address_of_gestureRecognizedEvent_10() { return &___gestureRecognizedEvent_10; }
	inline void set_gestureRecognizedEvent_10(Action_1_t961113298 * value)
	{
		___gestureRecognizedEvent_10 = value;
		Il2CppCodeGenWriteBarrier((&___gestureRecognizedEvent_10), value);
	}

	inline static int32_t get_offset_of_gestureCompleteEvent_11() { return static_cast<int32_t>(offsetof(TKPinchRecognizer_t788645703, ___gestureCompleteEvent_11)); }
	inline Action_1_t961113298 * get_gestureCompleteEvent_11() const { return ___gestureCompleteEvent_11; }
	inline Action_1_t961113298 ** get_address_of_gestureCompleteEvent_11() { return &___gestureCompleteEvent_11; }
	inline void set_gestureCompleteEvent_11(Action_1_t961113298 * value)
	{
		___gestureCompleteEvent_11 = value;
		Il2CppCodeGenWriteBarrier((&___gestureCompleteEvent_11), value);
	}

	inline static int32_t get_offset_of_minimumScaleDistanceToRecognize_12() { return static_cast<int32_t>(offsetof(TKPinchRecognizer_t788645703, ___minimumScaleDistanceToRecognize_12)); }
	inline float get_minimumScaleDistanceToRecognize_12() const { return ___minimumScaleDistanceToRecognize_12; }
	inline float* get_address_of_minimumScaleDistanceToRecognize_12() { return &___minimumScaleDistanceToRecognize_12; }
	inline void set_minimumScaleDistanceToRecognize_12(float value)
	{
		___minimumScaleDistanceToRecognize_12 = value;
	}

	inline static int32_t get_offset_of_deltaScale_13() { return static_cast<int32_t>(offsetof(TKPinchRecognizer_t788645703, ___deltaScale_13)); }
	inline float get_deltaScale_13() const { return ___deltaScale_13; }
	inline float* get_address_of_deltaScale_13() { return &___deltaScale_13; }
	inline void set_deltaScale_13(float value)
	{
		___deltaScale_13 = value;
	}

	inline static int32_t get_offset_of__intialDistance_14() { return static_cast<int32_t>(offsetof(TKPinchRecognizer_t788645703, ____intialDistance_14)); }
	inline float get__intialDistance_14() const { return ____intialDistance_14; }
	inline float* get_address_of__intialDistance_14() { return &____intialDistance_14; }
	inline void set__intialDistance_14(float value)
	{
		____intialDistance_14 = value;
	}

	inline static int32_t get_offset_of__firstDistance_15() { return static_cast<int32_t>(offsetof(TKPinchRecognizer_t788645703, ____firstDistance_15)); }
	inline float get__firstDistance_15() const { return ____firstDistance_15; }
	inline float* get_address_of__firstDistance_15() { return &____firstDistance_15; }
	inline void set__firstDistance_15(float value)
	{
		____firstDistance_15 = value;
	}

	inline static int32_t get_offset_of__previousDistance_16() { return static_cast<int32_t>(offsetof(TKPinchRecognizer_t788645703, ____previousDistance_16)); }
	inline float get__previousDistance_16() const { return ____previousDistance_16; }
	inline float* get_address_of__previousDistance_16() { return &____previousDistance_16; }
	inline void set__previousDistance_16(float value)
	{
		____previousDistance_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TKPINCHRECOGNIZER_T788645703_H
#ifndef TKROTATIONRECOGNIZER_T651141287_H
#define TKROTATIONRECOGNIZER_T651141287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TKRotationRecognizer
struct  TKRotationRecognizer_t651141287  : public TKAbstractGestureRecognizer_t1903772339
{
public:
	// System.Action`1<TKRotationRecognizer> TKRotationRecognizer::gestureRecognizedEvent
	Action_1_t823608882 * ___gestureRecognizedEvent_10;
	// System.Action`1<TKRotationRecognizer> TKRotationRecognizer::gestureCompleteEvent
	Action_1_t823608882 * ___gestureCompleteEvent_11;
	// System.Single TKRotationRecognizer::deltaRotation
	float ___deltaRotation_12;
	// System.Single TKRotationRecognizer::minimumRotationToRecognize
	float ___minimumRotationToRecognize_13;
	// System.Single TKRotationRecognizer::_previousRotation
	float ____previousRotation_14;
	// System.Single TKRotationRecognizer::_firstRotation
	float ____firstRotation_15;
	// System.Single TKRotationRecognizer::_initialRotation
	float ____initialRotation_16;

public:
	inline static int32_t get_offset_of_gestureRecognizedEvent_10() { return static_cast<int32_t>(offsetof(TKRotationRecognizer_t651141287, ___gestureRecognizedEvent_10)); }
	inline Action_1_t823608882 * get_gestureRecognizedEvent_10() const { return ___gestureRecognizedEvent_10; }
	inline Action_1_t823608882 ** get_address_of_gestureRecognizedEvent_10() { return &___gestureRecognizedEvent_10; }
	inline void set_gestureRecognizedEvent_10(Action_1_t823608882 * value)
	{
		___gestureRecognizedEvent_10 = value;
		Il2CppCodeGenWriteBarrier((&___gestureRecognizedEvent_10), value);
	}

	inline static int32_t get_offset_of_gestureCompleteEvent_11() { return static_cast<int32_t>(offsetof(TKRotationRecognizer_t651141287, ___gestureCompleteEvent_11)); }
	inline Action_1_t823608882 * get_gestureCompleteEvent_11() const { return ___gestureCompleteEvent_11; }
	inline Action_1_t823608882 ** get_address_of_gestureCompleteEvent_11() { return &___gestureCompleteEvent_11; }
	inline void set_gestureCompleteEvent_11(Action_1_t823608882 * value)
	{
		___gestureCompleteEvent_11 = value;
		Il2CppCodeGenWriteBarrier((&___gestureCompleteEvent_11), value);
	}

	inline static int32_t get_offset_of_deltaRotation_12() { return static_cast<int32_t>(offsetof(TKRotationRecognizer_t651141287, ___deltaRotation_12)); }
	inline float get_deltaRotation_12() const { return ___deltaRotation_12; }
	inline float* get_address_of_deltaRotation_12() { return &___deltaRotation_12; }
	inline void set_deltaRotation_12(float value)
	{
		___deltaRotation_12 = value;
	}

	inline static int32_t get_offset_of_minimumRotationToRecognize_13() { return static_cast<int32_t>(offsetof(TKRotationRecognizer_t651141287, ___minimumRotationToRecognize_13)); }
	inline float get_minimumRotationToRecognize_13() const { return ___minimumRotationToRecognize_13; }
	inline float* get_address_of_minimumRotationToRecognize_13() { return &___minimumRotationToRecognize_13; }
	inline void set_minimumRotationToRecognize_13(float value)
	{
		___minimumRotationToRecognize_13 = value;
	}

	inline static int32_t get_offset_of__previousRotation_14() { return static_cast<int32_t>(offsetof(TKRotationRecognizer_t651141287, ____previousRotation_14)); }
	inline float get__previousRotation_14() const { return ____previousRotation_14; }
	inline float* get_address_of__previousRotation_14() { return &____previousRotation_14; }
	inline void set__previousRotation_14(float value)
	{
		____previousRotation_14 = value;
	}

	inline static int32_t get_offset_of__firstRotation_15() { return static_cast<int32_t>(offsetof(TKRotationRecognizer_t651141287, ____firstRotation_15)); }
	inline float get__firstRotation_15() const { return ____firstRotation_15; }
	inline float* get_address_of__firstRotation_15() { return &____firstRotation_15; }
	inline void set__firstRotation_15(float value)
	{
		____firstRotation_15 = value;
	}

	inline static int32_t get_offset_of__initialRotation_16() { return static_cast<int32_t>(offsetof(TKRotationRecognizer_t651141287, ____initialRotation_16)); }
	inline float get__initialRotation_16() const { return ____initialRotation_16; }
	inline float* get_address_of__initialRotation_16() { return &____initialRotation_16; }
	inline void set__initialRotation_16(float value)
	{
		____initialRotation_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TKROTATIONRECOGNIZER_T651141287_H
#ifndef TKSWIPERECOGNIZER_T3210539170_H
#define TKSWIPERECOGNIZER_T3210539170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TKSwipeRecognizer
struct  TKSwipeRecognizer_t3210539170  : public TKAbstractGestureRecognizer_t1903772339
{
public:
	// System.Action`1<TKSwipeRecognizer> TKSwipeRecognizer::gestureRecognizedEvent
	Action_1_t3383006765 * ___gestureRecognizedEvent_10;
	// System.Single TKSwipeRecognizer::timeToSwipe
	float ___timeToSwipe_11;
	// System.Single TKSwipeRecognizer::<swipeVelocity>k__BackingField
	float ___U3CswipeVelocityU3Ek__BackingField_12;
	// TKSwipeDirection TKSwipeRecognizer::<completedSwipeDirection>k__BackingField
	int32_t ___U3CcompletedSwipeDirectionU3Ek__BackingField_13;
	// System.Int32 TKSwipeRecognizer::minimumNumberOfTouches
	int32_t ___minimumNumberOfTouches_14;
	// System.Int32 TKSwipeRecognizer::maximumNumberOfTouches
	int32_t ___maximumNumberOfTouches_15;
	// System.Boolean TKSwipeRecognizer::triggerWhenCriteriaMet
	bool ___triggerWhenCriteriaMet_16;
	// System.Single TKSwipeRecognizer::_minimumDistance
	float ____minimumDistance_17;
	// System.Single TKSwipeRecognizer::_maximumDistance
	float ____maximumDistance_18;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> TKSwipeRecognizer::_points
	List_1_t3628304265 * ____points_19;
	// System.Single TKSwipeRecognizer::_startTime
	float ____startTime_20;

public:
	inline static int32_t get_offset_of_gestureRecognizedEvent_10() { return static_cast<int32_t>(offsetof(TKSwipeRecognizer_t3210539170, ___gestureRecognizedEvent_10)); }
	inline Action_1_t3383006765 * get_gestureRecognizedEvent_10() const { return ___gestureRecognizedEvent_10; }
	inline Action_1_t3383006765 ** get_address_of_gestureRecognizedEvent_10() { return &___gestureRecognizedEvent_10; }
	inline void set_gestureRecognizedEvent_10(Action_1_t3383006765 * value)
	{
		___gestureRecognizedEvent_10 = value;
		Il2CppCodeGenWriteBarrier((&___gestureRecognizedEvent_10), value);
	}

	inline static int32_t get_offset_of_timeToSwipe_11() { return static_cast<int32_t>(offsetof(TKSwipeRecognizer_t3210539170, ___timeToSwipe_11)); }
	inline float get_timeToSwipe_11() const { return ___timeToSwipe_11; }
	inline float* get_address_of_timeToSwipe_11() { return &___timeToSwipe_11; }
	inline void set_timeToSwipe_11(float value)
	{
		___timeToSwipe_11 = value;
	}

	inline static int32_t get_offset_of_U3CswipeVelocityU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(TKSwipeRecognizer_t3210539170, ___U3CswipeVelocityU3Ek__BackingField_12)); }
	inline float get_U3CswipeVelocityU3Ek__BackingField_12() const { return ___U3CswipeVelocityU3Ek__BackingField_12; }
	inline float* get_address_of_U3CswipeVelocityU3Ek__BackingField_12() { return &___U3CswipeVelocityU3Ek__BackingField_12; }
	inline void set_U3CswipeVelocityU3Ek__BackingField_12(float value)
	{
		___U3CswipeVelocityU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CcompletedSwipeDirectionU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(TKSwipeRecognizer_t3210539170, ___U3CcompletedSwipeDirectionU3Ek__BackingField_13)); }
	inline int32_t get_U3CcompletedSwipeDirectionU3Ek__BackingField_13() const { return ___U3CcompletedSwipeDirectionU3Ek__BackingField_13; }
	inline int32_t* get_address_of_U3CcompletedSwipeDirectionU3Ek__BackingField_13() { return &___U3CcompletedSwipeDirectionU3Ek__BackingField_13; }
	inline void set_U3CcompletedSwipeDirectionU3Ek__BackingField_13(int32_t value)
	{
		___U3CcompletedSwipeDirectionU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_minimumNumberOfTouches_14() { return static_cast<int32_t>(offsetof(TKSwipeRecognizer_t3210539170, ___minimumNumberOfTouches_14)); }
	inline int32_t get_minimumNumberOfTouches_14() const { return ___minimumNumberOfTouches_14; }
	inline int32_t* get_address_of_minimumNumberOfTouches_14() { return &___minimumNumberOfTouches_14; }
	inline void set_minimumNumberOfTouches_14(int32_t value)
	{
		___minimumNumberOfTouches_14 = value;
	}

	inline static int32_t get_offset_of_maximumNumberOfTouches_15() { return static_cast<int32_t>(offsetof(TKSwipeRecognizer_t3210539170, ___maximumNumberOfTouches_15)); }
	inline int32_t get_maximumNumberOfTouches_15() const { return ___maximumNumberOfTouches_15; }
	inline int32_t* get_address_of_maximumNumberOfTouches_15() { return &___maximumNumberOfTouches_15; }
	inline void set_maximumNumberOfTouches_15(int32_t value)
	{
		___maximumNumberOfTouches_15 = value;
	}

	inline static int32_t get_offset_of_triggerWhenCriteriaMet_16() { return static_cast<int32_t>(offsetof(TKSwipeRecognizer_t3210539170, ___triggerWhenCriteriaMet_16)); }
	inline bool get_triggerWhenCriteriaMet_16() const { return ___triggerWhenCriteriaMet_16; }
	inline bool* get_address_of_triggerWhenCriteriaMet_16() { return &___triggerWhenCriteriaMet_16; }
	inline void set_triggerWhenCriteriaMet_16(bool value)
	{
		___triggerWhenCriteriaMet_16 = value;
	}

	inline static int32_t get_offset_of__minimumDistance_17() { return static_cast<int32_t>(offsetof(TKSwipeRecognizer_t3210539170, ____minimumDistance_17)); }
	inline float get__minimumDistance_17() const { return ____minimumDistance_17; }
	inline float* get_address_of__minimumDistance_17() { return &____minimumDistance_17; }
	inline void set__minimumDistance_17(float value)
	{
		____minimumDistance_17 = value;
	}

	inline static int32_t get_offset_of__maximumDistance_18() { return static_cast<int32_t>(offsetof(TKSwipeRecognizer_t3210539170, ____maximumDistance_18)); }
	inline float get__maximumDistance_18() const { return ____maximumDistance_18; }
	inline float* get_address_of__maximumDistance_18() { return &____maximumDistance_18; }
	inline void set__maximumDistance_18(float value)
	{
		____maximumDistance_18 = value;
	}

	inline static int32_t get_offset_of__points_19() { return static_cast<int32_t>(offsetof(TKSwipeRecognizer_t3210539170, ____points_19)); }
	inline List_1_t3628304265 * get__points_19() const { return ____points_19; }
	inline List_1_t3628304265 ** get_address_of__points_19() { return &____points_19; }
	inline void set__points_19(List_1_t3628304265 * value)
	{
		____points_19 = value;
		Il2CppCodeGenWriteBarrier((&____points_19), value);
	}

	inline static int32_t get_offset_of__startTime_20() { return static_cast<int32_t>(offsetof(TKSwipeRecognizer_t3210539170, ____startTime_20)); }
	inline float get__startTime_20() const { return ____startTime_20; }
	inline float* get_address_of__startTime_20() { return &____startTime_20; }
	inline void set__startTime_20(float value)
	{
		____startTime_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TKSWIPERECOGNIZER_T3210539170_H
#ifndef TKTOUCHPADRECOGNIZER_T1493779134_H
#define TKTOUCHPADRECOGNIZER_T1493779134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TKTouchPadRecognizer
struct  TKTouchPadRecognizer_t1493779134  : public TKAbstractGestureRecognizer_t1903772339
{
public:
	// System.Action`1<TKTouchPadRecognizer> TKTouchPadRecognizer::gestureRecognizedEvent
	Action_1_t1666246729 * ___gestureRecognizedEvent_10;
	// System.Action`1<TKTouchPadRecognizer> TKTouchPadRecognizer::gestureCompleteEvent
	Action_1_t1666246729 * ___gestureCompleteEvent_11;
	// UnityEngine.AnimationCurve TKTouchPadRecognizer::inputCurve
	AnimationCurve_t3046754366 * ___inputCurve_12;
	// UnityEngine.Vector2 TKTouchPadRecognizer::value
	Vector2_t2156229523  ___value_13;

public:
	inline static int32_t get_offset_of_gestureRecognizedEvent_10() { return static_cast<int32_t>(offsetof(TKTouchPadRecognizer_t1493779134, ___gestureRecognizedEvent_10)); }
	inline Action_1_t1666246729 * get_gestureRecognizedEvent_10() const { return ___gestureRecognizedEvent_10; }
	inline Action_1_t1666246729 ** get_address_of_gestureRecognizedEvent_10() { return &___gestureRecognizedEvent_10; }
	inline void set_gestureRecognizedEvent_10(Action_1_t1666246729 * value)
	{
		___gestureRecognizedEvent_10 = value;
		Il2CppCodeGenWriteBarrier((&___gestureRecognizedEvent_10), value);
	}

	inline static int32_t get_offset_of_gestureCompleteEvent_11() { return static_cast<int32_t>(offsetof(TKTouchPadRecognizer_t1493779134, ___gestureCompleteEvent_11)); }
	inline Action_1_t1666246729 * get_gestureCompleteEvent_11() const { return ___gestureCompleteEvent_11; }
	inline Action_1_t1666246729 ** get_address_of_gestureCompleteEvent_11() { return &___gestureCompleteEvent_11; }
	inline void set_gestureCompleteEvent_11(Action_1_t1666246729 * value)
	{
		___gestureCompleteEvent_11 = value;
		Il2CppCodeGenWriteBarrier((&___gestureCompleteEvent_11), value);
	}

	inline static int32_t get_offset_of_inputCurve_12() { return static_cast<int32_t>(offsetof(TKTouchPadRecognizer_t1493779134, ___inputCurve_12)); }
	inline AnimationCurve_t3046754366 * get_inputCurve_12() const { return ___inputCurve_12; }
	inline AnimationCurve_t3046754366 ** get_address_of_inputCurve_12() { return &___inputCurve_12; }
	inline void set_inputCurve_12(AnimationCurve_t3046754366 * value)
	{
		___inputCurve_12 = value;
		Il2CppCodeGenWriteBarrier((&___inputCurve_12), value);
	}

	inline static int32_t get_offset_of_value_13() { return static_cast<int32_t>(offsetof(TKTouchPadRecognizer_t1493779134, ___value_13)); }
	inline Vector2_t2156229523  get_value_13() const { return ___value_13; }
	inline Vector2_t2156229523 * get_address_of_value_13() { return &___value_13; }
	inline void set_value_13(Vector2_t2156229523  value)
	{
		___value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TKTOUCHPADRECOGNIZER_T1493779134_H
#ifndef ARPLANEANCHORGAMEOBJECT_T1947719815_H
#define ARPLANEANCHORGAMEOBJECT_T1947719815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARPlaneAnchorGameObject
struct  ARPlaneAnchorGameObject_t1947719815  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityEngine.XR.iOS.ARPlaneAnchorGameObject::gameObject
	GameObject_t1113636619 * ___gameObject_0;
	// UnityEngine.XR.iOS.ARPlaneAnchor UnityEngine.XR.iOS.ARPlaneAnchorGameObject::planeAnchor
	ARPlaneAnchor_t2049372221  ___planeAnchor_1;

public:
	inline static int32_t get_offset_of_gameObject_0() { return static_cast<int32_t>(offsetof(ARPlaneAnchorGameObject_t1947719815, ___gameObject_0)); }
	inline GameObject_t1113636619 * get_gameObject_0() const { return ___gameObject_0; }
	inline GameObject_t1113636619 ** get_address_of_gameObject_0() { return &___gameObject_0; }
	inline void set_gameObject_0(GameObject_t1113636619 * value)
	{
		___gameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_0), value);
	}

	inline static int32_t get_offset_of_planeAnchor_1() { return static_cast<int32_t>(offsetof(ARPlaneAnchorGameObject_t1947719815, ___planeAnchor_1)); }
	inline ARPlaneAnchor_t2049372221  get_planeAnchor_1() const { return ___planeAnchor_1; }
	inline ARPlaneAnchor_t2049372221 * get_address_of_planeAnchor_1() { return &___planeAnchor_1; }
	inline void set_planeAnchor_1(ARPlaneAnchor_t2049372221  value)
	{
		___planeAnchor_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARPLANEANCHORGAMEOBJECT_T1947719815_H
#ifndef TKONEFINGERROTATIONRECOGNIZER_T564097970_H
#define TKONEFINGERROTATIONRECOGNIZER_T564097970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TKOneFingerRotationRecognizer
struct  TKOneFingerRotationRecognizer_t564097970  : public TKRotationRecognizer_t651141287
{
public:
	// System.Action`1<TKOneFingerRotationRecognizer> TKOneFingerRotationRecognizer::gestureRecognizedEvent
	Action_1_t736565565 * ___gestureRecognizedEvent_17;
	// System.Action`1<TKOneFingerRotationRecognizer> TKOneFingerRotationRecognizer::gestureCompleteEvent
	Action_1_t736565565 * ___gestureCompleteEvent_18;
	// UnityEngine.Vector2 TKOneFingerRotationRecognizer::targetPosition
	Vector2_t2156229523  ___targetPosition_19;

public:
	inline static int32_t get_offset_of_gestureRecognizedEvent_17() { return static_cast<int32_t>(offsetof(TKOneFingerRotationRecognizer_t564097970, ___gestureRecognizedEvent_17)); }
	inline Action_1_t736565565 * get_gestureRecognizedEvent_17() const { return ___gestureRecognizedEvent_17; }
	inline Action_1_t736565565 ** get_address_of_gestureRecognizedEvent_17() { return &___gestureRecognizedEvent_17; }
	inline void set_gestureRecognizedEvent_17(Action_1_t736565565 * value)
	{
		___gestureRecognizedEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___gestureRecognizedEvent_17), value);
	}

	inline static int32_t get_offset_of_gestureCompleteEvent_18() { return static_cast<int32_t>(offsetof(TKOneFingerRotationRecognizer_t564097970, ___gestureCompleteEvent_18)); }
	inline Action_1_t736565565 * get_gestureCompleteEvent_18() const { return ___gestureCompleteEvent_18; }
	inline Action_1_t736565565 ** get_address_of_gestureCompleteEvent_18() { return &___gestureCompleteEvent_18; }
	inline void set_gestureCompleteEvent_18(Action_1_t736565565 * value)
	{
		___gestureCompleteEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___gestureCompleteEvent_18), value);
	}

	inline static int32_t get_offset_of_targetPosition_19() { return static_cast<int32_t>(offsetof(TKOneFingerRotationRecognizer_t564097970, ___targetPosition_19)); }
	inline Vector2_t2156229523  get_targetPosition_19() const { return ___targetPosition_19; }
	inline Vector2_t2156229523 * get_address_of_targetPosition_19() { return &___targetPosition_19; }
	inline void set_targetPosition_19(Vector2_t2156229523  value)
	{
		___targetPosition_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TKONEFINGERROTATIONRECOGNIZER_T564097970_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef UNITYARUSERANCHOREXAMPLE_T2657819511_H
#define UNITYARUSERANCHOREXAMPLE_T2657819511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityARUserAnchorExample
struct  UnityARUserAnchorExample_t2657819511  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject UnityARUserAnchorExample::prefabObject
	GameObject_t1113636619 * ___prefabObject_2;
	// System.Int32 UnityARUserAnchorExample::distanceFromCamera
	int32_t ___distanceFromCamera_3;
	// System.Collections.Generic.HashSet`1<System.String> UnityARUserAnchorExample::m_Clones
	HashSet_1_t412400163 * ___m_Clones_4;
	// System.Single UnityARUserAnchorExample::m_TimeUntilRemove
	float ___m_TimeUntilRemove_5;

public:
	inline static int32_t get_offset_of_prefabObject_2() { return static_cast<int32_t>(offsetof(UnityARUserAnchorExample_t2657819511, ___prefabObject_2)); }
	inline GameObject_t1113636619 * get_prefabObject_2() const { return ___prefabObject_2; }
	inline GameObject_t1113636619 ** get_address_of_prefabObject_2() { return &___prefabObject_2; }
	inline void set_prefabObject_2(GameObject_t1113636619 * value)
	{
		___prefabObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___prefabObject_2), value);
	}

	inline static int32_t get_offset_of_distanceFromCamera_3() { return static_cast<int32_t>(offsetof(UnityARUserAnchorExample_t2657819511, ___distanceFromCamera_3)); }
	inline int32_t get_distanceFromCamera_3() const { return ___distanceFromCamera_3; }
	inline int32_t* get_address_of_distanceFromCamera_3() { return &___distanceFromCamera_3; }
	inline void set_distanceFromCamera_3(int32_t value)
	{
		___distanceFromCamera_3 = value;
	}

	inline static int32_t get_offset_of_m_Clones_4() { return static_cast<int32_t>(offsetof(UnityARUserAnchorExample_t2657819511, ___m_Clones_4)); }
	inline HashSet_1_t412400163 * get_m_Clones_4() const { return ___m_Clones_4; }
	inline HashSet_1_t412400163 ** get_address_of_m_Clones_4() { return &___m_Clones_4; }
	inline void set_m_Clones_4(HashSet_1_t412400163 * value)
	{
		___m_Clones_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clones_4), value);
	}

	inline static int32_t get_offset_of_m_TimeUntilRemove_5() { return static_cast<int32_t>(offsetof(UnityARUserAnchorExample_t2657819511, ___m_TimeUntilRemove_5)); }
	inline float get_m_TimeUntilRemove_5() const { return ___m_TimeUntilRemove_5; }
	inline float* get_address_of_m_TimeUntilRemove_5() { return &___m_TimeUntilRemove_5; }
	inline void set_m_TimeUntilRemove_5(float value)
	{
		___m_TimeUntilRemove_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARUSERANCHOREXAMPLE_T2657819511_H
#ifndef ARCAMERATRACKER_T1108422940_H
#define ARCAMERATRACKER_T1108422940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARCameraTracker
struct  ARCameraTracker_t1108422940  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera ARCameraTracker::trackedCamera
	Camera_t4157153871 * ___trackedCamera_2;
	// System.Boolean ARCameraTracker::sessionStarted
	bool ___sessionStarted_3;

public:
	inline static int32_t get_offset_of_trackedCamera_2() { return static_cast<int32_t>(offsetof(ARCameraTracker_t1108422940, ___trackedCamera_2)); }
	inline Camera_t4157153871 * get_trackedCamera_2() const { return ___trackedCamera_2; }
	inline Camera_t4157153871 ** get_address_of_trackedCamera_2() { return &___trackedCamera_2; }
	inline void set_trackedCamera_2(Camera_t4157153871 * value)
	{
		___trackedCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___trackedCamera_2), value);
	}

	inline static int32_t get_offset_of_sessionStarted_3() { return static_cast<int32_t>(offsetof(ARCameraTracker_t1108422940, ___sessionStarted_3)); }
	inline bool get_sessionStarted_3() const { return ___sessionStarted_3; }
	inline bool* get_address_of_sessionStarted_3() { return &___sessionStarted_3; }
	inline void set_sessionStarted_3(bool value)
	{
		___sessionStarted_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCAMERATRACKER_T1108422940_H
#ifndef BLENDSHAPEDRIVER_T961242622_H
#define BLENDSHAPEDRIVER_T961242622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BlendshapeDriver
struct  BlendshapeDriver_t961242622  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.SkinnedMeshRenderer BlendshapeDriver::skinnedMeshRenderer
	SkinnedMeshRenderer_t245602842 * ___skinnedMeshRenderer_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Single> BlendshapeDriver::currentBlendShapes
	Dictionary_2_t1182523073 * ___currentBlendShapes_3;

public:
	inline static int32_t get_offset_of_skinnedMeshRenderer_2() { return static_cast<int32_t>(offsetof(BlendshapeDriver_t961242622, ___skinnedMeshRenderer_2)); }
	inline SkinnedMeshRenderer_t245602842 * get_skinnedMeshRenderer_2() const { return ___skinnedMeshRenderer_2; }
	inline SkinnedMeshRenderer_t245602842 ** get_address_of_skinnedMeshRenderer_2() { return &___skinnedMeshRenderer_2; }
	inline void set_skinnedMeshRenderer_2(SkinnedMeshRenderer_t245602842 * value)
	{
		___skinnedMeshRenderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___skinnedMeshRenderer_2), value);
	}

	inline static int32_t get_offset_of_currentBlendShapes_3() { return static_cast<int32_t>(offsetof(BlendshapeDriver_t961242622, ___currentBlendShapes_3)); }
	inline Dictionary_2_t1182523073 * get_currentBlendShapes_3() const { return ___currentBlendShapes_3; }
	inline Dictionary_2_t1182523073 ** get_address_of_currentBlendShapes_3() { return &___currentBlendShapes_3; }
	inline void set_currentBlendShapes_3(Dictionary_2_t1182523073 * value)
	{
		___currentBlendShapes_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentBlendShapes_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLENDSHAPEDRIVER_T961242622_H
#ifndef UNITYARVIDEO_T1146951207_H
#define UNITYARVIDEO_T1146951207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARVideo
struct  UnityARVideo_t1146951207  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Material UnityEngine.XR.iOS.UnityARVideo::m_ClearMaterial
	Material_t340375123 * ___m_ClearMaterial_2;
	// UnityEngine.Rendering.CommandBuffer UnityEngine.XR.iOS.UnityARVideo::m_VideoCommandBuffer
	CommandBuffer_t2206337031 * ___m_VideoCommandBuffer_3;
	// UnityEngine.Texture2D UnityEngine.XR.iOS.UnityARVideo::_videoTextureY
	Texture2D_t3840446185 * ____videoTextureY_4;
	// UnityEngine.Texture2D UnityEngine.XR.iOS.UnityARVideo::_videoTextureCbCr
	Texture2D_t3840446185 * ____videoTextureCbCr_5;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.UnityARVideo::_displayTransform
	Matrix4x4_t1817901843  ____displayTransform_6;
	// System.Boolean UnityEngine.XR.iOS.UnityARVideo::bCommandBufferInitialized
	bool ___bCommandBufferInitialized_7;

public:
	inline static int32_t get_offset_of_m_ClearMaterial_2() { return static_cast<int32_t>(offsetof(UnityARVideo_t1146951207, ___m_ClearMaterial_2)); }
	inline Material_t340375123 * get_m_ClearMaterial_2() const { return ___m_ClearMaterial_2; }
	inline Material_t340375123 ** get_address_of_m_ClearMaterial_2() { return &___m_ClearMaterial_2; }
	inline void set_m_ClearMaterial_2(Material_t340375123 * value)
	{
		___m_ClearMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClearMaterial_2), value);
	}

	inline static int32_t get_offset_of_m_VideoCommandBuffer_3() { return static_cast<int32_t>(offsetof(UnityARVideo_t1146951207, ___m_VideoCommandBuffer_3)); }
	inline CommandBuffer_t2206337031 * get_m_VideoCommandBuffer_3() const { return ___m_VideoCommandBuffer_3; }
	inline CommandBuffer_t2206337031 ** get_address_of_m_VideoCommandBuffer_3() { return &___m_VideoCommandBuffer_3; }
	inline void set_m_VideoCommandBuffer_3(CommandBuffer_t2206337031 * value)
	{
		___m_VideoCommandBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_VideoCommandBuffer_3), value);
	}

	inline static int32_t get_offset_of__videoTextureY_4() { return static_cast<int32_t>(offsetof(UnityARVideo_t1146951207, ____videoTextureY_4)); }
	inline Texture2D_t3840446185 * get__videoTextureY_4() const { return ____videoTextureY_4; }
	inline Texture2D_t3840446185 ** get_address_of__videoTextureY_4() { return &____videoTextureY_4; }
	inline void set__videoTextureY_4(Texture2D_t3840446185 * value)
	{
		____videoTextureY_4 = value;
		Il2CppCodeGenWriteBarrier((&____videoTextureY_4), value);
	}

	inline static int32_t get_offset_of__videoTextureCbCr_5() { return static_cast<int32_t>(offsetof(UnityARVideo_t1146951207, ____videoTextureCbCr_5)); }
	inline Texture2D_t3840446185 * get__videoTextureCbCr_5() const { return ____videoTextureCbCr_5; }
	inline Texture2D_t3840446185 ** get_address_of__videoTextureCbCr_5() { return &____videoTextureCbCr_5; }
	inline void set__videoTextureCbCr_5(Texture2D_t3840446185 * value)
	{
		____videoTextureCbCr_5 = value;
		Il2CppCodeGenWriteBarrier((&____videoTextureCbCr_5), value);
	}

	inline static int32_t get_offset_of__displayTransform_6() { return static_cast<int32_t>(offsetof(UnityARVideo_t1146951207, ____displayTransform_6)); }
	inline Matrix4x4_t1817901843  get__displayTransform_6() const { return ____displayTransform_6; }
	inline Matrix4x4_t1817901843 * get_address_of__displayTransform_6() { return &____displayTransform_6; }
	inline void set__displayTransform_6(Matrix4x4_t1817901843  value)
	{
		____displayTransform_6 = value;
	}

	inline static int32_t get_offset_of_bCommandBufferInitialized_7() { return static_cast<int32_t>(offsetof(UnityARVideo_t1146951207, ___bCommandBufferInitialized_7)); }
	inline bool get_bCommandBufferInitialized_7() const { return ___bCommandBufferInitialized_7; }
	inline bool* get_address_of_bCommandBufferInitialized_7() { return &___bCommandBufferInitialized_7; }
	inline void set_bCommandBufferInitialized_7(bool value)
	{
		___bCommandBufferInitialized_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARVIDEO_T1146951207_H
#ifndef UNITYREMOTEVIDEO_T705138647_H
#define UNITYREMOTEVIDEO_T705138647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityRemoteVideo
struct  UnityRemoteVideo_t705138647  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.XR.iOS.ConnectToEditor UnityEngine.XR.iOS.UnityRemoteVideo::connectToEditor
	ConnectToEditor_t595742893 * ___connectToEditor_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.UnityRemoteVideo::m_Session
	UnityARSessionNativeInterface_t3929719369 * ___m_Session_3;
	// System.Boolean UnityEngine.XR.iOS.UnityRemoteVideo::bTexturesInitialized
	bool ___bTexturesInitialized_4;
	// System.Int32 UnityEngine.XR.iOS.UnityRemoteVideo::currentFrameIndex
	int32_t ___currentFrameIndex_5;
	// System.Byte[] UnityEngine.XR.iOS.UnityRemoteVideo::m_textureYBytes
	ByteU5BU5D_t4116647657* ___m_textureYBytes_6;
	// System.Byte[] UnityEngine.XR.iOS.UnityRemoteVideo::m_textureUVBytes
	ByteU5BU5D_t4116647657* ___m_textureUVBytes_7;
	// System.Byte[] UnityEngine.XR.iOS.UnityRemoteVideo::m_textureYBytes2
	ByteU5BU5D_t4116647657* ___m_textureYBytes2_8;
	// System.Byte[] UnityEngine.XR.iOS.UnityRemoteVideo::m_textureUVBytes2
	ByteU5BU5D_t4116647657* ___m_textureUVBytes2_9;
	// System.Runtime.InteropServices.GCHandle UnityEngine.XR.iOS.UnityRemoteVideo::m_pinnedYArray
	GCHandle_t3351438187  ___m_pinnedYArray_10;
	// System.Runtime.InteropServices.GCHandle UnityEngine.XR.iOS.UnityRemoteVideo::m_pinnedUVArray
	GCHandle_t3351438187  ___m_pinnedUVArray_11;

public:
	inline static int32_t get_offset_of_connectToEditor_2() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t705138647, ___connectToEditor_2)); }
	inline ConnectToEditor_t595742893 * get_connectToEditor_2() const { return ___connectToEditor_2; }
	inline ConnectToEditor_t595742893 ** get_address_of_connectToEditor_2() { return &___connectToEditor_2; }
	inline void set_connectToEditor_2(ConnectToEditor_t595742893 * value)
	{
		___connectToEditor_2 = value;
		Il2CppCodeGenWriteBarrier((&___connectToEditor_2), value);
	}

	inline static int32_t get_offset_of_m_Session_3() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t705138647, ___m_Session_3)); }
	inline UnityARSessionNativeInterface_t3929719369 * get_m_Session_3() const { return ___m_Session_3; }
	inline UnityARSessionNativeInterface_t3929719369 ** get_address_of_m_Session_3() { return &___m_Session_3; }
	inline void set_m_Session_3(UnityARSessionNativeInterface_t3929719369 * value)
	{
		___m_Session_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Session_3), value);
	}

	inline static int32_t get_offset_of_bTexturesInitialized_4() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t705138647, ___bTexturesInitialized_4)); }
	inline bool get_bTexturesInitialized_4() const { return ___bTexturesInitialized_4; }
	inline bool* get_address_of_bTexturesInitialized_4() { return &___bTexturesInitialized_4; }
	inline void set_bTexturesInitialized_4(bool value)
	{
		___bTexturesInitialized_4 = value;
	}

	inline static int32_t get_offset_of_currentFrameIndex_5() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t705138647, ___currentFrameIndex_5)); }
	inline int32_t get_currentFrameIndex_5() const { return ___currentFrameIndex_5; }
	inline int32_t* get_address_of_currentFrameIndex_5() { return &___currentFrameIndex_5; }
	inline void set_currentFrameIndex_5(int32_t value)
	{
		___currentFrameIndex_5 = value;
	}

	inline static int32_t get_offset_of_m_textureYBytes_6() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t705138647, ___m_textureYBytes_6)); }
	inline ByteU5BU5D_t4116647657* get_m_textureYBytes_6() const { return ___m_textureYBytes_6; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_textureYBytes_6() { return &___m_textureYBytes_6; }
	inline void set_m_textureYBytes_6(ByteU5BU5D_t4116647657* value)
	{
		___m_textureYBytes_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_textureYBytes_6), value);
	}

	inline static int32_t get_offset_of_m_textureUVBytes_7() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t705138647, ___m_textureUVBytes_7)); }
	inline ByteU5BU5D_t4116647657* get_m_textureUVBytes_7() const { return ___m_textureUVBytes_7; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_textureUVBytes_7() { return &___m_textureUVBytes_7; }
	inline void set_m_textureUVBytes_7(ByteU5BU5D_t4116647657* value)
	{
		___m_textureUVBytes_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textureUVBytes_7), value);
	}

	inline static int32_t get_offset_of_m_textureYBytes2_8() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t705138647, ___m_textureYBytes2_8)); }
	inline ByteU5BU5D_t4116647657* get_m_textureYBytes2_8() const { return ___m_textureYBytes2_8; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_textureYBytes2_8() { return &___m_textureYBytes2_8; }
	inline void set_m_textureYBytes2_8(ByteU5BU5D_t4116647657* value)
	{
		___m_textureYBytes2_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_textureYBytes2_8), value);
	}

	inline static int32_t get_offset_of_m_textureUVBytes2_9() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t705138647, ___m_textureUVBytes2_9)); }
	inline ByteU5BU5D_t4116647657* get_m_textureUVBytes2_9() const { return ___m_textureUVBytes2_9; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_textureUVBytes2_9() { return &___m_textureUVBytes2_9; }
	inline void set_m_textureUVBytes2_9(ByteU5BU5D_t4116647657* value)
	{
		___m_textureUVBytes2_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_textureUVBytes2_9), value);
	}

	inline static int32_t get_offset_of_m_pinnedYArray_10() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t705138647, ___m_pinnedYArray_10)); }
	inline GCHandle_t3351438187  get_m_pinnedYArray_10() const { return ___m_pinnedYArray_10; }
	inline GCHandle_t3351438187 * get_address_of_m_pinnedYArray_10() { return &___m_pinnedYArray_10; }
	inline void set_m_pinnedYArray_10(GCHandle_t3351438187  value)
	{
		___m_pinnedYArray_10 = value;
	}

	inline static int32_t get_offset_of_m_pinnedUVArray_11() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_t705138647, ___m_pinnedUVArray_11)); }
	inline GCHandle_t3351438187  get_m_pinnedUVArray_11() const { return ___m_pinnedUVArray_11; }
	inline GCHandle_t3351438187 * get_address_of_m_pinnedUVArray_11() { return &___m_pinnedUVArray_11; }
	inline void set_m_pinnedUVArray_11(GCHandle_t3351438187  value)
	{
		___m_pinnedUVArray_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYREMOTEVIDEO_T705138647_H
#ifndef UNITYARFACEANCHORMANAGER_T1630882107_H
#define UNITYARFACEANCHORMANAGER_T1630882107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityARFaceAnchorManager
struct  UnityARFaceAnchorManager_t1630882107  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject UnityARFaceAnchorManager::anchorPrefab
	GameObject_t1113636619 * ___anchorPrefab_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityARFaceAnchorManager::m_session
	UnityARSessionNativeInterface_t3929719369 * ___m_session_3;

public:
	inline static int32_t get_offset_of_anchorPrefab_2() { return static_cast<int32_t>(offsetof(UnityARFaceAnchorManager_t1630882107, ___anchorPrefab_2)); }
	inline GameObject_t1113636619 * get_anchorPrefab_2() const { return ___anchorPrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_anchorPrefab_2() { return &___anchorPrefab_2; }
	inline void set_anchorPrefab_2(GameObject_t1113636619 * value)
	{
		___anchorPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___anchorPrefab_2), value);
	}

	inline static int32_t get_offset_of_m_session_3() { return static_cast<int32_t>(offsetof(UnityARFaceAnchorManager_t1630882107, ___m_session_3)); }
	inline UnityARSessionNativeInterface_t3929719369 * get_m_session_3() const { return ___m_session_3; }
	inline UnityARSessionNativeInterface_t3929719369 ** get_address_of_m_session_3() { return &___m_session_3; }
	inline void set_m_session_3(UnityARSessionNativeInterface_t3929719369 * value)
	{
		___m_session_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_session_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARFACEANCHORMANAGER_T1630882107_H
#ifndef BLENDSHAPEPRINTER_T4276887874_H
#define BLENDSHAPEPRINTER_T4276887874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BlendshapePrinter
struct  BlendshapePrinter_t4276887874  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean BlendshapePrinter::shapeEnabled
	bool ___shapeEnabled_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Single> BlendshapePrinter::currentBlendShapes
	Dictionary_2_t1182523073 * ___currentBlendShapes_3;

public:
	inline static int32_t get_offset_of_shapeEnabled_2() { return static_cast<int32_t>(offsetof(BlendshapePrinter_t4276887874, ___shapeEnabled_2)); }
	inline bool get_shapeEnabled_2() const { return ___shapeEnabled_2; }
	inline bool* get_address_of_shapeEnabled_2() { return &___shapeEnabled_2; }
	inline void set_shapeEnabled_2(bool value)
	{
		___shapeEnabled_2 = value;
	}

	inline static int32_t get_offset_of_currentBlendShapes_3() { return static_cast<int32_t>(offsetof(BlendshapePrinter_t4276887874, ___currentBlendShapes_3)); }
	inline Dictionary_2_t1182523073 * get_currentBlendShapes_3() const { return ___currentBlendShapes_3; }
	inline Dictionary_2_t1182523073 ** get_address_of_currentBlendShapes_3() { return &___currentBlendShapes_3; }
	inline void set_currentBlendShapes_3(Dictionary_2_t1182523073 * value)
	{
		___currentBlendShapes_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentBlendShapes_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLENDSHAPEPRINTER_T4276887874_H
#ifndef EDITORHITTEST_T1253817588_H
#define EDITORHITTEST_T1253817588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.EditorHitTest
struct  EditorHitTest_t1253817588  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UnityEngine.XR.iOS.EditorHitTest::m_HitTransform
	Transform_t3600365921 * ___m_HitTransform_2;
	// System.Single UnityEngine.XR.iOS.EditorHitTest::maxRayDistance
	float ___maxRayDistance_3;
	// UnityEngine.LayerMask UnityEngine.XR.iOS.EditorHitTest::collisionLayerMask
	LayerMask_t3493934918  ___collisionLayerMask_4;

public:
	inline static int32_t get_offset_of_m_HitTransform_2() { return static_cast<int32_t>(offsetof(EditorHitTest_t1253817588, ___m_HitTransform_2)); }
	inline Transform_t3600365921 * get_m_HitTransform_2() const { return ___m_HitTransform_2; }
	inline Transform_t3600365921 ** get_address_of_m_HitTransform_2() { return &___m_HitTransform_2; }
	inline void set_m_HitTransform_2(Transform_t3600365921 * value)
	{
		___m_HitTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_HitTransform_2), value);
	}

	inline static int32_t get_offset_of_maxRayDistance_3() { return static_cast<int32_t>(offsetof(EditorHitTest_t1253817588, ___maxRayDistance_3)); }
	inline float get_maxRayDistance_3() const { return ___maxRayDistance_3; }
	inline float* get_address_of_maxRayDistance_3() { return &___maxRayDistance_3; }
	inline void set_maxRayDistance_3(float value)
	{
		___maxRayDistance_3 = value;
	}

	inline static int32_t get_offset_of_collisionLayerMask_4() { return static_cast<int32_t>(offsetof(EditorHitTest_t1253817588, ___collisionLayerMask_4)); }
	inline LayerMask_t3493934918  get_collisionLayerMask_4() const { return ___collisionLayerMask_4; }
	inline LayerMask_t3493934918 * get_address_of_collisionLayerMask_4() { return &___collisionLayerMask_4; }
	inline void set_collisionLayerMask_4(LayerMask_t3493934918  value)
	{
		___collisionLayerMask_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORHITTEST_T1253817588_H
#ifndef TOUCHKIT_T3280788656_H
#define TOUCHKIT_T3280788656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchKit
struct  TouchKit_t3280788656  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean TouchKit::simulateTouches
	bool ___simulateTouches_2;
	// System.Boolean TouchKit::simulateMultitouch
	bool ___simulateMultitouch_3;
	// System.Boolean TouchKit::drawTouches
	bool ___drawTouches_4;
	// System.Boolean TouchKit::drawDebugBoundaryFrames
	bool ___drawDebugBoundaryFrames_5;
	// System.Boolean TouchKit::autoScaleRectsAndDistances
	bool ___autoScaleRectsAndDistances_6;
	// System.Boolean TouchKit::shouldAutoUpdateTouches
	bool ___shouldAutoUpdateTouches_7;
	// UnityEngine.Vector2 TouchKit::_designTimeResolution
	Vector2_t2156229523  ____designTimeResolution_8;
	// System.Int32 TouchKit::maxTouchesToProcess
	int32_t ___maxTouchesToProcess_9;
	// UnityEngine.Vector2 TouchKit::<runtimeScaleModifier>k__BackingField
	Vector2_t2156229523  ___U3CruntimeScaleModifierU3Ek__BackingField_10;
	// System.Single TouchKit::<runtimeDistanceModifier>k__BackingField
	float ___U3CruntimeDistanceModifierU3Ek__BackingField_11;
	// UnityEngine.Vector2 TouchKit::<pixelsToUnityUnitsMultiplier>k__BackingField
	Vector2_t2156229523  ___U3CpixelsToUnityUnitsMultiplierU3Ek__BackingField_12;
	// System.Collections.Generic.List`1<TKAbstractGestureRecognizer> TouchKit::_gestureRecognizers
	List_1_t3375847081 * ____gestureRecognizers_13;
	// TKTouch[] TouchKit::_touchCache
	TKTouchU5BU5D_t3441859050* ____touchCache_14;
	// System.Collections.Generic.List`1<TKTouch> TouchKit::_liveTouches
	List_1_t34712449 * ____liveTouches_15;
	// System.Boolean TouchKit::_shouldCheckForLostTouches
	bool ____shouldCheckForLostTouches_16;

public:
	inline static int32_t get_offset_of_simulateTouches_2() { return static_cast<int32_t>(offsetof(TouchKit_t3280788656, ___simulateTouches_2)); }
	inline bool get_simulateTouches_2() const { return ___simulateTouches_2; }
	inline bool* get_address_of_simulateTouches_2() { return &___simulateTouches_2; }
	inline void set_simulateTouches_2(bool value)
	{
		___simulateTouches_2 = value;
	}

	inline static int32_t get_offset_of_simulateMultitouch_3() { return static_cast<int32_t>(offsetof(TouchKit_t3280788656, ___simulateMultitouch_3)); }
	inline bool get_simulateMultitouch_3() const { return ___simulateMultitouch_3; }
	inline bool* get_address_of_simulateMultitouch_3() { return &___simulateMultitouch_3; }
	inline void set_simulateMultitouch_3(bool value)
	{
		___simulateMultitouch_3 = value;
	}

	inline static int32_t get_offset_of_drawTouches_4() { return static_cast<int32_t>(offsetof(TouchKit_t3280788656, ___drawTouches_4)); }
	inline bool get_drawTouches_4() const { return ___drawTouches_4; }
	inline bool* get_address_of_drawTouches_4() { return &___drawTouches_4; }
	inline void set_drawTouches_4(bool value)
	{
		___drawTouches_4 = value;
	}

	inline static int32_t get_offset_of_drawDebugBoundaryFrames_5() { return static_cast<int32_t>(offsetof(TouchKit_t3280788656, ___drawDebugBoundaryFrames_5)); }
	inline bool get_drawDebugBoundaryFrames_5() const { return ___drawDebugBoundaryFrames_5; }
	inline bool* get_address_of_drawDebugBoundaryFrames_5() { return &___drawDebugBoundaryFrames_5; }
	inline void set_drawDebugBoundaryFrames_5(bool value)
	{
		___drawDebugBoundaryFrames_5 = value;
	}

	inline static int32_t get_offset_of_autoScaleRectsAndDistances_6() { return static_cast<int32_t>(offsetof(TouchKit_t3280788656, ___autoScaleRectsAndDistances_6)); }
	inline bool get_autoScaleRectsAndDistances_6() const { return ___autoScaleRectsAndDistances_6; }
	inline bool* get_address_of_autoScaleRectsAndDistances_6() { return &___autoScaleRectsAndDistances_6; }
	inline void set_autoScaleRectsAndDistances_6(bool value)
	{
		___autoScaleRectsAndDistances_6 = value;
	}

	inline static int32_t get_offset_of_shouldAutoUpdateTouches_7() { return static_cast<int32_t>(offsetof(TouchKit_t3280788656, ___shouldAutoUpdateTouches_7)); }
	inline bool get_shouldAutoUpdateTouches_7() const { return ___shouldAutoUpdateTouches_7; }
	inline bool* get_address_of_shouldAutoUpdateTouches_7() { return &___shouldAutoUpdateTouches_7; }
	inline void set_shouldAutoUpdateTouches_7(bool value)
	{
		___shouldAutoUpdateTouches_7 = value;
	}

	inline static int32_t get_offset_of__designTimeResolution_8() { return static_cast<int32_t>(offsetof(TouchKit_t3280788656, ____designTimeResolution_8)); }
	inline Vector2_t2156229523  get__designTimeResolution_8() const { return ____designTimeResolution_8; }
	inline Vector2_t2156229523 * get_address_of__designTimeResolution_8() { return &____designTimeResolution_8; }
	inline void set__designTimeResolution_8(Vector2_t2156229523  value)
	{
		____designTimeResolution_8 = value;
	}

	inline static int32_t get_offset_of_maxTouchesToProcess_9() { return static_cast<int32_t>(offsetof(TouchKit_t3280788656, ___maxTouchesToProcess_9)); }
	inline int32_t get_maxTouchesToProcess_9() const { return ___maxTouchesToProcess_9; }
	inline int32_t* get_address_of_maxTouchesToProcess_9() { return &___maxTouchesToProcess_9; }
	inline void set_maxTouchesToProcess_9(int32_t value)
	{
		___maxTouchesToProcess_9 = value;
	}

	inline static int32_t get_offset_of_U3CruntimeScaleModifierU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(TouchKit_t3280788656, ___U3CruntimeScaleModifierU3Ek__BackingField_10)); }
	inline Vector2_t2156229523  get_U3CruntimeScaleModifierU3Ek__BackingField_10() const { return ___U3CruntimeScaleModifierU3Ek__BackingField_10; }
	inline Vector2_t2156229523 * get_address_of_U3CruntimeScaleModifierU3Ek__BackingField_10() { return &___U3CruntimeScaleModifierU3Ek__BackingField_10; }
	inline void set_U3CruntimeScaleModifierU3Ek__BackingField_10(Vector2_t2156229523  value)
	{
		___U3CruntimeScaleModifierU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CruntimeDistanceModifierU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(TouchKit_t3280788656, ___U3CruntimeDistanceModifierU3Ek__BackingField_11)); }
	inline float get_U3CruntimeDistanceModifierU3Ek__BackingField_11() const { return ___U3CruntimeDistanceModifierU3Ek__BackingField_11; }
	inline float* get_address_of_U3CruntimeDistanceModifierU3Ek__BackingField_11() { return &___U3CruntimeDistanceModifierU3Ek__BackingField_11; }
	inline void set_U3CruntimeDistanceModifierU3Ek__BackingField_11(float value)
	{
		___U3CruntimeDistanceModifierU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CpixelsToUnityUnitsMultiplierU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(TouchKit_t3280788656, ___U3CpixelsToUnityUnitsMultiplierU3Ek__BackingField_12)); }
	inline Vector2_t2156229523  get_U3CpixelsToUnityUnitsMultiplierU3Ek__BackingField_12() const { return ___U3CpixelsToUnityUnitsMultiplierU3Ek__BackingField_12; }
	inline Vector2_t2156229523 * get_address_of_U3CpixelsToUnityUnitsMultiplierU3Ek__BackingField_12() { return &___U3CpixelsToUnityUnitsMultiplierU3Ek__BackingField_12; }
	inline void set_U3CpixelsToUnityUnitsMultiplierU3Ek__BackingField_12(Vector2_t2156229523  value)
	{
		___U3CpixelsToUnityUnitsMultiplierU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of__gestureRecognizers_13() { return static_cast<int32_t>(offsetof(TouchKit_t3280788656, ____gestureRecognizers_13)); }
	inline List_1_t3375847081 * get__gestureRecognizers_13() const { return ____gestureRecognizers_13; }
	inline List_1_t3375847081 ** get_address_of__gestureRecognizers_13() { return &____gestureRecognizers_13; }
	inline void set__gestureRecognizers_13(List_1_t3375847081 * value)
	{
		____gestureRecognizers_13 = value;
		Il2CppCodeGenWriteBarrier((&____gestureRecognizers_13), value);
	}

	inline static int32_t get_offset_of__touchCache_14() { return static_cast<int32_t>(offsetof(TouchKit_t3280788656, ____touchCache_14)); }
	inline TKTouchU5BU5D_t3441859050* get__touchCache_14() const { return ____touchCache_14; }
	inline TKTouchU5BU5D_t3441859050** get_address_of__touchCache_14() { return &____touchCache_14; }
	inline void set__touchCache_14(TKTouchU5BU5D_t3441859050* value)
	{
		____touchCache_14 = value;
		Il2CppCodeGenWriteBarrier((&____touchCache_14), value);
	}

	inline static int32_t get_offset_of__liveTouches_15() { return static_cast<int32_t>(offsetof(TouchKit_t3280788656, ____liveTouches_15)); }
	inline List_1_t34712449 * get__liveTouches_15() const { return ____liveTouches_15; }
	inline List_1_t34712449 ** get_address_of__liveTouches_15() { return &____liveTouches_15; }
	inline void set__liveTouches_15(List_1_t34712449 * value)
	{
		____liveTouches_15 = value;
		Il2CppCodeGenWriteBarrier((&____liveTouches_15), value);
	}

	inline static int32_t get_offset_of__shouldCheckForLostTouches_16() { return static_cast<int32_t>(offsetof(TouchKit_t3280788656, ____shouldCheckForLostTouches_16)); }
	inline bool get__shouldCheckForLostTouches_16() const { return ____shouldCheckForLostTouches_16; }
	inline bool* get_address_of__shouldCheckForLostTouches_16() { return &____shouldCheckForLostTouches_16; }
	inline void set__shouldCheckForLostTouches_16(bool value)
	{
		____shouldCheckForLostTouches_16 = value;
	}
};

struct TouchKit_t3280788656_StaticFields
{
public:
	// TouchKit TouchKit::_instance
	TouchKit_t3280788656 * ____instance_18;

public:
	inline static int32_t get_offset_of__instance_18() { return static_cast<int32_t>(offsetof(TouchKit_t3280788656_StaticFields, ____instance_18)); }
	inline TouchKit_t3280788656 * get__instance_18() const { return ____instance_18; }
	inline TouchKit_t3280788656 ** get_address_of__instance_18() { return &____instance_18; }
	inline void set__instance_18(TouchKit_t3280788656 * value)
	{
		____instance_18 = value;
		Il2CppCodeGenWriteBarrier((&____instance_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHKIT_T3280788656_H
#ifndef TOUCHKITLITE_T1167724046_H
#define TOUCHKITLITE_T1167724046_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.TouchKitLite
struct  TouchKitLite_t1167724046  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Prime31.TouchKitLite::shouldProcessTouches
	bool ___shouldProcessTouches_2;
	// System.Collections.Generic.List`1<Prime31.TKLTouch> Prime31.TouchKitLite::liveTouches
	List_1_t661897764 * ___liveTouches_4;
	// Prime31.TKLTouch[] Prime31.TouchKitLite::_touchCache
	TKLTouchU5BU5D_t3381730651* ____touchCache_5;

public:
	inline static int32_t get_offset_of_shouldProcessTouches_2() { return static_cast<int32_t>(offsetof(TouchKitLite_t1167724046, ___shouldProcessTouches_2)); }
	inline bool get_shouldProcessTouches_2() const { return ___shouldProcessTouches_2; }
	inline bool* get_address_of_shouldProcessTouches_2() { return &___shouldProcessTouches_2; }
	inline void set_shouldProcessTouches_2(bool value)
	{
		___shouldProcessTouches_2 = value;
	}

	inline static int32_t get_offset_of_liveTouches_4() { return static_cast<int32_t>(offsetof(TouchKitLite_t1167724046, ___liveTouches_4)); }
	inline List_1_t661897764 * get_liveTouches_4() const { return ___liveTouches_4; }
	inline List_1_t661897764 ** get_address_of_liveTouches_4() { return &___liveTouches_4; }
	inline void set_liveTouches_4(List_1_t661897764 * value)
	{
		___liveTouches_4 = value;
		Il2CppCodeGenWriteBarrier((&___liveTouches_4), value);
	}

	inline static int32_t get_offset_of__touchCache_5() { return static_cast<int32_t>(offsetof(TouchKitLite_t1167724046, ____touchCache_5)); }
	inline TKLTouchU5BU5D_t3381730651* get__touchCache_5() const { return ____touchCache_5; }
	inline TKLTouchU5BU5D_t3381730651** get_address_of__touchCache_5() { return &____touchCache_5; }
	inline void set__touchCache_5(TKLTouchU5BU5D_t3381730651* value)
	{
		____touchCache_5 = value;
		Il2CppCodeGenWriteBarrier((&____touchCache_5), value);
	}
};

struct TouchKitLite_t1167724046_StaticFields
{
public:
	// Prime31.TouchKitLite Prime31.TouchKitLite::_instance
	TouchKitLite_t1167724046 * ____instance_7;

public:
	inline static int32_t get_offset_of__instance_7() { return static_cast<int32_t>(offsetof(TouchKitLite_t1167724046_StaticFields, ____instance_7)); }
	inline TouchKitLite_t1167724046 * get__instance_7() const { return ____instance_7; }
	inline TouchKitLite_t1167724046 ** get_address_of__instance_7() { return &____instance_7; }
	inline void set__instance_7(TouchKitLite_t1167724046 * value)
	{
		____instance_7 = value;
		Il2CppCodeGenWriteBarrier((&____instance_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHKITLITE_T1167724046_H
#ifndef TKLSWIPEDETECTOR_T1290972488_H
#define TKLSWIPEDETECTOR_T1290972488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.TKLSwipeDetector
struct  TKLSwipeDetector_t1290972488  : public MonoBehaviour_t3962482529
{
public:
	// System.Action`1<Prime31.SwipeDirection> Prime31.TKLSwipeDetector::onSwipeDeteced
	Action_1_t3637933324 * ___onSwipeDeteced_2;
	// System.Single Prime31.TKLSwipeDetector::timeToSwipe
	float ___timeToSwipe_3;
	// System.Single Prime31.TKLSwipeDetector::swipeVelocity
	float ___swipeVelocity_4;
	// Prime31.SwipeDirection Prime31.TKLSwipeDetector::completedSwipeDirection
	int32_t ___completedSwipeDirection_5;
	// System.Single Prime31.TKLSwipeDetector::_minimumDistance
	float ____minimumDistance_6;
	// System.Single Prime31.TKLSwipeDetector::_allowedVariance
	float ____allowedVariance_7;
	// Prime31.SwipeDirection Prime31.TKLSwipeDetector::_swipesToDetect
	int32_t ____swipesToDetect_8;
	// UnityEngine.Vector2 Prime31.TKLSwipeDetector::_startPoint
	Vector2_t2156229523  ____startPoint_9;
	// System.Single Prime31.TKLSwipeDetector::_startTime
	float ____startTime_10;
	// Prime31.SwipeDirection Prime31.TKLSwipeDetector::_swipeDetectionState
	int32_t ____swipeDetectionState_11;
	// System.Boolean Prime31.TKLSwipeDetector::_didCompleteDetection
	bool ____didCompleteDetection_12;

public:
	inline static int32_t get_offset_of_onSwipeDeteced_2() { return static_cast<int32_t>(offsetof(TKLSwipeDetector_t1290972488, ___onSwipeDeteced_2)); }
	inline Action_1_t3637933324 * get_onSwipeDeteced_2() const { return ___onSwipeDeteced_2; }
	inline Action_1_t3637933324 ** get_address_of_onSwipeDeteced_2() { return &___onSwipeDeteced_2; }
	inline void set_onSwipeDeteced_2(Action_1_t3637933324 * value)
	{
		___onSwipeDeteced_2 = value;
		Il2CppCodeGenWriteBarrier((&___onSwipeDeteced_2), value);
	}

	inline static int32_t get_offset_of_timeToSwipe_3() { return static_cast<int32_t>(offsetof(TKLSwipeDetector_t1290972488, ___timeToSwipe_3)); }
	inline float get_timeToSwipe_3() const { return ___timeToSwipe_3; }
	inline float* get_address_of_timeToSwipe_3() { return &___timeToSwipe_3; }
	inline void set_timeToSwipe_3(float value)
	{
		___timeToSwipe_3 = value;
	}

	inline static int32_t get_offset_of_swipeVelocity_4() { return static_cast<int32_t>(offsetof(TKLSwipeDetector_t1290972488, ___swipeVelocity_4)); }
	inline float get_swipeVelocity_4() const { return ___swipeVelocity_4; }
	inline float* get_address_of_swipeVelocity_4() { return &___swipeVelocity_4; }
	inline void set_swipeVelocity_4(float value)
	{
		___swipeVelocity_4 = value;
	}

	inline static int32_t get_offset_of_completedSwipeDirection_5() { return static_cast<int32_t>(offsetof(TKLSwipeDetector_t1290972488, ___completedSwipeDirection_5)); }
	inline int32_t get_completedSwipeDirection_5() const { return ___completedSwipeDirection_5; }
	inline int32_t* get_address_of_completedSwipeDirection_5() { return &___completedSwipeDirection_5; }
	inline void set_completedSwipeDirection_5(int32_t value)
	{
		___completedSwipeDirection_5 = value;
	}

	inline static int32_t get_offset_of__minimumDistance_6() { return static_cast<int32_t>(offsetof(TKLSwipeDetector_t1290972488, ____minimumDistance_6)); }
	inline float get__minimumDistance_6() const { return ____minimumDistance_6; }
	inline float* get_address_of__minimumDistance_6() { return &____minimumDistance_6; }
	inline void set__minimumDistance_6(float value)
	{
		____minimumDistance_6 = value;
	}

	inline static int32_t get_offset_of__allowedVariance_7() { return static_cast<int32_t>(offsetof(TKLSwipeDetector_t1290972488, ____allowedVariance_7)); }
	inline float get__allowedVariance_7() const { return ____allowedVariance_7; }
	inline float* get_address_of__allowedVariance_7() { return &____allowedVariance_7; }
	inline void set__allowedVariance_7(float value)
	{
		____allowedVariance_7 = value;
	}

	inline static int32_t get_offset_of__swipesToDetect_8() { return static_cast<int32_t>(offsetof(TKLSwipeDetector_t1290972488, ____swipesToDetect_8)); }
	inline int32_t get__swipesToDetect_8() const { return ____swipesToDetect_8; }
	inline int32_t* get_address_of__swipesToDetect_8() { return &____swipesToDetect_8; }
	inline void set__swipesToDetect_8(int32_t value)
	{
		____swipesToDetect_8 = value;
	}

	inline static int32_t get_offset_of__startPoint_9() { return static_cast<int32_t>(offsetof(TKLSwipeDetector_t1290972488, ____startPoint_9)); }
	inline Vector2_t2156229523  get__startPoint_9() const { return ____startPoint_9; }
	inline Vector2_t2156229523 * get_address_of__startPoint_9() { return &____startPoint_9; }
	inline void set__startPoint_9(Vector2_t2156229523  value)
	{
		____startPoint_9 = value;
	}

	inline static int32_t get_offset_of__startTime_10() { return static_cast<int32_t>(offsetof(TKLSwipeDetector_t1290972488, ____startTime_10)); }
	inline float get__startTime_10() const { return ____startTime_10; }
	inline float* get_address_of__startTime_10() { return &____startTime_10; }
	inline void set__startTime_10(float value)
	{
		____startTime_10 = value;
	}

	inline static int32_t get_offset_of__swipeDetectionState_11() { return static_cast<int32_t>(offsetof(TKLSwipeDetector_t1290972488, ____swipeDetectionState_11)); }
	inline int32_t get__swipeDetectionState_11() const { return ____swipeDetectionState_11; }
	inline int32_t* get_address_of__swipeDetectionState_11() { return &____swipeDetectionState_11; }
	inline void set__swipeDetectionState_11(int32_t value)
	{
		____swipeDetectionState_11 = value;
	}

	inline static int32_t get_offset_of__didCompleteDetection_12() { return static_cast<int32_t>(offsetof(TKLSwipeDetector_t1290972488, ____didCompleteDetection_12)); }
	inline bool get__didCompleteDetection_12() const { return ____didCompleteDetection_12; }
	inline bool* get_address_of__didCompleteDetection_12() { return &____didCompleteDetection_12; }
	inline void set__didCompleteDetection_12(bool value)
	{
		____didCompleteDetection_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TKLSWIPEDETECTOR_T1290972488_H
#ifndef DEMOTWO_T1529025922_H
#define DEMOTWO_T1529025922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoTwo
struct  DemoTwo_t1529025922  : public MonoBehaviour_t3962482529
{
public:
	// VirtualControls DemoTwo::_controls
	VirtualControls_t3611233992 * ____controls_2;

public:
	inline static int32_t get_offset_of__controls_2() { return static_cast<int32_t>(offsetof(DemoTwo_t1529025922, ____controls_2)); }
	inline VirtualControls_t3611233992 * get__controls_2() const { return ____controls_2; }
	inline VirtualControls_t3611233992 ** get_address_of__controls_2() { return &____controls_2; }
	inline void set__controls_2(VirtualControls_t3611233992 * value)
	{
		____controls_2 = value;
		Il2CppCodeGenWriteBarrier((&____controls_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOTWO_T1529025922_H
#ifndef DEMOONE_T3048645529_H
#define DEMOONE_T3048645529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoOne
struct  DemoOne_t3048645529  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform DemoOne::cube
	Transform_t3600365921 * ___cube_2;
	// UnityEngine.Vector2 DemoOne::_scrollPosition
	Vector2_t2156229523  ____scrollPosition_3;
	// UnityEngine.AnimationCurve DemoOne::touchPadInputCurve
	AnimationCurve_t3046754366 * ___touchPadInputCurve_4;

public:
	inline static int32_t get_offset_of_cube_2() { return static_cast<int32_t>(offsetof(DemoOne_t3048645529, ___cube_2)); }
	inline Transform_t3600365921 * get_cube_2() const { return ___cube_2; }
	inline Transform_t3600365921 ** get_address_of_cube_2() { return &___cube_2; }
	inline void set_cube_2(Transform_t3600365921 * value)
	{
		___cube_2 = value;
		Il2CppCodeGenWriteBarrier((&___cube_2), value);
	}

	inline static int32_t get_offset_of__scrollPosition_3() { return static_cast<int32_t>(offsetof(DemoOne_t3048645529, ____scrollPosition_3)); }
	inline Vector2_t2156229523  get__scrollPosition_3() const { return ____scrollPosition_3; }
	inline Vector2_t2156229523 * get_address_of__scrollPosition_3() { return &____scrollPosition_3; }
	inline void set__scrollPosition_3(Vector2_t2156229523  value)
	{
		____scrollPosition_3 = value;
	}

	inline static int32_t get_offset_of_touchPadInputCurve_4() { return static_cast<int32_t>(offsetof(DemoOne_t3048645529, ___touchPadInputCurve_4)); }
	inline AnimationCurve_t3046754366 * get_touchPadInputCurve_4() const { return ___touchPadInputCurve_4; }
	inline AnimationCurve_t3046754366 ** get_address_of_touchPadInputCurve_4() { return &___touchPadInputCurve_4; }
	inline void set_touchPadInputCurve_4(AnimationCurve_t3046754366 * value)
	{
		___touchPadInputCurve_4 = value;
		Il2CppCodeGenWriteBarrier((&___touchPadInputCurve_4), value);
	}
};

struct DemoOne_t3048645529_StaticFields
{
public:
	// System.Action`1<TKTapRecognizer> DemoOne::<>f__am$cache0
	Action_1_t2562972046 * ___U3CU3Ef__amU24cache0_5;
	// System.Action`1<TKLongPressRecognizer> DemoOne::<>f__am$cache1
	Action_1_t1648329908 * ___U3CU3Ef__amU24cache1_6;
	// System.Action`1<TKLongPressRecognizer> DemoOne::<>f__am$cache2
	Action_1_t1648329908 * ___U3CU3Ef__amU24cache2_7;
	// System.Action`1<TKPanRecognizer> DemoOne::<>f__am$cache3
	Action_1_t1776408048 * ___U3CU3Ef__amU24cache3_8;
	// System.Action`1<TKTouchPadRecognizer> DemoOne::<>f__am$cache4
	Action_1_t1666246729 * ___U3CU3Ef__amU24cache4_9;
	// System.Action`1<TKTouchPadRecognizer> DemoOne::<>f__am$cache5
	Action_1_t1666246729 * ___U3CU3Ef__amU24cache5_10;
	// System.Action`1<TKSwipeRecognizer> DemoOne::<>f__am$cache6
	Action_1_t3383006765 * ___U3CU3Ef__amU24cache6_11;
	// System.Action`1<TKAngleSwipeRecognizer> DemoOne::<>f__am$cache7
	Action_1_t3839277408 * ___U3CU3Ef__amU24cache7_12;
	// System.Action`1<TKAngleSwipeRecognizer> DemoOne::<>f__am$cache8
	Action_1_t3839277408 * ___U3CU3Ef__amU24cache8_13;
	// System.Action`1<TKButtonRecognizer> DemoOne::<>f__am$cache9
	Action_1_t863969551 * ___U3CU3Ef__amU24cache9_14;
	// System.Action`1<TKButtonRecognizer> DemoOne::<>f__am$cacheA
	Action_1_t863969551 * ___U3CU3Ef__amU24cacheA_15;
	// System.Action`1<TKButtonRecognizer> DemoOne::<>f__am$cacheB
	Action_1_t863969551 * ___U3CU3Ef__amU24cacheB_16;
	// System.Action`1<TKAnyTouchRecognizer> DemoOne::<>f__am$cacheC
	Action_1_t3824429044 * ___U3CU3Ef__amU24cacheC_17;
	// System.Action`1<TKAnyTouchRecognizer> DemoOne::<>f__am$cacheD
	Action_1_t3824429044 * ___U3CU3Ef__amU24cacheD_18;
	// System.Action`1<TKCurveRecognizer> DemoOne::<>f__am$cacheE
	Action_1_t2054284907 * ___U3CU3Ef__amU24cacheE_19;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(DemoOne_t3048645529_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Action_1_t2562972046 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Action_1_t2562972046 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Action_1_t2562972046 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_6() { return static_cast<int32_t>(offsetof(DemoOne_t3048645529_StaticFields, ___U3CU3Ef__amU24cache1_6)); }
	inline Action_1_t1648329908 * get_U3CU3Ef__amU24cache1_6() const { return ___U3CU3Ef__amU24cache1_6; }
	inline Action_1_t1648329908 ** get_address_of_U3CU3Ef__amU24cache1_6() { return &___U3CU3Ef__amU24cache1_6; }
	inline void set_U3CU3Ef__amU24cache1_6(Action_1_t1648329908 * value)
	{
		___U3CU3Ef__amU24cache1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_7() { return static_cast<int32_t>(offsetof(DemoOne_t3048645529_StaticFields, ___U3CU3Ef__amU24cache2_7)); }
	inline Action_1_t1648329908 * get_U3CU3Ef__amU24cache2_7() const { return ___U3CU3Ef__amU24cache2_7; }
	inline Action_1_t1648329908 ** get_address_of_U3CU3Ef__amU24cache2_7() { return &___U3CU3Ef__amU24cache2_7; }
	inline void set_U3CU3Ef__amU24cache2_7(Action_1_t1648329908 * value)
	{
		___U3CU3Ef__amU24cache2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_8() { return static_cast<int32_t>(offsetof(DemoOne_t3048645529_StaticFields, ___U3CU3Ef__amU24cache3_8)); }
	inline Action_1_t1776408048 * get_U3CU3Ef__amU24cache3_8() const { return ___U3CU3Ef__amU24cache3_8; }
	inline Action_1_t1776408048 ** get_address_of_U3CU3Ef__amU24cache3_8() { return &___U3CU3Ef__amU24cache3_8; }
	inline void set_U3CU3Ef__amU24cache3_8(Action_1_t1776408048 * value)
	{
		___U3CU3Ef__amU24cache3_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_9() { return static_cast<int32_t>(offsetof(DemoOne_t3048645529_StaticFields, ___U3CU3Ef__amU24cache4_9)); }
	inline Action_1_t1666246729 * get_U3CU3Ef__amU24cache4_9() const { return ___U3CU3Ef__amU24cache4_9; }
	inline Action_1_t1666246729 ** get_address_of_U3CU3Ef__amU24cache4_9() { return &___U3CU3Ef__amU24cache4_9; }
	inline void set_U3CU3Ef__amU24cache4_9(Action_1_t1666246729 * value)
	{
		___U3CU3Ef__amU24cache4_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_10() { return static_cast<int32_t>(offsetof(DemoOne_t3048645529_StaticFields, ___U3CU3Ef__amU24cache5_10)); }
	inline Action_1_t1666246729 * get_U3CU3Ef__amU24cache5_10() const { return ___U3CU3Ef__amU24cache5_10; }
	inline Action_1_t1666246729 ** get_address_of_U3CU3Ef__amU24cache5_10() { return &___U3CU3Ef__amU24cache5_10; }
	inline void set_U3CU3Ef__amU24cache5_10(Action_1_t1666246729 * value)
	{
		___U3CU3Ef__amU24cache5_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache5_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_11() { return static_cast<int32_t>(offsetof(DemoOne_t3048645529_StaticFields, ___U3CU3Ef__amU24cache6_11)); }
	inline Action_1_t3383006765 * get_U3CU3Ef__amU24cache6_11() const { return ___U3CU3Ef__amU24cache6_11; }
	inline Action_1_t3383006765 ** get_address_of_U3CU3Ef__amU24cache6_11() { return &___U3CU3Ef__amU24cache6_11; }
	inline void set_U3CU3Ef__amU24cache6_11(Action_1_t3383006765 * value)
	{
		___U3CU3Ef__amU24cache6_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache6_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_12() { return static_cast<int32_t>(offsetof(DemoOne_t3048645529_StaticFields, ___U3CU3Ef__amU24cache7_12)); }
	inline Action_1_t3839277408 * get_U3CU3Ef__amU24cache7_12() const { return ___U3CU3Ef__amU24cache7_12; }
	inline Action_1_t3839277408 ** get_address_of_U3CU3Ef__amU24cache7_12() { return &___U3CU3Ef__amU24cache7_12; }
	inline void set_U3CU3Ef__amU24cache7_12(Action_1_t3839277408 * value)
	{
		___U3CU3Ef__amU24cache7_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache7_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_13() { return static_cast<int32_t>(offsetof(DemoOne_t3048645529_StaticFields, ___U3CU3Ef__amU24cache8_13)); }
	inline Action_1_t3839277408 * get_U3CU3Ef__amU24cache8_13() const { return ___U3CU3Ef__amU24cache8_13; }
	inline Action_1_t3839277408 ** get_address_of_U3CU3Ef__amU24cache8_13() { return &___U3CU3Ef__amU24cache8_13; }
	inline void set_U3CU3Ef__amU24cache8_13(Action_1_t3839277408 * value)
	{
		___U3CU3Ef__amU24cache8_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache8_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_14() { return static_cast<int32_t>(offsetof(DemoOne_t3048645529_StaticFields, ___U3CU3Ef__amU24cache9_14)); }
	inline Action_1_t863969551 * get_U3CU3Ef__amU24cache9_14() const { return ___U3CU3Ef__amU24cache9_14; }
	inline Action_1_t863969551 ** get_address_of_U3CU3Ef__amU24cache9_14() { return &___U3CU3Ef__amU24cache9_14; }
	inline void set_U3CU3Ef__amU24cache9_14(Action_1_t863969551 * value)
	{
		___U3CU3Ef__amU24cache9_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache9_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_15() { return static_cast<int32_t>(offsetof(DemoOne_t3048645529_StaticFields, ___U3CU3Ef__amU24cacheA_15)); }
	inline Action_1_t863969551 * get_U3CU3Ef__amU24cacheA_15() const { return ___U3CU3Ef__amU24cacheA_15; }
	inline Action_1_t863969551 ** get_address_of_U3CU3Ef__amU24cacheA_15() { return &___U3CU3Ef__amU24cacheA_15; }
	inline void set_U3CU3Ef__amU24cacheA_15(Action_1_t863969551 * value)
	{
		___U3CU3Ef__amU24cacheA_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheA_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_16() { return static_cast<int32_t>(offsetof(DemoOne_t3048645529_StaticFields, ___U3CU3Ef__amU24cacheB_16)); }
	inline Action_1_t863969551 * get_U3CU3Ef__amU24cacheB_16() const { return ___U3CU3Ef__amU24cacheB_16; }
	inline Action_1_t863969551 ** get_address_of_U3CU3Ef__amU24cacheB_16() { return &___U3CU3Ef__amU24cacheB_16; }
	inline void set_U3CU3Ef__amU24cacheB_16(Action_1_t863969551 * value)
	{
		___U3CU3Ef__amU24cacheB_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_16), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_17() { return static_cast<int32_t>(offsetof(DemoOne_t3048645529_StaticFields, ___U3CU3Ef__amU24cacheC_17)); }
	inline Action_1_t3824429044 * get_U3CU3Ef__amU24cacheC_17() const { return ___U3CU3Ef__amU24cacheC_17; }
	inline Action_1_t3824429044 ** get_address_of_U3CU3Ef__amU24cacheC_17() { return &___U3CU3Ef__amU24cacheC_17; }
	inline void set_U3CU3Ef__amU24cacheC_17(Action_1_t3824429044 * value)
	{
		___U3CU3Ef__amU24cacheC_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheC_17), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheD_18() { return static_cast<int32_t>(offsetof(DemoOne_t3048645529_StaticFields, ___U3CU3Ef__amU24cacheD_18)); }
	inline Action_1_t3824429044 * get_U3CU3Ef__amU24cacheD_18() const { return ___U3CU3Ef__amU24cacheD_18; }
	inline Action_1_t3824429044 ** get_address_of_U3CU3Ef__amU24cacheD_18() { return &___U3CU3Ef__amU24cacheD_18; }
	inline void set_U3CU3Ef__amU24cacheD_18(Action_1_t3824429044 * value)
	{
		___U3CU3Ef__amU24cacheD_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheD_18), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheE_19() { return static_cast<int32_t>(offsetof(DemoOne_t3048645529_StaticFields, ___U3CU3Ef__amU24cacheE_19)); }
	inline Action_1_t2054284907 * get_U3CU3Ef__amU24cacheE_19() const { return ___U3CU3Ef__amU24cacheE_19; }
	inline Action_1_t2054284907 ** get_address_of_U3CU3Ef__amU24cacheE_19() { return &___U3CU3Ef__amU24cacheE_19; }
	inline void set_U3CU3Ef__amU24cacheE_19(Action_1_t2054284907 * value)
	{
		___U3CU3Ef__amU24cacheE_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheE_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOONE_T3048645529_H
#ifndef UNITYARFACEMESHMANAGER_T3766034196_H
#define UNITYARFACEMESHMANAGER_T3766034196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityARFaceMeshManager
struct  UnityARFaceMeshManager_t3766034196  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.MeshFilter UnityARFaceMeshManager::meshFilter
	MeshFilter_t3523625662 * ___meshFilter_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityARFaceMeshManager::m_session
	UnityARSessionNativeInterface_t3929719369 * ___m_session_3;
	// UnityEngine.Mesh UnityARFaceMeshManager::faceMesh
	Mesh_t3648964284 * ___faceMesh_4;

public:
	inline static int32_t get_offset_of_meshFilter_2() { return static_cast<int32_t>(offsetof(UnityARFaceMeshManager_t3766034196, ___meshFilter_2)); }
	inline MeshFilter_t3523625662 * get_meshFilter_2() const { return ___meshFilter_2; }
	inline MeshFilter_t3523625662 ** get_address_of_meshFilter_2() { return &___meshFilter_2; }
	inline void set_meshFilter_2(MeshFilter_t3523625662 * value)
	{
		___meshFilter_2 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_2), value);
	}

	inline static int32_t get_offset_of_m_session_3() { return static_cast<int32_t>(offsetof(UnityARFaceMeshManager_t3766034196, ___m_session_3)); }
	inline UnityARSessionNativeInterface_t3929719369 * get_m_session_3() const { return ___m_session_3; }
	inline UnityARSessionNativeInterface_t3929719369 ** get_address_of_m_session_3() { return &___m_session_3; }
	inline void set_m_session_3(UnityARSessionNativeInterface_t3929719369 * value)
	{
		___m_session_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_session_3), value);
	}

	inline static int32_t get_offset_of_faceMesh_4() { return static_cast<int32_t>(offsetof(UnityARFaceMeshManager_t3766034196, ___faceMesh_4)); }
	inline Mesh_t3648964284 * get_faceMesh_4() const { return ___faceMesh_4; }
	inline Mesh_t3648964284 ** get_address_of_faceMesh_4() { return &___faceMesh_4; }
	inline void set_faceMesh_4(Mesh_t3648964284 * value)
	{
		___faceMesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___faceMesh_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARFACEMESHMANAGER_T3766034196_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef CONNECTTOEDITOR_T595742893_H
#define CONNECTTOEDITOR_T595742893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ConnectToEditor
struct  ConnectToEditor_t595742893  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.XR.iOS.ConnectToEditor::playerConnection
	PlayerConnection_t3081694049 * ___playerConnection_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.ConnectToEditor::m_session
	UnityARSessionNativeInterface_t3929719369 * ___m_session_3;
	// System.Int32 UnityEngine.XR.iOS.ConnectToEditor::editorID
	int32_t ___editorID_4;
	// UnityEngine.Texture2D UnityEngine.XR.iOS.ConnectToEditor::frameBufferTex
	Texture2D_t3840446185 * ___frameBufferTex_5;

public:
	inline static int32_t get_offset_of_playerConnection_2() { return static_cast<int32_t>(offsetof(ConnectToEditor_t595742893, ___playerConnection_2)); }
	inline PlayerConnection_t3081694049 * get_playerConnection_2() const { return ___playerConnection_2; }
	inline PlayerConnection_t3081694049 ** get_address_of_playerConnection_2() { return &___playerConnection_2; }
	inline void set_playerConnection_2(PlayerConnection_t3081694049 * value)
	{
		___playerConnection_2 = value;
		Il2CppCodeGenWriteBarrier((&___playerConnection_2), value);
	}

	inline static int32_t get_offset_of_m_session_3() { return static_cast<int32_t>(offsetof(ConnectToEditor_t595742893, ___m_session_3)); }
	inline UnityARSessionNativeInterface_t3929719369 * get_m_session_3() const { return ___m_session_3; }
	inline UnityARSessionNativeInterface_t3929719369 ** get_address_of_m_session_3() { return &___m_session_3; }
	inline void set_m_session_3(UnityARSessionNativeInterface_t3929719369 * value)
	{
		___m_session_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_session_3), value);
	}

	inline static int32_t get_offset_of_editorID_4() { return static_cast<int32_t>(offsetof(ConnectToEditor_t595742893, ___editorID_4)); }
	inline int32_t get_editorID_4() const { return ___editorID_4; }
	inline int32_t* get_address_of_editorID_4() { return &___editorID_4; }
	inline void set_editorID_4(int32_t value)
	{
		___editorID_4 = value;
	}

	inline static int32_t get_offset_of_frameBufferTex_5() { return static_cast<int32_t>(offsetof(ConnectToEditor_t595742893, ___frameBufferTex_5)); }
	inline Texture2D_t3840446185 * get_frameBufferTex_5() const { return ___frameBufferTex_5; }
	inline Texture2D_t3840446185 ** get_address_of_frameBufferTex_5() { return &___frameBufferTex_5; }
	inline void set_frameBufferTex_5(Texture2D_t3840446185 * value)
	{
		___frameBufferTex_5 = value;
		Il2CppCodeGenWriteBarrier((&___frameBufferTex_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTTOEDITOR_T595742893_H
#ifndef FOCUSSQUARE_T2880014214_H
#define FOCUSSQUARE_T2880014214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FocusSquare
struct  FocusSquare_t2880014214  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject FocusSquare::findingSquare
	GameObject_t1113636619 * ___findingSquare_2;
	// UnityEngine.GameObject FocusSquare::foundSquare
	GameObject_t1113636619 * ___foundSquare_3;
	// System.Single FocusSquare::maxRayDistance
	float ___maxRayDistance_4;
	// UnityEngine.LayerMask FocusSquare::collisionLayerMask
	LayerMask_t3493934918  ___collisionLayerMask_5;
	// System.Single FocusSquare::findingSquareDist
	float ___findingSquareDist_6;
	// FocusSquare/FocusState FocusSquare::squareState
	int32_t ___squareState_7;
	// System.Boolean FocusSquare::trackingInitialized
	bool ___trackingInitialized_8;

public:
	inline static int32_t get_offset_of_findingSquare_2() { return static_cast<int32_t>(offsetof(FocusSquare_t2880014214, ___findingSquare_2)); }
	inline GameObject_t1113636619 * get_findingSquare_2() const { return ___findingSquare_2; }
	inline GameObject_t1113636619 ** get_address_of_findingSquare_2() { return &___findingSquare_2; }
	inline void set_findingSquare_2(GameObject_t1113636619 * value)
	{
		___findingSquare_2 = value;
		Il2CppCodeGenWriteBarrier((&___findingSquare_2), value);
	}

	inline static int32_t get_offset_of_foundSquare_3() { return static_cast<int32_t>(offsetof(FocusSquare_t2880014214, ___foundSquare_3)); }
	inline GameObject_t1113636619 * get_foundSquare_3() const { return ___foundSquare_3; }
	inline GameObject_t1113636619 ** get_address_of_foundSquare_3() { return &___foundSquare_3; }
	inline void set_foundSquare_3(GameObject_t1113636619 * value)
	{
		___foundSquare_3 = value;
		Il2CppCodeGenWriteBarrier((&___foundSquare_3), value);
	}

	inline static int32_t get_offset_of_maxRayDistance_4() { return static_cast<int32_t>(offsetof(FocusSquare_t2880014214, ___maxRayDistance_4)); }
	inline float get_maxRayDistance_4() const { return ___maxRayDistance_4; }
	inline float* get_address_of_maxRayDistance_4() { return &___maxRayDistance_4; }
	inline void set_maxRayDistance_4(float value)
	{
		___maxRayDistance_4 = value;
	}

	inline static int32_t get_offset_of_collisionLayerMask_5() { return static_cast<int32_t>(offsetof(FocusSquare_t2880014214, ___collisionLayerMask_5)); }
	inline LayerMask_t3493934918  get_collisionLayerMask_5() const { return ___collisionLayerMask_5; }
	inline LayerMask_t3493934918 * get_address_of_collisionLayerMask_5() { return &___collisionLayerMask_5; }
	inline void set_collisionLayerMask_5(LayerMask_t3493934918  value)
	{
		___collisionLayerMask_5 = value;
	}

	inline static int32_t get_offset_of_findingSquareDist_6() { return static_cast<int32_t>(offsetof(FocusSquare_t2880014214, ___findingSquareDist_6)); }
	inline float get_findingSquareDist_6() const { return ___findingSquareDist_6; }
	inline float* get_address_of_findingSquareDist_6() { return &___findingSquareDist_6; }
	inline void set_findingSquareDist_6(float value)
	{
		___findingSquareDist_6 = value;
	}

	inline static int32_t get_offset_of_squareState_7() { return static_cast<int32_t>(offsetof(FocusSquare_t2880014214, ___squareState_7)); }
	inline int32_t get_squareState_7() const { return ___squareState_7; }
	inline int32_t* get_address_of_squareState_7() { return &___squareState_7; }
	inline void set_squareState_7(int32_t value)
	{
		___squareState_7 = value;
	}

	inline static int32_t get_offset_of_trackingInitialized_8() { return static_cast<int32_t>(offsetof(FocusSquare_t2880014214, ___trackingInitialized_8)); }
	inline bool get_trackingInitialized_8() const { return ___trackingInitialized_8; }
	inline bool* get_address_of_trackingInitialized_8() { return &___trackingInitialized_8; }
	inline void set_trackingInitialized_8(bool value)
	{
		___trackingInitialized_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOCUSSQUARE_T2880014214_H
#ifndef BALLMAKER_T4057675501_H
#define BALLMAKER_T4057675501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallMaker
struct  BallMaker_t4057675501  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject BallMaker::ballPrefab
	GameObject_t1113636619 * ___ballPrefab_2;
	// System.Single BallMaker::createHeight
	float ___createHeight_3;
	// UnityEngine.MaterialPropertyBlock BallMaker::props
	MaterialPropertyBlock_t3213117958 * ___props_4;

public:
	inline static int32_t get_offset_of_ballPrefab_2() { return static_cast<int32_t>(offsetof(BallMaker_t4057675501, ___ballPrefab_2)); }
	inline GameObject_t1113636619 * get_ballPrefab_2() const { return ___ballPrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_ballPrefab_2() { return &___ballPrefab_2; }
	inline void set_ballPrefab_2(GameObject_t1113636619 * value)
	{
		___ballPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___ballPrefab_2), value);
	}

	inline static int32_t get_offset_of_createHeight_3() { return static_cast<int32_t>(offsetof(BallMaker_t4057675501, ___createHeight_3)); }
	inline float get_createHeight_3() const { return ___createHeight_3; }
	inline float* get_address_of_createHeight_3() { return &___createHeight_3; }
	inline void set_createHeight_3(float value)
	{
		___createHeight_3 = value;
	}

	inline static int32_t get_offset_of_props_4() { return static_cast<int32_t>(offsetof(BallMaker_t4057675501, ___props_4)); }
	inline MaterialPropertyBlock_t3213117958 * get_props_4() const { return ___props_4; }
	inline MaterialPropertyBlock_t3213117958 ** get_address_of_props_4() { return &___props_4; }
	inline void set_props_4(MaterialPropertyBlock_t3213117958 * value)
	{
		___props_4 = value;
		Il2CppCodeGenWriteBarrier((&___props_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLMAKER_T4057675501_H
#ifndef SVBOXSLIDER_T4192470748_H
#define SVBOXSLIDER_T4192470748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SVBoxSlider
struct  SVBoxSlider_t4192470748  : public MonoBehaviour_t3962482529
{
public:
	// ColorPicker SVBoxSlider::picker
	ColorPicker_t228004619 * ___picker_2;
	// UnityEngine.UI.BoxSlider SVBoxSlider::slider
	BoxSlider_t2380464200 * ___slider_3;
	// UnityEngine.UI.RawImage SVBoxSlider::image
	RawImage_t3182918964 * ___image_4;
	// System.Single SVBoxSlider::lastH
	float ___lastH_5;
	// System.Boolean SVBoxSlider::listen
	bool ___listen_6;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(SVBoxSlider_t4192470748, ___picker_2)); }
	inline ColorPicker_t228004619 * get_picker_2() const { return ___picker_2; }
	inline ColorPicker_t228004619 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPicker_t228004619 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_slider_3() { return static_cast<int32_t>(offsetof(SVBoxSlider_t4192470748, ___slider_3)); }
	inline BoxSlider_t2380464200 * get_slider_3() const { return ___slider_3; }
	inline BoxSlider_t2380464200 ** get_address_of_slider_3() { return &___slider_3; }
	inline void set_slider_3(BoxSlider_t2380464200 * value)
	{
		___slider_3 = value;
		Il2CppCodeGenWriteBarrier((&___slider_3), value);
	}

	inline static int32_t get_offset_of_image_4() { return static_cast<int32_t>(offsetof(SVBoxSlider_t4192470748, ___image_4)); }
	inline RawImage_t3182918964 * get_image_4() const { return ___image_4; }
	inline RawImage_t3182918964 ** get_address_of_image_4() { return &___image_4; }
	inline void set_image_4(RawImage_t3182918964 * value)
	{
		___image_4 = value;
		Il2CppCodeGenWriteBarrier((&___image_4), value);
	}

	inline static int32_t get_offset_of_lastH_5() { return static_cast<int32_t>(offsetof(SVBoxSlider_t4192470748, ___lastH_5)); }
	inline float get_lastH_5() const { return ___lastH_5; }
	inline float* get_address_of_lastH_5() { return &___lastH_5; }
	inline void set_lastH_5(float value)
	{
		___lastH_5 = value;
	}

	inline static int32_t get_offset_of_listen_6() { return static_cast<int32_t>(offsetof(SVBoxSlider_t4192470748, ___listen_6)); }
	inline bool get_listen_6() const { return ___listen_6; }
	inline bool* get_address_of_listen_6() { return &___listen_6; }
	inline void set_listen_6(bool value)
	{
		___listen_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVBOXSLIDER_T4192470748_H
#ifndef UNITYARUSERANCHORCOMPONENT_T969893952_H
#define UNITYARUSERANCHORCOMPONENT_T969893952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARUserAnchorComponent
struct  UnityARUserAnchorComponent_t969893952  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityEngine.XR.iOS.UnityARUserAnchorComponent::m_AnchorId
	String_t* ___m_AnchorId_2;

public:
	inline static int32_t get_offset_of_m_AnchorId_2() { return static_cast<int32_t>(offsetof(UnityARUserAnchorComponent_t969893952, ___m_AnchorId_2)); }
	inline String_t* get_m_AnchorId_2() const { return ___m_AnchorId_2; }
	inline String_t** get_address_of_m_AnchorId_2() { return &___m_AnchorId_2; }
	inline void set_m_AnchorId_2(String_t* value)
	{
		___m_AnchorId_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnchorId_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARUSERANCHORCOMPONENT_T969893952_H
#ifndef UNITYARKITLIGHTMANAGER_T380315540_H
#define UNITYARKITLIGHTMANAGER_T380315540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityARKitLightManager
struct  UnityARKitLightManager_t380315540  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Light[] UnityARKitLightManager::lightsInScene
	LightU5BU5D_t3678959411* ___lightsInScene_2;
	// UnityEngine.Rendering.SphericalHarmonicsL2 UnityARKitLightManager::shl
	SphericalHarmonicsL2_t3220866195  ___shl_3;

public:
	inline static int32_t get_offset_of_lightsInScene_2() { return static_cast<int32_t>(offsetof(UnityARKitLightManager_t380315540, ___lightsInScene_2)); }
	inline LightU5BU5D_t3678959411* get_lightsInScene_2() const { return ___lightsInScene_2; }
	inline LightU5BU5D_t3678959411** get_address_of_lightsInScene_2() { return &___lightsInScene_2; }
	inline void set_lightsInScene_2(LightU5BU5D_t3678959411* value)
	{
		___lightsInScene_2 = value;
		Il2CppCodeGenWriteBarrier((&___lightsInScene_2), value);
	}

	inline static int32_t get_offset_of_shl_3() { return static_cast<int32_t>(offsetof(UnityARKitLightManager_t380315540, ___shl_3)); }
	inline SphericalHarmonicsL2_t3220866195  get_shl_3() const { return ___shl_3; }
	inline SphericalHarmonicsL2_t3220866195 * get_address_of_shl_3() { return &___shl_3; }
	inline void set_shl_3(SphericalHarmonicsL2_t3220866195  value)
	{
		___shl_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARKITLIGHTMANAGER_T380315540_H
#ifndef UNITYARKITCONTROL_T1358211756_H
#define UNITYARKITCONTROL_T1358211756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARKitControl
struct  UnityARKitControl_t1358211756  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.XR.iOS.UnityARSessionRunOption[] UnityEngine.XR.iOS.UnityARKitControl::runOptions
	UnityARSessionRunOptionU5BU5D_t4225291891* ___runOptions_2;
	// UnityEngine.XR.iOS.UnityARAlignment[] UnityEngine.XR.iOS.UnityARKitControl::alignmentOptions
	UnityARAlignmentU5BU5D_t3682394155* ___alignmentOptions_3;
	// UnityEngine.XR.iOS.UnityARPlaneDetection[] UnityEngine.XR.iOS.UnityARKitControl::planeOptions
	UnityARPlaneDetectionU5BU5D_t3458580926* ___planeOptions_4;
	// System.Int32 UnityEngine.XR.iOS.UnityARKitControl::currentOptionIndex
	int32_t ___currentOptionIndex_5;
	// System.Int32 UnityEngine.XR.iOS.UnityARKitControl::currentAlignmentIndex
	int32_t ___currentAlignmentIndex_6;
	// System.Int32 UnityEngine.XR.iOS.UnityARKitControl::currentPlaneIndex
	int32_t ___currentPlaneIndex_7;

public:
	inline static int32_t get_offset_of_runOptions_2() { return static_cast<int32_t>(offsetof(UnityARKitControl_t1358211756, ___runOptions_2)); }
	inline UnityARSessionRunOptionU5BU5D_t4225291891* get_runOptions_2() const { return ___runOptions_2; }
	inline UnityARSessionRunOptionU5BU5D_t4225291891** get_address_of_runOptions_2() { return &___runOptions_2; }
	inline void set_runOptions_2(UnityARSessionRunOptionU5BU5D_t4225291891* value)
	{
		___runOptions_2 = value;
		Il2CppCodeGenWriteBarrier((&___runOptions_2), value);
	}

	inline static int32_t get_offset_of_alignmentOptions_3() { return static_cast<int32_t>(offsetof(UnityARKitControl_t1358211756, ___alignmentOptions_3)); }
	inline UnityARAlignmentU5BU5D_t3682394155* get_alignmentOptions_3() const { return ___alignmentOptions_3; }
	inline UnityARAlignmentU5BU5D_t3682394155** get_address_of_alignmentOptions_3() { return &___alignmentOptions_3; }
	inline void set_alignmentOptions_3(UnityARAlignmentU5BU5D_t3682394155* value)
	{
		___alignmentOptions_3 = value;
		Il2CppCodeGenWriteBarrier((&___alignmentOptions_3), value);
	}

	inline static int32_t get_offset_of_planeOptions_4() { return static_cast<int32_t>(offsetof(UnityARKitControl_t1358211756, ___planeOptions_4)); }
	inline UnityARPlaneDetectionU5BU5D_t3458580926* get_planeOptions_4() const { return ___planeOptions_4; }
	inline UnityARPlaneDetectionU5BU5D_t3458580926** get_address_of_planeOptions_4() { return &___planeOptions_4; }
	inline void set_planeOptions_4(UnityARPlaneDetectionU5BU5D_t3458580926* value)
	{
		___planeOptions_4 = value;
		Il2CppCodeGenWriteBarrier((&___planeOptions_4), value);
	}

	inline static int32_t get_offset_of_currentOptionIndex_5() { return static_cast<int32_t>(offsetof(UnityARKitControl_t1358211756, ___currentOptionIndex_5)); }
	inline int32_t get_currentOptionIndex_5() const { return ___currentOptionIndex_5; }
	inline int32_t* get_address_of_currentOptionIndex_5() { return &___currentOptionIndex_5; }
	inline void set_currentOptionIndex_5(int32_t value)
	{
		___currentOptionIndex_5 = value;
	}

	inline static int32_t get_offset_of_currentAlignmentIndex_6() { return static_cast<int32_t>(offsetof(UnityARKitControl_t1358211756, ___currentAlignmentIndex_6)); }
	inline int32_t get_currentAlignmentIndex_6() const { return ___currentAlignmentIndex_6; }
	inline int32_t* get_address_of_currentAlignmentIndex_6() { return &___currentAlignmentIndex_6; }
	inline void set_currentAlignmentIndex_6(int32_t value)
	{
		___currentAlignmentIndex_6 = value;
	}

	inline static int32_t get_offset_of_currentPlaneIndex_7() { return static_cast<int32_t>(offsetof(UnityARKitControl_t1358211756, ___currentPlaneIndex_7)); }
	inline int32_t get_currentPlaneIndex_7() const { return ___currentPlaneIndex_7; }
	inline int32_t* get_address_of_currentPlaneIndex_7() { return &___currentPlaneIndex_7; }
	inline void set_currentPlaneIndex_7(int32_t value)
	{
		___currentPlaneIndex_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARKITCONTROL_T1358211756_H
#ifndef UNITYARHITTESTEXAMPLE_T457226377_H
#define UNITYARHITTESTEXAMPLE_T457226377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARHitTestExample
struct  UnityARHitTestExample_t457226377  : public MonoBehaviour_t3962482529
{
public:
	// PlaneVisualizationManager UnityEngine.XR.iOS.UnityARHitTestExample::planevisualizationmanager
	PlaneVisualizationManager_t1447224982 * ___planevisualizationmanager_2;
	// UnityEngine.Transform UnityEngine.XR.iOS.UnityARHitTestExample::m_HitTransform
	Transform_t3600365921 * ___m_HitTransform_3;
	// System.Single UnityEngine.XR.iOS.UnityARHitTestExample::maxRayDistance
	float ___maxRayDistance_4;
	// UnityEngine.LayerMask UnityEngine.XR.iOS.UnityARHitTestExample::collisionLayer
	LayerMask_t3493934918  ___collisionLayer_5;
	// UnityEngine.Vector2 UnityEngine.XR.iOS.UnityARHitTestExample::m_ScreenPosition
	Vector2_t2156229523  ___m_ScreenPosition_6;
	// UnityEngine.XR.iOS.ARPoint UnityEngine.XR.iOS.UnityARHitTestExample::point
	ARPoint_t499615819  ___point_7;

public:
	inline static int32_t get_offset_of_planevisualizationmanager_2() { return static_cast<int32_t>(offsetof(UnityARHitTestExample_t457226377, ___planevisualizationmanager_2)); }
	inline PlaneVisualizationManager_t1447224982 * get_planevisualizationmanager_2() const { return ___planevisualizationmanager_2; }
	inline PlaneVisualizationManager_t1447224982 ** get_address_of_planevisualizationmanager_2() { return &___planevisualizationmanager_2; }
	inline void set_planevisualizationmanager_2(PlaneVisualizationManager_t1447224982 * value)
	{
		___planevisualizationmanager_2 = value;
		Il2CppCodeGenWriteBarrier((&___planevisualizationmanager_2), value);
	}

	inline static int32_t get_offset_of_m_HitTransform_3() { return static_cast<int32_t>(offsetof(UnityARHitTestExample_t457226377, ___m_HitTransform_3)); }
	inline Transform_t3600365921 * get_m_HitTransform_3() const { return ___m_HitTransform_3; }
	inline Transform_t3600365921 ** get_address_of_m_HitTransform_3() { return &___m_HitTransform_3; }
	inline void set_m_HitTransform_3(Transform_t3600365921 * value)
	{
		___m_HitTransform_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_HitTransform_3), value);
	}

	inline static int32_t get_offset_of_maxRayDistance_4() { return static_cast<int32_t>(offsetof(UnityARHitTestExample_t457226377, ___maxRayDistance_4)); }
	inline float get_maxRayDistance_4() const { return ___maxRayDistance_4; }
	inline float* get_address_of_maxRayDistance_4() { return &___maxRayDistance_4; }
	inline void set_maxRayDistance_4(float value)
	{
		___maxRayDistance_4 = value;
	}

	inline static int32_t get_offset_of_collisionLayer_5() { return static_cast<int32_t>(offsetof(UnityARHitTestExample_t457226377, ___collisionLayer_5)); }
	inline LayerMask_t3493934918  get_collisionLayer_5() const { return ___collisionLayer_5; }
	inline LayerMask_t3493934918 * get_address_of_collisionLayer_5() { return &___collisionLayer_5; }
	inline void set_collisionLayer_5(LayerMask_t3493934918  value)
	{
		___collisionLayer_5 = value;
	}

	inline static int32_t get_offset_of_m_ScreenPosition_6() { return static_cast<int32_t>(offsetof(UnityARHitTestExample_t457226377, ___m_ScreenPosition_6)); }
	inline Vector2_t2156229523  get_m_ScreenPosition_6() const { return ___m_ScreenPosition_6; }
	inline Vector2_t2156229523 * get_address_of_m_ScreenPosition_6() { return &___m_ScreenPosition_6; }
	inline void set_m_ScreenPosition_6(Vector2_t2156229523  value)
	{
		___m_ScreenPosition_6 = value;
	}

	inline static int32_t get_offset_of_point_7() { return static_cast<int32_t>(offsetof(UnityARHitTestExample_t457226377, ___point_7)); }
	inline ARPoint_t499615819  get_point_7() const { return ___point_7; }
	inline ARPoint_t499615819 * get_address_of_point_7() { return &___point_7; }
	inline void set_point_7(ARPoint_t499615819  value)
	{
		___point_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARHITTESTEXAMPLE_T457226377_H
#ifndef UNITYARGENERATEPLANE_T272564669_H
#define UNITYARGENERATEPLANE_T272564669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARGeneratePlane
struct  UnityARGeneratePlane_t272564669  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject UnityEngine.XR.iOS.UnityARGeneratePlane::planePrefab
	GameObject_t1113636619 * ___planePrefab_2;
	// UnityEngine.XR.iOS.UnityARAnchorManager UnityEngine.XR.iOS.UnityARGeneratePlane::unityARAnchorManager
	UnityARAnchorManager_t1557554123 * ___unityARAnchorManager_3;

public:
	inline static int32_t get_offset_of_planePrefab_2() { return static_cast<int32_t>(offsetof(UnityARGeneratePlane_t272564669, ___planePrefab_2)); }
	inline GameObject_t1113636619 * get_planePrefab_2() const { return ___planePrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_planePrefab_2() { return &___planePrefab_2; }
	inline void set_planePrefab_2(GameObject_t1113636619 * value)
	{
		___planePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___planePrefab_2), value);
	}

	inline static int32_t get_offset_of_unityARAnchorManager_3() { return static_cast<int32_t>(offsetof(UnityARGeneratePlane_t272564669, ___unityARAnchorManager_3)); }
	inline UnityARAnchorManager_t1557554123 * get_unityARAnchorManager_3() const { return ___unityARAnchorManager_3; }
	inline UnityARAnchorManager_t1557554123 ** get_address_of_unityARAnchorManager_3() { return &___unityARAnchorManager_3; }
	inline void set_unityARAnchorManager_3(UnityARAnchorManager_t1557554123 * value)
	{
		___unityARAnchorManager_3 = value;
		Il2CppCodeGenWriteBarrier((&___unityARAnchorManager_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARGENERATEPLANE_T272564669_H
#ifndef UNITYARCAMERANEARFAR_T982368306_H
#define UNITYARCAMERANEARFAR_T982368306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityARCameraNearFar
struct  UnityARCameraNearFar_t982368306  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera UnityARCameraNearFar::attachedCamera
	Camera_t4157153871 * ___attachedCamera_2;
	// System.Single UnityARCameraNearFar::currentNearZ
	float ___currentNearZ_3;
	// System.Single UnityARCameraNearFar::currentFarZ
	float ___currentFarZ_4;

public:
	inline static int32_t get_offset_of_attachedCamera_2() { return static_cast<int32_t>(offsetof(UnityARCameraNearFar_t982368306, ___attachedCamera_2)); }
	inline Camera_t4157153871 * get_attachedCamera_2() const { return ___attachedCamera_2; }
	inline Camera_t4157153871 ** get_address_of_attachedCamera_2() { return &___attachedCamera_2; }
	inline void set_attachedCamera_2(Camera_t4157153871 * value)
	{
		___attachedCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___attachedCamera_2), value);
	}

	inline static int32_t get_offset_of_currentNearZ_3() { return static_cast<int32_t>(offsetof(UnityARCameraNearFar_t982368306, ___currentNearZ_3)); }
	inline float get_currentNearZ_3() const { return ___currentNearZ_3; }
	inline float* get_address_of_currentNearZ_3() { return &___currentNearZ_3; }
	inline void set_currentNearZ_3(float value)
	{
		___currentNearZ_3 = value;
	}

	inline static int32_t get_offset_of_currentFarZ_4() { return static_cast<int32_t>(offsetof(UnityARCameraNearFar_t982368306, ___currentFarZ_4)); }
	inline float get_currentFarZ_4() const { return ___currentFarZ_4; }
	inline float* get_address_of_currentFarZ_4() { return &___currentFarZ_4; }
	inline void set_currentFarZ_4(float value)
	{
		___currentFarZ_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARCAMERANEARFAR_T982368306_H
#ifndef UNITYARCAMERAMANAGER_T4002280589_H
#define UNITYARCAMERAMANAGER_T4002280589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityARCameraManager
struct  UnityARCameraManager_t4002280589  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera UnityARCameraManager::m_camera
	Camera_t4157153871 * ___m_camera_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityARCameraManager::m_session
	UnityARSessionNativeInterface_t3929719369 * ___m_session_3;
	// UnityEngine.Material UnityARCameraManager::savedClearMaterial
	Material_t340375123 * ___savedClearMaterial_4;
	// UnityEngine.XR.iOS.UnityARAlignment UnityARCameraManager::startAlignment
	int32_t ___startAlignment_5;
	// UnityEngine.XR.iOS.UnityARPlaneDetection UnityARCameraManager::planeDetection
	int32_t ___planeDetection_6;
	// System.Boolean UnityARCameraManager::getPointCloud
	bool ___getPointCloud_7;
	// System.Boolean UnityARCameraManager::enableLightEstimation
	bool ___enableLightEstimation_8;

public:
	inline static int32_t get_offset_of_m_camera_2() { return static_cast<int32_t>(offsetof(UnityARCameraManager_t4002280589, ___m_camera_2)); }
	inline Camera_t4157153871 * get_m_camera_2() const { return ___m_camera_2; }
	inline Camera_t4157153871 ** get_address_of_m_camera_2() { return &___m_camera_2; }
	inline void set_m_camera_2(Camera_t4157153871 * value)
	{
		___m_camera_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_camera_2), value);
	}

	inline static int32_t get_offset_of_m_session_3() { return static_cast<int32_t>(offsetof(UnityARCameraManager_t4002280589, ___m_session_3)); }
	inline UnityARSessionNativeInterface_t3929719369 * get_m_session_3() const { return ___m_session_3; }
	inline UnityARSessionNativeInterface_t3929719369 ** get_address_of_m_session_3() { return &___m_session_3; }
	inline void set_m_session_3(UnityARSessionNativeInterface_t3929719369 * value)
	{
		___m_session_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_session_3), value);
	}

	inline static int32_t get_offset_of_savedClearMaterial_4() { return static_cast<int32_t>(offsetof(UnityARCameraManager_t4002280589, ___savedClearMaterial_4)); }
	inline Material_t340375123 * get_savedClearMaterial_4() const { return ___savedClearMaterial_4; }
	inline Material_t340375123 ** get_address_of_savedClearMaterial_4() { return &___savedClearMaterial_4; }
	inline void set_savedClearMaterial_4(Material_t340375123 * value)
	{
		___savedClearMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___savedClearMaterial_4), value);
	}

	inline static int32_t get_offset_of_startAlignment_5() { return static_cast<int32_t>(offsetof(UnityARCameraManager_t4002280589, ___startAlignment_5)); }
	inline int32_t get_startAlignment_5() const { return ___startAlignment_5; }
	inline int32_t* get_address_of_startAlignment_5() { return &___startAlignment_5; }
	inline void set_startAlignment_5(int32_t value)
	{
		___startAlignment_5 = value;
	}

	inline static int32_t get_offset_of_planeDetection_6() { return static_cast<int32_t>(offsetof(UnityARCameraManager_t4002280589, ___planeDetection_6)); }
	inline int32_t get_planeDetection_6() const { return ___planeDetection_6; }
	inline int32_t* get_address_of_planeDetection_6() { return &___planeDetection_6; }
	inline void set_planeDetection_6(int32_t value)
	{
		___planeDetection_6 = value;
	}

	inline static int32_t get_offset_of_getPointCloud_7() { return static_cast<int32_t>(offsetof(UnityARCameraManager_t4002280589, ___getPointCloud_7)); }
	inline bool get_getPointCloud_7() const { return ___getPointCloud_7; }
	inline bool* get_address_of_getPointCloud_7() { return &___getPointCloud_7; }
	inline void set_getPointCloud_7(bool value)
	{
		___getPointCloud_7 = value;
	}

	inline static int32_t get_offset_of_enableLightEstimation_8() { return static_cast<int32_t>(offsetof(UnityARCameraManager_t4002280589, ___enableLightEstimation_8)); }
	inline bool get_enableLightEstimation_8() const { return ___enableLightEstimation_8; }
	inline bool* get_address_of_enableLightEstimation_8() { return &___enableLightEstimation_8; }
	inline void set_enableLightEstimation_8(bool value)
	{
		___enableLightEstimation_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARCAMERAMANAGER_T4002280589_H
#ifndef UNITYARAMBIENT_T2710679068_H
#define UNITYARAMBIENT_T2710679068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARAmbient
struct  UnityARAmbient_t2710679068  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Light UnityEngine.XR.iOS.UnityARAmbient::l
	Light_t3756812086 * ___l_2;

public:
	inline static int32_t get_offset_of_l_2() { return static_cast<int32_t>(offsetof(UnityARAmbient_t2710679068, ___l_2)); }
	inline Light_t3756812086 * get_l_2() const { return ___l_2; }
	inline Light_t3756812086 ** get_address_of_l_2() { return &___l_2; }
	inline void set_l_2(Light_t3756812086 * value)
	{
		___l_2 = value;
		Il2CppCodeGenWriteBarrier((&___l_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARAMBIENT_T2710679068_H
#ifndef POINTCLOUDPARTICLEEXAMPLE_T182386800_H
#define POINTCLOUDPARTICLEEXAMPLE_T182386800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PointCloudParticleExample
struct  PointCloudParticleExample_t182386800  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.ParticleSystem PointCloudParticleExample::pointCloudParticlePrefab
	ParticleSystem_t1800779281 * ___pointCloudParticlePrefab_2;
	// System.Int32 PointCloudParticleExample::maxPointsToShow
	int32_t ___maxPointsToShow_3;
	// System.Single PointCloudParticleExample::particleSize
	float ___particleSize_4;
	// UnityEngine.Vector3[] PointCloudParticleExample::m_PointCloudData
	Vector3U5BU5D_t1718750761* ___m_PointCloudData_5;
	// System.Boolean PointCloudParticleExample::frameUpdated
	bool ___frameUpdated_6;
	// UnityEngine.ParticleSystem PointCloudParticleExample::currentPS
	ParticleSystem_t1800779281 * ___currentPS_7;
	// UnityEngine.ParticleSystem/Particle[] PointCloudParticleExample::particles
	ParticleU5BU5D_t3069227754* ___particles_8;
	// System.Boolean PointCloudParticleExample::isStop
	bool ___isStop_9;

public:
	inline static int32_t get_offset_of_pointCloudParticlePrefab_2() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t182386800, ___pointCloudParticlePrefab_2)); }
	inline ParticleSystem_t1800779281 * get_pointCloudParticlePrefab_2() const { return ___pointCloudParticlePrefab_2; }
	inline ParticleSystem_t1800779281 ** get_address_of_pointCloudParticlePrefab_2() { return &___pointCloudParticlePrefab_2; }
	inline void set_pointCloudParticlePrefab_2(ParticleSystem_t1800779281 * value)
	{
		___pointCloudParticlePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudParticlePrefab_2), value);
	}

	inline static int32_t get_offset_of_maxPointsToShow_3() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t182386800, ___maxPointsToShow_3)); }
	inline int32_t get_maxPointsToShow_3() const { return ___maxPointsToShow_3; }
	inline int32_t* get_address_of_maxPointsToShow_3() { return &___maxPointsToShow_3; }
	inline void set_maxPointsToShow_3(int32_t value)
	{
		___maxPointsToShow_3 = value;
	}

	inline static int32_t get_offset_of_particleSize_4() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t182386800, ___particleSize_4)); }
	inline float get_particleSize_4() const { return ___particleSize_4; }
	inline float* get_address_of_particleSize_4() { return &___particleSize_4; }
	inline void set_particleSize_4(float value)
	{
		___particleSize_4 = value;
	}

	inline static int32_t get_offset_of_m_PointCloudData_5() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t182386800, ___m_PointCloudData_5)); }
	inline Vector3U5BU5D_t1718750761* get_m_PointCloudData_5() const { return ___m_PointCloudData_5; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_PointCloudData_5() { return &___m_PointCloudData_5; }
	inline void set_m_PointCloudData_5(Vector3U5BU5D_t1718750761* value)
	{
		___m_PointCloudData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointCloudData_5), value);
	}

	inline static int32_t get_offset_of_frameUpdated_6() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t182386800, ___frameUpdated_6)); }
	inline bool get_frameUpdated_6() const { return ___frameUpdated_6; }
	inline bool* get_address_of_frameUpdated_6() { return &___frameUpdated_6; }
	inline void set_frameUpdated_6(bool value)
	{
		___frameUpdated_6 = value;
	}

	inline static int32_t get_offset_of_currentPS_7() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t182386800, ___currentPS_7)); }
	inline ParticleSystem_t1800779281 * get_currentPS_7() const { return ___currentPS_7; }
	inline ParticleSystem_t1800779281 ** get_address_of_currentPS_7() { return &___currentPS_7; }
	inline void set_currentPS_7(ParticleSystem_t1800779281 * value)
	{
		___currentPS_7 = value;
		Il2CppCodeGenWriteBarrier((&___currentPS_7), value);
	}

	inline static int32_t get_offset_of_particles_8() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t182386800, ___particles_8)); }
	inline ParticleU5BU5D_t3069227754* get_particles_8() const { return ___particles_8; }
	inline ParticleU5BU5D_t3069227754** get_address_of_particles_8() { return &___particles_8; }
	inline void set_particles_8(ParticleU5BU5D_t3069227754* value)
	{
		___particles_8 = value;
		Il2CppCodeGenWriteBarrier((&___particles_8), value);
	}

	inline static int32_t get_offset_of_isStop_9() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t182386800, ___isStop_9)); }
	inline bool get_isStop_9() const { return ___isStop_9; }
	inline bool* get_address_of_isStop_9() { return &___isStop_9; }
	inline void set_isStop_9(bool value)
	{
		___isStop_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTCLOUDPARTICLEEXAMPLE_T182386800_H
#ifndef DONTDESTROYONLOAD_T1456007215_H
#define DONTDESTROYONLOAD_T1456007215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DontDestroyOnLoad
struct  DontDestroyOnLoad_t1456007215  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONTDESTROYONLOAD_T1456007215_H
#ifndef AR3DOFCAMERAMANAGER_T1160001149_H
#define AR3DOFCAMERAMANAGER_T1160001149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AR3DOFCameraManager
struct  AR3DOFCameraManager_t1160001149  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera AR3DOFCameraManager::m_camera
	Camera_t4157153871 * ___m_camera_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface AR3DOFCameraManager::m_session
	UnityARSessionNativeInterface_t3929719369 * ___m_session_3;
	// UnityEngine.Material AR3DOFCameraManager::savedClearMaterial
	Material_t340375123 * ___savedClearMaterial_4;

public:
	inline static int32_t get_offset_of_m_camera_2() { return static_cast<int32_t>(offsetof(AR3DOFCameraManager_t1160001149, ___m_camera_2)); }
	inline Camera_t4157153871 * get_m_camera_2() const { return ___m_camera_2; }
	inline Camera_t4157153871 ** get_address_of_m_camera_2() { return &___m_camera_2; }
	inline void set_m_camera_2(Camera_t4157153871 * value)
	{
		___m_camera_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_camera_2), value);
	}

	inline static int32_t get_offset_of_m_session_3() { return static_cast<int32_t>(offsetof(AR3DOFCameraManager_t1160001149, ___m_session_3)); }
	inline UnityARSessionNativeInterface_t3929719369 * get_m_session_3() const { return ___m_session_3; }
	inline UnityARSessionNativeInterface_t3929719369 ** get_address_of_m_session_3() { return &___m_session_3; }
	inline void set_m_session_3(UnityARSessionNativeInterface_t3929719369 * value)
	{
		___m_session_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_session_3), value);
	}

	inline static int32_t get_offset_of_savedClearMaterial_4() { return static_cast<int32_t>(offsetof(AR3DOFCameraManager_t1160001149, ___savedClearMaterial_4)); }
	inline Material_t340375123 * get_savedClearMaterial_4() const { return ___savedClearMaterial_4; }
	inline Material_t340375123 ** get_address_of_savedClearMaterial_4() { return &___savedClearMaterial_4; }
	inline void set_savedClearMaterial_4(Material_t340375123 * value)
	{
		___savedClearMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___savedClearMaterial_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AR3DOFCAMERAMANAGER_T1160001149_H
#ifndef UNITYPOINTCLOUDEXAMPLE_T3649008995_H
#define UNITYPOINTCLOUDEXAMPLE_T3649008995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityPointCloudExample
struct  UnityPointCloudExample_t3649008995  : public MonoBehaviour_t3962482529
{
public:
	// System.UInt32 UnityPointCloudExample::numPointsToShow
	uint32_t ___numPointsToShow_2;
	// UnityEngine.GameObject UnityPointCloudExample::PointCloudPrefab
	GameObject_t1113636619 * ___PointCloudPrefab_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityPointCloudExample::pointCloudObjects
	List_1_t2585711361 * ___pointCloudObjects_4;
	// UnityEngine.Vector3[] UnityPointCloudExample::m_PointCloudData
	Vector3U5BU5D_t1718750761* ___m_PointCloudData_5;

public:
	inline static int32_t get_offset_of_numPointsToShow_2() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t3649008995, ___numPointsToShow_2)); }
	inline uint32_t get_numPointsToShow_2() const { return ___numPointsToShow_2; }
	inline uint32_t* get_address_of_numPointsToShow_2() { return &___numPointsToShow_2; }
	inline void set_numPointsToShow_2(uint32_t value)
	{
		___numPointsToShow_2 = value;
	}

	inline static int32_t get_offset_of_PointCloudPrefab_3() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t3649008995, ___PointCloudPrefab_3)); }
	inline GameObject_t1113636619 * get_PointCloudPrefab_3() const { return ___PointCloudPrefab_3; }
	inline GameObject_t1113636619 ** get_address_of_PointCloudPrefab_3() { return &___PointCloudPrefab_3; }
	inline void set_PointCloudPrefab_3(GameObject_t1113636619 * value)
	{
		___PointCloudPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___PointCloudPrefab_3), value);
	}

	inline static int32_t get_offset_of_pointCloudObjects_4() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t3649008995, ___pointCloudObjects_4)); }
	inline List_1_t2585711361 * get_pointCloudObjects_4() const { return ___pointCloudObjects_4; }
	inline List_1_t2585711361 ** get_address_of_pointCloudObjects_4() { return &___pointCloudObjects_4; }
	inline void set_pointCloudObjects_4(List_1_t2585711361 * value)
	{
		___pointCloudObjects_4 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudObjects_4), value);
	}

	inline static int32_t get_offset_of_m_PointCloudData_5() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t3649008995, ___m_PointCloudData_5)); }
	inline Vector3U5BU5D_t1718750761* get_m_PointCloudData_5() const { return ___m_PointCloudData_5; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_PointCloudData_5() { return &___m_PointCloudData_5; }
	inline void set_m_PointCloudData_5(Vector3U5BU5D_t1718750761* value)
	{
		___m_PointCloudData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointCloudData_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYPOINTCLOUDEXAMPLE_T3649008995_H
#ifndef PARTICLEPAINTER_T1984011264_H
#define PARTICLEPAINTER_T1984011264_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticlePainter
struct  ParticlePainter_t1984011264  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.ParticleSystem ParticlePainter::painterParticlePrefab
	ParticleSystem_t1800779281 * ___painterParticlePrefab_2;
	// System.Single ParticlePainter::minDistanceThreshold
	float ___minDistanceThreshold_3;
	// System.Single ParticlePainter::maxDistanceThreshold
	float ___maxDistanceThreshold_4;
	// System.Boolean ParticlePainter::frameUpdated
	bool ___frameUpdated_5;
	// System.Single ParticlePainter::particleSize
	float ___particleSize_6;
	// System.Single ParticlePainter::penDistance
	float ___penDistance_7;
	// ColorPicker ParticlePainter::colorPicker
	ColorPicker_t228004619 * ___colorPicker_8;
	// UnityEngine.ParticleSystem ParticlePainter::currentPS
	ParticleSystem_t1800779281 * ___currentPS_9;
	// UnityEngine.ParticleSystem/Particle[] ParticlePainter::particles
	ParticleU5BU5D_t3069227754* ___particles_10;
	// UnityEngine.Vector3 ParticlePainter::previousPosition
	Vector3_t3722313464  ___previousPosition_11;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> ParticlePainter::currentPaintVertices
	List_1_t899420910 * ___currentPaintVertices_12;
	// UnityEngine.Color ParticlePainter::currentColor
	Color_t2555686324  ___currentColor_13;
	// System.Collections.Generic.List`1<UnityEngine.ParticleSystem> ParticlePainter::paintSystems
	List_1_t3272854023 * ___paintSystems_14;
	// System.Int32 ParticlePainter::paintMode
	int32_t ___paintMode_15;

public:
	inline static int32_t get_offset_of_painterParticlePrefab_2() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___painterParticlePrefab_2)); }
	inline ParticleSystem_t1800779281 * get_painterParticlePrefab_2() const { return ___painterParticlePrefab_2; }
	inline ParticleSystem_t1800779281 ** get_address_of_painterParticlePrefab_2() { return &___painterParticlePrefab_2; }
	inline void set_painterParticlePrefab_2(ParticleSystem_t1800779281 * value)
	{
		___painterParticlePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___painterParticlePrefab_2), value);
	}

	inline static int32_t get_offset_of_minDistanceThreshold_3() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___minDistanceThreshold_3)); }
	inline float get_minDistanceThreshold_3() const { return ___minDistanceThreshold_3; }
	inline float* get_address_of_minDistanceThreshold_3() { return &___minDistanceThreshold_3; }
	inline void set_minDistanceThreshold_3(float value)
	{
		___minDistanceThreshold_3 = value;
	}

	inline static int32_t get_offset_of_maxDistanceThreshold_4() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___maxDistanceThreshold_4)); }
	inline float get_maxDistanceThreshold_4() const { return ___maxDistanceThreshold_4; }
	inline float* get_address_of_maxDistanceThreshold_4() { return &___maxDistanceThreshold_4; }
	inline void set_maxDistanceThreshold_4(float value)
	{
		___maxDistanceThreshold_4 = value;
	}

	inline static int32_t get_offset_of_frameUpdated_5() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___frameUpdated_5)); }
	inline bool get_frameUpdated_5() const { return ___frameUpdated_5; }
	inline bool* get_address_of_frameUpdated_5() { return &___frameUpdated_5; }
	inline void set_frameUpdated_5(bool value)
	{
		___frameUpdated_5 = value;
	}

	inline static int32_t get_offset_of_particleSize_6() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___particleSize_6)); }
	inline float get_particleSize_6() const { return ___particleSize_6; }
	inline float* get_address_of_particleSize_6() { return &___particleSize_6; }
	inline void set_particleSize_6(float value)
	{
		___particleSize_6 = value;
	}

	inline static int32_t get_offset_of_penDistance_7() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___penDistance_7)); }
	inline float get_penDistance_7() const { return ___penDistance_7; }
	inline float* get_address_of_penDistance_7() { return &___penDistance_7; }
	inline void set_penDistance_7(float value)
	{
		___penDistance_7 = value;
	}

	inline static int32_t get_offset_of_colorPicker_8() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___colorPicker_8)); }
	inline ColorPicker_t228004619 * get_colorPicker_8() const { return ___colorPicker_8; }
	inline ColorPicker_t228004619 ** get_address_of_colorPicker_8() { return &___colorPicker_8; }
	inline void set_colorPicker_8(ColorPicker_t228004619 * value)
	{
		___colorPicker_8 = value;
		Il2CppCodeGenWriteBarrier((&___colorPicker_8), value);
	}

	inline static int32_t get_offset_of_currentPS_9() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___currentPS_9)); }
	inline ParticleSystem_t1800779281 * get_currentPS_9() const { return ___currentPS_9; }
	inline ParticleSystem_t1800779281 ** get_address_of_currentPS_9() { return &___currentPS_9; }
	inline void set_currentPS_9(ParticleSystem_t1800779281 * value)
	{
		___currentPS_9 = value;
		Il2CppCodeGenWriteBarrier((&___currentPS_9), value);
	}

	inline static int32_t get_offset_of_particles_10() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___particles_10)); }
	inline ParticleU5BU5D_t3069227754* get_particles_10() const { return ___particles_10; }
	inline ParticleU5BU5D_t3069227754** get_address_of_particles_10() { return &___particles_10; }
	inline void set_particles_10(ParticleU5BU5D_t3069227754* value)
	{
		___particles_10 = value;
		Il2CppCodeGenWriteBarrier((&___particles_10), value);
	}

	inline static int32_t get_offset_of_previousPosition_11() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___previousPosition_11)); }
	inline Vector3_t3722313464  get_previousPosition_11() const { return ___previousPosition_11; }
	inline Vector3_t3722313464 * get_address_of_previousPosition_11() { return &___previousPosition_11; }
	inline void set_previousPosition_11(Vector3_t3722313464  value)
	{
		___previousPosition_11 = value;
	}

	inline static int32_t get_offset_of_currentPaintVertices_12() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___currentPaintVertices_12)); }
	inline List_1_t899420910 * get_currentPaintVertices_12() const { return ___currentPaintVertices_12; }
	inline List_1_t899420910 ** get_address_of_currentPaintVertices_12() { return &___currentPaintVertices_12; }
	inline void set_currentPaintVertices_12(List_1_t899420910 * value)
	{
		___currentPaintVertices_12 = value;
		Il2CppCodeGenWriteBarrier((&___currentPaintVertices_12), value);
	}

	inline static int32_t get_offset_of_currentColor_13() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___currentColor_13)); }
	inline Color_t2555686324  get_currentColor_13() const { return ___currentColor_13; }
	inline Color_t2555686324 * get_address_of_currentColor_13() { return &___currentColor_13; }
	inline void set_currentColor_13(Color_t2555686324  value)
	{
		___currentColor_13 = value;
	}

	inline static int32_t get_offset_of_paintSystems_14() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___paintSystems_14)); }
	inline List_1_t3272854023 * get_paintSystems_14() const { return ___paintSystems_14; }
	inline List_1_t3272854023 ** get_address_of_paintSystems_14() { return &___paintSystems_14; }
	inline void set_paintSystems_14(List_1_t3272854023 * value)
	{
		___paintSystems_14 = value;
		Il2CppCodeGenWriteBarrier((&___paintSystems_14), value);
	}

	inline static int32_t get_offset_of_paintMode_15() { return static_cast<int32_t>(offsetof(ParticlePainter_t1984011264, ___paintMode_15)); }
	inline int32_t get_paintMode_15() const { return ___paintMode_15; }
	inline int32_t* get_address_of_paintMode_15() { return &___paintMode_15; }
	inline void set_paintMode_15(int32_t value)
	{
		___paintMode_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLEPAINTER_T1984011264_H
#ifndef COLORSLIDERIMAGE_T1393030097_H
#define COLORSLIDERIMAGE_T1393030097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorSliderImage
struct  ColorSliderImage_t1393030097  : public MonoBehaviour_t3962482529
{
public:
	// ColorPicker ColorSliderImage::picker
	ColorPicker_t228004619 * ___picker_2;
	// ColorValues ColorSliderImage::type
	int32_t ___type_3;
	// UnityEngine.UI.Slider/Direction ColorSliderImage::direction
	int32_t ___direction_4;
	// UnityEngine.UI.RawImage ColorSliderImage::image
	RawImage_t3182918964 * ___image_5;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(ColorSliderImage_t1393030097, ___picker_2)); }
	inline ColorPicker_t228004619 * get_picker_2() const { return ___picker_2; }
	inline ColorPicker_t228004619 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPicker_t228004619 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(ColorSliderImage_t1393030097, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_direction_4() { return static_cast<int32_t>(offsetof(ColorSliderImage_t1393030097, ___direction_4)); }
	inline int32_t get_direction_4() const { return ___direction_4; }
	inline int32_t* get_address_of_direction_4() { return &___direction_4; }
	inline void set_direction_4(int32_t value)
	{
		___direction_4 = value;
	}

	inline static int32_t get_offset_of_image_5() { return static_cast<int32_t>(offsetof(ColorSliderImage_t1393030097, ___image_5)); }
	inline RawImage_t3182918964 * get_image_5() const { return ___image_5; }
	inline RawImage_t3182918964 ** get_address_of_image_5() { return &___image_5; }
	inline void set_image_5(RawImage_t3182918964 * value)
	{
		___image_5 = value;
		Il2CppCodeGenWriteBarrier((&___image_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORSLIDERIMAGE_T1393030097_H
#ifndef COLORSLIDER_T2624382019_H
#define COLORSLIDER_T2624382019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorSlider
struct  ColorSlider_t2624382019  : public MonoBehaviour_t3962482529
{
public:
	// ColorPicker ColorSlider::hsvpicker
	ColorPicker_t228004619 * ___hsvpicker_2;
	// ColorValues ColorSlider::type
	int32_t ___type_3;
	// UnityEngine.UI.Slider ColorSlider::slider
	Slider_t3903728902 * ___slider_4;
	// System.Boolean ColorSlider::listen
	bool ___listen_5;

public:
	inline static int32_t get_offset_of_hsvpicker_2() { return static_cast<int32_t>(offsetof(ColorSlider_t2624382019, ___hsvpicker_2)); }
	inline ColorPicker_t228004619 * get_hsvpicker_2() const { return ___hsvpicker_2; }
	inline ColorPicker_t228004619 ** get_address_of_hsvpicker_2() { return &___hsvpicker_2; }
	inline void set_hsvpicker_2(ColorPicker_t228004619 * value)
	{
		___hsvpicker_2 = value;
		Il2CppCodeGenWriteBarrier((&___hsvpicker_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(ColorSlider_t2624382019, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_slider_4() { return static_cast<int32_t>(offsetof(ColorSlider_t2624382019, ___slider_4)); }
	inline Slider_t3903728902 * get_slider_4() const { return ___slider_4; }
	inline Slider_t3903728902 ** get_address_of_slider_4() { return &___slider_4; }
	inline void set_slider_4(Slider_t3903728902 * value)
	{
		___slider_4 = value;
		Il2CppCodeGenWriteBarrier((&___slider_4), value);
	}

	inline static int32_t get_offset_of_listen_5() { return static_cast<int32_t>(offsetof(ColorSlider_t2624382019, ___listen_5)); }
	inline bool get_listen_5() const { return ___listen_5; }
	inline bool* get_address_of_listen_5() { return &___listen_5; }
	inline void set_listen_5(bool value)
	{
		___listen_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORSLIDER_T2624382019_H
#ifndef COLORPRESETS_T2117877396_H
#define COLORPRESETS_T2117877396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorPresets
struct  ColorPresets_t2117877396  : public MonoBehaviour_t3962482529
{
public:
	// ColorPicker ColorPresets::picker
	ColorPicker_t228004619 * ___picker_2;
	// UnityEngine.GameObject[] ColorPresets::presets
	GameObjectU5BU5D_t3328599146* ___presets_3;
	// UnityEngine.UI.Image ColorPresets::createPresetImage
	Image_t2670269651 * ___createPresetImage_4;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(ColorPresets_t2117877396, ___picker_2)); }
	inline ColorPicker_t228004619 * get_picker_2() const { return ___picker_2; }
	inline ColorPicker_t228004619 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPicker_t228004619 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_presets_3() { return static_cast<int32_t>(offsetof(ColorPresets_t2117877396, ___presets_3)); }
	inline GameObjectU5BU5D_t3328599146* get_presets_3() const { return ___presets_3; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_presets_3() { return &___presets_3; }
	inline void set_presets_3(GameObjectU5BU5D_t3328599146* value)
	{
		___presets_3 = value;
		Il2CppCodeGenWriteBarrier((&___presets_3), value);
	}

	inline static int32_t get_offset_of_createPresetImage_4() { return static_cast<int32_t>(offsetof(ColorPresets_t2117877396, ___createPresetImage_4)); }
	inline Image_t2670269651 * get_createPresetImage_4() const { return ___createPresetImage_4; }
	inline Image_t2670269651 ** get_address_of_createPresetImage_4() { return &___createPresetImage_4; }
	inline void set_createPresetImage_4(Image_t2670269651 * value)
	{
		___createPresetImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___createPresetImage_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPRESETS_T2117877396_H
#ifndef COLORPICKER_T228004619_H
#define COLORPICKER_T228004619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorPicker
struct  ColorPicker_t228004619  : public MonoBehaviour_t3962482529
{
public:
	// System.Single ColorPicker::_hue
	float ____hue_2;
	// System.Single ColorPicker::_saturation
	float ____saturation_3;
	// System.Single ColorPicker::_brightness
	float ____brightness_4;
	// System.Single ColorPicker::_red
	float ____red_5;
	// System.Single ColorPicker::_green
	float ____green_6;
	// System.Single ColorPicker::_blue
	float ____blue_7;
	// System.Single ColorPicker::_alpha
	float ____alpha_8;
	// ColorChangedEvent ColorPicker::onValueChanged
	ColorChangedEvent_t3019780707 * ___onValueChanged_9;
	// HSVChangedEvent ColorPicker::onHSVChanged
	HSVChangedEvent_t911780251 * ___onHSVChanged_10;

public:
	inline static int32_t get_offset_of__hue_2() { return static_cast<int32_t>(offsetof(ColorPicker_t228004619, ____hue_2)); }
	inline float get__hue_2() const { return ____hue_2; }
	inline float* get_address_of__hue_2() { return &____hue_2; }
	inline void set__hue_2(float value)
	{
		____hue_2 = value;
	}

	inline static int32_t get_offset_of__saturation_3() { return static_cast<int32_t>(offsetof(ColorPicker_t228004619, ____saturation_3)); }
	inline float get__saturation_3() const { return ____saturation_3; }
	inline float* get_address_of__saturation_3() { return &____saturation_3; }
	inline void set__saturation_3(float value)
	{
		____saturation_3 = value;
	}

	inline static int32_t get_offset_of__brightness_4() { return static_cast<int32_t>(offsetof(ColorPicker_t228004619, ____brightness_4)); }
	inline float get__brightness_4() const { return ____brightness_4; }
	inline float* get_address_of__brightness_4() { return &____brightness_4; }
	inline void set__brightness_4(float value)
	{
		____brightness_4 = value;
	}

	inline static int32_t get_offset_of__red_5() { return static_cast<int32_t>(offsetof(ColorPicker_t228004619, ____red_5)); }
	inline float get__red_5() const { return ____red_5; }
	inline float* get_address_of__red_5() { return &____red_5; }
	inline void set__red_5(float value)
	{
		____red_5 = value;
	}

	inline static int32_t get_offset_of__green_6() { return static_cast<int32_t>(offsetof(ColorPicker_t228004619, ____green_6)); }
	inline float get__green_6() const { return ____green_6; }
	inline float* get_address_of__green_6() { return &____green_6; }
	inline void set__green_6(float value)
	{
		____green_6 = value;
	}

	inline static int32_t get_offset_of__blue_7() { return static_cast<int32_t>(offsetof(ColorPicker_t228004619, ____blue_7)); }
	inline float get__blue_7() const { return ____blue_7; }
	inline float* get_address_of__blue_7() { return &____blue_7; }
	inline void set__blue_7(float value)
	{
		____blue_7 = value;
	}

	inline static int32_t get_offset_of__alpha_8() { return static_cast<int32_t>(offsetof(ColorPicker_t228004619, ____alpha_8)); }
	inline float get__alpha_8() const { return ____alpha_8; }
	inline float* get_address_of__alpha_8() { return &____alpha_8; }
	inline void set__alpha_8(float value)
	{
		____alpha_8 = value;
	}

	inline static int32_t get_offset_of_onValueChanged_9() { return static_cast<int32_t>(offsetof(ColorPicker_t228004619, ___onValueChanged_9)); }
	inline ColorChangedEvent_t3019780707 * get_onValueChanged_9() const { return ___onValueChanged_9; }
	inline ColorChangedEvent_t3019780707 ** get_address_of_onValueChanged_9() { return &___onValueChanged_9; }
	inline void set_onValueChanged_9(ColorChangedEvent_t3019780707 * value)
	{
		___onValueChanged_9 = value;
		Il2CppCodeGenWriteBarrier((&___onValueChanged_9), value);
	}

	inline static int32_t get_offset_of_onHSVChanged_10() { return static_cast<int32_t>(offsetof(ColorPicker_t228004619, ___onHSVChanged_10)); }
	inline HSVChangedEvent_t911780251 * get_onHSVChanged_10() const { return ___onHSVChanged_10; }
	inline HSVChangedEvent_t911780251 ** get_address_of_onHSVChanged_10() { return &___onHSVChanged_10; }
	inline void set_onHSVChanged_10(HSVChangedEvent_t911780251 * value)
	{
		___onHSVChanged_10 = value;
		Il2CppCodeGenWriteBarrier((&___onHSVChanged_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPICKER_T228004619_H
#ifndef COLORLABEL_T2272707290_H
#define COLORLABEL_T2272707290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorLabel
struct  ColorLabel_t2272707290  : public MonoBehaviour_t3962482529
{
public:
	// ColorPicker ColorLabel::picker
	ColorPicker_t228004619 * ___picker_2;
	// ColorValues ColorLabel::type
	int32_t ___type_3;
	// System.String ColorLabel::prefix
	String_t* ___prefix_4;
	// System.Single ColorLabel::minValue
	float ___minValue_5;
	// System.Single ColorLabel::maxValue
	float ___maxValue_6;
	// System.Int32 ColorLabel::precision
	int32_t ___precision_7;
	// UnityEngine.UI.Text ColorLabel::label
	Text_t1901882714 * ___label_8;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(ColorLabel_t2272707290, ___picker_2)); }
	inline ColorPicker_t228004619 * get_picker_2() const { return ___picker_2; }
	inline ColorPicker_t228004619 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPicker_t228004619 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(ColorLabel_t2272707290, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_prefix_4() { return static_cast<int32_t>(offsetof(ColorLabel_t2272707290, ___prefix_4)); }
	inline String_t* get_prefix_4() const { return ___prefix_4; }
	inline String_t** get_address_of_prefix_4() { return &___prefix_4; }
	inline void set_prefix_4(String_t* value)
	{
		___prefix_4 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_4), value);
	}

	inline static int32_t get_offset_of_minValue_5() { return static_cast<int32_t>(offsetof(ColorLabel_t2272707290, ___minValue_5)); }
	inline float get_minValue_5() const { return ___minValue_5; }
	inline float* get_address_of_minValue_5() { return &___minValue_5; }
	inline void set_minValue_5(float value)
	{
		___minValue_5 = value;
	}

	inline static int32_t get_offset_of_maxValue_6() { return static_cast<int32_t>(offsetof(ColorLabel_t2272707290, ___maxValue_6)); }
	inline float get_maxValue_6() const { return ___maxValue_6; }
	inline float* get_address_of_maxValue_6() { return &___maxValue_6; }
	inline void set_maxValue_6(float value)
	{
		___maxValue_6 = value;
	}

	inline static int32_t get_offset_of_precision_7() { return static_cast<int32_t>(offsetof(ColorLabel_t2272707290, ___precision_7)); }
	inline int32_t get_precision_7() const { return ___precision_7; }
	inline int32_t* get_address_of_precision_7() { return &___precision_7; }
	inline void set_precision_7(int32_t value)
	{
		___precision_7 = value;
	}

	inline static int32_t get_offset_of_label_8() { return static_cast<int32_t>(offsetof(ColorLabel_t2272707290, ___label_8)); }
	inline Text_t1901882714 * get_label_8() const { return ___label_8; }
	inline Text_t1901882714 ** get_address_of_label_8() { return &___label_8; }
	inline void set_label_8(Text_t1901882714 * value)
	{
		___label_8 = value;
		Il2CppCodeGenWriteBarrier((&___label_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORLABEL_T2272707290_H
#ifndef COLORIMAGE_T1922452376_H
#define COLORIMAGE_T1922452376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorImage
struct  ColorImage_t1922452376  : public MonoBehaviour_t3962482529
{
public:
	// ColorPicker ColorImage::picker
	ColorPicker_t228004619 * ___picker_2;
	// UnityEngine.UI.Image ColorImage::image
	Image_t2670269651 * ___image_3;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(ColorImage_t1922452376, ___picker_2)); }
	inline ColorPicker_t228004619 * get_picker_2() const { return ___picker_2; }
	inline ColorPicker_t228004619 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPicker_t228004619 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_image_3() { return static_cast<int32_t>(offsetof(ColorImage_t1922452376, ___image_3)); }
	inline Image_t2670269651 * get_image_3() const { return ___image_3; }
	inline Image_t2670269651 ** get_address_of_image_3() { return &___image_3; }
	inline void set_image_3(Image_t2670269651 * value)
	{
		___image_3 = value;
		Il2CppCodeGenWriteBarrier((&___image_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORIMAGE_T1922452376_H
#ifndef TILTWINDOW_T335293945_H
#define TILTWINDOW_T335293945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TiltWindow
struct  TiltWindow_t335293945  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector2 TiltWindow::range
	Vector2_t2156229523  ___range_2;
	// UnityEngine.Transform TiltWindow::mTrans
	Transform_t3600365921 * ___mTrans_3;
	// UnityEngine.Quaternion TiltWindow::mStart
	Quaternion_t2301928331  ___mStart_4;
	// UnityEngine.Vector2 TiltWindow::mRot
	Vector2_t2156229523  ___mRot_5;

public:
	inline static int32_t get_offset_of_range_2() { return static_cast<int32_t>(offsetof(TiltWindow_t335293945, ___range_2)); }
	inline Vector2_t2156229523  get_range_2() const { return ___range_2; }
	inline Vector2_t2156229523 * get_address_of_range_2() { return &___range_2; }
	inline void set_range_2(Vector2_t2156229523  value)
	{
		___range_2 = value;
	}

	inline static int32_t get_offset_of_mTrans_3() { return static_cast<int32_t>(offsetof(TiltWindow_t335293945, ___mTrans_3)); }
	inline Transform_t3600365921 * get_mTrans_3() const { return ___mTrans_3; }
	inline Transform_t3600365921 ** get_address_of_mTrans_3() { return &___mTrans_3; }
	inline void set_mTrans_3(Transform_t3600365921 * value)
	{
		___mTrans_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_3), value);
	}

	inline static int32_t get_offset_of_mStart_4() { return static_cast<int32_t>(offsetof(TiltWindow_t335293945, ___mStart_4)); }
	inline Quaternion_t2301928331  get_mStart_4() const { return ___mStart_4; }
	inline Quaternion_t2301928331 * get_address_of_mStart_4() { return &___mStart_4; }
	inline void set_mStart_4(Quaternion_t2301928331  value)
	{
		___mStart_4 = value;
	}

	inline static int32_t get_offset_of_mRot_5() { return static_cast<int32_t>(offsetof(TiltWindow_t335293945, ___mRot_5)); }
	inline Vector2_t2156229523  get_mRot_5() const { return ___mRot_5; }
	inline Vector2_t2156229523 * get_address_of_mRot_5() { return &___mRot_5; }
	inline void set_mRot_5(Vector2_t2156229523  value)
	{
		___mRot_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTWINDOW_T335293945_H
#ifndef COLORPICKERTESTER_T3074432426_H
#define COLORPICKERTESTER_T3074432426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorPickerTester
struct  ColorPickerTester_t3074432426  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Renderer ColorPickerTester::renderer
	Renderer_t2627027031 * ___renderer_2;
	// ColorPicker ColorPickerTester::picker
	ColorPicker_t228004619 * ___picker_3;

public:
	inline static int32_t get_offset_of_renderer_2() { return static_cast<int32_t>(offsetof(ColorPickerTester_t3074432426, ___renderer_2)); }
	inline Renderer_t2627027031 * get_renderer_2() const { return ___renderer_2; }
	inline Renderer_t2627027031 ** get_address_of_renderer_2() { return &___renderer_2; }
	inline void set_renderer_2(Renderer_t2627027031 * value)
	{
		___renderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___renderer_2), value);
	}

	inline static int32_t get_offset_of_picker_3() { return static_cast<int32_t>(offsetof(ColorPickerTester_t3074432426, ___picker_3)); }
	inline ColorPicker_t228004619 * get_picker_3() const { return ___picker_3; }
	inline ColorPicker_t228004619 ** get_address_of_picker_3() { return &___picker_3; }
	inline void set_picker_3(ColorPicker_t228004619 * value)
	{
		___picker_3 = value;
		Il2CppCodeGenWriteBarrier((&___picker_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPICKERTESTER_T3074432426_H
#ifndef MODESWITCHER_T3643344453_H
#define MODESWITCHER_T3643344453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModeSwitcher
struct  ModeSwitcher_t3643344453  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject ModeSwitcher::ballMake
	GameObject_t1113636619 * ___ballMake_2;
	// UnityEngine.GameObject ModeSwitcher::ballMove
	GameObject_t1113636619 * ___ballMove_3;
	// System.Int32 ModeSwitcher::appMode
	int32_t ___appMode_4;

public:
	inline static int32_t get_offset_of_ballMake_2() { return static_cast<int32_t>(offsetof(ModeSwitcher_t3643344453, ___ballMake_2)); }
	inline GameObject_t1113636619 * get_ballMake_2() const { return ___ballMake_2; }
	inline GameObject_t1113636619 ** get_address_of_ballMake_2() { return &___ballMake_2; }
	inline void set_ballMake_2(GameObject_t1113636619 * value)
	{
		___ballMake_2 = value;
		Il2CppCodeGenWriteBarrier((&___ballMake_2), value);
	}

	inline static int32_t get_offset_of_ballMove_3() { return static_cast<int32_t>(offsetof(ModeSwitcher_t3643344453, ___ballMove_3)); }
	inline GameObject_t1113636619 * get_ballMove_3() const { return ___ballMove_3; }
	inline GameObject_t1113636619 ** get_address_of_ballMove_3() { return &___ballMove_3; }
	inline void set_ballMove_3(GameObject_t1113636619 * value)
	{
		___ballMove_3 = value;
		Il2CppCodeGenWriteBarrier((&___ballMove_3), value);
	}

	inline static int32_t get_offset_of_appMode_4() { return static_cast<int32_t>(offsetof(ModeSwitcher_t3643344453, ___appMode_4)); }
	inline int32_t get_appMode_4() const { return ___appMode_4; }
	inline int32_t* get_address_of_appMode_4() { return &___appMode_4; }
	inline void set_appMode_4(int32_t value)
	{
		___appMode_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODESWITCHER_T3643344453_H
#ifndef BALLZ_T1012779874_H
#define BALLZ_T1012779874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ballz
struct  Ballz_t1012779874  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Ballz::yDistanceThreshold
	float ___yDistanceThreshold_2;
	// System.Single Ballz::startingY
	float ___startingY_3;

public:
	inline static int32_t get_offset_of_yDistanceThreshold_2() { return static_cast<int32_t>(offsetof(Ballz_t1012779874, ___yDistanceThreshold_2)); }
	inline float get_yDistanceThreshold_2() const { return ___yDistanceThreshold_2; }
	inline float* get_address_of_yDistanceThreshold_2() { return &___yDistanceThreshold_2; }
	inline void set_yDistanceThreshold_2(float value)
	{
		___yDistanceThreshold_2 = value;
	}

	inline static int32_t get_offset_of_startingY_3() { return static_cast<int32_t>(offsetof(Ballz_t1012779874, ___startingY_3)); }
	inline float get_startingY_3() const { return ___startingY_3; }
	inline float* get_address_of_startingY_3() { return &___startingY_3; }
	inline void set_startingY_3(float value)
	{
		___startingY_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLZ_T1012779874_H
#ifndef BALLMOVER_T2920303374_H
#define BALLMOVER_T2920303374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallMover
struct  BallMover_t2920303374  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject BallMover::collBallPrefab
	GameObject_t1113636619 * ___collBallPrefab_2;
	// UnityEngine.GameObject BallMover::collBallGO
	GameObject_t1113636619 * ___collBallGO_3;

public:
	inline static int32_t get_offset_of_collBallPrefab_2() { return static_cast<int32_t>(offsetof(BallMover_t2920303374, ___collBallPrefab_2)); }
	inline GameObject_t1113636619 * get_collBallPrefab_2() const { return ___collBallPrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_collBallPrefab_2() { return &___collBallPrefab_2; }
	inline void set_collBallPrefab_2(GameObject_t1113636619 * value)
	{
		___collBallPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___collBallPrefab_2), value);
	}

	inline static int32_t get_offset_of_collBallGO_3() { return static_cast<int32_t>(offsetof(BallMover_t2920303374, ___collBallGO_3)); }
	inline GameObject_t1113636619 * get_collBallGO_3() const { return ___collBallGO_3; }
	inline GameObject_t1113636619 ** get_address_of_collBallGO_3() { return &___collBallGO_3; }
	inline void set_collBallGO_3(GameObject_t1113636619 * value)
	{
		___collBallGO_3 = value;
		Il2CppCodeGenWriteBarrier((&___collBallGO_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLMOVER_T2920303374_H
#ifndef HEXCOLORFIELD_T944280679_H
#define HEXCOLORFIELD_T944280679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HexColorField
struct  HexColorField_t944280679  : public MonoBehaviour_t3962482529
{
public:
	// ColorPicker HexColorField::hsvpicker
	ColorPicker_t228004619 * ___hsvpicker_2;
	// System.Boolean HexColorField::displayAlpha
	bool ___displayAlpha_3;
	// UnityEngine.UI.InputField HexColorField::hexInputField
	InputField_t3762917431 * ___hexInputField_4;

public:
	inline static int32_t get_offset_of_hsvpicker_2() { return static_cast<int32_t>(offsetof(HexColorField_t944280679, ___hsvpicker_2)); }
	inline ColorPicker_t228004619 * get_hsvpicker_2() const { return ___hsvpicker_2; }
	inline ColorPicker_t228004619 ** get_address_of_hsvpicker_2() { return &___hsvpicker_2; }
	inline void set_hsvpicker_2(ColorPicker_t228004619 * value)
	{
		___hsvpicker_2 = value;
		Il2CppCodeGenWriteBarrier((&___hsvpicker_2), value);
	}

	inline static int32_t get_offset_of_displayAlpha_3() { return static_cast<int32_t>(offsetof(HexColorField_t944280679, ___displayAlpha_3)); }
	inline bool get_displayAlpha_3() const { return ___displayAlpha_3; }
	inline bool* get_address_of_displayAlpha_3() { return &___displayAlpha_3; }
	inline void set_displayAlpha_3(bool value)
	{
		___displayAlpha_3 = value;
	}

	inline static int32_t get_offset_of_hexInputField_4() { return static_cast<int32_t>(offsetof(HexColorField_t944280679, ___hexInputField_4)); }
	inline InputField_t3762917431 * get_hexInputField_4() const { return ___hexInputField_4; }
	inline InputField_t3762917431 ** get_address_of_hexInputField_4() { return &___hexInputField_4; }
	inline void set_hexInputField_4(InputField_t3762917431 * value)
	{
		___hexInputField_4 = value;
		Il2CppCodeGenWriteBarrier((&___hexInputField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEXCOLORFIELD_T944280679_H
#ifndef SELECTABLE_T3250028441_H
#define SELECTABLE_T3250028441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t3250028441  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t3049316579  ___m_Navigation_3;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_4;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2139031574  ___m_Colors_5;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1362986479  ___m_SpriteState_6;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t2532145056 * ___m_AnimationTriggers_7;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_8;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t1660335611 * ___m_TargetGraphic_9;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_10;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_11;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_12;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_13;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_14;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t1260619206 * ___m_CanvasGroupCache_15;

public:
	inline static int32_t get_offset_of_m_Navigation_3() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Navigation_3)); }
	inline Navigation_t3049316579  get_m_Navigation_3() const { return ___m_Navigation_3; }
	inline Navigation_t3049316579 * get_address_of_m_Navigation_3() { return &___m_Navigation_3; }
	inline void set_m_Navigation_3(Navigation_t3049316579  value)
	{
		___m_Navigation_3 = value;
	}

	inline static int32_t get_offset_of_m_Transition_4() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Transition_4)); }
	inline int32_t get_m_Transition_4() const { return ___m_Transition_4; }
	inline int32_t* get_address_of_m_Transition_4() { return &___m_Transition_4; }
	inline void set_m_Transition_4(int32_t value)
	{
		___m_Transition_4 = value;
	}

	inline static int32_t get_offset_of_m_Colors_5() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Colors_5)); }
	inline ColorBlock_t2139031574  get_m_Colors_5() const { return ___m_Colors_5; }
	inline ColorBlock_t2139031574 * get_address_of_m_Colors_5() { return &___m_Colors_5; }
	inline void set_m_Colors_5(ColorBlock_t2139031574  value)
	{
		___m_Colors_5 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_6() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_SpriteState_6)); }
	inline SpriteState_t1362986479  get_m_SpriteState_6() const { return ___m_SpriteState_6; }
	inline SpriteState_t1362986479 * get_address_of_m_SpriteState_6() { return &___m_SpriteState_6; }
	inline void set_m_SpriteState_6(SpriteState_t1362986479  value)
	{
		___m_SpriteState_6 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_7() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_AnimationTriggers_7)); }
	inline AnimationTriggers_t2532145056 * get_m_AnimationTriggers_7() const { return ___m_AnimationTriggers_7; }
	inline AnimationTriggers_t2532145056 ** get_address_of_m_AnimationTriggers_7() { return &___m_AnimationTriggers_7; }
	inline void set_m_AnimationTriggers_7(AnimationTriggers_t2532145056 * value)
	{
		___m_AnimationTriggers_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_7), value);
	}

	inline static int32_t get_offset_of_m_Interactable_8() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Interactable_8)); }
	inline bool get_m_Interactable_8() const { return ___m_Interactable_8; }
	inline bool* get_address_of_m_Interactable_8() { return &___m_Interactable_8; }
	inline void set_m_Interactable_8(bool value)
	{
		___m_Interactable_8 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_9() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_TargetGraphic_9)); }
	inline Graphic_t1660335611 * get_m_TargetGraphic_9() const { return ___m_TargetGraphic_9; }
	inline Graphic_t1660335611 ** get_address_of_m_TargetGraphic_9() { return &___m_TargetGraphic_9; }
	inline void set_m_TargetGraphic_9(Graphic_t1660335611 * value)
	{
		___m_TargetGraphic_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_9), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_10() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_GroupsAllowInteraction_10)); }
	inline bool get_m_GroupsAllowInteraction_10() const { return ___m_GroupsAllowInteraction_10; }
	inline bool* get_address_of_m_GroupsAllowInteraction_10() { return &___m_GroupsAllowInteraction_10; }
	inline void set_m_GroupsAllowInteraction_10(bool value)
	{
		___m_GroupsAllowInteraction_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_11() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CurrentSelectionState_11)); }
	inline int32_t get_m_CurrentSelectionState_11() const { return ___m_CurrentSelectionState_11; }
	inline int32_t* get_address_of_m_CurrentSelectionState_11() { return &___m_CurrentSelectionState_11; }
	inline void set_m_CurrentSelectionState_11(int32_t value)
	{
		___m_CurrentSelectionState_11 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerInsideU3Ek__BackingField_12)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_12() const { return ___U3CisPointerInsideU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_12() { return &___U3CisPointerInsideU3Ek__BackingField_12; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_12(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerDownU3Ek__BackingField_13)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_13() const { return ___U3CisPointerDownU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_13() { return &___U3CisPointerDownU3Ek__BackingField_13; }
	inline void set_U3CisPointerDownU3Ek__BackingField_13(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3ChasSelectionU3Ek__BackingField_14)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_14() const { return ___U3ChasSelectionU3Ek__BackingField_14; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_14() { return &___U3ChasSelectionU3Ek__BackingField_14; }
	inline void set_U3ChasSelectionU3Ek__BackingField_14(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_15() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CanvasGroupCache_15)); }
	inline List_1_t1260619206 * get_m_CanvasGroupCache_15() const { return ___m_CanvasGroupCache_15; }
	inline List_1_t1260619206 ** get_address_of_m_CanvasGroupCache_15() { return &___m_CanvasGroupCache_15; }
	inline void set_m_CanvasGroupCache_15(List_1_t1260619206 * value)
	{
		___m_CanvasGroupCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_15), value);
	}
};

struct Selectable_t3250028441_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t427135887 * ___s_List_2;

public:
	inline static int32_t get_offset_of_s_List_2() { return static_cast<int32_t>(offsetof(Selectable_t3250028441_StaticFields, ___s_List_2)); }
	inline List_1_t427135887 * get_s_List_2() const { return ___s_List_2; }
	inline List_1_t427135887 ** get_address_of_s_List_2() { return &___s_List_2; }
	inline void set_s_List_2(List_1_t427135887 * value)
	{
		___s_List_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T3250028441_H
#ifndef BOXSLIDER_T2380464200_H
#define BOXSLIDER_T2380464200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BoxSlider
struct  BoxSlider_t2380464200  : public Selectable_t3250028441
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.BoxSlider::m_HandleRect
	RectTransform_t3704657025 * ___m_HandleRect_16;
	// System.Single UnityEngine.UI.BoxSlider::m_MinValue
	float ___m_MinValue_17;
	// System.Single UnityEngine.UI.BoxSlider::m_MaxValue
	float ___m_MaxValue_18;
	// System.Boolean UnityEngine.UI.BoxSlider::m_WholeNumbers
	bool ___m_WholeNumbers_19;
	// System.Single UnityEngine.UI.BoxSlider::m_Value
	float ___m_Value_20;
	// System.Single UnityEngine.UI.BoxSlider::m_ValueY
	float ___m_ValueY_21;
	// UnityEngine.UI.BoxSlider/BoxSliderEvent UnityEngine.UI.BoxSlider::m_OnValueChanged
	BoxSliderEvent_t439394298 * ___m_OnValueChanged_22;
	// UnityEngine.Transform UnityEngine.UI.BoxSlider::m_HandleTransform
	Transform_t3600365921 * ___m_HandleTransform_23;
	// UnityEngine.RectTransform UnityEngine.UI.BoxSlider::m_HandleContainerRect
	RectTransform_t3704657025 * ___m_HandleContainerRect_24;
	// UnityEngine.Vector2 UnityEngine.UI.BoxSlider::m_Offset
	Vector2_t2156229523  ___m_Offset_25;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.BoxSlider::m_Tracker
	DrivenRectTransformTracker_t2562230146  ___m_Tracker_26;

public:
	inline static int32_t get_offset_of_m_HandleRect_16() { return static_cast<int32_t>(offsetof(BoxSlider_t2380464200, ___m_HandleRect_16)); }
	inline RectTransform_t3704657025 * get_m_HandleRect_16() const { return ___m_HandleRect_16; }
	inline RectTransform_t3704657025 ** get_address_of_m_HandleRect_16() { return &___m_HandleRect_16; }
	inline void set_m_HandleRect_16(RectTransform_t3704657025 * value)
	{
		___m_HandleRect_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleRect_16), value);
	}

	inline static int32_t get_offset_of_m_MinValue_17() { return static_cast<int32_t>(offsetof(BoxSlider_t2380464200, ___m_MinValue_17)); }
	inline float get_m_MinValue_17() const { return ___m_MinValue_17; }
	inline float* get_address_of_m_MinValue_17() { return &___m_MinValue_17; }
	inline void set_m_MinValue_17(float value)
	{
		___m_MinValue_17 = value;
	}

	inline static int32_t get_offset_of_m_MaxValue_18() { return static_cast<int32_t>(offsetof(BoxSlider_t2380464200, ___m_MaxValue_18)); }
	inline float get_m_MaxValue_18() const { return ___m_MaxValue_18; }
	inline float* get_address_of_m_MaxValue_18() { return &___m_MaxValue_18; }
	inline void set_m_MaxValue_18(float value)
	{
		___m_MaxValue_18 = value;
	}

	inline static int32_t get_offset_of_m_WholeNumbers_19() { return static_cast<int32_t>(offsetof(BoxSlider_t2380464200, ___m_WholeNumbers_19)); }
	inline bool get_m_WholeNumbers_19() const { return ___m_WholeNumbers_19; }
	inline bool* get_address_of_m_WholeNumbers_19() { return &___m_WholeNumbers_19; }
	inline void set_m_WholeNumbers_19(bool value)
	{
		___m_WholeNumbers_19 = value;
	}

	inline static int32_t get_offset_of_m_Value_20() { return static_cast<int32_t>(offsetof(BoxSlider_t2380464200, ___m_Value_20)); }
	inline float get_m_Value_20() const { return ___m_Value_20; }
	inline float* get_address_of_m_Value_20() { return &___m_Value_20; }
	inline void set_m_Value_20(float value)
	{
		___m_Value_20 = value;
	}

	inline static int32_t get_offset_of_m_ValueY_21() { return static_cast<int32_t>(offsetof(BoxSlider_t2380464200, ___m_ValueY_21)); }
	inline float get_m_ValueY_21() const { return ___m_ValueY_21; }
	inline float* get_address_of_m_ValueY_21() { return &___m_ValueY_21; }
	inline void set_m_ValueY_21(float value)
	{
		___m_ValueY_21 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_22() { return static_cast<int32_t>(offsetof(BoxSlider_t2380464200, ___m_OnValueChanged_22)); }
	inline BoxSliderEvent_t439394298 * get_m_OnValueChanged_22() const { return ___m_OnValueChanged_22; }
	inline BoxSliderEvent_t439394298 ** get_address_of_m_OnValueChanged_22() { return &___m_OnValueChanged_22; }
	inline void set_m_OnValueChanged_22(BoxSliderEvent_t439394298 * value)
	{
		___m_OnValueChanged_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_22), value);
	}

	inline static int32_t get_offset_of_m_HandleTransform_23() { return static_cast<int32_t>(offsetof(BoxSlider_t2380464200, ___m_HandleTransform_23)); }
	inline Transform_t3600365921 * get_m_HandleTransform_23() const { return ___m_HandleTransform_23; }
	inline Transform_t3600365921 ** get_address_of_m_HandleTransform_23() { return &___m_HandleTransform_23; }
	inline void set_m_HandleTransform_23(Transform_t3600365921 * value)
	{
		___m_HandleTransform_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleTransform_23), value);
	}

	inline static int32_t get_offset_of_m_HandleContainerRect_24() { return static_cast<int32_t>(offsetof(BoxSlider_t2380464200, ___m_HandleContainerRect_24)); }
	inline RectTransform_t3704657025 * get_m_HandleContainerRect_24() const { return ___m_HandleContainerRect_24; }
	inline RectTransform_t3704657025 ** get_address_of_m_HandleContainerRect_24() { return &___m_HandleContainerRect_24; }
	inline void set_m_HandleContainerRect_24(RectTransform_t3704657025 * value)
	{
		___m_HandleContainerRect_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleContainerRect_24), value);
	}

	inline static int32_t get_offset_of_m_Offset_25() { return static_cast<int32_t>(offsetof(BoxSlider_t2380464200, ___m_Offset_25)); }
	inline Vector2_t2156229523  get_m_Offset_25() const { return ___m_Offset_25; }
	inline Vector2_t2156229523 * get_address_of_m_Offset_25() { return &___m_Offset_25; }
	inline void set_m_Offset_25(Vector2_t2156229523  value)
	{
		___m_Offset_25 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_26() { return static_cast<int32_t>(offsetof(BoxSlider_t2380464200, ___m_Tracker_26)); }
	inline DrivenRectTransformTracker_t2562230146  get_m_Tracker_26() const { return ___m_Tracker_26; }
	inline DrivenRectTransformTracker_t2562230146 * get_address_of_m_Tracker_26() { return &___m_Tracker_26; }
	inline void set_m_Tracker_26(DrivenRectTransformTracker_t2562230146  value)
	{
		___m_Tracker_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXSLIDER_T2380464200_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (TKButtonRecognizer_t691501956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2500[5] = 
{
	TKButtonRecognizer_t691501956::get_offset_of_onSelectedEvent_10(),
	TKButtonRecognizer_t691501956::get_offset_of_onDeselectedEvent_11(),
	TKButtonRecognizer_t691501956::get_offset_of_onTouchUpInsideEvent_12(),
	TKButtonRecognizer_t691501956::get_offset_of__defaultFrame_13(),
	TKButtonRecognizer_t691501956::get_offset_of__highlightedFrame_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (TKCurveRecognizer_t1881817312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2501[11] = 
{
	TKCurveRecognizer_t1881817312::get_offset_of_gestureRecognizedEvent_10(),
	TKCurveRecognizer_t1881817312::get_offset_of_gestureCompleteEvent_11(),
	TKCurveRecognizer_t1881817312::get_offset_of_reportRotationStep_12(),
	TKCurveRecognizer_t1881817312::get_offset_of_squareDistance_13(),
	TKCurveRecognizer_t1881817312::get_offset_of_maxSharpnes_14(),
	TKCurveRecognizer_t1881817312::get_offset_of_minimumNumberOfTouches_15(),
	TKCurveRecognizer_t1881817312::get_offset_of_maximumNumberOfTouches_16(),
	TKCurveRecognizer_t1881817312::get_offset_of_deltaRotation_17(),
	TKCurveRecognizer_t1881817312::get_offset_of__previousLocation_18(),
	TKCurveRecognizer_t1881817312::get_offset_of__deltaTranslation_19(),
	TKCurveRecognizer_t1881817312::get_offset_of__previousDeltaTranslation_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (TKLongPressRecognizer_t1475862313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2502[7] = 
{
	TKLongPressRecognizer_t1475862313::get_offset_of_gestureRecognizedEvent_10(),
	TKLongPressRecognizer_t1475862313::get_offset_of_gestureCompleteEvent_11(),
	TKLongPressRecognizer_t1475862313::get_offset_of_minimumPressDuration_12(),
	TKLongPressRecognizer_t1475862313::get_offset_of_requiredTouchesCount_13(),
	TKLongPressRecognizer_t1475862313::get_offset_of_allowableMovementCm_14(),
	TKLongPressRecognizer_t1475862313::get_offset_of__beginLocation_15(),
	TKLongPressRecognizer_t1475862313::get_offset_of__waiting_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (U3CbeginGestureU3Ec__Iterator0_t696754078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2503[5] = 
{
	U3CbeginGestureU3Ec__Iterator0_t696754078::get_offset_of_U3CendTimeU3E__0_0(),
	U3CbeginGestureU3Ec__Iterator0_t696754078::get_offset_of_U24this_1(),
	U3CbeginGestureU3Ec__Iterator0_t696754078::get_offset_of_U24current_2(),
	U3CbeginGestureU3Ec__Iterator0_t696754078::get_offset_of_U24disposing_3(),
	U3CbeginGestureU3Ec__Iterator0_t696754078::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (TKOneFingerRotationRecognizer_t564097970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2504[3] = 
{
	TKOneFingerRotationRecognizer_t564097970::get_offset_of_gestureRecognizedEvent_17(),
	TKOneFingerRotationRecognizer_t564097970::get_offset_of_gestureCompleteEvent_18(),
	TKOneFingerRotationRecognizer_t564097970::get_offset_of_targetPosition_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (TKPanRecognizer_t1603940453), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2505[11] = 
{
	TKPanRecognizer_t1603940453::get_offset_of_gestureRecognizedEvent_10(),
	TKPanRecognizer_t1603940453::get_offset_of_gestureCompleteEvent_11(),
	TKPanRecognizer_t1603940453::get_offset_of_deltaTranslation_12(),
	TKPanRecognizer_t1603940453::get_offset_of_deltaTranslationCm_13(),
	TKPanRecognizer_t1603940453::get_offset_of_minimumNumberOfTouches_14(),
	TKPanRecognizer_t1603940453::get_offset_of_maximumNumberOfTouches_15(),
	TKPanRecognizer_t1603940453::get_offset_of_totalDeltaMovementInCm_16(),
	TKPanRecognizer_t1603940453::get_offset_of__previousLocation_17(),
	TKPanRecognizer_t1603940453::get_offset_of__minDistanceToPanCm_18(),
	TKPanRecognizer_t1603940453::get_offset_of__startPoint_19(),
	TKPanRecognizer_t1603940453::get_offset_of__endPoint_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (TKPinchRecognizer_t788645703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2506[7] = 
{
	TKPinchRecognizer_t788645703::get_offset_of_gestureRecognizedEvent_10(),
	TKPinchRecognizer_t788645703::get_offset_of_gestureCompleteEvent_11(),
	TKPinchRecognizer_t788645703::get_offset_of_minimumScaleDistanceToRecognize_12(),
	TKPinchRecognizer_t788645703::get_offset_of_deltaScale_13(),
	TKPinchRecognizer_t788645703::get_offset_of__intialDistance_14(),
	TKPinchRecognizer_t788645703::get_offset_of__firstDistance_15(),
	TKPinchRecognizer_t788645703::get_offset_of__previousDistance_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (TKRotationRecognizer_t651141287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2507[7] = 
{
	TKRotationRecognizer_t651141287::get_offset_of_gestureRecognizedEvent_10(),
	TKRotationRecognizer_t651141287::get_offset_of_gestureCompleteEvent_11(),
	TKRotationRecognizer_t651141287::get_offset_of_deltaRotation_12(),
	TKRotationRecognizer_t651141287::get_offset_of_minimumRotationToRecognize_13(),
	TKRotationRecognizer_t651141287::get_offset_of__previousRotation_14(),
	TKRotationRecognizer_t651141287::get_offset_of__firstRotation_15(),
	TKRotationRecognizer_t651141287::get_offset_of__initialRotation_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (TKSwipeDirection_t58737069)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2508[22] = 
{
	TKSwipeDirection_t58737069::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (TKSwipeRecognizer_t3210539170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2509[11] = 
{
	TKSwipeRecognizer_t3210539170::get_offset_of_gestureRecognizedEvent_10(),
	TKSwipeRecognizer_t3210539170::get_offset_of_timeToSwipe_11(),
	TKSwipeRecognizer_t3210539170::get_offset_of_U3CswipeVelocityU3Ek__BackingField_12(),
	TKSwipeRecognizer_t3210539170::get_offset_of_U3CcompletedSwipeDirectionU3Ek__BackingField_13(),
	TKSwipeRecognizer_t3210539170::get_offset_of_minimumNumberOfTouches_14(),
	TKSwipeRecognizer_t3210539170::get_offset_of_maximumNumberOfTouches_15(),
	TKSwipeRecognizer_t3210539170::get_offset_of_triggerWhenCriteriaMet_16(),
	TKSwipeRecognizer_t3210539170::get_offset_of__minimumDistance_17(),
	TKSwipeRecognizer_t3210539170::get_offset_of__maximumDistance_18(),
	TKSwipeRecognizer_t3210539170::get_offset_of__points_19(),
	TKSwipeRecognizer_t3210539170::get_offset_of__startTime_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (TKTapRecognizer_t2390504451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2510[7] = 
{
	TKTapRecognizer_t2390504451::get_offset_of_gestureRecognizedEvent_10(),
	TKTapRecognizer_t2390504451::get_offset_of_numberOfTapsRequired_11(),
	TKTapRecognizer_t2390504451::get_offset_of_numberOfTouchesRequired_12(),
	TKTapRecognizer_t2390504451::get_offset_of__maxDurationForTapConsideration_13(),
	TKTapRecognizer_t2390504451::get_offset_of__maxDeltaMovementForTapConsideration_14(),
	TKTapRecognizer_t2390504451::get_offset_of__touchBeganTime_15(),
	TKTapRecognizer_t2390504451::get_offset_of__preformedTapsCount_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (TKTouchPadRecognizer_t1493779134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2511[4] = 
{
	TKTouchPadRecognizer_t1493779134::get_offset_of_gestureRecognizedEvent_10(),
	TKTouchPadRecognizer_t1493779134::get_offset_of_gestureCompleteEvent_11(),
	TKTouchPadRecognizer_t1493779134::get_offset_of_inputCurve_12(),
	TKTouchPadRecognizer_t1493779134::get_offset_of_value_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (TouchKit_t3280788656), -1, sizeof(TouchKit_t3280788656_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2512[17] = 
{
	TouchKit_t3280788656::get_offset_of_simulateTouches_2(),
	TouchKit_t3280788656::get_offset_of_simulateMultitouch_3(),
	TouchKit_t3280788656::get_offset_of_drawTouches_4(),
	TouchKit_t3280788656::get_offset_of_drawDebugBoundaryFrames_5(),
	TouchKit_t3280788656::get_offset_of_autoScaleRectsAndDistances_6(),
	TouchKit_t3280788656::get_offset_of_shouldAutoUpdateTouches_7(),
	TouchKit_t3280788656::get_offset_of__designTimeResolution_8(),
	TouchKit_t3280788656::get_offset_of_maxTouchesToProcess_9(),
	TouchKit_t3280788656::get_offset_of_U3CruntimeScaleModifierU3Ek__BackingField_10(),
	TouchKit_t3280788656::get_offset_of_U3CruntimeDistanceModifierU3Ek__BackingField_11(),
	TouchKit_t3280788656::get_offset_of_U3CpixelsToUnityUnitsMultiplierU3Ek__BackingField_12(),
	TouchKit_t3280788656::get_offset_of__gestureRecognizers_13(),
	TouchKit_t3280788656::get_offset_of__touchCache_14(),
	TouchKit_t3280788656::get_offset_of__liveTouches_15(),
	TouchKit_t3280788656::get_offset_of__shouldCheckForLostTouches_16(),
	0,
	TouchKit_t3280788656_StaticFields::get_offset_of__instance_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (DemoOne_t3048645529), -1, sizeof(DemoOne_t3048645529_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2513[18] = 
{
	DemoOne_t3048645529::get_offset_of_cube_2(),
	DemoOne_t3048645529::get_offset_of__scrollPosition_3(),
	DemoOne_t3048645529::get_offset_of_touchPadInputCurve_4(),
	DemoOne_t3048645529_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
	DemoOne_t3048645529_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_6(),
	DemoOne_t3048645529_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_7(),
	DemoOne_t3048645529_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_8(),
	DemoOne_t3048645529_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_9(),
	DemoOne_t3048645529_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_10(),
	DemoOne_t3048645529_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_11(),
	DemoOne_t3048645529_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_12(),
	DemoOne_t3048645529_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_13(),
	DemoOne_t3048645529_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_14(),
	DemoOne_t3048645529_StaticFields::get_offset_of_U3CU3Ef__amU24cacheA_15(),
	DemoOne_t3048645529_StaticFields::get_offset_of_U3CU3Ef__amU24cacheB_16(),
	DemoOne_t3048645529_StaticFields::get_offset_of_U3CU3Ef__amU24cacheC_17(),
	DemoOne_t3048645529_StaticFields::get_offset_of_U3CU3Ef__amU24cacheD_18(),
	DemoOne_t3048645529_StaticFields::get_offset_of_U3CU3Ef__amU24cacheE_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (U3COnGUIU3Ec__AnonStorey0_t592312657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2514[1] = 
{
	U3COnGUIU3Ec__AnonStorey0_t592312657::get_offset_of_recognizer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (U3COnGUIU3Ec__AnonStorey1_t592312656), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2515[2] = 
{
	U3COnGUIU3Ec__AnonStorey1_t592312656::get_offset_of_recognizer_0(),
	U3COnGUIU3Ec__AnonStorey1_t592312656::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (U3COnGUIU3Ec__AnonStorey2_t592312655), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2516[2] = 
{
	U3COnGUIU3Ec__AnonStorey2_t592312655::get_offset_of_recognizer_0(),
	U3COnGUIU3Ec__AnonStorey2_t592312655::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (U3COnGUIU3Ec__AnonStorey3_t592312654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2517[2] = 
{
	U3COnGUIU3Ec__AnonStorey3_t592312654::get_offset_of_recognizer_0(),
	U3COnGUIU3Ec__AnonStorey3_t592312654::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (U3COnGUIU3Ec__AnonStorey4_t592312661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2518[2] = 
{
	U3COnGUIU3Ec__AnonStorey4_t592312661::get_offset_of_recognizer_0(),
	U3COnGUIU3Ec__AnonStorey4_t592312661::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (DemoTwo_t1529025922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2519[1] = 
{
	DemoTwo_t1529025922::get_offset_of__controls_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (VirtualControls_t3611233992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2520[12] = 
{
	VirtualControls_t3611233992::get_offset_of_leftDown_0(),
	VirtualControls_t3611233992::get_offset_of_upDown_1(),
	VirtualControls_t3611233992::get_offset_of_rightDown_2(),
	VirtualControls_t3611233992::get_offset_of_attackDown_3(),
	VirtualControls_t3611233992::get_offset_of_jumpDown_4(),
	VirtualControls_t3611233992::get_offset_of_buttonWidth_5(),
	VirtualControls_t3611233992::get_offset_of_buttonHeight_6(),
	VirtualControls_t3611233992::get_offset_of__leftRecognizer_7(),
	VirtualControls_t3611233992::get_offset_of__rightRecognizer_8(),
	VirtualControls_t3611233992::get_offset_of__upRecognizer_9(),
	VirtualControls_t3611233992::get_offset_of__attackRecognizer_10(),
	VirtualControls_t3611233992::get_offset_of__jumpRecognizer_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (SwipeDirection_t3465465729)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2521[8] = 
{
	SwipeDirection_t3465465729::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (TKLSwipeDetector_t1290972488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2522[11] = 
{
	TKLSwipeDetector_t1290972488::get_offset_of_onSwipeDeteced_2(),
	TKLSwipeDetector_t1290972488::get_offset_of_timeToSwipe_3(),
	TKLSwipeDetector_t1290972488::get_offset_of_swipeVelocity_4(),
	TKLSwipeDetector_t1290972488::get_offset_of_completedSwipeDirection_5(),
	TKLSwipeDetector_t1290972488::get_offset_of__minimumDistance_6(),
	TKLSwipeDetector_t1290972488::get_offset_of__allowedVariance_7(),
	TKLSwipeDetector_t1290972488::get_offset_of__swipesToDetect_8(),
	TKLSwipeDetector_t1290972488::get_offset_of__startPoint_9(),
	TKLSwipeDetector_t1290972488::get_offset_of__startTime_10(),
	TKLSwipeDetector_t1290972488::get_offset_of__swipeDetectionState_11(),
	TKLSwipeDetector_t1290972488::get_offset_of__didCompleteDetection_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (TKLTouch_t3484790318), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2523[6] = 
{
	TKLTouch_t3484790318::get_offset_of_fingerId_0(),
	TKLTouch_t3484790318::get_offset_of_position_1(),
	TKLTouch_t3484790318::get_offset_of_deltaPosition_2(),
	TKLTouch_t3484790318::get_offset_of_deltaTime_3(),
	TKLTouch_t3484790318::get_offset_of_tapCount_4(),
	TKLTouch_t3484790318::get_offset_of_phase_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (TouchKitLite_t1167724046), -1, sizeof(TouchKitLite_t1167724046_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2524[6] = 
{
	TouchKitLite_t1167724046::get_offset_of_shouldProcessTouches_2(),
	0,
	TouchKitLite_t1167724046::get_offset_of_liveTouches_4(),
	TouchKitLite_t1167724046::get_offset_of__touchCache_5(),
	0,
	TouchKitLite_t1167724046_StaticFields::get_offset_of__instance_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (ConnectionMessageIds_t1387126779), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (SubMessageIds_t1008824323), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (ConnectToEditor_t595742893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2527[4] = 
{
	ConnectToEditor_t595742893::get_offset_of_playerConnection_2(),
	ConnectToEditor_t595742893::get_offset_of_m_session_3(),
	ConnectToEditor_t595742893::get_offset_of_editorID_4(),
	ConnectToEditor_t595742893::get_offset_of_frameBufferTex_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (EditorHitTest_t1253817588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2528[3] = 
{
	EditorHitTest_t1253817588::get_offset_of_m_HitTransform_2(),
	EditorHitTest_t1253817588::get_offset_of_maxRayDistance_3(),
	EditorHitTest_t1253817588::get_offset_of_collisionLayerMask_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (ObjectSerializationExtension_t1046383205), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (SerializableVector4_t1862640084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2530[4] = 
{
	SerializableVector4_t1862640084::get_offset_of_x_0(),
	SerializableVector4_t1862640084::get_offset_of_y_1(),
	SerializableVector4_t1862640084::get_offset_of_z_2(),
	SerializableVector4_t1862640084::get_offset_of_w_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (serializableUnityARMatrix4x4_t78255337), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2531[4] = 
{
	serializableUnityARMatrix4x4_t78255337::get_offset_of_column0_0(),
	serializableUnityARMatrix4x4_t78255337::get_offset_of_column1_1(),
	serializableUnityARMatrix4x4_t78255337::get_offset_of_column2_2(),
	serializableUnityARMatrix4x4_t78255337::get_offset_of_column3_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (serializableSHC_t2667429767), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2532[1] = 
{
	serializableSHC_t2667429767::get_offset_of_shcData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (serializableUnityARLightData_t3935513283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2533[5] = 
{
	serializableUnityARLightData_t3935513283::get_offset_of_whichLight_0(),
	serializableUnityARLightData_t3935513283::get_offset_of_lightSHC_1(),
	serializableUnityARLightData_t3935513283::get_offset_of_primaryLightDirAndIntensity_2(),
	serializableUnityARLightData_t3935513283::get_offset_of_ambientIntensity_3(),
	serializableUnityARLightData_t3935513283::get_offset_of_ambientColorTemperature_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (serializableUnityARCamera_t4158151215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2534[8] = 
{
	serializableUnityARCamera_t4158151215::get_offset_of_worldTransform_0(),
	serializableUnityARCamera_t4158151215::get_offset_of_projectionMatrix_1(),
	serializableUnityARCamera_t4158151215::get_offset_of_trackingState_2(),
	serializableUnityARCamera_t4158151215::get_offset_of_trackingReason_3(),
	serializableUnityARCamera_t4158151215::get_offset_of_videoParams_4(),
	serializableUnityARCamera_t4158151215::get_offset_of_lightData_5(),
	serializableUnityARCamera_t4158151215::get_offset_of_pointCloud_6(),
	serializableUnityARCamera_t4158151215::get_offset_of_displayTransform_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (serializableUnityARPlaneAnchor_t1446774435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2535[5] = 
{
	serializableUnityARPlaneAnchor_t1446774435::get_offset_of_worldTransform_0(),
	serializableUnityARPlaneAnchor_t1446774435::get_offset_of_center_1(),
	serializableUnityARPlaneAnchor_t1446774435::get_offset_of_extent_2(),
	serializableUnityARPlaneAnchor_t1446774435::get_offset_of_planeAlignment_3(),
	serializableUnityARPlaneAnchor_t1446774435::get_offset_of_identifierStr_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (serializableFaceGeometry_t157334219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2536[3] = 
{
	serializableFaceGeometry_t157334219::get_offset_of_vertices_0(),
	serializableFaceGeometry_t157334219::get_offset_of_texCoords_1(),
	serializableFaceGeometry_t157334219::get_offset_of_triIndices_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (serializableUnityARFaceAnchor_t2162490026), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2537[4] = 
{
	serializableUnityARFaceAnchor_t2162490026::get_offset_of_worldTransform_0(),
	serializableUnityARFaceAnchor_t2162490026::get_offset_of_faceGeometry_1(),
	serializableUnityARFaceAnchor_t2162490026::get_offset_of_arBlendShapes_2(),
	serializableUnityARFaceAnchor_t2162490026::get_offset_of_identifierStr_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (serializablePointCloud_t455238287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2538[1] = 
{
	serializablePointCloud_t455238287::get_offset_of_pointCloudData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (serializableARSessionConfiguration_t1467016906), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2539[4] = 
{
	serializableARSessionConfiguration_t1467016906::get_offset_of_alignment_0(),
	serializableARSessionConfiguration_t1467016906::get_offset_of_planeDetection_1(),
	serializableARSessionConfiguration_t1467016906::get_offset_of_getPointCloudData_2(),
	serializableARSessionConfiguration_t1467016906::get_offset_of_enableLightEstimation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (serializableARKitInit_t3885066048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2540[2] = 
{
	serializableARKitInit_t3885066048::get_offset_of_config_0(),
	serializableARKitInit_t3885066048::get_offset_of_runOption_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (serializableFromEditorMessage_t3245497382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2541[2] = 
{
	serializableFromEditorMessage_t3245497382::get_offset_of_subMessageId_0(),
	serializableFromEditorMessage_t3245497382::get_offset_of_arkitConfigMsg_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (UnityRemoteVideo_t705138647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2542[10] = 
{
	UnityRemoteVideo_t705138647::get_offset_of_connectToEditor_2(),
	UnityRemoteVideo_t705138647::get_offset_of_m_Session_3(),
	UnityRemoteVideo_t705138647::get_offset_of_bTexturesInitialized_4(),
	UnityRemoteVideo_t705138647::get_offset_of_currentFrameIndex_5(),
	UnityRemoteVideo_t705138647::get_offset_of_m_textureYBytes_6(),
	UnityRemoteVideo_t705138647::get_offset_of_m_textureUVBytes_7(),
	UnityRemoteVideo_t705138647::get_offset_of_m_textureYBytes2_8(),
	UnityRemoteVideo_t705138647::get_offset_of_m_textureUVBytes2_9(),
	UnityRemoteVideo_t705138647::get_offset_of_m_pinnedYArray_10(),
	UnityRemoteVideo_t705138647::get_offset_of_m_pinnedUVArray_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (UnityARUserAnchorExample_t2657819511), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2543[4] = 
{
	UnityARUserAnchorExample_t2657819511::get_offset_of_prefabObject_2(),
	UnityARUserAnchorExample_t2657819511::get_offset_of_distanceFromCamera_3(),
	UnityARUserAnchorExample_t2657819511::get_offset_of_m_Clones_4(),
	UnityARUserAnchorExample_t2657819511::get_offset_of_m_TimeUntilRemove_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (ARCameraTracker_t1108422940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2544[2] = 
{
	ARCameraTracker_t1108422940::get_offset_of_trackedCamera_2(),
	ARCameraTracker_t1108422940::get_offset_of_sessionStarted_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (BlendshapeDriver_t961242622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2545[2] = 
{
	BlendshapeDriver_t961242622::get_offset_of_skinnedMeshRenderer_2(),
	BlendshapeDriver_t961242622::get_offset_of_currentBlendShapes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (BlendshapePrinter_t4276887874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2546[2] = 
{
	BlendshapePrinter_t4276887874::get_offset_of_shapeEnabled_2(),
	BlendshapePrinter_t4276887874::get_offset_of_currentBlendShapes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (UnityARFaceAnchorManager_t1630882107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2547[2] = 
{
	UnityARFaceAnchorManager_t1630882107::get_offset_of_anchorPrefab_2(),
	UnityARFaceAnchorManager_t1630882107::get_offset_of_m_session_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (UnityARFaceMeshManager_t3766034196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2548[3] = 
{
	UnityARFaceMeshManager_t3766034196::get_offset_of_meshFilter_2(),
	UnityARFaceMeshManager_t3766034196::get_offset_of_m_session_3(),
	UnityARFaceMeshManager_t3766034196::get_offset_of_faceMesh_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (FocusSquare_t2880014214), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2549[7] = 
{
	FocusSquare_t2880014214::get_offset_of_findingSquare_2(),
	FocusSquare_t2880014214::get_offset_of_foundSquare_3(),
	FocusSquare_t2880014214::get_offset_of_maxRayDistance_4(),
	FocusSquare_t2880014214::get_offset_of_collisionLayerMask_5(),
	FocusSquare_t2880014214::get_offset_of_findingSquareDist_6(),
	FocusSquare_t2880014214::get_offset_of_squareState_7(),
	FocusSquare_t2880014214::get_offset_of_trackingInitialized_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (FocusState_t138798281)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2550[4] = 
{
	FocusState_t138798281::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { sizeof (BallMaker_t4057675501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2551[3] = 
{
	BallMaker_t4057675501::get_offset_of_ballPrefab_2(),
	BallMaker_t4057675501::get_offset_of_createHeight_3(),
	BallMaker_t4057675501::get_offset_of_props_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (BallMover_t2920303374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2552[2] = 
{
	BallMover_t2920303374::get_offset_of_collBallPrefab_2(),
	BallMover_t2920303374::get_offset_of_collBallGO_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (Ballz_t1012779874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2553[2] = 
{
	Ballz_t1012779874::get_offset_of_yDistanceThreshold_2(),
	Ballz_t1012779874::get_offset_of_startingY_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (ModeSwitcher_t3643344453), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2554[3] = 
{
	ModeSwitcher_t3643344453::get_offset_of_ballMake_2(),
	ModeSwitcher_t3643344453::get_offset_of_ballMove_3(),
	ModeSwitcher_t3643344453::get_offset_of_appMode_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (ColorValues_t1603089408)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2555[8] = 
{
	ColorValues_t1603089408::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { sizeof (ColorChangedEvent_t3019780707), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (HSVChangedEvent_t911780251), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (ColorPickerTester_t3074432426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2558[2] = 
{
	ColorPickerTester_t3074432426::get_offset_of_renderer_2(),
	ColorPickerTester_t3074432426::get_offset_of_picker_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (TiltWindow_t335293945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2559[4] = 
{
	TiltWindow_t335293945::get_offset_of_range_2(),
	TiltWindow_t335293945::get_offset_of_mTrans_3(),
	TiltWindow_t335293945::get_offset_of_mStart_4(),
	TiltWindow_t335293945::get_offset_of_mRot_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (ColorImage_t1922452376), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2560[2] = 
{
	ColorImage_t1922452376::get_offset_of_picker_2(),
	ColorImage_t1922452376::get_offset_of_image_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (ColorLabel_t2272707290), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2561[7] = 
{
	ColorLabel_t2272707290::get_offset_of_picker_2(),
	ColorLabel_t2272707290::get_offset_of_type_3(),
	ColorLabel_t2272707290::get_offset_of_prefix_4(),
	ColorLabel_t2272707290::get_offset_of_minValue_5(),
	ColorLabel_t2272707290::get_offset_of_maxValue_6(),
	ColorLabel_t2272707290::get_offset_of_precision_7(),
	ColorLabel_t2272707290::get_offset_of_label_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (ColorPicker_t228004619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2562[9] = 
{
	ColorPicker_t228004619::get_offset_of__hue_2(),
	ColorPicker_t228004619::get_offset_of__saturation_3(),
	ColorPicker_t228004619::get_offset_of__brightness_4(),
	ColorPicker_t228004619::get_offset_of__red_5(),
	ColorPicker_t228004619::get_offset_of__green_6(),
	ColorPicker_t228004619::get_offset_of__blue_7(),
	ColorPicker_t228004619::get_offset_of__alpha_8(),
	ColorPicker_t228004619::get_offset_of_onValueChanged_9(),
	ColorPicker_t228004619::get_offset_of_onHSVChanged_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (ColorPresets_t2117877396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2563[3] = 
{
	ColorPresets_t2117877396::get_offset_of_picker_2(),
	ColorPresets_t2117877396::get_offset_of_presets_3(),
	ColorPresets_t2117877396::get_offset_of_createPresetImage_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (ColorSlider_t2624382019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2564[4] = 
{
	ColorSlider_t2624382019::get_offset_of_hsvpicker_2(),
	ColorSlider_t2624382019::get_offset_of_type_3(),
	ColorSlider_t2624382019::get_offset_of_slider_4(),
	ColorSlider_t2624382019::get_offset_of_listen_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (ColorSliderImage_t1393030097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2565[4] = 
{
	ColorSliderImage_t1393030097::get_offset_of_picker_2(),
	ColorSliderImage_t1393030097::get_offset_of_type_3(),
	ColorSliderImage_t1393030097::get_offset_of_direction_4(),
	ColorSliderImage_t1393030097::get_offset_of_image_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (HexColorField_t944280679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2566[4] = 
{
	HexColorField_t944280679::get_offset_of_hsvpicker_2(),
	HexColorField_t944280679::get_offset_of_displayAlpha_3(),
	HexColorField_t944280679::get_offset_of_hexInputField_4(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (SVBoxSlider_t4192470748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2567[5] = 
{
	SVBoxSlider_t4192470748::get_offset_of_picker_2(),
	SVBoxSlider_t4192470748::get_offset_of_slider_3(),
	SVBoxSlider_t4192470748::get_offset_of_image_4(),
	SVBoxSlider_t4192470748::get_offset_of_lastH_5(),
	SVBoxSlider_t4192470748::get_offset_of_listen_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (BoxSlider_t2380464200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2568[11] = 
{
	BoxSlider_t2380464200::get_offset_of_m_HandleRect_16(),
	BoxSlider_t2380464200::get_offset_of_m_MinValue_17(),
	BoxSlider_t2380464200::get_offset_of_m_MaxValue_18(),
	BoxSlider_t2380464200::get_offset_of_m_WholeNumbers_19(),
	BoxSlider_t2380464200::get_offset_of_m_Value_20(),
	BoxSlider_t2380464200::get_offset_of_m_ValueY_21(),
	BoxSlider_t2380464200::get_offset_of_m_OnValueChanged_22(),
	BoxSlider_t2380464200::get_offset_of_m_HandleTransform_23(),
	BoxSlider_t2380464200::get_offset_of_m_HandleContainerRect_24(),
	BoxSlider_t2380464200::get_offset_of_m_Offset_25(),
	BoxSlider_t2380464200::get_offset_of_m_Tracker_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { sizeof (Direction_t524882829)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2569[5] = 
{
	Direction_t524882829::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (BoxSliderEvent_t439394298), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (Axis_t1354568546)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2571[3] = 
{
	Axis_t1354568546::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (HSVUtil_t1472193074), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (HsvColor_t2280895388)+ sizeof (RuntimeObject), sizeof(HsvColor_t2280895388 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2573[3] = 
{
	HsvColor_t2280895388::get_offset_of_H_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HsvColor_t2280895388::get_offset_of_S_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HsvColor_t2280895388::get_offset_of_V_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (ParticlePainter_t1984011264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2574[14] = 
{
	ParticlePainter_t1984011264::get_offset_of_painterParticlePrefab_2(),
	ParticlePainter_t1984011264::get_offset_of_minDistanceThreshold_3(),
	ParticlePainter_t1984011264::get_offset_of_maxDistanceThreshold_4(),
	ParticlePainter_t1984011264::get_offset_of_frameUpdated_5(),
	ParticlePainter_t1984011264::get_offset_of_particleSize_6(),
	ParticlePainter_t1984011264::get_offset_of_penDistance_7(),
	ParticlePainter_t1984011264::get_offset_of_colorPicker_8(),
	ParticlePainter_t1984011264::get_offset_of_currentPS_9(),
	ParticlePainter_t1984011264::get_offset_of_particles_10(),
	ParticlePainter_t1984011264::get_offset_of_previousPosition_11(),
	ParticlePainter_t1984011264::get_offset_of_currentPaintVertices_12(),
	ParticlePainter_t1984011264::get_offset_of_currentColor_13(),
	ParticlePainter_t1984011264::get_offset_of_paintSystems_14(),
	ParticlePainter_t1984011264::get_offset_of_paintMode_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (AR3DOFCameraManager_t1160001149), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2575[3] = 
{
	AR3DOFCameraManager_t1160001149::get_offset_of_m_camera_2(),
	AR3DOFCameraManager_t1160001149::get_offset_of_m_session_3(),
	AR3DOFCameraManager_t1160001149::get_offset_of_savedClearMaterial_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (ARPlaneAnchorGameObject_t1947719815), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2576[2] = 
{
	ARPlaneAnchorGameObject_t1947719815::get_offset_of_gameObject_0(),
	ARPlaneAnchorGameObject_t1947719815::get_offset_of_planeAnchor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (DontDestroyOnLoad_t1456007215), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (PointCloudParticleExample_t182386800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2578[8] = 
{
	PointCloudParticleExample_t182386800::get_offset_of_pointCloudParticlePrefab_2(),
	PointCloudParticleExample_t182386800::get_offset_of_maxPointsToShow_3(),
	PointCloudParticleExample_t182386800::get_offset_of_particleSize_4(),
	PointCloudParticleExample_t182386800::get_offset_of_m_PointCloudData_5(),
	PointCloudParticleExample_t182386800::get_offset_of_frameUpdated_6(),
	PointCloudParticleExample_t182386800::get_offset_of_currentPS_7(),
	PointCloudParticleExample_t182386800::get_offset_of_particles_8(),
	PointCloudParticleExample_t182386800::get_offset_of_isStop_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (UnityARAmbient_t2710679068), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2579[1] = 
{
	UnityARAmbient_t2710679068::get_offset_of_l_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (UnityARAnchorManager_t1557554123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2580[1] = 
{
	UnityARAnchorManager_t1557554123::get_offset_of_planeAnchorMap_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (UnityARCameraManager_t4002280589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2581[7] = 
{
	UnityARCameraManager_t4002280589::get_offset_of_m_camera_2(),
	UnityARCameraManager_t4002280589::get_offset_of_m_session_3(),
	UnityARCameraManager_t4002280589::get_offset_of_savedClearMaterial_4(),
	UnityARCameraManager_t4002280589::get_offset_of_startAlignment_5(),
	UnityARCameraManager_t4002280589::get_offset_of_planeDetection_6(),
	UnityARCameraManager_t4002280589::get_offset_of_getPointCloud_7(),
	UnityARCameraManager_t4002280589::get_offset_of_enableLightEstimation_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (UnityARCameraNearFar_t982368306), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2582[3] = 
{
	UnityARCameraNearFar_t982368306::get_offset_of_attachedCamera_2(),
	UnityARCameraNearFar_t982368306::get_offset_of_currentNearZ_3(),
	UnityARCameraNearFar_t982368306::get_offset_of_currentFarZ_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (UnityARGeneratePlane_t272564669), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2583[2] = 
{
	UnityARGeneratePlane_t272564669::get_offset_of_planePrefab_2(),
	UnityARGeneratePlane_t272564669::get_offset_of_unityARAnchorManager_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (UnityARHitTestExample_t457226377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2584[6] = 
{
	UnityARHitTestExample_t457226377::get_offset_of_planevisualizationmanager_2(),
	UnityARHitTestExample_t457226377::get_offset_of_m_HitTransform_3(),
	UnityARHitTestExample_t457226377::get_offset_of_maxRayDistance_4(),
	UnityARHitTestExample_t457226377::get_offset_of_collisionLayer_5(),
	UnityARHitTestExample_t457226377::get_offset_of_m_ScreenPosition_6(),
	UnityARHitTestExample_t457226377::get_offset_of_point_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (UnityARKitControl_t1358211756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2585[6] = 
{
	UnityARKitControl_t1358211756::get_offset_of_runOptions_2(),
	UnityARKitControl_t1358211756::get_offset_of_alignmentOptions_3(),
	UnityARKitControl_t1358211756::get_offset_of_planeOptions_4(),
	UnityARKitControl_t1358211756::get_offset_of_currentOptionIndex_5(),
	UnityARKitControl_t1358211756::get_offset_of_currentAlignmentIndex_6(),
	UnityARKitControl_t1358211756::get_offset_of_currentPlaneIndex_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (UnityARKitLightManager_t380315540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2586[2] = 
{
	UnityARKitLightManager_t380315540::get_offset_of_lightsInScene_2(),
	UnityARKitLightManager_t380315540::get_offset_of_shl_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (UnityARMatrixOps_t2790111267), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (UnityARUserAnchorComponent_t969893952), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2588[1] = 
{
	UnityARUserAnchorComponent_t969893952::get_offset_of_m_AnchorId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (UnityARUtility_t2509807446), -1, sizeof(UnityARUtility_t2509807446_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2589[4] = 
{
	UnityARUtility_t2509807446::get_offset_of_meshCollider_0(),
	UnityARUtility_t2509807446::get_offset_of_meshFilter_1(),
	UnityARUtility_t2509807446_StaticFields::get_offset_of_planePrefab_2(),
	UnityARUtility_t2509807446_StaticFields::get_offset_of_planes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (UnityARVideo_t1146951207), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2590[6] = 
{
	UnityARVideo_t1146951207::get_offset_of_m_ClearMaterial_2(),
	UnityARVideo_t1146951207::get_offset_of_m_VideoCommandBuffer_3(),
	UnityARVideo_t1146951207::get_offset_of__videoTextureY_4(),
	UnityARVideo_t1146951207::get_offset_of__videoTextureCbCr_5(),
	UnityARVideo_t1146951207::get_offset_of__displayTransform_6(),
	UnityARVideo_t1146951207::get_offset_of_bCommandBufferInitialized_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (UnityPointCloudExample_t3649008995), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2591[4] = 
{
	UnityPointCloudExample_t3649008995::get_offset_of_numPointsToShow_2(),
	UnityPointCloudExample_t3649008995::get_offset_of_PointCloudPrefab_3(),
	UnityPointCloudExample_t3649008995::get_offset_of_pointCloudObjects_4(),
	UnityPointCloudExample_t3649008995::get_offset_of_m_PointCloudData_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (ARAnchor_t362826948)+ sizeof (RuntimeObject), sizeof(ARAnchor_t362826948_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2592[2] = 
{
	ARAnchor_t362826948::get_offset_of_identifier_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARAnchor_t362826948::get_offset_of_transform_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (ARCamera_t2831687281)+ sizeof (RuntimeObject), sizeof(ARCamera_t2831687281 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2593[7] = 
{
	ARCamera_t2831687281::get_offset_of_worldTransform_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARCamera_t2831687281::get_offset_of_eulerAngles_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARCamera_t2831687281::get_offset_of_trackingQuality_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARCamera_t2831687281::get_offset_of_intrinsics_row1_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARCamera_t2831687281::get_offset_of_intrinsics_row2_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARCamera_t2831687281::get_offset_of_intrinsics_row3_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARCamera_t2831687281::get_offset_of_imageResolution_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (ARErrorCode_t1180871917)+ sizeof (RuntimeObject), sizeof(int64_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2594[5] = 
{
	ARErrorCode_t1180871917::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (ARBlendShapeLocation_t2653069299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2595[51] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (UnityARFaceGeometry_t4178775532)+ sizeof (RuntimeObject), sizeof(UnityARFaceGeometry_t4178775532 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2596[6] = 
{
	UnityARFaceGeometry_t4178775532::get_offset_of_vertexCount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARFaceGeometry_t4178775532::get_offset_of_vertices_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARFaceGeometry_t4178775532::get_offset_of_textureCoordinateCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARFaceGeometry_t4178775532::get_offset_of_textureCoordinates_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARFaceGeometry_t4178775532::get_offset_of_triangleCount_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARFaceGeometry_t4178775532::get_offset_of_triangleIndices_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (UnityARFaceAnchorData_t2028622935)+ sizeof (RuntimeObject), sizeof(UnityARFaceAnchorData_t2028622935 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2597[4] = 
{
	UnityARFaceAnchorData_t2028622935::get_offset_of_ptrIdentifier_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARFaceAnchorData_t2028622935::get_offset_of_transform_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARFaceAnchorData_t2028622935::get_offset_of_faceGeometry_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARFaceAnchorData_t2028622935::get_offset_of_blendShapes_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (ARFaceGeometry_t5139606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2598[1] = 
{
	ARFaceGeometry_t5139606::get_offset_of_uFaceGeometry_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (ARFaceAnchor_t1844206636), -1, sizeof(ARFaceAnchor_t1844206636_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2599[3] = 
{
	ARFaceAnchor_t1844206636::get_offset_of_faceAnchorData_0(),
	ARFaceAnchor_t1844206636_StaticFields::get_offset_of_blendshapesDictionary_1(),
	ARFaceAnchor_t1844206636_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
