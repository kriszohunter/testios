﻿using UnityEngine;
using System.Collections;

public class AllRef : MonoBehaviour {



	public string[] inAppPackages;
	public string inAPP_KEY;

	public static int finishAnimationNumber = 1;

	#region SINGLETON PATTERN
	public static AllRef _instance;
	public static AllRef Instance
	{
		get {
			if (_instance == null)
			{
				_instance = GameObject.FindObjectOfType<AllRef>();

				if (_instance == null)
				{
					GameObject container = new GameObject("AllRef");
					_instance = container.AddComponent<AllRef>();
				}
			}

			return _instance;
		}
	}
	#endregion

//	public static AudioSource _FxPuzzleComplete;

	public AudioSource FxPuzzleComplete;
	public AudioSource FXCoin;

	void Awake()
	{
//		_FxPuzzleComplete = FxPuzzleComplete;
	}


	public void WaitForAnimationToComplete(Animator anim, string animName)
	{
		anim.enabled = true;
		StopAllCoroutines ();
		StopCoroutine ("EWaitForAnimationToComplete");
		StartCoroutine (EWaitForAnimationToComplete (anim,animName));
	}

	public IEnumerator EWaitForAnimationToComplete(Animator anim, string animName)
	{
//		print ("1");


//		if (anim.GetCurrentAnimatorStateInfo(0).IsName(animName))
//		{
//			print ("2");
//			anim.Stop ();
//
//		}
//		yield return null;

		yield return new WaitForSeconds (1.5f );

		//yield return new WaitForSeconds (2f);
//			anim.Stop ();
		anim.enabled = false;
//		anim.enabled = true;

//		print ("3--"+anim.GetCurrentAnimatorStateInfo (0).normalizedTime );
	}


}
