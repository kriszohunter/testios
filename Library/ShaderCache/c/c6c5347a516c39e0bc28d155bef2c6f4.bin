��                         SPOT   SHADOWS_DEPTH      SHADOWS_SOFT   DIRLIGHTMAP_SEPARATE   LIGHTMAP_OFF   DYNAMICLIGHTMAP_OFF >     xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct FGlobals_Type
{
    float3 _WorldSpaceCameraPos;
    float4 _WorldSpaceLightPos0;
    half4 _LightShadowData;
    float4 _ShadowOffsets[4];
    half4 _LightColor0;
    float4 _MainTex_ST;
    float4 _Diffusecolor;
    float4 _BumpMap_ST;
    float _NormalIntensity;
    float4 _SpecGlossMap_ST;
    float4 _Speccolor;
    float _SpecIntensity;
    float _Gloss;
    float _Transparency;
};

struct Mtl_FragmentIn
{
    float2 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    float4 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    float3 TEXCOORD4 [[ user(TEXCOORD4) ]] ;
    float3 TEXCOORD5 [[ user(TEXCOORD5) ]] ;
    float3 TEXCOORD6 [[ user(TEXCOORD6) ]] ;
    float4 TEXCOORD7 [[ user(TEXCOORD7) ]] ;
    float4 TEXCOORD8 [[ user(TEXCOORD8) ]] ;
};

struct Mtl_FragmentOut
{
    float4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
};

constexpr sampler _mtl_xl_shadow_sampler(address::clamp_to_edge, filter::linear, compare_func::greater_equal);
fragment Mtl_FragmentOut xlatMtlMain(
    constant FGlobals_Type& FGlobals [[ buffer(0) ]],
    sampler sampler_ShadowMapTexture [[ sampler (0) ]],
    sampler sampler_LightTexture0 [[ sampler (1) ]],
    sampler sampler_LightTextureB0 [[ sampler (2) ]],
    sampler sampler_MainTex [[ sampler (3) ]],
    sampler sampler_BumpMap [[ sampler (4) ]],
    sampler sampler_SpecGlossMap [[ sampler (5) ]],
    texture2d<half, access::sample > _BumpMap [[ texture (0) ]] ,
    texture2d<half, access::sample > _LightTexture0 [[ texture (1) ]] ,
    texture2d<half, access::sample > _LightTextureB0 [[ texture (2) ]] ,
    texture2d<half, access::sample > _SpecGlossMap [[ texture (3) ]] ,
    texture2d<half, access::sample > _MainTex [[ texture (4) ]] ,
    depth2d<float, access::sample > _ShadowMapTexture [[ texture (5) ]] ,
    bool mtl_FrontFace [[ front_facing ]],
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float3 u_xlat0;
    half3 u_xlat16_0;
    float4 u_xlat1;
    half4 u_xlat16_1;
    float4 u_xlat2;
    half3 u_xlat16_2;
    half u_xlat16_3;
    float3 u_xlat4;
    float3 u_xlat5;
    float2 u_xlat6;
    half u_xlat16_6;
    bool u_xlatb12;
    float u_xlat18;
    u_xlat0.xyz = input.TEXCOORD8.xyz / input.TEXCOORD8.www;
    u_xlat1.xyz = u_xlat0.xyz + FGlobals._ShadowOffsets[0].xyz;
    u_xlat1.x = float(_ShadowMapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat1.xy, saturate(u_xlat1.z), level(0.0)));
    u_xlat2.xyz = u_xlat0.xyz + FGlobals._ShadowOffsets[1].xyz;
    u_xlat1.y = float(_ShadowMapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat2.xy, saturate(u_xlat2.z), level(0.0)));
    u_xlat2.xyz = u_xlat0.xyz + FGlobals._ShadowOffsets[2].xyz;
    u_xlat0.xyz = u_xlat0.xyz + FGlobals._ShadowOffsets[3].xyz;
    u_xlat1.w = float(_ShadowMapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat0.xy, saturate(u_xlat0.z), level(0.0)));
    u_xlat1.z = float(_ShadowMapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat2.xy, saturate(u_xlat2.z), level(0.0)));
    u_xlat0.x = dot(u_xlat1, float4(0.25, 0.25, 0.25, 0.25));
    u_xlat16_6 = (-FGlobals._LightShadowData.x) + half(1.0);
    u_xlat0.x = fma(u_xlat0.x, float(u_xlat16_6), float(FGlobals._LightShadowData.x));
    u_xlat6.xy = input.TEXCOORD7.xy / input.TEXCOORD7.ww;
    u_xlat6.xy = u_xlat6.xy + float2(0.5, 0.5);
    u_xlat16_6 = _LightTexture0.sample(sampler_LightTexture0, u_xlat6.xy).w;
    u_xlatb12 = 0.0<input.TEXCOORD7.z;
    u_xlat16_3 = (u_xlatb12) ? half(1.0) : half(0.0);
    u_xlat16_3 = u_xlat16_6 * u_xlat16_3;
    u_xlat6.x = dot(input.TEXCOORD7.xyz, input.TEXCOORD7.xyz);
    u_xlat16_6 = _LightTextureB0.sample(sampler_LightTextureB0, u_xlat6.xx).x;
    u_xlat16_3 = u_xlat16_6 * u_xlat16_3;
    u_xlat16_3 = half(u_xlat0.x * float(u_xlat16_3));
    u_xlat16_0.xyz = half3(u_xlat16_3) * FGlobals._LightColor0.xyz;
    u_xlat18 = dot(input.TEXCOORD4.xyz, input.TEXCOORD4.xyz);
    u_xlat18 = rsqrt(u_xlat18);
    u_xlat1.xyz = float3(u_xlat18) * input.TEXCOORD4.xyz;
    u_xlat18 = ((mtl_FrontFace ? 0xffffffffu : uint(0)) != 0u) ? 1.0 : -1.0;
    u_xlat1.xyz = float3(u_xlat18) * u_xlat1.xyz;
    u_xlat2.xy = fma(input.TEXCOORD0.xy, FGlobals._BumpMap_ST.xy, FGlobals._BumpMap_ST.zw);
    u_xlat16_2.xyz = _BumpMap.sample(sampler_BumpMap, u_xlat2.xy).xyz;
    u_xlat16_2.xyz = fma(u_xlat16_2.xyz, half3(2.0, 2.0, 2.0), half3(-1.0, -1.0, -2.0));
    u_xlat2.xyz = fma(float3(FGlobals._NormalIntensity), float3(u_xlat16_2.xyz), float3(0.0, 0.0, 1.0));
    u_xlat4.xyz = u_xlat2.yyy * input.TEXCOORD6.xyz;
    u_xlat2.xyw = fma(u_xlat2.xxx, input.TEXCOORD5.xyz, u_xlat4.xyz);
    u_xlat1.xyz = fma(u_xlat2.zzz, u_xlat1.xyz, u_xlat2.xyw);
    u_xlat18 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat18 = rsqrt(u_xlat18);
    u_xlat1.xyz = float3(u_xlat18) * u_xlat1.xyz;
    u_xlat2.xyz = fma(FGlobals._WorldSpaceLightPos0.www, (-input.TEXCOORD3.xyz), FGlobals._WorldSpaceLightPos0.xyz);
    u_xlat18 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat18 = rsqrt(u_xlat18);
    u_xlat2.xyz = float3(u_xlat18) * u_xlat2.xyz;
    u_xlat18 = dot(u_xlat1.xyz, u_xlat2.xyz);
    u_xlat18 = max(u_xlat18, 0.0);
    u_xlat4.xyz = float3(u_xlat16_0.xyz) * float3(u_xlat18);
    u_xlat5.xyz = (-input.TEXCOORD3.xyz) + FGlobals._WorldSpaceCameraPos.xyzx.xyz;
    u_xlat18 = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat18 = rsqrt(u_xlat18);
    u_xlat2.xyz = fma(u_xlat5.xyz, float3(u_xlat18), u_xlat2.xyz);
    u_xlat18 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat18 = rsqrt(u_xlat18);
    u_xlat2.xyz = float3(u_xlat18) * u_xlat2.xyz;
    u_xlat18 = dot(u_xlat2.xyz, u_xlat1.xyz);
    u_xlat18 = max(u_xlat18, 0.0);
    u_xlat18 = log2(u_xlat18);
    u_xlat1.x = fma(FGlobals._Gloss, 10.0, 1.0);
    u_xlat1.x = exp2(u_xlat1.x);
    u_xlat18 = u_xlat18 * u_xlat1.x;
    u_xlat1.x = u_xlat1.x + 8.0;
    u_xlat1.x = u_xlat1.x * 0.0397887342;
    u_xlat18 = exp2(u_xlat18);
    u_xlat0.xyz = float3(u_xlat18) * float3(u_xlat16_0.xyz);
    u_xlat0.xyz = u_xlat1.xxx * u_xlat0.xyz;
    u_xlat1.xy = fma(input.TEXCOORD0.xy, FGlobals._SpecGlossMap_ST.xy, FGlobals._SpecGlossMap_ST.zw);
    u_xlat16_1.xyz = _SpecGlossMap.sample(sampler_SpecGlossMap, u_xlat1.xy).xyz;
    u_xlat1.xyz = float3(u_xlat16_1.xyz) * float3(FGlobals._SpecIntensity);
    u_xlat1.xyz = u_xlat1.xyz * FGlobals._Speccolor.xyz;
    u_xlat0.xyz = u_xlat0.xyz * u_xlat1.xyz;
    u_xlat18 = max(u_xlat1.y, u_xlat1.x);
    u_xlat18 = max(u_xlat1.z, u_xlat18);
    u_xlat18 = (-u_xlat18) + 1.0;
    u_xlat1.xy = fma(input.TEXCOORD0.xy, FGlobals._MainTex_ST.xy, FGlobals._MainTex_ST.zw);
    u_xlat16_1 = _MainTex.sample(sampler_MainTex, u_xlat1.xy);
    u_xlat1.xyz = float3(u_xlat16_1.xyz) * FGlobals._Diffusecolor.xyz;
    u_xlat1.xyz = float3(u_xlat18) * u_xlat1.xyz;
    u_xlat0.xyz = fma(u_xlat4.xyz, u_xlat1.xyz, u_xlat0.xyz);
    u_xlat18 = (-FGlobals._Transparency) + 1.0;
    u_xlat18 = u_xlat18 * float(u_xlat16_1.w);
    output.SV_Target0.xyz = float3(u_xlat18) * u_xlat0.xyz;
    output.SV_Target0.w = 0.0;
    return output;
}
                                FGlobals�         _WorldSpaceCameraPos                         _WorldSpaceLightPos0                        _LightShadowData                        _ShadowOffsets                   0      _LightColor0                 p      _MainTex_ST                   �      _Diffusecolor                     �      _BumpMap_ST                   �      _NormalIntensity                  �      _SpecGlossMap_ST                  �   
   _Speccolor                    �      _SpecIntensity                    �      _Gloss                    �      _Transparency                     �             _BumpMap                 _LightTexture0                  _LightTextureB0                 _SpecGlossMap                   _MainTex                _ShadowMapTexture                    FGlobals           