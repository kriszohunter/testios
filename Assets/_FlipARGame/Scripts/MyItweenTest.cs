﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyItweenTest : MonoBehaviour {

    public GameObject TargetPos;
	void Start () {
        OnItweenBtnClick();
	}
	
	
	public void OnItweenBtnClick () {
        iTween.MoveTo(gameObject, iTween.Hash("position", new Vector3(TargetPos.transform.position.x - 0.05f, TargetPos.transform.position.y, TargetPos.transform.position.z), "time", 0.8f, "easeType", iTween.EaseType.easeOutCirc));

        iTween.RotateBy(gameObject, iTween.Hash("x", 8.0f, "looptype", iTween.LoopType.none));
	}
}
