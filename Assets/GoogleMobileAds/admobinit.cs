﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public class admobinit : MonoBehaviour {

    private BannerView bannerView;
    // Use this for initialization
    void Start ()
    {
#if UNITY_ANDROID
            string appId = "ca-app-pub-8912234089672476~7032519897";
#elif UNITY_IPHONE
        string appId = "ca-app-pub-8912234089672476~7032519897";
#else
            string appId = "unexpected_platform";
#endif

        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appId);
        this.RequestBanner();
    }


    private void RequestBanner()
    {
#if UNITY_ANDROID
            string adUnitId = "ca-app-pub-8912234089672476/2326988127";
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-8912234089672476/2326988127";
#else
            string adUnitId = "unexpected_platform";
#endif

        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Top);
    }



    // Update is called once per frame
    void Update () {
		
	}
}
