﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    public static SoundManager Instance;


    public AudioSource Chocking;
    public AudioSource DeathBiteAndChew;
    public AudioSource DanceTheWorld;
    public AudioSource TwoOfSwords;
    public AudioSource Shuffling;


	
	void Awake () {
        Instance = this;
	}
	
	
	
}
