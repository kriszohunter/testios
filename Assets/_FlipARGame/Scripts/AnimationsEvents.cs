﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class AnimationsEvents : StateMachineBehaviour
{
    //public PlaneVisualizationManager planevisualizationmanager;
    public string StateName = "StateName";
    public float timeBeforeStateComplete = 0.99f;
    public bool isFadeIn = true;

	// OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Debug.Log("Anim is Playing ");
        //if (PlaneVisualizationManager.Instance.currentCardName == "Death")
            //PlaneVisualizationManager.Instance.LollipopFall();
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (stateInfo.IsName(StateName) && stateInfo.normalizedTime > timeBeforeStateComplete)
        {
            if (SceneManager.GetActiveScene().name != "MainMenu")
            {
                //Debug.Log("state Update - OnCharacterAnimComplete being called");
                PlaneVisualizationManager.Instance.OnCharacterAnimComplete(isFadeIn);
            }
        }
    }

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //Debug.Log("state exit");
        //PlaneVisualizationManager.Instance.OnCharacterAnimComplete(isFadeIn);
    }

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
