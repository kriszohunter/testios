��                         POINT_COOKIE   DIRLIGHTMAP_COMBINED   LIGHTMAP_OFF   DYNAMICLIGHTMAP_OFF 8     xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct FGlobals_Type
{
    float3 _WorldSpaceCameraPos;
    float4 _WorldSpaceLightPos0;
    half4 _LightColor0;
    float4 _MainTex_ST;
    float4 _Diffusecolor;
    float4 _BumpMap_ST;
    float _NormalIntensity;
    float4 _SpecGlossMap_ST;
    float4 _Speccolor;
    float _SpecIntensity;
    float _Gloss;
    float _Transparency;
};

struct Mtl_FragmentIn
{
    float2 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    float4 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    float3 TEXCOORD4 [[ user(TEXCOORD4) ]] ;
    float3 TEXCOORD5 [[ user(TEXCOORD5) ]] ;
    float3 TEXCOORD6 [[ user(TEXCOORD6) ]] ;
    float3 TEXCOORD7 [[ user(TEXCOORD7) ]] ;
};

struct Mtl_FragmentOut
{
    float4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
};

fragment Mtl_FragmentOut xlatMtlMain(
    constant FGlobals_Type& FGlobals [[ buffer(0) ]],
    sampler sampler_LightTexture0 [[ sampler (0) ]],
    sampler sampler_LightTextureB0 [[ sampler (1) ]],
    sampler sampler_MainTex [[ sampler (2) ]],
    sampler sampler_BumpMap [[ sampler (3) ]],
    sampler sampler_SpecGlossMap [[ sampler (4) ]],
    texture2d<half, access::sample > _BumpMap [[ texture (0) ]] ,
    texture2d<half, access::sample > _LightTextureB0 [[ texture (1) ]] ,
    texturecube<half, access::sample > _LightTexture0 [[ texture (2) ]] ,
    texture2d<half, access::sample > _SpecGlossMap [[ texture (3) ]] ,
    texture2d<half, access::sample > _MainTex [[ texture (4) ]] ,
    bool mtl_FrontFace [[ front_facing ]],
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float4 u_xlat0;
    float4 u_xlat1;
    half3 u_xlat16_1;
    float3 u_xlat2;
    half4 u_xlat16_2;
    float u_xlat3;
    float u_xlat6;
    half u_xlat16_6;
    float u_xlat9;
    u_xlat0.x = dot(input.TEXCOORD4.xyz, input.TEXCOORD4.xyz);
    u_xlat0.x = rsqrt(u_xlat0.x);
    u_xlat0.xyz = u_xlat0.xxx * input.TEXCOORD4.xyz;
    u_xlat9 = ((mtl_FrontFace ? 0xffffffffu : uint(0)) != 0u) ? 1.0 : -1.0;
    u_xlat0.xyz = float3(u_xlat9) * u_xlat0.xyz;
    u_xlat1.xy = fma(input.TEXCOORD0.xy, FGlobals._BumpMap_ST.xy, FGlobals._BumpMap_ST.zw);
    u_xlat16_1.xyz = _BumpMap.sample(sampler_BumpMap, u_xlat1.xy).xyz;
    u_xlat16_1.xyz = fma(u_xlat16_1.xyz, half3(2.0, 2.0, 2.0), half3(-1.0, -1.0, -2.0));
    u_xlat1.xyz = fma(float3(FGlobals._NormalIntensity), float3(u_xlat16_1.xyz), float3(0.0, 0.0, 1.0));
    u_xlat2.xyz = u_xlat1.yyy * input.TEXCOORD6.xyz;
    u_xlat1.xyw = fma(u_xlat1.xxx, input.TEXCOORD5.xyz, u_xlat2.xyz);
    u_xlat0.xyz = fma(u_xlat1.zzz, u_xlat0.xyz, u_xlat1.xyw);
    u_xlat9 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat9 = rsqrt(u_xlat9);
    u_xlat0.xyz = float3(u_xlat9) * u_xlat0.xyz;
    u_xlat1.xyz = fma(FGlobals._WorldSpaceLightPos0.www, (-input.TEXCOORD3.xyz), FGlobals._WorldSpaceLightPos0.xyz);
    u_xlat9 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat9 = rsqrt(u_xlat9);
    u_xlat1.xyz = float3(u_xlat9) * u_xlat1.xyz;
    u_xlat2.xyz = (-input.TEXCOORD3.xyz) + FGlobals._WorldSpaceCameraPos.xyzx.xyz;
    u_xlat9 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat9 = rsqrt(u_xlat9);
    u_xlat2.xyz = fma(u_xlat2.xyz, float3(u_xlat9), u_xlat1.xyz);
    u_xlat0.w = dot(u_xlat0.xyz, u_xlat1.xyz);
    u_xlat1.x = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat1.x = rsqrt(u_xlat1.x);
    u_xlat1.xyz = u_xlat1.xxx * u_xlat2.xyz;
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat0.xyz);
    u_xlat0.xw = max(u_xlat0.xw, float2(0.0, 0.0));
    u_xlat0.x = log2(u_xlat0.x);
    u_xlat3 = fma(FGlobals._Gloss, 10.0, 1.0);
    u_xlat3 = exp2(u_xlat3);
    u_xlat0.x = u_xlat0.x * u_xlat3;
    u_xlat3 = u_xlat3 + 8.0;
    u_xlat3 = u_xlat3 * 0.0397887342;
    u_xlat0.x = exp2(u_xlat0.x);
    u_xlat6 = dot(input.TEXCOORD7.xyz, input.TEXCOORD7.xyz);
    u_xlat16_6 = _LightTextureB0.sample(sampler_LightTextureB0, float2(u_xlat6)).x;
    u_xlat16_1.x = _LightTexture0.sample(sampler_LightTexture0, input.TEXCOORD7.xyz).w;
    u_xlat16_6 = u_xlat16_6 * u_xlat16_1.x;
    u_xlat16_1.xyz = half3(u_xlat16_6) * FGlobals._LightColor0.xyz;
    u_xlat2.xyz = u_xlat0.xxx * float3(u_xlat16_1.xyz);
    u_xlat0.xzw = u_xlat0.www * float3(u_xlat16_1.xyz);
    u_xlat1.xyz = float3(u_xlat3) * u_xlat2.xyz;
    u_xlat2.xy = fma(input.TEXCOORD0.xy, FGlobals._SpecGlossMap_ST.xy, FGlobals._SpecGlossMap_ST.zw);
    u_xlat16_2.xyz = _SpecGlossMap.sample(sampler_SpecGlossMap, u_xlat2.xy).xyz;
    u_xlat2.xyz = float3(u_xlat16_2.xyz) * float3(FGlobals._SpecIntensity);
    u_xlat2.xyz = u_xlat2.xyz * FGlobals._Speccolor.xyz;
    u_xlat1.xyz = u_xlat1.xyz * u_xlat2.xyz;
    u_xlat3 = max(u_xlat2.y, u_xlat2.x);
    u_xlat3 = max(u_xlat2.z, u_xlat3);
    u_xlat3 = (-u_xlat3) + 1.0;
    u_xlat2.xy = fma(input.TEXCOORD0.xy, FGlobals._MainTex_ST.xy, FGlobals._MainTex_ST.zw);
    u_xlat16_2 = _MainTex.sample(sampler_MainTex, u_xlat2.xy);
    u_xlat2.xyz = float3(u_xlat16_2.xyz) * FGlobals._Diffusecolor.xyz;
    u_xlat2.xyz = float3(u_xlat3) * u_xlat2.xyz;
    u_xlat0.xyz = fma(u_xlat0.xzw, u_xlat2.xyz, u_xlat1.xyz);
    u_xlat9 = (-FGlobals._Transparency) + 1.0;
    u_xlat9 = u_xlat9 * float(u_xlat16_2.w);
    output.SV_Target0.xyz = float3(u_xlat9) * u_xlat0.xyz;
    output.SV_Target0.w = 0.0;
    return output;
}
                              FGlobals�         _WorldSpaceCameraPos                         _WorldSpaceLightPos0                        _LightColor0                        _MainTex_ST                   0      _Diffusecolor                     @      _BumpMap_ST                   P      _NormalIntensity                  `      _SpecGlossMap_ST                  p   
   _Speccolor                    �      _SpecIntensity                    �      _Gloss                    �      _Transparency                     �             _BumpMap                 _LightTextureB0                 _LightTexture0                   _SpecGlossMap                   _MainTex                FGlobals           