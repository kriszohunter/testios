﻿using UnityEngine;
using System.Collections;

public class Preferences {

	static Preferences _instance;
	private int _level = 1;
	private int _coins = 0;
	private int _rateUs = 1;
    private int _ads = 0;
	private int _isRate = 0;
    private int _score = 0;


	public static Preferences Instance
    {
		get
		{
			if (_instance == null)
				_instance = new Preferences();
			return _instance;
		}
	}

	private Preferences(){
		Load();
	}

	void Load(){
        _isRate = PlayerPrefs.GetInt("israte", 0);
		_level = PlayerPrefs.GetInt("level", 1);
		_coins = PlayerPrefs.GetInt("coins", 0);
		_rateUs = PlayerPrefs.GetInt("rateUs", 1);
		_ads = PlayerPrefs.GetInt("ads", 0);
		_score = PlayerPrefs.GetInt("score", 0);
	}
	
	void Save(){
        PlayerPrefs.SetInt("israte", _isRate);
		PlayerPrefs.SetInt("level", _level);
		PlayerPrefs.SetInt("coins", _coins);
		PlayerPrefs.SetInt("rateUs", _rateUs);
		PlayerPrefs.SetInt("ads", _ads);
        PlayerPrefs.SetInt("score", _score);
		PlayerPrefs.Save();
	}

	public void Reset(){
		PlayerPrefs.DeleteAll();
		PlayerPrefs.Save();
		Load();
	}
	
	public int Level{
		get{    return _level;    }
		set {   _level = value;    Save();  }
	}

	public int Coins{
		get{    return _coins;    }
		set {   _coins = value;    Save();  }
	}
	
	public int RateUs{
		get{    return _rateUs;    }
		set{    _rateUs = value;    Save(); }
	}

    public int isRate
    {
        get { return _isRate; }
        set { _isRate = value; Save(); }
    }

    public int RemoveAds
    {
        get { return _ads; }
        set { _ads = value; Save(); }
    }
	public int Score
	{
		get { return _score; }
		set { _score = value; Save(); }
	}
}