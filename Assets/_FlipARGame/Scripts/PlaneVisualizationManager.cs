﻿/*================================================
== Adapted by  : Ibrahim Wasim (ibi)	        ==
== Description : A prefab for tracking 			==
==			    and visualizing detected 		==
==			    planes.                     	==	          
==================================================*/
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;
using GoogleARCore.Examples.HelloAR;
using GoogleARCore.Examples.Common;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.XR.iOS;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class PlaneVisualizationManager : MonoBehaviour {

    #region DebugObjects Only
    //Only debug
    public Text scaleText;//only for Debug
    public Text scaleText2;//only for Debug
    public Slider slider;
    public void OnSliderValueChanged()
    {
        Debug.Log("On Slider Value Changed ");
        //ParentObj.transform.localScale = new Vector3(slider.value, slider.value, slider.value);
        myCurrentCharacter.transform.localScale = new Vector3(slider.value, slider.value, slider.value);
    }
    #endregion

    #region References Specific to Android, but still needed in IOS too 
    public GameObject TrackedPlanePrefab;
	public Camera m_firstPersonCamera;
    private List<DetectedPlane> _newPlanes = new List<DetectedPlane>();
    List<GameObject> PlaneObject = new List<GameObject>();
    bool isStopInitialInstruction = false;
    #endregion

    #region Other References

    //UI References
    public GameObject[] Characters;   
    public GameObject[] cardImages;
    public GameObject[] cardPrefabs;
    public Material[] CharacterMaterials;
    public Material[] SwordMaterials;
    public string[] cardNames;
    public GameObject cardPrefabToInstantiate;
    public Animator cardShuffleAnim;
    public Text instructionText;
    public GameObject[] CardDescriptionPopUpObj;
    public GameObject CardDeckObj;
    public GameObject Instruction1Obj;

    //Object references
    public GameObject CharacterToInstantiate;
    public Animator anim;   //To Animate the character
    private GameObject currentCard = null;
    private string currentCardName = null;
    GameObject[] cardPositions;
    public float lerpSpeed = 2.5f;
    public AnimationCurve curveFadeIn;
    public AnimationCurve curveFadeOut;
    
    //Variables
    public GameObject myCurrentCharacter;//current character 
    public Material myCurrentCharacterMaterial; //current character Material
    bool isAlreadyCreated = false; //Check if Charater is created only once    
    public bool stopPlaneDetection = false;
    bool isSwipeAllowed = false;
    bool isTapAllowed = false;
    int randomNumber = 0;//for cards shuffle
    int previousrandomNumber = -1;
    public  bool isFadeIn = false;
    public bool isFadeOut = false;
    private float currentCurveValue = 0f;
    float rotationAngle = 180f; //used to rotate generated character towards camera
    public static PlaneVisualizationManager Instance;

    
    


    #endregion

    TKPinchRecognizer pinchRecognizer = new TKPinchRecognizer();
    TKSwipeRecognizer swipeRecognizer = new TKSwipeRecognizer();
    TKTapRecognizer tapRecognizer = new TKTapRecognizer();
    TKLongPressRecognizer longPressRecognizer = new TKLongPressRecognizer();

    #region Reference Specific to IOS Platform & IOS Scene
    //IOS references
	[Header("For IOS Only")]
	public UnityARHitTestExample unityARHitTestExample;//reference to the script that will place the object in IOS

	public GameObject[] toSwitchOff; //to swith off all the planes visualizations
    //public bool isStartDetecting = true;//false;
	public PointCloudParticleExample _PointCloudParticleExample;
	public UnityARGeneratePlane _UnityARGeneratePlane;
	public UnityPointCloudExample _UnityPointCloudExample;
	public UnityARKitControl _UnityARKitControl;
    #endregion

    

    #region IOS only

    public Vector3 CharacterPosIOS = Vector3.zero;
	public Quaternion CharacterRotIOS = Quaternion.identity;

	public void OnPlaneDetectionCharacterInstantiateIOS()
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer) {

            //Debug.Log("** isStartDetecting= "+isStartDetecting+" - isAlreadyCreated= "+isAlreadyCreated);
				
            //if(!isStartDetecting)//so that only detect when the camera is on 
            //    return;

			isStopInitialInstruction = true;
			StopCoroutine(InitialInstructionE());


			if (!isAlreadyCreated) {//so that created only once
				isAlreadyCreated = true;
                
				// Instantiate Andy model at the hit pose.
				var andyObject = Instantiate (CharacterToInstantiate, CharacterPosIOS, CharacterRotIOS);
				myCurrentCharacter = andyObject;
				// Compensate for the hitPose rotation facing away from the raycast (i.e. camera).
                andyObject.transform.LookAt(Camera.main.transform);
                andyObject.transform.Rotate(0, andyObject.transform.localRotation.y, 0, Space.Self);
                Vector3 temp = andyObject.transform.rotation.eulerAngles;
                temp.z = 0.0f;
                temp.x = 0.0f;
                andyObject.transform.rotation = Quaternion.Euler(temp);

				anim = andyObject.GetComponent<Animator> ();
				myCurrentCharacter.transform.localScale = new Vector3 (0.2f, 0.2f, 0.2f);
				StartCoroutine (SetMyModelScale ());
				Debug.Log ("** Creating model IOS");
				stopPlaneDetection = true;
				cardPositions = myCurrentCharacter.GetComponent<CharacterScript> ().cardPositions;
                

//				foreach (GameObject item in toSwitchOff) {
//					item.SetActive (false);
//				}



                instructionText.text = "PRESS DOWN ON THE CARDS TO MIX THE DECK AND LET GO TO FLIP A CARD";
				instructionText.gameObject.SetActive (true);
                isTapAllowed = true;

//				_UnityARKitControl.StopDetection ();
				_UnityARGeneratePlane.Stop ();
//				_PointCloudParticleExample.Stop();
				_UnityPointCloudExample.Stop ();
//				_UnityARGeneratePlane.gameObject.SetActive (false);
                OnCharacterSpawnComplete();

			}
		}//IOS ka code end hota ha yahan ha
	}



    public void VerifyPermission()
    {
        Debug.Log("VerifyPermission()");

        // Use native UI to request camera permission.
        iOSCameraPermission.VerifyPermission(gameObject.name, "SampleCallback");
    }
    private void SampleCallback(string permissionWasGranted)
    {
        Debug.Log("Callback.permissionWasGranted = " + permissionWasGranted);

        if (permissionWasGranted == "true")
        {
            Debug.LogError("****** Camera Permission granted");
        }
        else
        {
            Debug.LogError("****** Camera Permission NOT granted");
        }
    }

    #endregion

    #region mono

    void OnEnable()
    {
        Debug.Log("OnEnable has run");
        pinchRecognizer.gestureRecognizedEvent += pinchRecognizer_gestureRecognizedEvent;
        pinchRecognizer.gestureCompleteEvent += pinchRecognizer_gestureCompleteEvent;
        TouchKit.addGestureRecognizer(pinchRecognizer);
        
        swipeRecognizer.gestureRecognizedEvent += swipeRecognizer_gestureRecognizedEvent;
        TouchKit.addGestureRecognizer(swipeRecognizer);
        tapRecognizer.gestureRecognizedEvent += tapRecognizer_gestureRecognizedEvent;
        TouchKit.addGestureRecognizer(tapRecognizer);
        longPressRecognizer.gestureRecognizedEvent += longPressRecognizer_gestureRecognizedEvent;
        longPressRecognizer.gestureCompleteEvent += longPressRecognizer_gestureCompleteEvent;
        TouchKit.addGestureRecognizer(longPressRecognizer);
    }

    
    void OnDisable()
    {
        pinchRecognizer.gestureRecognizedEvent -= pinchRecognizer_gestureRecognizedEvent;
        pinchRecognizer.gestureCompleteEvent -= pinchRecognizer_gestureCompleteEvent;
        TouchKit.removeGestureRecognizer(pinchRecognizer);
        swipeRecognizer.gestureRecognizedEvent -= swipeRecognizer_gestureRecognizedEvent;
        TouchKit.removeGestureRecognizer(swipeRecognizer);
        longPressRecognizer.gestureRecognizedEvent -= longPressRecognizer_gestureRecognizedEvent;
        longPressRecognizer.gestureCompleteEvent -= longPressRecognizer_gestureCompleteEvent;
        TouchKit.removeGestureRecognizer(longPressRecognizer);
    }

    IEnumerator InitialInstructionE()
    {
		if (Application.platform == RuntimePlatform.Android) {
			int temp = 1;
			instructionText.gameObject.SetActive (true);
			while (!isStopInitialInstruction) {
				if (temp == 1) {
					instructionText.text = "PLEASE POINT THE CAMERA TOWARDS SOMETHING PLANE TO DETECT A SURFACE";
					temp = 0;
				} else if (temp == 0) {
					instructionText.text = "PLEASE WAIT WHILE CAMERA IS DETECTING SURFACE";
					temp = 1;
				}
                yield return new WaitForSeconds(5f);
			}
            if (instructionText.text != "PRESS DOWN ON THE CARDS TO MIX THE DECK AND LET GO TO FLIP A CARD")
                instructionText.text = "LOOK UP AND TAP ON THE HIGHLIGHTED AREA TO SHOW YOUR AVATAR";
		}
    }

    void Start()
    {
        Instance = this;
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			VerifyPermission ();

//			_UnityARGeneratePlane.gameObject.SetActive (true);
			_PointCloudParticleExample.isStop = false;
//			_UnityARKitControl.StartDetection ();
		}


        StopAllCoroutines();
        CharacterToInstantiate = Characters[GameManager.Instance.selectedCharacterNumber - 1];
        myCurrentCharacterMaterial = CharacterMaterials[GameManager.Instance.selectedCharacterNumber - 1];
        cardPrefabToInstantiate = cardPrefabs[0];
        foreach (Material item in CharacterMaterials)
        {
            item.SetFloat("_Transparency",0);
        }
        StartCoroutine(InitialInstructionE());
    }
	void Update ()
	{
        FadeInMaterial();
        FadeOutMaterial();

        #region DebugOnly
        //if (myCharacter != null)
        //{
        //    scaleText.text = myCharacter.transform.localScale.x.ToString();            
        //}
        //else
        //    scaleText.text = "myOBJ is null";

        //if (currentCard != null)
        //{
        //    scaleText2.text = currentCard.transform.localScale.x.ToString();
        //}
        //else
        //    scaleText2.text = "Card is null";
       #endregion

        if (stopPlaneDetection)
        {
            return;
        }


		if (Application.platform == RuntimePlatform.Android) {
			_UpdateApplicationLifecycle ();
			Session.GetTrackables<DetectedPlane> (_newPlanes, TrackableQueryFilter.New);

			// Iterate over planes found in this frame and instantiate corresponding GameObjects to visualize them.
			foreach (var curPlane in _newPlanes)
            {
                isStopInitialInstruction = true;
                StopCoroutine(InitialInstructionE());

                #region for plane drawing
                // Instantiate a plane visualization prefab and set it to track the new plane. The transform is set to
	            // the origin with an identity rotation since the mesh for our prefab is updated in Unity World
	            // coordinates.
	            var planeObject = Instantiate(TrackedPlanePrefab, Vector3.zero, Quaternion.identity, transform);
	            planeObject.GetComponent<DetectedPlaneVisualizer>().Initialize(curPlane);
	            // Apply a random color and grid rotation.
	            planeObject.GetComponent<Renderer>().material.SetColor("_GridColor", new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f)));
	            planeObject.GetComponent<Renderer>().material.SetFloat("_UvRotation", Random.Range(0.0f, 360.0f));
                PlaneObject.Add(planeObject.gameObject);


                instructionText.text = "LOOK UP AND TAP ON THE HIGHLIGHTED AREA TO SHOW YOUR AVATAR";
                instructionText.gameObject.SetActive(true);
                #endregion


                #region Android Model instantiating
                //if (_newPlanes.Count > 0) {
                //    if (!isAlreadyCreated) {//so that created only once
                //        isAlreadyCreated = true;
                        
                //        Debug.Log ("Creating model without pointer click Start. Plane count = " + _newPlanes.Count);
                //        // Instantiate Andy model at the hit pose.
                //        var andyObject = Instantiate (CharacterToInstantiate, _newPlanes [0].CenterPose.position, _newPlanes [0].CenterPose.rotation);
                //        myCurrentCharacter = andyObject;
                //        // Compensate for the hitPose rotation facing away from the raycast (i.e. camera).
                //        andyObject.transform.LookAt(Camera.main.transform);
                //        andyObject.transform.Rotate(0, andyObject.transform.localRotation.y, 0, Space.Self);
                //        Vector3 temp = andyObject.transform.rotation.eulerAngles;
                //        temp.z = 0.0f;
                //        temp.x = 0.0f;
                //        andyObject.transform.rotation = Quaternion.Euler(temp);
                //        // Create an anchor to allow ARCore to track the hitpoint as understanding of the physical
                //        // world evolves.
                //        var anchor = _newPlanes [0].CreateAnchor (_newPlanes [0].CenterPose);

                //        // Make Andy model a child of the anchor.
                //        andyObject.transform.parent = anchor.transform;
                //        //andyObject.GetComponent<Renderer>().material.SetFloat("_UvRotation", Random.Range(0.0f, 360.0f));
                //        anim = andyObject.GetComponent<Animator> ();
                //        myCurrentCharacter.transform.localScale = new Vector3 (0.2f, 0.2f, 0.2f);
                //        StartCoroutine (SetMyModelScale ());
                //        stopPlaneDetection = true;
                //        FindObjectOfType<PointcloudVisualizer> ().StopVisualising ();
                //        cardPositions = myCurrentCharacter.GetComponent<CharacterScript> ().cardPositions;
                //        instructionText.text = "PRESS DOWN ON THE CARDS TO MIX THE DECK AND LET GO TO FLIP A CARD";
                //        instructionText.gameObject.SetActive (true);
                //        isTapAllowed = true;
                //        OnCharacterSpawnComplete();
                //    }
                //}
                #endregion

            }
          
            #region Touch to Instantiate

            //Debug.Log("** isStartDetecting= " + isStartDetecting);
            //if(!isStartDetecting)//so that only detect when the camera is on 
            //    return;

           
            Touch touch;
            if (Input.touchCount < 1 || (touch = Input.GetTouch(0)).phase != TouchPhase.Began)
            {
                return;
            }

            TrackableHit hit;
            TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon | TrackableHitFlags.PlaneWithinBounds;
            if (Frame.Raycast(touch.position.x, touch.position.y, raycastFilter, out hit))
            {
                // Use hit pose and camera pose to check if hittest is from the
                // back of the plane, if it is, no need to create the anchor.
                if ((hit.Trackable is DetectedPlane) &&
                   Vector3.Dot(m_firstPersonCamera.transform.position - hit.Pose.position,
                       hit.Pose.rotation * Vector3.up) < 0)
                {
                    Debug.Log("Hit at back of the current DetectedPlane");
                }
                else
                {
                    if (isAlreadyCreated)//so that created only once
                        return;
                    isAlreadyCreated = true;
                    isTapAllowed = true;
                    Debug.Log("** Creating model on Pointer click");
                    // Instantiate Andy model at the hit pose.
                    var andyObject = Instantiate(CharacterToInstantiate, hit.Pose.position, hit.Pose.rotation);
                    myCurrentCharacter = andyObject;
                    // Compensate for the hitPose rotation facing away from the raycast (i.e. camera).
                    //andyObject.transform.Rotate(0, 0f, 0, Space.Self);
                    andyObject.transform.LookAt(Camera.main.transform);
                    andyObject.transform.Rotate(0, andyObject.transform.localRotation.y, 0, Space.Self);
                    Vector3 temp = andyObject.transform.rotation.eulerAngles;
                    temp.z = 0.0f;
                    temp.x = 0.0f;
                    andyObject.transform.rotation = Quaternion.Euler(temp);
                    // Create an anchor to allow ARCore to track the hitpoint as understanding of the physical
                    // world evolves.
                    var anchor = hit.Trackable.CreateAnchor(hit.Pose);

                    // Make Andy model a child of the anchor.
                    andyObject.transform.parent = anchor.transform;
                    //andyObject.GetComponent<Renderer>().material.SetFloat("_UvRotation", Random.Range(0.0f, 360.0f));
                    anim = andyObject.GetComponent<Animator>();
                    myCurrentCharacter.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
                    StartCoroutine(SetMyModelScale());
                    stopPlaneDetection = true;
                    FindObjectOfType<PointcloudVisualizer>().StopVisualising();
                    cardPositions = myCurrentCharacter.GetComponent<CharacterScript>().cardPositions;
                    instructionText.text = "PRESS DOWN ON THE CARDS TO MIX THE DECK AND LET GO TO FLIP A CARD";
                    instructionText.gameObject.SetActive(true);

                    foreach (GameObject item in PlaneObject)
                    {
                        Destroy(item);
                    }
                    PlaneObject.Clear();
                    OnCharacterSpawnComplete();
                }
            }
            #endregion
        }//Android ka code ha yahan tak


    }//update ends here
  
    #endregion

    #region Android Only

    private void _UpdateApplicationLifecycle()
	{
		// Exit the app when the 'back' button is pressed.
		if (Input.GetKey(KeyCode.Escape))
		{
			Application.Quit();
		}

		// Only allow the screen to sleep when not tracking.
		if (Session.Status != SessionStatus.Tracking)
		{
			const int lostTrackingSleepTimeout = 15;
			Screen.sleepTimeout = lostTrackingSleepTimeout;
		}
		else
		{
			Screen.sleepTimeout = SleepTimeout.NeverSleep;
		}

		if (m_IsQuitting)
		{
			return;
		}

		if (Application.platform == RuntimePlatform.Android) {
			// Quit if ARCore was unable to connect and give Unity some time for the toast to appear.
			if (Session.Status == SessionStatus.ErrorPermissionNotGranted) {
				_ShowAndroidToastMessage ("Camera permission is needed to run this application.");
				m_IsQuitting = true;
				Invoke ("_DoQuit", 0.5f);
			} else if (Session.Status.IsError ()) {
				_ShowAndroidToastMessage ("ARCore encountered a problem connecting.  Please start the app again.");
				Debug.LogError ("** AR error = " + Session.Status.ToString ());
				m_IsQuitting = true;
				Invoke ("_DoQuit", 0.5f);
			}
		} else if (Application.platform == RuntimePlatform.IPhonePlayer) {
//			if (Application.HasUserAuthorization(UserAuthorization.WebCam)) {
//				Debug.LogError ("** user has authorized for camera permission");
//			} 
//			else {
//				Debug.LogError ("** user has NOT authorized for camera permission");
//			}
		}
	}

	private bool m_IsQuitting = false;
	private void _DoQuit()
	{
		Application.Quit();
	}
	private void _ShowAndroidToastMessage(string message)
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer)
			return;
		
		AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

		if (unityActivity != null)
		{
			AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
			unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
				{
					AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity,
						message, 0);
					toastObject.Call("show");
				}));
		}
	}
  
    #endregion



    #region After Character spwan Handles

    public void OnCharacterSpawnComplete()
    {
        Debug.Log("*** Chatacter Spawn complete has run");
        instructionText.gameObject.SetActive(false);
        Instruction1Obj.SetActive(true);
    }

    bool onScreen = false;//Check if Character is rendering in camera or not, so that we throw card on that bases
    public void OnCardBtnClick()
    {
        StartCoroutine(OnCardBtnClickE());
    }    
    IEnumerator OnCardBtnClickE()
    {
        onScreen = false;
        if (myCurrentCharacter != null)
        {
            Vector3 screenPoint = Camera.main.WorldToViewportPoint(myCurrentCharacter.transform.position);
            onScreen = screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;
        }
        //if (onScreen)
        //{
            Debug.Log("** Character is in the view");
            Vector2 m_ScreenPosition = new Vector2(Screen.width / 2, Screen.height / 2);
            var ray = m_firstPersonCamera.ScreenPointToRay(m_ScreenPosition);
            
            if (currentCard != null)
            {
                Destroy(currentCard);
            }
            
            var Card = Instantiate(cardPrefabToInstantiate, ray.origin + ray.direction * -0.5f, Quaternion.identity);
            currentCard = Card;
            Card.gameObject.transform.parent = myCurrentCharacter.transform;
            //Card.transform.localScale = myOBJ.transform.localScale;
            Card.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            yield return new WaitForSeconds(0.01f);

            float smallestDistance = 99999;
            int nearestCardPos = 0;
            //calculate nearest position
            for (int i = 0; i < cardPositions.Length; i++)
			{
                float dist = Vector3.Distance(Card.transform.position, cardPositions[i].transform.position);
                if(dist < smallestDistance)
                {
                    smallestDistance = dist;
                    nearestCardPos = i;
                }
            }

            //if(currentCard != null)
            //    Destroy(currentCard,6f);

            iTween.MoveTo(Card.gameObject, iTween.Hash("position", new Vector3(cardPositions[nearestCardPos].transform.position.x, cardPositions[nearestCardPos].transform.position.y, cardPositions[nearestCardPos].transform.position.z), "time", 0.8f, "easeType", iTween.EaseType.easeOutCirc));
            iTween.RotateBy(Card.gameObject, iTween.Hash("x", 8.0f, "looptype", iTween.LoopType.none));

            //Destroy(Card.gameObject, 3f);
        //}
        //else
        //{
        //    yield return null;
        //    Debug.Log("** Character is not in the view");
        //}
    }

    IEnumerator SetMyModelScale()
    {
        Debug.Log("** Setting Character Scale");
        yield return new WaitForSeconds(2f);
        myCurrentCharacter.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
    }


    bool isPinching = false;
    bool isResetPinching = false;
    bool isLongTouchDetectedAlready = false;
    void pinchRecognizer_gestureRecognizedEvent(TKPinchRecognizer obj)
    {
        if (isLongTouchDetectedAlready)
            return;

        isPinching = true;
        if (myCurrentCharacter.transform.localScale.x > 0.05f)
            myCurrentCharacter.transform.localScale = myCurrentCharacter.transform.localScale + Vector3.one * pinchRecognizer.deltaScale;
        else
            myCurrentCharacter.transform.localScale = Vector3.one * 0.1f;
        Debug.Log("** Pinch Touch has been reconized");
    }
    void pinchRecognizer_gestureCompleteEvent(TKPinchRecognizer obj)
    {
        //isPinching = false;
        isResetPinching = true;
        Debug.Log("** Pinch Touch has been Completed");
    }
    void swipeRecognizer_gestureRecognizedEvent(TKSwipeRecognizer obj)
    {
        //if (!isSwipeAllowed)
        //    return;
        //Debug.Log("** Swipe Touch has been reconized");
        //cardImages[randomNumber].SetActive(false);
        //StartCoroutine(OnCardBtnClickE());
        //isSwipeAllowed = false;
        //StartCoroutine(PlayCharacterAnim());
        //CardInfoPopUpObj.SetActive(false);
    }
    void tapRecognizer_gestureRecognizedEvent(TKTapRecognizer obj)
    {
        //if (!isTapAllowed)
        //    return;
        //Debug.Log("** Tap Touch has been reconized");
        ////cardShuffleAnim.Play("CardShuffle");
        //cardShuffleAnim.Play("CardShuffleLoop");
        //isTapAllowed = false;
    }
    void longPressRecognizer_gestureRecognizedEvent(TKLongPressRecognizer obj)
    {
        if (isLongTouchDetectedAlready)
            return;
        isLongTouchDetectedAlready = true;

        if (isPinching)
        {
            return;
        }
        if (!isTapAllowed)
            return;
        Debug.Log("** LongPress Started");
        cardShuffleAnim.Play("CardShuffleLoop");
        SoundManager.Instance.Shuffling.volume = 1f;
        SoundManager.Instance.Shuffling.Play();
    }
    void longPressRecognizer_gestureCompleteEvent(TKLongPressRecognizer obj)
    {
        isLongTouchDetectedAlready = false;
        if (isPinching)
        {
            if (isResetPinching)
                isPinching = false;
            isResetPinching = false;
            return;
        }
        if (!isTapAllowed)
            return;

        isTapAllowed = false;
        Debug.Log("** LongPress Ended");
        cardShuffleAnim.Play("CardShuffleAfterLoop");
        OnShuffleComplete();
        SoundManager.Instance.Shuffling.Stop();
    }

    public void OnShuffleComplete()
    {
        Debug.Log("** On Shuffle Complete has run");
        do
        {
            randomNumber = Random.Range(0, cardPrefabs.Length);            
        } while (randomNumber == previousrandomNumber);

        previousrandomNumber = randomNumber;
        cardPrefabToInstantiate = cardPrefabs[randomNumber];
        currentCardName = cardNames[randomNumber];

        //cardImages[randomNumber].SetActive(true);
        isSwipeAllowed = true;
        //instructionText.text = "SWIPE UP TO FLIP A CARD";
        //instructionText.gameObject.SetActive(true);
        instructionText.gameObject.SetActive(false);
        //CardInfoPopUpObj.SetActive(true);

        StartCoroutine(OnCardBtnClickE());
        StartCoroutine(PlayCharacterAnim());

    }

    bool isRecieveAnimationCallback = true;
    public void ResetAnimationReciverBool()
    {
        isRecieveAnimationCallback = true;
    }
    public void OnCharacterAnimComplete(bool _isFadeIn = true)
    {
        if (!isRecieveAnimationCallback)
            return;
        isRecieveAnimationCallback = false;
        Debug.Log("** On Character Anim Complete has run");
        //isTapAllowed = true;
        if (instructionText != null)
        {
            instructionText.text = "PRESS DOWN ON THE CARDS TO MIX THE DECK AND LET GO TO FLIP A CARD";
            instructionText.gameObject.SetActive(true);
        }
        //isFadeIn = _isFadeIn;
        
        CardDescriptionPopUpObj[randomNumber].SetActive(true);
        CardDescriptionPopUpObj[randomNumber].GetComponent<Animator>().Play("CardDescriptionAnim");

        if (currentCardName == "TwoOfSwords")
        {
            StartCoroutine(VolumeFadeOutE(SoundManager.Instance.TwoOfSwords));
        }
        else if (currentCardName == "Death")
        {
            StartCoroutine(VolumeFadeOutE(SoundManager.Instance.Chocking));
        }
        else if (currentCardName == "WheelOfFortune")
        {
            StartCoroutine(VolumeFadeOutE(SoundManager.Instance.DanceTheWorld));
        }


    }
    IEnumerator OnCharacterAnimCompleteE()//Depricated
    {
        yield return new WaitForSeconds(10f);
        Debug.Log("** On Character Anim Complete Coroutine has run");
        isTapAllowed = true;
        instructionText.text = "PRESS DOWN ON THE CARDS TO MIX THE DECK AND LET GO TO FLIP A CARD";
        instructionText.gameObject.SetActive(true);
    }
    IEnumerator PlayCharacterAnim()
    {
        //if (onScreen)
        //{
        Debug.Log("** PlayCharacterAnim has run");
        yield return new WaitForSeconds(1f);
        instructionText.gameObject.SetActive(false);
        anim.Play(currentCardName);
        previousCardGame = currentCardName;
        isRecieveAnimationCallback = true;
        //------------------------------------------------EDIT
        if (currentCardName == "Death")
        {
            LollipopFall();
            yield return new WaitForSeconds(0.2f);
            SoundManager.Instance.DeathBiteAndChew.volume = 1f;
            SoundManager.Instance.DeathBiteAndChew.Play();
            yield return new WaitForSeconds(2f);
            SoundManager.Instance.DeathBiteAndChew.volume = 1f;
            SoundManager.Instance.DeathBiteAndChew.Play();
            yield return new WaitForSeconds(2f);
            SoundManager.Instance.Chocking.volume = 1f;
            SoundManager.Instance.Chocking.Play();
        }
        else if (currentCardName == "TwoOfSwords")
        {
            SoundManager.Instance.TwoOfSwords.volume = 1f;
            SoundManager.Instance.TwoOfSwords.Play();
            SwordsShow();
        }
        else if (currentCardName == "WheelOfFortune")
        {
            SoundManager.Instance.DanceTheWorld.volume = 1f;
            SoundManager.Instance.DanceTheWorld.Play();
        }
        else
        {
            LollipopFall();
            yield return new WaitForSeconds(0.2f);
            SoundManager.Instance.DeathBiteAndChew.volume = 1f;
            SoundManager.Instance.DeathBiteAndChew.Play();
            yield return new WaitForSeconds(2f);
            SoundManager.Instance.DeathBiteAndChew.volume = 1f;
            SoundManager.Instance.DeathBiteAndChew.Play();
            yield return new WaitForSeconds(2f);
            SoundManager.Instance.Chocking.volume = 1f;
            SoundManager.Instance.Chocking.Play();
        }

        ResetAnimationReciverBool();
        //StartCoroutine(OnCharacterAnimCompleteE());        
        //}
    }

    string previousCardGame = "abc";
    public void OnCardDescriptionDialogClose()
    {
        Debug.Log("*** OnCardDescriptionDialogClose is running = " + previousCardGame);
        if (previousCardGame == "TwoOfSwords" || previousCardGame == "WheelOfFortune")
        {
            anim.Play(previousCardGame, -1, 0f);
        }
        isFadeIn = true;
        //CardInfoPopUpObj.SetActive(false);
        CardDescriptionPopUpObj[randomNumber].GetComponent<Animator>().Play("CardDescriptionAnimReverse");
        CardDescriptionPopUpObj[randomNumber].SetActive(false);

        if (currentCard != null)
            Destroy(currentCard, 1.5f);
    }


    void FadeInMaterial()
    {
        if (!isFadeIn)
            return;

        currentCurveValue += Time.deltaTime / 2f;
        myCurrentCharacterMaterial.SetFloat("_Transparency", curveFadeIn.Evaluate(currentCurveValue));
        foreach (Material item in SwordMaterials)
        {
            item.SetFloat("_Transparency", curveFadeIn.Evaluate(currentCurveValue));
        }

        if (myCurrentCharacterMaterial.GetFloat("_Transparency") >= 1 && isFadeIn)
        {
            currentCurveValue = 0;
            isFadeIn = false;
            ResetCharacterAfterAnim();
            myCurrentCharacter.GetComponent<CharacterScript>().Shadow.SetActive(false);
        }
    }
    void FadeOutMaterial()
    {
        if (!isFadeOut)
            return;
        currentCurveValue += Time.deltaTime / 1f;
        myCurrentCharacterMaterial.SetFloat("_Transparency", curveFadeOut.Evaluate(currentCurveValue));

        foreach (Material item in SwordMaterials)
        {
            item.SetFloat("_Transparency", curveFadeOut.Evaluate(currentCurveValue));
        }

        myCurrentCharacter.GetComponent<CharacterScript>().Shadow.SetActive(true);
        if (myCurrentCharacterMaterial.GetFloat("_Transparency") <= 0 && isFadeOut)
        {
            currentCurveValue = 0;
            isFadeOut = false;
            isTapAllowed = true;
        }
    }
    public void ResetCharacterAfterAnim()
    {
        StartCoroutine(ResetCharacterAfterAnimE());        
    }
    IEnumerator ResetCharacterAfterAnimE()
    {
        yield return new WaitForSeconds(2f);
        foreach (GameObject item in myCurrentCharacter.GetComponent<CharacterScript>().Swords)
        {
            item.SetActive(false);
        }
        if (myCurrentCharacter.GetComponent<CharacterScript>().CharacterGlasses != null)
            myCurrentCharacter.GetComponent<CharacterScript>().CharacterGlasses.SetActive(true);
        //SoundManager.Instance.DanceTheWorld.Stop();
        anim.Play("Idle");
        yield return new WaitForSeconds(1f);
        isFadeOut = true;        
    }

    public void VolumeFadeOut(AudioSource a)
    {
        StartCoroutine(VolumeFadeOutE(a));
    }
    IEnumerator VolumeFadeOutE(AudioSource audio)
    {
        float audioVolume = audio.volume;
        while (audio.volume >= 0.05f)
        {
            audioVolume -= 0.05f;
            audio.volume = audioVolume;
            yield return new WaitForSeconds(0.1f);
        }
        audio.Stop();
    }
    public void playAudio(AudioSource a)
    {
        a.volume = 1f;
        a.Play();
    }

   
    public void LollipopFall()
    {
        StopCoroutine(LollipopFallE());
        StartCoroutine(LollipopFallE());
    }
    IEnumerator LollipopFallE()
    {
        myCurrentCharacter.GetComponent<CharacterScript> ().LollipopObj.SetActive(true);
        yield return new WaitForSeconds(5.5f);
        myCurrentCharacter.GetComponent<CharacterScript> ().LollipopObj.SetActive(false);
    }

    public void SwordsShow()
    {
        StopCoroutine(SwordsHideE());
        StartCoroutine(SwordsHideE());
    }
    IEnumerator SwordsHideE()
    {
        foreach (GameObject item in myCurrentCharacter.GetComponent<CharacterScript>().Swords)
        {
            item.SetActive(true);
        }
        
        if (myCurrentCharacter.GetComponent<CharacterScript>().CharacterGlasses != null)
            myCurrentCharacter.GetComponent<CharacterScript>().CharacterGlasses.SetActive(false);

        yield return new WaitForSeconds(5.5f);
        //foreach (GameObject item in myCurrentCharacter.GetComponent<CharacterScript>().Swords)
        //{
        //    item.SetActive(false);
        //}
    }

    #endregion


    public GameObject LoadingPanel;
    #region Menu Functions

    public void BackButton()
    {
        
        isRecieveAnimationCallback = false;
        StopAllCoroutines();
        LoadingPanel.SetActive(true);
        GameManager.Instance.isFromGamePlayToMainMenu = true;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

	public void StartDetecting()
	{
        //isStartDetecting = true;
        CardDeckObj.SetActive(true);
        instructionText.gameObject.SetActive(true);
        Instruction1Obj.SetActive(false);
	}

    #endregion



}
